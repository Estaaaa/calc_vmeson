#!/bin/bash

#SBATCH --job-name=esther_test	## Name des Jobs.
#SBATCH --output=output_file		## Ausgabe (stdout) wird in die Datei "output_file" geschrieben. Da was sinnvollen hinschreiben um spaeter verschiedene Ausgaben unterscheiden zu koennen.
#SBATCH --nodes=1					## Verwende einen Node (fuer mehrere Nodes muesste man mit MPI arbeiten).
#SBATCH --constraint=new			## new = neue Nodes (24 Kerne), ghost = alte Nodes (16 Kerne).
#SBATCH --ntasks=1					## Ein Job auf einem Node.
#SBATCH --cpus-per-task=24			## Anzahl der Kerne pro Job festlegen.

# Ob man das wirklich braucht weiss ich nicht genau. Setze ich eigentlich immer auf die gleiche Anzahl wie cpus-per-task.
export OMP_NUM_THREADS=24

# Hier dann was ausgefuehrt werden soll:
#/home/esther/GitHub/calc_vmeson/source/testing/main/testmain
/home/esther/GitHub/calc_vmeson/source/testing/main_formfactor/formfactormain
