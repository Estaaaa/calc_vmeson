#ifndef CHAUCHYINTERPOL
#define CHAUCHYINTERPOL

#endif // CHAUCHYINTERPOL


//vecd &t, vecd &t_weight,VectorXcd &A, VectorXcd B,
void chauchy_parabola(int n_int2, int n_int3, quark_prop &quark,int PauliVillar, string name, double M, double Pr, double fac)
{
    complex<double> result[2];
    complex<double> pc[n_int2+n_int3];
    vecd t; vecd t_weight; VectorXcd A(n_int2+n_int3); VectorXcd B(n_int2+n_int3);


    double xt[n_int2]; double wt[n_int2];
    gauleg(-Pr,Pr,xt,wt,n_int2);

//    for(int i=0; i<n_int2; i++)
//    {
//        xt[i]=-1.0+i*0.001;
//        wt[i]=0.001;
//    }

    for(int i=0; i<n_int2; i++)
    {
        t.push_back(xt[i]); t_weight.push_back(wt[i]);
//        pc[i]=ii*t[i]*0.01+t[i]*t[i]-0.25;
         pc[i]=ii*t[i]*fac*M+t[i]*t[i]-M*M;

        inte_com(result,quark, pc[i], PauliVillar);

        B(i)= quark.Zm*quark.Z2+ quark.Z2*quark.Z2*result[0];
        A(i)= quark.Z2 + quark.Z2*quark.Z2*result[1];
    }

    double xt3[n_int3]; double wt3[n_int3];
    gauleg(-1.0,1.0,xt3,wt3,n_int3);

    for(int i=n_int2; i<n_int3+n_int2; i++)
    {
        t.push_back(xt3[i-n_int2]); t_weight.push_back(wt3[i-n_int2]);
//        pc[i]=1.0-0.25+ii*t[i]*0.01;
          pc[i]=Pr*Pr-M*M+ii*t[i]*M*Pr*fac;

        inte_com(result,quark, pc[i], PauliVillar);

        B(i)= quark.Zm*quark.Z2+ quark.Z2*quark.Z2*result[0];
        A(i)= quark.Z2 + quark.Z2*quark.Z2*result[1];
    }



    //write to file..

    string buffer= "data/input/"+name+".txt";
    ofstream f(buffer.c_str());
    f<<"#real/imag   A		B           t       t_weight    pc "<<endl;
    for(int i=0; i<n_int2+n_int3; i++)
    {
            f<<real(A(i))<<"\t"<<imag(A(i))<<"\t"<<real(B(i))<<"\t"<<imag(B(i))<<"\t"<<t[i]<<"\t"<<t_weight[i]<<"\t"<<real(pc[i])<<"\t"<<imag(pc[i])<<endl;
    }
    f<<quark.Z2<<"\t"<<quark.Zm<<"\t"<<quark.mass<<"\t"<<n_int2<<"\t"<<n_int3<<endl;
    f.close();

    cout<<"Complex propagator calculated!"<<endl;

    string buffer2 ="data/input/"+name+"_info.txt";
    ofstream f2(buffer2.c_str());
    f2<<n_int2<<"\t"<<n_int3<<"\t"<<M<<"\t"<<Pr<<"\t"<<fac<<endl;
    f2.close();
}

complex<double> chauchy_interpol(complex<double> p, VectorXcd &f,vecd &t, vecd &t_weight, int n_int2, int n_int3, double M, double Pr, double fac)
{
    complex<double> result=0.0;
    //cout<<"Im printing p:"<<p<<endl;

    for(int i=0; i<n_int2; i++)
    {
        result += (ii*M*fac+2.0*t[i])*(f(i))/(ii*t[i]*fac*M+t[i]*t[i]-M*M-p)*t_weight[i];
    }

    for(int i=n_int2; i<n_int3+n_int2; i++)
    {
        result += -(ii*M*Pr*fac)*(f(i))/(Pr*Pr-M*M+ii*t[i]*fac*M*Pr-p)*t_weight[i];
    }

    result=-result/(2*Pi*ii);
    return result;

}

void chauchy_ReadInn(string name, int &n_int2, int n_int3,vecd &t, vecd &t_weight,VectorXcd &A, VectorXcd &B)
{

    string buffer = "data/input/"+name+".txt";

    //Reading inn complex quark propagator from file!
    double p2r, p2c, Ar, Ac, Br, Bc, dummy1,dummy2;
    vecd dummyA, dummyB, dummyC, dummyD, dummyE, dummyF;

    ifstream infile;
    infile.open(buffer.c_str(), ios::in);           //DSEreal
    if(infile.is_open())
    {
        string daten;
        while(getline(infile,daten))
        {
            //the following line trims white space from the beginning of the string
            daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

            if(daten[0] == '#') {continue;}
            stringstream(daten)  >> Ar >> Ac >> Br >> Bc >> dummy1 >> dummy2 >> p2r >> p2c;
            if(daten=="") {continue;}
            if(daten[0] == '#') {continue;}

             dummyE.push_back(dummy1);
             dummyF.push_back(dummy2);
             dummyA.push_back(Ar);
             dummyB.push_back(Ac);
             dummyC.push_back(Br);
             dummyD.push_back(Bc);

        }
    }else{cout<<"No data file with propagator found!"<<endl;}

        infile.close();
        int mv=dummyA.size()-1;
        n_int2=dummyD[mv];
        n_int3=dummyE[mv];

        //cout<<"Testing in readInn:  "<<mv<<" , "<<n_int2<<" , "<<n_int3<<endl;

        if(mv==(n_int2+n_int3))
        {
            for(int i=0; i<n_int2+n_int3; i++)
            {
                A(i).real()=dummyA[i];
                A(i).imag()=dummyB[i];
                B(i).real()=dummyC[i];
                B(i).imag()=dummyD[i];
                t.push_back(dummyE[i]);
                t_weight.push_back(dummyF[i]);
            }



           cout<<"Complex prop inn! With    Z2= "<<dummyA[mv]<<" , Zm="<<dummyB[mv]<<" , mass="<<dummyC[mv]<<"\t"<<"n_int2="<<dummyD[mv]<<"\t"<<"  ,n_int3="<<dummyE[mv]<<endl;


            }
            else{cout<<"The complex quark has not the same dimensions than the data you are trying to readINN!!! ABORTED"<<endl; return;}


}


void com_quark_chauchy_interpol(string name, VectorXcd &An, VectorXcd &Bn, vecdcx &pn,double M, double Pr, double fac)
{
   vecd t; vecd t_weight; int n_int2; int n_int3;

   string buffer = "data/input/"+name+"_info.txt";
   ifstream infile;
   infile.open(buffer.c_str(), ios::in);
   if(infile.is_open())
   {
       string daten;
       while(getline(infile,daten))
       {
           stringstream(daten)  >> n_int2 >> n_int3;
           if(daten=="") {continue;}

       }
   }else{cout<<"No data file with propagator found!"<<endl;}
   infile.close();


   VectorXcd A(n_int2+n_int3); VectorXcd B(n_int2+n_int3);

    chauchy_ReadInn(name, n_int2, n_int3, t, t_weight, A, B);

    int np=pn.size();
    cout<<"Here: "<<np<<endl;

       for(int i=0; i<np; i++)
       {
           An(i)=chauchy_interpol(pn[i],A,t,t_weight,n_int2, n_int3, M, Pr, fac);
           Bn(i)=chauchy_interpol(pn[i],B,t,t_weight,n_int2, n_int3,M,Pr, fac);
       }

}

void com_quark_chauchy_interpol_grid(string name, VectorXcd &An, VectorXcd &Bn, grid &g)
{
   vecd t; vecd t_weight; int n_int2; int n_int3; double M; double Pr; double fac;

   string buffer = "data/input/"+name+"_info.txt";
   ifstream infile;
   infile.open(buffer.c_str(), ios::in);
   if(infile.is_open())
   {
       string daten;
       while(getline(infile,daten))
       {
           stringstream(daten)  >> n_int2 >> n_int3 >> M >> Pr >> fac;
           if(daten=="") {continue;}

       }
   }else{cout<<"No data file with propagator found!"<<endl;}
   infile.close();


   VectorXcd A(n_int2+n_int3); VectorXcd B(n_int2+n_int3);

    chauchy_ReadInn(name, n_int2, n_int3, t, t_weight, A, B);

    int np=g.np; complex<double> pn;
    //cout<<"Here: "<<np<<endl;

       for(int i=0; i<np; i++)
       {
           pn=g.p_vector[i]*g.p_vector[i];
           An(i)=chauchy_interpol(pn,A,t,t_weight,n_int2, n_int3, M, Pr,fac);
           Bn(i)=chauchy_interpol(pn,B,t,t_weight,n_int2, n_int3,M,Pr,fac);
       }

}
