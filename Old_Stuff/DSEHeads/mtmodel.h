/*
 * mtmodel.h
 *
 * the Maris-Tandy model for the quark-gluon vertex
 * Used for the kernel in BSE and DSE self-energy, through DSEclass.h and directly
 *
 * ---------------------------------------------------------------------
 * 01.04.16 Esther Weil
 *
 *Content: Contains all MT-model parameter and the function for the interaction mt, mtc
 * ---------------------------------------------------------------------
 *
 *
 *
 */



#ifndef MTMODEL
#define MTMODEL

#endif // MTMODEL

class mtmodel
{
  public:

    double w2; double D; double gm; double tau; double lamQCD2;

 public:

    mtmodel()
    {
        w2=0.16 ; D=0.93312 ; gm=(12.0/(33.0-8.0)); tau=(2.718281828459045235*2.718281828459045235-1.0);
        lamQCD2=(0.234*0.234);

    }

    double mt(double k2, int PauliVillar, int logtail);
    complex<double> mtc(complex<double> k2, int PauliVillar, int logtail);



};

double mtmodel::mt(double k2, int PauliVillar, int logtail)
{
    double Fuv=0.0;
    double F=0.0;
    double a=0.0;
    double wp2=0.4*0.4;
//    double Dp=1.0;
    double Dp=1.01306;



     /*log-tail*/
    if(logtail==1.0)
    {
       F=(1.0-exp(-k2))/k2;
       Fuv=(gm*F)/(0.5*log(tau+(1.0+k2/lamQCD2)*(1.0+k2/lamQCD2)));

       /*G(k2) berechnen*/
       a = 4.0*Pi*Pi*((D/(w2*w2*w2))*exp(-k2/w2)*k2*k2+Fuv*k2);

       if(PauliVillar==1){a = 4.0*Pi*Pi*((Dp/(wp2*wp2*wp2))*exp(-k2/wp2)*k2*k2+Fuv*k2);}
    }
    else
    {
       a = 4.0*Pi*Pi*(D/(w2*w2*w2))*exp(-k2/w2)*k2*k2;
       if(PauliVillar==1){4.0*Pi*Pi*(Dp/wp2)*exp(-k2/wp2)*k2*k2;}
    }
   return a;
}

complex<double> mtmodel::mtc(complex<double> k2, int PauliVillar, int logtail)
{
    complex<double> Fuv=0.0;
    complex<double> F=0.0;
    complex<double> a=0.0;
//    double wp2=0.4111111111*0.411111111111;
//    double Dp=0.98568;
    double wp2=0.4*0.4;
//    double Dp=1.0;
    double Dp=1.01306;

    /*log-tail*/
   if(logtail==1.0)
   {
      F=(1.0-exp(-k2))/k2;
      Fuv=(gm*F)/(0.5*log(tau+(1.0+k2/lamQCD2)*(1.0+k2/lamQCD2)));

      /*G(k2) berechnen*/
      a = 4.0*Pi*Pi*((D/(w2*w2*w2))*exp(-k2/w2)*k2*k2+Fuv*k2);

      if(PauliVillar==1){a = 4.0*Pi*Pi*((Dp/(wp2*wp2*wp2))*exp(-k2/wp2)*k2*k2+Fuv*k2);}
   }
   else
   {
      a = 4.0*Pi*Pi*(D/w2)*exp(-k2/w2)*k2*k2;
      if(PauliVillar==1){4.0*Pi*Pi*(Dp/wp2)*exp(-k2/wp2)*k2*k2;}
   }

  return a;
}
