

class grid
{
  public:

    int m; int nz; int ny;
    vecdcx q2; vecd z; vecd y; vecd q2_weight; vecd z_weight; vecd y_weight; vecd helper;

    int np; vecfv p_vector;

    vecd p_weight;
    int nkw; int nqw; int np3; int nz1; int nz2;
    double g1; double g2; int gridtag;

    //rare decay
    int n_phi;
    vecd phi; vecd phi_weight;

    //Dalitz Decay:
    int n_s; vecd s; vecd s_weight;

    //four body
    vecd k_weight; vecd q_weight; vecd p3_weight; vecd z1_weight; vecd z2_weight;
    vecdcx alpha;
    vecd p1vec, p2vec, p3vec, z2vec, z1vec;


 public:

    //This is if we initialize only a grid for a BSE
    grid(int a, int b, int c)
    {
        m=a; nz=b; ny=c; g1=-1.0; g2=1.0; np=0;
        for(int i=0; i<m; i++)
        {
            q2.push_back(1.0);
            q2_weight.push_back(1.0);
        }
        for(int i=0; i<nz; i++)
        {
            z.push_back(1.0);
            z_weight.push_back(1.0);
        }
        for(int i=0; i<ny; i++)
        {
            y.push_back(1.0);
            y_weight.push_back(1.0);
        }

        n_phi=0; n_s=0; gridtag=0; np=0;

    }

    //This for the pion decay grid
    grid(int a, int b, int c, int d, int e)
    {
        m=a; nz=b; ny=c; g1=-1.0; g2=1.0; np=0; gridtag=1;
        for(int i=0; i<m; i++)
        {
            q2.push_back(1.0);
            q2_weight.push_back(1.0);
        }
        for(int i=0; i<nz; i++)
        {
            z.push_back(1.0);
            z_weight.push_back(1.0);
        }
        for(int i=0; i<ny; i++)
        {
            y.push_back(1.0);
            y_weight.push_back(1.0);
        }

        n_phi=d;
        for(int i=0; i<n_phi; i++)
        {
            phi.push_back(1.0);
            phi_weight.push_back(1.0);
        }

        n_s=e;
        for(int i=0; i<n_s; i++)
        {
            s.push_back(1.0);
            s_weight.push_back(1.0);
        }

    }

    void init(double a1, double a2, int logtag, int decaytag, double x1, double x2);
    void init(double a1, double a2, int logtag, double x1, double x2);

    void set_p(double M, int decaytag);

    void set_p(int n, double IRcut, double UVcut, double Mpi );
    void set_p(int n, int n2, double IRcut, double UVcut, double Mpi, double IReary);
    void set_p_not_log(int n, int n2, double IRcut, double UVcut, double Mpi, double IReary);
    void set_p_dev(int n, double IRcut, double UVcut, double Mpi );
    void set_p_2ep2em(int n, int n2, double Mpi,double me);
    void set_p_epemg(double Mpi, double me);
    void set_p_allrange(int n, double IRcut, double UVcut,double Mpi);
    void set_p_certainvalues(double Mpi);
    void set_p_onevirtual(int n,double Mpi);
    void set_p_2ep2em_direct(int n, int n2, double Mpi, double me);
    void set_p_2ep2em_indirect(int n1, int n2, int n3, int nz1, int nz2, double Mpi, double me, string rundef);
    void set_newwave(int n,double Mpi);

    void get_p(vecd k4, vecd k5,double Mpi);
    void get_p(vecdcx k4, vecdcx k5, double Mpi);

    void write(string name, string rundef);
    void writingout_p(string name, string rundef,double Mpi);



};


//functions of class grid

void grid::init(double a1, double a2, int logtag, int decaytag, double x1, double x2)
{
    g1=a1; g2=a2;
    double xa[nz], wa[nz];
    gauleg(a1, a2, xa,wa,nz);
    for(int i=0; i<nz; i++)          //Winkel SS nz
    {
        if(a1==-1 && a2==1)
        {
            z[i]=xa[i];
            z_weight[i]=wa[i];
        }
        if(a1==0 && a2==Pi)
        {
            z[i]=cos(xa[i]);
            z_weight[i]=wa[i];
        }

    }

    double xay[ny], way[ny];
    gauleg(a1, a2, xay,way,ny);
    for(int i=0; i<ny; i++)          //Winkel SS ny
    {

        if(a1==-1 && a2==1)
        {
            y[i]=xay[i];
            y_weight[i]=way[i];
        }
        if(a1==0 && a2==Pi)
        {
            y[i]=cos(xay[i]);
            y_weight[i]=way[i];
        }
    }

    if(decaytag==3 && gridtag == 1 && n_phi>0)
    {
        double xaphi[n_phi], waphi[n_phi];
        gauleg(-Pi, Pi, xaphi,waphi,n_phi);
        for(int i=0; i<n_phi; i++)          //Winkel SS nphi for rare decay
        {
            phi[i]=xaphi[i];
            phi_weight[i]=waphi[i];
        }

    }


    double x[m];
    double wr[m];		//Radiale SS


//    cout<<"new grid set-up, with sqrt!"<<endl;
//    gauleg(sqrt(log(x1*x1)),sqrt(log(x2*x2)),x,wr,m);
//
//    for (int i=0; i<m; i++)
//    {
//        q2[i]=exp(x[i]*x[i]);
//        q2_weight[i]=wr[i]*2.0*x[i];
//    }

    if(logtag==1)
    {
        gauleg(log(x1*x1),log(x2*x2),x,wr,m);

        for (int i=0; i<m; i++)
        {
            q2[i]=exp(x[i]);
            q2_weight[i]=wr[i];
        }

    }else if(logtag ==2)
    {
        int am=5;
        if(m%am != 0){cout<<"ERROR in init quark: m/amount !=0, change amount or m!! Aborted"<<endl; return;}
        int len = (m/am);
        double k= (log(x2*x2)-log(x1*x1))/(am);
        double x[len];
        double wr[len];


        for(int o=0 ;o<am ;o++)
        {

            gauleg(log(x1*x1)+(o*k),log(x1*x1)+(o+1)*k,x,wr,len);

            for (int i=0; i<len; i++)
            {
                q2[i+o*len]=exp(x[i]);
                q2_weight[i+o*len]=wr[i];
            }
        }
    }else if(logtag ==3)
    {
        int am=3;
        if(m%am != 0){cout<<"ERROR in init quark: m/amount !=0, change amount or m!! Aborted"<<endl; return;}
        int len = (m/am);
        double x[len];
        double wr[len];


        for(int o=0 ;o<am ;o++)
        {

            if(o==0){gauleg(log(x1*x1),0.1,x,wr,len);}
            else if(o==1){gauleg(0.11,10,x,wr,len);}
            else if(o==2){gauleg(11,log(x2*x2),x,wr,len);}

            for (int i=0; i<len; i++)
            {
                q2[i+o*len]=exp(x[i]);
                q2_weight[i+o*len]=wr[i];
            }
        }

    }else if(logtag== 4)
    {
        cout<<"The radial grid is in special distrubition for pion/TFF"<<endl;
        gauleg(-1.0,1.0,x,wr,m);
        double par=3.0; double f, df;

        for (int i = 0; i <m ; ++i) {

            f = sinh(par * x[i])/sinh(par);
            df = par*cosh(par * x[i])/sinh(par);

            q2[i]= x1*x1*pow((x2*x2)/(x1*x1), ((1.0+f)/2.0));
//            q2_weight[i] = ((df/2.0)*log((x2*x2)/(x1*x1))*real(q2[i]))*wr[i]; //*pow(real(q2[i]),2.0);
            q2_weight[i] = ((df/2.0)*log((x2*x2)/(x1*x1)))*wr[i]; //*pow(real(q2[i]),2.0);
//            cout<<x[i]<<tab<< f<<tab<<q2[i]<<tab<<q2_weight[i]<<tab<<wr[i]<<endl;
        }



    }else
    {
      cout<<"The grid is not a log grid"<<endl;
      gauleg(x1*x1,x2*x2,x,wr,m);

      for (int i=0; i<m; i++)
      {
          q2[i]=x[i];
          q2_weight[i]=wr[i];
      }
    }


}
void grid::init(double a1, double a2, int logtag, double x1, double x2)
{
    g1=a1; g2=a2;
    double xa[nz], wa[nz];
    gauleg(a1, a2, xa,wa,nz);
    for(int i=0; i<nz; i++)          //Winkel SS nz
    {
        if(a1==-1 && a2==1)
        {
            z[i]=xa[i];
            z_weight[i]=wa[i];
        }
        if(a1==0 && a2==Pi)
        {
            z[i]=cos(xa[i]);
            z_weight[i]=wa[i];
        }

    }

    double xay[ny], way[ny];
    gauleg(a1, a2, xay,way,ny);
    for(int i=0; i<ny; i++)          //Winkel SS ny
    {

        if(a1==-1 && a2==1)
        {
            y[i]=xay[i];
            y_weight[i]=way[i];
        }
        if(a1==0 && a2==Pi)
        {
            y[i]=cos(xay[i]);
            y_weight[i]=way[i];
        }
    }


    double x[m];
    double wr[m];		//Radiale SS


//    cout<<"new grid set-up, with sqrt!"<<endl;
//    gauleg(sqrt(log(x1*x1)),sqrt(log(x2*x2)),x,wr,m);
//
//    for (int i=0; i<m; i++)
//    {
//        q2[i]=exp(x[i]*x[i]);
//        q2_weight[i]=wr[i]*2.0*x[i];
//    }

    if(logtag==1)
    {
        gauleg(log(x1*x1),log(x2*x2),x,wr,m);

        for (int i=0; i<m; i++)
        {
            q2[i]=exp(x[i]);
            q2_weight[i]=wr[i];
        }

    }else if(logtag ==2)
    {
        int am=5;
        if(m%am != 0){cout<<"ERROR in init quark: m/amount !=0, change amount or m!! Aborted"<<endl; return;}
        int len = (m/am);
        double k= (log(x2*x2)-log(x1*x1))/(am);
        double x[len];
        double wr[len];


        for(int o=0 ;o<am ;o++)
        {

            gauleg(log(x1*x1)+(o*k),log(x1*x1)+(o+1)*k,x,wr,len);

            for (int i=0; i<len; i++)
            {
                q2[i+o*len]=exp(x[i]);
                q2_weight[i+o*len]=wr[i];
            }
        }
    }else if(logtag ==3)
    {
        int am=3;
        if(m%am != 0){cout<<"ERROR in init quark: m/amount !=0, change amount or m!! Aborted"<<endl; return;}
        int len = (m/am);
        double x[len];
        double wr[len];


        for(int o=0 ;o<am ;o++)
        {

            if(o==0){gauleg(log(x1*x1),0.1,x,wr,len);}
            else if(o==1){gauleg(0.11,10,x,wr,len);}
            else if(o==2){gauleg(11,log(x2*x2),x,wr,len);}

            for (int i=0; i<len; i++)
            {
                q2[i+o*len]=exp(x[i]);
                q2_weight[i+o*len]=wr[i];
            }
        }

    }else if(logtag== 4)
    {
        cout<<"The radial grid is in special distrubition for pion/TFF"<<endl;
        gauleg(-1.0,1.0,x,wr,m);
        double par=3.0; double f, df;

        for (int i = 0; i <m ; ++i) {

            f = sinh(par * x[i])/sinh(par);
            df = par*cosh(par * x[i])/sinh(par);

            q2[i]= x1*x1*pow((x2*x2)/(x1*x1), ((1.0+f)/2.0));
//            q2_weight[i] = ((df/2.0)*log((x2*x2)/(x1*x1))*real(q2[i]))*wr[i]; //*pow(real(q2[i]),2.0);
            q2_weight[i] = ((df/2.0)*log((x2*x2)/(x1*x1)))*wr[i]; //*pow(real(q2[i]),2.0);
//            cout<<x[i]<<tab<< f<<tab<<q2[i]<<tab<<q2_weight[i]<<tab<<wr[i]<<endl;
        }



    }else
    {
        cout<<"The grid is not a log grid"<<endl;
        gauleg(x1*x1,x2*x2,x,wr,m);

        for (int i=0; i<m; i++)
        {
            q2[i]=x[i];
            q2_weight[i]=wr[i];
        }
    }


}

void grid::set_p(double M, int decaytag)
{
    if(gridtag!=1){cout<<"This is not an approipiate function, since we do not have a decay grid. ABORTED"<<endl; return;}
    if( decaytag==3 || decaytag==4 || decaytag ==1)
    {
        if(z[0]==1.0){cout<<"The z vector in the grid is empty not modified, forgot init function! Aborted"<<endl; return;}

        // relativ momenta p:

        four_vec p; int counter=0;
        p.inhalt[2]= M;
        p_vector.push_back(p); counter++;
        if(decaytag !=4)
        {
            for(int i=0; i<nz; i++)
            {
                for(int j=0; j<m; j++)
                {

                   p.inhalt[2]= sqrt(q2[j])*sqrt(1.0-z[i]*z[i]); p.inhalt[3]=sqrt(q2[j])*z[i];
                   p_vector.push_back(p);
                   counter++;
                }
            }
        }

      np=counter;
    }
    else if(decaytag==2)
    {
        // relativ momenta p:
        double me=0.000511;
        double xs[n_s]; double ws[n_s];
        four_vec pr,k4,P,p;
        complex<double> p0r, p3r;

        gauleg(4.0*me*me,M*M,xs,ws,n_s);
        int counter=0;

        for(int i=0; i<n_s; i++)
        {
            s[i]=xs[i]; s_weight[i]=ws[i];
            P.inhalt[2]=(M*M-s[i])/(2.0*sqrt(s[i])); P.inhalt[3]=ii*sqrt(M*M+pow(P.inhalt[2],2));
            k4.inhalt[3]=sqrt(s[i])*ii;

            //COMMENTRAY: I have no clue what I did here.. kinda :(

            p=2.0*k4-P;

            p0r=(P*p)/M;
            p3r=sqrt(p*p+p0r*p0r);

            pr.inhalt[2]=p3r; pr.inhalt[3]=ii*p0r;

           p_vector.push_back(pr);
           counter++;
        }

        np=counter;gridtag=4;

    }else{cout<<"Which decay is it? decaytag wrong, no p set! "<<endl;return;}



}

void grid::set_p(int n, double IRcut, double UVcut, double Mpi)
{

    np=n;
    double x[np];
    double wr[np]; double q2n;

    gauleg(log(IRcut*IRcut),log(UVcut*UVcut),x,wr,np);

    double a,b; vecd k4,k5;
    four_vec p; p_vector.resize(np); p_weight.resize(np);
    for (int k = 0; k <np ; ++k)
    {
        /*
        * symmetric case:
        */

        q2n=exp(x[k]);
        k4.push_back(q2n);
        k5.push_back(q2n);
        p_weight[k]=wr[k];

    }

    get_p(k4,k5,Mpi);


}

void grid::set_p(int n, int n2, double IRcut, double UVcut, double Mpi, double IRearly)
{

    np=n+n2; double q2n[n+n2]; p_weight.resize(np);

    double x2[n2];
    double wr2[n2];
    vecd k4,k5;

    simpson(log(IRcut*IRcut),log(IRearly*IRearly),x2,wr2,n2);

    for (int i=0; i<n2; i++)
    {
        q2n[i]=exp(x2[i]);
        p_weight[i]=wr2[i];
        k4.push_back(q2n[i]);
        k5.push_back(q2n[i]);
    }

    double x[n];
    double wr[n];

    gauleg(log(IRearly*IRearly),log(UVcut*UVcut),x,wr,n);

    for (int i=0; i<n; i++)
    {
        q2n[n2+i]=exp(x[i]);
        p_weight[n2+i]=wr[i];
        k4.push_back(q2n[n2+i]);
        k5.push_back(q2n[n2+i]);
    }

    get_p(k4,k5,Mpi);

//    double a,b;
//    four_vec p; p_vector.resize(np);
//    for (int k = 0; k <np ; ++k)
//    {
//        /*
//         * symmetric case:
//         */
//
//        a=0.5*sqrt(4.0*q2n[k]+Mpi*Mpi);
//        b=0;
//
//        p.inhalt[2]= 2.0*a; p.inhalt[3]=2.0*ii*b;
//        p_vector[k]=p;

//    }


}
void grid::set_p_not_log(int n, int n2, double IRcut, double UVcut, double Mpi, double IRearly)
{

    np=n+n2; double q2n[n+n2]; p_weight.resize(np); gridtag=2; helper.resize(np);
    gridtag=2;

    double x2[n2];
    double wr2[n2];

    simpson(IRcut*IRcut,IRearly*IRearly,x2,wr2,n2);         //Setting the grid point linear scale.

    for (int i=0; i<n2; i++)
    {
        q2n[i]=x2[i];
        p_weight[i]=wr2[i]/x2[i];
        helper[i]=wr2[i];
    }

    double x[n];
    double wr[n];

    gauleg(IRearly*IRearly,UVcut*UVcut,x,wr,n);             //Setting the rest of the gp with gauleg.

    for (int i=0; i<n; i++)
    {
        q2n[n2+i]=x[i];
        p_weight[n2+i]=wr[i]/x[i];
        helper[n2+i]=wr[i];
    }

    double a,b;
    four_vec p; p_vector.resize(np);
    for (int k = 0; k <np ; ++k)
    {
        /*
         * symmetric case:
         */

        a=0.5*sqrt(4.0*q2n[k]+Mpi*Mpi);
        b=0;

        p.inhalt[2]= 2.0*a; p.inhalt[3]=2.0*ii*b;
        p_vector[k]=p;

    }


}

void grid::set_p_2ep2em(int n, int n2, double Mpi, double me)
{
    gridtag=3;   //This tell the write function that we have the grid for the pi-> 2e⁺ 2 e⁻ decay
    int nd=n*n2;  k_weight.resize(n); q_weight.resize(nd); nkw=n; nqw=nd;
    double q[nd]; double k[n];

    double x2[n2];
    double wr2[n2];

    double x[n];
    double wr[n];

    gauleg(4.0*pow(me,2),pow(Mpi-2.0*me,2),x,wr,n);

    for (int i=0; i<n; i++)
    {
        k[i]=x[i];
        k_weight[i]=wr[i];
    }


    for (int i = 0; i < n; ++i)
    {
        gauleg(4.0*pow(me,2),pow((Mpi-sqrt(k[i])),2),x2,wr2,n2);
        //cout<<"Test: "<<k[i]<<tab<<sqrt(k[i])<<tab<<pow((Mpi-sqrt(k[i])),2)<<tab<<tab<<4.0*pow(me,2)<<tab<<pow(Mpi-2.0*me,2)<<endl;

        for (int j=0; j<n2; j++)
        {
            q[j+i*n2]=x2[j];
            q_weight[j+i*n2]=wr2[j];

            //cout<<"Testing: "<<k[i]<<tab<<q[j+i*n2]<<tab<<tab<<is<<tab<<tab<<4.0*pow(me,2)<<tab<<pow(Mpi-2.0*me,2)<<endl;
        }

    }

    //Testing the output

//    string buffer2 = "data12/output_0/test.txt";
//    ofstream outa(buffer2.c_str());
//    for (int l = 0; l <n ; ++l) {
//        for (int i = 0; i < n2; ++i) {
//            outa<<k[l]<<tab<<k_weight[l]<<tab<<q[i+l*n2]<<tab<<q_weight[i+l*n2]<<endl;
//        }
//
//    }outa.close();
//    four_vec P; P.inhalt[3]=ii*Mpi; four_vec k4,k5;
//
//    string buffer = "data12/output_0/test1.txt";
//    ofstream outb(buffer.c_str());



    double a,b; np=nd;
    four_vec p; p_vector.resize(np);
    for (int i = 0; i <n ; ++i)
    {
        /*
         * unsymmetric case:
         */

        for (int j = 0; j < n2; ++j)
        {
            a=0.5*sqrt(2.0*(k[i]+q[j+i*n2])+Mpi*Mpi+(k[i]*k[i]+q[j+i*n2]*q[j+i*n2])/(Mpi*Mpi)-2.0*(q[j+i*n2]*k[i])/(Mpi*Mpi));
            b=(k[i]-q[j+i*n2])/(2.0*Mpi);

            p.inhalt[2]= 2.0*a; p.inhalt[3]=2.0*ii*b;
            p_vector[j+n2*i]=p;

//            k4=0.5*(p-P);
//            k5=0.5*(p+P);
//            k4.inhalt[2]=a; k4.inhalt[3]=ii*(b-0.5*Mpi);
//            k5.inhalt[2]=a; k5.inhalt[3]=ii*(b+0.5*Mpi);
//
//            outb<<real(k4*k4)<<tab<<k[i]<<tab<<real(k5*k5)<<tab<<q[j+i*n2]<<tab<<imag(k4*k4)<<tab<<imag(k5*k5)<<endl;
        }


    }
//    outb.close();


}

void grid::set_p_2ep2em_direct(int n, int n2, double Mpi, double me)
{
    gridtag=3;   //This tell the write function that we have the grid for the pi-> 2e⁺ 2 e⁻ decay
    int nd=n*n2;  k_weight.resize(n); q_weight.resize(nd); nkw=n; nqw=nd;
    double q[nd]; double k[n];

    double x2[n2];
    double wr2[n2];

    double x[n];
    double wr[n];


    //Changed it to positve numbers again and put the minus sign in the push_back.
//    gauleg(4.0*pow(me,2),pow(Mpi-2.0*me,2),x,wr,n);
//
//    for (int i=0; i<n; i++)
//    {
//        k[i]=x[i];
//        k_weight[i]=wr[i];
//    }
//
//
//    for (int i = 0; i < n; ++i)
//    {
//        gauleg(4.0*pow(me,2),pow((Mpi-sqrt(k[i])),2),x2,wr2,n2);
//
//        for (int j=0; j<n2; j++)
//        {
//            q[j+i*n2]=x2[j];
//            q_weight[j+i*n2]=wr2[j];
//
//        }
//
//    }

    /*
     * Rewritten grid distribution and integrand Mathematica notebook: esthersfourbodyintegrand
     */

    gauleg(sqrt(2.0*me),sqrt(Mpi-2.0*me),x,wr,n);

    for (int i=0; i<n; i++)
    {
        k[i]=x[i];
        k_weight[i]=wr[i];
    }

    for (int i = 0; i < n; ++i)
    {
        gauleg(sqrt(2.0*me),sqrt(Mpi-x[i]*x[i]),x2,wr2,n2);

        for (int j=0; j<n2; j++)
        {
            q[j+i*n2]=x2[j];
            q_weight[j+i*n2]=wr2[j];

        }

    }



    double a,b; np=nd; vecd k4,k5;
    four_vec p; p_vector.resize(np);
    for (int i = 0; i <n ; ++i)
    {
        for (int j = 0; j < n2; ++j)
        {

//            k4.push_back(-k[i]); k5.push_back(-q[j+i*n2]);
            k4.push_back(-pow(k[i],4.0)); k5.push_back(-pow(q[j+i*n2],4.0));
        }

    }


    get_p(k4,k5,Mpi);



}

void grid::set_p_2ep2em_indirect(int n1, int n2, int n3, int nz1, int nz2, double Mpi, double me, string rundef)
{
    gridtag=5;   //This tell the write function that we have the grid for the pi-> 2e⁺ 2 e⁻ decay

//    k_weight.resize(n1); q_weight.resize(n2); z1_weight.resize(nz1);
//    z2_weight.resize(nz2); p3_weight.resize(np3);
    nkw=n1; nqw=n2;

//    if(n1==n2 && n2==np3 && nz1==nz2){cout<<"Right choice"<<endl;}else{cout<<"So far only same n of momenta is possible, ABORTED!"<<endl; assert(0);}

    double x[n1]; double w[n1];
    double xz[nz1]; double wz[nz1];

    double x2[n2]; double w2[n2];
    double xz2[nz2]; double wz2[nz2];

    double x3[n3]; double w3[n3];


//    gauleg(0.0,sqrt(Mpi*Mpi-me*me),x,w,n1);
    gauleg(0.0,1.0,x,w,n1);

//    gauleg(0.0,sqrt(Mpi*Mpi-me*me),x2,w2,n2);
    gauleg(0.0,1.0,x2,w2,n2);

//    gauleg(0.0,sqrt(Mpi*Mpi-me*me),x3,w3,n3);
    gauleg(0.0,1.0,x3,w3,n3);


    gauleg(-1.0,1.0,xz,wz,nz1);

    gauleg(-1.0,1.0,xz2,wz2,nz2);


    vecdcx k4, k5;
    four_vec p1, p2, p3, p4, P;
    P.inhalt[3]=ii*Mpi;
    double E1, E2, E3, E4=0.0;
    double alphavalue;
    double k4s, k5s, k4sp, k5sp;
    double q1,q2,q3,cos2,cos3;
//    vecdcx alpha;

    vecvecint saver; vecint combi; combi.resize(5);


    for (int j = 0; j < n1; ++j) {
        for (int i = 0; i < n2; ++i) {
            for (int k = 0; k < n3; ++k) {
                for (int l = 0; l < nz1; ++l) {
                    for (int n = 0; n < nz2; ++n) {

                        q1=x[j]*sqrt(Mpi*Mpi-me*me); q2=x2[i]*sqrt(Mpi*Mpi-me*me); q3=x3[k]*sqrt(Mpi*Mpi-me*me); cos2=xz[l]; cos3=xz2[n];
                        E1=sqrt(me*me+q1*q1); E2=sqrt(me*me+q2*q2); E3=sqrt(me*me+q3*q3);
                        p1.inhalt[2]=q1; p1.inhalt[3]=ii*E1;
                        p2.inhalt[0]=q2*sqrt(1.0-cos2*cos2);  p2.inhalt[2]=q2*cos2; p2.inhalt[3]=ii*E2;
                        p3.inhalt[0]=q3*sqrt(1.0-cos3*cos3); p3.inhalt[2]=q3*cos3; p3.inhalt[3]=ii*E3;
                        p4=P-p1-p2-p3; E4=imag(p4.inhalt[3]);


                        //k and q
                        k4s=real((p1+p2)*(p1+p2));
                        k5s=real((p3+p4)*(p3+p4));

                        //k' and q'
                        k4sp=real((p1+p4)*(p1+p4));
                        k5sp=real((p3+p2)*(p3+p2));


//                        alphavalue= 1.0/(2.0*x2[i]*x3[k]*sqrt(1.0-xz[l]*xz[l])*sqrt(1.0-xz2[n]*xz2[n])) *
//                                    (pow((Mpi-E1-E2-E3),2.0)-me*me-x[j]*x[j]
//                                     -x2[i]*x2[i]-x3[k]*x3[k]-2.0*x[j]*x2[i]*xz[l]-2.0*x[j]*x3[k]*xz2[n]
//                                     -2.0*x2[i]*x3[k]*xz[l]*xz2[n]);

                        alphavalue= 1.0/(2.0*q2*q3*sqrt(1.0-cos2*cos2)*sqrt(1.0-cos3*cos3)) *
                                    (pow((Mpi-E1-E2-E3),2.0)-me*me-q1*q1
                                     -q2*q2-q3*q3-2.0*q1*q2*cos2-2.0*q1*q3*cos3
                                     -2.0*q2*q3*cos2*cos3);

                        if(j==0 && i==25 && k==41 & l==9 & n==3){cout<<"-------------My values:  "<<alphavalue<<tab<<E1<<tab<<E2<<tab<<k4s<<tab<<k5sp<<endl;}

                        if(alphavalue!=alphavalue){cin.get();}
                        if((k4s < -4.0*me*me) && (k5s < -4.0*me*me) && k4sp < -4.0*me*me && k5s < -4.0*me*me && abs(alphavalue) < 1.0 && E4> me &&
                                (sqrt(-k4s)+sqrt(-k5s)) < Mpi && (sqrt(-k4sp)+sqrt(-k5sp)) < Mpi)
//                        if((k4s < -4.0*me*me) && (k5s < -4.0*me*me) && (k4sp < -4.0*me*me) && (k5s < -4.0*me*me) && E4> me &&
//                           (sqrt(-k4s)+sqrt(-k5s)) < Mpi && (sqrt(-k4sp)+sqrt(-k5sp)) < Mpi)
//                        if( abs(alphavalue) < 1.0 )
                        {
//                            cout<<j<<":"<<i<<":"<<k<<":"<<l<<":"<<n<<"  ::";
//                            cout<<k4s<<tab<<k5s<<tab<<k4sp<<tab<<k5sp<<tab<<alphavalue<<tab<<E1<<tab<<E2<<tab<<E3<<tab<<E4<<endl;
//                            cout<<x[j]<<tab<<x2[i]<<tab<<x3[k]<<endl;

                            k4.push_back(k4s);
                            k5.push_back(k5s);
                            k4.push_back(k4sp);
                            k5.push_back(k5sp);

                            alpha.push_back(alphavalue);

                            combi[0]=j; combi[1]=i; combi[2]=k; combi[3]=l; combi[4]=n;
                            saver.push_back(combi);


                        }

//                        if(cz2==1){z2_weight.push_back(wz[n]); z2vec.push_back(xz[n]);cz2=0;}else{z2_weight.push_back(0.0); z2vec.push_back(0.0);}
                    }
//                    if(cz1==1){z1_weight.push_back(wz[l]);z1vec.push_back(xz[l]);cz1=0;}else{z1_weight.push_back(0.0);z1vec.push_back(0.0);}
                }
//                if(cq3==1){p3_weight.push_back(w[k]);p3vec.push_back(x[k]);cq3=0;}else{p3_weight.push_back(0.0);p3vec.push_back(0.0);}
            }
//            if(cq2==1){q_weight.push_back(w[i]);p2vec.push_back(x[i]);cq2=0;}else{q_weight.push_back(0.0);p2vec.push_back(0.0);}
        }
    }



//    n1=p1vec.size(); n2=p2vec.size(); np3=p3vec.size(); nz1=z1vec.size(); nz2=z2vec.size();
    int nd=k4.size(); int nsaver=saver.size();

    cout<<"set_p_2ep2em_indirect:: The size that was achieved was: np="<<nd<<tab<<nsaver<<endl;
    if(nd==0){}//cout<<"Shout!"<<endl;cin.get();}

    p_vector.resize(nd);

    get_p(k4,k5,Mpi);


    string buffer = rundef+"indirect_part_grid_access.txt";
    ofstream f(buffer.c_str());

    for (int i1 = 0; i1 < nsaver; ++i1)
    {
        f<<saver[i1][0]<<tab<<saver[i1][1]<<tab<<saver[i1][2]<<tab<<saver[i1][3]<<tab<<saver[i1][4]<<endl;
    }

    f.close();

    buffer = rundef+"indirect_part_alpha.txt";
    ofstream f3(buffer.c_str());

    for (int i1 = 0; i1 < nsaver; ++i1)
    {
        f3<<scientific<<real(alpha[i1])<<tab<<imag(alpha[i1])<<endl;
    }

    f3.close();

    buffer = rundef+"indirect_part_weights.txt";
    ofstream f2(buffer.c_str());

    for (int k1 = 0; k1 < n1; ++k1) {
        f2<<scientific<<x[k1]<<tab<<w[k1]<<endl;
    }f2<<"#"<<endl;
    for (int k2 = 0; k2 < n2; ++k2) {
        f2<<scientific<<x2[k2]<<tab<<w2[k2]<<endl;
    }f2<<"#"<<endl;
    for (int k3 = 0; k3 < n3; ++k3) {
        f2<<scientific<<x3[k3]<<tab<<w3[k3]<<endl;
    }f2<<"#"<<endl;
    for (int k4 = 0; k4 < nz1; ++k4) {
        f2<<scientific<<xz[k4]<<tab<<wz[k4]<<endl;
    }f2<<"#"<<endl;
    for (int k5 = 0; k5 < nz2; ++k5) {
        f2<<scientific<<xz2[k5]<<tab<<wz2[k5]<<endl;
    }
    f2.close();

}

void grid::set_p_allrange(int n, double IRcut, double UVcut,double Mpi)
{
    gridtag=1;   //This tell the write function that we have the grid for the pi-> gamma decay


    double x[n];
    double wr[n];

    gauleg(log(IRcut*IRcut),log(UVcut*UVcut),x,wr,n);


    double q; double k;
    double a,b; np=n*n+1;
    four_vec p; p_vector.resize(np);

    //on-shell point:
    p.inhalt[2]= Mpi;
    p_vector[0]=p;

    for (int i = 0; i <n ; ++i)
    {
        /*
         * unsymmetric case:
         */

        q=exp(x[i]);

        for (int j = 0; j < n; ++j)
        {
            k=exp(x[j]);

            a=0.5*sqrt(2.0*(k+q)+Mpi*Mpi+(k*k+q*q)/(Mpi*Mpi)-2.0*(q*k)/(Mpi*Mpi));
            b=(k-q)/(2.0*Mpi);

            p.inhalt[2]= 2.0*a; p.inhalt[3]=2.0*ii*b;
            p_vector[1+j+i*n]=p;
        }



    }


}

void grid::set_p_onevirtual(int n,double Mpi)
{
    gridtag=1;   //This tell the write function that we have the grid for the pi-> gamma decay

    double a,b; np=n;
    vecd q,k;


    for (int j = 0; j < n; ++j)
    {
        k.push_back(-0.134+((0.134-(-0.134))/n)*j);
        q.push_back(0.0);
    }

    get_p(k,q,Mpi);




}

void grid::set_newwave(int n,double Mpi)
{
    gridtag=1;   //This tell the write function that we have the grid for the pi-> gamma decay

    double a,b; np=n;
    vecd q,k;


    for (int j = 0; j < n; ++j)
    {
        for (int i = 0; i < n; ++i)
        {
            k.push_back(-0.7+((4.0-(-0.7))/n)*j);
            q.push_back(-0.7+((4.0-(-0.7))/n)*i);
        }
    }

    get_p(k,q,Mpi);




}

void grid::set_p_certainvalues(double Mpi)
{
    gridtag=1;   //This tell the write function that we have the grid for the pi-> gamma decay

//    double qv[]={-0.5,0.0,1.0,10,20,25,30};
//    double kv[]={-1.0,-0.5,0.0,1.0,10,20,25,30};

//    double qv[]={-0.14,-0.10,-0.05,0,0.05,0.1,0.14};
//    double kv[]={-0.14,-0.10,-0.05,0,0.05,0.1,0.14};

//    double qv[]={0.0, -2.24243e-06, -0.00095176, -0.00276692, -0.018676 };
//    double kv[]={0.0};

    double qv[]={0.0};
    double kv[]={0.0};


//    double qv[]={-0.0183545,5.4845e-19,-0.0178439,0};
//    double kv[]={5.4845e-19,-0.0183545,-3.5839e-06,0};


    int n1=sizeof(qv)/sizeof(double); int n2=sizeof(kv)/sizeof(double);
    np=n1*n2;
//    np=n1;
    vecd q,k;
    four_vec p; p_vector.resize(np);

    for (int i = 0; i <n1 ; ++i)
    {

        for (int j = 0; j < n2; ++j)
        {
            k.push_back(kv[j]);
            q.push_back(qv[i]);
        }
    }

    get_p(q,k,Mpi);

//    //on-shell point:
//    p.inhalt[2]= Mpi;
//    p_vector.push_back(p); np=np+1;
    cout<<"This is the new n_p= "<<np<<endl;


}


void grid::set_p_dev(int n, double IRcut, double UVcut, double Mpi)
{

    double q2n[n]; p_weight.resize(n); np=n;

    double x2[n];
    double wr2[n];

    simpson(log(IRcut*IRcut),log(UVcut*UVcut),x2,wr2,n);

    for (int i=0; i<n; i++)
    {
        q2n[i]=exp(x2[i]);
        p_weight[i]=wr2[i];
    }

    double a,b;
    four_vec p; p_vector.resize(n);
    for (int k = 0; k <n ; ++k)
    {
        /*
         * symmetric case:
         */

        a=0.5*sqrt(4.0*q2n[k]+Mpi*Mpi);
        b=0;

        p.inhalt[2]= 2.0*a; p.inhalt[3]=2.0*ii*b;
        p_vector[k]=p;

    }


}

void grid::set_p_epemg(double Mpi, double me)
{
    gridtag=4;
    double xs[n_s]; double ws[n_s];

    gauleg(4.0*me*me,Mpi*Mpi,xs,ws,n_s);
//    gauleg(4.0*me*me,0.135*0.135,xs,ws,n_s);

    double a,b;
    four_vec p; p_vector.resize(n_s); np=n_s; p_weight.resize(np); vecd k5;

    for(int i=0; i<n_s; i++)
    {
        s[i]=-xs[i]; s_weight[i]=ws[i]; //I changed a minus sign here to try ro calculate the form factor for negative values as its suppose to..!!!!!

        /*
         * Dalitz Decay
         * k5²=0 and k4²=s²
         */

        k5.push_back(0.0);

//        a=0.5*sqrt(2.0*s[i] + Mpi*Mpi + (s[i]*s[i])/(Mpi*Mpi));
//        b=s[i]/(2.0*Mpi);
//
//        p.inhalt[2]= 2.0*a; imag(p.inhalt[3])=2.0*b;
//        p_vector[i]=p; p_weight[i]=s_weight[i];
//        cout<<"Test: "<<p*p<<endl;

    }

    get_p(s,k5,Mpi);


}


void grid::get_p(vecd k4, vecd k5, double Mpi)
{
    int n=k4.size(); cdouble k,q;
    cdouble a,b;
    four_vec p;
    p_vector.resize(n);

    for (int i = 0; i < n; ++i)
    {
        k=k4[i]; q=k5[i];

        a=0.5*sqrt(2.0*(k+q)+Mpi*Mpi+(k*k+q*q)/(Mpi*Mpi)-2.0*(q*k)/(Mpi*Mpi));
        b=(k-q)/(2.0*Mpi);

        p.inhalt[2]= 2.0*a; p.inhalt[3]=2.0*ii*b;
        p_vector[i]=p;
        cout<<"p="<<p*p<<tab<<k<<tab<<q<<tab<<a<<tab<<b<<endl;
    }

}

void grid::get_p(vecdcx k4, vecdcx k5, double Mpi)
{
    int n=k4.size(); cdouble k,q;
    cdouble a,b;
    four_vec p;
    p_vector.resize(n);

    for (int i = 0; i < n; ++i)
    {
        k=k4[i]; q=k5[i];

        a=0.5*sqrt(2.0*(k+q)+Mpi*Mpi+(k*k+q*q)/(Mpi*Mpi)-2.0*(q*k)/(Mpi*Mpi));
        b=(k-q)/(2.0*Mpi);

        p.inhalt[2]= 2.0*a; p.inhalt[3]=2.0*b*ii;
        p_vector[i]=p;
//        cout<<"p="<<p*p<<tab<<k<<tab<<q<<endl;
    }
    np=n;
    cout<<"This is the p momentum with n_p="<<n<<endl;

}


void grid::write(string name, string rundef)
{
    string buffer = rundef+"decay_grid_"+name+".txt";
    ofstream outb(buffer.c_str());
    outb<<"#--------------------------------------------------# "<<endl;
    outb<<"m="<<m<<" ,nz="<<nz<<" ,ny="<<ny<<", np="<<np<<", n_phi="<<n_phi<<" ,n_a="<<n_s<<endl;
    outb<<"# p2"<<endl;
    for(int i=0; i<m; i++)
    {
            outb<<q2[i]<<tab<<q2_weight[i]<<endl;
    }outb<<endl<<endl;
    outb<<"# z"<<endl;
    for(int i=0; i<nz; i++)
    {
            outb<<z[i]<<tab<<z_weight[i]<<endl;
    }outb<<endl<<endl;
     outb<<"# y"<<endl;
    for(int i=0; i<ny; i++)
    {
            outb<<y[i]<<tab<<y_weight[i]<<endl;
    }outb<<endl<<endl;
     outb<<"# phi"<<endl;
    for(int i=0; i<n_phi; i++)
    {
            outb<<phi[i]<<tab<<phi_weight[i]<<endl;
    }outb<<endl<<endl;
    outb<<"# p"<<endl;
    for(int i=0; i<np; i++)
    {
            outb<<p_vector[i].inhalt[0]<<tab<<p_vector[i].inhalt[1]<<tab<<p_vector[i].inhalt[2]<<tab<<p_vector[i].inhalt[3]<<endl;
    }outb<<endl<<endl;

    outb.close();

    if(gridtag==4)
    {
        buffer=rundef+"s_weights.txt";
        ofstream outa(buffer.c_str());
        outa<<"# s"<<endl;
        for(int i=0; i<n_s; i++)
        {
            outa<<s[i]<<tab<<s_weight[i]<<endl;
        }
        outa.close();


    }
    else if(gridtag==3)
    {
        buffer=rundef+"qk_weights.txt";
        ofstream outa(buffer.c_str());

        for (int i = 0; i <nkw ; ++i) {
            outa<<k_weight[i]<<endl;
        }
        outa<<endl<<endl;

        for (int i = 0; i < nqw; ++i) {
            outa<<q_weight[i]<<endl;
        }
//        if(gridtag==5)
//        {
//            for (int i = 0; i < np3; ++i) {
//                outa<<p3_weight[i]<<endl;
//            }
//
//        }

        outa.close();

        if(gridtag==5)
        {
//            buffer=rundef+"angular_weights.txt";
//            ofstream outa(buffer.c_str());
//
//            for (int i = 0; i <nz1 ; ++i) {
//                outa<<z1_weight[i]<<endl;
//            }
//            outa<<endl<<endl;
//
//            for (int i = 0; i < nz2; ++i) {
//                outa<<z2_weight[i]<<endl;
//            }
//
//            outa.close();
        }

    }else
    {
        if(p_weight.size()==p_vector.size())
        {
            buffer=rundef+"p_weights.txt";
            ofstream outa(buffer.c_str());
            for (int i = 0; i <p_vector.size() ; ++i) {
                if(gridtag==2){outa<<helper[i]<<endl;}
                else{outa<<p_weight[i]<<endl;}
            }

            outa.close();
        }

    }


}

void grid::writingout_p(string name, string rundef,double Mpi)
{

    string buffer = rundef+"decay_grid_"+name+".txt";
    ofstream outb(buffer.c_str());
    outb<<"#--------------------------------------------------# "<<endl;
    four_vec P,k4,k5; P.inhalt[3]=ii*Mpi;

    for (int i = 0; i <np ; ++i) {

        k4=0.5*(p_vector[i]-P);
        k5=0.5*(p_vector[i]+P);
        outb<<real(p_vector[i]*p_vector[i])<<tab<<imag(p_vector[i]*p_vector[i])<<tab<<real(k4*k4)<<tab<<imag(k4*k4)<<tab<<real(k5*k5)<<tab<<imag(k5*k5)<<endl;
    }

    outb.close();

}
