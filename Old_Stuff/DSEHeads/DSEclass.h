/*
 * DSEclass.h
 *
 * class for a quark propagator: contains the amplitudes and momenta
 * functions: init, calc, readInn, write
 *
 *class for the BSE: contains all amplitudes and momenta
 *functions: init, write, quarkinit
 *
 * ---------------------------------------------------------------------
 * 04.09.15 Esther Weil
 *
 *Content: cacl of real propagator, dse class, bse class
 *
 *
 *
 *class quark_prop: contains all the calculations needed for the real quark propagator and saves the results in this
 *new created object
 *
 *
 *class bse_amplitude : contains all the information of the bse, the calculation is done in BSE.h.. maybe include in this?
 *
 * ---------------------------------------------------------------------
 *
 *
 *WORK needs to be done: ReadInn & Writeout for a different number of BSE amplitudes
 *
 */


#pragma once
//DSE class

class quark_prop
{
    public:
    int m;
    int n;
    vecd A;
    vecd B;
    vecd p2;
    vecd radw;
    vecd ang;
    vecd angw;
    double Z2;
    double Zm;
    double mass;
    double renormp;
    double UVcut; double IRcut;

    public:
    quark_prop(){cout<<"DSEclass:: Need size for creation of quark."<<endl;}

        quark_prop(int a, int b)
        {
            m=a; n=b; renormp=361.0;
            Z2=1.0; Zm=1.0;
            for(int i=0; i<m; i++)
            {
                A.push_back(1.0); B.push_back(1.0); p2.push_back(0.0); radw.push_back(0.0);
            }
            p2.push_back(0.0);A.push_back(1.0); B.push_back(1.0);

            for(int i=0; i<n; i++)
            {
                ang.push_back(0.0); angw.push_back(0.0);
            }
        }

        quark_prop(int a, int b, vecd &p2out)
        {
            m=a; n=b; renormp=361.0;
            Z2=1.0; Zm=1.0;
            for(int i=0; i<m; i++)
            {
                A.push_back(1.0); B.push_back(1.0); p2.push_back(0.0); radw.push_back(0.0);
                p2[i]=p2out[i];
            }
            p2.push_back(0.0);A.push_back(1.0); B.push_back(1.0);

            for(int i=0; i<n; i++)
            {
                ang.push_back(0.0); angw.push_back(0.0);
            }
        }

        quark_prop(int a, int b, double IR, double UV)
        {
            renormp=361.0; IRcut=IR; UVcut=UV; m=a; n=b;
            Z2=1.0; Zm=1.0;
            for(int i=0; i<m; i++)
            {
                A.push_back(1.0); B.push_back(1.0); p2.push_back(0.0); radw.push_back(0.0);
            }
            p2.push_back(0.0);A.push_back(1.0); B.push_back(1.0);

            for(int i=0; i<n; i++)
            {
                ang.push_back(0.0); angw.push_back(0.0);
            }
        }


    bool init(double a1, double a2, int logtag, double x1, double x2);

    bool init_grid(double a1, double a2,grid g);

    void write(int steps, string rundef);

    void calc(double exact, int logtag, double masse,int PauliVillar, double x1, double x2);

    void readInn(int steps, double a1, double a2, string rundef);

    void spline_interp(quark_prop quark2);

    void integrate(double re[2], int j, int PauliVillar);






private:


//    void integrate(double re[2], int j, int PauliVillar);

	


};

//function of the quark class:

bool quark_prop::init(double a1, double a2, int logtag, double x1, double x2)
{
    bool outcome=true; //initz=true;
    double xa[n], wa[n];
    gauleg(a1, a2, xa,wa,n);
    if(a1==-1 && a2==1)
    {
        for(int i=0; i<n; i++)
        {
            ang[i]=xa[i];
            angw[i]=wa[i];
        }
    }
    if(a1==0 && a2==Pi)
    {
        for(int i=0; i<n; i++)
        {
            ang[i]=cos(xa[i]);
            angw[i]=wa[i];
        }
    }

    double x[m];
    double wr[m];		//Radiale SS


    if(logtag==1)
    {
        gauleg(log(x1*x1),log(x2*x2),x,wr,m);

        for (int i=0; i<m; i++)
        {
            p2[i]=exp(x[i]);
            radw[i]=wr[i];
        }

    }else if(logtag==0)
    {
        cout<<"The grid is not a log grid"<<endl;
        gauleg(x1*x1,x2*x2,x,wr,m);
        for (int i=0; i<m; i++)
        {
            p2[i]=x[i];
            radw[i]=wr[i];
        }

    }else
    {
            int am=5;
            if(m%am != 0){cout<<"ERROR in init quark: m/amount !=0, change amount or m!! Aborted"<<endl; outcome=false; return outcome;}
            int len = (m/am);
            double k= (log(x2*x2)-log(x1*x1))/(am);
            double x[len];
            double wr[len];


            for(int o=0 ;o<am ;o++)
            {

                gauleg(log(x1*x1)+(o*k),log(x1*x1)+(o+1)*k,x,wr,len);

                for (int i=0; i<len; i++)
                {
                    p2[i+o*len]=exp(x[i]);
                    radw[i+o*len]=wr[i];
                }
            }
    }

    Z2=1.0; Zm=1.0; UVcut=x2; IRcut=x1;

    return outcome;

}

bool quark_prop::init_grid(double a1, double a2,grid g)
{
    bool outcome=true;
    if(m!=g.m){outcome=false; cout<<"The dimension of the quark and the grid aren't the same! Aborted"<<endl; return outcome;}

    double xa[n], wa[n];
    gauleg(a1, a2, xa,wa,n);
    if(a1==-1 && a2==1)
    {
        for(int i=0; i<n; i++)
        {
            ang[i]=xa[i];
            angw[i]=wa[i];
        }
    }
    if(a1==0 && a2==Pi)
    {
        for(int i=0; i<n; i++)
        {
            ang[i]=cos(xa[i]);
            angw[i]=wa[i];
        }
    }


    double x[m];
    double wr[m];		//Radiale SS


    for (int i=0; i<m; i++)
    {
        p2[i]=real(g.q2[i]);
        radw[i]=g.q2_weight[i];
    }

    Z2=1.0; Zm=1.0;

    return outcome;

}

void quark_prop::write(int steps, string rundef)
{
    stringstream help;
    help<<steps;
    string buffer = rundef+"real_quark_propagator_"+help.str()+".txt";
    ofstream f(buffer.c_str());
    f<<"#p2   A		B        radw"<<endl;
    for(int i=0; i<m; i++)
    {
        f<<scientific<<setprecision(10)<<p2[i]<<tab<<A[i]<<tab<<B[i]<<tab<<radw[i]<<endl;
    }
    f<<"#Z2    Zm   mass    m   n   IRcut   Uvcut"<<endl;
    f<<Z2<<tab<<Zm<<tab<<mass<<tab<<m<<tab<<n<<tab<<IRcut<<tab<<UVcut<<endl;
    f.close();

}

void quark_prop::readInn(int steps, double a1, double a2, string rundef)
{
    stringstream help;
    help<<steps;
    string buffer = rundef+"real_quark_propagator_"+help.str()+".txt";

    //Reading inn real quark propagator from file!
            double Rp2, Ap, Bp, rad;
            vecd dummyA, dummyB, dummyC, dummyD; int dm, dn;

            ifstream infile;
            infile.open(buffer.c_str(), ios::in);           //DSEreal
            if(infile.is_open())
            {
                string daten;
                while(getline(infile,daten))
                {
                    //the following line trims white space from the beginning of the string
                    daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

                    if(daten[0] == '#') {continue;}
                    stringstream(daten) >> Rp2 >> Ap >> Bp >> rad;
                    if(daten=="#") { continue;}
                    stringstream(daten) >> Z2 >> Zm >> mass >> dm >> dn >> IRcut >> UVcut;
                    if(daten=="") {continue;}

                     dummyA.push_back(Rp2);
                     dummyB.push_back(Ap);
                     dummyC.push_back(Bp);
                     dummyD.push_back(rad);
                }
            }else{cout<<"No data file with real quark found! at: "<<buffer<<endl;}

            infile.close();
            int mv=dummyA.size()-1;
            //cout<<"HERE:        "<<mv<<endl;

            if(mv==m && dm==m)
            {
                for(int i=0; i<mv; i++)
                {
                    p2[i]=dummyA[i];
                    A[i]=dummyB[i];
                    B[i]=dummyC[i];
                    radw[i]=dummyD[i];
                }

                //Winkelstützstellen
                double xa[n], wa[n];
                gauleg(a1, a2, xa,wa,n);
                for(int i=0; i<n; i++)
                {
                    ang[i]=xa[i];
                    angw[i]=wa[i];
                }

                double x[m];
                double wr[m];		//Radiale SS


//                    gauleg(log(IRcut*IRcut),log(UVcut*UVcut),x,wr,m);
//
//                    for (int i=0; i<m; i++)
//                    {
//                        p2[i]=exp(x[i]);
//                        radw[i]=wr[i];
//                    }


                cout<<"Real propagator inn!  Z2: "<<Z2<<" , Zm: "<<Zm<<"   m="<<m<<" n="<<n<<" mass="<<mass<<"   IR="<<IRcut<<" , UV="<<UVcut<<endl<<endl;

            }
            else{cout<<"The quark has not the same dimensions that the data you are trying to readINN!!! ABORTED"<<endl; return;}



}

void quark_prop ::integrate(double re[2], int j, int PauliVillar)
{
    double resultB =0.0;
    double resultA =0.0;
    double q2=0.0;
    double B1=0.0;
    double A1=0.0;
    double pqz=0.0;
    double k2=0.0;
    double alpha=0.0;
    double vorfac=(1.0)/(8.0*Pi*Pi*Pi)*((4.0)/(3.0)) ;

    double pp=p2[j];
//    double pp=j;

    mtmodel interaction;


    //Radiale Integration
    for (int k=0; k<m; k++)
    {
        double wink1=0.0;
        double wink2=0.0;
        q2=p2[k];
        B1=B[k];
        A1=A[k];


        //Winkel Integration:
        for (int l=0; l<n; l++)
        {
            pqz= sqrt(pp*q2)*ang[l];
            k2= pp+q2-2.0*pqz;


           //Alpha berechnen
//            alpha = einter(k2,PauliVillar);
            alpha=interaction.mt(k2, PauliVillar,1);

            if(PauliVillar==0)
            { wink1 += ((sqrt(1.0-ang[l]*ang[l]))/k2)*angw[l]*alpha;
              wink2 += (((sqrt(1.0-ang[l]*ang[l]))/(k2*k2))*(-2.0*q2+3.0*(1.0+(q2/pp))*pqz-4.0*q2*ang[l]*ang[l]))*angw[l]*alpha;}

            else if(PauliVillar==1)
            { wink1 += ((sqrt(1.0-ang[l]*ang[l]))/(k2*(1.0+k2/lam2)))*angw[l]*alpha;
              wink2 += (((sqrt(1.0-ang[l]*ang[l]))/((k2*(1.0+k2/lam2))*k2))*(-2.0*q2+3.0*(1.0+(q2/pp))*pqz-4.0*q2*ang[l]*ang[l]))*angw[l]*alpha;}


        }

        resultB += (3.0*B1*vorfac)/(q2*A1*A1+B1*B1)*q2*q2*wink1*radw[k];			//Radiale Integration
        resultA += (A1*vorfac)/(q2*A1*A1+B1*B1)*q2*q2*wink2*radw[k];                  //p2*p2 wegen log-grid kommt eine zusätzlich p2 factor!

    }

    re[0]=resultB;
    re[1]=resultA;

//    cout<<"quark propb @"<<tab<<pp<<":     "<<resultA<<tab<<resultB<<tab<<A1<<tab<<(1.0+resultA)<<endl;
}

void quark_prop::calc(double exact, int logtag, double masse,int PauliVillar, double x1, double x2)
{
    if(IRcut!=x1 || UVcut !=x2){cout<<"quark_prop CALC:: The quark was initialized with a different cutoff. Changing cutoff to the calcution one."<<endl; IRcut=x1; UVcut=x2;}
    init(-1.0,1.0,logtag,IRcut,UVcut);
    p2[m]=renormp;
    mass=masse;
    if(mass==0 || mass<1e-5){cout<<"quark_prop CALC:: Note: The quark is in the chiral limit! "<<endl;}

    double eps=1.0;			//Genauigkeit für die Iteration
    double h=0.0;
    double An[m+1];
    double Bn[m+1];			// Array als Diskretisierung der Funktion B(p^2)
    double Mn[m+1];
    double Mq[m+1];
    double As[m+1];
    double Bs[m+1];
    double result[2];
    int steps=0;
    int wh=m;             //renormalisierungs punkt


    //Iteration:----- (so lange Genauigkeit nicht kleiner als eps)----
    while (eps>=exact)
    {
        eps=0.0;
        for (int l=0; l<m+1; l++)
        {

            Bn[l]=B[l];
            An[l]=A[l];
        }
        //#pragma omp parallel for
        for (int j=0; j<m+1; j++)
        {
            integrate(result,j, PauliVillar);                           //inte(result,n,m, B, p2,j,radw, asz, awz, A);

            Bs[j]= Z2*result[0];
            As[j]= Z2*result[1];
            B[j]= Zm*Z2+ Z2*Bs[j];
            A[j]= Z2 + Z2*As[j];
//            if(force==1.0){A[j]=1.0;}               //if only calcauting the bse with one ampltiude this is needed.


            Mq[j]=B[j]/A[j];
            Mn[j]=Bn[j]/An[j];
            h = fabs((Mq[j]-Mn[j])/(Mq[j]+Mn[j]));
            if(h >= eps)
            {
                eps=h;
            }
        }


         steps++;
         //cout<<"--------------------->"<<"Eps: "<<eps<<endl;

         //Renormierung innerhalb der iteration
         Z2= 1.0/(1.0+As[wh]);

         if(mass==0 || mass<1e-5){Zm=0.0;}
         else{Zm= mass/Z2-Bs[wh];}



    }

    cout<<"Calculated the real quark prop, HERE:.....Z2:"<<Z2<<"  Zm:"<<Zm<<"   mass:"<<mass<<endl;
    cout<<"---------ENDE:real quark prop------------"<<"Steps: "<<steps<<"  ,Stuetzstellen:  m:"<<m<<"  ,n :"<<n<<"  ,IR :"<<IRcut<<"  ,UV :"<<UVcut<<endl<<endl;


}

void quark_prop::spline_interp(quark_prop quark1)
{
    int a= quark1.m;
    int b= m;

    vecd q1(a), q2(b);
    vecd A1(a), B1(a);
    vecd A2(b), B2(b);


    for(int i=0; i<a; i++)
    {
        q1[i]= quark1.p2[i];
        A1[i]=quark1.A[i];
        B1[i]=quark1.B[i];
    }

    for(int i=0; i<b; i++)
    {
        q2[i]=p2[i];
    }

    Interpol splinetestA;
    splinetestA.spline_update_all(q1,A1);
    splinetestA.spline_interpol(q2,A2);

    Interpol splinetestB;
    splinetestB.spline_update_all(q1,B1);
    splinetestB.spline_interpol(q2,B2);

    for(int i=0;i<b;i++)
    {
        A[i]= A2[i];
        B[i]= B2[i];
    }

    Z2=quark1.Z2;
    Zm=quark1.Zm;

}




