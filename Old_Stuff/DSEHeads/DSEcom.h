/*
 * DSEcom.h
 *
 * calculation of the complex DSE for the 3 quarks in decay, k1, k2, k3
 *
 * ---------------------------------------------------------------------
 * 01.08.15 Esther Weil
 *
 *Content:calc of quark propagator in complex plane
 * ---------------------------------------------------------------------
 *
 *functions: einterC, inte_com, ComQuarkpm, ComQuark3
 *
 *
 *einterC: is the Maris-Tandy interaction for complex momenta
 *inte_com: is the integration of the quark dressing functions in the complex plane
 *ComQuarkpm: Calculates the complex quark at q_+, q_- : q_+(q,P,z)
 *ComQuark3: Calculatest the complex quark at k3 for the decay. Difference here is that k3(k,P,z,y)
 */



//The following headers need to be included infront of this: gaul.h , fourvec.h, omp.h, Eigen

//----------------------------------------------------------------------------------------------------------------//
//------------------------------COMPLEX: INTEGRATION------------------------------------------------------------//
void inte_com(complex <double> re [], quark_prop &quark, complex<double> p2, int PauliVillar)
{
    int m=quark.m;
    int n=quark.n;
//    complex<double> dummy=0.0;
    complex <double> resultB =0.0;
    complex <double> resultA =0.0;
    complex <double> q2=0.0;
    complex <double> B1=0.0;
    complex <double> A1=0.0;
    complex <double> pqz=0.0;
    complex <double> k2=0.0;
    complex <double> alpha=0.0;
    complex <double> wink1=0.0;
    complex <double> wink2=0.0;
    complex <double> asz=0.0;
    double vorfac=(1.0)/(8.0*Pi*Pi*Pi)*((4.0)/(3.0));

    mtmodel interaction;

    //Radiale Integration
    for(int k=0; k<m; k++)
    {
        wink1=0.0;
        wink2=0.0;
        q2=quark.p2[k];
        B1=quark.B[k];
        A1=quark.A[k];

         // Winkel Integration
        for (int l=0; l<n; l++)
        {
            asz=quark.ang[l];
            pqz= sqrt(p2*q2)*asz;
            k2= p2+q2-2.0*pqz;

            //Alpha berechnen
//            alpha = einterC(k2, PauliVillar);
            alpha = interaction.mtc(k2, PauliVillar,1);

            if(PauliVillar==0)
            {wink1 += ((sqrt(1.0-asz*asz))/k2)*quark.angw[l]*alpha;
            wink2 += (((sqrt(1.0-asz*asz))/(k2*k2))*(-2.0*q2+3.0*(1.0+(q2/p2))*pqz-4.0*q2*asz*asz))*quark.angw[l]*alpha;}
            else if (PauliVillar==1)
            {wink1 += ((sqrt(1.0-asz*asz))/(k2*(1.0+k2/lam2)))*quark.angw[l]*alpha;
            wink2 += (((sqrt(1.0-asz*asz))/(k2*k2*(1.0+k2/lam2)))*(-2.0*q2+3.0*(1.0+(q2/p2))*pqz-4.0*q2*asz*asz))*quark.angw[l]*alpha;}


//            dummy += (A1*vorfac)/(q2*A1*A1+B1*B1)*q2*q2*quark.radw[k]*(((sqrt(1.0-asz*asz))/(k2*k2))*(-2.0*q2+3.0*(1.0+(q2/p2))*pqz-4.0*q2*asz*asz))*quark.angw[l]*alpha;
//            if(k==0&&l==0 || k==m-1 && l==n-1 || k==92 && l==0){cout<<setprecision(20)<<dummy<<tab<<p2<<tab<<A1<<tab<<B1<<tab<<k2<<tab<<q2<<tab<<asz<<tab<<alpha; cin.get();}

        }


        resultB += (3.0*B1*vorfac)/(q2*A1*A1+B1*B1)*q2*q2*wink1*quark.radw[k];			//Radiale Integration
        resultA += (A1*vorfac)/(q2*A1*A1+B1*B1)*q2*q2*wink2*quark.radw[k];                  //p2*p2 wegen log-grid kommt eine zusätzlich p2 factor!


    }

//    cout<<"Test: "<<resultA<<tab<<dummy<<endl;


    re[0]=resultB;
    re[1]=resultA;


}

//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
//----------WriteOUT--k3-propagator----------------------------------------------------------------------------
void write_comprop_k3(MatrixXcd &Ac, MatrixXcd &Bc, grid &g, int steps, string name, double M, double sig, string rundef)
{

    int m_dec=g.m; int nz=g.nz; int ny=g.ny; int np=g.np;
    four_vec P,p,l1, k3,k4,k5;
    imag(P.inhalt[3])=M;

    complex<double> pc;

    string buffer;
    stringstream ss;
    ss << steps;
    buffer=rundef + "com_quark_k3_"+name+"_"+ss.str()+".txt";
    ofstream f(buffer.c_str());
    f<<"#real/imag p2   A		B   "<<endl;
    f<<"#nDec="<<m_dec<<"  , nzDec="<<nz<<"  ,nyDec="<<ny<<endl;
    for(int k_p=0; k_p<np; k_p++)
    {

        p=g.p_vector[k_p];
        k4=0.5*(p-P); k5=0.5*(p+P);
        for(int k=0; k<nz; k++)
        {
            for(int j=0; j<m_dec; j++)
            {


                for(int y_sum=0; y_sum<ny; y_sum++ )
                {
                    l1.inhalt[0]=0; l1.inhalt[1]=sqrt(g.q2[j])*sqrt(1.0-g.z[k]*g.z[k])*sqrt(1.0-g.y[y_sum]*g.y[y_sum]);
                    l1.inhalt[2]=sqrt(g.q2[j])*g.y[y_sum]*sqrt(1.0-g.z[k]*g.z[k]); l1.inhalt[3]=sqrt(g.q2[j])*g.z[k];

                    k3=l1+sig*P+k4;

                    pc=k3*k3;

                    f<<scientific<<real(pc)<<tab<<imag(pc)<<"\t"<<real(Ac(k_p,y_sum+ny*j+m_dec*ny*k))<<"\t"<<imag(Ac(k_p,y_sum+ny*j+m_dec*ny*k))<<"\t"<<real(Bc(k_p,y_sum+ny*j+m_dec*ny*k))<<"\t"<<imag(Bc(k_p,y_sum+ny*j+m_dec*ny*k))
                     <<tab<<real(k4*k4)<<tab<<imag(k4*k4)<<tab<<real(k5*k5)<<tab<<imag(k5*k5)
                     <<tab<<g.z[k]<<tab<<g.y[y_sum]<<tab<<real(g.q2[j])<<tab<<imag(g.q2[j])<<endl;

                }
            }
          }
    }

    f.close();

}

//------------------------------------------------------------------------------------------------------
//--------------------COMPLEX: quark propagator-------------------------------------------------------------
void ComQuarkpm(int m, int nz,quark_prop &quark, vecdcx &p2, vecd &zz,VectorXcd &A, VectorXcd &B, int tag, int writetag, string rundef,double M, double sig, int PauliVillar)
{
    complex<double> result[2];
    complex<double> pc;

    four_vec P,q, k_12;
    if(M<0){P.inhalt[3]=-M;}else{imag(P.inhalt[3])=M;}



    for(int k=0; k<nz; k++)
    {
        for(int j=0; j<m; j++)
        {
             q.inhalt[2]= sqrt(p2[j])*sqrt(1.0-zz[k]*zz[k]); q.inhalt[3]=sqrt(p2[j])*zz[k];
            if(tag==1)
            {k_12=q+sig*P; pc=k_12*k_12;}
            if(tag==2)
            {k_12=q-(1.0-sig)*P; pc=k_12*k_12;}

            inte_com(result,quark, pc, PauliVillar);

            B(j+m*k)= quark.Zm*quark.Z2+ quark.Z2*quark.Z2*result[0];
            A(j+m*k)= quark.Z2 + quark.Z2*quark.Z2*result[1];
//            if(force==1.0){A(j+m*k)=1.0;}

        }

    }

    //cout<<"Calculated the complex prop!	"<<tag<<endl;

    if(writetag==1)
    {
        //Writing out Results
         stringstream ss; ss<<tag; string buffer;

        buffer = rundef+"complex_quark_pm_"+ss.str()+".txt";
        ofstream f(buffer.c_str());
        f<<"#real/imag p2   A		B   "<<endl;
        for(int k=0; k<nz; k++)
        {
            for(int j=0; j<m; j++ )
            {
                q.inhalt[2]= sqrt(p2[j])*sqrt(1.0-zz[k]*zz[k]); q.inhalt[3]=sqrt(p2[j])*zz[k];
                if(tag==1)
                {k_12=q+sig*P; pc=k_12*k_12;}
                if(tag==2)
                {k_12=q-(1.0-sig)*P; pc=k_12*k_12;}
                f<<scientific<<real(pc)<<"\t"<<imag(pc)<<"\t"<<real(A(j+m*k))<<"\t"<<imag(A(j+m*k))<<"\t"<<real(B(j+m*k))<<"\t"<<imag(B(j+m*k))<<endl;
            }
        }
        f.close();

    }else{return;}



}
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
//----------------Complex: quark propagator für k3------------------------------------------------------
void ComQuark3_grid(grid &g,quark_prop &quark, MatrixXcd &Ac, MatrixXcd &Bc, double sig, int writetag, int steps, double M, int PauliVillar, string name, string rundef)
{
    //vecd pslpine;
    int m_dec=g.m; int nz=g.nz; int ny=g.ny; int np=g.np; int index;
    int counter1=0.0; int counter2=0.0;
    double mq=1.0; cout<<"Test: "<<np<<tab<<nz<<tab<<m_dec<<tab<<ny<<endl;

    #pragma omp parallel for
    for(int k_p=0; k_p<np; k_p++)
    {
        complex<double> result[2];
        complex<double> pc;

        four_vec P,p,l1,k3,k4,k5;
        if(M<0){P.inhalt[3]=-M;}else{imag(P.inhalt[3])=M;}

        p=g.p_vector[k_p];

        k4=0.5*(p-P);
        k5=0.5*(p+P);


                 //Inner Integration variables
                for(int k=0; k<nz; k++)
                {

                    for(int j=0; j<m_dec; j++)
                    {
                        for(int y_sum=0; y_sum<ny; y_sum++)
                        {
                            l1.inhalt[0]=0; l1.inhalt[1]=sqrt(g.q2[j])*sqrt(1.0-g.z[k]*g.z[k])*sqrt(1.0-g.y[y_sum]*g.y[y_sum]);
                            l1.inhalt[2]=sqrt(g.q2[j])*g.y[y_sum]*sqrt(1.0-g.z[k]*g.z[k]); l1.inhalt[3]=sqrt(g.q2[j])*g.z[k];

                            k3=l1+sig*P+k4;

                            pc=k3*k3;

                            if( real(pc)< pow((imag(pc)/mq),2.0)-mq*mq/4.0)
                            {
                                counter1++;
                                cout<<"We hit a piont that is outside the parabola at : y="<<g.y[y_sum]<<": q2="<<g.q2[j]<<": z="<<g.z[k]<<" ::p2="<<p*p<<" ::k3="<<pc<<"  ::k4²="<<k4*k4<<"  ::k5²="<<k5*k5<<endl;
                                cin.get();

//                                Bc(k_p,y_sum+ny*j+m_dec*ny*k)= 0.0;
//                                Ac(k_p,y_sum+ny*j+m_dec*ny*k)=0.0;

//                                k3.write();
                            }else{


//                            if( real(pc)<(-M*M/4.0) || (imag(pc) > sqrt(real(pc)+M*M/4.0)*M || imag(pc) < -sqrt(real(pc)+M*M/4.0)*M))
//                            if(real(pc)<(-M*M/4.0) || real(pc)> sqrt(pow(quark.UVcut,2)+M*M/4.0) || real(pc)<pow(imag(pc)/M,2)-M*M/4.0 )
//                            {
//                                cout<<"  ATTENTION: OUT of the Parabola   : y="<<g.y[y_sum]<<": q2="<<g.q2[j]<<": z="<<g.z[k]<<" ::p2="<<p*p<<" ::k3="<<pc<<endl;
//                            }

                                counter2++;
                                inte_com(result, quark, pc, PauliVillar);

                                Bc(k_p, y_sum + ny * j + m_dec * ny * k) =
                                        quark.Zm * quark.Z2 + quark.Z2 * quark.Z2 * result[0];
                                Ac(k_p, y_sum + ny * j + m_dec * ny * k) = quark.Z2 + quark.Z2 * quark.Z2 * result[1];


                            }


                        }
                    }
                }

        cout<<".";
    }

    cout<<endl<<"Calculated the complex prop!	3"<<endl; cout<<"Counter compa: out of= "<<counter1<<"   , in Parabola="<<counter2<<endl;





if(writetag==1)
{

    write_comprop_k3(Ac,Bc,g,steps,name,M,sig,rundef);
//    write_comprop_k3(Ac,Bc,g,steps,name,M,sig,rundef);

}


}


//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
void read_inn_com(VectorXcd &A, VectorXcd &B, string location)
{
    int ng=A.size();


    //Reading inn complex quark propagator from file!
            double Rp2, Ip2, Ar, Ac, Br, Bc;
            vecd dummyA, dummyB, dummyC, dummyD, dummyE, dummyAA;

            ifstream infile;
            infile.open(location.c_str(), ios::in);           //DSEreal
            if(infile.is_open())
            {
                string daten;
                while(getline(infile,daten))
                {
                    //the following line trims white space from the beginning of the string
                    daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

                    if(daten[0] == '#') {continue;}
                    if(daten=="") {continue;}
                    stringstream(daten) >> Rp2 >> Ip2 >> Ar >> Ac >> Br >> Bc;
                    if(daten=="") {continue;}

                     dummyA.push_back(Rp2);
                     dummyAA.push_back(Ip2);
                     dummyB.push_back(Ar);
                     dummyC.push_back(Ac);
                     dummyD.push_back(Br);
                     dummyE.push_back(Bc);
                }
            }else{cout<<"No data file with com quark found!"<<endl;}

            infile.close();
            int mv=dummyA.size();
            //cout<<"HERE:        "<<mv<<endl;

            if(mv==ng)
            {
                for(int i=0; i<mv; i++)
                {
                    real(A(i))=dummyB[i];
                    imag(A(i))=dummyC[i];
                    real(B(i))=dummyD[i];
                    imag(B(i))=dummyE[i];
                }


                cout<<"ReadInn: Complex propagator @"<<location<<endl;
            }
            else{cout<<"The complex quark has not the same dimensions that the data you are trying to readINN!!! ABORTED"<<endl; return;}


}
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
void read_inn_com(VectorXcd &A, VectorXcd &B, string rundef, int tag)
{
    int ng=A.size();
   stringstream ss; ss<<tag; string buffer;

   buffer = rundef+"complex_quark_pm_"+ss.str()+".txt";

    //Reading inn real quark propagator from file!
            double Rp2, Ip2, Ar, Ac, Br, Bc;
            vecd dummyA, dummyB, dummyC, dummyD, dummyE, dummyAA;

            ifstream infile;
            infile.open(buffer.c_str(), ios::in);           //DSEreal
            if(infile.is_open())
            {
                string daten;
                while(getline(infile,daten))
                {
                    //the following line trims white space from the beginning of the string
                    daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

                    if(daten[0] == '#') {continue;}
                    if(daten=="") {continue;}
                    stringstream(daten) >> Rp2 >> Ip2 >> Ar >> Ac >> Br >> Bc;
                    if(daten=="") {continue;}

                     dummyA.push_back(Rp2);
                     dummyAA.push_back(Ip2);
                     dummyB.push_back(Ar);
                     dummyC.push_back(Ac);
                     dummyD.push_back(Br);
                     dummyE.push_back(Bc);
                }
            }else{cout<<"No data file with com quark found!"<<endl;}

            infile.close();
            int mv=dummyA.size();
            //cout<<"HERE:        "<<mv<<endl;

            if(mv==ng)
            {
                for(int i=0; i<mv; i++)
                {
                    real(A(i))=dummyB[i];
                    imag(A(i))=dummyC[i];
                    real(B(i))=dummyD[i];
                    imag(B(i))=dummyE[i];
                }


                cout<<"ReadInn: Complex propagator "<<tag<<"!"<<endl;
            }
            else{cout<<"The complex quark has not the same dimensions that the data you are trying to readINN!!! ABORTED"<<endl; return;}


}
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
void read_inn_com_3(MatrixXcd &A, MatrixXcd &B, string location)
{
    int nc=A.cols(); int nr=A.rows();


    //Reading inn complex quark propagator from file!
    double Rp2, Ip2, Ar, Ac, Br, Bc, dum;
    vecd dummyA, dummyB, dummyC, dummyD, dummyE, dummyAA;

    ifstream infile;
    infile.open(location.c_str(), ios::in);           //DSEreal
    if(infile.is_open())
    {
        string daten;
        while(getline(infile,daten))
        {
            //the following line trims white space from the beginning of the string
            daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

            if(daten[0] == '#') {continue;}
            if(daten=="") {continue;}
            stringstream(daten) >> Rp2 >> Ip2 >> Ar >> Ac >> Br >> Bc ; //>> dum >> dum >> dum >> dum >> dum >> dum>> dum >> dum;
            if(daten=="") {continue;}

            dummyA.push_back(Rp2);
            dummyAA.push_back(Ip2);
            dummyB.push_back(Ar);
            dummyC.push_back(Ac);
            dummyD.push_back(Br);
            dummyE.push_back(Bc);
        }
    }else{cout<<"No data file with com quark found!"<<endl;}

    infile.close();
    int mv=dummyA.size();
    cout<<"HERE: Sizes "<<mv<<tab<<nr<<tab<<nc<<endl;

    if(mv==nr*nc)
    {
        for(int j=0; j<nr;j++)
        {
            for(int i=0; i<nc; i++)
            {
                real(A(j,i))=dummyB[i+nc*j];
                imag(A(j,i))=dummyC[i+nc*j];
                real(B(j,i))=dummyD[i+nc*j];
                imag(B(j,i))=dummyE[i+nc*j];
            }

        }


        cout<<"ReadInn: Complex propagator, @"<<location<<endl;
    }
    else{cout<<"The complex quark has not the same dimensions that the data you are trying to readINN!!! ABORTED"<<endl; return;}


}
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------


//----------SPLINE: Spline interpolation to get the real quark prop at different posotions------------------------------------------------------------------------------

void DSEspline(quark_prop &quark1, quark_prop &quark2)
{

    int a= quark1.m;
    int b= quark2.m;

    vecd q1(a), q2(b);
    vecd A1(a), B1(a);
    vecd A2(b), B2(b);


    for(int i=0; i<a; i++)
    {
        q1[i]= quark1.p2[i];
        A1[i]=quark1.A[i];
        B1[i]=quark1.B[i];
    }

    for(int i=0; i<b; i++)
    {
        q2[i]=quark2.p2[i];
    }

    Interpol splinetestA;
    splinetestA.spline_update_all(q1,A1);
    splinetestA.spline_interpol(q2,A2);

    Interpol splinetestB;
    splinetestB.spline_update_all(q1,B1);
    splinetestB.spline_interpol(q2,B2);

    for(int i=0;i<b;i++)
    {
        quark2.A[i]= A2[i];
        quark2.B[i]= B2[i];
    }

    quark2.Z2=quark1.Z2;
    quark2.Zm=quark1.Zm;



}


//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
//----------WriteOUT------------------------------------------------------------------------------
void write_prop(vecdcx p2, VectorXcd A, VectorXcd B, int n, int m)
{
    string buffer = "data/test_comquark.txt";
    ofstream f(buffer.c_str());
    f<<"#p2  A   B  "<<endl;
    for(int i=0; i<m; i++)
    {
        for(int j=0; j<n; j++)
        {
            f<<real(p2[j])<<"\t"<<imag(p2[j])<<"\t"<<A(j+n*i).real()<<"\t"<<A(j+n*i).imag()<<"\t"<<B(j+n*i).real()<<"\t"<<B(j+n*i).imag()<<endl;
        }
    }
    f.close();

}

//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------

