#ifndef BSECLASS_H
#define BSECLASS_H

#endif // BSECLASS_H


//--------------------------------------------------------------------------------------------------------------------------------------
//BSE class

class bse_amplitude
{
    public:
    int m;
    int ny;
    int nz;
    int nCheb;
    int Ampnumber;
    double Mbse; double fpi;
    double eigenv;
    double etabse;
    vecvecdx Amp;
    vecdcx p2;
    vecd radw;
    vecd zqp;
    vecd w_z;
    vecd zy;
    vecd w_y;

    public:
        bse_amplitude(double M, int mB, int nyB, int nzB, double e, double f)
        {
            m=mB; ny=nyB; nz=nzB; Ampnumber=f; eigenv=1.0; Mbse=M; etabse=e; nCheb=0; fpi=0.092;

            for(int j=0; j<Ampnumber; j++)
            {
                vecdcx row;
                for(int i=0; i<m*nz; i++)
                {
                    row.push_back(1.0);
                }
                Amp.push_back(row);
            }


            for(int i=0; i<m; i++)
            {
                p2.push_back(1.0); radw.push_back(1.0);
            }
            for(int i=0; i<nz; i++)
            {
                zqp.push_back(1.0); w_z.push_back(1.0);
            }
            for(int i=0; i<ny; i++)
            {
                zy.push_back(1.0); w_y.push_back(1.0);
            }

        }

        bse_amplitude(double M, int mB, int nyB, int nzB, double e, double f, double g)
        {
            m=mB; ny=nyB; nz=nzB; Ampnumber=f; eigenv=1.0; Mbse=M; etabse=e;  nCheb=g; fpi=0.092;
            cout<<"BSEclass::constructor: BSA is using Chebydependece"<<endl;

            for(int j=0; j<Ampnumber; j++)
            {
                vecdcx row;
                for(int i=0; i<m*nCheb; i++)
                {
                    row.push_back(1.0);
                }
                Amp.push_back(row);
            }


            for(int i=0; i<m; i++)
            {
                p2.push_back(1.0); radw.push_back(1.0);
            }
            for(int i=0; i<nz; i++)
            {
                zqp.push_back(1.0); w_z.push_back(1.0);
            }
            for(int i=0; i<ny; i++)
            {
                zy.push_back(1.0); w_y.push_back(1.0);
            }

        }



        bool gridinit(grid g, int grid_distri);
        bool gridinit(grid g);
        void write(int steps, int tag, string rundef);
        void readInn(int steps, int tag, string rundef);


};

bool bse_amplitude::gridinit(grid g)
{
    bool outcome=true;
    if(nz!=g.nz || m!=g.m ){cout<<"DSEclass.h bse_amplitude GRIDINIT:: The grid doesn't fir the bse amp (nz,m). Aborted"<<endl; outcome=false; return outcome;}

    if(nCheb!=0)
    {
        int nstep=4;
        int ncos=(nCheb+1)*nstep;
        zqp.resize(ncos); w_z.resize(ncos);

        cout<<"The grid get's distributed around the Cheby zeros: n="<<ncos<<endl;

        double zeros_Cheb[nCheb+2];
        if(g.g1==-1.0 && g.g2==1.0)
        {
            zeros_Cheb[0]=-1.0; zeros_Cheb[nCheb+1]=1.0;
        }
        if(g.g1==0.0 && g.g2==Pi)
        {
            zeros_Cheb[0]=0.0; zeros_Cheb[nCheb+1]=Pi;
        }

        for (int j = 0; j <nCheb ; ++j) {
            zeros_Cheb[j+1]=-cos((j-0.5+1.0)*Pi/nCheb);
//            cout<<zeros_Cheb[j]<<endl;
        }
//        cout<<zeros_Cheb[nCheb]<<endl<<zeros_Cheb[nCheb+1]<<endl<<endl;

        double xz[nstep]; double xzw[nstep];
        for (int k = 0; k < nCheb+1; ++k) {

            gauleg(zeros_Cheb[k],zeros_Cheb[k+1],xz,xzw,nstep);
            for (int i = 0; i <nstep; ++i) {

                if(g.g1==-1 && g.g2==1)
                {
                    zqp[i+nstep*k]=xz[i];
                    w_z[i+nstep*k]=xzw[i];
                }
                if(g.g1==0 && g.g2==Pi)
                {
                    zqp[i+nstep*k]=cos(xz[i]);
                    w_z[i+nstep*k]=xzw[i];
                }

//                cout<<zeros_Cheb[k]<<tab<<zeros_Cheb[k+1]<<tab<<zqp[i+nstep*k]<<tab<<w_z[i+nstep*k]<<endl;
            }
        }

        nz=ncos;


    } else{

        for(int i=0; i<nz; i++)
        {
            zqp[i]=g.z[i];
            w_z[i]=g.z_weight[i];
        }

    }


    if(ny==g.ny)
    {
        for(int i=0; i<ny; i++)
        {
            zy[i]=g.y[i];
            w_y[i]=g.y_weight[i];
        }

    }else
    {
        double xay[ny], way[ny];
        gauleg(g.g1, g.g2, xay,way,ny);
        for(int i=0; i<ny; i++)
        {
            zy[i]=xay[i];
            w_y[i]=way[i];
        }
        cout<<"DSEclass.h GRIDINIT:: Note: ny grid != ny BSE"<<endl;

    }


    for (int i=0; i<m; i++)
    {
        p2[i]=g.q2[i];
        radw[i]=g.q2_weight[i];
    }

    return outcome;
}
bool bse_amplitude::gridinit(grid g, int grid_distri)
{
    bool outcome=true;
    if(nz!=g.nz || m!=g.m ){cout<<"DSEclass.h bse_amplitude GRIDINIT:: The grid doesn't fir the bse amp (nz,m). Aborted"<<endl; outcome=false; return outcome;}

    if(nCheb!=0 && grid_distri==1)
    {
        int nstep=4;
        int ncos=(nCheb+1)*nstep;
        zqp.resize(ncos); w_z.resize(ncos);

        cout<<"The grid get's distributed around the Cheby zeros: n="<<ncos<<endl;

        double zeros_Cheb[nCheb+2];
        if(g.g1==-1.0 && g.g2==1.0)
        {
            zeros_Cheb[0]=-1.0; zeros_Cheb[nCheb+1]=1.0;
        }
        if(g.g1==0.0 && g.g2==Pi)
        {
            zeros_Cheb[0]=0.0; zeros_Cheb[nCheb+1]=Pi;
        }

        for (int j = 0; j <nCheb ; ++j) {
            zeros_Cheb[j+1]=-cos((j-0.5+1.0)*Pi/nCheb);
//            cout<<zeros_Cheb[j]<<endl;
        }
//        cout<<zeros_Cheb[nCheb]<<endl<<zeros_Cheb[nCheb+1]<<endl<<endl;

        double xz[nstep]; double xzw[nstep];
        for (int k = 0; k < nCheb+1; ++k) {

            gauleg(zeros_Cheb[k],zeros_Cheb[k+1],xz,xzw,nstep);
            for (int i = 0; i <nstep; ++i) {

                if(g.g1==-1 && g.g2==1)
                {
                    zqp[i+nstep*k]=xz[i];
                    w_z[i+nstep*k]=xzw[i];
                }
                if(g.g1==0 && g.g2==Pi)
                {
                    zqp[i+nstep*k]=cos(xz[i]);
                    w_z[i+nstep*k]=xzw[i];
                }

//                cout<<zeros_Cheb[k]<<tab<<zeros_Cheb[k+1]<<tab<<zqp[i+nstep*k]<<tab<<w_z[i+nstep*k]<<endl;
            }
        }

        nz=ncos;


    } else{

        for(int i=0; i<nz; i++)
        {
            zqp[i]=g.z[i];
            w_z[i]=g.z_weight[i];
        }

    }


    if(ny==g.ny)
    {
        for(int i=0; i<ny; i++)
        {
            zy[i]=g.y[i];
            w_y[i]=g.y_weight[i];
        }

    }else
    {
        double xay[ny], way[ny];
        gauleg(g.g1, g.g2, xay,way,ny);
        for(int i=0; i<ny; i++)
        {
            zy[i]=xay[i];
            w_y[i]=way[i];
        }
        cout<<"DSEclass.h GRIDINIT:: Note: ny grid != ny BSE"<<endl;

    }


    for (int i=0; i<m; i++)
    {
        p2[i]=g.q2[i];
        radw[i]=g.q2_weight[i];
    }

    return outcome;
}

void bse_amplitude::write(int steps, int tag, string rundef)
{
    stringstream help;
    help<<steps;
    string buffer = rundef+"bse_amplitude_"+help.str()+".txt";
    ofstream f(buffer.c_str());
    f<<"#   m="<<m<<" nz:"<<nz<<" ny_decay:"<<ny<<"  pionM:"<<Mbse<<" Amplitudes:"<<Ampnumber<<endl;
    f<<"#--------------------------------------------------# "<<endl;
    f<<"#   p2  Re(E) Re(F) Re(G) Re(H)  imag(E) .. "<<endl;

    if(tag==1)
    {
        if(Ampnumber==1)
        {
            for(int i=0; i<nz; i++)
                {
                    for(int j=0; j<m; j++)
                    {
                        f<<real(p2[j])<<"\t"<<abs(Amp[0][j+m*i])<<endl;
                    }
                }
            f<<"# Mass  Eigenvalue  fpi"<<endl; f<<Mbse<<tab<<eigenv<<tab
                                                 <<fpi<<endl;

        }

        if(Ampnumber==2)
        {
            for(int i=0; i<nz; i++)
                {
                    for(int j=0; j<m; j++)
                    {
                        f<<real(p2[j])<<"\t"<<abs(Amp[0][j+m*i])<<"\t"<<abs(Amp[1][j+m*i])<<endl;
                    }
                }
            f<<"# Mass  Eigenvalue  fpi"<<endl; f<<Mbse<<tab<<eigenv<<tab
                                                 <<fpi<<endl;

        }

        if(Ampnumber==3)
        {
            for(int i=0; i<nz; i++)
                {
                    for(int j=0; j<m; j++)
                    {
                        f<<real(p2[j])<<"\t"<<abs(Amp[0][j+m*i])<<"\t"<<abs(Amp[1][j+m*i])<<"\t"<<abs(Amp[2][j+m*i])<<endl;
                    }
                }
            f<<"# Mass  Eigenvalue  fpi"<<endl; f<<Mbse<<tab<<eigenv<<tab
                                                 <<fpi<<endl;

        }

        if(Ampnumber==4)
        {
            for(int i=0; i<nz; i++)
                {
                    for(int j=0; j<m; j++)
                    {
                        f<<real(p2[j])<<"\t"<<abs(Amp[0][j+m*i])<<"\t"<<abs(Amp[1][j+m*i])<<"\t"<<abs(Amp[2][j+m*i])<<"\t"<<abs(Amp[3][j+m*i])<<endl;
                    }
                }
            f<<"# Mass  Eigenvalue  fpi"<<endl; f<<Mbse<<tab<<eigenv<<tab
                                                 <<fpi<<endl;

        }

    }


    if(tag==2)
    {

        if(Ampnumber==1)
        {
            for(int i=0; i<nz; i++)
            {
                for(int j=0; j<m; j++)
                {
                    f<<real(p2[j])<<tab<<zqp[i]<<"\t"<<real(Amp[0][j+m*i])<<"\t"<<imag(Amp[0][j+m*i])<<endl;
                }
            }
            f<<"# Mass  Eigenvalue  fpi"<<endl; f<<Mbse<<tab<<eigenv<<tab
                                                 <<fpi<<endl;
        }

        if(Ampnumber==2)
        {
            for(int i=0; i<nz; i++)
            {
                for(int j=0; j<m; j++)
                {
                    f<<real(p2[j])<<"\t"<<real(Amp[0][j+m*i])<<"\t"<<real(Amp[1][j+m*i])<<"\t"<<imag(Amp[0][j+m*i])<<"\t"<<imag(Amp[1][j+m*i])<<endl;
                }
            }
            f<<"# Mass  Eigenvalue  fpi"<<endl; f<<Mbse<<tab<<eigenv<<tab
                                                 <<fpi<<endl;
        }

        if(Ampnumber==3)
        {
            for(int i=0; i<nz; i++)
            {
                for(int j=0; j<m; j++)
                {
                    f<<real(p2[j])<<"\t"<<real(Amp[0][j+m*i])<<"\t"<<real(Amp[1][j+m*i])<<"\t"<<real(Amp[2][j+m*i])
                            <<"\t"<<imag(Amp[0][j+m*i])<<"\t"<<imag(Amp[1][j+m*i])<<"\t"<<imag(Amp[2][j+m*i])<<endl;
                }
            }
            f<<"# Mass  Eigenvalue  fpi"<<endl; f<<Mbse<<tab<<eigenv<<tab
                                                 <<fpi<<endl;
        }

        if(Ampnumber==4)
        {
            for(int i=0; i<nz; i++)
            {
                for(int j=0; j<m; j++)
                {
                    f<<scientific<<real(p2[j])<<"\t"<<real(Amp[0][j+m*i])<<"\t"<<real(Amp[1][j+m*i])<<"\t"<<real(Amp[2][j+m*i])<<"\t"<<real(Amp[3][j+m*i])
                            <<"\t"<<imag(Amp[0][j+m*i])<<"\t"<<imag(Amp[1][j+m*i])<<"\t"<<imag(Amp[2][j+m*i])<<"\t"<<imag(Amp[3][j+m*i])<<"\t"<<zqp[i]<<endl;
                }
            }
            f<<"# Mass  Eigenvalue  fpi"<<endl; f<<Mbse<<tab<<eigenv<<tab
                                                 <<fpi<<endl;
        }

     }

    if(tag==3)
    {

        if(Ampnumber==1)
        {
            for(int i=0; i<nCheb; i++)
            {
                for(int j=0; j<m; j++)
                {
                    f<<real(p2[j])<<"\t"<<real(Amp[0][j+m*i])<<"\t"<<imag(Amp[0][j+m*i])<<endl;
                }
                f<<endl;
            }
            f<<"# Mass  Eigenvalue  fpi"<<endl; f<<Mbse<<tab<<eigenv<<tab
                                                 <<fpi<<endl;
        }

        if(Ampnumber==2)
        {
            for(int i=0; i<nCheb; i++)
            {
                for(int j=0; j<m; j++)
                {
                    f<<real(p2[j])<<"\t"<<real(Amp[0][j+m*i])<<"\t"<<real(Amp[1][j+m*i])<<"\t"<<imag(Amp[0][j+m*i])<<"\t"<<imag(Amp[1][j+m*i])<<endl;
                }
            }
            f<<"# Mass  Eigenvalue  fpi"<<endl; f<<Mbse<<tab<<eigenv<<tab
                                                 <<fpi<<endl;
        }

        if(Ampnumber==3)
        {
            for(int i=0; i<nCheb; i++)
            {
                for(int j=0; j<m; j++)
                {
                    f<<real(p2[j])<<"\t"<<real(Amp[0][j+m*i])<<"\t"<<real(Amp[1][j+m*i])<<"\t"<<real(Amp[2][j+m*i])
                            <<"\t"<<imag(Amp[0][j+m*i])<<"\t"<<imag(Amp[1][j+m*i])<<"\t"<<imag(Amp[2][j+m*i])<<endl;
                }
            }
            f<<"# Mass  Eigenvalue  fpi"<<endl; f<<Mbse<<tab<<eigenv<<tab
                                                 <<fpi<<endl;
        }

        if(Ampnumber==4)
        {
            for(int i=0; i<nCheb; i++)
            {
                for(int j=0; j<m; j++)
                {
                    f<<scientific<<real(p2[j])<<"\t"<<real(Amp[0][j+m*i])<<"\t"<<real(Amp[1][j+m*i])<<"\t"<<real(Amp[2][j+m*i])<<"\t"<<real(Amp[3][j+m*i])
                            <<"\t"<<imag(Amp[0][j+m*i])<<"\t"<<imag(Amp[1][j+m*i])<<"\t"<<imag(Amp[2][j+m*i])<<"\t"<<imag(Amp[3][j+m*i])<<"\t"<<zqp[i]<<endl;
                }
            }
            f<<"# Mass  Eigenvalue  fpi"<<endl; f<<Mbse<<tab<<eigenv<<tab
            <<fpi<<endl;
        }

     }
    f.close();

}

void bse_amplitude::readInn(int steps, int tag, string rundef)
{
   if((tag==2 && Ampnumber==4) || (tag==3 && Ampnumber==4))
   {
       stringstream help;
       help<<steps;
       string buffer = rundef+"bse_amplitude_"+help.str()+".txt";

       //Reading inn real quark propagator from file!
               double p2p, Er, Ec, Fr, Fc, Gr, Gc, Hr, Hc;
               vecd dummyA, dummyB, dummyC, dummyD, dummyE, dummyF, dummyG, dummyH, dummyI;

               ifstream infile;
               infile.open(buffer.c_str(), ios::in);           //DSEreal
               if(infile.is_open())
               {
                   string daten;
                   while(getline(infile,daten))
                   {
                       //the following line trims white space from the beginning of the string
                       daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

                       if(daten[0] == '#') {continue;}
                       stringstream(daten) >> p2p >> Er >> Fr >> Gr >> Hr >> Ec >> Fc >> Gc >> Hc;
                       if(daten=="") {continue;}
                       if(daten[0] == '#') {continue;}

                        dummyA.push_back(p2p);
                        dummyB.push_back(Er);
                        dummyC.push_back(Ec);
                        dummyD.push_back(Fr);
                        dummyE.push_back(Fc);
                        dummyF.push_back(Gr);
                        dummyG.push_back(Gc);
                        dummyH.push_back(Hr);
                        dummyI.push_back(Hc);

                   }
               }else{cout<<"DSEclass bse_ampitude READINN ::No data file with bse amp found!"<<endl;}

               infile.close();
               int mv=dummyA.size()-1.0;
               //cout<<"HERE:        "<<mv<<endl;

               if(mv==m*nz || mv==m*nCheb)
               {
                   for(int i=0; i<m; i++)
                   {
                       p2[i]=dummyA[i];

                   }

                   for(int i=0; i<mv; i++)
                   {
                       real(Amp[0][i])=dummyB[i];
                       real(Amp[1][i])=dummyD[i];
                       real(Amp[2][i])=dummyF[i];
                       real(Amp[3][i])=dummyH[i];
                       imag(Amp[0][i])=dummyC[i];
                       imag(Amp[1][i])=dummyE[i];
                       imag(Amp[2][i])=dummyG[i];
                       imag(Amp[3][i])=dummyI[i];

                   }

                   Mbse=dummyA[mv]; eigenv=dummyB[mv]; fpi=dummyD[mv]; if(fpi==0){fpi=0.092;}


                   cout<<"BSEclass bse_ampitude READINN :: BSE Amps inn! With Mass: M "<<Mbse<<" , EV "<<eigenv<<" , fpi="<<fpi<<" @"<<buffer<<endl;

               }
               else{cout<<"BSEclass bse_ampitude READINN ::The bse amp has not the same dimensions that the data you are trying to readINN!!! ABORTED"<<endl; return;}

   }else if(tag==2){cout<<"Bseclass bse_ampitude READINN ::Not defined jet, need to put it in. Syr"<<endl; return;}


   if(Ampnumber !=4){cout<<"Bseclass bse_ampitude READINN ::ERROR! Wrong dimensions in readInn"<<endl; return;}



}


