/*
 * BSEKernel.h
 *
 * calculation of the Bethe-Salpeter equation for the pion
 *
 * ---------------------------------------------------------------------
 * 04.16 Esther Weil
 *
 *Content:Kernel for the pion BSE, Power iteration, Calculation of Kernel with and without Chebys.
 * ---------------------------------------------------------------------
 *
 *functions: m_eigenvalue,
 *
 *
 * m_eigenvalue: calc the eigenvalue of any abitray matrix using power iteration
 * normy: has the normalization denominator for Y and L
 * BSAgammaL: Lij
 * BSAgammaY: Yik
 **/

#ifndef BSEKERNEL
#define BSEKERNEL

#endif // BSEKERNEL


// getting the biggest eigenvalue of a Matrix trough Poweriteration (for only one amplitude)
//---------------------------------------------------------------------------------------------------------------------//
double m_eigenvalue(MatrixXcd &Me, vecdcx &Gamma0, double M)
{
    int ng=Me.rows(); //cout<<"Let's see... ng="<<ng<<" Let's see G   "<<Gamma0.size()<<endl;
    complex<double> G0[ng];
    complex<double> KG[ng];
    complex<double> eigenv=0.0;
    complex<double> eigenv0=1.0;
    complex<double> norm=1.0;
    complex<double> normG=1.0;

    double eps2=1.0;
    double h2=0;
    int steps=0;

    while (eps2>=1e-10)
   {
       complex<double> GG0=0.0;
       complex<double> sh=0.0;
       eps2=0.0;
       complex<double> dummy2=0.0;

       for (int l=0; l<ng; l++)
       {
           G0[l]=Gamma0[l];
       }
       eigenv0=eigenv;

      for(int k=0; k<ng; k++)
      {
          complex<double> kgsum=0.0;
          for(int l=0; l<ng; l++)
          {
              kgsum+= conj(Me(k,l))*G0[l];

          }
          KG[k]=kgsum;
      }


       //Jede beliebige Norm mögich hier: vec norm, bestimmter eintrag von gamma, ..
       norm=0.0;
       //for(int i=0; i<ng; i++)
       //{
        //norm += conj(KG[i])*KG[i];
        //norm += KG[i]*KG[i];    //Norm von neuem Gamma
       //}
      //normG= sqrt(norm);
      normG=KG[0];

      //cout<<"BSA solved for: "<<steps<<" iteration! Norm: "<<normG<<endl;

      for(int i =0; i<ng; i++)
      {
          Gamma0[i]=KG[i]/(normG);
          GG0 += conj(G0[i])*G0[i];
          sh += conj(G0[i])*KG[i];
      }

      eigenv=sh/GG0;
      h2 = abs((eigenv0-eigenv)/(eigenv0+eigenv));
      eps2=h2;

      //cout<<"Norm["<<steps<<"]: "<<normG<<" , EV: "<<eigenv<<endl;
      //cout<<"--------------------->"<<"Eps2: "<<eps2<<endl;
      steps++;

   }

    cout<<"Norm["<<steps<<"]: EV: "<<eigenv<<"......  with M="<<M<<endl;
    //cout<<"Stutzstellen:	"<<"nz: "<<nzBSA<<", m:"<<mBSA<<", nc:"<<nnBSA<<", ny:"<<nyBSA<<endl;

    return real(eigenv);


}


/*---------------------------------BSA: Calculating the Gamma structures Yij Lij----(Klammer mir Quark props)-----------------------------------*/

//This is form code stuff--------------------------------------------------------------begin
//Norm-factoren
complex<double> normy(int a, complex<double> P_P, complex<double> k_k, complex<double> P_k)
{
    complex<double> norma;
    complex<double> T1[4];

        T1[0]=1.0/(4.0);
        T1[1]=1.0/(4.0*(P_P*k_k-P_k*P_k));
        T1[2]=1.0/(4.0*(P_k*P_k*P_k-k_k*P_P*P_k));
        T1[3]=1.0/(16.0*(P_k*P_k-P_P*k_k));



    norma=T1[a];

    return norma;

}

//---------------------------------------------------------------------------------------------------------------------//
complex<double> BSAgammaY(int a, int b, complex<double> A1, complex<double> A2, complex<double> B1, complex<double> B2,
                          complex<double> kp_km, complex<double> P_P, complex<double> P_kp,complex<double> P_km, complex<double> P_k, complex<double> k_kp, complex<double> k_km, complex<double> k_k )
{
    complex<double> Y;
    complex<double> Yr;

        if(a==0 && b==0){ Y = (4.E+0*B1*B2 + 4.E+0*kp_km*A1*A2);}

        if(a==0 && b==1){ Y = (4.E+0*P_kp*A1*B2 - 4.E+0*P_km*A2*B1);}

        if(a==0 && b==2){ Y = (4.E+0*P_k*k_kp*A1*B2 - 4.E+0*P_k*k_km*A2*B1);}

        if(a==0 && b==3){ Y = ( - 8.E+0*P_kp*k_km*A1*A2 + 8.E+0*P_km*k_kp*
         A1*A2);}

        if(a==1 && b==0){ Y = (4.E+0*P_k*k_kp*A1*B2 - 4.E+0*P_k*k_km*A2*B1
          - 4.E+0*P_kp*k_k*A1*B2 + 4.E+0*P_km*k_k*A2*B1);}

        if(a==1 && b==1){ Y = ( - 4.E+0*P_P*k_k*kp_km*A1*A2 + 4.E+0*P_P*
         k_k*B1*B2 - 4.E+0*P_k*P_kp*k_km*A1*A2 - 4.E+0*P_k*P_km*k_kp*A1*A2
          + 4.E+0*pow(P_k,2)*kp_km*A1*A2 - 4.E+0*pow(P_k,2)*B1*B2 + 8.E+0*
         P_kp*P_km*k_k*A1*A2);}

        if(a==1 && b==2){ Y = (4.E+0*P_k*P_kp*k_k*k_km*A1*A2 + 4.E+0*P_k*
         P_km*k_k*k_kp*A1*A2 - 8.E+0*pow(P_k,2)*k_kp*k_km*A1*A2);}

        if(a==1 && b==3){ Y = ( - 8.E+0*P_P*k_k*k_kp*A1*B2 - 8.E+0*P_P*k_k
         *k_km*A2*B1 + 8.E+0*pow(P_k,2)*k_kp*A1*B2 + 8.E+0*pow(P_k,2)*k_km
         *A2*B1);}

        if(a==2 && b==0){ Y = (4.E+0*P_P*k_kp*A1*B2 - 4.E+0*P_P*k_km*A2*B1
          - 4.E+0*P_k*P_kp*A1*B2 + 4.E+0*P_k*P_km*A2*B1);}

        if(a==2 && b==1){ Y = ( - 4.E+0*P_P*P_kp*k_km*A1*A2 - 4.E+0*P_P*
         P_km*k_kp*A1*A2 + 8.E+0*P_k*P_kp*P_km*A1*A2);}

        if(a==2 && b==2){ Y = (4.E+0*P_P*P_k*k_k*kp_km*A1*A2 - 4.E+0*P_P*
         P_k*k_k*B1*B2 - 8.E+0*P_P*P_k*k_kp*k_km*A1*A2 + 4.E+0*pow(P_k,2)*
         P_kp*k_km*A1*A2 + 4.E+0*pow(P_k,2)*P_km*k_kp*A1*A2 - 4.E+0*pow(
         P_k,3)*kp_km*A1*A2 + 4.E+0*pow(P_k,3)*B1*B2);}

        if(a==2 && b==3){ Y = ( - 8.E+0*P_P*P_kp*k_k*A1*B2 - 8.E+0*P_P*
         P_km*k_k*A2*B1 + 8.E+0*pow(P_k,2)*P_kp*A1*B2 + 8.E+0*pow(P_k,2)*
         P_km*A2*B1);}

        if(a==3 && b==0){ Y = (8.E+0*P_kp*k_km*A1*A2 - 8.E+0*P_km*k_kp*A1*
         A2);}

        if(a==3 && b==1){ Y = ( - 8.E+0*P_P*k_kp*A1*B2 - 8.E+0*P_P*k_km*A2
         *B1 + 8.E+0*P_k*P_kp*A1*B2 + 8.E+0*P_k*P_km*A2*B1);}

        if(a==3 && b==2){ Y = (8.E+0*P_k*P_kp*k_k*A1*B2 + 8.E+0*P_k*P_km*
         k_k*A2*B1 - 8.E+0*pow(P_k,2)*k_kp*A1*B2 - 8.E+0*pow(P_k,2)*k_km*
         A2*B1);}

        if(a==3 && b==3){ Y = ( - 1.6E+1*P_P*k_k*kp_km*A1*A2 - 1.6E+1*P_P*
         k_k*B1*B2 + 3.2E+1*P_P*k_kp*k_km*A1*A2 - 3.2E+1*P_k*P_kp*k_km*A1*
         A2 - 3.2E+1*P_k*P_km*k_kp*A1*A2 + 1.6E+1*pow(P_k,2)*kp_km*A1*A2
          + 1.6E+1*pow(P_k,2)*B1*B2 + 3.2E+1*P_kp*P_km*k_k*A1*A2);}

         Yr=Y*normy(a, P_P , k_k, P_k);

    return Yr;


}

//---------------------------------------------------------------------------------------------------------------------//
complex<double> BSAgammaL(int a, int b, complex<double> P_P, complex<double> P_q, complex<double> P_l, complex<double> l_l,
                          complex<double> k_l, complex<double> P_k, complex<double> k_q, complex<double> q_l , complex<double> q_q)
{
    complex<double> Lr;
    complex<double> L;

        if(a==0 && b==0){ L =  - 1.2E+1;}

        if(a==0 && b==1){ L =  0;}

        if(a==0 && b==2){ L =  0;}

        if(a==0 && b==3){ L =  0;}

        if(a==1 && b==0){ L =  0;}

        if(a==1 && b==1){ L = 4.E+0*P_P*q_q - 8.E+0*P_q*P_l*q_l*pow(
         l_l,-1) - 4.E+0*pow(P_q,2) + 8.E+0*pow(P_l,2)*q_q*pow(l_l,-1);}

        if(a==1 && b==2){ L =  - 4.E+0*P_k*P_q*k_q - 8.E+0*P_k*P_q*k_l*q_l
         *pow(l_l,-1) + 8.E+0*P_k*P_l*k_l*q_q*pow(l_l,-1) + 4.E+0*pow(
         P_k,2)*q_q;}

        if(a==1 && b==3){ L =  0;}

        if(a==2 && b==0){ L =  0;}

        if(a==2 && b==1){ L =  - 8.E+0*P_P*P_l*q_l*pow(l_l,-1) + 8.E+0*P_q
         *pow(P_l,2)*pow(l_l,-1);}

        if(a==2 && b==2){ L =  - 4.E+0*P_P*P_k*k_q - 8.E+0*P_P*P_k*k_l*q_l
         *pow(l_l,-1) + 8.E+0*P_k*P_q*P_l*k_l*pow(l_l,-1) + 4.E+0*pow(
         P_k,2)*P_q;}

        if(a==2 && b==3){ L =  0;}

        if(a==3 && b==0){ L =  0;}

        if(a==3 && b==1){ L =  0;}

        if(a==3 && b==2){ L =  0;}

        if(a==3 && b==3){ L =  - 1.6E+1*P_P*k_q + 3.2E+1*P_P*k_l*q_l*pow(
         l_l,-1) + 1.6E+1*P_k*P_q - 3.2E+1*P_k*P_l*q_l*pow(l_l,-1) -
         3.2E+1*P_q*P_l*k_l*pow(l_l,-1) + 3.2E+1*pow(P_l,2)*k_q*pow(
         l_l,-1);}


        Lr=L*normy(a,P_P,q_q, P_q);

    return Lr;


}


//---------------------------------------------------------------------------------------------------------------------//
vecdcx get_matrix_Y(int nvector, int nA, complex<double> A1, complex<double> A2, complex<double> B1, complex<double> B2,
                          complex<double> kp_km, complex<double> P_P, complex<double> P_kp,complex<double> P_km, complex<double> P_k, complex<double> k_kp,
                    complex<double> k_km, complex<double> k_k , complex<double> K)
{
    vecdcx Y;
    Y.resize(nvector);


    Y[0+nA*0]= (4.E+0*B1*B2 + 4.E+0*kp_km*A1*A2)*normy(0, P_P , k_k, P_k)*K;

    Y[0+nA*1]= (4.E+0*P_kp*A1*B2 - 4.E+0*P_km*A2*B1)*normy(0, P_P , k_k, P_k)*K;

    Y[0+nA*2]= (4.E+0*P_k*k_kp*A1*B2 - 4.E+0*P_k*k_km*A2*B1)*normy(0, P_P , k_k, P_k)*K;

    Y[0+nA*3]= ( - 8.E+0*P_kp*k_km*A1*A2 + 8.E+0*P_km*k_kp*A1*A2)*normy(0, P_P , k_k, P_k)*K;

    Y[1+nA*0]= (4.E+0*P_k*k_kp*A1*B2 - 4.E+0*P_k*k_km*A2*B1 - 4.E+0*
                                                              P_kp*k_k*A1*B2 + 4.E+0*P_km*k_k*A2*B1)*normy(1, P_P , k_k, P_k)*K;

    Y[1+nA*1]= ( - 4.E+0*P_P*k_k*kp_km*A1*A2 + 4.E+0*P_P*k_k*B1*B2 -
                 4.E+0*P_k*P_kp*k_km*A1*A2 - 4.E+0*P_k*P_km*k_kp*A1*A2 + 4.E+0*
                                                                         pow(P_k,2)*kp_km*A1*A2 - 4.E+0*pow(P_k,2)*B1*B2 + 8.E+0*P_kp*P_km
                                                                                                                           *k_k*A1*A2)*normy(1, P_P , k_k, P_k)*K;

    Y[1+nA*2]= (4.E+0*P_k*P_kp*k_k*k_km*A1*A2 + 4.E+0*P_k*P_km*k_k*
                                                k_kp*A1*A2 - 8.E+0*pow(P_k,2)*k_kp*k_km*A1*A2)*normy(1, P_P , k_k, P_k)*K;

    Y[1+nA*3]= ( - 8.E+0*P_P*k_k*k_kp*A1*B2 - 8.E+0*P_P*k_k*k_km*A2*B1
                 + 8.E+0*pow(P_k,2)*k_kp*A1*B2 + 8.E+0*pow(P_k,2)*k_km*A2*B1)*normy(1, P_P , k_k, P_k)*K;

    Y[2+nA*0]= (4.E+0*P_P*k_kp*A1*B2 - 4.E+0*P_P*k_km*A2*B1 - 4.E+0*
                                                              P_k*P_kp*A1*B2 + 4.E+0*P_k*P_km*A2*B1)*normy(2, P_P , k_k, P_k)*K;

    Y[2+nA*1]= ( - 4.E+0*P_P*P_kp*k_km*A1*A2 - 4.E+0*P_P*P_km*k_kp*A1*
                                               A2 + 8.E+0*P_k*P_kp*P_km*A1*A2)*normy(2, P_P , k_k, P_k)*K;

    Y[2+nA*2]= (4.E+0*P_P*P_k*k_k*kp_km*A1*A2 - 4.E+0*P_P*P_k*k_k*B1*
                                                B2 - 8.E+0*P_P*P_k*k_kp*k_km*A1*A2 + 4.E+0*pow(P_k,2)*P_kp*k_km*
                                                                                     A1*A2 + 4.E+0*pow(P_k,2)*P_km*k_kp*A1*A2 - 4.E+0*pow(P_k,3)*kp_km
                                                                                                                                *A1*A2 + 4.E+0*pow(P_k,3)*B1*B2)*normy(2, P_P , k_k, P_k)*K;

    Y[2+nA*3]= ( - 8.E+0*P_P*P_kp*k_k*A1*B2 - 8.E+0*P_P*P_km*k_k*A2*B1
                 + 8.E+0*pow(P_k,2)*P_kp*A1*B2 + 8.E+0*pow(P_k,2)*P_km*A2*B1)*normy(2, P_P , k_k, P_k)*K;

    Y[3+nA*0]= (8.E+0*P_kp*k_km*A1*A2 - 8.E+0*P_km*k_kp*A1*A2)*normy(3, P_P , k_k, P_k)*K;

    Y[3+nA*1]= ( - 8.E+0*P_P*k_kp*A1*B2 - 8.E+0*P_P*k_km*A2*B1 + 8.E+0
                                                                 *P_k*P_kp*A1*B2 + 8.E+0*P_k*P_km*A2*B1)*normy(3, P_P , k_k, P_k)*K;

    Y[3+nA*2]= (8.E+0*P_k*P_kp*k_k*A1*B2 + 8.E+0*P_k*P_km*k_k*A2*B1 -
                8.E+0*pow(P_k,2)*k_kp*A1*B2 - 8.E+0*pow(P_k,2)*k_km*A2*B1)*normy(3, P_P , k_k, P_k)*K;

    Y[3+nA*3]= ( - 1.6E+1*P_P*k_k*kp_km*A1*A2 - 1.6E+1*P_P*k_k*B1*B2
                 + 3.2E+1*P_P*k_kp*k_km*A1*A2 - 3.2E+1*P_k*P_kp*k_km*A1*A2 -
                 3.2E+1*P_k*P_km*k_kp*A1*A2 + 1.6E+1*pow(P_k,2)*kp_km*A1*A2 +
                 1.6E+1*pow(P_k,2)*B1*B2 + 3.2E+1*P_kp*P_km*k_k*A1*A2)*normy(3, P_P , k_k, P_k)*K;


    return Y;


}

//---------------------------------------------------------------------------------------------------------------------//


//----------------------------------------------------------------------------------------------------------------------//
//void precalculate_Y(bse_amplitude &bse,VectorXcd &Adsep, VectorXcd &Bdsep,VectorXcd &Adsem, VectorXcd &Bdsem, vecvecdx &Ym, double M)
void precalculate_Y(bse_amplitude &bse,VectorXcd &Adsep, VectorXcd &Bdsep,VectorXcd &Adsem, VectorXcd &Bdsem, vecdcx &Ym, double M)
{
    int m=bse.m;
    int nz=bse.nz;
    int ny=bse.ny;
    int nA=bse.Ampnumber;

    four_vec P;
    if(M<0){P.inhalt[3]=-M;}else{imag(P.inhalt[3])=M;}

    Ym.resize(nA*nA*nz*m);


//#pragma omp parallel for default(shared)
//radiale Integration über k2 innen
    for (int i_k=0; i_k<m; i_k++)
    {
        complex<double> rad_weigth=bse.radw[i_k]; complex<double> k_k=bse.p2[i_k];

                //z Integration
                for(int i_zk=0; i_zk<nz; i_zk++)
                {

                    double zk=bse.zqp[i_zk]; double zk_weigth=bse.w_z[i_zk];

                    complex<double> A1=Adsep(i_k+i_zk*m);
                    complex<double> A2=Adsem(i_k+i_zk*m);
                    complex<double> B1=Bdsep(i_k+i_zk*m);
                    complex<double> B2=Bdsem(i_k+i_zk*m);


                    four_vec k, kp, km;
                    complex<double> kp_kp, km_km,Y, K;

                    //since the following dot products only depend on k² or k.P where as only the
                    k.inhalt[0]=0.0; k.inhalt[1]=0.0;
                    k.inhalt[2]=sqrt(k_k)*sqrt(1.0-zk*zk); k.inhalt[3]=sqrt(k_k)*zk;

                    kp=k+bse.etabse*P;
                    km=k-(1.0-bse.etabse)*P;

                    kp_kp=kp*kp;
                    km_km=km*km;
                    //Denominator
                    K= 1.0/((kp_kp*A1*A1+B1*B1)*(km_km*A2*A2+B2*B2));

//                    Ym.push_back(get_matrix_Y(ny*nz*m,nA,A1,A2,B1,B2, kp*km, P*P, P*kp, P*km, P*k, k*kp, k*km, k*k,K));

                    //can either set the whole vector as vector in a vector or everything in one vector.
                    for (int k_amp = 0; k_amp < nA; ++k_amp) {
                        for (int j_amp = 0; j_amp < nA; ++j_amp) {

                            Ym[j_amp+nA*k_amp+nA*nA*i_zk+nA*nA*nz*i_k]=BSAgammaY(j_amp,k_amp,A1,A2,B1,B2, kp*km, P*P, P*kp, P*km, P*k, k*kp, k*km, k*k)*K;

                        }

                    }


                 }
    }


}
//----------------------------------------------------------------------------------------------------------------------//
void precalculate_L(double Z2, bse_amplitude &bse,vecdcx &Lm, double M, int PauliVillar) {
    int m = bse.m;
    int nz = bse.nz;
    int ny = bse.ny;
    int nA = bse.Ampnumber;
    int Nche = bse.nCheb;
    double Vorfac = -(1.0) / (3.0 * Pi * Pi * 4.0 * Pi) * Z2 *
                    Z2;         //BSA Integral faktor: 1/(2Pi)⁴ * 1/2(k Integral)* 2Pi(Winkel)* Cf(4/3) =(1/ 3*Pi*Pi*4)

    four_vec P;
    if (M < 0) { P.inhalt[3] = -M; } else { imag(P.inhalt[3]) = M; }

    mtmodel interaction;

    Lm.resize(nz * nA * nA * nz * m * m);

#pragma omp parallel for default(shared)
//Winkel Grid zq außen
    for (int ia = 0; ia < nz; ia++) {
        //radiales Grid über q2 außen
        for (int ja = 0; ja < m; ja++) {
            four_vec q;
            double zq = bse.zqp[ia];
            complex<double> q_q = bse.p2[ja];
            q.inhalt[2] = sqrt(q_q) * sqrt(1.0 - zq * zq);
            q.inhalt[3] = sqrt(q_q) * zq;

            //Amplituden loop außen
            for (int i_amp = 0; i_amp < nA; i_amp++) {

                //numerische Winkel Integration über zk innen
                for (int i = 0; i < nz; i++) {
                    double z = bse.zqp[i];
                    double z_weigth = bse.w_z[i];


                    //radiale Integration über k2 innen
                    for (int j = 0; j < m; j++) {
                        complex<double> rad_weigth = bse.radw[j];
                        complex<double> k_k = bse.p2[j];

                        //Amplituden loop innen: sum up!
                        for (int j_amp = 0; j_amp < nA; j_amp++) {

                            complex<double> sum = 0.0;
                            // numerische Winkel Integration über y: sum up!
                            for (int ly = 0; ly < ny; ly++) {

                                four_vec k, kp, km, l;
                                complex<double> kp_kp, km_km, L, K;

                                double y = bse.zy[ly];
                                double y_weigth = bse.w_y[ly];

                                k.inhalt[0] = 0.0;
                                k.inhalt[1] = sqrt(k_k) * sqrt(1.0 - z * z) * sqrt(1.0 - y * y);
                                k.inhalt[2] = sqrt(k_k) * y * sqrt(1.0 - z * z);
                                k.inhalt[3] = sqrt(k_k) * z;

                                kp = k + bse.etabse * P;
                                km = k - (1.0 - bse.etabse) * P;

                                kp_kp = kp * kp;
                                km_km = km * km;

                                l = q - k;


                                if (PauliVillar == 0) {
                                    K = 1.0;

                                } else if (PauliVillar == 1) {
                                    K = (lam2 / (lam2 + l * l));
                                }


                                L = BSAgammaL(i_amp, j_amp, P * P, P * q, P * l, l * l, k * l, P * k, k * q, q * l,
                                              q * q);

                                ////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!BEWARE!!!!!!!!!!!!!!!!!!!!
//                                if(i_amp>1||j_amp> 1){L=0;}

                                sum += interaction.mtc(l * l, PauliVillar, 1) / (l * l) * y_weigth * L * K;


                            }

                            Lm[j_amp + nA * j + nA * m * i + nA * m * nz * i_amp + nA * m * nz * nA * ja +
                               nA * m * nz * nA * m * ia] = sum;


                        }
                    }
                }

                //innen vs. aussen

            }
        }
    }
}
//----------------------------------------------------------------------------------------------------------------------//
//----------------------------------------------------------------------------------------------------------------------//
void precalculate_L_Cheb(double Z2, bse_amplitude &bse,vecdcx &Lm, double M, int PauliVillar)
{
    int m=bse.m;
    int nz=bse.nz;
    int ny=bse.ny;
    int nA=bse.Ampnumber;
    int Nche=bse.nCheb;
    double Vorfac= -(1.0)/(3.0*Pi*Pi*4.0*Pi)*Z2*Z2;         //BSA Integral faktor: 1/(2Pi)⁴ * 1/2(k Integral)* 2Pi(Winkel)* Cf(4/3) =(1/ 3*Pi*Pi*4)

    four_vec P;
    if(M<0){P.inhalt[3]=-M;}else{imag(P.inhalt[3])=M;}

    mtmodel interaction;

    Lm.resize(Nche*nA*nA*nz*m*m);


#pragma omp parallel for default(shared)
//Cheby's außen
for(int alpha=0; alpha<Nche; alpha++)
{
    //radiales Grid über q2 außen
    for(int i_q=0; i_q<m; i_q++)
    {
        cdouble q_q=bse.p2[i_q];

        //Amplituden loop außen
        for(int i_amp=0; i_amp<nA; i_amp++)
        {

                //radiale Integration über k2 innen
                for (int i_k=0; i_k<m; i_k++)
                {
                    complex<double> rad_weigth=bse.radw[i_k]; complex<double> k_k=bse.p2[i_k];


                        //Amplituden summe innen j(Lij Yjk)::
                        for(int j_amp=0; j_amp<nA; j_amp++)
                        {
                            //z Integration
                            for(int i_zk=0; i_zk<nz; i_zk++)
                            {

                                double zk=bse.zqp[i_zk]; double zk_weigth=bse.w_z[i_zk];


                                complex<double> sum2=0.0;
                                //Cheby Summe
                                for(int m_cheb=0; m_cheb<Nche; m_cheb++)
                                {
                                    four_vec q;
                                    double zq=cos((m_cheb-0.5+1.0)*Pi/Nche);
                                    q.inhalt[2]= sqrt(q_q)*sqrt(1.0-zq*zq); q.inhalt[3]=sqrt(q_q)*zq;

                                    cdouble Talpha=Cheby(alpha,zq)*pow(ii,-alpha);



                                    complex<double> sum=0.0;
                                    // numerische Winkel Integration über y: sum up
                                    for(int ly=0; ly<ny; ly++)
                                    {

                                        four_vec k, kp, km;
                                        complex<double> kp_kp, km_km, K;

                                        double y= bse.zy[ly];
                                        double y_weigth= bse.w_y[ly];

                                        k.inhalt[0]=0.0; k.inhalt[1]=sqrt(k_k)*sqrt(1.0-zk*zk)*sqrt(1.0-y*y);
                                        k.inhalt[2]=sqrt(k_k)*y*sqrt(1.0-zk*zk); k.inhalt[3]=sqrt(k_k)*zk;

                                        kp=k+bse.etabse*P;
                                        km=k-(1.0-bse.etabse)*P;

                                        kp_kp=kp*kp;
                                        km_km=km*km;

                                        four_vec l; cdouble L;

                                        l=q-k;

                                        //Denominator
                                        if(PauliVillar==0)
                                        {
                                            K= 1.0;

                                        }else if (PauliVillar==1)
                                        {
                                            K= (lam2/(lam2+l*l));
                                        }

                                        L = BSAgammaL(i_amp,j_amp, P*P, P*q, P*l, l*l, k*l, P*k, k*q, q*l, q*q);


                                        //Putting it all together
                                        sum += interaction.mtc(l*l,PauliVillar,1)/(l*l)*y_weigth*L*K;

                                    }

                                    sum2 += sum*Talpha;
                                }

                                Lm[i_zk+nz*j_amp+nz*nA*i_k+nz*nA*m*i_amp+nz*nA*m*nA*i_q+nz*nA*m*nA*m*alpha]=sum2;
                            }

                        }
                }
        }
    }
}

}



//----------------------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------------------------//
void InteBSA_All_par(double Z2, bse_amplitude &bse,VectorXcd &Adsep, VectorXcd &Bdsep,VectorXcd &Adsem, VectorXcd &Bdsem, MatrixXcd &Me, double M, int PauliVillar)
{
    int m=bse.m;
    int nz=bse.nz;
    int ny=bse.ny;
    int nA=bse.Ampnumber;
    double Vorfac= -(1.0)/(3.0*Pi*Pi*4.0*Pi)*Z2*Z2;         //BSA Integral faktor: 1/(2Pi)⁴ * 1/2(k Integral)* 2Pi(Winkel)* Cf(4/3) =(1/ 3*Pi*Pi*4)

    four_vec P;
    if(M<0){P.inhalt[3]=-M;}else{imag(P.inhalt[3])=M;}

    mtmodel interaction;

    vecdcx Lm;
    precalculate_L(Z2, bse, Lm, M, PauliVillar);

//    vecvecdx Ym;
    vecdcx Ym;
    precalculate_Y(bse, Adsep, Bdsep, Adsem, Bdsem, Ym, M);


#pragma omp parallel for default(shared)
//Winkel Grid zq außen
    for(int ia=0; ia<nz; ia++)
    {
        //radiales Grid über q2 außen
        for(int ja=0; ja<m; ja++)
        {
            four_vec q;
            double zq=bse.zqp[ia]; complex<double> q_q=bse.p2[ja];
            q.inhalt[2]= sqrt(q_q)*sqrt(1.0-zq*zq); q.inhalt[3]=sqrt(q_q)*zq;

            //Amplituden loop außen
            for(int i_amp=0; i_amp<nA; i_amp++)
            {

                //numerische Winkel Integration über zk innen
                for(int i=0; i<nz; i++)
                {
                    double z=bse.zqp[i]; double z_weigth=bse.w_z[i];


                    //radiale Integration über k2 innen
                    for (int j=0; j<m; j++)
                    {
                        complex<double> rad_weigth=bse.radw[j]; complex<double> k_k=bse.p2[j];

                        //Amplituden loop innen
                        for(int k_amp=0; k_amp<nA; k_amp++)
                        {

                            complex<double> wink2=0.0;
                            //Amplituden loop innen: sum up!
                            for(int j_amp=0; j_amp<nA; j_amp++)
                            {

                                complex<double>  Y, L;

//                                Y = Ym[k_amp + nA * j_amp][i + nz * j];
                                Y = Ym[j_amp+nA*k_amp+nA*nA*i+nA*nA*nz*j];

                                L = Lm[j_amp + nA * j + nA * m * i + nA * m * nz * i_amp + nA * m * nz * nA * ja + nA * m * nz * nA * m * ia];

                                wink2 += Y*L;



                            }


                            Me(i_amp+nA*ja+ia*nA*m,k_amp+nA*j+i*nA*m)= Vorfac*rad_weigth*(k_k)*(k_k)*sqrt(1.0-z*z)*z_weigth*wink2;


                        }
                    }
                }

                //innen vs. aussen

            }
        }
    }



}
//----------------------------------------------------------------------------------------------------------------------//
//----------------------------------------------------------------------------------------------------------------------//
void InteBSA_All_par_Cheby(double Z2, bse_amplitude &bse,VectorXcd &Adsep, VectorXcd &Bdsep,VectorXcd &Adsem, VectorXcd &Bdsem, MatrixXcd &Me, double M, int PauliVillar) {
    int m = bse.m;
    int nz = bse.nz;
    int ny = bse.ny;
    int nA = bse.Ampnumber;
    int Nche = bse.nCheb;
    double Vorfac = -(1.0) / (3.0 * Pi * Pi * 4.0 * Pi) * Z2 * Z2;         //BSA Integral faktor: 1/(2Pi)⁴ * 1/2(k Integral)* 2Pi(Winkel)* Cf(4/3) =(1/ 3*Pi*Pi*4)

    four_vec P;
    if (M < 0) { P.inhalt[3] = -M; } else { imag(P.inhalt[3]) = M; }

    mtmodel interaction;

    vecdcx Lm;
    precalculate_L_Cheb(Z2, bse, Lm, M, PauliVillar);

//    vecvecdx Ym;
    vecdcx Ym;
    precalculate_Y(bse, Adsep, Bdsep, Adsem, Bdsem, Ym, M);


//#pragma omp parallel for default(shared)
//Cheby's außen
    for (int alpha = 0; alpha < Nche; alpha++) {
        //radiales Grid über q2 außen
        for (int i_q = 0; i_q < m; i_q++) {

            //Amplituden loop außen
            for (int i_amp = 0; i_amp < nA; i_amp++) {

                //Cheby's innen
                for (int beta = 0; beta < Nche; beta++) {


                    //radiale Integration über k2 innen
                    for (int i_k = 0; i_k < m; i_k++) {
                        complex<double> rad_weigth = bse.radw[i_k];
                        complex<double> k_k = bse.p2[i_k];

                        //Amplituden loop innen
                        for (int k_amp = 0; k_amp < nA; k_amp++) {

                            //Matrix indices stop, summation starts:: Everything below get's summed up!

                            complex<double> sum = 0.0;
                            //Amplituden summe innen j(Lij Yjk)::
                            for (int j_amp = 0; j_amp < nA; j_amp++) {
                                //z Integration
                                for (int i_zk = 0; i_zk < nz; i_zk++) {

                                    double zk = bse.zqp[i_zk];
                                    double zk_weigth = bse.w_z[i_zk];
                                    complex<double> Tbeta = Cheby(beta, zk) * pow(ii, beta);
                                    cdouble Y, L;

//                                    Y = Ym[k_amp + nA * j_amp][i_zk + nz * i_k];
                                    Y=Ym[j_amp+nA*k_amp+nA*nA*i_zk+nA*nA*nz*i_k];

                                    L = Lm[i_zk + nz * j_amp + nz * nA * i_k + nz * nA * m * i_amp +
                                           nz * nA * m * nA * i_q + nz * nA * m * nA * m * alpha];

//                                    cout<<"test: "<<L<<tab<<Y<<endl;


                                    //Putting it all together
                                    sum += Y * L * sqrt(1.0 - zk * zk) * zk_weigth * (2.0 / Nche) * Tbeta;
                                }

                            }


                            Me(i_amp + nA * i_q + alpha * nA * m, k_amp + nA * i_k + beta * nA * m) =
                                    Vorfac * rad_weigth * (k_k) * (k_k) * sum;


                        }
                    }
                }

                //innen vs. aussen

            }
        }
    }


}

//----------------------------------------------------------------------------------------------------------------------//


//----------------------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------------------------//
void InteBSA_All_par_Cheby_new_old(double Z2, bse_amplitude &bse,VectorXcd &Adsep, VectorXcd &Bdsep,VectorXcd &Adsem, VectorXcd &Bdsem, MatrixXcd &Me, double M, int PauliVillar)
{
    int m=bse.m;
    int nz=bse.nz;
    int ny=bse.ny;
    int nA=bse.Ampnumber;
    int Nche=bse.nCheb;
    double Vorfac= -(1.0)/(3.0*Pi*Pi*4.0*Pi)*Z2*Z2;         //BSA Integral faktor: 1/(2Pi)⁴ * 1/2(k Integral)* 2Pi(Winkel)* Cf(4/3) =(1/ 3*Pi*Pi*4)

    four_vec P;
    if(M<0){P.inhalt[3]=-M;}else{imag(P.inhalt[3])=M;}

    mtmodel interaction;


#pragma omp parallel for default(shared)
//Cheby's außen
    for(int alpha=0; alpha<Nche; alpha++)
    {
        //radiales Grid über q2 außen
        for(int i_q=0; i_q<m; i_q++)
        {

            //Amplituden loop außen
            for(int i_amp=0; i_amp<nA; i_amp++)
            {

                //Cheby's innen
                for(int beta=0; beta<Nche; beta++)
                {


                    //radiale Integration über k2 innen
                    for (int i_k=0; i_k<m; i_k++)
                    {
                        complex<double> rad_weigth=bse.radw[i_k]; complex<double> k_k=bse.p2[i_k];

                        //Amplituden loop innen
                        for(int k_amp=0; k_amp<nA; k_amp++)
                        {


                            //Matrix indices stop, summation starts:: Everything below get's summed up!

                            complex<double> sum=0.0;
                            //Amplituden summe innen j(Lij Yjk)::
                            for(int j_amp=0; j_amp<nA; j_amp++)
                            {
                                //z Integration
                                for(int i_zk=0; i_zk<nz; i_zk++)
                                {

                                    double zk=bse.zqp[i_zk]; double zk_weigth=bse.w_z[i_zk];
                                    complex<double> Tbeta=Cheby(beta,zk)*pow(ii,beta);

                                    complex<double> A1=Adsep(i_k+i_zk*m);
                                    complex<double> A2=Adsem(i_k+i_zk*m);
                                    complex<double> B1=Bdsep(i_k+i_zk*m);
                                    complex<double> B2=Bdsem(i_k+i_zk*m);



                                    // numerische Winkel Integration über y: sum up!
                                    for(int ly=0; ly<ny; ly++)
                                    {

                                        four_vec k, kp, km;
                                        complex<double> kp_kp, km_km,Y, K;

                                        double y= bse.zy[ly];
                                        double y_weigth= bse.w_y[ly];

                                        k.inhalt[0]=0.0; k.inhalt[1]=sqrt(k_k)*sqrt(1.0-zk*zk)*sqrt(1.0-y*y);
                                        k.inhalt[2]=sqrt(k_k)*y*sqrt(1.0-zk*zk); k.inhalt[3]=sqrt(k_k)*zk;

                                        kp=k+bse.etabse*P;
                                        km=k-(1.0-bse.etabse)*P;

                                        kp_kp=kp*kp;
                                        km_km=km*km;


                                        Y = (BSAgammaY(j_amp,k_amp,A1,A2,B1,B2, kp*km, P*P, P*kp, P*km, P*k, k*kp, k*km, k*k));

                                        //Cheby Summe
//                              for(int m_cheb=1; m_cheb<Nche; m_cheb++)
                                        for(int m_cheb=0; m_cheb<Nche; m_cheb++)
                                        {
                                            four_vec q,l; cdouble L;
                                            cdouble q_q=bse.p2[i_q];
                                            double zq=cos((m_cheb-0.5+1.0)*Pi/Nche);
                                            q.inhalt[2]= sqrt(q_q)*sqrt(1.0-zq*zq); q.inhalt[3]=sqrt(q_q)*zq;

                                            l=q-k;

                                            //Denominator
                                            if(PauliVillar==0)
                                            {
                                                K= 1.0/((kp_kp*A1*A1+B1*B1)*(km_km*A2*A2+B2*B2));

                                            }else if (PauliVillar==1)
                                            {
                                                K= 1.0/((kp_kp*A1*A1+B1*B1)*(km_km*A2*A2+B2*B2))*(lam2/(lam2+l*l));
                                            }

                                            cdouble Talpha=Cheby(alpha,zq)*pow(ii,-alpha);



                                            L = BSAgammaL(i_amp,j_amp, P*P, P*q, P*l, l*l, k*l, P*k, k*q, q*l, q*q);


                                            //Putting it all together
                                            sum += interaction.mtc(l*l,PauliVillar,1)/(l*l)*y_weigth*Y*L*K*sqrt(1.0-zk*zk)*zk_weigth*(2.0/Nche)*Talpha*Tbeta;

                                        }
                                    }
                                }

                            }


                            Me(i_amp+nA*i_q+alpha*nA*m,k_amp+nA*i_k+beta*nA*m)= Vorfac*rad_weigth*(k_k)*(k_k)*sum;


                        }
                    }
                }

                //innen vs. aussen

            }
        }
    }



}
//----------------------------------------------------------------------------------------------------------------------//
void InteBSA_All_par_Cheby_old(double Z2, bse_amplitude &bse,VectorXcd &Adsep, VectorXcd &Bdsep,VectorXcd &Adsem, VectorXcd &Bdsem, MatrixXcd &Me, double M, int PauliVillar)
{
    int m=bse.m;
    int nz=bse.nz;
    int ny=bse.ny;
    int nA=bse.Ampnumber;
    int Nche=bse.nCheb;
    double Vorfac= -(1.0)/(3.0*Pi*Pi*4.0*Pi)*Z2*Z2;         //BSA Integral faktor: 1/(2Pi)⁴ * 1/2(k Integral)* 2Pi(Winkel)* Cf(4/3) =(1/ 3*Pi*Pi*4)

    four_vec P;
    if(M<0){P.inhalt[3]=-M;}else{imag(P.inhalt[3])=M;}

    mtmodel interaction;


#pragma omp parallel for default(shared)
//Cheby's außen
    for(int alpha=0; alpha<Nche; alpha++)
    {
        //radiales Grid über q2 außen
        for(int i_q=0; i_q<m; i_q++)
        {

            //Amplituden loop außen
            for(int i_amp=0; i_amp<nA; i_amp++)
            {

                //Cheby's innen
                for(int beta=0; beta<Nche; beta++)
                {
//           complex<double> z=bse.zqp[i]; complex<double> z_weigth=bse.w_z[i];


                    //radiale Integration über k2 innen
                    for (int i_k=0; i_k<m; i_k++)
                    {
                        complex<double> rad_weigth=bse.radw[i_k]; complex<double> k_k=bse.p2[i_k];

                        //Amplituden loop innen
                        for(int k_amp=0; k_amp<nA; k_amp++)
                        {

                            complex<double> sum=0.0;
                            //Amplituden summe innen j(Lij Yjk)::         Everything below get's summed up!
                            for(int j_amp=0; j_amp<nA; j_amp++)
                            {
                                // numerische Winkel Integration über y: sum up!
                                for(int ly=0; ly<ny; ly++)
                                {
                                    //Cheby Summe
//                              for(int m_cheb=1; m_cheb<Nche; m_cheb++)
                                    for(int m_cheb=0; m_cheb<Nche; m_cheb++)
                                    {
                                        four_vec q;
                                        complex<double> q_q=bse.p2[i_q];
                                        double zq=cos((m_cheb-0.5+1.0)*Pi/Nche);
                                        q.inhalt[2]= sqrt(q_q)*sqrt(1.0-zq*zq); q.inhalt[3]=sqrt(q_q)*zq;

                                        complex<double> Talpha=Cheby(alpha,zq)*pow(ii,-alpha);


                                        //z Integration
                                        for(int i_zk=0; i_zk<nz; i_zk++)
                                        {

                                            four_vec k, kp, km, l;
                                            complex<double> kp_kp, km_km,Y, L, K;

                                            double y= bse.zy[ly];
                                            double y_weigth= bse.w_y[ly];

                                            double zk=bse.zqp[i_zk]; double zk_weigth=bse.w_z[i_zk];

                                            complex<double> Tbeta=Cheby(beta,zk)*pow(ii,beta);

                                            complex<double> A1=Adsep(i_k+i_zk*m);
                                            complex<double> A2=Adsem(i_k+i_zk*m);
                                            complex<double> B1=Bdsep(i_k+i_zk*m);
                                            complex<double> B2=Bdsem(i_k+i_zk*m);

                                            k.inhalt[0]=0.0; k.inhalt[1]=sqrt(k_k)*sqrt(1.0-zk*zk)*sqrt(1.0-y*y);
                                            k.inhalt[2]=sqrt(k_k)*y*sqrt(1.0-zk*zk); k.inhalt[3]=sqrt(k_k)*zk;

                                            kp=k+bse.etabse*P;
                                            km=k-(1.0-bse.etabse)*P;

                                            kp_kp=kp*kp;
                                            km_km=km*km;


                                            l=q-k;


                                            Y = (BSAgammaY(j_amp,k_amp,A1,A2,B1,B2, kp*km, P*P, P*kp, P*km, P*k, k*kp, k*km, k*k));

                                            L = BSAgammaL(i_amp,j_amp, P*P, P*q, P*l, l*l, k*l, P*k, k*q, q*l, q*q);


                                            //Denominator
                                            if(PauliVillar==0)
                                            {
                                                K= 1.0/((kp_kp*A1*A1+B1*B1)*(km_km*A2*A2+B2*B2));

                                            }else if (PauliVillar==1)
                                            {
                                                K= 1.0/((kp_kp*A1*A1+B1*B1)*(km_km*A2*A2+B2*B2))*(lam2/(lam2+l*l));
                                            }


                                            //Putting it all together
                                            sum += interaction.mtc(l*l,PauliVillar,1)/(l*l)*y_weigth*Y*L*K*sqrt(1.0-zk*zk)*zk_weigth*(2.0/Nche)*Talpha*Tbeta;

                                        }
                                    }
                                }

                            }


                            Me(i_amp+nA*i_q+alpha*nA*m,k_amp+nA*i_k+beta*nA*m)= Vorfac*rad_weigth*(k_k)*(k_k)*sum;


                        }
                    }
                }

                //innen vs. aussen

            }
        }
    }



}
//---------------------------------------------------------------------------------------------------------------------//
void InteBSA_All_par_old(double Z2, bse_amplitude &bse,VectorXcd &Adsep, VectorXcd &Bdsep,VectorXcd &Adsem, VectorXcd &Bdsem, MatrixXcd &Me, double M, int PauliVillar)
{
    int m=bse.m;
    int nz=bse.nz;
    int ny=bse.ny;
    int nA=bse.Ampnumber;
    double Vorfac= -(1.0)/(3.0*Pi*Pi*4.0*Pi)*Z2*Z2;         //BSA Integral faktor: 1/(2Pi)⁴ * 1/2(k Integral)* 2Pi(Winkel)* Cf(4/3) =(1/ 3*Pi*Pi*4)

    four_vec P;
    if(M<0){P.inhalt[3]=-M;}else{imag(P.inhalt[3])=M;}

    mtmodel interaction;

#pragma omp parallel for default(shared)
//Winkel Grid zq außen
    for(int ia=0; ia<nz; ia++)
    {
        //radiales Grid über q2 außen
        for(int ja=0; ja<m; ja++)
        {
            four_vec q;
            double zq=bse.zqp[ia]; complex<double> q_q=bse.p2[ja];
            q.inhalt[2]= sqrt(q_q)*sqrt(1.0-zq*zq); q.inhalt[3]=sqrt(q_q)*zq;

            //Amplituden loop außen
            for(int i_amp=0; i_amp<nA; i_amp++)
            {

                //numerische Winkel Integration über zk innen
                for(int i=0; i<nz; i++)
                {
                    double z=bse.zqp[i]; double z_weigth=bse.w_z[i];


                    //radiale Integration über k2 innen
                    for (int j=0; j<m; j++)
                    {
                        complex<double> rad_weigth=bse.radw[j]; complex<double> k_k=bse.p2[j];

                        complex<double> A1=Adsep(j+i*m);
                        complex<double> A2=Adsem(j+i*m);
                        complex<double> B1=Bdsep(j+i*m);
                        complex<double> B2=Bdsem(j+i*m);

                        //Amplituden loop innen
                        for(int k_amp=0; k_amp<nA; k_amp++)
                        {

                            complex<double> wink2=0.0;
                            // numerische Winkel Integration über y: sum up!
                            for(int ly=0; ly<ny; ly++)
                            {
                                //Amplituden loop innen: sum up!
                                for(int j_amp=0; j_amp<nA; j_amp++)
                                {

                                    four_vec k, kp, km, l;
                                    complex<double> kp_kp, km_km, Y, L, K;
//                              complex<double> k_kp, k_km, P_kp, P_km, P_l, P_k, kp_km, P_q, k_l, k_q, q_l;

                                    double y= bse.zy[ly];
                                    double y_weigth= bse.w_y[ly];

                                    k.inhalt[0]=0.0; k.inhalt[1]=sqrt(k_k)*sqrt(1.0-z*z)*sqrt(1.0-y*y);
                                    k.inhalt[2]=sqrt(k_k)*y*sqrt(1.0-z*z); k.inhalt[3]=sqrt(k_k)*z;

                                    kp=k+bse.etabse*P;
                                    km=k-(1.0-bse.etabse)*P;

                                    kp_kp=kp*kp;
                                    km_km=km*km;

                                    l=q-k;

                                    Y = (BSAgammaY(j_amp,k_amp,A1,A2,B1,B2, kp*km, P*P, P*kp, P*km, P*k, k*kp, k*km, k*k));


                                    if(PauliVillar==0)
                                    {
                                        K= 1.0/((kp_kp*A1*A1+B1*B1)*(km_km*A2*A2+B2*B2));

                                    }else if (PauliVillar==1)
                                    {
                                        K= 1.0/((kp_kp*A1*A1+B1*B1)*(km_km*A2*A2+B2*B2))*(lam2/(lam2+l*l));
                                    }

                                    L = BSAgammaL(i_amp,j_amp, P*P, P*q, P*l, l*l, k*l, P*k, k*q, q*l, q*q);

                                    wink2 += interaction.mtc(l*l,PauliVillar,1)/(l*l)*y_weigth*Y*L*K;


                                }


                            }


                            Me(i_amp+nA*ja+ia*nA*m,k_amp+nA*j+i*nA*m)= Vorfac*rad_weigth*(k_k)*(k_k)*sqrt(1.0-z*z)*z_weigth*wink2;


                        }
                    }
                }

                //innen vs. aussen

            }
        }
    }



}
