/*
 * BSE.h
 *
 * calculation of the Bethe-Salpeter equation for the pion
 *
 * ---------------------------------------------------------------------
 * 01.08.15 Esther Weil
 *
 *Content:calc of the BSE for pion, mass for a certain setup of grid-pionts. Chi.
 * ---------------------------------------------------------------------
 *
 *functions: m_eigenvalue,
 *
 *
 * BSA_Chi: turns BSA Gamma into the untruncated version Chi
 * pion_mass: Find the pion Mass for which the eigenvalue will be 1
 * bs_quarkmass: Finds the corresponding quark mass to a fixed bound state mass
 *BSE_parameter: Genorts parameterization of the amplitude for comparision
 * bs_amplitude_calc: Thing that was in main_BSE before. Different choices of calcution and initialization of BSE.
 *
 */




//This is form code stuff--------------------------------------------------------------end
//---------------------------------------------------------------------------------------------------------------------//


//---------------------------------------------------------------------------------------------------------------------//
void BSA_Chi(bse_amplitude &bse, bse_amplitude &chi, double M, VectorXcd &Adsep, VectorXcd &Bdsep,VectorXcd &Adsem, VectorXcd &Bdsem, string rundef)
{

    int m=bse.m;
    int nz=bse.nz;
    int nA=bse.Ampnumber;

    four_vec P;
    if(M<0){P.inhalt[3]=-M;}else{imag(P.inhalt[3])=M;}
//    bse_amplitude chi_n(M, m,chi.ny,nz,eta,nA);

#pragma omp parallel for default(shared)
//Winkel Grid zq außen
for(int ia=0; ia<nz; ia++)
{
    //radiales Grid über q2 außen
    for(int ja=0; ja<m; ja++)
    {
      four_vec q;
      complex<double> zq=bse.zqp[ia]; complex<double> q_q=bse.p2[ja];
      q.inhalt[2]= sqrt(q_q)*sqrt(1.0-zq*zq); q.inhalt[3]=sqrt(q_q)*zq;

      four_vec qp, qm;


      complex<double> A1=Adsep(ja+ia*m);
      complex<double> A2=Adsem(ja+ia*m);
      complex<double> B1=Bdsep(ja+ia*m);
      complex<double> B2=Bdsem(ja+ia*m);

      qp=q+chi.etabse*P;
      qm=q-(1.0-chi.etabse)*P;


      //Amplituden loop außen
      for(int i_amp=0; i_amp<nA; i_amp++)
      {

          complex<double> Y=0.0;
          //Amplituden loop innen
          for(int k_amp=0; k_amp<nA; k_amp++)
          {

             Y += (BSAgammaY(i_amp,k_amp,A1,A2,B1,B2, qp*qm, P*P, P*qp, P*qm, P*q, q*qp, q*qm, q_q))*bse.Amp[k_amp][ja+m*ia];

          }

          chi.Amp[i_amp][ja+m*ia]=Y;
//          chi_n.Amp[i_amp][ja+m*ia]=Y*(1.0/(qp*qp*A1*A1+B1*B1))*(1.0/(qm*qm*A2*A2+B2*B2));

      }

//      chi_n.p2[ja]=chi.p2[ja];
    }
}

chi.eigenv=bse.eigenv; chi.fpi=bse.fpi; chi.Mbse=bse.Mbse;

chi.write(7,2,rundef);
//chi_n.write(8,2,rundef);

}
//---------------------------------------------------------------------------------------------------------------------//
double BSA_propagator(bse_amplitude &bse, quark_prop &quark, double M, int calctag, string rundef, int PauliVillar, int ComqSaver)
{

    int m=bse.m; int nz=bse.nz;
    VectorXcd Adsep(m*nz);
    VectorXcd Adsem(m*nz);
    VectorXcd Bdsep(m*nz);
    VectorXcd Bdsem(m*nz);
    double etam=bse.etabse;


    if(calctag==1)
    {
        ComQuarkpm(m,nz,quark,bse.p2,bse.zqp,Adsep, Bdsep, 1, ComqSaver, rundef, M , etam, PauliVillar);
        ComQuarkpm(m,nz,quark,bse.p2,bse.zqp,Adsem, Bdsem, 2, ComqSaver, rundef, M , etam, PauliVillar);
    }else
    {
        read_inn_com(Adsep, Bdsep, rundef,1); cout<<"Read Inn 1   ,";
        read_inn_com(Adsem, Bdsem, rundef,2); cout<<"Read Inn 2"<<endl;

    }

    double eigenv;


    if(bse.nCheb==0)
    {

        MatrixXcd Me(bse.m*bse.nz*bse.Ampnumber,bse.m*bse.nz*bse.Ampnumber);

        InteBSA_All_par(quark.Z2,bse, Adsep, Bdsep, Adsem, Bdsem, Me, M, PauliVillar);


        cout<<"BSA Matrix for all amplitudes calculated with the direct angle!-----------"<<Me(0,0)<<endl;


        vecdcx Gamma0;
        for (int k=0; k<bse.m*bse.nz*bse.Ampnumber; k++)
        {
            Gamma0.push_back(1.0);
        }

        eigenv= m_eigenvalue(Me,Gamma0,M);

        for(int i=0; i<m*nz; i++)
        {
            for (int k=0; k<bse.Ampnumber; k++)
            {
                bse.Amp[k][i]=Gamma0[k+bse.Ampnumber*i];
            }
        }

    }else
    {
        MatrixXcd Me(bse.m*bse.nCheb*bse.Ampnumber,bse.m*bse.nCheb*bse.Ampnumber);

        InteBSA_All_par_Cheby(quark.Z2,bse, Adsep, Bdsep, Adsem, Bdsem, Me, M, PauliVillar);

        cout<<"BSA Matrix for all amplitudes calculated with Chebys!-----------"<<Me(0,0)<<endl;

        vecdcx Gamma0;
        for (int k=0; k<m*bse.nCheb*bse.Ampnumber; k++)
        {
            Gamma0.push_back(1.0);
        }

        eigenv= m_eigenvalue(Me,Gamma0,M);

        for(int i=0; i<m*bse.nCheb; i++)
        {
            for (int k=0; k<bse.Ampnumber; k++)
            {
                bse.Amp[k][i]=Gamma0[k+bse.Ampnumber*i];
            }
        }

    }

    bse.eigenv=eigenv;

    return eigenv;

}

//---------------------------------------------------------------------------------------------------------------------//
void pion_mass(bse_amplitude &bse, quark_prop &quark, double Mstart, double Mend, double &Mresult, string rundef, int PauliVillar)
{

    double M=Mstart;
    complex<double> saveeign[3];
    complex<double> saveM[3];
    //double savslope;
    Mresult=0.0;

    //savslope=0.0;
    cout<<endl<<"|Starting interpolation procedure for pion mass.........|"<<endl;
    cout<<"Mstart="<<Mstart<<" , Mend="<<Mend<<endl;

    int flaggy=1;
    int flaggy2=0;
    //int flaggy3=0;
    double mcounter=0.0;
    while(flaggy>=1)
    {
        for(int qs=0; qs<3; qs++)
        {

            if(qs<2){M=Mstart+qs*(Mend-Mstart);}
            else{M=Mresult;}
            saveM[qs]=M;


            if(flaggy2==1 && qs==0 || flaggy2==2 && qs==1)
            {saveeign[qs]=saveeign[2]; cout<<"ALREADY CALC: EV="<<saveeign[qs]<<"......M="<<M<<endl;}
            else if(flaggy2==1 && qs==1 || flaggy2==2 && qs==0)
            {saveeign[qs]=saveeign[qs]; cout<<"ALREADY CALC: EV="<<saveeign[qs]<<"......M="<<M<<endl;}
            else
            {
                //Calculation of eigenvalue
               saveeign[qs]=BSA_propagator(bse,quark, M,1,rundef, PauliVillar,1);
               bse.Mbse=M;


            }

           if(qs==1)
           {   double slope=(real(saveeign[1])-real(saveeign[0]))/(Mend-Mstart);
               double bg=real(saveeign[0])-slope*Mstart;
               Mresult=(1.0-bg)/slope;
               cout<<"INTERPOLATION: "<<slope<<"*M+"<<bg<<" between Ms: "<<Mstart<<" & Me: "<<Mend<<" ,   MRESULT: "<<Mresult<<"    : "<<mcounter<<endl;
               //if(flaggy3=0){flaggy3=1; savslope=(real(saveeign[1])-real(saveeign[0]))/(Mend*Mend-Mstart*Mstart);}
               if(Mresult<0){cout<<"The value of the Mass we would need to calculate is below 0! ABORTED"<<endl; return;}
           }

           double eps4=1e-6;

           if(qs==2)
           {
               double val=real(saveeign[2])-1.0;
               if(val>=eps4){Mend=Mresult;flaggy2=2;}
               else if(val<=-eps4){Mstart=Mresult;flaggy2=1;}
               else{flaggy=0;}
           }

           if(qs==2 && Mresult<0){cout<<"You're borders make no sense! Mstart has to be smaller or not enough GP. ABORTED"<<endl; return;}

       }
       mcounter++;
   }

    cout<<"Calcuations Done!!!! Steps:	"<<mcounter<<"  , -----------------> Results: Ev "<<saveeign[2]<<" ,M "<<saveM[2]<<endl<<endl;

    return;
}

//---------------------------------------------------------------------------------------------------------------------//
void bs_quarkmass(bse_amplitude &bse, quark_prop &quark, double Mbs, double &mquark, string rundef, int PauliVillar, double diff, double IRcutoff, double UVcutoff)
{

    double mstart=Mbs*Mbs/pow(1.9874,2)-diff; double mend=Mbs*Mbs/pow(1.9874,2)+diff;
    if(Mbs>1.0){mstart=0.75041; mend=0.8;}
    double m=mstart;
    complex<double> saveeign[3];
    complex<double> saveM[3];
    double saveslope;
    mquark=0.0;
    bse.Mbse=Mbs;

    cout<<endl<<"|Starting interpolation procedure for quark mass.........|"<<endl;
    cout<<"mstart="<<mstart<<" , mend="<<mend<<endl;

    int flaggy=1; //flag for stoppin gor persuring the iteration
    int flaggy2=0; //flag for remebering and using the result form the round before
    //int flaggy3=0;
    double mcounter=0.0;
    while(flaggy>=1)
    {
        for(int qs=0; qs<3; qs++)
        {

            if(qs<2){m=mstart+qs*(mend-mstart);}
            else{m=mquark;}
            saveM[qs]=m;


            if(flaggy2==1 && qs==0 || flaggy2==2 && qs==1)
            {saveeign[qs]=saveeign[2]; cout<<"ALREADY CALC: EV="<<saveeign[qs]<<"......M="<<m<<endl;}
            else if(flaggy2==1 && qs==1 || flaggy2==2 && qs==0)
            {saveeign[qs]=saveeign[qs]; cout<<"ALREADY CALC: EV="<<saveeign[qs]<<"......M="<<m<<endl;}
            else
            {
                //Calculation of eigenvalue
               quark.calc(1e-8,1,m,PauliVillar, IRcutoff, UVcutoff);
               //if(mcounter==0){saveeign[qs]=BSA_propagator(bse,quark, Mbs,1,rundef, PauliVillar,1);}
//               else{saveeign[qs]=BSA_propagator(bse,quark, Mbs,0,rundef, PauliVillar,0);}
               saveeign[qs]=BSA_propagator(bse,quark, Mbs,1,rundef, PauliVillar,1);



            }

           if(qs==1)
           {   double slope=(real(saveeign[1])-real(saveeign[0]))/(mend-mstart);
               double bg=real(saveeign[0])-slope*mstart;
               saveslope=slope;
              mquark=(1.0-bg)/slope;
               cout<<endl<<"INTERPOLATION: "<<slope<<"*M+"<<bg<<" between ms: "<<mstart<<" & me: "<<mend<<" ,   mRESULT: "<<mquark<<"    : "<<mcounter<<endl<<endl;
               //if(flaggy3=0){flaggy3=1; savslope=(real(saveeign[1])-real(saveeign[0]))/(Mend*Mend-Mstart*Mstart);}
               if(mquark<0){cout<<"The value of the Mass we would need to calculate is below 0! ABORTED"<<endl; return;}
           }

           double eps4=1e-6;

           if(qs==2)
           {
               double val=real(saveeign[2])-1.0;
               if(val>=eps4 && saveslope >= 0){mend=mquark;flaggy2=2;}
               else if(val<=-eps4 && saveslope >= 0 ){mstart=mquark;flaggy2=1;}
               else if(val>=eps4 && saveslope < 0){mstart=mquark;flaggy2=1;}
               else if(val<=-eps4 && saveslope < 0 ){mend=mquark;flaggy2=2;}
               else{flaggy=0;}
           }

           if(qs==2 && mquark<0){cout<<"You're borders make no sense! Mstart has to be smaller or not enough GP. ABORTED"<<endl; return;}
           cout<<endl;

       }
       mcounter++;
   }

    cout<<"Calcuations Done!!!! Steps:	"<<mcounter<<"  , -----------------> Results: Ev "<<saveeign[2]<<" ,mq "<<saveM[2]<<endl<<endl;

    return;
}
//---------------------------------------------------------------------------------------------------------------------//
void BSE_parameter(bse_amplitude &bse)
{
    complex<double> x;
    for(int i=0; i<bse.nz; i++)
    {
        for(int j=0; j<bse.m; j++)
        {
            x = bse.p2[j]/pow(0.7,2);

            bse.Amp[0][j+bse.m*i] = 0.85*exp(-0.7*x) + 0.15/pow((1.0+x),1.2);
            bse.Amp[1][j+bse.m*i] = -1.0*(-0.46*exp(-0.95*x) - 0.02/pow((1.0+x),1.2));
            bse.Amp[2][j+bse.m*i] = -1.0*(-0.65*exp(-1.4*x) - 0.02/pow((1.0+x),2.0));
            bse.Amp[3][j+bse.m*i] = 0.5*(0.31*exp(-1.6*x) + 0.015/pow((1.0+x),2.0));

            bse.Amp[0][j+bse.m*i] = bse.Amp[0][j+bse.m*i]*9.063112048;
            bse.Amp[1][j+bse.m*i] = bse.Amp[1][j+bse.m*i]*9.063112048;
            bse.Amp[2][j+bse.m*i] = bse.Amp[2][j+bse.m*i]*9.063112048;
            bse.Amp[3][j+bse.m*i] = bse.Amp[3][j+bse.m*i]*9.063112048;

        }
    }

    bse.Mbse=0.137;


}

//---------------------------------------------------------------------------------------------------------------------//
void BSE_interpolator(bse_amplitude &bseold, bse_amplitude &bsenew)
{
    int a,b, m1,n1, m2, n2;
    if(bseold.nCheb==0){m1=bseold.m; n1=bseold.nz; a=m1*n1;}else{m1=bseold.m; n1=bseold.nCheb; a=m1*n1;}
    if(bsenew.nCheb==0){m2=bsenew.m; n2=bsenew.nz; a=m2*n2;}else{m2=bsenew.m; n2=bsenew.nCheb; a=m2*n2;}


    vecd q1(a), q2(b);
    vecd E1(a), F1(a);
    vecd G1(a), H1(a);
    vecd E2(b), F2(b);
    vecd G2(b), H2(b);


    for(int i=0; i<a; i++)
    {
        q1[i]=real(bseold.p2[i]);
        E1[i]= real(bseold.Amp[0][i]);
        F1[i]=real(bseold.Amp[1][i]);
        G1[i]=real(bseold.Amp[2][i]);
        H1[i]=real(bseold.Amp[3][i]);

    }

    for(int i=0; i<b; i++)
    {
         q2[i]=real(bsenew.p2[i]);
    }

    Interpol splinetestE;
    splinetestE.spline_update_all(q1,E1);
    splinetestE.spline_interpol(q2,E2);

    Interpol splinetestF;
    splinetestF.spline_update_all(q1,F1);
    splinetestF.spline_interpol(q2,F2);

    Interpol splinetestG;
    splinetestG.spline_update_all(q1,G1);
    splinetestG.spline_interpol(q2,G2);

    Interpol splinetestH;
    splinetestH.spline_update_all(q1,H1);
    splinetestH.spline_interpol(q2,H2);

    for(int i=0;i<b;i++)
    {
        bsenew.Amp[0][i]= E2[i];
        bsenew.Amp[1][i]= F2[i];
        bsenew.Amp[2][i]= G2[i];
        bsenew.Amp[3][i]= H2[i];
    }

    bsenew.Mbse=bseold.Mbse;
    bsenew.eigenv=bseold.eigenv;

}
//---------------------------------------------------------------------------------------------------------------------//

//---------------------------------------------------------------------------------------------------------------------//
void bs_amplitude_calc(bse_amplitude &bse, quark_prop & quarkr,double &Mresult2, double &quarkmass, string rundef, string q_data_name, int PauliVillartag, int round,double diff, double IRcutoff, double UVcutoff, int BSEcalctag, double &Resultfpion )
{
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    // BSA: quark with momentum form BSA & A,B function------------------------------------------------------------------------------
//    quark_prop quarkBSE(mDec,nQuark);
//    if(!quarkBSE.init_grid(-1.0,1.0,decay_grid)){cout<<"TESTING.cpp :: ABORTED PROCESS at BSEquark init!"<<endl;return 0;}
//    quarkBSE.spline_interp(quarkr);                //Interpolation for quark on real axis at different momentum pionts quarkBSE.q2[i]
//    quarkBSE.write(2,rundef);


    complex<double> Resultnorm;
    complex<double> Resultev;
    complex<double> Resultpion;

    //Quark propagator
    if(round==0){cout<<"BSE.h::bs_ampltide_calc :: What about the quark propagator? ABORTED"<<endl; return;}
    if(round>1)
    {
        cout<<"_______|ReadInn the real quark propagator|________"<<endl;
        quarkr.readInn(1,-1.0,1.0,q_data_name);
        quarkmass=quarkr.mass;
        quarkr.write(1,"test_");
    }
    if(round==1)
    {
        cout<<"_______|Calculating the real quark propagator|________"<<endl;
        quarkr.calc(1e-8,1,quarkmass,PauliVillartag, IRcutoff, UVcutoff);
        quarkr.write(1,q_data_name);
    }

    cout<<"___|BSE|______________:"<<endl;

    //BSE amplitude calculation
    if(0<BSEcalctag && BSEcalctag<5)
    {

        if(BSEcalctag==1 )
        {
            cout<<"_______|Calculating EV for PMass all amplitudes for a given m and given Mbs..|_________"<<endl;
            Resultev=BSA_propagator(bse, quarkr, bse.Mbse,1,rundef, PauliVillartag,1);
            cout<<"-----------------This is the RESULT: ev="<<setprecision(10)<<Resultev<<endl;
            if(bse.nCheb==0){bse.write(2,2,rundef);}else{bse.write(2,3,rundef);}


        }
        else if (BSEcalctag==2)
        {
            cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;
            cout<<"________|Calculating BS amplitude for fixed mq and extrapolating for Mbs...|___________"<<endl;

            pion_mass(bse,quarkr, 0.137, 0.142, Mresult2,rundef,PauliVillartag);


            if(bse.nCheb==0){bse.write(2,2,rundef);}else{bse.write(2,3,rundef);}
            cout<<"The Mass that got extracted was: M="<<Mresult2<<endl;
            cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;
            cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;

        }
        else if (BSEcalctag==3) //Uses Gernots Parameterization
        {
            cout<<"_______|Pramaterization for BSE amplitudes|_________"<<endl;
            BSE_parameter(bse);
            bse.write(6,2,rundef);
            bse.fpi=0.0928;
            cout<<"The Parameter are inn, with Mbse="<<Mresult2<<" , BSE[0]="<<bse.Amp[0][0]<<" , fpion="<<bse.fpi<<endl;

        }
        else if (BSEcalctag==4)
        {
            //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
            cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;
            cout<<"________|Calculating BS amplitude with Mbs and extraploating the quark mass...|___________"<<endl;

            bs_quarkmass(bse,quarkr,bse.Mbse, quarkmass,rundef, PauliVillartag, diff, IRcutoff, UVcutoff );


            quarkr.write(1,q_data_name);
            if(bse.nCheb==0){bse.write(2,2,rundef);}else{bse.write(2,3,rundef);}
            cout<<"The Mass that got extracted was: mq="<<quarkr.mass<<" with Mbs="<<Mresult2<<endl;
            cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;
            cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;

        }

        //Normalization (only if not using the parameterization)
        if(BSEcalctag!=3) // && bse.Ampnumber==4)
        {
            cout<<"_____|Calculating the normalization for the Pmass...|________"<<endl;
            double epsilon=1e-6; if(Mresult2<=1e-3){epsilon=1e-9;cout<<"Normalization in Chiral limit: on."<<endl;}
            Resultnorm=BSE_derivative(Mresult2, bse, quarkr,rundef,Resultpion, epsilon,PauliVillartag);
            bse.fpi=real(Resultpion);
            if(bse.nCheb==0){bse.write(5,2,rundef);}else{bse.write(5,3,rundef);}

            cout<<"The Normalization: N= "<<Resultnorm<<endl;
            cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;

        }
        else{cout<<"The Normalization was not calcluated since the Ampnumber<4 and this is not implemented or because not needed BSEcalctag=3 ? or because nCheb="<<bse.nCheb<<endl;}



    }else if (BSEcalctag==0) //ReadInn, saved it somehwere?
    {


        //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
        cout<<"_______|ReadInn BSE amplitudes|_________"<<endl;
        bse.readInn(5,2,rundef);
        Mresult2=bse.Mbse;
        //bse.write(5,2);
        cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl<<endl;

    }else if (BSEcalctag>4){ if(BSEcalctag==5){cout<<"BSE.h::bs_ampltide_calc ::Chi will be used"<<endl;}else{cout<<"BSE.h::bs_ampltide_calc :: BSE amplitude was not calculated nor ReadInn."<<endl;} return;}

    Resultfpion=bse.fpi;


}

//---------------------------------------------------------------------------------------------------------------------//
