/*
 * BSEnorm.h
 *
 * calculation of the Normalization for the BSA, and the pion decay constant
 *
 * ---------------------------------------------------------------------
 * 01.08.15 Esther Weil
 *
 *Content: normfuncion_BSe, derivative('main_BSE' function), pion_decay, chiral_condensate
 * ---------------------------------------------------------------------
 *
 *
 * FORM CODE: norm_pion_decay
 */



//---------------------------------------------------------------------------------------------------------------------------------//
//----------------------------------------------------------------------------------------------------------------------//
double Cheby(int index, double z)
{
    double Chebychev= 0.0;
    if(index==0){Chebychev=1.0/sqrt(2.0);}
    else{Chebychev=cos(double(index)*acos(z));}

    return Chebychev;
}

//----------------------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------------------------------------//
complex<double> pion_decay(double M, double Z2, bse_amplitude &bse,VectorXcd &Adsep, VectorXcd &Adsem, VectorXcd &Bdsep, VectorXcd &Bdsem, complex<double> &resultpi)
{

    complex<double> sum=0.0;
    complex<double> fpi=0.0;
    complex<double> sum2=0.0;
    complex<double> rpi=0.0;
    int numAm=bse.Ampnumber;

    four_vec P;
    imag(P.inhalt[3])=M;
    complex<double> P_P=P*P;

    complex<double> Vorfac=(1.0/(Pi*Pi*Pi*8.0))*(Z2/P_P)*3.0;


    sum=0.0;
    for(int i=0; i<bse.nz; i++)
    {
        for(int j=0; j<bse.m; j++)
        {
            complex<double> E,F,G,H=0;
            if(bse.nCheb==0)
            {
                E=bse.Amp[0][j+i*bse.m];
                if(numAm>1){ F=bse.Amp[1][j+i*bse.m];}
                if(numAm>2){ G=bse.Amp[2][j+i*bse.m];}
                if(numAm>3){ H=bse.Amp[3][j+i*bse.m];}
            }else
            {

                for(int i_cheb=0; i_cheb<bse.nCheb; i_cheb++)
                {
                    E+=bse.Amp[0][j+i_cheb*bse.m]*Cheby(i_cheb,bse.zqp[i]);
                    if(numAm>1){ F+=bse.Amp[1][j+i_cheb*bse.m]*Cheby(i_cheb,bse.zqp[i]);}
                    if(numAm>2){ G+=bse.Amp[2][j+i_cheb*bse.m]*Cheby(i_cheb,bse.zqp[i]);}
                    if(numAm>3){ H+=bse.Amp[3][j+i_cheb*bse.m]*Cheby(i_cheb,bse.zqp[i]);}
                }

            }



            for(int l=0; l<bse.ny; l++)
            {

                four_vec q, q_1, q_2;
                complex<double> q_q, L, kp2, km2, P_q;
                complex<double> Ap=Adsep(j+i*bse.m);
                complex<double> Am=Adsem(j+i*bse.m);
                complex<double> Bp=Bdsep(j+i*bse.m);
                complex<double> Bm=Bdsem(j+i*bse.m);

                q.inhalt[0]=0.0; q.inhalt[1]=sqrt(bse.p2[j])*sqrt(1.0-bse.zqp[i]*bse.zqp[i])*sqrt(1.0-bse.zy[l]*bse.zy[l]);
                q.inhalt[2]=sqrt(bse.p2[j])*bse.zy[l]*sqrt(1.0-bse.zqp[i]*bse.zqp[i]); q.inhalt[3]=sqrt(bse.p2[j])*bse.zqp[i];

                q_1=q+bse.etabse*P;
                q_2=q-(1.0-bse.etabse)*P;

                q_q=bse.p2[j];
                kp2=q_1*q_1;
                km2=q_2*q_2;
                P_q=P*q;

                L=1.0/((kp2*Ap*Ap+Bp*Bp)*(km2*Am*Am+Bm*Bm));

                              //#########################################################################################

                                        fpi =
                                               + H * ( 8.E+0*P_P*q_q*Am*Bp + 8.E+0*P_P*q_q*Ap*Bm - 8.E+0*pow(
                                                 P_q,2)*Am*Bp - 8.E+0*pow(P_q,2)*Ap*Bm );

                                              fpi +=  + G * ( 4.E+0*P_P*P_q*q_q*Ap*Am - 8.E+0*P_P*P_q*q_q*eta*
                                                 Ap*Am + 4.E+0*P_P*pow(P_q,2)*eta*Ap*Am - 4.E+0*P_P*pow(P_q,2)*
                                                 pow(eta,2)*Ap*Am - 4.E+0*pow(P_q,2)*q_q*Ap*Am - 4.E+0*pow(
                                                 P_q,2)*Bp*Bm );

                                              fpi +=  + F * ( 4.E+0*P_P*P_q*Ap*Am - 8.E+0*P_P*P_q*eta*Ap*Am +
                                                 4.E+0*P_P*q_q*Ap*Am - 4.E+0*P_P*Bp*Bm + 4.E+0*pow(P_P,2)*eta*
                                                 Ap*Am - 4.E+0*pow(P_P,2)*pow(eta,2)*Ap*Am - 8.E+0*pow(P_q,2)*
                                                 Ap*Am );

                                              fpi +=  + E * ( 4.E+0*P_P*Am*Bp - 4.E+0*P_P*eta*Am*Bp + 4.E+0*P_P
                                                 *eta*Ap*Bm - 4.E+0*P_q*Am*Bp + 4.E+0*P_q*Ap*Bm );

                              //#########################################################################################

                                              rpi =
                                                    + H * (  - 8.E+0*P_P*q_q*Ap*Am + 8.E+0*pow(P_q,2)*Ap*Am );

                                                   rpi +=  + G * (  - 4.E+0*P_q*q_q*Am*Bp + 4.E+0*P_q*q_q*Ap*Bm +
                                                      4.E+0*pow(P_q,2)*Am*Bp - 4.E+0*pow(P_q,2)*eta*Am*Bp + 4.E+0*
                                                      pow(P_q,2)*eta*Ap*Bm );

                                                   rpi +=  + F * ( 4.E+0*P_P*Am*Bp - 4.E+0*P_P*eta*Am*Bp + 4.E+0*P_P
                                                      *eta*Ap*Bm - 4.E+0*P_q*Am*Bp + 4.E+0*P_q*Ap*Bm );

                                                   rpi +=  + E * ( 4.E+0*Bp*Bm - 4.E+0*P_P*eta*Ap*Am + 4.E+0*P_P*
                                                      pow(eta,2)*Ap*Am - 4.E+0*P_q*Ap*Am + 8.E+0*P_q*eta*Ap*Am +
                                                      4.E+0*q_q*Ap*Am );

                             //#########################################################################################



                sum+= Vorfac*L*fpi*bse.p2[j]*bse.p2[j]*0.5*sqrt(1.0-bse.zqp[i]*bse.zqp[i])*bse.w_z[i]*bse.radw[j]*bse.w_y[l];
                sum2+= Vorfac*L*rpi*bse.p2[j]*bse.p2[j]*0.5*sqrt(1.0-bse.zqp[i]*bse.zqp[i])*bse.w_z[i]*bse.radw[j]*bse.w_y[l];




            }


        }
    }

    cout<<"The Pion decay constant has been calculated! And fpi= "<<sum<<endl<<"The difference between 0.092 GeV: "<<abs(0.092/sum)
       <<endl<<"_______________________________________"<<endl<<endl;
    //cout<<"The other pion residue follows as!  rpi= "<<sum2<<endl<<"_______________________________________"<<endl<<endl;

    resultpi=sum2;
    return sum;


}
//---------------------------------------------------------------------------------------------------------------------------------//
complex<double> chiral_condensate(string rundef)
{

    complex<double> sum=0.0;
    double Z4; double mq=0.1;

    quark_prop quark(70,20);
//    quark.calc(1e-8, 1,0.08,0,1e-2,1e2);
    quark.calc(1e-8, 1,mq,0,1e-2,1e2);
    Z4=quark.Zm*quark.Z2/mq;
    cout<<"Z4: "<<Z4<<endl;

    quark.calc(1e-8,1,0.0000001,0,1e-2,1e2);
    //quark.write(5,rundef);

    complex<double> Vorfac=(1.0/(Pi*Pi*8.0))*Z4*3;


    sum=0.0;
    for(int i=0; i<quark.m; i++)
    {

//                sum+= Vorfac*4.0*quark.B[i]*quark.p2[i]*quark.p2[i]*0.5*quark.radw[i];
                sum+= Vorfac*4.0*quark.B[i]*quark.p2[i]*quark.p2[i]*0.5*quark.radw[i]*1.0/(pow(quark.A[i],2.0)*quark.p2[i]+pow(quark.B[i],2.0));

    }

    cout<<"The chiral condensate has been calculated "<<sum<<"   Compare it to rp0: "<<1.0/0.091*sum<<endl<<"_______________________________________"<<endl<<endl;
    return sum;


}
//---------------------------------------------------------------------------------------------------------------------------------//
complex<double> normfunction_BSE (double M, bse_amplitude &bse, double Mfix, VectorXcd &Adsep, VectorXcd &Adsem, VectorXcd &Bdsep, VectorXcd &Bdsem)
{

  double Vorfac=(1.0/(Pi*Pi*Pi*8.0))*6.0;
  complex<double> sum=0.0;
  complex<double> norm=0.0;
  int numAm=bse.Ampnumber;

  four_vec P;
  imag(P.inhalt[3])=M;

  four_vec Q;
  imag(Q.inhalt[3])=Mfix;

  sum=0.0;
  for(int i=0; i<bse.nz; i++)
  {
      for(int j=0; j<bse.m; j++)
      {
          complex<double> E,F,G,H=0;
          if(bse.nCheb==0)
          {
              E=bse.Amp[0][j+i*bse.m];
              if(numAm>1){ F=bse.Amp[1][j+i*bse.m];}
              if(numAm>2){ G=bse.Amp[2][j+i*bse.m];}
              if(numAm>3){ H=bse.Amp[3][j+i*bse.m];}
          }else
          {

              for(int i_cheb=0; i_cheb<bse.nCheb; i_cheb++)
              {
                  E+=bse.Amp[0][j+i_cheb*bse.m]*Cheby(i_cheb,bse.zqp[i]);
                  if(numAm>1){ F+=bse.Amp[1][j+i_cheb*bse.m]*Cheby(i_cheb,bse.zqp[i]);}
                  if(numAm>2){ G+=bse.Amp[2][j+i_cheb*bse.m]*Cheby(i_cheb,bse.zqp[i]);}
                  if(numAm>3){ H+=bse.Amp[3][j+i_cheb*bse.m]*Cheby(i_cheb,bse.zqp[i]);}
              }

          }


          for(int l=0; l<bse.ny; l++)
          {

              four_vec q, q_1, q_2;
              complex<double> q1_q2, q_q2, q_q1, q_Qi, q2_Qi, q1_Qi, Qi_Qi, q_q, Y, L, kp2, km2, P_P, P_Qi, P_q;
              complex<double> Ap=Adsep(j+i*bse.m);
              complex<double> Am=Adsem(j+i*bse.m);
              complex<double> Bp=Bdsep(j+i*bse.m);
              complex<double> Bm=Bdsem(j+i*bse.m);

              q.inhalt[0]=0.0; q.inhalt[1]=sqrt(bse.p2[j])*sqrt(1.0-bse.zqp[i]*bse.zqp[i])*sqrt(1.0-bse.zy[l]*bse.zy[l]);
              q.inhalt[2]=sqrt(bse.p2[j])*bse.zy[l]*sqrt(1.0-bse.zqp[i]*bse.zqp[i]); q.inhalt[3]=sqrt(bse.p2[j])*bse.zqp[i];

              q_1=q+bse.etabse*P;
              q_2=q-(1.0-bse.etabse)*P;

              q1_q2=q_1*q_2;
              q_Qi=Q*q;
              q1_Qi=Q*q_1;
              q2_Qi=Q*q_2;
              q_q2=q*q_2;
              q_q1=q*q_1;
              Qi_Qi=Q*Q;
              q_q=bse.p2[j];
              kp2=q_1*q_1;
              km2=q_2*q_2;
              P_P=P*P;
              P_Qi=P*Q;
              P_q=P*q;

              L=1.0/((kp2*Ap*Ap+Bp*Bp)*(km2*Am*Am+Bm*Bm));

                //######################################################################################

                          norm =
                                + pow(H,2) * (  - 1.6E+1*P_P*q_q*Qi_Qi*eta*Ap*Am + 1.6E+1*P_P*
                                  q_q*Qi_Qi*pow(eta,2)*Ap*Am + 1.6E+1*P_P*pow(q_Qi,2)*eta*Ap*Am
                                   - 1.6E+1*P_P*pow(q_Qi,2)*pow(eta,2)*Ap*Am - 6.4E+1*P_q*P_Qi*
                                  q_Qi*eta*Ap*Am + 6.4E+1*P_q*P_Qi*q_Qi*pow(eta,2)*Ap*Am +
                                  1.6E+1*P_q*q_q*Qi_Qi*Ap*Am - 3.2E+1*P_q*q_q*Qi_Qi*eta*Ap*Am -
                                  1.6E+1*P_q*pow(q_Qi,2)*Ap*Am + 3.2E+1*P_q*pow(q_Qi,2)*eta*Ap*
                                  Am + 3.2E+1*pow(P_q,2)*Qi_Qi*eta*Ap*Am - 3.2E+1*pow(P_q,2)*
                                  Qi_Qi*pow(eta,2)*Ap*Am + 3.2E+1*pow(P_Qi,2)*q_q*eta*Ap*Am -
                                  3.2E+1*pow(P_Qi,2)*q_q*pow(eta,2)*Ap*Am + 1.6E+1*q_q*pow(
                                  q_Qi,2)*Ap*Am + 1.6E+1*q_q*Qi_Qi*Bp*Bm - 1.6E+1*pow(q_q,2)*
                                  Qi_Qi*Ap*Am - 1.6E+1*pow(q_Qi,2)*Bp*Bm );

                               norm +=  + G*H * (  - 1.6E+1*P_q*pow(q_Qi,2)*Am*Bp + 1.6E+1*P_q*
                                  pow(q_Qi,2)*eta*Am*Bp + 1.6E+1*P_q*pow(q_Qi,2)*eta*Ap*Bm +
                                  1.6E+1*P_Qi*q_q*q_Qi*Am*Bp - 1.6E+1*P_Qi*q_q*q_Qi*eta*Am*Bp -
                                  1.6E+1*P_Qi*q_q*q_Qi*eta*Ap*Bm );

                               norm +=  + pow(G,2) * (  - 4.E+0*P_P*q_q*pow(q_Qi,2)*eta*Ap*Am +
                                  4.E+0*P_P*q_q*pow(q_Qi,2)*pow(eta,2)*Ap*Am + 4.E+0*P_q*q_q*
                                  pow(q_Qi,2)*Ap*Am - 8.E+0*P_q*q_q*pow(q_Qi,2)*eta*Ap*Am +
                                  8.E+0*pow(P_q,2)*pow(q_Qi,2)*eta*Ap*Am - 8.E+0*pow(P_q,2)*pow(
                                  q_Qi,2)*pow(eta,2)*Ap*Am - 4.E+0*q_q*pow(q_Qi,2)*Bp*Bm - 4.E+0
                                  *pow(q_q,2)*pow(q_Qi,2)*Ap*Am );

                               norm +=  + F*H * (  - 1.6E+1*P_q*Qi_Qi*Am*Bp + 1.6E+1*P_q*Qi_Qi*
                                  eta*Am*Bp + 1.6E+1*P_q*Qi_Qi*eta*Ap*Bm + 1.6E+1*P_Qi*q_Qi*Am*
                                  Bp - 1.6E+1*P_Qi*q_Qi*eta*Am*Bp - 1.6E+1*P_Qi*q_Qi*eta*Ap*Bm
                                   + 1.6E+1*q_q*Qi_Qi*Am*Bp + 1.6E+1*q_q*Qi_Qi*Ap*Bm - 1.6E+1*
                                  pow(q_Qi,2)*Am*Bp - 1.6E+1*pow(q_Qi,2)*Ap*Bm );

                               norm +=  + F*G * (  - 8.E+0*P_P*pow(q_Qi,2)*eta*Ap*Am + 8.E+0*P_P
                                  *pow(q_Qi,2)*pow(eta,2)*Ap*Am + 1.6E+1*P_q*P_Qi*q_Qi*eta*Ap*Am
                                   - 1.6E+1*P_q*P_Qi*q_Qi*pow(eta,2)*Ap*Am + 8.E+0*P_Qi*q_q*q_Qi
                                  *Ap*Am - 1.6E+1*P_Qi*q_q*q_Qi*eta*Ap*Am - 8.E+0*q_q*pow(
                                  q_Qi,2)*Ap*Am - 8.E+0*pow(q_Qi,2)*Bp*Bm );

                               norm +=  + pow(F,2) * (  - 4.E+0*P_P*Qi_Qi*eta*Ap*Am + 4.E+0*P_P*
                                  Qi_Qi*pow(eta,2)*Ap*Am - 4.E+0*P_q*Qi_Qi*Ap*Am + 8.E+0*P_q*
                                  Qi_Qi*eta*Ap*Am + 8.E+0*P_Qi*q_Qi*Ap*Am - 1.6E+1*P_Qi*q_Qi*eta
                                  *Ap*Am + 8.E+0*pow(P_Qi,2)*eta*Ap*Am - 8.E+0*pow(P_Qi,2)*pow(
                                  eta,2)*Ap*Am + 4.E+0*q_q*Qi_Qi*Ap*Am - 8.E+0*pow(q_Qi,2)*Ap*Am
                                   - 4.E+0*Qi_Qi*Bp*Bm );

                               norm +=  + E*H * ( 1.6E+1*P_q*q_Qi*Ap*Am - 1.6E+1*P_Qi*q_q*Ap*Am
                                   );

                               norm +=  + E*G * ( 8.E+0*P_q*q_Qi*Am*Bp - 8.E+0*P_q*q_Qi*eta*Am*
                                  Bp + 8.E+0*P_q*q_Qi*eta*Ap*Bm - 8.E+0*q_q*q_Qi*Am*Bp + 8.E+0*
                                  q_q*q_Qi*Ap*Bm );

                               norm +=  + E*F * ( 8.E+0*P_Qi*Am*Bp - 8.E+0*P_Qi*eta*Am*Bp +
                                  8.E+0*P_Qi*eta*Ap*Bm - 8.E+0*q_Qi*Am*Bp + 8.E+0*q_Qi*Ap*Bm );

                               norm +=  + pow(E,2) * ( 4.E+0*Bp*Bm - 4.E+0*P_P*eta*Ap*Am + 4.E+0
                                  *P_P*pow(eta,2)*Ap*Am - 4.E+0*P_q*Ap*Am + 8.E+0*P_q*eta*Ap*Am
                                   + 4.E+0*q_q*Ap*Am );

                    //######################################################################################




                 sum+= Vorfac*L*norm*bse.p2[j]*bse.p2[j]*0.5*sqrt(1.0-bse.zqp[i]*bse.zqp[i])*bse.w_z[i]*bse.radw[j]*bse.w_y[l];

          }


      }
  }

  //cout<<"The NormInteg has been calcuated! And sum= "<<sum<<endl<<"_______________________________________"<<endl<<endl;
  return sum;


}



//Main-part: usage of functions above
//---------------------------------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------------------------------------//
complex<double> BSE_derivative(double M, bse_amplitude &bse, quark_prop &quark, string rundef, complex<double> &piond, double epsilon, int PauliVillar)
{
    //double epsilon= 1e-6;
    complex<double> result;
    complex<double> result1;
    complex<double> result2;
    complex<double> Resultnorm;
    complex<double> rpi;
    complex<double> rp0;

    int m=bse.m; int nz=bse.nz;

    VectorXcd Adsep(m*nz);
    VectorXcd Adsem(m*nz);
    VectorXcd Bdsep(m*nz);
    VectorXcd Bdsem(m*nz);

    ComQuarkpm(m,nz,quark,bse.p2,bse.zqp,Adsep, Bdsep, 1, 0,"", sqrt(M*M+epsilon) , eta, PauliVillar);
    ComQuarkpm(m,nz,quark,bse.p2,bse.zqp,Adsem, Bdsem, 2, 0,"",sqrt(M*M+epsilon) , eta, PauliVillar);

    result1=normfunction_BSE(sqrt(M*M+epsilon),bse,M, Adsep, Adsem, Bdsep, Bdsem);

    ComQuarkpm(m,nz,quark,bse.p2,bse.zqp,Adsep, Bdsep, 1, 0,"", sqrt(M*M-epsilon) , eta, PauliVillar);
    ComQuarkpm(m,nz,quark,bse.p2,bse.zqp,Adsem, Bdsem, 2, 0,"",sqrt(M*M-epsilon) , eta, PauliVillar);

    result2=normfunction_BSE(sqrt(M*M-epsilon),bse,M, Adsep, Adsem, Bdsep, Bdsem);


    result= (result1-result2)/(2.0*epsilon);

    Resultnorm=1.0/sqrt(result);

    //Resultnorm=8.5;

    for(int i=0; i<bse.Ampnumber; i++)
    {
        if(bse.nCheb==0)
        {
            for(int j=0; j<m*nz; j++)
            {
                bse.Amp[i][j]=bse.Amp[i][j]*Resultnorm;
            }

        }else{

            for(int j=0; j<m*bse.nCheb; j++)
            {
                bse.Amp[i][j]=bse.Amp[i][j]*Resultnorm;
            }
        }


    }



    ComQuarkpm(m,nz,quark,bse.p2,bse.zqp,Adsep, Bdsep, 1, 0,"", M , eta,PauliVillar);
    ComQuarkpm(m,nz,quark,bse.p2,bse.zqp,Adsem, Bdsem, 2, 0,"", M , eta,PauliVillar);

    piond=pion_decay(M,quark.Z2,bse,Adsep, Adsem, Bdsep, Bdsem, rpi);

//    rp0=1.0/0.091*chiral_condensate(rundef);
//    rp0=chiral_condensate(rundef);

//    cout<<"Hier ist das Chirale Condensat: "<<rp0<<tab<<1.0/0.091*rp0<<endl;

    if(bse.nCheb!=0){Resultnorm=Resultnorm/sqrt(2.0);}

    return Resultnorm;

}


