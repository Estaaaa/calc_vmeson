//
// Created by esther on 02.12.16.
//

#ifndef FORMFACTOR_QUARKPHOTONOUTPUT_H
#define FORMFACTOR_QUARKPHOTONOUTPUT_H

void output_qphv_grid(string rundef, double M, double sig, grid g, int which_vertex)
{

    four_vec p,Q,P,loop, poutside;
    cdouble p_p, Q_Q, p_Q;

    P.inhalt[3]=ii*M;
    int n=g.np; int nz=g.nz; int ny=g.ny; int m=g.m;

    stringstream help;
    help<<which_vertex;
    string buffer = rundef+"quarkphoton_grid_"+help.str()+".txt";
    ofstream f(buffer.c_str());


//    f<<"#   pionts="<<n*m*nz*ny<<"  pionM:"<<M<<"   np="<<n<<" , nrad="<<m<<" , nz="<<nz<<" , ny="<<ny<<endl;
//    f<<"#--------------------------------------------------# "<<endl;
//    f<<"#real(p)    imag(p) real(Q) imag(Q)"<<endl;

    f<<endl<<tab<<tab<<m*nz*ny*n<<endl<<endl<<tab<<tab<<"k2"<<tab<<tab<<tab<<"kQ"<<tab<<tab<<tab<<"Q2"<<endl<<endl;

    for (int j = 0; j < m; ++j) {
        for (int k = 0; k < nz; ++k) {
            for (int l = 0; l < ny; ++l) {


                loop.spherical_fv(g.q2[j],g.z[k], g.y[l]);

                for (int i = 0; i < n; ++i)
                {
                    poutside=g.p_vector[i];

                    if(which_vertex==1)
                    {
                        Q=0.5*(poutside-P);
                        p=loop+sig*P+0.5*Q;

                    }else if(which_vertex==2)
                    {
                        Q=-0.5*(poutside+P);
                        p=loop-(1.0-sig)*P-0.5*Q;
                    }

                    p_p=p*p; Q_Q=Q*Q; p_Q=p*Q;
//                    f<<real(p_p)<<tab<<imag(p_p)<<tab<<real(Q_Q)<<tab<<imag(Q_Q)<<endl;
                    f<<scientific<<tab<<real(p_p)<<tab<<imag(p_p)<<tab<<real(p_Q)<<tab<<imag(p_Q)<<tab<<real(Q_Q)<<endl;
                }


            }

        }

    }

    f.close();


}



#endif //FORMFACTOR_QUARKPHOTONOUTPUT_H
