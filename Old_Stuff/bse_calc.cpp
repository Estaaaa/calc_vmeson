#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <complex>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>

using namespace std;

#include "Toolbox/ToolHeader.h"


#define mo 0.0037
#define Mpion 0.139
#define Mv 0.769
#define sigma 0.5                   //decay
#define eta 0.5                     //pion
#define fpion 0.092
#define elm 1.6021766208e-19
#define alphae 1.0/137.036
//#define numAm 4                     //Im moment geht nur 1,4 da die write function nur die beiden hat!, ist aber drinne?

#define lam2 1000.0



#include "DSEHeads/DSEHeader.h"
#include "BSEHeads/BSEHeader.h"



#include "Toolbox/fourvec_ops.h"

//------------------------------------CALCULATION OF THE COMPLEX QUARK---------------------------------------------------//
int main (int argc, char * const argv[]) {

    clock_t start;
    float timet;
    time_t starting, dasende;
    time(&starting);

    int mQuark=150;
    int nQuark=32;
    string rundef ="data/output";

    int mBSA=20;
    int nzBSA=24;
    int nyBSA=12;

    double IRcutoff=1.0e-2;
    double UVcutoff=1.0e+2;

    int BSEcalctag = 1;
    int Chiraltag = 0;
    int PauliVillartag=0;
    double quarkmass=0.0037;
    double Mbse=0.1377;
    int round= 1;
    int AmpNumber=4;

    if(argc==4){cout<<"Reading Inn grid-points: "; mBSA=atoi(argv[1]); nzBSA=atoi(argv[2]); nyBSA=atoi(argv[3]);}
    if(argc==5){cout<<"Reading Inn grid-points: "; mBSA=atoi(argv[1]); nzBSA=atoi(argv[2]); nyBSA=atoi(argv[3]); rundef=rundef+"_"+argv[4]+"/";}
    if(argc==6){cout<<"Reading Inn grid-points: "; mBSA=atoi(argv[1]); nzBSA=atoi(argv[2]); nyBSA=atoi(argv[3]); rundef=rundef+"_"+argv[4]+"/"; BSEcalctag=atoi(argv[5]);}
    if(argc==7){cout<<"Reading Inn grid-points: "; mBSA=atoi(argv[1]); nzBSA=atoi(argv[2]); nyBSA=atoi(argv[3]); rundef=rundef+"_"+argv[4]+"/"; BSEcalctag=atoi(argv[5]);Chiraltag=atoi(argv[6]);}
    if(argc==8){cout<<"Reading Inn grid-points: "; mBSA=atoi(argv[1]); nzBSA=atoi(argv[2]); nyBSA=atoi(argv[3]); rundef=rundef+"_"+argv[4]+"/"; BSEcalctag=atoi(argv[5]);Chiraltag=atoi(argv[6]); PauliVillartag=atoi(argv[7]);}
    if(argc==9){cout<<"Reading Inn grid-points: "; mBSA=atoi(argv[1]); nzBSA=atoi(argv[2]); nyBSA=atoi(argv[3]); rundef=rundef+"_"+argv[4]+"/"; BSEcalctag=atoi(argv[5]);Chiraltag=atoi(argv[6]); PauliVillartag=atoi(argv[7]);
        quarkmass=atof(argv[8]);}
    if(argc==10){cout<<"Reading Inn grid-points: "; mBSA=atoi(argv[1]); nzBSA=atoi(argv[2]); nyBSA=atoi(argv[3]); rundef=rundef+"_"+argv[4]+"/"; BSEcalctag=atoi(argv[5]);Chiraltag=atoi(argv[6]);
        PauliVillartag=atoi(argv[7]); quarkmass=atof(argv[8]); Mbse=atof(argv[9]);}
    if(argc==11){cout<<"Reading Inn grid-points: "; mBSA=atoi(argv[1]); nzBSA=atoi(argv[2]); nyBSA=atoi(argv[3]); rundef=rundef+"_"+argv[4]+"/"; BSEcalctag=atoi(argv[5]);Chiraltag=atoi(argv[6]);
        PauliVillartag=atoi(argv[7]); quarkmass=atof(argv[8]); Mbse=atof(argv[9]); AmpNumber=atoi(argv[10]);}
    if(argc==12){cout<<"Reading Inn grid-points: "; mBSA=atoi(argv[1]); nzBSA=atoi(argv[2]); nyBSA=atoi(argv[3]); rundef=rundef+"_"+argv[4]+"/"; BSEcalctag=atoi(argv[5]);Chiraltag=atoi(argv[6]);
        PauliVillartag=atoi(argv[7]); quarkmass=atof(argv[8]); Mbse=atof(argv[9]); AmpNumber=atoi(argv[10]); round=atoi(argv[11]);}
    if(argc>12){cout<<"Wrong input paramters, Aborted"<<endl;}

    cout<<"Starting....."<<endl;
    cout<<"The Parameter of this session: m="<<mBSA<<" , ny="<<nyBSA<<" , nz="<<nzBSA<<"  , mBSE="<<Mbse<<"  , mq="<<quarkmass<<"  , #Amp="<<AmpNumber<<endl;
    cout<<"PauliVillar= "<<PauliVillartag<<" , Chiraltag= "<<Chiraltag<<" ,BSEcalctag= "<<BSEcalctag<<" , folder="<<atoi(argv[4])<<endl;
    cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;

    string q_data_name=rundef;


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //real QUARK propagator---------------------------------------------------------------------------------------------------------
    quark_prop quarkr(mQuark,nQuark);

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    // grid------------------------------------------------------------------------------
    grid bseg(mBSA,nzBSA,nyBSA);
    bseg.init(-1.0,1.0,1, 0, IRcutoff, UVcutoff);

    double diff=0.001;
    if(Chiraltag==1){Mbse=0.001; quarkmass=0.0;cout<<"The quark & bse are in chiral limit!"<<endl;}
    cout<<"The mass of the bound state in this session M="<<Mbse<<endl;


    // BSA amplitude
    bse_amplitude bse(Mbse, mBSA,nyBSA,nzBSA,0.5, AmpNumber,4);
    bse.gridinit(bseg);

    complex<double> Resultfpion;


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    // Calculation of the BSE amplitude and the quark-------------------------------------------------------------------------
    start = clock();

    bs_amplitude_calc(bse,quarkr, Mbse, quarkmass,  rundef, q_data_name,  PauliVillartag,  round, diff,  IRcutoff,  UVcutoff, BSEcalctag,  Resultfpion );

    timet = (float)(clock() - start) / CLOCKS_PER_SEC;
    cout <<"BSE time: "<<timet << endl;
    cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl<<endl;




time(&dasende); give_time(starting, dasende,"Der ganze Prozess ");


    return 0;


    //    for(int k=0; k<4; k++)
    //    {
    //        for(int j=0; j<4; j++)
    //        {
    //            double sum=0; int N=4; double Nh=4.0;
    //            for(int i=0; i<N; i++)
    //            {
    //                double zq=cos((i+0.5)*Pi/Nh);
    //                sum += Cheby(j,zq)*Cheby(k,zq)*(2.0/Nh);
    //            }

    //            cout<<j<<tab<<k<<tab<<sum<<endl;
    //        }

    //    }
}
 
