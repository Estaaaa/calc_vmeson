#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <complex>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>
#include "../../Dropbox/BETHESAL_VT2/Eigen/Dense"
#include "interpolation.hpp"                //CW Spline Interplolation: compile interpolation.cpp as well!
using namespace Eigen;

using namespace std;

#define mo 0.0037

#define Pi 3.141592653589793
#define epsi 1.0e-2
#define cut2 1.0e+6

#define Mpion 0.139
#define Mv 0.769
#define sigma 0.5                   //decay
#define eta 0.5                     //pion
#define fpion 0.092
#define elm 1.6021766208e-19
#define alphae 1.0/137.036
#define numAm 4                     //Im moment geht nur 1,4 da die write function nur die beiden hat!, ist aber drinne?

#define lam2 1000.0


#ifndef ii
#define ii std::complex<double> (0.,1.)
#endif

#ifndef i_
#define i_ std::complex<double> (0.,1.)
#endif

typedef complex<double> dcx;
typedef vector<dcx> vecdcx;
typedef vector<double> vecd;
typedef vector<vecdcx> vecvecdx;


#include "DSEclass.h"                    //includes "gaul.h", "fourvec,h" , spline-interpolation (interpolation.hpp before) AND four_vec operator definitions!!!!
#include "DSEcom.h"
#include"chauchyinterpol.h"
//#include "BSE.h"
//#include "BSEnorm.h"
//#include "2dinterpol.h"



four_vec operator+(four_vec k, four_vec p)
{
    four_vec result;
    for(int i =0; i<4; i++)
    {
        result.inhalt[i]=k.inhalt[i]+p.inhalt[i];
    }
    return result;
}

four_vec operator-(four_vec k, four_vec p)
{
    four_vec result;
    for(int i =0; i<4; i++)
    {
        result.inhalt[i]=k.inhalt[i]-p.inhalt[i];
    }
    return result;
}

four_vec operator*(double k, four_vec p)
{
    four_vec result;
    for(int i =0; i<4; i++)
    {
        result.inhalt[i]=k*p.inhalt[i];
    }
    return result;
}

four_vec operator*(four_vec p, double k)
{
    four_vec result;
    for(int i =0; i<4; i++)
    {
        result.inhalt[i]=p.inhalt[i]*k;
    }
    return result;
}


four_vec operator*(complex<double> k, four_vec p)
{
    four_vec result;
    for(int i =0; i<4; i++)
    {
        result.inhalt[i]=k*p.inhalt[i];
    }
    return result;
}

four_vec operator*( four_vec p, complex<double> k)
{
    four_vec result;
    for(int i =0; i<4; i++)
    {
        result.inhalt[i]=p.inhalt[i]*k;
    }
    return result;
}

complex<double> operator*(four_vec k, four_vec p)
{
    complex<double> result=0.0;
    for(int i =0; i<4; i++)
    {
        result += k.inhalt[i]*p.inhalt[i];
    }
    return result;
}


//------------------------------------INTERPOLATION OF THE COMPLEX QUARK---------------------------------------------------//
int main (int argc, char * const argv[]) {

    clock_t start;
    float timet;
    time_t starting, dasende;
    time(&starting);

    int mQuark=150;
    int nQuark=32;
    double quarkmass=mo;
    string q_data_name="data/input/";

    int PauliVillartag=1;
    int Chiraltag = 0;
    int round = 2;

    if(PauliVillartag==1){if(Chiraltag==0){q_data_name="data/input/PV_";}else{q_data_name="data/input/PV_CL_";}}
    else if(PauliVillartag==0){if(Chiraltag==0){q_data_name="data/input/";}else{q_data_name="data/input/CL_";}}
    else{cout<<"PauliVillartag is wrong! ABORTED"<<endl; return 0;}

    cout<<"Starting....."<<endl;

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //real QUARK propagator---------------------------------------------------------------------------------------------------------
    quark_prop quarkr(mQuark,nQuark);
    if(round==0){cout<<"What about the quark propagator? ABORTED"<<endl; return 0;}
    if(round==1)
    {
        cout<<"_______|Calculating the real quark propagator|________"<<endl;
        if(Chiraltag==1){quarkr.calc(1e-8,1,0,PauliVillartag);quarkmass=0.0;cout<<"quark prop is in chiral limit!"<<endl;}
        else{quarkr.calc(1e-8,1,mo,PauliVillartag);quarkmass=mo;}
        //cout<<"Real quark: m="<<mDSE<<" ,n="<<nDSE<<", mass="<<quarkmass<<endl;
        quarkr.write(1,q_data_name);
    }
    if(round>1)
    {
        cout<<"_______|ReadInn the real quark propagator|________"<<endl;
        quarkr.readInn(1,-1.0,1.0,q_data_name);
        quarkmass=quarkr.mass;
    }

     cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;



    int n_int2=1000; int n_int3=10; double M=sqrt(0.4); double Pr=sqrt(1000); double fac=1.0;
    chauchy_parabola(n_int2,n_int3,quarkr,PauliVillartag,"Chauchy_comquark",M,Pr, fac);


    complex<double> p=0.5;
    vecd t; vecd t_weight;
    VectorXcd A(n_int2+n_int3); VectorXcd B(n_int2+n_int3); int nn=n_int2+n_int3;
    chauchy_ReadInn("Chauchy_comquark", n_int2, n_int3, t, t_weight, A, B);

    VectorXcd f(n_int2+n_int3);
    complex<double> h;
    for(int i=0; i<n_int2; i++)
    {
//        h=ii*t[i]*0.01+t[i]*t[i]-0.25;
        h=ii*t[i]*fac*M+t[i]*t[i]-M*M;
        f(i)=exp(-h*h);
    }
    for(int i=n_int2; i<n_int3+n_int2; i++)
    {
//        h=1.0-0.25+ii*t[i]*0.01;
          h=Pr*Pr-M*M+ii*t[i]*M*Pr*fac;
        f(i)=exp(-h*h);
    }
    cout<<endl;
    complex<double> result;

    result=chauchy_interpol(p,f,t,t_weight,n_int2, n_int3, M, Pr, fac);

    cout<<"Das sind die Ergebnisse: f(x)="<<exp(-0.5*0.5)<<" , C(x)="<<result<<endl<<endl<<"-----------------------------------------"<<endl;

    //##########################################################################################################

    int nt=300;
    double xt[nt]; double wt[nt];
    gauleg(0.001,500,xt,wt,nt);
    VectorXcd An(nt+1); VectorXcd Bn(nt+1); vecdcx pp;
    for(int i=0; i<nt; i++)
    {
        pp.push_back(xt[i]);
    }
    pp.push_back(p);

    com_quark_chauchy_interpol("Chauchy_comquark",An, Bn, pp, M, Pr, fac);

    cout<<"Ergebnis für propagator: pp="<<pp[0]<<", A="<<An(0)<<" ,B="<<Bn(0)<<endl;

    string buffer2 ="data/interpol_test.txt";
    ofstream f2(buffer2.c_str());
    for(int i=0; i<pp.size(); i++)
    {
       f2<<real(pp[i])<<tab<<imag(pp[i])<<"\t"<<real(An(i))<<tab<<imag(An(i))<<tab<<real(Bn(i))<<imag(Bn(i))<<endl;
    }
    f2.close();




     //|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<<




time(&dasende); cout<<"Parabel berechnet in: Stunden="<<difftime(dasende,starting)/(3600)<<"  , Minuten="<<(difftime(dasende,starting)/(3600)-trunc(difftime(dasende,starting)/3600))*60<<endl;


    return 0;
}
 
