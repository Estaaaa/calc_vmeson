/*
 *ffactor_qphv_addon.h
 *
 *
 *functions to readInn Gernots parameter
 *
 * ---------------------------------------------------------------------
 */



//---------------------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------------------------//
void quark_photon_vertex_model_ReadInn(VectorXcd &fitQPV)
{

    //Reading inn Parameter form file
    string buffer="vertex_parameter.txt";   //D2 has a reason, never take D as a constant ever again!
    double A, B, C, D2, E, F;
    vecd dummyA, dummyB, dummyC, dummyD;

        ifstream infile;
        infile.open(buffer.c_str(), ios::in);           //DSEreal
        if(infile.is_open())
        {
            string daten;
            while(getline(infile,daten))
            {
                //the following line trims white space from the beginning of the string
                daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

                if(daten[0] == '#') {continue;}
                if(daten=="") {continue;}
                stringstream(daten) >> C >> D2 >> E >> F;
                if(daten=="") {continue;}

                 dummyA.push_back(C);
                 dummyB.push_back(D2);
                 dummyC.push_back(E);
                 dummyD.push_back(F);
            }
        }else{cout<<"No data file found! qph-vertex parameter"<<endl;}

        infile.close();
        int mv=dummyA.size();
        //cout<<"HERE:        "<<mv<<endl;

        if(mv==2*8)
        {
            for(int i=0; i<8; i++)
            {
                for(int j=0; j<2; j++)
                {
                    fitQPV(0+4*j+8*i)=dummyA[j+2*i];
                    fitQPV(1+4*j+8*i)=dummyB[j+2*i];
                    fitQPV(2+4*j+8*i)=dummyC[j+2*i];
                    fitQPV(3+4*j+8*i)=dummyD[j+2*i];
                }
            }
        }

        cout<<"ReadInn quark-photon parameter"<<endl;
        //cout<<"Test the ReadInn: "<<fitQPV(0+8*1)<<fitQPV(0+4+8)<<fitQPV(3+4+8*7)<<endl;
}

//---------------------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------------------------//
void quark_photon_vertex_model(complex<double> &f0, complex<double> &f1, complex<double> &f2, complex<double> &f3, complex<double> &f4, complex<double> &f5, complex<double> &f6, complex<double> &f7, complex<double> k_k, complex<double> P_P, VectorXcd &fitQPV )
{
    complex<double> S0 = (k_k)/3.0+P_P/4.0;
    complex<double> ss = 1.0-P_P/(2.0*S0);
    complex<double> f[8];


    for(int i=0; i<8; i++)
    {
        f[i]=(fitQPV(0+4*0+8*i)+fitQPV(1+4*0+8*i)*S0+fitQPV(2+4*0+8*i)*S0*S0)*exp(-fitQPV(3+4*0+8*i)*S0)
                +S0*ss*(fitQPV(0+4*1+8*i)+fitQPV(1+4*1+8*i)*S0+fitQPV(2+4*1+8*i)*S0*S0)*exp(-fitQPV(3+4*1+8*i)*S0);
    }
    f0=f[0];f1=f[1]; f2=f[2]; f3=f[3]; f4=f[4]; f5=f[5]; f6=f[6]; f7=f[7];


}
//---------------------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------------------------//
int quark_photon_vertex_interpol_ReadInn(vecvecdx &QPV, int m, int which, string rundef)
{

    //Reading inn Parameter form file
    stringstream ss; ss<<which;
    string buffer=rundef+"quarkphoton_grid_out_"+ss.str()+".txt";   //D2 has a reason, never take D as a constant ever again!
    double k1, k1c, Q,f1, f1c, f2, f2c, f3, f3c, f4, f4c, f5, f5c, f6, f6c, f7, f7c, f8, f8c, kq,kqc;
    vecd d1, d1c, d2, d2c, d3, d3c, d4, d4c, d5, d5c, d6, d6c, d7, d7c, d8, d8c;
    vecdcx d1v, d2v, d3v, d4v, d5v, d6v, d7v, d8v;

    ifstream infile;
    infile.open(buffer.c_str(), ios::in);           //DSEreal
    if(infile.is_open())
    {
        string daten;
        while(getline(infile,daten))
        {
            //the following line trims white space from the beginning of the string
            daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

            if(daten[0] == '#') {continue;}
            if(daten=="") {continue;}
            stringstream(daten) >> k1 >> k1c >> kq >> kqc >> Q >> f1 >> f1c >> f2 >> f2c >> f3 >> f3c >> f4 >> f4c >> f5 >> f5c >> f6 >> f6c >> f7 >> f7c >> f8 >> f8c;
            if(daten=="") {continue;}

            d1.push_back(f1);
            d1c.push_back(f1c);
            d2.push_back(f2);
            d2c.push_back(f2c);
            d3.push_back(f3);
            d3c.push_back(f3c);
            d4.push_back(f4);
            d4c.push_back(f4c);
            d5.push_back(f5);
            d5c.push_back(f5c);
            d6.push_back(f6);
            d6c.push_back(f6c);
            d7.push_back(f7);
            d7c.push_back(f7c);
            d8.push_back(f8);
            d8c.push_back(f8c);


        }
    }else{cout<<"No data file found! qph-vertex parameter"<<endl; return 0;}

    infile.close();
    int mv=d1.size();

    d1v.resize(mv);d2v.resize(mv);d3v.resize(mv);d4v.resize(mv);
    d5v.resize(mv);d6v.resize(mv);d7v.resize(mv);d8v.resize(mv);

    if(mv==m)
    {
            for(int j=0; j<m; j++)
            {
                real(d1v[j])=d1[j];
                imag(d1v[j])=d1c[j];
                real(d2v[j])=d2[j];
                imag(d2v[j])=d2c[j];
                real(d3v[j])=d3[j];
                imag(d3v[j])=d3c[j];
                real(d4v[j])=d4[j];
                imag(d4v[j])=d4c[j];
                real(d5v[j])=d5[j];
                imag(d5v[j])=d5c[j];
                real(d6v[j])=d6[j];
                imag(d6v[j])=d6c[j];
                real(d7v[j])=d7[j];
                imag(d7v[j])=d7c[j];
                real(d8v[j])=d8[j];
                imag(d8v[j])=d8c[j];

            }

        QPV.push_back(d1v);QPV.push_back(d2v);QPV.push_back(d3v);QPV.push_back(d4v);
        QPV.push_back(d5v);QPV.push_back(d6v);QPV.push_back(d7v);QPV.push_back(d8v);

        cout<<"ReadInn quark-photon form interpolation with: m="<<m<<"  @"<<buffer<<endl;

    }else{cout<<"ffactor_qphv_addon::QPV ReadInn: the dimension do not fit   "<<mv<<tab<<m<<"   @"<<buffer<<endl; return 0;}

    return 1;


}


//---------------------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------------------------//
void pigammagamma_qphv_grid(grid g,VectorXcd &A1, VectorXcd &B1,VectorXcd &A2, VectorXcd &B2,MatrixXcd &A3, MatrixXcd &B3, bse_amplitude &bse, VectorXcd &Re , double M, string rundef, int round, double fpi, int tensortag, string name)
{

 int m_dec=bse.m; int nz=bse.nz; int ny=g.ny; int numAm=bse.Ampnumber;
 double sig=sigma;
 complex<double> Sum=0;
 complex<double> Sum2=0;
 complex<double> Sum3=0;
 complex<double> Ak1, Bk1, Ak2, Bk2, Ak3, Bk3, L, ChiA;
 complex<double> fl10, fl11, fl12, fl13, fl14, fl15 ,fl16, fl17,fl20, fl21, fl22, fl23, fl24, fl25 ,fl26, fl27;
 four_vec k1,k2,k3,k4,k5,P,p,q,l1,l2;
 MatrixXcd Chi(11,11);
 VectorXcd fitQPV(2*4*8);

 quark_photon_vertex_model_ReadInn(fitQPV);
 cout<<"Now calculating the decay with parmatized q-ph vertex"<<endl;


    if(M<0){P.inhalt[3]=-M;}else{imag(P.inhalt[3])=M;}


 double Vorfac=1.0/((2*Pi)*(2*Pi)*(2*Pi))*2.0;


// string stringh;
// if(tensortag==0){stringh="data/test_data_qphv.txt";}else{stringh="trash.txt";}
// ofstream outa(stringh.c_str());

 //äußeres Winkelgrid zq
 for(int i_p=0; i_p<g.np; i_p++)
 {

        p=g.p_vector[i_p];
        k4=0.5*(p-P);
        k5=0.5*(p+P);


        Sum3=0;
        //inneres Winkelgrid z
        for(int i=0; i<nz; i++)
        {
            complex<double> z=bse.zqp[i]; complex<double> z_weigth=bse.w_z[i];
            Sum2=0;
            //inneres radial Grid l1
            for(int j=0; j<m_dec; j++)
            {
                complex<double> q_q=bse.p2[j];
                complex<double> rad_weigths=bse.radw[j];

                complex<double> E=bse.Amp[0][j+i*bse.m];
                complex<double> F,G,H=0;
                if(numAm>1){ F=bse.Amp[1][j+i*bse.m];}
                if(numAm>2){ G=bse.Amp[2][j+i*bse.m];}
                if(numAm>3){ H=bse.Amp[3][j+i*bse.m];}


                Sum=0.0;
                //2 Winkelgrid bse.zy
                for(int y_sum=0; y_sum<ny; y_sum++)
                {
                    complex<double> y=g.y[y_sum]; complex<double> y_weigth=g.y_weight[y_sum];
                    q.inhalt[0]=0; q.inhalt[1]=sqrt(q_q)*sqrt(1.0-z*z)*sqrt(1.0-y*y);
                    q.inhalt[2]=sqrt(q_q)*y*sqrt(1.0-z*z); q.inhalt[3]=sqrt(q_q)*z;

                    four_vec Q1, Q2;
                    complex<double> x1, y1, z1, x2, y2, z2;

                    k1=q+sig*P;
                    k2=q-(1.0-sig)*P;
                    k3=k1+k4;

                    l1=0.5*(k3+k1);
                    l2=0.5*(k2+k3);

                    Q1=k4;
                    Q2=k5;



                    Ak1=A1(j+m_dec*i);
                    Bk1=B1(j+m_dec*i);
                    Ak2=A2(j+m_dec*i);
                    Bk2=B2(j+m_dec*i);

                    Ak3=A3(i_p,y_sum+ny*j+m_dec*ny*i);
                    Bk3=B3(i_p,y_sum+ny*j+m_dec*ny*i);


                    x1= 0.5*(Ak3+Ak1);
                    x2= 0.5*(Ak3+Ak2);
                    y1= (Ak3-Ak1)/(k3*k3-k1*k1);
                    y2= (Ak2-Ak3)/(k2*k2-k3*k3);
                    z1= (Bk3-Bk1)/(k3*k3-k1*k1);
                    z2= (Bk2-Bk3)/(k2*k2-k3*k3);

                    quark_photon_vertex_model(fl10,fl11,fl12,fl13,fl14,fl15,fl16,fl17,l1*l1 ,Q1*Q1 ,fitQPV);
                    quark_photon_vertex_model(fl20,fl21,fl22,fl23,fl24,fl25,fl26,fl27,l2*l2 ,Q2*Q2 ,fitQPV);


  /*{                  if(tensortag==4){fl10=fl11=fl12=fl14=fl15=fl16=fl17=0.0;fl20=fl21=fl22=fl24=fl25=fl26=fl27=0.0;}
                    if(tensortag==5){fl10=fl11=fl12=fl13=fl15=fl16=fl17=0.0;fl20=fl21=fl22=fl23=fl25=fl26=fl27=0.0;}
                    if(tensortag==8){fl10=fl11=fl12=fl14=fl15=fl16=fl13=0.0;fl20=fl21=fl22=fl24=fl25=fl26=fl23=0.0;}
                    if(tensortag==1){fl17=fl11=fl12=fl14=fl15=fl16=fl13=0.0;fl27=fl21=fl22=fl24=fl25=fl26=fl23=0.0;}
                    if(tensortag==6){fl10=fl11=fl12=fl14=fl17=fl16=fl13=0.0;fl20=fl21=fl22=fl24=fl27=fl26=fl23=0.0;}
                    if(tensortag==7){fl10=fl11=fl12=fl14=fl15=fl17=fl13=0.0;fl20=fl21=fl22=fl24=fl25=fl27=fl23=0.0;}
                    if(tensortag==3){fl10=fl11=fl17=fl14=fl15=fl16=fl13=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=0.0;}
                    if(tensortag==2){fl10=fl12=fl17=fl14=fl15=fl16=fl13=0.0;fl20=fl22=fl27=fl24=fl25=fl26=fl23=0.0;}
                    if(tensortag==12){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;}
                    if(tensortag==13){fl20=fl21=fl27=fl24=fl25=fl26=fl23=fl22=0.0;}
                    if(tensortag==14){fl20=fl21=fl27=fl24=fl25=fl26=fl23=fl22=0.0;fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;}
                    if(tensortag==20){fl20=fl21=fl27=fl24=fl25=fl26=fl23=fl22=0.0;fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0; x1=x2=1.0; z2=z1=y1=y2=0;}
                    if(tensortag==19){fl20=fl21=fl24=fl25=fl26=fl23=fl22=0.0; x1=x2=ii; y1=z1=y2=z2=0;fl10=fl11=fl13=fl14=fl15=fl16=fl12=0.0;}
                    if(tensortag==21){fl20=fl21=fl27=fl24=fl25=fl26=fl23=fl22=0.0; x1=1.0; y1=z1=0;fl10=fl11=fl14=fl13=fl15=fl16=fl12=0.0;}
                    if(tensortag==22){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0; x2=1.0; y2=z2=0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=0.0;}
                    if(tensortag==23){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=0.0;}
                    if(tensortag==24){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl22=0.0;}
                    if(tensortag==25){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;fl20=fl21=fl22=fl24=fl25=fl26=fl23=0.0;}
                    if(tensortag==26){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;}
                    if(tensortag==27){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=x2=1.0; y1=z1=y2=z2=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=0.0;}
                    if(tensortag==28){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=x2=1.0; y1=z1=y2=z2=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl22=0.0;}
                    if(tensortag==29){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=x2=1.0; y1=z1=y2=z2=0.0;fl20=fl21=fl22=fl24=fl25=fl26=fl23=0.0;}
                    if(tensortag==30){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=x2=y2=z2=0.0;}
                    if(tensortag==32){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=0.0;}
                    if(tensortag==31){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=fl22=0.0;}
                    if(tensortag==33){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=y2=z2=x2=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=0.0;}
                    if(tensortag==34){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=y2=z2=x2=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl22=0.0;}
                    if(tensortag==35){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=y2=z2=x2=0.0;fl20=fl21=fl22=fl24=fl25=fl26=fl23=0.0;}
                    if(tensortag==36){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=y2=z2=x2=0.0;}
                    if(tensortag==37){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0;fl20=fl21=fl24=fl25=fl26=fl23=0.0; fl22=fl27=1.0; y1=z1=y2=z2=x2=0.0;}
                    if(tensortag==38){
                        fl12=0.33/((1.0+l1*l1/pow(0.7,2))*(1.0+l1*l1/pow(0.7,2)))*ii;
                        fl22=0.33/((1.0+l2*l2/pow(0.7,2))*(1.0+l2*l2/pow(0.7,2)))*ii;
                        fl13=5.4/((1.0+l1*l1/pow(0.7,2))*(1.0+l1*l1/pow(0.7,2)))*ii;
                        fl23=5.4/((1.0+l2*l2/pow(0.7,2))*(1.0+l2*l2/pow(0.7,2)))*ii;
                        fl17=1.53/((1.0+l1*l1/pow(0.7,2))*(1.0+l1*l1/pow(0.7,2)))*ii;
                        fl27=1.53/((1.0+l2*l2/pow(0.7,2))*(1.0+l2*l2/pow(0.7,2)))*ii;
                        fl10=fl11=fl14=fl15=fl16=0.0;fl20=fl21=fl24=fl25=fl26=0.0;  y1=z1=y2=z2=0.0; x1=x2=ii;
                    }
                    if(tensortag==39){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0; fl20=fl21=fl24=fl25=fl26=fl23=fl22=fl27=0.0; y1=z1=y2=z2=ii; x2=x1=ii;
                    }
                    if(tensortag==40){fl20=fl21=fl24=fl25=fl26=fl23=fl22=0.0;fl10=fl11=fl14=fl15=fl16=fl13=fl12=0.0; y1=y2=z1=z2=0.0; x1=x2=ii;}
                    if(tensortag==41){
                        complex<double> x11= (l1*l1/3.0+Q1*Q1/4.0)/pow(0.65,2);
                        complex<double> x22= (l2*l2/3.0+Q2*Q2/4.0)/pow(0.65,2);
                        fl12=0.33/((1.0+x11)*(1.0+x11))*ii;
                        fl22=0.33/((1.0+x22)*(1.0+x22))*ii;
                        fl13=fl23=fl27=fl17=0.0;
                        fl10=fl11=fl14=fl15=fl16=0.0;fl20=fl21=fl24=fl25=fl26=0.0;  y1=z1=y2=z2=0.0; x1=x2=ii;
                    }
                    if(tensortag==42){
                        complex<double> x11= (l1*l1/3.0+Q1*Q1/4.0)/pow(0.65,2);
                        complex<double> x22= (l2*l2/3.0+Q2*Q2/4.0)/pow(0.65,2);
                        fl13=5.4/((1.0+x11)*(1.0+x11))*ii;
                        fl23=5.4/((1.0+x22)*(1.0+x22))*ii;
                        fl12=fl22=fl27=fl17=0.0;
                        fl10=fl11=fl14=fl15=fl16=0.0;fl20=fl21=fl24=fl25=fl26=0.0;  y1=z1=y2=z2=0.0; x1=x2=ii;
                    }
                    if(tensortag==43){
                        complex<double> x11= (l1*l1/3.0+Q1*Q1/4.0)/pow(0.65,2);
                        complex<double> x22= (l2*l2/3.0+Q2*Q2/4.0)/pow(0.65,2);
                        fl17=1.53/((1.0+x11)*(1.0+x11))*ii;
                        fl27=1.53/((1.0+x22)*(1.0+x22))*ii;
                        fl13=fl23=fl22=fl12=0.0;
                        fl10=fl11=fl14=fl15=fl16=0.0;fl20=fl21=fl24=fl25=fl26=0.0;  y1=z1=y2=z2=0.0; x1=x2=ii;
                    }
                    if(tensortag==44){
                        complex<double> x11= (l1*l1/3.0+Q1*Q1/4.0)/pow(0.65,2);
                        complex<double> x22= (l2*l2/3.0+Q2*Q2/4.0)/pow(0.65,2);
                        fl12=0.33/((1.0+x11)*(1.0+x11))*ii;
                        fl22=0.33/((1.0+x22)*(1.0+x22))*ii;
                        fl13=5.4/((1.0+x11)*(1.0+x11))*ii;
                        fl23=5.4/((1.0+x22)*(1.0+x22))*ii;
                        fl17=1.53/((1.0+x11)*(1.0+x11))*ii;
                        fl27=1.53/((1.0+x22)*(1.0+x22))*ii;
                        fl10=fl11=fl14=fl15=fl16=0.0;fl20=fl21=fl24=fl25=fl26=0.0;  y1=z1=y2=z2=0.0; x1=x2=ii;
                    }
}*/

                    L=-(1.0/(k1*k1*Ak1*Ak1+Bk1*Bk1))*(1.0/(k2*k2*Ak2*Ak2+Bk2*Bk2))
                            *(1.0/((k3*k3)*Ak3*Ak3+Bk3*Bk3))*(1.0/(2.0*((k4*k4)*(k5*k5)-pow(k4*k5,2))));

//                    if((i_p==0 && y_sum==0 && i==0 && j==0)||(i_p==1 && y_sum==0 && i==0 && j==10)){k3.write();k4.write();k5.write(); l2.write();
//                        cout<<"Other stuff: "<<L<<tab<<k1*k1<<tab<<Ak1<<tab<<Bk1<<tab<<k2*k2<<tab<<(1.0/(k1*k1*Ak1*Ak1+Bk1*Bk1))<<tab<<(1.0/(k2*k2*Ak2*Ak2+Bk2*Bk2))<<endl;}



                    //#########################################################################################

                    trace_decay_qph(Chi,x1,y1,z1,x2,y2,z2,fl10,fl11,fl12,fl13,fl14,fl15,fl16,fl17,fl20,fl21,fl22,fl23,fl24,fl25,fl26,fl27,
                                    Ak3,Bk3,k3,k4,k5,P,q,p,l1,l2, E, F, G, H);


                    //#########################################################################################

                    ChiA=0.0;
                    for(int mat_i=0; mat_i<11; mat_i++)
                    {
                        for(int mat_j=0; mat_j<11; mat_j++)
                        {
                            ChiA += Chi(mat_i, mat_j);

                        }
                    }


                    Sum += ChiA*L*y_weigth;
                }



                Sum2 +=rad_weigths*(0.5)*q_q*q_q*Sum;

            }

            Sum3 += sqrt(1.0-z*z)*z_weigth*Sum2;
        }

        Re(i_p)=Vorfac*Sum3;

 }

//outa.close();



 //Writing out Results
 stringstream ss; ss<<round; string sss=ss.str();  stringstream ssa; ssa<<tensortag; string sssa=ssa.str();string buffer;

 buffer = rundef+"decay_"+name+".txt";
 if(tensortag!=0){buffer = rundef+"decay_qph_para_"+name+"_t"+sssa+".txt";}
 ofstream outb(buffer.c_str());
 outb<<"#   m="<<m_dec<<" nz:"<<nz<<" ny_BSE:"<<bse.ny<<" ny_Decay:"<<ny<<"  pionM:"<<M<<endl;
 outb<<"#--------------------------------------------------# "<<endl;
 outb<<"#   Re(p2) imag(p2)  Re(Lambda)  imag(Lambda)    real(k4) real(k5)  p"<<endl;
 for(int i_p=0; i_p<g.np; i_p++)
 {
         p=g.p_vector[i_p];
         k4=0.5*(p-P);
         k5=0.5*(p+P);
         k1=q+sig*P;
         k2=q-(1.0-sig)*P;
         k3=k1+k4;

         outb<<real(p*p)<<"\t"<<imag(p*p)<<"\t"<<(4.0*Pi*Pi*fpi)*real(Re(i_p))<<"\t"<<(4.0*Pi*Pi*fpi)*imag(Re(i_p))<<"\t"
         <<real(k4*k4)<<"\t"<<imag(k4*k4)<<"\t"<<real(k5*k5)<<"\t"<<imag(k5*k5)<<"\t"<<k3*k3<<endl;
 }
 outb.close();




}
//---------------------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------------------------//
void pigammagamma_qphv_grid_interpolation(grid g,VectorXcd &A1, VectorXcd &B1,VectorXcd &A2, VectorXcd &B2,MatrixXcd &A3, MatrixXcd &B3, bse_amplitude &bse, VectorXcd &Re , double M, string rundef, int round, double fpi, int tensortag, string name)
{

    int m_dec=bse.m; int nz=bse.nz; int ny=g.ny; int numAm=bse.Ampnumber;
    double sig=sigma;
    complex<double> Sum=0;
    complex<double> Sum2=0;
    complex<double> Sum3=0;
    complex<double> Ak1, Bk1, Ak2, Bk2, Ak3, Bk3, L, ChiA;
    complex<double> fl10, fl11, fl12, fl13, fl14, fl15 ,fl16, fl17,fl20, fl21, fl22, fl23, fl24, fl25 ,fl26, fl27;
    four_vec k1,k2,k3,k4,k5,P,p,q,l1,l2;
    MatrixXcd Chi(11,11);
    int test;

    vecvecdx QPV1;
    vecvecdx QPV2;

    test=quark_photon_vertex_interpol_ReadInn(QPV1,m_dec*nz*ny*g.np,1,rundef);
    if(test==0){cout<<"Calction Aborted QPV not found"<<endl; return;}
    test=quark_photon_vertex_interpol_ReadInn(QPV2,m_dec*nz*ny*g.np,2,rundef);
    if(test==0){cout<<"Calction Aborted QPV not found"<<endl; return;}

    if(M<0){P.inhalt[3]=-M;}else{imag(P.inhalt[3])=M;}


    double Vorfac=1.0/((2*Pi)*(2*Pi)*(2*Pi))*2.0;


// string stringh;
// if(tensortag==0){stringh="data/test_data_qphv.txt";}else{stringh="trash.txt";}
// ofstream outa(stringh.c_str());

    //äußeres Winkelgrid zq
    for(int i_p=0; i_p<g.np; i_p++)
    {

        p=g.p_vector[i_p];
        k4=0.5*(p-P);
        k5=0.5*(p+P);


        Sum3=0;
        //inneres Winkelgrid z
        for(int i=0; i<nz; i++)
        {
            complex<double> z=bse.zqp[i]; complex<double> z_weigth=bse.w_z[i];
            Sum2=0;
            //inneres radial Grid l1
            for(int j=0; j<m_dec; j++)
            {
                complex<double> q_q=bse.p2[j];
                complex<double> rad_weigths=bse.radw[j];

                complex<double> E=bse.Amp[0][j+i*bse.m];
                complex<double> F,G,H=0;
                if(numAm>1){ F=bse.Amp[1][j+i*bse.m];}
                if(numAm>2){ G=bse.Amp[2][j+i*bse.m];}
                if(numAm>3){ H=bse.Amp[3][j+i*bse.m];}


                Sum=0.0;
                //2 Winkelgrid bse.zy
                for(int y_sum=0; y_sum<ny; y_sum++)
                {
                    complex<double> y=g.y[y_sum]; complex<double> y_weigth=g.y_weight[y_sum];
                    q.inhalt[0]=0; q.inhalt[1]=sqrt(q_q)*sqrt(1.0-z*z)*sqrt(1.0-y*y);
                    q.inhalt[2]=sqrt(q_q)*y*sqrt(1.0-z*z); q.inhalt[3]=sqrt(q_q)*z;

                    four_vec Q1, Q2;
                    complex<double> x1, y1, z1, x2, y2, z2;

                    k1=q+sig*P;
                    k2=q-(1.0-sig)*P;
                    k3=k1+k4;

                    l1=0.5*(k3+k1);
                    l2=0.5*(k2+k3);

                    Q1=k4;
                    Q2=k5;



                    Ak1=A1(j+m_dec*i);
                    Bk1=B1(j+m_dec*i);
                    Ak2=A2(j+m_dec*i);
                    Bk2=B2(j+m_dec*i);

                    Ak3=A3(i_p,y_sum+ny*j+m_dec*ny*i);
                    Bk3=B3(i_p,y_sum+ny*j+m_dec*ny*i);


                    x1= 0.5*(Ak3+Ak1);
                    x2= 0.5*(Ak3+Ak2);
                    y1= (Ak3-Ak1)/(k3*k3-k1*k1);
                    y2= (Ak2-Ak3)/(k2*k2-k3*k3);
                    z1= (Bk3-Bk1)/(k3*k3-k1*k1);
                    z2= (Bk2-Bk3)/(k2*k2-k3*k3);

                    fl10=QPV1[0][i_p+y_sum*g.np+g.np*ny*i+g.np*ny*nz*j];
                    fl11=QPV1[1][i_p+y_sum*g.np+g.np*ny*i+g.np*ny*nz*j];
                    fl12=QPV1[2][i_p+y_sum*g.np+g.np*ny*i+g.np*ny*nz*j];
                    fl13=QPV1[3][i_p+y_sum*g.np+g.np*ny*i+g.np*ny*nz*j];
                    fl14=QPV1[4][i_p+y_sum*g.np+g.np*ny*i+g.np*ny*nz*j];
                    fl15=QPV1[5][i_p+y_sum*g.np+g.np*ny*i+g.np*ny*nz*j];
                    fl16=QPV1[6][i_p+y_sum*g.np+g.np*ny*i+g.np*ny*nz*j];
                    fl17=QPV1[7][i_p+y_sum*g.np+g.np*ny*i+g.np*ny*nz*j];

                    fl20=QPV2[0][i_p+y_sum*g.np+g.np*ny*i+g.np*ny*nz*j];
                    fl21=QPV2[1][i_p+y_sum*g.np+g.np*ny*i+g.np*ny*nz*j];
                    fl22=QPV2[2][i_p+y_sum*g.np+g.np*ny*i+g.np*ny*nz*j];
                    fl23=QPV2[3][i_p+y_sum*g.np+g.np*ny*i+g.np*ny*nz*j];
                    fl24=QPV2[4][i_p+y_sum*g.np+g.np*ny*i+g.np*ny*nz*j];
                    fl25=QPV2[5][i_p+y_sum*g.np+g.np*ny*i+g.np*ny*nz*j];
                    fl26=QPV2[6][i_p+y_sum*g.np+g.np*ny*i+g.np*ny*nz*j];
                    fl27=QPV2[7][i_p+y_sum*g.np+g.np*ny*i+g.np*ny*nz*j];

                    /*{                  if(tensortag==4){fl10=fl11=fl12=fl14=fl15=fl16=fl17=0.0;fl20=fl21=fl22=fl24=fl25=fl26=fl27=0.0;}
                                      if(tensortag==5){fl10=fl11=fl12=fl13=fl15=fl16=fl17=0.0;fl20=fl21=fl22=fl23=fl25=fl26=fl27=0.0;}
                                      if(tensortag==8){fl10=fl11=fl12=fl14=fl15=fl16=fl13=0.0;fl20=fl21=fl22=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==1){fl17=fl11=fl12=fl14=fl15=fl16=fl13=0.0;fl27=fl21=fl22=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==6){fl10=fl11=fl12=fl14=fl17=fl16=fl13=0.0;fl20=fl21=fl22=fl24=fl27=fl26=fl23=0.0;}
                                      if(tensortag==7){fl10=fl11=fl12=fl14=fl15=fl17=fl13=0.0;fl20=fl21=fl22=fl24=fl25=fl27=fl23=0.0;}
                                      if(tensortag==3){fl10=fl11=fl17=fl14=fl15=fl16=fl13=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==2){fl10=fl12=fl17=fl14=fl15=fl16=fl13=0.0;fl20=fl22=fl27=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==12){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;}
                                      if(tensortag==13){fl20=fl21=fl27=fl24=fl25=fl26=fl23=fl22=0.0;}
                                      if(tensortag==14){fl20=fl21=fl27=fl24=fl25=fl26=fl23=fl22=0.0;fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;}
                                      if(tensortag==20){fl20=fl21=fl27=fl24=fl25=fl26=fl23=fl22=0.0;fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0; x1=x2=1.0; z2=z1=y1=y2=0;}
                                      if(tensortag==19){fl20=fl21=fl24=fl25=fl26=fl23=fl22=0.0; x1=x2=ii; y1=z1=y2=z2=0;fl10=fl11=fl13=fl14=fl15=fl16=fl12=0.0;}
                                      if(tensortag==21){fl20=fl21=fl27=fl24=fl25=fl26=fl23=fl22=0.0; x1=1.0; y1=z1=0;fl10=fl11=fl14=fl13=fl15=fl16=fl12=0.0;}
                                      if(tensortag==22){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0; x2=1.0; y2=z2=0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==23){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==24){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl22=0.0;}
                                      if(tensortag==25){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;fl20=fl21=fl22=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==26){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;}
                                      if(tensortag==27){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=x2=1.0; y1=z1=y2=z2=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==28){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=x2=1.0; y1=z1=y2=z2=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl22=0.0;}
                                      if(tensortag==29){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=x2=1.0; y1=z1=y2=z2=0.0;fl20=fl21=fl22=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==30){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=x2=y2=z2=0.0;}
                                      if(tensortag==32){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=0.0;}
                                      if(tensortag==31){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=fl22=0.0;}
                                      if(tensortag==33){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=y2=z2=x2=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==34){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=y2=z2=x2=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl22=0.0;}
                                      if(tensortag==35){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=y2=z2=x2=0.0;fl20=fl21=fl22=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==36){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=y2=z2=x2=0.0;}
                                      if(tensortag==37){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0;fl20=fl21=fl24=fl25=fl26=fl23=0.0; fl22=fl27=1.0; y1=z1=y2=z2=x2=0.0;}
                                      if(tensortag==38){
                                          fl12=0.33/((1.0+l1*l1/pow(0.7,2))*(1.0+l1*l1/pow(0.7,2)))*ii;
                                          fl22=0.33/((1.0+l2*l2/pow(0.7,2))*(1.0+l2*l2/pow(0.7,2)))*ii;
                                          fl13=5.4/((1.0+l1*l1/pow(0.7,2))*(1.0+l1*l1/pow(0.7,2)))*ii;
                                          fl23=5.4/((1.0+l2*l2/pow(0.7,2))*(1.0+l2*l2/pow(0.7,2)))*ii;
                                          fl17=1.53/((1.0+l1*l1/pow(0.7,2))*(1.0+l1*l1/pow(0.7,2)))*ii;
                                          fl27=1.53/((1.0+l2*l2/pow(0.7,2))*(1.0+l2*l2/pow(0.7,2)))*ii;
                                          fl10=fl11=fl14=fl15=fl16=0.0;fl20=fl21=fl24=fl25=fl26=0.0;  y1=z1=y2=z2=0.0; x1=x2=ii;
                                      }
                                      if(tensortag==39){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0; fl20=fl21=fl24=fl25=fl26=fl23=fl22=fl27=0.0; y1=z1=y2=z2=ii; x2=x1=ii;
                                      }
                                      if(tensortag==40){fl20=fl21=fl24=fl25=fl26=fl23=fl22=0.0;fl10=fl11=fl14=fl15=fl16=fl13=fl12=0.0; y1=y2=z1=z2=0.0; x1=x2=ii;}
                                      if(tensortag==41){
                                          complex<double> x11= (l1*l1/3.0+Q1*Q1/4.0)/pow(0.65,2);
                                          complex<double> x22= (l2*l2/3.0+Q2*Q2/4.0)/pow(0.65,2);
                                          fl12=0.33/((1.0+x11)*(1.0+x11))*ii;
                                          fl22=0.33/((1.0+x22)*(1.0+x22))*ii;
                                          fl13=fl23=fl27=fl17=0.0;
                                          fl10=fl11=fl14=fl15=fl16=0.0;fl20=fl21=fl24=fl25=fl26=0.0;  y1=z1=y2=z2=0.0; x1=x2=ii;
                                      }
                                      if(tensortag==42){
                                          complex<double> x11= (l1*l1/3.0+Q1*Q1/4.0)/pow(0.65,2);
                                          complex<double> x22= (l2*l2/3.0+Q2*Q2/4.0)/pow(0.65,2);
                                          fl13=5.4/((1.0+x11)*(1.0+x11))*ii;
                                          fl23=5.4/((1.0+x22)*(1.0+x22))*ii;
                                          fl12=fl22=fl27=fl17=0.0;
                                          fl10=fl11=fl14=fl15=fl16=0.0;fl20=fl21=fl24=fl25=fl26=0.0;  y1=z1=y2=z2=0.0; x1=x2=ii;
                                      }
                                      if(tensortag==43){
                                          complex<double> x11= (l1*l1/3.0+Q1*Q1/4.0)/pow(0.65,2);
                                          complex<double> x22= (l2*l2/3.0+Q2*Q2/4.0)/pow(0.65,2);
                                          fl17=1.53/((1.0+x11)*(1.0+x11))*ii;
                                          fl27=1.53/((1.0+x22)*(1.0+x22))*ii;
                                          fl13=fl23=fl22=fl12=0.0;
                                          fl10=fl11=fl14=fl15=fl16=0.0;fl20=fl21=fl24=fl25=fl26=0.0;  y1=z1=y2=z2=0.0; x1=x2=ii;
                                      }
                                      if(tensortag==44){
                                          complex<double> x11= (l1*l1/3.0+Q1*Q1/4.0)/pow(0.65,2);
                                          complex<double> x22= (l2*l2/3.0+Q2*Q2/4.0)/pow(0.65,2);
                                          fl12=0.33/((1.0+x11)*(1.0+x11))*ii;
                                          fl22=0.33/((1.0+x22)*(1.0+x22))*ii;
                                          fl13=5.4/((1.0+x11)*(1.0+x11))*ii;
                                          fl23=5.4/((1.0+x22)*(1.0+x22))*ii;
                                          fl17=1.53/((1.0+x11)*(1.0+x11))*ii;
                                          fl27=1.53/((1.0+x22)*(1.0+x22))*ii;
                                          fl10=fl11=fl14=fl15=fl16=0.0;fl20=fl21=fl24=fl25=fl26=0.0;  y1=z1=y2=z2=0.0; x1=x2=ii;
                                      }
                  }*/

                    L=-(1.0/(k1*k1*Ak1*Ak1+Bk1*Bk1))*(1.0/(k2*k2*Ak2*Ak2+Bk2*Bk2))
                      *(1.0/((k3*k3)*Ak3*Ak3+Bk3*Bk3))*(1.0/(2.0*((k4*k4)*(k5*k5)-pow(k4*k5,2))));

//                    if((i_p==0 && y_sum==0 && i==0 && j==0)||(i_p==1 && y_sum==0 && i==0 && j==10)){k3.write();k4.write();k5.write(); l2.write();
//                        cout<<"Other stuff: "<<L<<tab<<k1*k1<<tab<<Ak1<<tab<<Bk1<<tab<<k2*k2<<tab<<(1.0/(k1*k1*Ak1*Ak1+Bk1*Bk1))<<tab<<(1.0/(k2*k2*Ak2*Ak2+Bk2*Bk2))<<endl;}



                    //#########################################################################################

                    trace_decay_qph(Chi,x1,y1,z1,x2,y2,z2,fl10,fl11,fl12,fl13,fl14,fl15,fl16,fl17,fl20,fl21,fl22,fl23,fl24,fl25,fl26,fl27,
                                    Ak3,Bk3,k3,k4,k5,P,q,p,l1,l2, E, F, G, H);


                    //#########################################################################################

                    ChiA=0.0;
                    for(int mat_i=0; mat_i<11; mat_i++)
                    {
                        for(int mat_j=0; mat_j<11; mat_j++)
                        {
                            ChiA += Chi(mat_i, mat_j);

                        }
                    }


                    Sum += ChiA*L*y_weigth;
                }



                Sum2 +=rad_weigths*(0.5)*q_q*q_q*Sum;

            }

            Sum3 += sqrt(1.0-z*z)*z_weigth*Sum2;
        }

        Re(i_p)=Vorfac*Sum3;

    }

//outa.close();



    //Writing out Results
    stringstream ss; ss<<round; string sss=ss.str();  stringstream ssa; ssa<<tensortag; string sssa=ssa.str();string buffer;

    buffer = rundef+"decay_"+name+".txt";
    if(tensortag!=0){buffer = rundef+"decay_qph_para_"+name+"_t"+sssa+".txt";}
    ofstream outb(buffer.c_str());
    outb<<"#   m="<<m_dec<<" nz:"<<nz<<" ny_BSE:"<<bse.ny<<" ny_Decay:"<<ny<<"  pionM:"<<M<<endl;
    outb<<"#--------------------------------------------------# "<<endl;
    outb<<"#   Re(p2) imag(p2)  Re(Lambda)  imag(Lambda)    real(k4) real(k5)  p"<<endl;
    for(int i_p=0; i_p<g.np; i_p++)
    {
        p=g.p_vector[i_p];
        k4=0.5*(p-P);
        k5=0.5*(p+P);
        k1=q+sig*P;
        k2=q-(1.0-sig)*P;
        k3=k1+k4;

        outb<<real(p*p)<<"\t"<<imag(p*p)<<"\t"<<(4.0*Pi*Pi*fpi)*real(Re(i_p))<<"\t"<<(4.0*Pi*Pi*fpi)*imag(Re(i_p))<<"\t"
            <<real(k4*k4)<<"\t"<<imag(k4*k4)<<"\t"<<real(k5*k5)<<"\t"<<imag(k5*k5)<<"\t"<<k3*k3<<endl;
    }
    outb.close();

    //Writting out a second file in a different format.
    string buffer2 = rundef +"FF_output.txt";
    ofstream f(buffer2.c_str());
    for(int i=0; i<g.np; i++)
    {
        p=g.p_vector[i];
        k4=0.5*(p-P);
        f<<scientific<<setprecision(10)<<(4.0*Pi*Pi*fpi)*real(Re(i))<<tab<<(4.0*Pi*Pi*fpi)*imag(Re(i))<<tab<<real(k4*k4)<<tab<<imag(k4*k4)<<endl;
    }
    f.close();
    cout<<"Done"<<endl;




}
//---------------------------------------------------------------------------------------------------------------------//
bool ff_ReadInn(string rundef,VectorXcd &Re, string name)
{
    bool helper=true;
    int ng=Re.size();
    string buffer;
    buffer = rundef+"decay_"+name+".txt";
//    cout<<buffer<<endl;

    //Reading inn real quark propagator from file!
            double Rp2, Ip2, Rr, Rc, k4r, k4c, k5r, k5c;
            vecd dummyA, dummyB, dummyC, dummyAA;

            ifstream infile;
            infile.open(buffer.c_str(), ios::in);
            if(infile.is_open())
            {
                string daten;
                while(getline(infile,daten))
                {
                    //the following line trims white space from the beginning of the string
                    daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

                    if(daten[0] == '#') {continue;}
                    if(daten=="") {continue;}
                    stringstream(daten) >> Rp2 >> Ip2 >> Rr >> Rc >> k4r >> k4c >> k5r >> k5c;
                    if(daten=="") {continue;}

                     dummyA.push_back(Rp2);
                     dummyAA.push_back(Ip2);
                     dummyB.push_back(Rr);
                     dummyC.push_back(Rc);
                }
            }else{cout<<"No data file with the formfactor found!"<<endl; helper=false; return helper;}

            infile.close();
            int mv=dummyA.size();
            //cout<<"HERE:        "<<mv<<endl;

            if(mv==ng)
            {
                for(int i=0; i<mv; i++)
                {
                    real(Re(i))=dummyB[i];
                    imag(Re(i))=dummyC[i];
                }


                cout<<"ReadInn: Form Factor ! "; return helper;
            }
            else{cout<<"The Form Factor has not the same dimensions that the data you are trying to readINN!!! ABORTED"<<endl; helper=false; return helper;}

}

//---------------------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------------------------//
//#######################################################################################################################

