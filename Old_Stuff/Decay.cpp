#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <complex>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>

using namespace std;

#include "Toolbox/ToolHeader.h"

#define Mpion 0.139
#define Mv 0.769
#define sigma 0.5                   //decay
#define eta 0.5                     //pion
#define fpion 0.092
#define elm 1.6021766208e-19
#define alphae 1.0/137.036

#define lam2 4.0e+4

#include "DSEHeads/DSEHeader.h"
#include "BSEHeads/BSEHeader.h"
#include "ffactor_qphv.h"
#include "ffactor_qphv_addon.h"
#include "DecayHeads/DecayHeader.h"



/*------------------------------------START MAIN-----------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
int main (int argc, char * const argv[]) {

    clock_t start;
    float timet;
    time_t starting, dasende;
    time(&starting);

    string folder="0";
    string rundef ="data/output_"+folder+"/";
    string q_data_name="data/input/";

    double IRcutoff=1.0e-2;
    double UVcutoff=1.0e+3;

    int mQuark=150;
    int nQuark=32;

    int mDec=20;
    int nyDec=12;
    int nzDec=24;
    int nyBSA=12;

    int nphi=0;

    int n_s=50;

    int BSEcalctag = 1;
    int Chiraltag = 0;
    int PauliVillartag=0;
    double quarkmass=0.0037;
    double Mresult2=0.1377;
    int round= 1;
    int calc_quark3 =1;
    int calc_quark3_steps=7;
    int formfactortag =1;
    int decaytag=1;
    int AmpNumber=4;
    double Mdecay=-0.1;


    int ReadInn=1;

    if(ReadInn==1)
    {
        Input_reader_file(argc,argv,mDec,nzDec,nyDec,nyBSA,rundef,BSEcalctag,Chiraltag,PauliVillartag, round, mQuark, nQuark, calc_quark3, calc_quark3_steps,
                     formfactortag, decaytag, n_s, quarkmass,Mresult2, Mdecay);

    }


    if(PauliVillartag==1){if(Chiraltag==0){q_data_name="data/input/PV_";}else{q_data_name="data/input/PV_CL_";}}
    else if(PauliVillartag==0){if(Chiraltag==0){q_data_name="data/input/";}else{q_data_name="data/input/CL_";}}
    else{cout<<"PauliVillartag is wrong! ABORTED"<<endl; return 0;}

    cout<<"Starting....."<<endl;
    cout<<"The Parameter of this session: m="<<mDec<<" , ny="<<nyDec<<" , nz="<<nzDec<<"  BSA para: ny="<<nyBSA<<endl;
    cout<<"PauliVillar= "<<PauliVillartag<<" , Chiraltag= "<<Chiraltag<<" ,BSEcalctag= "<<BSEcalctag<<" , folder="<<atoi(argv[5])<<"   ,  calc_quark3="<<calc_quark3<<" ,formfactortag="<<formfactortag<<" ,decaytag="<<decaytag<<endl;
    cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;

    string decayname;
    if(decaytag ==3 || decaytag ==4 || decaytag == 1)
    {
        decayname="gamma"; if(decaytag!=1){cout<<"In this session we are calculating the pi->gamma gamma decay"<<endl;}
        else{cout<<"In this session we are calculating for the pi->e+ e- :rare decay"<<endl;} n_s=0;
    }
    else if(decaytag==2)
    {

     if(n_s==0 ){cout<<"The Darlitz n_s has been automatic set"<<endl; n_s=12;}
     decayname="dalitz";cout<<"In this session we are calculating the pi->gamma gamma for the pi -< gamma e+ e-  :Dalitz decay"<<endl; cout<<"n_s="<<n_s<<endl;
    }else{cout<<"Which decay did you want to calculate? "<<endl; return 0;}
    cout<<"________________________________________________________"<<endl;

    complex<double> Resultfpion=0.0928;

    grid decay_grid(mDec,nzDec,nyDec,nphi,n_s);
    decay_grid.init(-1.0,1.0,1, decaytag,IRcutoff, UVcutoff );

    //Grid stuff (setting the p_vector for the S(k3) calculation and loop integral of decay)
    decay_grid.set_p(Mresult2,decaytag);

    decay_grid.write(decayname,rundef); //for checking the grid!

    //|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<<

    cout<<"_____|Calculating the DECAY|________"<<endl;


    VectorXcd Res(decay_grid.np);

   if(formfactortag!=1)
   {
    if(ff_ReadInn(rundef,Res,decayname)){cout<<"Inn!"<<endl;}else{cout<<"We abort the process. Check file!"<<endl; return 0;}
   }else
   {
           for(int i=0; i<decay_grid.np; i++)
           {
               Res(i)=1.0;
           }
           cout<<"Der formfactor ist jetzt eine Konstante Fpi=1"<<endl;

   }


    double hbar=6.58119514*1e-16;
    double couplinge = 4.0*Pi*(1.0/137.0);
    double pionlive= 1.0/(8.52*1e-17);

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //RARE DECAY
    complex<double> resultrare;
    if(decaytag==3)
    {
        resultrare=decay_rare_rate(decay_grid,Mresult2,Res, Resultfpion);
        cout<<"Der rare Zerfall wurde erechent!: R="<<resultrare<<"   Das PDG Ergebis wäre: "<<hbar*pionlive*6.36*1e-8*1e-9<<endl;
        cout<<"Vergleiche...: "<<resultrare/(hbar*pionlive*6.36*1e-8*1e-9)<<endl;
    }



    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //Dalitz DECAY
    complex<double> resultdalitz;
    if(decaytag==2)
    {
        resultdalitz= three_body_phase(decay_grid,Res,Mresult2,real(Resultfpion));
        cout<<endl<<"Der Dalitz Zerfall wurde erechent!: D="<<resultdalitz<<"    Das PDG Ergebnis wäre: "<<hbar*pionlive*0.01174*1e-9<<endl;
        cout<<"Vergleiche...: "<<resultdalitz/(hbar*pionlive*0.01174*1e-9)<<endl;
    }



    //cout<<"Ende Gelaende! :)      :First value of Gamma="<<4.0*Pi*Pi*fpion*Res(0)<<"  ,  "<<4.0*Pi*Pi*Resultfpion*Res(0)<<endl;
    time(&dasende); cout<<endl<<"Der pi->gamma gamma Process braucht: Stunden="<<difftime(dasende,starting)/(3600)<<"  , Minuten="<<(difftime(dasende,starting)/(3600)-trunc(difftime(dasende,starting)/3600))*60<<endl;




    return 0;
}

