
   PROGRAM test
   
   IMPLICIT NONE
   
   integer nIn, i1
   real(8) Q2, S0
   complex(8) k2, kQ
   
          nIn = 100
   


          open( 2, file="__QPV_Grid_in.dat")      

     	    write( 2, *)      	    
     	    write( 2, '(5X,I15)') nIn
     	    write( 2, *)   
     	    write( 2, '(5X,A15,15X,A15,15X,A15)') 'k2', 'kQ', 'Q2'  
     	    write( 2, *)  
     	    
     	    DO i1 = 1,nIn      
     	    	
     	    	 Q2 = -0.02D0 + 2D0*REAL(i1-1)/REAL(nIn-1)
     	    	 
     	    	 k2 = 1.D-2
     	    	 
     	    	 kQ = sqrt(k2*(1D0,0D0))*sqrt(Q2*(1D0,0D0))*0.5D0
!      	    	 
!      	    	 k2 = -0.1D0**2 ! + REAL(i1-1)/REAL(nIn-1) ! + 2D0*0.5D0*(0D0,1D0)*sqrt(REAL(i1-1)/REAL(nIn-1)) * 0.1D0
!      	    	 
!      	    	 S0 = REAL(i1-1)/REAL(nIn-1)
     	    	 
!      	    	 k2 = 3D0*S0/2D0
!      	    	 Q2 = 2D0*S0

     	    	 write( 2, '(5X,5D15.7)') k2, kQ, Q2 
     	    	 
     	    END DO   

     	    close(2)    
   
   END PROGRAM test