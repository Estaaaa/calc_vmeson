//
// Created by Esther Weil on 12.07.16.
//

#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <complex>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>

using namespace std;

#include "Toolbox/ToolHeader.h"


#define mo 0.0037
#define Mpion 0.139
#define Mv 0.769
#define sigma 0.5                   //decay
#define eta 0.5                     //pion
//#define fpion 0.092
#define elm 1.6021766208e-19
#define alphae 1.0/137.036
#define hbar 6.58119514*1e-16
#define pionlive 1.0/(8.52*1e-17)

#define lam2 1000.0



#include "DSEHeads/DSEHeader.h"
#include "DecayHeads/pi_ep_em_4.h"





#include "Toolbox/fourvec_ops.h"



int main (int argc, char * const argv[]) {

    time_t starting, dasende;
    time(&starting);
    time(&starting);

    string rundef ="data/output_20/";

    int mDec=300;
    int nzDec=16;
    int nyDec=8;
    int n_s=0;
    int nphi=0;


    int decaytag = 3;
    double xend,xbegin,Q,M;
    xend = 1.0e3;
    xbegin = 1.0e-2;
    Q = sqrt(pow(0.134,2)-pow(0.000511,2));
    M=0.134;
    double fpion=0.092;




    grid g(mDec,nzDec,nyDec,nphi,n_s);
    g.init(-1.0,1.0,1, decaytag, xbegin, xend);

    g.set_p(M,decaytag);

    int nFpi=40*16;
    vecd Fpi; vecd q2Fpi; vecd q2grid(g.np); vecd Fpin(g.np);

    double h1,h2,h3;
    ifstream inn; string adress="data3/output_0/decay_gamma.txt";
    inn.open(adress.c_str(),ios::in);
    if(inn.is_open())
    {
        string daten;
        while(getline(inn,daten))
        {
            daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

            if(daten[0] == '#') {continue;}
            stringstream(daten) >> h1 >> h3  >>h2 >> h3 >> h3 >> h3 >> h3 >> h3 >> h3 >> h3;
            if(daten=="#") { continue;}
            if(daten=="") {continue;}

            Fpi.push_back(h2);
            q2Fpi.push_back(h1);
        }
        cout<<Fpi.size()<<endl;
        if(Fpi.size()-1 == nFpi){cout<<"Dimesion fits.."<<endl;}else{cout<<"The file hast the wrong dimesions! :( Aborted"<<endl; return 0;}

    }else{cout<<"No data file found!"<<endl; return 0;}

    inn.close();

    Interpol IFpi;
    IFpi.spline_update_all(q2Fpi,Fpi);

    for(int i=0; i<g.nz; i++)
    {
        for(int j=0; j<g.m; j++)
        {
            q2grid[j+i*g.m]=real(g.q2[j]);
        }

    }


    IFpi.spline_interpol(q2grid,Fpin);

    string buffer = "data/test_ff.txt";
    ofstream f(buffer.c_str());
    for (int k = 0; k <g.m ; ++k) {
        f<<q2grid[k]<<tab<<Fpin[k]<<endl;
    }
    f.close();

    cdouble result;

    cout<<"..........Starting with the Decay calc now"<<tab<<"m="<<mDec<<tab<<"z="<<nzDec<<tab<<"y="<<nyDec
        <<tab<<"nphi="<<nphi<<tab<<"eps="<<tab<<xbegin<<tab<<"lambda="<<xend<<tab<<"M="<<M<<tab<<"Q="<<Q<<endl;


    result=decay_rare_rate_4(g,M,Fpin,fpion);
    double mathematicaresult=0.0;

        cout<<"Here comes the resutl: "<<result+mathematicaresult
        <<"   Das PDG Ergebis wäre: "<<hbar*pionlive*6.36*1e-8*1e-9
        << "   Das KTeV Ergebis wäre: "<<hbar*pionlive*7.48*1e-8*1e-9<<endl;

    A0_integration(g,xbegin,Fpin);

    time(&dasende);give_time(starting,dasende,"All");

    return 0;
}
