#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <complex>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>

using namespace std;

#include "Toolbox/ToolHeader.h"

#define Mpion 0.139
#define Mv 0.769
#define sigma 0.5                   //decay
#define eta 0.5                     //pion
#define fpion 0.093
#define elm 1.6021766208e-19
#define alphae 1.0/137.036

#define lam2 4.0e+4

//#define force 1.0

#include "DSEHeads/DSEHeader.h"
#include "BSEHeads/BSEHeader.h"
#include "ffactor_qphv.h"
#include "ffactor_qphv_addon.h"
#include "DecayHeads/DecayHeader.h"
#include "quarkphotonoutput.h"



/*------------------------------------START MAIN-----------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
int main (int argc, char * const argv[]) {

    time_t starting, dasende;
    time(&starting);

    string folder="01";

    string rundef ="../../data/data"+folder+"/output";
    string q_data_name="../../data/data"+folder+"/input/";

    double IRcutoff=1.0e-4;
    double UVcutoff=1.0e+3;

    int mQuark=150;
    int nQuark=32;

    int mDec=50;
    int nzDec=16;
    int nyDec=28;

    int mBSA=45;
    int nzBSA=12;
    int nyBSA=32;

    int nphi=0;

    int n_s=150;
//    int n_s=0;

    int BSEcalctag =1;
    int Chicalctag=1;
    int round= 1;
    int calc_quark3 =1;
    int formfactortag =1;
    int decaytag=1;
    int bse_interpolate=0;
    int QPV_interpolation=1;

    int Chiraltag = 0;     ///!!!!!!!!!!!!!!!!!!!!! =0
    int PauliVillartag=1;
//    double quarkmass=0.0037;
    double quarkmass=0.00358706;
//    double quarkmass=0.00357177;
    double distri_para=0.5;


    int calc_quark3_steps=0; //dummy index that I still need cause I programmed it this way

    int AmpNumber=4;
//    double Mresult2=0.135483;
//    double Mdecay=0.135483;

    double Mresult2=0.135;
    double Mdecay=0.135;
    double Mforfurthercalcs=0.135;


    double m_electron=0.0005109989461;
    double Resultfpion; // 0.0920981;



//    Input_reader(argc,argv,mDec,nzDec,nyDec,nyBSA,rundef,BSEcalctag,Chiraltag,PauliVillartag, round, mQuark, nQuark, calc_quark3, calc_quark3_steps,
//                 formfactortag, decaytag, n_s, quarkmass,Mresult2, Mdecay);
    Input_reader_new(argc,argv,mDec, nzDec, nyDec, folder, n_s, rundef);

    int steps=0; //if(rundef=="data6/output_0/"){steps=0;}else{steps=atoi(argv[5]);}

    if(PauliVillartag==1){if(Chiraltag==0){q_data_name=q_data_name+"PV_";}else{q_data_name=q_data_name+"PV_CL_";}}
    else if(PauliVillartag==0){if(Chiraltag==0){}else{q_data_name=q_data_name+"CL_";}}
    else{cout<<"PauliVillartag is wrong! ABORTED"<<endl; return 0;}

    cout<<"Starting....."<<endl;
    cout<<"The Parameter of this session: m="<<mDec<<" , ny="<<nyDec<<" , nz="<<nzDec<<"  BSA para: ny="<<nyBSA<<endl;
    cout<<"PauliVillar= "<<PauliVillartag<<" , Chiraltag= "<<Chiraltag<<" ,BSEcalctag= "<<BSEcalctag<<" , folder="<<atoi(argv[5])<<"   ,  calc_quark3="<<calc_quark3<<" ,formfactortag="<<formfactortag<<" ,decaytag="<<decaytag<<endl;
    cout<<"IRcut="<<IRcutoff<<"  ,UVcut="<<UVcutoff<<" , rundef="<<rundef<<" , Amp#="<<AmpNumber<<" , round="<<round<<endl;
    cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;

    string decayname;
    if(decaytag ==3 || decaytag ==4 || decaytag == 1)
    {
        decayname="gamma"; if(decaytag!=3){cout<<"In this session we are calculating the pi->gamma gamma decay"<<endl;}
        else{cout<<"In this session we are calculating for the pi->e+ e- :rare decay"<<endl;} n_s=0;
    }
    else if(decaytag==2)
    {

     if(n_s==0 ){cout<<"The Darlitz n_s has been automatic set"<<endl; n_s=12;}
     decayname="dalitz";cout<<"In this session we are calculating the pi->gamma gamma for the pi -< gamma e+ e-  :Dalitz decay"<<endl; cout<<"n_s="<<n_s<<endl;
    }else{cout<<"Which decay did you want to calculate? "<<endl; return 0;}
    cout<<"________________________________________________________"<<endl;

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //real QUARK propagator---------------------------------------------------------------------------------------------------------
    quark_prop quarkr(mQuark,nQuark, IRcutoff, UVcutoff);


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    // grid-  bse-----------------------------------------------------------------------------
    grid bse_grid(mBSA,nzBSA,nyBSA);
//    bse_grid.init(-1.0,1.0,4, decaytag, IRcutoff, UVcutoff);

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    // grid- decay --------------------------------------------------------------------------
    grid decay_grid(mDec,nzDec,nyDec,nphi,n_s);
    decay_grid.init(-1.0,1.0,4, decaytag, IRcutoff, UVcutoff);


    // Should not usually be in here, but setting the grid the same cause the interpolation isn't good.
    bse_grid=decay_grid; mBSA=mDec; nzBSA=nzDec; nyBSA=nyDec;
//      decay_grid=bse_grid; mDec=mBSA; nzDec=nzBSA; nyDec=nyBSA;


    double diff=0.001;
    if(Chiraltag==1){Mresult2=0.001; quarkmass=0.0;cout<<"The quark & bse are in chiral limit!"<<endl;}
    cout<<"The mass of the bound state in this session M="<<Mresult2<<endl;


    // BSA amplitude
    //Comment: Chebys can be activated by adding the number of Chebys at the end as an argument of the bse after AmpNumber.

    bse_amplitude bse(Mresult2, mBSA,nyBSA,nzBSA,distri_para, AmpNumber); bse.fpi=Resultfpion;
    bse.gridinit(bse_grid,0);

    // Calculation of the BSE amplitude and the quark-------------------------------------------------------------------------
    time_t bseend, bsestart; time(&bsestart);
    if(Chicalctag==0){BSEcalctag=5;}

    bs_amplitude_calc(bse,quarkr, Mresult2, quarkmass,  rundef, q_data_name,  PauliVillartag,  round, diff,  IRcutoff,  UVcutoff, BSEcalctag,  Resultfpion );

    time(&bseend); give_time(bsestart,bseend, "BSE");
    cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl<<endl;

//    cdouble rp0;
//    rp0=chiral_condensate(rundef);
//
//    cout<<"Hier ist das Chirale Condensat: "<<rp0<<tab<<1.0/0.091*rp0<<endl;

    //Interpolation of the BSE if grid isn't the same.   -Is not working so far
    if(decay_grid.m != bse_grid.m ||decay_grid.nz != bse_grid.nz)
    {
        cout<<"We need to interpolate the BSE onto the decay grid!"<<endl; bse_interpolate=1;
        bse_amplitude bse_temp(Mresult2,mDec,nyDec,nzDec,distri_para, AmpNumber);
        bse_temp.gridinit(decay_grid,0);
        BSE_interpolator(bse, bse_temp);

        bse=bse_temp;

//        cout<<"Test: "<<bse_temp.Amp[0][0]<<tab<<bse.Amp[0][0]<<tab<<bse_temp.Amp[3][6]<<tab<<bse.Amp[3][6]<<endl;


    }else{cout<<"BSE grid and Decay Grid are the same."<<endl;}

    //Trying to compare data with Pauls complex quark
//
//   ofstream dat4("parameter.txt");
//    for (int i = 0; i < quarkr.m; i++) {
//        dat4 << real(quarkr.p2[i])<<tab;
//    }
//
//
//    for (int i = 0; i < quarkr.m; i++) {
//        dat4 << quarkr.radw[i]<<tab;
//    }
//    for (int i = 0; i < quarkr.n; i++) {
//        dat4 << quarkr.ang[i]<<tab;
//    }
//    for (int i = 0; i < quarkr.n; i++) {
//        dat4 << quarkr.angw[i]<<tab;
//    }
//    dat4.close();
//
//
//    complex<double> result[2];
//    cdouble pc=0.1+ ii*0.1; quarkr.Z2=quarkr.Zm=1.0;
//    inte_com(result, quarkr, pc, PauliVillartag);
//    cout<<"This is my result: A="<<quarkr.Z2 + quarkr.Z2 * quarkr.Z2 * result[1]<<tab<<quarkr.Z2 + quarkr.Z2 * quarkr.Z2 * result[0]<<endl;
//
//
//return 0;



    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //Complex propagators
    VectorXcd Ak1(mDec*nzDec);
    VectorXcd Bk1(mDec*nzDec);
    VectorXcd Ak2(mDec*nzDec);
    VectorXcd Bk2(mDec*nzDec);
    cout<<"_______|S(k1), S(k2)|__________________"<<endl;
    if(BSEcalctag==3 || bse_interpolate==1) // || BSEcalctag == 0 )
    {
        //Propagator an k1--------------------------------------------------------------------
         cout<<"_____|Calculating the complex quark at k_+...|________"<<endl;
         ComQuarkpm(mDec,nzDec,quarkr,bse.p2,bse.zqp, Ak1, Bk1, 1, 1, rundef,Mresult2, sigma, PauliVillartag);
         cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;


         //Propagator an k2--------------------------------------------------------------------
         cout<<"_____|Calculating the complex quark at k_-...|________"<<endl;
         ComQuarkpm(mDec,nzDec,quarkr,bse.p2,bse.zqp, Ak2, Bk2, 2, 1, rundef,Mresult2, sigma,PauliVillartag);
         cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;

    }else if(BSEcalctag<6)
    {
        read_inn_com(Ak1,Bk1,rundef+"complex_quark_pm_1.txt");
        read_inn_com(Ak2,Bk2,rundef+"complex_quark_pm_2.txt");
    }


    // saving values for CHI
    time_t chia, chib; time(&chia);
    cout<<"_______|Chi_BS|__________________"<<endl;
    bse_amplitude chi(Mresult2, mDec,nyBSA,nzDec,0.5,AmpNumber);
    chi.gridinit(decay_grid,0);
    if(Chicalctag==1)
    {
        BSA_Chi(bse,chi,Mresult2, Ak1, Bk1, Ak2, Bk2, rundef);  cout<<"Calculated Chi."<<endl;
    }else if(Chicalctag==0)
    {
        //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
        chi.readInn(7,2,rundef);
        Resultfpion=chi.fpi; Mresult2=chi.Mbse;
        cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl<<endl;

    }else if(Chicalctag > 1){cout<<"There is no need for a BSE wavef. in this session!"<<endl;}

    time(&chib); give_time(chia,chib,"Chi");

    return 0;
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<

    //Grid stuff (setting the p_vector for the S(k3) calculation and loop integral of decay)

//       decay_grid.set_p(Mresult2,decaytag);
//    decay_grid.set_p(500,1.0e-16,10e+4,Mresult2);
//    decay_grid.set_p(150,1.0e-4,10e+4,Mresult2);
//    decay_grid.set_p(200,1.0e-2,1.0e+2,Mresult2);
//    decay_grid.set_p(300,300,1.0e-6,10e+4,Mresult2, 1.0e-1);
//    decay_grid.set_p_not_log(200,200,1.0e-10,10e+4,Mresult2, 1.0e-2);
//   decay_grid.set_p_dev(600,1.0e-6,10e+4,Mresult2);
//    decay_grid.set_p_2ep2em_direct(25,25,Mresult2,m_electron);
//    decay_grid.set_p_epemg(Mresult2,m_electron);
//    decay_grid.set_p_allrange(25,10e-2,1.0e+2,Mresult2);
    decay_grid.set_p_certainvalues(Mresult2);
//    decay_grid.set_p_onevirtual(50,Mresult2);
//    decay_grid.set_p_2ep2em_indirect(100,100,100,20,20,Mresult2,m_electron,rundef);

//    decay_grid.gridtag=6;

    decay_grid.write(decayname,rundef); //for checking the grid!

    output_qphv_grid(rundef,Mresult2,0.5,decay_grid,1);
    output_qphv_grid(rundef,Mresult2, 0.5, decay_grid,2);


    cout<<"-----|The QPV vertex need to be externally calcualted/interpolated|----"<<endl;
    time_t starting6, dasende6;
    time(&starting6);
    if(QPV_interpolation==1)
    {
        string helper="cd ../interpolate_qpv/data/interpol"+folder+" ; ./interpolateprase.sh ";
//        string helper="cd ../interpolate_qpv/; ./interpolateprase.sh ";
        int dummy=system(helper.c_str());

    }else if(QPV_interpolation==2){
        string helper="cd ../interpolate_qpv/data/interpol"+folder+" ; ./copier.sh ";
        int dummy=system(helper.c_str());

    } else{cout<<"QPV already calculated earlier. Done"<<endl;}
    time(&dasende6);give_time(starting6,dasende6,"QPV");

    //cin.get();

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //Complex propagator k3:

    MatrixXcd Ak3(decay_grid.np,mDec*nzDec*nyDec);
    MatrixXcd Bk3(decay_grid.np,mDec*nzDec*nyDec);

    //-----------------------------------------------------------------------------------------------------------------------------------------
    //Propagator at k3--------------------------------------------------------------------
    cout<<"_____|Calculating the complex quark at k_3...|________"<<endl;
    time_t starting3, dasende3;
    time(&starting3);
    if(calc_quark3==1)
    {

        ComQuark3_grid(decay_grid,quarkr,Ak3, Bk3, 0.5, 1, calc_quark3_steps, Mresult2,PauliVillartag, decayname,q_data_name);


    }else if(calc_quark3==0)
    {
        cout<<"Reading inn..."<<endl;
        stringstream ss;
        ss << steps;
        string buffer=q_data_name+"com_quark_k3_"+decayname+"_"+ss.str()+".txt";
        read_inn_com_3(Ak3,Bk3,buffer);


    }else{cout<<"Upsi! What about the the quark at k3?...Change Tags!"<<endl; return 0;}

    time(&dasende3);give_time(starting3,dasende3,"Comquark3");


    cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;


    //Decay
    cout<<"_____|Calculating the DECAY|________"<<endl;

    time_t starting4, dasende4;
    time(&starting4);

    VectorXcd Res(decay_grid.np);

    if(formfactortag==0)
    {
        if(ff_ReadInn(rundef,Res,decayname)){cout<<"Inn!"<<endl;}

    }
    else if(formfactortag==1)
    {

       pigammagamma_qphv_grid_interpolation(decay_grid,Ak1, Bk1, Ak2, Bk2,Ak3, Bk3, chi, Res, Mresult2,rundef,1, Resultfpion,0, decayname);
       cout<<"Calculated Gamma "<<0<<": "<<4.0*Pi*Pi*fpion*Res(0)<<"  ,  "<<4.0*Pi*Pi*Resultfpion*Res(0)<<endl;
//       for(int i=0; i<decay_grid.np; i++)
//       {
//           Res(i)=4.0*Pi*Pi*fpion*Res(i);
//       }


    }
    else if(formfactortag>1){cout<<"What about the formfactor?"<<endl; return 0;}
    time(&dasende4);give_time(starting4,dasende4,"FormFactor");




    time(&dasende);give_time(starting,dasende,"All");



//    return 0;
}

