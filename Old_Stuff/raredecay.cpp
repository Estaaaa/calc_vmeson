#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <complex>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>

using namespace std;

#include "Toolbox/ToolHeader.h"


#define mo 0.0037
#define Mpion 0.139
#define Mv 0.769
#define sigma 0.5                   //decay
#define eta 0.5                     //pion
//#define fpion 0.092
#define elm 1.6021766208e-19
#define alphae 1.0/137.036

#define lam2 1000.0



#include "DSEHeads/DSEHeader.h"
#include "DecayHeads/pi_ep_em_1.h"
#include "DecayHeads/pi_ep_em_3.h"
#include "DecayHeads/pi_ep_em_2.h"




#include "Toolbox/fourvec_ops.h"



int main (int argc, char * const argv[]) {


    string rundef ="data/output_20/";

    int mDec=81;
    int nzDec=22;
    int nyDec=14;
    int n_s=0;
    int nphi=0;

//    int BSEcalctag = 1;
//    int Chiraltag = 0;
//    int PauliVillartag=0;
//    int quarkmass=0.0037;
//    int round= 1;

    int decaytag = 3;
    double xend,xbegin,Q,M;
    xend = 1.0e6;
    xbegin = 1.0e-7;
    Q = sqrt(pow(0.135,2)-pow(0.000511,2));
    M=-0.05;
    double fpion=0.093;

    int gpm[]={100};
    int gmnz[]={40};            //should be even otherwise we divide by 0.
    int gmny[]={0};

    double myints[]  ={-0.3,-0.25,-0.2,-0.15,-0.1,-0.05,-0.02,-0.01,-0.008,-0.005,-0.001,-0.0005, -0.0001,-0.00008,-0.00005,-0.00001,-0.000005,-0.000001,-0.0000005,-0.0000001};
    int Nm=sizeof(myints)/ sizeof(myints[0]);
    double Qdiv[]={0.0005,0.005,0.11,0.12,sqrt(pow(0.135,2)-pow(0.000511,2)),0.2,0.3};
    int NQ=sizeof(Qdiv)/ sizeof(Qdiv[0]);

//    vecd Massdiv(myints, myints + sizeof(myints) / sizeof(int) ); Massdiv.resize(Nm);
//    vecd realsaver;

    cdouble saveDR[Nm];
    double realsaveDr[Nm*NQ]; NQ=Nm=1;

for (int l = 0; l < NQ; ++l) {


for (int k = 0; k < Nm; ++k)
{
    mDec=gpm[0]; nzDec=gmnz[0]; nyDec=gmny[0];     M=myints[4]; Q=Qdiv[4]; //M=myints[k]; Q=Qdiv[l];

    grid g(mDec,nzDec,nyDec,nphi,n_s);
    g.init(-1.0,1.0,1, decaytag, xbegin, xend);

    g.set_p(M,decaytag); VectorXcd Fpi(g.np);

    for(int i=0; i<g.nz; i++)
    {
        for(int j=0; j<g.m; j++)
        {
            Fpi(1+j+g.m*i)=1.0/(1.0+g.q2[j]);
        }
    }
    cdouble result; cout<<"..........Starting with the Decay calc now"<<tab<<"m="<<mDec<<tab<<"z="<<nzDec<<tab<<"y="<<nyDec
                    <<tab<<"nphi="<<nphi<<tab<<"eps="<<tab<<xbegin<<tab<<"lambda="<<xend<<tab<<"M="<<M<<tab<<"Q="<<Q<<endl;

    result=decay_rare_rate_3(g,M,Fpi,fpion,Q);

    double hbar=6.58119514*1e-16;
    double pionlive= 1.0/(8.52*1e-17);

     cout<<"Here comes the resutl 3: "<<result<<"   Das PDG Ergebis wäre: "<<hbar*pionlive*6.36*1e-8*1e-9<<endl;
    saveDR[k]=result;
    realsaveDr[k+Nm*l]=real(result);

//    if(l==0){realsaveDr.push_back(real(saveDR[k]));}

//
//    result=decay_rare_rate_2(g,M,Fpi,fpion,Q);
//
//    cout<<"Here comes the resutl 2: "<<result<<"   Das PDG Ergebis wäre: "<<hbar*pionlive*6.36*1e-8*1e-9<<endl;

//    time_t starting3, dasende3;
//    time(&starting3);
//
//    result=decay_rare_rate_1(g,M,Fpi,fpion,Q);
//
//    cout<<"Here comes the resutl: "<<result<<"   Das PDG Ergebis wäre: "<<hbar*pionlive*6.36*1e-8*1e-9<<endl;
//
//    time(&dasende3);give_time(starting3,dasende3,"direct Methode, rare decay ");
//
//    cout<<"----------------------------------------------"<<endl;

}
    string name= "../data/rare/MassVsDR.txt";
//    writer(name,Nm,myints,saveDR,Qdiv[l]);


}

//    tk::spline s;
//    s.set_points(Massdiv,realsaveDr);
//
//    double Mx=0.135;
//
//    cout<<"The value at the on-shell piont: "<<s(Mx)<<endl;






    return 0;
}
