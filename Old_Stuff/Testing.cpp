#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <complex>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>
#include "../Eigen/Dense"
#include "Toolbox/interpolation.hpp"                //CW Spline Interplolation: compile interpolation.cpp as well!
using namespace Eigen;

using namespace std;

#define mo 0.0037

//#define EPS 3.0e-11
#define Pi 3.141592653589793
#define epsi 1.0e-2
#define cut2 1.0e+6

#define Mpion 0.139
#define Mv 0.769
#define sigma 0.5                   //decay
#define eta 0.5                     //pion
#define fpion 0.092
#define elm 1.6021766208e-19
#define alphae 1.0/137.036
//#define numAm 4                     //Im moment geht nur 1,4 da die write function nur die beiden hat!, ist aber drinne?

#define lam2 4.0e+4


//#ifndef ii
//#define ii std::complex<double> (0.,1.)
//#endif

//#ifndef i_
//#define i_ std::complex<double> (0.,1.)
//#endif

//typedef complex<double> dcx;
//typedef vector<dcx> vecdcx;
//typedef vector<double> vecd;
//typedef vector<vecdcx> vecvecdx;



#include "DSEHeads/DSEHeader.h"
#include "BSEHeads/BSEHeader.h"
#include "ffactor_qphv.h"
#include "ffactor_qphv_addon.h"
#include "DecayHeads/DecayHeader.h"




/*------------------------------------START MAIN-----------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
int main (int argc, char * const argv[]) {

    clock_t start;
    float timet;
    time_t starting, dasende;
    time(&starting);

    string rundef ="data/output";
    string q_data_name="data/input/";
    string com="";

    double IRcutoff=1.0e-2;
    double UVcutoff=1.0e+3;

    int mQuark=150;
    int nQuark=32;

    int mDec=20;
    int nyDec=12;
    int nzDec=24;
    int nyBSA=12;

    int nphi=12;

//    int n_E1=50;
//    int n_E2=50;
    int n_s=50;

    int BSEcalctag = 1;
    int Chiraltag = 0;
    int PauliVillartag=1;
    double quarkmass=mo;
    int round= 1;
    int calc_quark3 =1;
    int calc_quark3_steps=7;
    int formfactortag =2;
    int decaytag=0;
    int AmpNumber=4;



    if(argc<5){cout<<"Using set-up specified in main_BSE: "<<endl;rundef=rundef+"_0/";}
    else if(argc==5){cout<<"Reading Inn grid-points: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/";}
    else if(argc==6){cout<<"Reading Inn grid-points & later BSE is calculated: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; }
    else if(argc==7){cout<<"Reading Inn grid-points, BSE can be choosen to be calculated(1) or readInn(0) or masscalc(2): "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);}
    else if(argc==8){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);}
    else if(argc==9){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag, Pauli-Villar tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);PauliVillartag=atoi(argv[8]);}
    else if(argc==10){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag, Pauli-Villar tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);
        PauliVillartag=atoi(argv[8]); round=atoi(argv[9]);}
    else if(argc==12){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag, Pauli-Villar tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);
        PauliVillartag=atoi(argv[8]); round =atoi(argv[9]); mQuark=atoi(argv[10]); nQuark=atoi(argv[11]);}
    else if(argc==11 || argc==13){cout<<"Something is wrong here? Need angular and momentum number for real quark propagator & or the nuber of the complex quark k3 which is readInn"<<endl<<"Standard settings are running!"<<endl;}
    else if(argc==14){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag, Pauli-Villar tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);
        PauliVillartag=atoi(argv[8]); round =atoi(argv[9]); mQuark=atoi(argv[10]); nQuark=atoi(argv[11]); calc_quark3=atoi(argv[12]);calc_quark3_steps=atoi(argv[13]);}

    else if(argc==15){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag, Pauli-Villar tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);
        PauliVillartag=atoi(argv[8]); round =atoi(argv[9]); mQuark=atoi(argv[10]); nQuark=atoi(argv[11]); calc_quark3=atoi(argv[12]);calc_quark3_steps=atoi(argv[13]);formfactortag=atoi(argv[14]);}
    else if(argc==16){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag, Pauli-Villar tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);
        PauliVillartag=atoi(argv[8]); round =atoi(argv[9]); mQuark=atoi(argv[10]); nQuark=atoi(argv[11]); calc_quark3=atoi(argv[12]);calc_quark3_steps=atoi(argv[13]);formfactortag=atoi(argv[14]); decaytag=atoi(argv[15]);}
    else if(argc==17){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag, Pauli-Villar tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);
        PauliVillartag=atoi(argv[8]); round =atoi(argv[9]); mQuark=atoi(argv[10]); nQuark=atoi(argv[11]); calc_quark3=atoi(argv[12]);calc_quark3_steps=atoi(argv[13]);
        formfactortag=atoi(argv[15]); decaytag=atoi(argv[16]); n_s=atoi(argv[17]);}
    else if(argc>17){cout<<"Something is wrong here? Standart set-up runs. "<<endl;}

    if(PauliVillartag==1){if(Chiraltag==0){q_data_name="data/input/PV_";}else{q_data_name="data/input/PV_CL_";}}
    else if(PauliVillartag==0){if(Chiraltag==0){q_data_name="data/input/";}else{q_data_name="data/input/CL_";}}
    else{cout<<"PauliVillartag is wrong! ABORTED"<<endl; return 0;}

    cout<<"Starting....."<<endl;
    cout<<"The Parameter of this session: m="<<mDec<<" , ny="<<nyDec<<" , nz="<<nzDec<<"  BSA para: ny="<<nyBSA<<endl;
    cout<<"PauliVillar= "<<PauliVillartag<<" , Chiraltag= "<<Chiraltag<<" ,BSEcalctag= "<<BSEcalctag<<" , folder="<<atoi(argv[5])<<"   ,  calc_quark3="<<calc_quark3<<" ,formfactortag="<<formfactortag<<" ,decaytag="<<decaytag<<endl;
    cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;

    string decayname;
    if(decaytag==0 || decaytag ==3 || decaytag ==4 || decaytag == 1)
    {
        decayname="gamma"; if(decaytag!=1){cout<<"In this session we are calculating the pi->gamma gamma decay"<<endl;}
        else{cout<<"In this session we are calculating the pi->e+ e- :rare decay"<<endl;}
    }
    else if(decaytag==2)
    {
     decayname="dalitz";cout<<"In this session we are calculating the pi->gamma e+ e-  :Dalitz decay"<<endl; cout<<"n_s="<<n_s<<endl;
    }else{cout<<"Which decay did you want to calculate? "<<endl; return 0;}
    cout<<"________________________________________________________"<<endl;

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //real QUARK propagator---------------------------------------------------------------------------------------------------------
    quark_prop quarkr(mQuark,nQuark);

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    // grid------------------------------------------------------------------------------
    grid decay_grid(mDec,nzDec,nyDec,nphi,n_s);
    decay_grid.init(-1.0,1.0,1, decaytag, IRcutoff, UVcutoff);

    complex<double> Resultfpion;

    double Mresult2=0.1377; quarkmass=0.0037; double diff=0.001;
    if(Chiraltag==1){Mresult2=0.001; quarkmass=0.0;cout<<"The quark & bse are in chiral limit!"<<endl;}
    cout<<"The mass of the bound state in this session M="<<Mresult2<<endl;


    // BSA amplitude
    bse_amplitude bse(Mresult2, mDec,nyBSA,nzDec,eta, AmpNumber);
    bse.gridinit(decay_grid);




    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    // Calculation of the BSE amplitude and the quark-------------------------------------------------------------------------
    start = clock();

    bs_amplitude_calc(bse,quarkr, Mresult2, quarkmass,  rundef, q_data_name,  PauliVillartag,  round, diff,  IRcutoff,  UVcutoff, BSEcalctag,  Resultfpion );

    timet = (float)(clock() - start) / CLOCKS_PER_SEC;
    cout <<"BSE time: "<<timet << endl;
    cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl<<endl;


    //Grid stuff (setting the p_vector for the S(k3) calculation and loop integral of decay)
    decay_grid.set_p(Mresult2,decaytag);

    decay_grid.write(decayname,rundef); //for checking the grid!
    //|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<<


     //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //Complex propagators
    VectorXcd Ak1(mDec*nzDec);
    VectorXcd Bk1(mDec*nzDec);
    VectorXcd Ak2(mDec*nzDec);
    VectorXcd Bk2(mDec*nzDec);
    MatrixXcd Ak3(decay_grid.np,mDec*nzDec*nyDec);
    MatrixXcd Bk3(decay_grid.np,mDec*nzDec*nyDec);

    if(BSEcalctag==3 || BSEcalctag == 0 )
    {
        //Propagator an k1--------------------------------------------------------------------
         cout<<"_____|Calculating the complex quark at k_+...|________"<<endl;
         ComQuarkpm(mDec,nzDec,quarkr,bse.p2,bse.zqp, Ak1, Bk1, 1, 1, rundef,Mresult2, sigma, PauliVillartag);
         cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;


         //Propagator an k2--------------------------------------------------------------------
         cout<<"_____|Calculating the complex quark at k_-...|________"<<endl;
         ComQuarkpm(mDec,nzDec,quarkr,bse.p2,bse.zqp, Ak2, Bk2, 2, 1, rundef,Mresult2, sigma,PauliVillartag);
         cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;

    }else
    {
        read_inn_com(Ak1,Bk1,rundef,1);
        read_inn_com(Ak2,Bk2,rundef,2);
    }




    // saving values for chi
    bse_amplitude chi(Mresult2, mDec,nyBSA,nzDec,eta,AmpNumber);
    chi.gridinit(decay_grid);
    BSA_Chi(bse,chi,Mresult2, Ak1, Bk1, Ak2, Bk2, rundef);

    //-----------------------------------------------------------------------------------------------------------------------------------------
    //Propagator an k3--------------------------------------------------------------------
    cout<<"_____|Calculating the complex quark at k_3...|________"<<endl;
    time_t starting3, dasende3;
    time(&starting3);
    if(calc_quark3==1)
    {

        ComQuark3_grid(decay_grid,quarkr,Ak3, Bk3, sigma, 1, calc_quark3_steps, Mresult2,PauliVillartag, decayname);


    }else if(calc_quark3==0)
    {

        cout<<"not wrtitten jet!!"<<endl; return 0;
        //aborting=ReadInn_com_3(mDec, nzDec, nyDec,Ak3, Bk3, calc_quark3_steps, decay); if(aborting==1){cout<<"The k3 quark was not ReadInn! Aborting"<<endl; return 0;}


    }else{cout<<"Upsi! What about the the quark at k3?...Change Tags!"<<endl; return 0;}

   time(&dasende3); cout<<"S(k3) ausrechnen braucht: Stunden="<<difftime(dasende3,starting3)/(3600)<<"  , Minuten="<<(difftime(dasende3,starting3)/(3600)-trunc(difftime(dasende3,starting3)/3600))*60<<endl;

//    quark3_interpol(mDec,nzDec,nyDec,bse.p2,bse.zqp,y,Ak3,Bk3,sigma,1,7,Mresult2,PauliVillartag,0);

    cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;




    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //cout<<"_____|Calculating the DECAY for all amplitudes...|________"<<endl;


    start = clock();
    cout<<"_____|Calculating the DECAY both photon off shell|________"<<endl;

    time_t starting4, dasende4;
    time(&starting4);

    VectorXcd Res(decay_grid.np);

    if(formfactortag==0)
    {
        if(ff_ReadInn(rundef,Res,decayname)){cout<<"Inn!"<<endl;}

    }
    if(formfactortag==1)
    {

       pigammagamma_qphv_grid(decay_grid,Ak1, Bk1, Ak2, Bk2,Ak3, Bk3, chi, Res, Mresult2,rundef,1, real(Resultfpion),0, decayname);
       cout<<"Calculated Gamma "<<0<<": "<<4.0*Pi*Pi*fpion*Res(0)<<"  ,  "<<4.0*Pi*Pi*Resultfpion*Res(0)<<endl;
       for(int i=0; i<decay_grid.np; i++)
       {
           Res(i)=4.0*Pi*Pi*fpion*Res(i);
       }


    }else if(formfactortag==3)
    {
        //#pragma omp parallel for
        for(int i=0; i<41; i++)
        {
            VectorXcd Resu(decay_grid.np);

            if(i ==0 || i ==14){pigammagamma_qphv_grid(decay_grid,Ak1, Bk1, Ak2, Bk2,Ak3, Bk3, chi, Resu, Mresult2,rundef,1, real(Resultfpion),i,"gamma");
             cout<<"Calculated Gamma "<<i<<": "<<4.0*Pi*Pi*fpion*Resu(0)<<"  ,  "<<4.0*Pi*Pi*Resultfpion*Resu(0)<<endl;}

        }

    }else if(formfactortag!=0){cout<<"What about the formfactor?"<<endl; return 0;}
    time(&dasende4); cout<<"Gamma_0-Gamma_37 ausrechnen braucht: Stunden="<<difftime(dasende4,starting4)/(3600)<<"  , Minuten="<<(difftime(dasende4,starting4)/(3600)-trunc(difftime(dasende4,starting4)/3600))*60<<endl;


    double hbar=6.58119514*1e-16;
    double couplinge = 4.0*Pi*(1.0/137.0);
    double pionwidth= 1.0/(8.52*1e-17);

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //RARE DECAY
    complex<double> resultrare;
    if(decaytag==1)
    {
//        resultrare=decay_rare_rate(decay_grid,Mresult2,Res);
//        cout<<"Der rare Zerfall wurde erechent!: R="<<resultrare*pow(couplinge,4)<<"   Das PDG Ergebis wäre: "<<hbar*pionwidth*6.36*1e-8*1e-9<<endl;
    }



    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //Dalitz DECAY
    complex<double> resultdalitz;
    if(decaytag==2)
    {
        resultdalitz= three_body_phase(decay_grid,Res,Mresult2, real(Resultfpion));
        cout<<endl<<"Der Dalitz Zerfall wurde erechent!: D="<<resultdalitz<<"    Das PDG Ergebnis wäre: "<<hbar*pionwidth*0.01174*1e-9<<endl;
    }





    //cout<<"Ende Gelaende! :)      :First value of Gamma="<<4.0*Pi*Pi*fpion*Res(0)<<"  ,  "<<4.0*Pi*Pi*Resultfpion*Res(0)<<endl;
    time(&dasende); cout<<endl<<"Der ganze Process braucht: Stunden="<<difftime(dasende,starting)/(3600)<<"  , Minuten="<<(difftime(dasende,starting)/(3600)-trunc(difftime(dasende,starting)/3600))*60<<endl;



    return 0;
}

