#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <complex>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>
#include "Toolbox/fourvec.h"
#include "../Eigen/Dense"


using namespace std;
using namespace Eigen;


void trace_decay_qph(MatrixXcd &Chi, complex<double> &x1, complex<double> &y1, complex<double> &z1, complex<double> &x2, complex<double> &y2, complex<double> &z2, complex<double> &fl10, complex<double> &fl11, complex<double> &fl12, complex<double> &fl13,
                     complex<double> &fl14, complex<double> &fl15, complex<double> &fl16, complex<double> &fl17, complex<double> &fl20, complex<double> &fl21, complex<double> fl22, complex<double> &fl23, complex<double> &fl24, complex<double> &fl25, complex<double> &fl26, complex<double> &fl27,
                     complex<double> &Ak3, complex<double> &Bk3, four_vec &k3, four_vec &k4, four_vec &k5, four_vec &P, four_vec &q, four_vec &p, four_vec &l1, four_vec &l2,
                     complex<double> &E, complex<double> &F, complex<double> &G, complex<double> &H);
