//
// Created by esther on 05.05.17.
//

#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <complex>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>

using namespace std;

#include "Toolbox/ToolHeader.h"

#define Mpion 0.139
#define Mv 0.769
#define sigma 0.5                   //decay
#define eta 0.5                     //pion
#define fpion 0.093
#define elm 1.6021766208e-19
#define alphae 1.0/137.036

#define lam2 4.0e+4

//#define force 1.0

#include "DSEHeads/DSEHeader.h"
#include "BSEHeads/BSEHeader.h"
#include "ffactor_qphv.h"
#include "ffactor_qphv_addon.h"
#include "DecayHeads/DecayHeader.h"
#include "quarkphotonoutput.h"



/*------------------------------------START MAIN-----------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
int main (int argc, char * const argv[]) {

    time_t starting, dasende;
    time(&starting);

    string folder="01";

    string rundef ="data/data"+folder+"/output";
    string q_data_name="data/data"+folder+"/input/";



    double IRcutoff=1.0e-3;
    double UVcutoff=1.0e+3;

    int mQuark=150;
    int nQuark=32;

    int mBSA=16;
    int nzBSA=8;
    int nyBSA=8;


    int BSEcalctag =1;
    int Chicalctag=1;
    int round= 1;
    int bse_interpolate=0;

    int Chiraltag = 0;     ///!!!!!!!!!!!!!!!!!!!!! =0
    int PauliVillartag=0;
    double distri_para=0.5;
    double quarkmass=0.0037;



    int AmpNumber=4;
    double Mresult2=0.137;
//    Mresult2=0.121;


    double m_electron=0.0005109989461;
    double Resultfpion; // 0.0920981;



    int steps=0; //if(rundef=="data6/output_0/"){steps=0;}else{steps=atoi(argv[5]);}
    rundef=rundef+"_0/";

    if(PauliVillartag==1){if(Chiraltag==0){q_data_name=q_data_name+"PV_";}else{q_data_name=q_data_name+"PV_CL_";}}
    else if(PauliVillartag==0){if(Chiraltag==0){}else{q_data_name=q_data_name+"CL_";}}
    else{cout<<"PauliVillartag is wrong! ABORTED"<<endl; return 0;}

    cout<<"Starting....."<<endl;
    cout<<"The Parameter of this session: m="<<mBSA<<" , ny="<<nyBSA<<" , nz="<<nzBSA<<endl;
    cout<<"PauliVillar= "<<PauliVillartag<<" , Chiraltag= "<<Chiraltag<<" ,BSEcalctag= "<<BSEcalctag<<endl;
    cout<<"IRcut="<<IRcutoff<<"  ,UVcut="<<UVcutoff<<" , rundef="<<rundef<<" , Amp#="<<AmpNumber<<" , round="<<round<<endl;
    cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //real QUARK propagator---------------------------------------------------------------------------------------------------------
    quark_prop quarkr(mQuark,nQuark, IRcutoff, UVcutoff);


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    // grid-  bse-----------------------------------------------------------------------------
    grid bse_grid(mBSA,nzBSA,nyBSA);
    bse_grid.init(-1.0,1.0,4, IRcutoff, UVcutoff);


    //Comment if we are working in the Chiral limit.
    double diff=0.001;
    if(Chiraltag==1){Mresult2=0.001; quarkmass=0.0;cout<<"The quark & bse are in chiral limit!"<<endl;}
    cout<<"The mass of the bound state in this session M="<<Mresult2<<endl;


    // BSA amplitude
    //Comment: Chebys can be activated by adding the number of Chebys at the end as an argument of the bse after AmpNumber.

    bse_amplitude bse(Mresult2, mBSA,nyBSA,nzBSA,distri_para, AmpNumber);
    bse.fpi=Resultfpion;
    bse.gridinit(bse_grid);

    // Calculation of the BSE amplitude and the quark-------------------------------------------------------------------------
    time_t bseend, bsestart; time(&bsestart);
    if(Chicalctag==0){BSEcalctag=5;}

    bs_amplitude_calc(bse,quarkr, Mresult2, quarkmass,  rundef, q_data_name,  PauliVillartag,  round, diff,  IRcutoff,  UVcutoff, BSEcalctag,  Resultfpion );

    time(&bseend); give_time(bsestart,bseend, "BSE");
    cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl<<endl;

//    cdouble rp0;
//    rp0=chiral_condensate(rundef);
//
//    cout<<"Hier ist das Chirale Condensat: "<<rp0<<tab<<1.0/0.092*rp0<<endl;

//    //Interpolation of the BSE if grid isn't the same.   -Is not working so far
//    if(decay_grid.m != bse_grid.m ||decay_grid.nz != bse_grid.nz)
//    {
//        cout<<"We need to interpolate the BSE onto the decay grid!"<<endl; bse_interpolate=1;
//        bse_amplitude bse_temp(Mresult2,mBSA,nyBSA,nzBSA,distri_para, AmpNumber);
//        bse_temp.gridinit(decay_grid,0);
//        BSE_interpolator(bse, bse_temp);
//
//        bse=bse_temp;
//
////        cout<<"Test: "<<bse_temp.Amp[0][0]<<tab<<bse.Amp[0][0]<<tab<<bse_temp.Amp[3][6]<<tab<<bse.Amp[3][6]<<endl;
//
//
//    }else{cout<<"BSE grid and Decay Grid are the same."<<endl;}



    if(bse.nCheb!=0) {cout<<"Chi is not calculated cause I havent fixed the calc for Chebs yet.."<<endl; return 0; }


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //Complex propagators
    VectorXcd Ak1(mBSA*nzBSA);
    VectorXcd Bk1(mBSA*nzBSA);
    VectorXcd Ak2(mBSA*nzBSA);
    VectorXcd Bk2(mBSA*nzBSA);
    cout<<"_______|S(k1), S(k2)|__________________"<<endl;
    if(BSEcalctag==3 || bse_interpolate==1) // || BSEcalctag == 0 )
    {
        //Propagator an k1--------------------------------------------------------------------
        cout<<"_____|Calculating the complex quark at k_+...|________"<<endl;
        ComQuarkpm(mBSA,nzBSA,quarkr,bse.p2,bse.zqp, Ak1, Bk1, 1, 1, rundef,Mresult2, sigma, PauliVillartag);
        cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;


        //Propagator an k2--------------------------------------------------------------------
        cout<<"_____|Calculating the complex quark at k_-...|________"<<endl;
        ComQuarkpm(mBSA,nzBSA,quarkr,bse.p2,bse.zqp, Ak2, Bk2, 2, 1, rundef,Mresult2, sigma,PauliVillartag);
        cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;

    }else if(BSEcalctag<6)
    {
        read_inn_com(Ak1,Bk1,rundef+"complex_quark_pm_1.txt");
        read_inn_com(Ak2,Bk2,rundef+"complex_quark_pm_2.txt");
    }


    // saving values for CHI
    time_t chia, chib; time(&chia);
    cout<<"_______|Chi_BS|__________________"<<endl;
    bse_amplitude chi(Mresult2, mBSA,nyBSA,nzBSA,0.5,AmpNumber);
    chi.gridinit(bse_grid);
    if(Chicalctag==1)
    {
        BSA_Chi(bse,chi,Mresult2, Ak1, Bk1, Ak2, Bk2, rundef);  cout<<"Calculated Chi."<<endl;
    }else if(Chicalctag==0)
    {
        //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
        chi.readInn(7,2,rundef);
        Resultfpion=chi.fpi; Mresult2=chi.Mbse;
        cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl<<endl;

    }else if(Chicalctag > 1){cout<<"There is no need for a BSE wavef. in this session!"<<endl;}

    time(&chib); give_time(chia,chib,"Chi");



    time(&dasende);give_time(starting,dasende,"All");



    return 0;
}

