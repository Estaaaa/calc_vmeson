
/*{
class gamma_mat
{
  public:

    int li; Matrix4cd matg;

 public:
    gamma_mat(int lorentz)
    {

      li=lorentz;
      if(li==0){ matg<<0,0,1,0, 0,0,0,1, 1,0,0,0 ,0,1,0,0;}
      else if(li==1){ matg<<0,0,0,1 ,0,0,1,0, 0,-1,0,0, -1,0,0,0;}
      else if(li==2){ matg<<0,0,0,-ii, 0,0,ii,0, 0,ii,0,0, -ii,0,0,0;}
      else if(li==3){ matg<<0,0,1,0, 0,0,0,-1, -1,0,0,0 ,0,1,0,0;}
      else if(li==5){ matg<<-1,0,0,0, 0,-1,0,0, 0,0,1,0, 0,0,0,1;}
      else{cout<<"ERROR in gamma_mat"<<endl;}

    }



};

{
    double result;
    gamma_mat ga(a); gamma_mat gb(b); gamma_mat gc(c); gamma_mat gd(d); gamma_mat r(0); gamma_mat g5(5);

    r.matg=ii*g5.matg*ga.matg*gb.matg*gc.matg*gd.matg;
    result=real(r.matg.trace())/4;

    return result;

}

}*/

Matrix4cd gamma_mat(int li)
{
    Matrix4cd matg;
    if(li==0){ matg<<0,0,1,0, 0,0,0,1, 1,0,0,0 ,0,1,0,0;}
    else if(li==1){ matg<<0,0,0,1 ,0,0,1,0, 0,-1,0,0, -1,0,0,0;}
    else if(li==2){ matg<<0,0,0,-ii, 0,0,ii,0, 0,ii,0,0, -ii,0,0,0;}
    else if(li==3){ matg<<0,0,1,0, 0,0,0,-1, -1,0,0,0 ,0,1,0,0;}
    else if(li==5){ matg<<-1,0,0,0, 0,-1,0,0, 0,0,1,0, 0,0,0,1;}
    else{cout<<"ERROR in gamma_mat, index out of boundary."<<endl;}

    return matg;

}

double epsilon(int a, int b, int c, int d)
{
    double result;
    Matrix4cd r;
    if(a>5 || b>5 || c>5 || d>5){cout<<"ERROR! The index of epsilon is wrong, aborted."<<endl; return 0;}

    r=ii*gamma_mat(5)*(gamma_mat(a)*(gamma_mat(b)*(gamma_mat(c)*gamma_mat(d))));
    result=real(r.trace())/4;

    return result;

}

double gmunu(int nu, int mu)
{   double result;
    if(mu==nu){result=1;}
    if(mu!=nu){result=0;}
    return result;
}

Matrix4cd slashed(four_vec p)
{
    Matrix4cd result;
    for(int i=0; i<4; i++)
    {
        //gamma_mat g(i);
        result += gamma_mat(i)*p.inhalt[i];
    }
    return result;

}

complex<double> photon(int nu, int mu, complex<double> p) //only dirac structure
{
    complex<double> result;
    result=gmunu(nu,mu);
    return result;
}

Matrix4cd Se(four_vec p, double me) //Note: The electron propagator only contains the dirac structure not the denominator, need to be out in later!
{
    Matrix4cd result; Matrix4cd Inden; Inden<<1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1;
    result=-ii*slashed(p)+Inden*me;
    return result;

}








