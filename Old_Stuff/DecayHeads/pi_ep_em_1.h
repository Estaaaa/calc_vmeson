#ifndef PI_EP_EM_1_H
#define PI_EP_EM_1_H




//--------------------RARE: Pi-> e+ e- -----------------------------------------------------------------
Matrix4cd decay_rare_diracMat(four_vec &k6,four_vec &k4, four_vec &k5, double me, complex<double> Fpi)
{

    Matrix4cd diracM;

    for(int mu=0; mu<4;mu++)
    {
        for(int nu=0; nu<4;nu++)
        {
            for(int a=0; a<4; a++)
            {
                for(int b=0; b<4; b++)
                {
                    for(int alpha=0; alpha<4; alpha++)
                    {
                        for(int beta=0; beta<4; beta++)
                        {

                            diracM= diracM +(ii*ii)*photon(mu, a, k5*k5)*Fpi*epsilon(mu,nu, alpha, beta)*k4.inhalt[alpha]*k5.inhalt[beta]* photon(nu,b,k4*k4)
                                    *gamma_mat(b)*Se(k6,me)*gamma_mat(a);
                        }
                    }
                }
            }

        }
    }

    return diracM;



}
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
Matrix4cd decay_rare_inte_1(grid &g, VectorXcd &Fpigamma, four_vec &P, four_vec &p1, four_vec &p2, double me)
{

    Matrix4cd result;
    Matrix4cd sum2, sum3;
    Matrix4cd diracM; complex<double> Fpi;
    four_vec p,k4,k5,k6, p12;
    double z, zw,y, yw,rw; //phi,phiw;
    complex<double> p_p,denom;


    for(int j=0; j<g.nz; j++)
    {
        z=g.z[j]; zw=g.z_weight[j];

        sum2=Matrix4cd::Zero(4,4);
        for(int i=0; i<g.m ;i++)
        {
            p_p=g.q2[i];
            rw=g.q2_weight[i];
            sum3=Matrix4cd::Zero(4,4);
            for(int i_y=0; i_y<g.ny; i_y++)
            {
               y=g.y[i_y]; yw=g.y_weight[i_y];
//               sum=Matrix4cd::Zero(4,4);
//               for(int i_phi=0; i_phi<g.n_phi; i_phi++)
//               {
//                    phi=g.phi[i_phi]; phiw=g.phi_weight[i_phi];

//                    p.inhalt[0]=sqrt(p_p)*sqrt(1.0-z*z)*sqrt(1.0-y*y)*sin(phi);
//                    p.inhalt[1]=sqrt(p_p)*sqrt(1.0-z*z)*sqrt(1.0-y*y)*cos(phi);
//                    p.inhalt[0]=0; p.inhalt[1]=sqrt(p_p)*sqrt(1.0-z*z)*sqrt(1.0-y*y);
//                    p.inhalt[2]= sqrt(p_p)*sqrt(1.0-z*z)*y; p.inhalt[3]=sqrt(p_p)*z;
                   p.spherical_fv(p_p,z,y);

                   Fpi=Fpigamma(1+i+g.m*j);


//                    k4=0.5*(p+P);
//                    k5=0.5*(p-P);
                    p12=(p1+p2)*0.5;
                    k4=0.5*(p+P)+p12;
                    k5=0.5*(p-P)+p12;
                    k6=k5-p2;

                    denom=1.0/((k4*k4)*(k5*k5))*1.0/(k6*k6+me*me);
                    diracM=decay_rare_diracMat(k6,k4,k5,me,Fpi);
//               }

               sum3 = sum3 + diracM*denom*yw;

            }

            sum2 = sum2 + sum3*rw*p_p*p_p*0.5;

        }

        result = result+ sum2*sqrt(1.0-z*z)*zw;

    }

    double intconst=pow(1.0/(2.0*Pi),4)*2.0*Pi;

    return result*intconst;




}
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
complex<double> decay_rare_rate_1(grid &g, double M,VectorXcd &Fpigamma, complex<double> Resultfpion, double Q)
{
    double couplinge = 4.0*Pi*(1.0/137.0);  double me=0.000511;
    complex<double> result;
    Matrix4cd diracMat; Matrix4cd amp;
    four_vec p1,p2, P;

    double vorfac=sqrt(M*M/4.0-me*me)*1.0/(8.0*Pi*M*M);
    if(M<0){
        P.inhalt[3]=-M;
        p1.inhalt[2]=Q; imag(p1.inhalt[3])=-M/2.0;
        p2.inhalt[2]=Q; imag(p2.inhalt[3])=M/2.0;
//        cout<<"The pion mass is space-like"<<endl;
    }else
    {
        imag(P.inhalt[3])=M;
        p1.inhalt[2]=sqrt(M*M/4.0-me*me); imag(p1.inhalt[3])=-M/2.0;
        p2.inhalt[2]=sqrt(M*M/4.0-me*me); imag(p2.inhalt[3])=M/2.0;
    }

    diracMat=decay_rare_inte_1(g,Fpigamma,P,p1,p2,Q);
//    cout<<"Here come the matrix"<<diracMat<<endl;

    Matrix4cd diracBar;
    diracBar= gamma_mat(0)*diracMat.conjugate()*gamma_mat(0);

    amp=diracMat*(-slashed(p2)-Matrix4cd::Identity(4, 4)*me)*diracBar*(-slashed(p1)+Matrix4cd::Identity(4, 4)*me);
    result=amp.trace();

    return result*vorfac*pow(couplinge,4)/pow((4.0*Pi*Pi*Resultfpion),2);



}


#endif // PI_EP_EM_1_H
