//
// Created by Esther Weil on 12.07.16.
//

#ifndef FORMFACTOR_PI_EP_EM_4_H_H
#define FORMFACTOR_PI_EP_EM_4_H_H


//--------------------RARE: Pi-> e+ e- -----------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
complex<double> decay_rare_inte_4(grid &g, vecd &Fpigamma, double M, double me)
{
// The integral expression is taken form the Upsala Thesis. It neatly reduces to a simple numerator and the y-integration
// is already evaluated in Mathematica, the other angle is trivial and gives a factor of (2 pi).
//
// The analytic structure of the form factor is chosen as: F=1/(1+(k4+k5)^2).

    cdouble result;
    cdouble sum2;
    cdouble lambda;
    complex<double> Fpi_num, Fpi_ana;
    double z, zw,y, yw,rw;  //phi,phiw;
    complex<double> p_p; double p;
    cdouble func;



    for(int j=0; j<g.nz; j++)
    {
        z=g.z[j]; zw=g.z_weight[j];

        sum2=0;
        for(int i=0; i<g.m ;i++)
        {
            p_p=g.q2[i]; p=sqrt(real(p_p));
            rw=g.q2_weight[i];



            Fpi_num=Fpigamma[i];

            Fpi_ana=1.0/(1.0+p_p);


            func = 32*p*(-log(pow(M,2) + pow(p,2) - 2*p*pow(pow(M,2) - 4*pow(me,2),0.5)*pow(1 - pow(z,2),0.5)) +
                         log(pow(M,2) + pow(p,2) + 2*p*pow(pow(M,2) - 4*pow(me,2),0.5)*pow(1 - pow(z,2),0.5)))*pow(M,2)*
                   pow(pow(M,2) - 4*pow(me,2),-0.5)*pow(1 - pow(z,2),0.5)*pow(pow(M,4) + pow(p,4) + 2*pow(M,2)*pow(p,2)*(-1 + 2*pow(z,2)),-1);

//            sum2+= rw*p_p*p_p*0.5*func*(Fpi_num-Fpi_ana);
            sum2+= rw*p_p*0.5*func*(Fpi_num-Fpi_ana);
           // cout<<Fpi_num-Fpi_ana<<tab<<func<<endl;

//            if((0.018225-0.005 < p*p) && (p*p < 0.018225+0.005) && (-0.1<z) && (z<0.1)){
//                cout<<z<<tab<<p*p<<tab<<tab<<Fpi_num-Fpi_ana<<tab<<func<<endl;
//            }


        }

        result += sum2*sqrt(1.0-z*z)*zw; //sum2*sqrt(1.0-z*z)*zw;

    }

    double intconst= pow(1.0/(2.0*Pi),4)*2.0*Pi; //cout<<"Compa this: "<<result<<tab<<dummy1<<endl;

    return result*intconst;




}
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
complex<double> decay_rare_rate_4(grid &g, double M, vecd &Fpigamma, complex<double> Resultfpion)
{
    four_vec P,p1,p2; double me=0.000511;

    imag(P.inhalt[3])=M;
    p1.inhalt[2]=sqrt(M*M/4.0-me*me); p1.inhalt[3]= - ii *M/2.0;
    p2.inhalt[2]=sqrt(M*M/4.0-me*me); p2.inhalt[3]= ii*M/2.0;



    double couplinge = 4.0*Pi*alphae;
    cdouble integral, result;

    integral=decay_rare_inte_4(g,Fpigamma, M,me);

    double vorfac=sqrt(M*M/4.0-me*me)*1.0/(4.0*Pi);
    cdouble Pvorfac=(-ii)*pow(couplinge,2)*me/(M*M);


//    result=pow(integral,2)*vorfac*pow(couplinge,4)/pow((4.0*Pi*Pi*Resultfpion),2);
//     result=pow(integral,2)*vorfac/pow((4.0*Pi*Pi*Resultfpion),2)*pow(alpha,4);

    result= vorfac* pow(Pvorfac*integral,2)*pow(1.0/(4.0*Pi*Pi*Resultfpion),2);

    return result;




}


//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
void A0_integration(grid g, double IRcut, vecd Fpi )
{
    /*
     * This is the the integration for calcuating the value of A0.
     * It return the value of A0.
     */

    complex<double> A0;
    complex<double> I;

    int n=g.q2.size();

    for (int i = 0; i <n ; ++i)
    {
        I +=  g.q2_weight[i]*1.0/g.q2[i]*Fpi[i];
    }

    A0=-log(IRcut*IRcut)*Fpi[0]-I;


    cout<<"Das ist der Wert von A0: "<<A0<<endl;


}




#endif //FORMFACTOR_PI_EP_EM_4_H_H
