#ifndef PI_EP_EM_3_H
#define PI_EP_EM_3_H




//--------------------RARE: Pi-> e+ e- -----------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
complex<double> decay_rare_inte_3(grid &g, VectorXcd &Fpigamma, four_vec &P, four_vec &p1, four_vec &p2, double me)
{

    cdouble result;
    cdouble sum2, sum3;
    cdouble dummy2=0.0;
    cdouble lambda;
    complex<double> Fpi;
    four_vec p,k4,k5,k6,p12;
    double z, zw,y, yw,rw;  //phi,phiw;
    complex<double> p_p,denom; double pp;
    cdouble test; double M=real(P.inhalt[3]); double Q=real(p1.inhalt[2]); cdouble func;


    for(int j=0; j<g.nz; j++)
    {
        z=g.z[j]; zw=g.z_weight[j];

        sum2=0;dummy2=0;
        for(int i=0; i<g.m ;i++)
        {
            p_p=g.q2[i]; pp=sqrt(real(p_p));
            rw=g.q2_weight[i];
            Fpi=Fpigamma(1+i+g.m*j);

            sum3=0;
//            for(int i_y=0; i_y<g.ny; i_y++)
//            {
//               y=g.y[i_y]; yw=g.y_weight[i_y];
//
//
//                p.spherical_fv(p_p,z,y);
//
//
////                    k4=0.5*(p+P);
////                    k5=0.5*(p-P);
//                    p12=(p1+p2)*0.5;
//                    k4=0.5*(p-P)+p12;
//                    k5=0.5*(p+P)+p12;
//                    k6=k4-p1;
//
//                    denom=1.0/((k4*k4)*(k5*k5)*(k6*k6+me*me));
//                    lambda=pow(P*P,2)+pow(k4*k4,2)+pow(k5*k5,2)-2.0*(P*P)*(k4*k4)-2.0*(P*P)*(k5*k5)-2.0*(k4*k4)*(k5*k5);
//
//               sum3 += lambda*denom*yw*Fpi;
//
//            }

            sum2 +=sum3*rw*p_p*p_p*0.5;

            func=(4.0*M*(-(pow(M - pp*z,2)*log(pow(M,2) + pow(pp,2) + 4.0*pow(Q,2) - 2.0*M*pp*z - 4.0*pp*Q*sqrt(1.0 - pow(z,2)))) +
                         pow(M + pp*z,2)*log(pow(M,2) + pow(pp,2) + 4.0*pow(Q,2) + 2.0*M*pp*z - 4.0*pp*Q*sqrt(1.0 - pow(z,2))) +
                         pow(M - pp*z,2)*log(pow(M,2) + pow(pp,2) + 4.0*pow(Q,2) - 2.0*M*pp*z + 4.0*pp*Q*sqrt(1.0 - pow(z,2))) -
                         pow(M + pp*z,2)*log(pow(M,2) + pow(pp,2) + 4.0*pow(Q,2) + 2.0*M*pp*z + 4.0*pp*Q*sqrt(1.0 - pow(z,2)))))/
                 (pow(pp,2)*(4.0*pow(me,2) + pow(pp,2))*Q*z*sqrt(1.0 - pow(z,2)));

            dummy2+= rw*p_p*p_p*Fpi*0.5*func;


        }

        result += dummy2*sqrt(1.0-z*z)*zw; //sum2*sqrt(1.0-z*z)*zw;

    }

    double intconst= pow(1.0/(2.0*Pi),4)*2.0*Pi; //cout<<"Compa this: "<<result<<tab<<dummy1<<endl;

    return result*intconst;




}
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
complex<double> decay_rare_rate_3(grid &g, double M,VectorXcd &Fpigamma, complex<double> Resultfpion, double Q)
{
    four_vec P,p1,p2; double me=0.000511; double Mpi=0.135;
    if(M<0){
        P.inhalt[3]=-M;
        p1.inhalt[2]=Q; p1.inhalt[3]=M/2.0;
        p2.inhalt[2]=Q; p2.inhalt[3]=-M/2.0;
//        cout<<"The pion mass is space-like"<<endl;
    }else
    {
        imag(P.inhalt[3])=M;
        p1.inhalt[2]=sqrt(M*M/4.0-me*me); p1.inhalt[3]= - ii *M/2.0;
        p2.inhalt[2]=sqrt(M*M/4.0-me*me); p2.inhalt[3]= ii*M/2.0;
    }


    double couplinge = 4.0*Pi*(1.0/137.0);
    cdouble integral, result;
    integral=decay_rare_inte_3(g,Fpigamma, P,p1,p2,me)*(-ii)*pow(couplinge,2)*me/(M*M);
    double vorfac=sqrt(Mpi*Mpi/4.0-me*me)*1.0/(4.0*Pi);


//    result=pow(integral,2)*vorfac*pow(couplinge,4)/pow((4.0*Pi*Pi*Resultfpion),2);
    result=pow(integral,2)*vorfac/pow((4.0*Pi*Pi*Resultfpion),2)*pow(couplinge,4);

    return result*2.0;




}


#endif // PI_EP_EM_3_H
