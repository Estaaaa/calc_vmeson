#ifndef PI_EP_EM_2_H
#define PI_EP_EM_2_H

//--------------------RARE: Pi-> e+ e- -----------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
complex<double> decay_rare_inte_2(grid &g, VectorXcd &Fpigamma, double M, double me)
{

//    double me=0.000511;
    complex<double> result=0.0;
    complex<double> sum2,sum3;
    complex<double> Fpi;
    complex<double> z, zw,rw;
    complex<double> p_p,inte;
    complex<double> tanobject;
    complex<double> test;
    double epsilon;
    if(M<0){epsilon= -4.0 * me * me / (M * M);}else{epsilon= 4.0 * me * me / (M * M);}

    for(int j=0; j<g.nz; j++)
    {
        z=g.z[j]; zw=g.z_weight[j];

        sum2=0.0; sum3=0.0;
        for(int i=0; i<g.m ;i++)
        {
            p_p=g.q2[i]/(M*M)*epsilon;
            rw=g.q2_weight[i];


                Fpi=Fpigamma(1+i+g.m*j);


                tanobject=(2.0*sqrt(p_p)*sqrt(1.0-epsilon)*sqrt(1.0-z*z))/(1.0+p_p);

                inte= ((pow(p_p,3/2)*(1.0-z*z))/((1.0-p_p)*(1.0-p_p)+4.0*p_p*z*z))*atanh(real(tanobject));

            sum2 += rw*p_p*inte*Fpi;
            sum3 += rw*p_p*inte;

        }

        result += sum2*zw;
        test += sum3*zw;

    }

    complex<double> intconst=pow(1.0/(2.0*Pi),3)*ii*1.0/sqrt(epsilon)*(1.0/epsilon);

//    cout<<"Value: "<<result*1.0/sqrt(epsilon)*ii<<tab<<result*intconst<<tab<<epsilon<<tab<<tanobject<<tab<<result<<endl;
//    cout<<"Test:  "<<test<<endl;

    return result*intconst;

}
//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
complex<double> decay_rare_rate_2(grid &g, double M,VectorXcd &Fpigamma, complex<double> Resultfpion, double Q)
{
    double me=0.000511; double Mpi=0.135;
    double couplinge = 4.0*Pi*(1.0/137.0);
    complex<double> result;
    Matrix4cd diracMat; Matrix4cd amp;
    four_vec p1,p2,P;
    complex<double> f3;
    double vorfac=sqrt(Mpi*Mpi/4.0-me*me)*1.0/(8.0*Pi*Mpi*Mpi);
    if(M<0){
        P.inhalt[3]=-M;
        p1.inhalt[2]=Q; imag(p1.inhalt[3])=-M/2.0;
        p2.inhalt[2]=Q; imag(p2.inhalt[3])=M/2.0;
//        cout<<"The pion mass is space-like"<<endl;
    }else
    {
        imag(P.inhalt[3])=M;
        p1.inhalt[2]=sqrt(M*M/4.0-me*me); imag(p1.inhalt[3])=-M/2.0;
        p2.inhalt[2]=sqrt(M*M/4.0-me*me); imag(p2.inhalt[3])=M/2.0;
    }

    f3=decay_rare_inte_2(g,Fpigamma,M,me);


//    diracMat=4.0*Q*gamma_mat(5)*f3;

//    cout<<"Value: "<<f3*(-4*me)<<endl;

//    Matrix4cd diracBar;
//    diracBar= gamma_mat(0)*diracMat.conjugate()*gamma_mat(0); cout<<"Test: "<<diracBar-diracMat<<endl;
//    Note that the complex conjugated version of the martix is the same. bar(A)=A

//    amp=diracMat*(-slashed(p2)-Matrix4cd::Identity(4, 4)*me)*diracMat*(-slashed(p1)+Matrix4cd::Identity(4, 4)*me);
//    result=amp.trace();
//
//    cdouble test;
//
//    test=2.0*pow(M*f3*4.0*Q,2); cout<<"This is a test:"<<test<<tab<<result<<endl;


    result=2.0*pow(M*f3*4.0*Q,2);

    return result*vorfac*pow(couplinge,4)/pow((4.0*Pi*Pi*Resultfpion),2)*pow(couplinge,4);




}



#endif // PI_EP_EM_2_H
