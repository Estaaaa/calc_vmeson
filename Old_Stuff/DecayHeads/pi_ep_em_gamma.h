/*
 * pi_ep_em_gamma.h
 *
 *Dalitz Decay Form code
 *
 */


//------------------------------------------------------------------------------------------------------
//--------------------DECAY DALITZ: Pi-> e+ e- gamma -------------------------------------------------------------
complex<double> three_body_phase(grid &g,VectorXcd &Fpigamma, double Mpi, double fpi)
{

    complex<double> Fpi,L;
    complex<double> s, sw;
    double me=0.000511;
    double vorfac =1.0/(pow(2,9)*pow(Pi,4)*Mpi*Mpi);  //1.0/pow(4.0*Pi*Pi*fpi,2)*


    complex<double> dalitz;
    complex<double> sum=0;

    for(int i=0; i<g.n_s; i++)
    {
        s=g.s[i]; sw=g.s_weight[i];
        Fpi=Fpigamma(i);

        L=sqrt(1.0-(4.0*me*me)/s)*sqrt(pow(((Mpi*Mpi+s)/(2.0*Mpi)),2)-s);


            //########################################################  //Commentary: factor of (4Pi/137)^3 1/(4Pi)^2

            dalitz =
                ((2*Pi*(pow(Mpi,4)*(3.986511787733379e-11 + 0.00007633456879633156*s) +
                        pow(Mpi,2)*(-7.973023575466757e-11 - 0.00015266913759266312*s)*s +
                        (3.986511787733379e-11 + 0.00007633456879633156*s)*pow(s,2)))/
                    pow(s,2))*pow(Fpi,2);


            //#######################################################

            sum += dalitz*sw*L;


    }

    return sum*vorfac;

}

//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------






