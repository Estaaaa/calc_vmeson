

#include "./Allgemein.h"
#include "./Global_utility/Vector_resize.h"
//#include "./Global_utility/Comparison.h"

#include "./Interpolation/Interpolation_polynomial.h"


int main(){
    //Anleitung Interpolation:

    // Die Klasse "Cartesin_Barycentric_interpolator" interpoliert ein Set aus
    // MDim Funktionen, definiert auf einem N-dimensionalen cartesischen Grid.
    //
    //
    // Funktionsweise, am Beispiel einer  2-dimensionalen Funktion:
    //
    //
    // Initializierung:
    Cartesin_Barycentric_interpolator<2, 10, std::complex<double>, double>    Interpolator;

    // Dim ist die Dimension des Grids.
    // MDim ist die Anzahl der Funktionen, die mit dem Interpolator interpoliert werden sollen.
    // y-type ist double oder complex.
    // x_type ist ebenfalls double oder complex.

    //Bevor der Interpolator genutz werden kann, muss er inintialisiert werden:


    //Pfad-klasse, die ein wenig die Arbeit  übernimmt, ein log-grid yu erzeugen:

    Log10Line_Legendre Line_Log;//Log-grid, Legendre-points
    Line_Legendre Line;//"Normal" grid, Legendre-points

    int N_super = 100;// Super index: dim_1*dim_2
    int N_1 = 10;//dim_1
    double IR_1 = 1e-4;// lower bound der ersten dimension. ACHTUNG "echter" lower bound, nicht der log des lower bounds!!
    double UV_1 = 1.e4;// upper bound der ersten dimension. ACHTUNG "echter" upper bound, nicht der log des upper bounds!!
    int N_2 = 10;//dim_2
    double IR_2 = 1e-3;// lower bound der zweiten dimension. ACHTUNG "echter" lower bound, nicht der log des lower bounds!!
    double UV_2 = 1.e4;// upper bound der zweiten dimension. ACHTUNG "echter" upper bound, nicht der log des upper bounds!!
    auto x_i = [&](int dim_i, int super_i){
        // dim_i ist der dimensions index
        // p_i ist der super_index
        // dieses Lambda muss also inder Lage sein aus dem dimensionsindex und dem super index
        // das korrekte x zu berechnen:
        // Regel für superindex:
        // super_i = N_2*index_1 + index2

        int index_2 = super_i%N_2;
        int index_1 = super_i/N_1;

        if(dim_i==0){
            return Line_Log.Get_x( IR_1,  UV_1,  N_1, index_1 );
        }
        if(dim_i==1){
            return Line.Get_x( IR_2,  UV_2,  N_2, index_2 );
        }
    };

    auto w_i = [&](int super_i){
        // p_i ist der super_index
        // dieses Lambda muss also inder Lage sein aus dem dimensionsindex und dem super index
        // das korrekte x zu berechnen:
        // Regel für superindex:
        // super_i = N_2*index_1 + index2
        // Achtung: Die Gewichte in jeder Dimension werden multipliziert

        int index_2 = super_i%N_2;
        int index_1 = super_i/N_1;

        double result = 1.;
        {
            result*= Line_Log.Get_w_interpolation( IR_1,  UV_1,  N_1, index_1 );
        }
        {
            result*= Line.Get_w_interpolation( IR_2,  UV_2,  N_2, index_2 );
        }

        return result;
    };

    Interpolator.Initialize(N_super, std::move(x_i), std::move(w_i) );


    //Um die Funktionswerte zu setzen, nutze:
    std::vector<double> y0(N_super, 3.);
    std::vector<double> y1(N_super, 3.);
    Interpolator.Set_y(0, y0);
    Interpolator.Set_y(1, y1);
    for (int i = 2; i < 10; ++i){
        Interpolator.Set_y(i, y0);
    }

    //der erste Index gibt an um welche der MDim dressing funktionen es sich handelt.

    //Um die Werte des grids zu bekommen, benutze:
    int dim_i = 0;
    int super_i = 10;
    auto x_value_0 = Interpolator.Get_x(dim_i, super_i);
    auto x_value_1 = Interpolator.Get_x(1, super_i);
    //super_i ist der super_index, bei dim_i handelt es sich um die konkrete dimension

    //Um einen konkreten Wert der dressing funktion zu bekommen benutze:
    int mdim_i = 0;
    std::complex<double> y_value = Interpolator.Get_y(mdim_i, super_i);
    // mdim gibt den konkreten index der dressing funktion an

    //Um zu interpolieren benutze:

    std::vector<std::complex<double>> y_interpolated(10);
    std::vector<double> x_values = {3.,2.};
    Interpolator.Interpolate(y_interpolated, x_values);

    std::cout<< x_values[0]<<" , "<<x_values[1]<<endl;

    // y_interpolated ist der container, der die interpolationsergebnisse fuer
    // alle 10 dressing-funktionen speichert.
    // x_values ist der Punkt an dem interpoliert werden soll.

    std::cout << y_interpolated[0] << endl;



    return 0;
}
