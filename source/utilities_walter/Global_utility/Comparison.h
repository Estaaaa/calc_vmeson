#pragma once

// Purpose:
// Compares floating point numbers.
// Very crude


// Attention: Dependencies on standard library is not resolved here
// and has to be included elsewhere




#include <complex.h>
#include <vector>

bool isApproximatelyEqual (double a, double b) ;



bool isApproximatelyEqual ( double a, double b, double epsilon) ;

bool isApproximatelyEqual ( std::complex<double> a,  std::complex<double> b) ;


bool isApproximatelyEqual ( std::complex<double> a,  std::complex<double> b, double epsilon) ;

bool isApproximatelyEqual_coarse (const double a, const double b) ;

bool isApproximatelyEqual (const std::vector<double> & a, const std::vector<double> & b) ;
bool isApproximatelyEqual (const std::vector<int> & a, const std::vector<int> & b) ;
