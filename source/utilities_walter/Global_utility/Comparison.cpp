//
// Created by paul on 24.08.17.
//

#include <limits>
#define _USE_MATH_DEFINES
#include <cmath>
#include "Comparison.h"


bool isApproximatelyEqual ( double a, double b) {
    if(abs(a)<1.e-13 and abs(b)<1.e-13){
        //treat them both as zero
        return true;
    }
    double maxRelDiff =1.0e-14;
    double diff = fabs(a - b);
    a = fabs(a);
    b = fabs(b);
    // Find the largest
    double largest = (b > a) ? b : a;

    if (diff <= largest * maxRelDiff)
        return true;
    return false;
}



bool isApproximatelyEqual ( double a, double b, double epsilon) {
    if(abs(a)<1.e-14 and abs(b)<1.e-14){
        //treat them both as zero
        return true;
    }
    double maxRelDiff =epsilon;
    double diff = fabs(a - b);
    a = fabs(a);
    b = fabs(b);
    // Find the largest
    double largest = (b > a) ? b : a;

    if (diff <= largest * maxRelDiff)
        return true;
    return false;
}

bool isApproximatelyEqual ( std::complex<double> a,  std::complex<double> b) {
    bool real_check = isApproximatelyEqual(real(a), real(b));
    bool imag_check = isApproximatelyEqual(imag(a), imag(b));
    // bool erg1 = std::fabs(real(a) - real(b) )< std::numeric_limits<double>::epsilon()*12.;
    // bool erg2 = std::fabs(imag(a) - imag(b) )< std::numeric_limits<double>::epsilon()*12.;
    // bool erg = erg1 and erg2;
    return real_check and imag_check;
}


bool isApproximatelyEqual ( std::complex<double> a,  std::complex<double> b, double epsilon) {
    bool real_check = isApproximatelyEqual(real(a), real(b),epsilon);
    bool imag_check = isApproximatelyEqual(imag(a), imag(b),epsilon);
    // bool erg1 = std::fabs(real(a) - real(b) )< std::numeric_limits<double>::epsilon()*12.;
    // bool erg2 = std::fabs(imag(a) - imag(b) )< std::numeric_limits<double>::epsilon()*12.;
    // bool erg = erg1 and erg2;
    return real_check and imag_check;
}

bool isApproximatelyEqual_coarse (const double a, const double b) {
    // return fabs (a - b) <= ((fabs (a) < fabs (b) ? fabs (b) : fabs (a)) *
    //     std::numeric_limits<double>::epsilon ());
    // bool result =std::fabs(a - b) < std::numeric_limits<double>::epsilon()*1000.;
    // cout << "result " << a << " " << b <<  " " << result << endl;
    return std::fabs(a - b) < std::numeric_limits<double>::epsilon()*1000.;
}

bool isApproximatelyEqual (const std::vector<double> & a, const std::vector<double> & b) {
    // return fabs (a - b) <= ((fabs (a) < fabs (b) ? fabs (b) : fabs (a)) *
    //     std::numeric_limits<double>::epsilon ());
    bool erg {true};
    for (int i = 0; i < a.size(); ++i){
        erg = std::fabs(a[i] - b[i]) < std::numeric_limits<double>::epsilon()*2.;
        if(erg==false){break;}
    }
    return erg;
}

bool isApproximatelyEqual (const std::vector<int> & a, const std::vector<int> & b) {
    // return fabs (a - b) <= ((fabs (a) < fabs (b) ? fabs (b) : fabs (a)) *
    //     std::numeric_limits<double>::epsilon ());
    bool erg {true};
    for (int i = 0; i < a.size(); ++i){
        erg = (a[i] - b[i])==0;
        if(erg==false){break;}
    }
    return erg;
}