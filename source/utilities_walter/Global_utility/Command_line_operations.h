#pragma once


// Purpose:
// SAdds a command line option to the commandline string


// Attention: Dependencies on standard library is not resolved here
// and has to be included elsewhere


void Add_commandline_option_static (int Increment_i, char* tba, auto & argc, auto & argv){
    //Local functions
    auto Add_option = [&](){
        argv[argc] = tba;//const_cast<char*>(tba);
    };
    auto Increment = [&](){
        argc+=Increment_i;
    };
    //Execution
    Add_option();
    Increment();
};
