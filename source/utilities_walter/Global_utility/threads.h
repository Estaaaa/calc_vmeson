#pragma once
#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>
//%%%GLOBAL%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
static thread_local int THREAD_NUM_MY__ = 0;
static thread_local int THREAD_PROGRESS_= 0 ;
static thread_local bool THREAD_LOCAL_FLAG_MY1_=false;
static thread_local bool THREAD_IS_PARALLEL_=false;
// __thread static int THREAD_NUM_MY__ = 0;
// __thread static bool THREAD_LOCAL_FLAG_MY1_=false;
// __thread static bool THREAD_IS_PARALLEL_=false;
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
class thread_pool {
	private:
	//&&&Variables&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	typedef std::function <void ()> task;
	std::vector<std::thread> the_pool;
	std::vector<int> pool_loop_start;
	std::vector<int> pool_loop_end;
	unsigned int num_thread=0;
	int workload_static=0;
	int work_load_total=0; 
	std::vector<int> Increment;
	std::mutex Mutex;
	public:	
	//&&&Functions&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	//***block_parallel()***********************************************************************************
	//
	//  Input:  Number of threads, function object for thread evaluation
	//	Output: Constructor
	//
	//	Description: Used for n threaded workteam 
	//	Additional:  Containes simple gurad to prevent starting of too many threads
	//				 Sets the GLOBAL thread local variable THREAD_NUM_MY__
	//				 Set Is_parallel_flag only just BEFORE starting the threads
	//				 Unset Is_parallel_flag IMMIDIEATLY after joining
	//****************************************************************************************************	
    template<typename T>
    void block_parallel(unsigned int num_thread_init, T && tbd) {
		//Sets parameters for thread pool
		num_thread=std::min(std::thread::hardware_concurrency(),num_thread_init);
		if(num_thread_init>num_thread){
			throw std::invalid_argument("invalid_argument: Set to many threads for block_parallel." ) ;
		}
		for(int i = 0; i < num_thread; ++i) {																//Starts pool
			the_pool.push_back(std::thread([=] () {
				  THREAD_NUM_MY__ = i;
				  THREAD_IS_PARALLEL_ = true;	
				  THREAD_PROGRESS_ = 0;															//Activate Is_parallel_flag flag
				  tbd();
				}));
		}
    }
	//***for_parallel()***********************************************************************************
	//
	//  Input:  Number of threads, loop_start, loop_end (int),  function object for thread evaluation
	//	Output: Constructor
	//
	//	Description: Used when parallizing a for loop. Static workload 
	//	Additional:  Containes simple guard to prevent starting of too many threads
	//		         Distributes workload evenly over possible threads
	//				 ASSUMES increment of ++ for the for loop
	//				 Structure of parallized for loop: for(int i; i<N; i++)
	//				 Sets the GLOBAL thread local variable THREAD_NUM_MY__
	//****************************************************************************************************
	template<typename T>
	void for_parallel(unsigned int num_thread_init, int loop_start, int loop_end, T && tbd) {//task tbd) {
		//Sets parameters for thread pool
		num_thread=std::min(std::thread::hardware_concurrency(),num_thread_init);					
		work_load_total= loop_end - loop_start;	
		//Handles case when workload is smaller than thread_num													
		if(work_load_total<num_thread){num_thread=work_load_total;}							
		//Calcs workload per thread
		workload_static = (loop_end - loop_start + 1)/num_thread;	
		//Sets progress related structures
		Increment.resize(num_thread,0);								
		//Calcs start and end points for threads 
		for(int i = 0; i < num_thread; ++i) {														
			//loop start
			if(i==0){pool_loop_start.push_back(loop_start);}
			else{pool_loop_start.push_back(pool_loop_start[i-1] + workload_static);}			
			//loop end
			if(i==(num_thread-1)){pool_loop_end.push_back(loop_end);}
			else{pool_loop_end.push_back(pool_loop_start[0]+(i+1)*workload_static);}
		}									
		//Starts pool							
		for(int i = 0; i < num_thread; ++i) {
			the_pool.push_back(std::thread([=] () {
				  THREAD_NUM_MY__ = i;
				  THREAD_PROGRESS_= 0;
				  THREAD_IS_PARALLEL_ = true;	
				  Increment[i] = (pool_loop_end[i] - pool_loop_start[i])/20;
				  tbd();
				}));
		}
    }  	
  	//***join()***********************************************************************
	//
	//  Input:  
	//	Output: 
	//
	//	Description: Joins the threads
	//******************************************************************************** 
    void join() {
      std::for_each(the_pool.begin(), the_pool.end(),
        [] (std::thread& t) {
			THREAD_NUM_MY__ = 0;
			THREAD_PROGRESS_ = 0;
			THREAD_LOCAL_FLAG_MY1_ = false;
			THREAD_IS_PARALLEL_ = false;
			t.join();
		});
        //Clears all thread structures
        the_pool.clear();
        pool_loop_start.clear();
        pool_loop_end.clear();
        num_thread=0;
        Increment.clear();
		workload_static=0;
		work_load_total=0; 
    }
	//***lock/unlock()*****************************************************************
	//
	//  Input:  
	//	Output: 
	//
	//	Description: puts and unputs lock
	//*********************************************************************************   
	template<typename T>  
	void Locked (T && lambda)
	{
		std::lock_guard<std::mutex> locker(Mutex);
		lambda(); 
	}	
 	//***Is_parallel()*****************************************************************
	//
	//  Input:  
	//	Output: Is_parallel(bool)
	//
	//	Description: checks if in parallel region or not
	//				 Relies on proper setting and unsetting the Is_parallel_flag flag	
	//*********************************************************************************   
	bool Is_parallel() { return THREAD_IS_PARALLEL_ ; }
 	//***get_num_threads()*************************************************************
	//
	//  Input:  
	//	Output: Number of (possible) running threads
	//
	//	Description: Number of (possible) running threads
	//**********************************************************************************   
    int get_num_threads() { return num_thread; }
 	//***get_thread_num()***************************************************************
	//
	//  Input:  
	//	Output: Returns pool-number of working thread
	//
	//	Description: Returns pool-number of working thread
	//				 If not in parallel region returns 0
	//**********************************************************************************            
    int get_thread_num() {
		return THREAD_NUM_MY__;
    }  	

    void Progress(int index){
    	int thread_num_local = get_thread_num();
    	if( index -  pool_loop_start[thread_num_local]  > (THREAD_PROGRESS_+1)*Increment[thread_num_local] ){

    		Locked(
    			[&](){
    				std::cerr << "\033[" + std::to_string(thread_num_local) +"B" << "\033[2K" << "\033[10D" <<   (THREAD_PROGRESS_+1)*5 << "%";
    				std::cerr << "\033[" + std::to_string(thread_num_local) +"A" ;
    			}
    		);
    		THREAD_PROGRESS_++;
    	}
    }
    void Progress_finalize(){
    	for (int i = 0; i < std::thread::hardware_concurrency(); ++i){
    		std::cout << std::endl;
    	}
    }
    void Progress_start(){
    	for (int i = 0; i < std::thread::hardware_concurrency(); ++i){
    		std::cout << std::endl;
    	}
    }    
 	//***get_threads_loop_start/end()***************************************************
	//
	//  Input:  
	//	Output: Start/end point of each threads for-loop chunk
	//
	//	Description: Start/end point of each threads for-loop chunk
	//**********************************************************************************       
    int get_threads_loop_start() { return pool_loop_start[get_thread_num()]; }
    int get_threads_loop_end() { return pool_loop_end[get_thread_num()]; }
 	//***get_thread_flag()***************************************************************
	//
	//  Input:  
	//	Output: Returns thread local bool flag value
	//
	//	Description: Returns thread local bool flag value
	//**********************************************************************************            
    bool get_thread_flag1() {
			return THREAD_LOCAL_FLAG_MY1_;
    } 
 	//***get_thread_flag()***************************************************************
	//
	//  Input:  
	//	Output: Returns thread local bool flag value
	//
	//	Description: Returns thread local bool flag value
	//**********************************************************************************            
    void set_thread_flag1(bool value) {
		 THREAD_LOCAL_FLAG_MY1_ = value;
    }     	    
};
