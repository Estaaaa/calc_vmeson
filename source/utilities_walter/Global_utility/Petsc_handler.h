#pragma once

// Purpose:
// Includes all Petsc related libraries and includes
// a petsc handler to set a global 'petsc is used flag'
// and calls the destructor if petsc was used

//header files
#include <slepceps.h>
#include <petscblaslapack.h>
#include <petscksp.h>
#include <petscsnes.h>


class Petsc_handler{
    // Variables******************
    bool Used_Petsc_FL = false;
    // Functions
    public:
        //***Set_petsc_used_FL()************************************************
        //
        //  Input:
        //	Output:
        //
        //	Description: Sets Used_Petsc_FL flag to true
        //               Can be called multiple times
        //**********************************************************************
        void Set_petsc_used_FL(){
           if(!Used_Petsc_FL){
               Used_Petsc_FL = true;
           }
        }
        //***Petsc_clean_up()***************************************************
        //
        //  Input:
        //	Output:
        //
        //	Description: Calls PetscFinalize if Used_Petsc_FL is true
        //**********************************************************************
        auto Petsc_clean_up(){
            if(Used_Petsc_FL){
                PetscErrorCode ierr;
                ierr = PetscFinalize();CHKERRQ(ierr);
            }
        }
};
