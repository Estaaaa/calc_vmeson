//
// Created by paul on 24.08.17.
//

#include <cmath>
#include "Grid_rules.h"
#include "../boost/math/special_functions.hpp"
#include "Allgemein.h"
//double pi = pi;

double Nodes_Equal_dist(double a, double b, int j, int n)// j starts at zero
{
    if(n==1){return (b-a)/2. +a; }
    return (b-a)/double(n-1)*double(j) + a ;
}


double Barycentric_weights_Equal_dist( int j, int n)
{
    return pow(-1.,j)*boost::math::binomial_coefficient<double>(n-1, j);
}

double Nodes_Tscheb_2kind( double a, double b, int j, int n)// j has to be added by 1, because it starts at 0
{

    double cos_sp;
    if(n==1)
    {
        cos_sp=0.;
    }
    else
    {
        cos_sp= cos(double(j+1)*pi/double(n+1) );

    }
    //double alpha = 0.9999999;
    //if(cos_sp==0.){cos_sp=1.;}
    //else{cos_sp = asin(alpha*cos_sp)/asin(alpha);}
    return (b-a)/2.*(-cos_sp+1.)+a;
}


double Nodes_Tscheb_Christian( double a, double b, int j, int n)// j has to be added by 1, because it starts at 0
{

    double cos_sp;
    if(n==1)
    {
        cos_sp=0.;
    }
    else
    {
        cos_sp= cos(double(j+1 - 0.5)*pi/double(n) );
    }
    //double alpha = 0.9999999;
    //if(cos_sp==0.){cos_sp=1.;}
    //else{cos_sp = asin(alpha*cos_sp)/asin(alpha);}
    return (b-a)/2.*(-cos_sp+1.)+a;
}



double Barycentric_weights_Tscheb_2kind(  int j, int n)
{
    double x_j =  cos(double(j+1)/double(n+1)*pi);
    double w_j = pi/double(n+1)*pow<2>(sin(double(j+1)/double(n+1)*pi) );

    return pow(-1.,j)*sqrt( (1.-pow<2>(x_j))*w_j );
}

double Gauss_weights_Tscheb_2kind(double a, double b, int j, int n)// j has to be added by 1, because it starts at 0
{
    // double x_j = Nodes_Tscheb_2kind( a,  b, j, n);
    double w_j = pi/double(n+1)*pow<2>(sin(double(j+1)/double(n+1)*pi));
    return w_j;
}


double Nodes_Legendre( double a, double b, int j, int n)// j has to be added by 1, because it starts at 0
{
    j++;
    double cos_sp;
    if(n==1)
    {
        cos_sp=0.;
    }
    else
    {
        double xm=0.;
        double xl=1.;
        double p1=0.;
        double p2=0.;
        double p3=0.;
        double pp=0.;

        double z=cos(pi*(double(j)-0.25)/(double(n)+0.5));
        double z1=0.;
        do
        {
            p1=1.;
            p2=0.;
            for(int jj=1;jj<=n;jj++)
            {
                p3=p2;
                p2=p1;
                p1=((2.*double(jj)-1.)*z*p2-(double(jj)-1.)*p3)/double(jj);
            }
            pp=double(n)*(z*p1-p2)/(z*z-1.);
            z1=z;
            z=z1-p1/pp;
            //cout << z <<" z " <<endl;
        }while(abs(z-z1) > 3.0e-15);
        cos_sp=xm-xl*z;
    }
    //cout << "Barycentric_weights_Legendre " << j << " " << n << " " << cos_sp << endl;cin.get();
    return (b-a)/2.*(cos_sp+1.)+a;									//the missing minus guarantees that the interval is traversed from a to b
}

double Gauss_weights_Legendre(double a, double b, int j, int n)// j has to be added by 1, because it starts at 0
{
    assert(j>=0);
    assert(j<n);
    j++;

    double xl=1.;
    double p1=0.;
    double p2=0.;
    double p3=0.;
    double pp=0.;

    double w_j = 0.;
    double z=cos(pi*(double(j)-0.25)/(double(n)+0.5));
    double z1=0.;
    do
    {
        p1=1.;
        p2=0.;
        for(int jj=1;jj<=n;jj++)
        {
            p3=p2;
            p2=p1;
            p1=((2.*double(jj)-1.)*z*p2-(double(jj)-1.)*p3)/double(jj);
        }
        pp=double(n)*(z*p1-p2)/(z*z-1.);
        z1=z;
        z=z1-p1/pp;
    }while(abs(z-z1) > 3.0e-15);
    w_j=2.*xl/((1.-z*z)*pp*pp);
    return w_j;
}

double Barycentric_weights_Legendre(  int j, int n)
{
    j++;
    double xm=0.;
    double xl=1.;
    double p1=0.;
    double p2=0.;
    double p3=0.;
    double pp=0.;

    double  x_j ;
    double w_j;
    double z=cos(pi*(double(j)-0.25)/(double(n)+0.5));
    double z1=0.;
    do
    {
        p1=1.;
        p2=0.;
        for(int jj=1;jj<=n;jj++)
        {
            p3=p2;
            p2=p1;
            p1=((2.*double(jj)-1.)*z*p2-(double(jj)-1.)*p3)/double(jj);
        }
        pp=double(n)*(z*p1-p2)/(z*z-1.);
        z1=z;
        z=z1-p1/pp;
    }while(abs(z-z1) > 3.0e-15);
    w_j=2.*xl/((1.-z*z)*pp*pp);

    x_j=xm-xl*z;

    //cout << "Barycentric_weights_Legendre " << j << " " << n << " " << x_j << endl;cin.get();
    return pow(-1.,j)*sqrt( (1.-pow<2>(x_j))*w_j );
}



double Barycentric_weights_Berrut( int d, int k, int n)
{
    if(n<d+1){d=max(0,n-2);}
    double epsilon=0.;//(4.*std::numeric_limits<double>::epsilon());
    auto x = [&](int k) { if(n==0){return -1. + epsilon; }else{return  (2. - 2.*epsilon)/double(n-1)*double(k)+ -1. + epsilon;} };
    vector<int> I(boost::counting_iterator<int>( max(0,k-d) ),boost::counting_iterator<int>(min(n-1-d+1,k+1)));
    double erg_sum=0.;
    for (int i : I )
    {
        vector<int> J(boost::counting_iterator<int>(i),boost::counting_iterator<int>(i+d+1));
        double erg_prod=1.;
        for(int j : J)
        {
            if(j!=k){erg_prod*=1./abs(x(k)-x(j));}
        }
        erg_sum+=erg_prod;
    }
    double w_j=pow(-1.,k-d/2)*erg_sum;
    return w_j;
}

double Nodes_Berrut(double a, double b, int j, int n)// j has to be added by 1, because it starts at 0
{
    if(n==1){return (b-a)/2. +a ; }
    return (b-a)/double(n-1)*double(j)+ a ;
}

double Nodes_Equi_distant(double a, double b, int j, int n)
{
    if(n==1){return (b-a)/2. +a ; }
    return (b-a)/double(n-1)*double(j)+ a ;
}

double Barycentric_weights_Berrut_new(auto && x_, int d, int N_e,  int k)
{
    int N = N_e - 1;
    double sign = pow(-1.,k-d);
    vector<int> I(N -d + 1,0);
    int max_index_J_k = std::min(k,N-d);
    int min_index_J_k = std::max(0, k-d);
    vector<int> J_k( max_index_J_k - min_index_J_k +1 ,0);
    //iota(J_k.begin(), J_k.end(), min_index_J_k);// J_k=( k-d,k-d +1,..., k)
    for (int i = 0; i < J_k.size(); ++i){
        J_k[i]=min_index_J_k+i;
    }

    double sum = 0.;
    for(auto i : J_k){
        double prod = 1.;
        for (int j = i; j <= i+d ; ++j){
            if(k!=j){
                prod*=1./abs( x_(k) - x_(j) );
            }
        }
        sum+=prod;
    }
    //cout << sum*sign << "       " << pow(10.,x_(k)*4.); cin.get();
    return sum*sign;

    return 1.;
}


double Nodes_Equal_dist_periodic( double start,  int j, int n)// j starts startt zero
{
    // if(n==1){return (b-start)/2. +start; }
    // return (b-start)/double(n-1)*double(j) + start ;
    if(n==1){return pi;}
    else{
        auto zw = start + 2.*pi/double(n)*double(j);
        if(zw>2.*pi ){zw = zw - 2.*pi;}
        //cout << "Nodes  " << zw/pi << endl;

        //cout << start << " " << j << " " << n << " " << zw << endl; cin.get();
        return zw;
    }
}

double Barycentric_weights_Equal_dist_periodic( int j, int n)
{
    return pow(-1.,j);
}
