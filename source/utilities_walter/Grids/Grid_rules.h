#pragma once

//Has to be cleaned...


double Nodes_Equal_dist( double a, double b, int j, int n);
double Barycentric_weights_Equal_dist( int j, int n);
double Nodes_Tscheb_2kind( double a, double b, int j, int n);// j has to be added by 1, because it starts at 0
double Nodes_Tscheb_Christian( double a, double b, int j, int n);// j has to be added by 1, because it starts at 0
double Barycentric_weights_Tscheb_2kind(  int j, int n);
double Gauss_weights_Tscheb_2kind(double a, double b, int j, int n);// j has to be added by 1, because it starts at 0
double Nodes_Legendre( double a, double b, int j, int n);// j has to be added by 1, because it starts at 0
double Gauss_weights_Legendre(double a, double b, int j, int n);// j has to be added by 1, because it starts at 0
double Barycentric_weights_Legendre(  int j, int n);
double Barycentric_weights_Berrut( int d, int k, int n);
double Nodes_Berrut(double a, double b, int j, int n);// j has to be added by 1, because it starts at 0
double Nodes_Equi_distant(double a, double b, int j, int n);
double Barycentric_weights_Berrut_new(auto && x_, int d, int N_e,  int k);
double Nodes_Equal_dist_periodic( double start,  int j, int n);// j starts startt zero
double Barycentric_weights_Equal_dist_periodic( int j, int n);

