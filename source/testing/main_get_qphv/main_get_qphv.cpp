//Gerneral includes
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>
#include <fenv.h>
#include <xmmintrin.h>

//Program related definitions

#define K_WRITEOUT_ANGPROJC
//#define K_WRITEOUT_NOPROJ

#include <extra_def.h>

//Program related inculdes
#include <typedefs.h>
#include <homdir.h>
#include <fv.h>
#include <mystream.h>
#include <Parameter.h>
#include <Grids.h>
#include <mytime.h>
#include <ps_amplitude.h>
#include <QuarkDse.h>
#include <Meson/MesonBse.h>
#include <qphv_naive_method.h>


mstream mout(gethomdir()+"tff.out");


//---------------------------------------------------
//MY PROGRAM SUMMARY AND TODO- LIST:

//---------------------------------------------------

//#define K_CALC_EACH_AMP_COMPONTENT
#define K_CALC_GERNOTS_INTERPOLATION
#define K_CREATE_INPUT_DATA_HERE

//comment: is not working jet, I think. It is weird.
//these two functions are basicaly cross-checks for my vertex. If I just need data I should decide which function I want to use and commentout the rest.
//#define K_CALC_MY_VERTEX
//#define K_CALC_MY_VERTEX_COMPLEX

/*------------------------------------START MAIN-----------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
int main (int argc, char * const argv[]) {

    //Command to seek for Naan in the program, debugger will stop when Naans appear and tell you where.
    //Comment: doesn't work on the Mac, maybe find alternative.
//    feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
//    feenableexcept(FE_INVALID | FE_OVERFLOW);
    _MM_SET_EXCEPTION_MASK(_MM_GET_EXCEPTION_MASK() & ~_MM_MASK_INVALID);
//    short fpflags = 0x1332; // Default FP flags, change this however you want.
//    asm("fnclex");
//    asm("fldcw _fpflags");


    //Preprogram set-ups:

    //Setting up time and folder structure:
    time_t starting, dasende;
    time(&starting);
    // Convert now to tm struct for local timezone
    time_t now = time(0);
    tm* localtm = localtime(&now);
    mout << "This program was run @  " << asctime(localtm) << endl;

    string folder="134";

    string output_folder_standart =gethomdir()+"data/data"+folder+"/";
    string output_folder =gethomdir()+"data/data"+folder+"/output_0/";
    string output_folder_ps =gethomdir()+"data/data"+folder+"/output_ps/";
    string output_folder_ve =gethomdir()+"data/data"+folder+"/output_ve/";
    string output_folder_qphv =gethomdir()+"data/data"+folder+"/output_qphv/";
    string input_folder=gethomdir()+"data/data"+folder+"/input/";



    //Creating the folder
//    string system_output2 = "cp -r "+gethomdir()+"data/data00 "+gethomdir()+"data/data"+folder;
//    int pp_error= system(system_output2.c_str());


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<

    //Pick the numerical variable, such as gridsizes etc pp:
    ///-----------------------------------------------------------------------//
    //Is not needed with input file.
    //Setting up the parameters in here and writing them in a file.
    int n_rad_dse =150; int n_ang_dse=32;

    double IRcutsq_dse= 1e-4;
    double UVcutsq_dse = 1e+4;

    double renorm_point = 19.0; double mass_at_renorm = 0.0037;

    //important: Watchout if I am using MTPV gluon or MT, they have a different - and PV cutoff to choose.
    //--------- MT gluon:    or   //-------- MTPV gluon:
//    string which_gluon = "MT";
    string which_gluon = "MTPV";
    double solver_tolerance; double d_gluon_mt; double gluon_omega;
    if(which_gluon=="MT"){
         solver_tolerance = 1e-8;  d_gluon_mt = 0.93312;  gluon_omega = 0.16;
    }else if(which_gluon == "MTPV"){
         solver_tolerance = 1e-8;  d_gluon_mt = 1.01306;  gluon_omega = 0.16;
    }else{assert(false);}

    double Pv_cutoff =4e+4;

    double IRcutsq= IRcutsq_dse;
    double UVcutsq = UVcutsq_dse;

    int n_ll = 20;
    int n_lz = 10;
    int n_ly = 32;

    int n_pp = n_ll;
    int n_zp = n_lz;
    int n_cheb =  4;

    int n_projector, n_basis;


    //Important Steps:-----------------WHICH METHOD FOR CALCUATIONS? --------------------------------------------

    //you can choose between: matrix, matrix_cheb, vector, vector_cheb as the iterative method.
//    string which_method = "vector_fullkernel";
//    string which_method = "vector";
    string which_method = "vector_cheb";
//    string which_method ="full_kernel_KG";
    cout<<"In this run we are using:     "<<which_method<<"    to solve the equation."<<endl;
    //-----------------Chose calculation method      --------------------------------------------


    mout<<output_folder_standart<<endl;
    mout<<"In this session: "<<" n_ll="<<n_ll<<" , n_lz="<<n_lz<<" ,n_ly="<<n_ly<<" ,n_pp="<<n_pp<<" , n_zp="<<n_zp<<" , n_cheb="<<n_cheb<<
        " , IRcut²="<<IRcutsq<<" , UVcut²="<<UVcutsq<<" , quark mass="<<mass_at_renorm<<endl;

    DseParameter p_dse(n_rad_dse, "leg" , "log", n_ang_dse, "leg", "zero_Pi", UVcutsq_dse, IRcutsq_dse,
                       renorm_point, mass_at_renorm,solver_tolerance ,
                       which_gluon, d_gluon_mt, gluon_omega, Pv_cutoff, input_folder );


    BseParameter bse_para(n_ll, n_lz, n_ly, n_pp, n_zp, n_cheb, n_basis, n_projector,
                          "leg", "log", "leg", "linear", "leg", "linear",
                          UVcutsq, IRcutsq, renorm_point, mass_at_renorm, which_gluon, input_folder, which_method);


    double Ps= 0.5;

    //setting up the phase space with the grids.
    Line ll =  Line(bse_para.IRcutsq, bse_para.UVcutsq, bse_para.n_ll, bse_para.func_meassure_rad, bse_para.point_distri_rad, true, false);
    Line  zl =  Line(-1.0,1.0, bse_para.n_lz, bse_para.func_meassure_ang1, bse_para.point_distri_ang1, true, false);

    //###########################################################################################################
    //########### PROGRAM BODY ################


#ifdef K_CALC_GERNOTS_INTERPOLATION

#ifdef K_CREATE_INPUT_DATA_HERE

    //important: choosing the values for the interpolation, here!!!!

    //step 1: Write out the interpolation values for the quark photon vertex

    string buffer = output_folder_qphv+"quarkphoton_grid_1.txt";
    ofstream f(buffer.c_str());

    f<<endl<<tab<<tab<<bse_para.n_lz*bse_para.n_ll<<endl<<endl<<tab<<tab<<"k2"<<tab<<tab<<tab<<"kQ"<<tab<<tab<<tab<<"Q2"<<endl<<endl;

    for (int i_ll = 0; i_ll < bse_para.n_ll; ++i_ll) {
        for (int i_zl = 0; i_zl < bse_para.n_lz; ++i_zl) {

                    f<<scientific<<tab<<real(ll.getGrid()[i_ll])<<tab<<0.0<<tab
                     <<real(sqrt(Ps*ll.getGrid()[i_ll])*zl.getGrid()[i_zl])<<tab<<imag(sqrt(Ps*ll.getGrid()[i_ll])*zl.getGrid()[i_zl])<<tab<<real(Ps)<<endl;
            }

    }

    f.close();

#endif

    //step---------------------------------------------------------------------------|||||||||||||||||||||||||||||||||||||||||||||<
    //step 2: Call the external Fortran interpolation of the quark-photon vertex.
    mout<<"-------------------------------------"<<endl;
    mout<<"Starting the vertex calcaution/interpolation"<<endl<<"|...........................................|"<<endl;


    time_t starting6, dasende6;
    time(&starting6);

        string helper = "cp " + output_folder_qphv +"quarkphoton_grid_1.txt " + gethomdir() + "scripts/interpolate_qpv/ ; " +
                                                   "echo copied the input to interpolation. ; ";


        int dummy=system(helper.c_str());

        helper="cd "+gethomdir()+"scripts/interpolate_qpv/; ./Start_interpolation.sh";
        dummy=system(helper.c_str());
        //comment: had problems with the shell script. Was complaining about previous direct call with ./a.out, thus change it with a file.

        mout<<"|...........................................|"<<endl;

        helper="cd "+gethomdir()+"scripts/interpolate_qpv/; mv quarkphoton_grid_out_1.txt "+ output_folder_qphv +  ";" +
                + "echo interpolation done; rm quarkphoton_grid_1.txt; ";

//        helper="cd "+gethomdir()+"scripts/interpolate_qpv/; mv quarkphoton_grid_out_1.txt "+ output_folder_qphv +  ";" +
//               "mv quarkphoton_grid_out_2.txt " +output_folder_qphv+";" + "echo interpolation done; rm quarkphoton_grid_1.txt; "
//                                                                          "rm quarkphoton_grid_2.txt; ";
        dummy=system(helper.c_str());

        helper = "cd "+output_folder_qphv+ "; cp quarkphoton_grid_out_1.txt G_quarkphoton_grid_out_1.txt; cp quarkphoton_grid_out_2.txt G_quarkphoton_grid_out_2.txt;" +
             "rm quarkphoton_grid_out_1.txt; rm quarkphoton_grid_out_2.txt; ";
        dummy=system(helper.c_str());

#endif


//comment: The Following 2 parts are not working, - i don't know why, something kept going wrong.
#ifdef K_CALC_MY_VERTEX


    array<int,3> bse_index_qphv;
    if(which_method == "matrix_cheb" || which_method == "vector_cheb"){
        bse_index_qphv= {12, bse_para.n_pp, bse_para.n_cheb}; cout<<"Using Chebys!"<<endl;
    }else{
        bse_index_qphv= {12, bse_para.n_pp, bse_para.n_pz};
    }

    ps_amplitude<3> amp_qphv(bse_index_qphv);


    //What is calculated in this set-up
    bool calc_quark=true;
    array<bool,5> what_to_calculate = {1,1,0,0,0};



    //Important Steps:------------------------REAL QUARK------------------------------------------------------------------
    ///This is where the real quark is either calcualated or read inn.
    /// The default folder is set to be in input/real_quark.txt.
    /// First the DSE is initalized with the parameters (cut-offs,..) those are read Inn @ cons_dse.txt
    /// Then it is calculated or read_Inn. The parameter file has to match the read inn quark.
    time_t inbetween1, inbetween2;
    time(&inbetween1);

    QuarkDse dse((input_folder+"cons_dse.txt"));
    if(calc_quark){
        dse.calc_quark();
    }else{
        dse.Fill_QuarkDse(input_folder+"real_quark.txt");
    }
    dse.getQuark().write_quark(input_folder+"real_quark.txt");

    time(&inbetween2); give_time(inbetween1,inbetween2,"real quark");

    //-------------------REAl QUARK END-------------------------------------------------------------------


    time_t ib3, ib4;
    time(&ib3);

    cout<<endl<<"----------------------Calculating full qphv--------------------------------------------"<<endl;
    //Normal Qphv calculation for one choosen value.

    //Or the whole quark photon vertex
    qphv_MesonBse bse_qphv(amp_qphv, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_qphv, false, what_to_calculate);
    bse_qphv.calc_for_different_Psq({Ps},"Qphv");
    bse_qphv.write_out_amplitude(output_folder_qphv+"Qphv.txt", true);

    bse_qphv.free_memory();
//    bse_qphv.free_more_memory();

    cout<<"------------------------------------------------------------------------------"<<endl;
    cout<<"------------------------------------------------------------------------------"<<endl;


#endif

#ifdef K_CALC_MY_VERTEX_COMPLEX


    array<int,3> bse_index_qphv;
    if(which_method == "matrix_cheb" || which_method == "vector_cheb"){
        bse_index_qphv= {12, bse_para.n_pp, bse_para.n_cheb}; cout<<"Using Chebys!"<<endl;
    }else{
        bse_index_qphv= {12, bse_para.n_pp, bse_para.n_pz};
    }

    ps_amplitude<3> amp_qphv(bse_index_qphv);


    //What is calculated in this set-up
    bool calc_quark=true;
    array<bool,5> what_to_calculate = {1,1,0,0,0};



    //Important Steps:------------------------REAL QUARK------------------------------------------------------------------
    ///This is where the real quark is either calcualated or read inn.
    /// The default folder is set to be in input/real_quark.txt.
    /// First the DSE is initalized with the parameters (cut-offs,..) those are read Inn @ cons_dse.txt
    /// Then it is calculated or read_Inn. The parameter file has to match the read inn quark.
    time_t inbetween1, inbetween2;
    time(&inbetween1);

    QuarkDse dse((input_folder+"cons_dse.txt"));
    if(calc_quark){
        dse.calc_quark();
    }else{
        dse.Fill_QuarkDse(input_folder+"real_quark.txt");
    }
    dse.getQuark().write_quark(input_folder+"real_quark.txt");

    time(&inbetween2); give_time(inbetween1,inbetween2,"real quark");

    //-------------------REAl QUARK END-------------------------------------------------------------------


    time_t ib3, ib4;
    time(&ib3);

    cout<<"Testing the Qphv naive interpolation:"<<endl;

    //Now we read Inn the quark-photon vertex grid data from a file an pre calcaute all needed qphv bses.
    VecCdoub ksquared; VecCdoub kP; VecDoub Psquared;
    qphv_MesonBse bse_qphv_2(amp_qphv, &dse, &dse, which_method, input_folder+"cons_bse.txt", output_folder_qphv, false, what_to_calculate);


    //Where is the form factror needed on which P, q, P.q:
    //important: What out- here we choose which vertex and that we only calc for one P².
    int n_pvec=1; int which_vertex=1;
    where_is_vertex_needed_read_inn(output_folder_qphv+"quarkphoton_grid_1.txt", n_pvec , ksquared, kP, Psquared);




    bool precacualtion_of_vertex_already_done=false;
    if(!precacualtion_of_vertex_already_done){
        cout<<"Starting precalculations of vertex"<<endl;
        precalc_bses_for_qphv(Psquared,bse_qphv_2,output_folder_qphv,which_vertex);

        bse_qphv_2.write_out_amplitude(output_folder_qphv+"Test_pro_again_Qphv.txt", true); cout<<"Precalculations done!"<<endl;
    }else{cout<<"precalculations are already done."<<endl;}

    //(1) Readin all the precalculated result. +
    //(2) Iterate qphv one more time at for all VALUES, to get our result
    //(3) Finally safe it into file, usable for the main formfactor.calc()
    //All included in following function():

    //important: What out for which method is choosen here!!
    //important: Always choose "vector" here. so far.
    calculate_all_qphv(bse_qphv_2, output_folder_qphv, Psquared, ksquared, kP,  "vector", which_vertex);


//    //Or the whole quark photon vertex
//    qphv_MesonBse bse_qphv(amp_qphv, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_qphv, false, what_to_calculate);
//    bse_qphv.calc_for_different_Psq({Ps},"Qphv");
//    bse_qphv.write_out_amplitude(output_folder_qphv+"Qphv.txt", true);
//
//    bse_qphv.free_memory();

    cout<<"------------------------------------------------------------------------------"<<endl;
    cout<<"------------------------------------------------------------------------------"<<endl;


#endif





    time(&dasende); give_time(starting,dasende,"the whole main");
    string system_output1 = "cp "+gethomdir()+"tff.out "+output_folder_standart;
    system(system_output1.c_str());


    return 0;



}
