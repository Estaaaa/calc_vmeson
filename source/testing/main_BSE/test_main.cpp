//Gerneral includes
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>
#include <fenv.h>
#include <xmmintrin.h>
#include <sys/stat.h>

//Program related definitions

#define K_WRITEOUT_ANGPROJC
//#define K_WRITEOUT_NOPROJ

#include <extra_def.h>

//Program related inculdes
#include <typedefs.h>
#include <ps_amplitude.h>
#include <Routing/dependence_of_all_kernels.h>
#include <Meson/MesonBse.h>
#include <Kernel/qphv_all/basis_switch.h>
#include <homdir.h>
#include <qphv_naive_method.h>
#include "QuarkDse.h"


//---------------------------------------------------
//MY PROGRAM SUMMARY AND TODO- LIST:


//TO DO's : next thing up -the most important current agenda
//todo: Up next: Quark photon vertex.
//done: easy set-up with P = (0,0,0,E).
//done: frist the scalar part - longitudinal part
//todo: vector part: Set up, but calcautions seem wrong! Check.
//todo: translation between dressing functions for the transverse part Gamma_tilde = Gamma ?
//Comment: I have memory issues, what can I clear, check all opbjects! -Soon.

//todo: What about the norm in this case, compare to Richard!?
//todo: Up next: For P = (0,0, p, iE) - moving Frame


//Things that are still need to be thought of/fixed:

//todo: Implement a search for eigenvalue one! -Check if it is working
//todo: can not choose only one ampltiude!? How can I fix this?
//todo: INTERPOLATION: is it wokring? last time I tried it ran into Abort in the inhomogenous part!? -> Check


//Comments on to do's:
//todo: Comment on the INTERPOATION: So it seems like the interpolation is working, but..
//todo: Normalization in case of interpolation is not working.. fix this! or is it?
//todo: Interpoaltion is not implemented with Chebys... change that! or kinda... Check

//not so important:
//todo: - parameterfile skipp readinn with # infront for putting in multiple gluons parameters. Not important
//todo: include sigma paramter in calcuation.
//todo: Automatic generation of folders when choosing new space to safe it.



//todo: ------DONE----- past thing that were implemented :
//done: vector implementation functioniert nicht mehr - jetzt doch
// - Meson Norm - included the norm with cheap tricks - works
//done: - Implement the possibility to interpolate!! How will I do this, tricky. Maybe after all make the Powermethode extern.
// this could be another possibility to totally break up the EigensolverStructure.
// - Write out for amplitude class. Meson write out.                   -Done, kinda.
// - Pre-calc for the angle is still not working in the new set-up. - works
//done: Implement Chebs completly with the Line class, how to define the Chebys properly?
// There is a slight difference in the imaginary part with Pauls vs my version.
//did: now the Norm works for Chebys and none, but it is all a cheap fix!
//-> Now That Chebys are working on to the interpolation!
//done: finish interpolation - wrong normalization - seems to work now!
//done: something is messed up when using multiple treats!!? What is going on...? -Solved! Sums and prallelization
//done: Issues with the normalization? different number than Richard, and also different when using the Chebychevs
//done: - Do the vector meson. - vector is running
//works: Norm of vector + decay constant
//(done): Read inn possibilities. -! done for Meson!
//done: Change read in option for quark such that in the ploted file we grt rid of the addtional factors.
//done: Change code such that I can include pion and vector mesons in one code!
//-So this should in priciple work now, but didn't test it
//done: rearange my cheby code such that is it faster! Done as far as it was possible - differnce between where you project out the chebys, in Lmat? in Y as well. Compar Richards notes.
//done: Also the full_matrix code was still not working! Need to fix that. It's good for comparison.
// The Form code seems to be right! So now what is different form the loop set-up - Forgot the denorminator of the quark propagators! -first error
//done: - implement everything with Chebys: Precalc the angles Cheb is still missing. Not annymore--Works and fast!





//Comments on performance:
// for a medium sized grid I can safe 6 seconds by reading in precalculated complex quarks.

//todo: MISTAKES THAT OCCURED ON THE PROCESS - Noted - comments -keep in mind
// - Cheb work? Wrong access of zl grid : ps.zl.getweights<- wrong!
// - Watch out for pointers
// - Watch out: When using the interpolation and having a log distrubted grid, you should interpolate in the "log-distributed" Grid!!!
// - Watch out: For round off error when using parllelization in the Norm and decay rate calculation.
// - When perfomring a integral with a parellelization one should use "reduction" to make sure the integration sum is not overwritten..
// (Reduction only works for simple data types like int, double, float...)
// - Form code to C++ tranfers, includsion of B_vector.h in Form, look out for what is reffered to first (P?,q?) vs. (q?,P?)
//Do not define matCdoub in parallized loop, but rather outside and use fristprivate(variable) for the parallelization command.
//Now that there are multiple mesons in one code, one has to be careful which folder structure is passed and where to read in an read out! Also each meson need their own set-up of the things that initalize a meson_: parameter file, amp
//Forgot the quark denormlinators when setting up the full martrix code.

//The Norm factor must be different when using Chebychev if I demand the Powermethod to normlaize both to be 1.0. (as I am currently am)
//That is why I had to put in an additional factor of sqrt(2) in the decay rate inegral when using Chebychevs.
//In princple you could redefine you Chebycehv such that the zero's component is equal to 1.0 when pluggin in ta factor of 1/sqrt(2) in the definition of the Cheby.
//TiTj =  detla_ij 2/N -> weight * Ti(1/sqrt(2)) Tj(1/sqrt(2))=Tj'  = 1.

//kernel_all coce was not the corresponding one to the kernel_L and kernel-Y code, aka not the same resulting kernel matrix. No wonders nothing works.

//Change all the basis element numbers in the form code generation!!!!!



//---------------------------------------------------




//More definitions
//#define K_CALC_QPHV
//#define K_CALC_QPHV_SEPERATED
//#define K_CALC_VECTOR
#define K_CALC_SCALAR
#define K_CALC_PS_SCALAR
//#define K_CALC_AXIAL
//#define K_TESTING_QPHV_ITER

/*------------------------------------START MAIN-----------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
int main (int argc, char * const argv[]) {

    //Command to seek for Naan in the program, debugger will stop when Naans appear and tell you where.
    //Comment: doesn't work on the Mac, maybe find alternative.
//    feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
//    feenableexcept(FE_INVALID | FE_OVERFLOW);
    _MM_SET_EXCEPTION_MASK(_MM_GET_EXCEPTION_MASK() & ~_MM_MASK_INVALID);
//    short fpflags = 0x1332; // Default FP flags, change this however you want.
//    asm("fnclex");
//    asm("fldcw _fpflags");


/*    string f = gethomdir()+"data/TESTFOLDER";
    const int dir_err = mkdir(f.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (-1 == dir_err)
    {
//        printf("Error creating directory!n");
        exit(1);
    }
    return(0);*/



    //Setting up time and folder structure:
    time_t starting, dasende;
    time(&starting);
    // Convert now to tm struct for local timezone
    time_t now = time(0);
    tm* localtm = localtime(&now);
    cout << "This program was run @  " << asctime(localtm) << endl;

    string folder="903";

    string output_folder_standart =gethomdir()+"data/data"+folder+"/";
    string output_folder =gethomdir()+"data/data"+folder+"/output_0/";
    string output_folder_ps =gethomdir()+"data/data"+folder+"/output_ps/";
    string output_folder_ve =gethomdir()+"data/data"+folder+"/output_ve/";
    string output_folder_qphv =gethomdir()+"data/data"+folder+"/output_qphv/";
    string input_folder=gethomdir()+"data/data"+folder+"/input/";


  /*  //Test of different quadratures:
    int n=51; string point_distri, func_meas;
    point_distri="sinh_tanh";
    func_meas = "arcsinh";
    Line l;
    l=Line(-1.0 ,1.0 ,n,func_meas,point_distri, true, false);
    double result=0.0;
    for (int j = 0; j < n; ++j) {
        result += sinh(l.getGrid()[j] )* l.getWeights_integration()[j];
    }
    cout<<"//Set up: "<< point_distri<< tab<< func_meas<<"   -> Here comes the result: "<<result<<endl;

    point_distri="leg";
    func_meas = "arcsinh";
    l=Line(-1.0 ,1.0 ,n,func_meas,point_distri, true, false);
    result=0.0;
    for (int j = 0; j < n; ++j) {
        result += sinh(l.getGrid()[j] )* l.getWeights_integration()[j];
    }
    cout<<"//Set up: "<< point_distri<< tab<< func_meas<<"   -> Here comes the result: "<<result<<endl;

    point_distri="leg";
    func_meas = "linear";
    l=Line(-1.0 ,1.0 ,n,func_meas,point_distri, true, false);
    result=0.0;
    for (int j = 0; j < n; ++j) {
        result += sinh(l.getGrid()[j] )* l.getWeights_integration()[j];
    }
    cout<<"//Set up: "<< point_distri<< tab<< func_meas<<"   -> Here comes the result: "<<result<<endl;

    point_distri="sinh_tanh";
    func_meas = "linear";
    l=Line(-1.0 ,1.0 ,n,func_meas,point_distri, true, false);
    result=0.0;
    for (int j = 0; j < n; ++j) {
        result += sinh(l.getGrid()[j] )* l.getWeights_integration()[j];
    }
    cout<<"//Set up: "<< point_distri<< tab<< func_meas<<"   -> Here comes the result: "<<result<<endl<<endl;

    //Comment:
    //Result of int _ -1 ^ 1  dx sinh(x) :
    //Set up: sinh_tanh	arcsinh   -> Here comes the result: 2.1684e-18
    //Set up: leg	arcsinh   -> Here comes the result: 2.1684e-18
    //Set up: leg	linear   -> Here comes the result: -8.67362e-19
    //Set up: sinh_tanh	linear   -> Here comes the result: -3.72334e-05

    point_distri="sinh_tanh";
    func_meas = "arcsinh";
    l=Line(-1.0 ,1.0 ,n,func_meas,point_distri, true, false);
    result=0.0;
    for (int j = 0; j < n; ++j) {
        result += l.getGrid()[j] * l.getWeights_integration()[j];
    }
    cout<<"//Set up: "<< point_distri<< tab<< func_meas<<"   -> Here comes the result: "<<result<<endl;

    point_distri="leg";
    func_meas = "arcsinh";
    l=Line(-1.0 ,1.0 ,n,func_meas,point_distri, true, false);
    result=0.0;
    for (int j = 0; j < n; ++j) {
        result += l.getGrid()[j] * l.getWeights_integration()[j];
    }
    cout<<"//Set up: "<< point_distri<< tab<< func_meas<<"   -> Here comes the result: "<<result<<endl;

    point_distri="leg";
    func_meas = "linear";
    l=Line(-1.0 ,1.0 ,n,func_meas,point_distri, true, false);
    result=0.0;
    for (int j = 0; j < n; ++j) {
        result += l.getGrid()[j] * l.getWeights_integration()[j];
    }
    cout<<"//Set up: "<< point_distri<< tab<< func_meas<<"   -> Here comes the result: "<<result<<endl;

    point_distri="sinh_tanh";
    func_meas = "linear";
    l=Line(-1.0 ,1.0 ,n,func_meas,point_distri, true, false);
    result=0.0;
    for (int j = 0; j < n; ++j) {
        result += l.getGrid()[j] * l.getWeights_integration()[j];
    }
    cout<<"//Set up: "<< point_distri<< tab<< func_meas<<"   -> Here comes the result: "<<result<<endl;

    //Comment:
    //Result of int _ -1 ^ 1  dx sinh(x) :
    //Set up: sinh_tanh	arcsinh   -> Here comes the result: 2.1684e-18
    //Set up: leg	arcsinh   -> Here comes the result: 2.1684e-18
    //Set up: leg	linear   -> Here comes the result: -8.67362e-19
    //Set up: sinh_tanh	linear   -> Here comes the result: -3.72334e-05


    return 0 ;
*/

//Is not working - sudo rights for the program cause it can not make a folder..?
//    int folder_status= system(("mkdir "+output_folder).c_str());
//    if(folder_status == 0){cout<<"A new folder was created:"<<tab<<output_folder<<endl;}
//
//    folder_status= system(("mkdir "+input_folder).c_str());
//    if(folder_status == 0){cout<<"A new folder was created:"<<tab<<input_folder<<endl;}

    ///-----------------------------------------------------------------------//
    //Is not needed with input file.
    //Setting up the parameters in here and writing them in a file.
    int n_rad_dse =150; int n_ang_dse=64;

    double IRcutsq_dse= 1e-6;
    double UVcutsq_dse = 1e+6;

    double renorm_point = 19.0; double mass_at_renorm = 0.0035748; //0.0037;

    //important: Choose your gluon
    //For the MT gluon
    string which_gluon = "MT";
//    double solver_tolerance = 1e-8; double d_gluon_mt = 0.93312; double gluon_omega_sq = 0.16;
    double solver_tolerance = 1e-8; double gluon_omega_sq = 0.1296;  double d_gluon_mt = pow(0.74,3.0)/(sqrt(gluon_omega_sq));

    //For the MTPV gluon
//    string which_gluon = "MTPV";
//    double solver_tolerance = 1e-8; double d_gluon_mt = 1.01306; double gluon_omega = 0.16;
    double Pv_cutoff =4e+4;

    double IRcutsq= 1e-4;
    double UVcutsq = 1e+6;

    int n_ll = 100;
    int n_lz = 24;
    int n_ly = 32;

    int n_pp = n_ll;
    int n_zp = n_lz;
    int n_cheb =  6;

    int n_projector, n_basis;


    //Important Steps:-----------------WHICH METHOD FOR CALCUATIONS? --------------------------------------------

    //you can choose between: matrix, matrix_cheb, vector, vector_cheb as the iterative method.
//    string which_method = "vector_fullkernel";
//    string which_method = "vector";
    string which_method = "vector_cheb";
//    string which_method ="full_kernel_KG";
    cout<<"In this run we are using:     "<<which_method<<"    to solve the equation."<<endl;
    //-----------------Chose calculation method      --------------------------------------------



#ifdef K_NOT_USING_INPUT_FILE

//        DseParameter p_dse(n_rad_dse, "leg" , "log", n_ang_dse, "leg", "linear",UVcutsq_dse, IRcutsq_dse,
//                           renorm_point, mass_at_renorm,solver_tolerance ,
//                           "MT", d_gluon_mt, gluon_omega, Pv_cutoff, input_folder );

        DseParameter p_dse(n_rad_dse, "leg" , "log", n_ang_dse, "leg", "zero_Pi", UVcutsq_dse, IRcutsq_dse,
                       renorm_point, mass_at_renorm,solver_tolerance ,
                       "MTPV", d_gluon_mt, gluon_omega_sq, Pv_cutoff, input_folder );

        //important: Watch out when choosing tscheb_zero this is not jet properly working


//        BseParameter bse_para(n_ll, n_lz, n_ly, n_pp, n_zp, n_cheb, n_basis, n_projector, "leg", "log", "leg", "ang_cos",
        BseParameter bse_para(n_ll, n_lz, n_ly, n_pp, n_zp, n_cheb, n_basis, n_projector,
                              "leg_split", "log", "leg", "linear", "leg", "linear",
//                              "leg", "log", "leg", "linear", "leg", "linear",
                              UVcutsq, IRcutsq, renorm_point, mass_at_renorm, which_gluon, input_folder, which_method);


    #endif

    cout<<"In this session: "<<" n_ll="<<n_ll<<" , n_lz="<<n_lz<<" ,n_ly="<<n_ly<<" ,n_pp="<<n_pp<<" , n_zp="<<n_zp<<" , n_cheb="<<n_cheb<<
        " , IRcut²="<<IRcutsq<<" , UVcut²="<<UVcutsq<<" , quark mass="<<mass_at_renorm<< tab<< "  , gluon= "<<which_gluon<<endl;



/*    int n=45;
    Line l(-1.0,1.0,n,"linear","sinh_tanh", true, false);
    Line l2(-1.0,1.0,n,"linear","leg", true, false);
    Line l3(-1.0,1.0,8,"linear","tscheb_zeros", true, false);

    double t_sum1, t_sum2, t_sum3=0.0;
    for (int j = 0; j < n; ++j) {
        t_sum1 +=  sqrt(1.0- l.getGrid()[j] )*l.getWeights_integration()[j];
        t_sum2 +=  sqrt(1.0 - l2.getGrid()[j] ) *l2.getWeights_integration()[j];
    }
    for (int j = 0; j < l3.getGrid().size(); ++j) {
        t_sum3 +=  sqrt(1.0 - l3.getGrid()[j] )*l3.getWeights_integration()[j];
        cout<<l3.getGrid()[j]<<tab;
    }cout<<endl;

    cout<<"Calculations: sinhtanh, leg, tscheb  = "<<t_sum1<<tab<<t_sum2<<tab<<t_sum3<<endl;*/


    //What is calculated in this set-up
    bool calc_quark=true;
    array<bool,5> what_to_calculate = {1,1,0,0,0};



    //Important Steps:------------------------REAL QUARK------------------------------------------------------------------
    ///This is where the real quark is either calcualated or read inn.
    /// The default folder is set to be in input/real_quark.txt.
    /// First the DSE is initalized with the parameters (cut-offs,..) those are read Inn @ cons_dse.txt
    /// Then it is calculated or read_Inn. The parameter file has to match the read inn quark.
    time_t inbetween1, inbetween2;
    time(&inbetween1);

    QuarkDse dse((input_folder+"cons_dse.txt"));
    if(calc_quark){
        dse.calc_quark();
    }else{
        dse.Fill_QuarkDse(input_folder+"real_quark.txt");
    }
    dse.getQuark().write_quark(input_folder+"real_quark.txt");

    time(&inbetween2); give_time(inbetween1,inbetween2,"real quark");

    //-------------------REAl QUARK END-------------------------------------------------------------------


//    return 0;


    //Important Steps: Start-------------------BSE------------------------------------------------------------------------------

#ifdef K_USING_INPUT_FILE
    BseParameter bse_para(input_folder+"cons_bse.txt");
#endif


    //These indices are all needed to set up the meson amplitude/bse.
    // The only important thing to pass it the number of basis elements here
    // and if we are using chebys or direct angle.
    // I set them for vector, scalar and qphv, but depending on the program we might not need them.

    array<int,3> bse_index_ve;
    array<int,3> bse_index_ps;
    array<int,3> bse_index_qphv;
    if(which_method == "matrix_cheb" || which_method == "vector_cheb"){
        bse_index_ps= {4, bse_para.n_pp, bse_para.n_cheb};
        bse_index_ve= {8, bse_para.n_pp, bse_para.n_cheb};
        bse_index_qphv= {12, bse_para.n_pp, bse_para.n_cheb}; cout<<"Using Chebys!"<<endl;
    }else{
        bse_index_ps= {4, bse_para.n_pp, bse_para.n_pz};
        bse_index_ve= {8, bse_para.n_pp, bse_para.n_pz};
        bse_index_qphv= {12, bse_para.n_pp, bse_para.n_pz};
    }

    ps_amplitude<3> amp_ve(bse_index_ve);
    ps_amplitude<3> amp_ps(bse_index_ps);
    ps_amplitude<3> amp_qphv(bse_index_qphv);



    //you can choose between: matrix, matrix_cheb, vector, vector_cheb as the method.
    //This is choosen above in : which_method.


#ifdef K_CALC_PS_SCALAR
    //Or a pseudoscalar
    cout<<"||    ----------------------------------------------     ||"<<endl<<"      ---------pseudo scalar meson----------------       "<<endl;
    ps_MesonBse bse_ps(amp_ps, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_ps, false, what_to_calculate, "ps");
//    bse_ps.solve_and_update_pion(- 0.138437* 0.138437);
    bse_ps.find_the_bs_mass(0.135, 0.005, 0.0001);
    bse_ps.write_out_amplitude(output_folder_ps+"pseudoscalar_meson.txt");
    bse_ps.norm_the_meson();
    bse_ps.write_out_amplitude(output_folder_ps+"normed_pseudoscalar_meson.txt");

#endif

    //------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------

#ifdef K_CALC_SCALAR
    //SCALAR:
    //Now I want to calcuate a scalar meson.
    cout<<"||    ----------------------------------------------     ||"<<endl<<"      ---------scalar meson----------------       "<<endl;

    ps_MesonBse bse_scalar(amp_ps, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_ps, false, what_to_calculate, "scalar");
//    bse_scalar.solve_and_update_pion(-0.6754*0.6754);
    bse_scalar.find_the_bs_mass(0.67, 0.01, 0.0001); // found mass=0.675371874416 for (48, 24, 10)
    bse_scalar.write_out_amplitude(output_folder_ps+"scalar_meson.txt", true);
    bse_scalar.norm_the_meson();
    bse_scalar.write_out_amplitude(output_folder_ps+"normed_scalar_meson.txt", true);

    testing_iteration_scalar(bse_scalar, output_folder, "vector");

    bse_scalar.free_memory();

    cout<<endl<<endl<<"____________________________________"<<endl<<endl;
#endif

    //------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------

#ifdef K_CALC_VECTOR
    //Now we can pick different Mesons, such as a vector.
    cout<<"||    ----------------------------------------------     ||"<<endl<<"      ---------vector meson----------------       "<<endl;
    vector_MesonBse bse_ve(amp_ve, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_ve, false, what_to_calculate, "vector");
//    bse_ve.solve_and_update_pion(- 0.754724331887*0.754724331887);
    bse_ve.find_the_bs_mass(0.7, 0.05, 0.001);
    bse_ve.write_out_amplitude(output_folder_ve+"vector_meson_amplitude.txt");
    bse_ve.norm_the_meson();
    bse_ve.write_out_amplitude(output_folder_ve+"normed_vector_meson.txt");
//    bse_ve.write_out_amplitude(output_folder_ve+"vector_meson_normed_withoutcheb.txt");
//    bse_ve.~vector_MesonBse();

#endif

    //------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------


#ifdef K_CALC_AXIAL
    //AXIAL:
    //Now I want to calcuate a axial vector meson.
    cout<<"||    ----------------------------------------------     ||"<<endl<<"      ---------axial meson----------------       "<<endl;
    vector_MesonBse bse_axial(amp_ve, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_ve, false, what_to_calculate, "axial");
//    bse_axial.solve_and_update_pion(- 0.92964*0.92964);
    bse_axial.find_the_bs_mass(0.8, 0.1, 0.0001);  //found mass= 0.929642311329 for (48, 24, ,10) // worng cause only included 4 basis elements
    bse_axial.write_out_amplitude(output_folder_ve+"axial_meson.txt");
    bse_axial.norm_the_meson();
    bse_axial.write_out_amplitude(output_folder_ve+"normed_axial_meson.txt");

    cout<<endl<<endl<<"____________________________________"<<endl<<endl;

#endif


    //------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------
    //-----------------------------QPHV VERTEX--------------------------------------------------------
    //Seperated or in one go with all the tensor structures.

#ifdef K_CALC_QPHV_SEPERATED

    time_t ib1, ib2;
    time(&ib1);

    cout<<endl<<"----------------------Calculating scalar qphv--------------------------------------------"<<endl;

    //Or a scalar with vector quantum numbers
    ps_MesonBse bse_qphv_ps(amp_ps, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_ps, false, what_to_calculate, "qphv");
    bse_qphv_ps.calc_for_different_Psq({0.5},"longi_Qphv");
//    bse_qphv_ps.write_out_amplitude(output_folder_ps+"amp_"+which_method+".txt");

    cout<<endl<<endl<<"____________________________________"<<endl<<endl;


//    ------------------------------------------------------------------------------------------------
//    ------------------------------------------------------------------------------------------------

    cout<<endl<<"----------------------Calculating vector qphv--------------------------------------------"<<endl;


    //Or the vector part of the qphv
    vector_MesonBse bse_qphv_ve(amp_ve, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_ve, false, what_to_calculate, "qphv");
////    bse_qphv_ve.calc_for_different_Psq((IRcutsq), (UVcutsq), 5);
    bse_qphv_ve.calc_for_different_Psq({0.5},"trans_Qphv");

    //Now we can translate the dressing function of the seperated transverse part for the qphv to the transverse
    // components when calcuated directly with all 12 basis elements
#ifdef K_WRITEOUT_ANGPROJC
    //comment: check transvere_4 and this worked. I am not so sure about no2 anymore.
    translate_qphv_transverse_4( bse_qphv_ve, bse_qphv_ps); // (1) translate (2) project
//    translate_qphv_transverse_2( bse_qphv_ve, bse_qphv_ps); // (1) project  (2)translate
#endif

#ifdef K_WRITEOUT_NOPROJ
    translate_qphv_transverse_3( bse_qphv_ve, bse_qphv_ps);
#endif


    bse_qphv_ve.write_out_amplitude(output_folder_ve+"transverse_translated_amplitude_between.txt"); cout<<endl;

    bse_qphv_ps.free_memory();
    bse_qphv_ve.free_memory();

    time(&ib2); give_time(ib1,ib2,"seperated quark-photon vertex");cout<<endl;

#endif

//    //------------------------------------------------------------------------------------------------


#ifdef K_CALC_QPHV

    time_t ib3, ib4;
    time(&ib3);

    cout<<endl<<"----------------------Calculating full qphv--------------------------------------------"<<endl;
    //Normal Qphv calculation for one choosen value.

    //Or the whole quark photon vertex
    qphv_MesonBse bse_qphv(amp_qphv, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_qphv, false, what_to_calculate);
    bse_qphv.calc_for_different_Psq({0.5},"Qphv");
    bse_qphv.write_out_amplitude(output_folder_qphv+"Qphv.txt", false);

    bse_qphv.free_memory();
//    bse_qphv.free_more_memory();

    cout<<"------------------------------------------------------------------------------"<<endl;
    cout<<"------------------------------------------------------------------------------"<<endl;


#ifdef K_TESTING_QPHV_ITER

    cout<<"Testing the Qphv naive interpolation:"<<endl;
    //testing the new set-up for the naive qphv.
//    qphv_MesonBse bse_qphv_3(amp_qphv, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_qphv, false, what_to_calculate);

    //Now we read Inn the quark-photon vertex grid data from a file an pre calcaute all needed qphv bses.
    VecCdoub ksquared; VecCdoub kP; VecDoub Psquared;
/*    where_is_vertex_needed_read_inn(output_folder_qphv+"quarkphoton_grid_1.txt", 20, ksquared, kP, Psquared);
    //And precalculate the corresponding qphv bses at all Q² values.
    precalc_bses_for_qphv(Psquared,bse_qphv_3,output_folder_qphv,1);
    bse_qphv_3.free_memory();*/

    //Now testing the iterpolation using one of the precalcuated and read amplitude Amp0. Calcualting the function on real values.
    qphv_MesonBse bse_qphv_2(amp_qphv, &dse, &dse, which_method, input_folder+"cons_bse.txt", output_folder_qphv, false, what_to_calculate);
/*    bse_qphv_2.init_via_file(output_folder_qphv+"Amp0/",bse_qphv.getPhaseSpacePsquared(),1);
 *
    string methode_for_the_iteration="vector";
    testing_iteration_qphv(bse_qphv_2, output_folder_qphv, methode_for_the_iteration);

    bse_qphv_2.free_memory();*/
    //comment: add a line where one copies the cons_bse into the output folder and calls it qphv_cons, ready for further calcs


    //or alternativly exactly what should be in form factor main:
    int n_pvec=20; int which_vertex=1;
    where_is_vertex_needed_read_inn(output_folder_qphv+"quarkphoton_grid_1.txt", n_pvec , ksquared, kP, Psquared);

    bool precacualtion_of_vertex_already_done=true;
    if(!precacualtion_of_vertex_already_done){
        precalc_bses_for_qphv(Psquared,bse_qphv_2,output_folder_qphv,which_vertex);  }else{cout<<"precalculations are already done."<<endl;}

    //(1) Readin all the precalculated result. +
    //(2) Iterate qphv one more time at for all VALUES, to get our result
    //(3) Finally safe it into file, usable for the main formfactor.calc()
    //All included in following function():

    calculate_all_qphv(bse_qphv_2, output_folder_qphv, Psquared, ksquared, kP,  "vector", which_vertex);


#endif

    time(&ib4); give_time(ib3,ib4,"full quark photon vertex.");


#endif

    //------------------------------------------------------------------------------------------------
    //-------------------BSE END--------------------------------------------------------------------------


    time(&dasende); give_time(starting,dasende,"the whole main_BSE");
//    string system_output1 = "cp "+gethomdir()+".out "+output_folder_standart;
//    system(system_output1.c_str());



    return 0;



}

