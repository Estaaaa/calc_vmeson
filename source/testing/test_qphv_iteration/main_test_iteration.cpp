//Gerneral includes
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>
#include <fenv.h>
#include <xmmintrin.h>

//Program related definitions

#define K_WRITEOUT_ANGPROJC
//#define K_WRITEOUT_NOPROJ

#include <extra_def.h>

//Program related inculdes
#include <typedefs.h>
#include <ps_amplitude.h>
#include <Routing/dependence_of_all_kernels.h>
#include <Meson/MesonBse.h>
#include <homdir.h>
#include <fv.h>
#include "QuarkDse.h"
//#include <formfactor.h>
#include <mystream.h>
#include <qphv_naive_method.h>


mstream mout(gethomdir()+"tff.out");

void produce_test_data(string& output_folder_qphv, double& mass_sq){

    //Create fake testing data
    fv<Cdoub> P ,ll;
    Cdoub rr, QQ, Qr;

    //Amoutnt of points where I will calculate on the real axis.
    int nll=20; int nz=10; int np =1;

//    Line l_ll =  Line(1e-4, 1e+4, nll, "log","leg", true, false);
    Line l_ll =  Line(1e-4, 1e+4, nll, "log","leg", true, false);
    Line l_zl =  Line(-1.0, 1.0, nz, "linear","leg", true, false);
//    Line l_zl =  Line(-0.5, 0.5, nz, "linear","leg", true, false);


    stringstream help;
    string buffer = output_folder_qphv+"quarkphoton_grid_1.txt";
    ofstream f(buffer.c_str());

    f<<endl<<tab<<tab<<nll*nz*np<<endl<<endl<<tab<<tab<<"k2"<<tab<<tab<<tab<<"kQ"<<tab<<tab<<tab<<"Q2"<<endl<<endl;

    P.content[3]= sqrt(mass_sq);
//    P.content[3]= sqrt(mass_sq);

    for (int i_ll = 0; i_ll < nll; ++i_ll) {
        for (int i_zl = 0; i_zl < nz; ++i_zl) {

            ll.set_spherical(l_ll.getGrid()[i_ll],0.0,0.0,l_zl.getGrid()[i_zl]);


            //todo: Change the order here.. could be faster, but now my next routine to read in these relies on the oder so watch out.
            for (int i_pp = 0; i_pp < np; ++i_pp)
            {

                rr=ll*ll; QQ=P*P; Qr=P*ll;
//                f<<scientific<<tab<<real(rr)<<tab<<imag(rr)<<tab<<real(Qr)<<tab<<imag(Qr)<<tab<<real(QQ)<<endl;
//                f<<scientific<<tab<<real(rr)<<tab<<imag(rr)<<tab<<real(Qr)<<tab<<0.01*real(Qr)<<tab<<real(QQ)<<endl;
//                f<<scientific<<tab<<real(rr)<<tab<<imag(rr)<<tab<<0.1<<tab<<0.001<<tab<<real(QQ)<<endl;

                if(mass_sq>0){
                    f<<scientific<<tab<<real(rr)<<tab<<imag(rr)<<tab<<imag(Qr)<<tab<<real(Qr)<<tab<<real(QQ)<<endl;
                }else{
                    f<<scientific<<tab<<real(rr)<<tab<<imag(rr)<<tab<<real(Qr)<<tab<<imag(Qr)<<tab<<real(QQ)<<endl;
                }

//                cout<<l_ll.getGrid()[i_ll]<<tab<<l_zl.getGrid()[i_zl]<<endl;
//                cout<<scientific<<tab<<real(rr)<<tab<<imag(rr)<<tab<<real(Qr)<<tab<<imag(Qr)<<tab<<real(QQ)<<endl;
            }
        }
    }

    f.close();

}



#define K_CALC_QPHV
#define K_TESTING_QPHV_ITER

#define K_CALC_GERNOTS_INTERPOLATION

//#define K_CALC_SCALAR
//#define K_CALC_VECTOR

//#define K_CALC_EACH_AMP_COMPONTENT

/*------------------------------------START MAIN-----------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
int main (int argc, char * const argv[]) {

    //Command to seek for Naan in the program, debugger will stop when Naans appear and tell you where.
    //Comment: doesn't work on the Mac, maybe find alternative.
//    feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
//    feenableexcept(FE_INVALID | FE_OVERFLOW);
    _MM_SET_EXCEPTION_MASK(_MM_GET_EXCEPTION_MASK() & ~_MM_MASK_INVALID);
//    short fpflags = 0x1332; // Default FP flags, change this however you want.
//    asm("fnclex");
//    asm("fldcw _fpflags");

    //Setting up time and folder structure:
    time_t starting, dasende;
    time(&starting);
    // Convert now to tm struct for local timezone
    time_t now = time(0);
    tm* localtm = localtime(&now);
    cout << "This program was run @  " << asctime(localtm) << endl;

    string folder="135";

    string output_folder_standart =gethomdir()+"data/data"+folder+"/";
    string output_folder =gethomdir()+"data/data"+folder+"/output_0/";
    string output_folder_ps =gethomdir()+"data/data"+folder+"/output_ps/";
    string output_folder_ve =gethomdir()+"data/data"+folder+"/output_ve/";
    string output_folder_qphv =gethomdir()+"data/data"+folder+"/output_qphv/";
    string input_folder=gethomdir()+"data/data"+folder+"/input/";


    /*  //Test of different quadratures:
      int n=51; string point_distri, func_meas;
      point_distri="sinh_tanh";
      func_meas = "arcsinh";
      Line l;
      l=Line(-1.0 ,1.0 ,n,func_meas,point_distri, true, false);
      double result=0.0;
      for (int j = 0; j < n; ++j) {
          result += sinh(l.getGrid()[j] )* l.getWeights_integration()[j];
      }
      cout<<"//Set up: "<< point_distri<< tab<< func_meas<<"   -> Here comes the result: "<<result<<endl;

      point_distri="leg";
      func_meas = "arcsinh";
      l=Line(-1.0 ,1.0 ,n,func_meas,point_distri, true, false);
      result=0.0;
      for (int j = 0; j < n; ++j) {
          result += sinh(l.getGrid()[j] )* l.getWeights_integration()[j];
      }
      cout<<"//Set up: "<< point_distri<< tab<< func_meas<<"   -> Here comes the result: "<<result<<endl;

      point_distri="leg";
      func_meas = "linear";
      l=Line(-1.0 ,1.0 ,n,func_meas,point_distri, true, false);
      result=0.0;
      for (int j = 0; j < n; ++j) {
          result += sinh(l.getGrid()[j] )* l.getWeights_integration()[j];
      }
      cout<<"//Set up: "<< point_distri<< tab<< func_meas<<"   -> Here comes the result: "<<result<<endl;

      point_distri="sinh_tanh";
      func_meas = "linear";
      l=Line(-1.0 ,1.0 ,n,func_meas,point_distri, true, false);
      result=0.0;
      for (int j = 0; j < n; ++j) {
          result += sinh(l.getGrid()[j] )* l.getWeights_integration()[j];
      }
      cout<<"//Set up: "<< point_distri<< tab<< func_meas<<"   -> Here comes the result: "<<result<<endl<<endl;

      //Comment:
      //Result of int _ -1 ^ 1  dx sinh(x) :
      //Set up: sinh_tanh	arcsinh   -> Here comes the result: 2.1684e-18
      //Set up: leg	arcsinh   -> Here comes the result: 2.1684e-18
      //Set up: leg	linear   -> Here comes the result: -8.67362e-19
      //Set up: sinh_tanh	linear   -> Here comes the result: -3.72334e-05

      point_distri="sinh_tanh";
      func_meas = "arcsinh";
      l=Line(-1.0 ,1.0 ,n,func_meas,point_distri, true, false);
      result=0.0;
      for (int j = 0; j < n; ++j) {
          result += l.getGrid()[j] * l.getWeights_integration()[j];
      }
      cout<<"//Set up: "<< point_distri<< tab<< func_meas<<"   -> Here comes the result: "<<result<<endl;

      point_distri="leg";
      func_meas = "arcsinh";
      l=Line(-1.0 ,1.0 ,n,func_meas,point_distri, true, false);
      result=0.0;
      for (int j = 0; j < n; ++j) {
          result += l.getGrid()[j] * l.getWeights_integration()[j];
      }
      cout<<"//Set up: "<< point_distri<< tab<< func_meas<<"   -> Here comes the result: "<<result<<endl;

      point_distri="leg";
      func_meas = "linear";
      l=Line(-1.0 ,1.0 ,n,func_meas,point_distri, true, false);
      result=0.0;
      for (int j = 0; j < n; ++j) {
          result += l.getGrid()[j] * l.getWeights_integration()[j];
      }
      cout<<"//Set up: "<< point_distri<< tab<< func_meas<<"   -> Here comes the result: "<<result<<endl;

      point_distri="sinh_tanh";
      func_meas = "linear";
      l=Line(-1.0 ,1.0 ,n,func_meas,point_distri, true, false);
      result=0.0;
      for (int j = 0; j < n; ++j) {
          result += l.getGrid()[j] * l.getWeights_integration()[j];
      }
      cout<<"//Set up: "<< point_distri<< tab<< func_meas<<"   -> Here comes the result: "<<result<<endl;

      //Comment:
      //Result of int _ -1 ^ 1  dx sinh(x) :
      //Set up: sinh_tanh	arcsinh   -> Here comes the result: 2.1684e-18
      //Set up: leg	arcsinh   -> Here comes the result: 2.1684e-18
      //Set up: leg	linear   -> Here comes the result: -8.67362e-19
      //Set up: sinh_tanh	linear   -> Here comes the result: -3.72334e-05


      return 0 ;
  */

//Is not working - sudo rights for the program cause it can not make a folder..?
//    int folder_status= system(("mkdir "+output_folder).c_str());
//    if(folder_status == 0){cout<<"A new folder was created:"<<tab<<output_folder<<endl;}
//
//    folder_status= system(("mkdir "+input_folder).c_str());
//    if(folder_status == 0){cout<<"A new folder was created:"<<tab<<input_folder<<endl;}

    ///-----------------------------------------------------------------------//
    //Is not needed with input file.
    //Setting up the parameters in here and writing them in a file.
    int n_rad_dse =150; int n_ang_dse=32;

    double IRcutsq_dse= 1e-4;
    double UVcutsq_dse = 1e+4;

    double renorm_point = 19.0; double mass_at_renorm = 0.0037;

    //important: Choose your gluon
    //For the MT gluon
//    string which_gluon = "MT";
//    double solver_tolerance = 1e-8; double d_gluon_mt = 0.93312; double gluon_omega = 0.16;

    //For the MTPV gluon
    string which_gluon = "MTPV";
    double solver_tolerance = 1e-8; double d_gluon_mt = 1.01306; double gluon_omega = 0.16;
    double Pv_cutoff =4e+4;

    double IRcutsq= 1e-4;
    double UVcutsq = 1e+4;

    int n_ll = 40;
    int n_lz = 16;
    int n_ly = 24;

//    int n_ll = 16;
//    int n_lz = 4;
//    int n_ly = 4;

    int n_pp = n_ll;
    int n_zp = n_lz;
    int n_cheb =  4;

    int n_projector, n_basis;



    //Important Steps:-----------------WHICH METHOD FOR CALCUATIONS? --------------------------------------------

    //you can choose between: matrix, matrix_cheb, vector, vector_cheb as the iterative method.
//    string which_method = "vector_fullkernel";
//    string which_method = "vector";
    string which_method = "vector_cheb";
//    string which_method ="full_kernel_KG";
    cout<<"In this run we are using:     "<<which_method<<"    to solve the equation."<<endl;
    //-----------------Chose calculation method      --------------------------------------------



#ifdef K_NOT_USING_INPUT_FILE

//        DseParameter p_dse(n_rad_dse, "leg" , "log", n_ang_dse, "leg", "linear",UVcutsq_dse, IRcutsq_dse,
//                           renorm_point, mass_at_renorm,solver_tolerance ,
//                           "MT", d_gluon_mt, gluon_omega, Pv_cutoff, input_folder );

    DseParameter p_dse(n_rad_dse, "leg" , "log", n_ang_dse, "leg", "zero_Pi", UVcutsq_dse, IRcutsq_dse,
                       renorm_point, mass_at_renorm,solver_tolerance ,
                       which_gluon, d_gluon_mt, gluon_omega, Pv_cutoff, input_folder );

    //important: Watch out when choosing tscheb_zero this is not jet properly implemented


//        BseParameter bse_para(n_ll, n_lz, n_ly, n_pp, n_zp, n_cheb, n_basis, n_projector, "leg", "log", "leg", "ang_cos",
    BseParameter bse_para(n_ll, n_lz, n_ly, n_pp, n_zp, n_cheb, n_basis, n_projector,
                          "leg", "log", "leg", "linear", "leg", "linear",
//                              "leg", "log", "leg", "linear", "leg", "linear",
                          UVcutsq, IRcutsq, renorm_point, mass_at_renorm, which_gluon, input_folder, which_method);


#endif

    cout<<"In this session: "<<" n_ll="<<n_ll<<" , n_lz="<<n_lz<<" ,n_ly="<<n_ly<<" ,n_pp="<<n_pp<<" , n_zp="<<n_zp<<" , n_cheb="<<n_cheb<<
        " , IRcut²="<<IRcutsq<<" , UVcut²="<<UVcutsq<<" , quark mass="<<mass_at_renorm<< tab<< "  , gluon= "<<which_gluon<<endl;



/*    int n=45;
    Line l(-1.0,1.0,n,"linear","sinh_tanh", true, false);
    Line l2(-1.0,1.0,n,"linear","leg", true, false);
    Line l3(-1.0,1.0,8,"linear","tscheb_zeros", true, false);

    double t_sum1, t_sum2, t_sum3=0.0;
    for (int j = 0; j < n; ++j) {
        t_sum1 +=  sqrt(1.0- l.getGrid()[j] )*l.getWeights_integration()[j];
        t_sum2 +=  sqrt(1.0 - l2.getGrid()[j] ) *l2.getWeights_integration()[j];
    }
    for (int j = 0; j < l3.getGrid().size(); ++j) {
        t_sum3 +=  sqrt(1.0 - l3.getGrid()[j] )*l3.getWeights_integration()[j];
        cout<<l3.getGrid()[j]<<tab;
    }cout<<endl;

    cout<<"Calculations: sinhtanh, leg, tscheb  = "<<t_sum1<<tab<<t_sum2<<tab<<t_sum3<<endl;*/


    //What is calculated in this set-up
    bool calc_quark=false;
    array<bool,5> what_to_calculate = {1,1,0,0,0};



    //Important Steps:------------------------REAL QUARK------------------------------------------------------------------
    ///This is where the real quark is either calcualated or read inn.
    /// The default folder is set to be in input/real_quark.txt.
    /// First the DSE is initalized with the parameters (cut-offs,..) those are read Inn @ cons_dse.txt
    /// Then it is calculated or read_Inn. The parameter file has to match the read inn quark.
    time_t inbetween1, inbetween2;
    time(&inbetween1);

    QuarkDse dse((input_folder+"cons_dse.txt"));
    if(calc_quark){
        dse.calc_quark();
    }else{
        dse.Fill_QuarkDse(input_folder+"real_quark.txt");
    }
    dse.getQuark().write_quark(input_folder+"real_quark.txt");

    time(&inbetween2); give_time(inbetween1,inbetween2,"real quark");

    //-------------------REAl QUARK END-------------------------------------------------------------------


//    return 0;



    //Important Steps: Start-------------------BSE------------------------------------------------------------------------------

    //These indices are all needed to set up the meson amplitude/bse.
    // The only important thing to pass it the number of basis elements here
    // and if we are using chebys or direct angle.
    // I set them for vector, scalar and qphv, but depending on the program we might not need them.

//    array<int,3> bse_index_ve;
    array<int,3> bse_index_ps;
    array<int,3> bse_index_qphv;


    if(which_method == "matrix_cheb" || which_method == "vector_cheb"){
        bse_index_qphv= {12, bse_para.n_pp, bse_para.n_cheb}; cout<<"Using Chebys!"<<endl;
        bse_index_ps= {4, bse_para.n_pp, bse_para.n_cheb}; cout<<"Using Chebys!"<<endl;
    }else{
        bse_index_qphv= {12, bse_para.n_pp, bse_para.n_pz};
        bse_index_ps= {4, bse_para.n_pp, bse_para.n_pz};
    }

    ps_amplitude<3> amp_qphv(bse_index_qphv);
    ps_amplitude<3> amp_ps(bse_index_ps);


#ifdef K_CALC_SCALAR
    //SCALAR:
    //Now I want to calcuate a scalar meson.
    cout<<"||    ----------------------------------------------     ||"<<endl<<"      ---------scalar meson----------------       "<<endl;

    ps_MesonBse bse_scalar(amp_ps, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_ps, false, what_to_calculate, "scalar");
    bse_scalar.solve_and_update_pion(- 0.6843* 0.6843);
//    bse_scalar.find_the_bs_mass(0.67, 0.01, 0.0001); // found mass=0.675371874416 for (48, 24, 10)
    bse_scalar.write_out_amplitude(output_folder_ps+"scalar_meson.txt", true);
    bse_scalar.norm_the_meson();
    bse_scalar.write_out_amplitude(output_folder_ps+"normed_scalar_meson.txt", true);

    testing_iteration_scalar(bse_scalar, output_folder, "vector");

    bse_scalar.free_memory();

    cout<<endl<<endl<<"____________________________________"<<endl<<endl;
#endif



#ifdef K_CALC_QPHV

    time_t ib3, ib4;
    time(&ib3);




#ifdef K_TESTING_QPHV_ITER

    cout<<"Testing the Qphv naive interpolation:"<<endl;
    //testing the new set-up for the naive qphv.
//    qphv_MesonBse bse_qphv_3(amp_qphv, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_qphv, false, what_to_calculate);

    //Now we read Inn the quark-photon vertex grid data from a file an pre calcaute all needed qphv bses.
    VecCdoub ksquared; VecCdoub kP; VecDoub Psquared;
/*    where_is_vertex_needed_read_inn(output_folder_qphv+"quarkphoton_grid_1.txt", 20, ksquared, kP, Psquared);
    //And precalculate the corresponding qphv bses at all Q² values.
    precalc_bses_for_qphv(Psquared,bse_qphv_3,output_folder_qphv,1);
    bse_qphv_3.free_memory();*/

    //Now testing the iterpolation using one of the precalcuated and read amplitude Amp0. Calcualting the function on real values.
    qphv_MesonBse bse_qphv_2(amp_qphv, &dse, &dse, which_method, input_folder+"cons_bse.txt", output_folder_qphv, false, what_to_calculate);
/*    bse_qphv_2.init_via_file(output_folder_qphv+"Amp0/",bse_qphv.getPhaseSpacePsquared(),1);

//    testing_iteration_qphv(bse_qphv, output_folder, "vector");
    string methode_for_the_iteration="vector";
    testing_iteration_qphv(bse_qphv_2, output_folder_qphv, methode_for_the_iteration);

    bse_qphv_2.free_memory();*/



    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------


    //important: Here I am setting the values which I want the vertex calculated on:
    //Choose value for P
    //watch out can only be positve at the moment cause the produce_test_data function will otherwise complain.
    double mass_sq = 0.5;

    //produce test data on the real axis -
    produce_test_data(output_folder_qphv, mass_sq);


    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------


    //or alternativly exactly what should be in form factor main:
    int n_pvec=1; int which_vertex=1;
    where_is_vertex_needed_read_inn(output_folder_qphv+"quarkphoton_grid_1.txt", n_pvec , ksquared, kP, Psquared);


    bool precacualtion_of_vertex_already_done=false;
    if(!precacualtion_of_vertex_already_done){
        cout<<"Satring precaclculation of vertex"<<endl;
        precalc_bses_for_qphv(Psquared,bse_qphv_2,output_folder_qphv,which_vertex); }else{cout<<"precalculations are already done."<<endl;}

    //(1) Readin all the precalculated result. +
    //(2) Iterate qphv one more time at for all VALUES, to get our result
    //(3) Finally safe it into file, usable for the main formfactor.calc()
    //All included in following function():

    //important: What out for which method is choosen here!!
    //important: Always choose "vector" here. so far.
    calculate_all_qphv(bse_qphv_2, output_folder_qphv, Psquared, ksquared, kP,  "vector", which_vertex);


    //alternative test
//    bse_qphv_2.init_via_file(output_folder_qphv+"Amp"+to_string(0)+"/",Psquared[0],which_vertex);

//    qphv_MesonBse bse_qphv_3(amp_qphv, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_qphv, false, what_to_calculate);


    //Frist using of the old test function: test_iteration for all real values.
//    bse_qphv_2.calc_for_different_Psq({mass_sq}, "direct_Qphv");
//    testing_iteration_qphv(bse_qphv_2,output_folder_qphv,"vector");

    //Comment: Checked 04.12 - alternative test worked, for init_via_file as well as for direct calculations.
    //Comment: calcualte_all_qphv using the vector method also works. 05.12

#endif

    time(&ib4); give_time(ib3,ib4,"full quark photon vertex.");


#endif


#ifdef K_CALC_GERNOTS_INTERPOLATION

    #ifdef K_TESTING_QPHV_ITER

    string helper2 = "cd "+output_folder_qphv+ "; cp quarkphoton_grid_out_1.txt My_quarkphoton_grid_out_1.txt; cp quarkphoton_grid_out_2.txt My_quarkphoton_grid_out_2.txt;" +
             "rm quarkphoton_grid_out_1.txt; rm quarkphoton_grid_out_2.txt; ";
    int dummy2=system(helper2.c_str());

    #endif


    #ifdef K_CREATE_GERNOT_INPUT_DATA_HERE

    //important: choosing the values for the interpolation, here!!!!

    //step 1: Write out the interpolation values for the quark photon vertex

    string buffer = output_folder_qphv+"quarkphoton_grid_1.txt";
    ofstream f(buffer.c_str());

    f<<endl<<tab<<tab<<bse_para.n_lz*bse_para.n_ll<<endl<<endl<<tab<<tab<<"k2"<<tab<<tab<<tab<<"kQ"<<tab<<tab<<tab<<"Q2"<<endl<<endl;

    for (int i_ll = 0; i_ll < bse_para.n_ll; ++i_ll) {
        for (int i_zl = 0; i_zl < bse_para.n_lz; ++i_zl) {

                    f<<scientific<<tab<<real(ll.getGrid()[i_ll])<<tab<<0.0<<tab
                     <<real(sqrt(Ps*ll.getGrid()[i_ll])*zl.getGrid()[i_zl])<<tab<<imag(sqrt(Ps*ll.getGrid()[i_ll])*zl.getGrid()[i_zl])<<tab<<real(Ps)<<endl;
            }

    }

    f.close();

    #endif

    //step---------------------------------------------------------------------------|||||||||||||||||||||||||||||||||||||||||||||<
    //step 2: Call the external Fortran interpolation of the quark-photon vertex.
    mout<<"-------------------------------------"<<endl;
    mout<<"Starting the vertex calcaution/interpolation"<<endl<<"|...........................................|"<<endl;


    time_t starting6, dasende6;
    time(&starting6);

        string helper = "cp " + output_folder_qphv +"quarkphoton_grid_1.txt " + gethomdir() + "scripts/interpolate_qpv/ ; " +
                                                   "echo copied the input to interpolation. ; ";


        int dummy=system(helper.c_str());

        helper="cd "+gethomdir()+"scripts/interpolate_qpv/; ./Start_interpolation.sh";
        dummy=system(helper.c_str());
        //comment: had problems with the shell script. Was complaining about previous direct call with ./a.out, thus change it with a file.

        mout<<"|...........................................|"<<endl;

        helper="cd "+gethomdir()+"scripts/interpolate_qpv/; mv quarkphoton_grid_out_1.txt "+ output_folder_qphv +  ";" +
                + "echo interpolation done; rm quarkphoton_grid_1.txt; ";

//        helper="cd "+gethomdir()+"scripts/interpolate_qpv/; mv quarkphoton_grid_out_1.txt "+ output_folder_qphv +  ";" +
//               "mv quarkphoton_grid_out_2.txt " +output_folder_qphv+";" + "echo interpolation done; rm quarkphoton_grid_1.txt; "
//                                                                          "rm quarkphoton_grid_2.txt; ";
        dummy=system(helper.c_str());

        helper = "cd "+output_folder_qphv+ "; cp quarkphoton_grid_out_1.txt G_quarkphoton_grid_out_1.txt; cp quarkphoton_grid_out_2.txt G_quarkphoton_grid_out_2.txt;" +
             "rm quarkphoton_grid_out_1.txt; rm quarkphoton_grid_out_2.txt; ";
        dummy=system(helper.c_str());

#endif






    /* int QPV_interpolation=1;

     //step---------------------------------------------------------------------------|||||||||||||||||||||||||||||||||||||||||||||<
     //step 6: Call the external Fortran interpolation of the quark-photon vertex.
     mout<<"-------------------------------------"<<endl;
     mout<<"Starting the vertex calcaution/interpolation"<<endl<<"|...........................................|"<<endl;


     time_t starting6, dasende6;
     time(&starting6);
     //If the interpolation outputfiles are already calculated and ready to use in your current folder: ==0.
     if(QPV_interpolation==0){mout<<"QPV already calculated earlier. Done"<<endl;}
     else if(QPV_interpolation==1)
     //Gernots interpolation -
         //Calling Gernots Fortran program for the interpolation of the qphv vertex
     {
         string helper = "cp " + output_folder_qphv +"quarkphoton_grid_1.txt " + gethomdir() + "scripts/interpolate_qpv/ ; " +
                         "cp "+ output_folder_qphv +"quarkphoton_grid_2.txt " + gethomdir() + "scripts/interpolate_qpv/; "
                                                    "echo copied the input to interpolation. ; ";


         int dummy=system(helper.c_str());

 //        helper="cd interpolate_qpv/interpol/; ./interpolateprase.sh ";
 //        helper="cd "+gethomdir()+"scripts/interpolate_qpv/; ./a.out; echo first vertex done - V2(rm,Q); ./b.out; echo second vertex done - V1(rp,Qp);";
         helper="cd "+gethomdir()+"scripts/interpolate_qpv/; ./CallAb.sh";
         dummy=system(helper.c_str());
         //comment: had problems with the shell script. Was complaining about previous direct call with ./a.out, thus change it with a file.


         mout<<"|...........................................|"<<endl;

         helper="cd "+gethomdir()+"scripts/interpolate_qpv/; mv quarkphoton_grid_out_1.txt "+ output_folder_qphv +  ";" +
                "mv quarkphoton_grid_out_2.txt " +output_folder_qphv+";" + "echo interpolation done; rm quarkphoton_grid_1.txt; "
                                                                           "rm quarkphoton_grid_2.txt; ";

         dummy=system(helper.c_str());

         //extra for comparison:
         QPV_interpolation=2;
         helper = "cd " +output_folder_qphv+"cp quarkphoton_grid_1.txt G_quarkphoton_grid_1.txt" +"cp quarkphoton_grid_2.txt G_quarkphoton_grid_2.txt";
         dummy=system(helper.c_str());


 //    }else if(QPV_interpolation==2){
     }if(QPV_interpolation==2){
     //interpolation with the naive method, using my BSE class. - my code.

         //todo: important: Check on which varibales they depend on because of angle_project_out....
         VecCdoub ksquared; VecCdoub kP; VecDoub Psquared;

         //For now I am using the same grid points etc as for the bse here. Thus
 //        string dummy = "cp "+input_folder+"cons_bse.txt " +input_folder+"cons_qphv.txt;";
 //        system(dummy.c_str());

         //create the object that carries the meson qphv.
         BseParameter qphv_para(input_folder+"cons_qphv.txt");
         array<int,3> qphv_bse_index;
         if(which_method == "matrix_cheb" || which_method == "vector_cheb"){
             qphv_bse_index= {12, qphv_para.n_pp, qphv_para.n_cheb};
         }else{
             qphv_bse_index= {12, qphv_para.n_pp, qphv_para.n_pz};
         }
         ps_amplitude<3> amp_qphv(qphv_bse_index);

 //        qphv_MesonBse qphv_bse(amp_qphv, &dse, &dse, which_method , input_folder+"cons_qphv.txt", output_folder_qphv, false, what_to_calculate);


         //Precalculate ready to use qphv bses:
         //using the direct calculation of the vertex with my previous calculated qphv. qphv data must be in the folder
         //or calculated now.

         //loop for the 2 vertices .
         for (int which_vertex = 1; which_vertex < 3; ++which_vertex) {


             qphv_MesonBse qphv_bse(amp_qphv, &dse, &dse, which_method , input_folder+"cons_qphv.txt", output_folder_qphv, false, what_to_calculate);



             where_is_vertex_needed_read_inn(output_folder_qphv+"quarkphoton_grid_"+ to_string(which_vertex) +".txt", n_pvec , ksquared, kP, Psquared);

             if(!precacualtion_of_vertex_already_done){
                 //todo: Watch out - need to make sure that the method of the read in file is the same here!
                 precalc_bses_for_qphv(Psquared,qphv_bse,output_folder_qphv,which_vertex);  }

             //(1) Readin all the precalculated result. +
             //(2) Iterate qphv one more time at for all VALUES, to get our result
             //(3) Finally safe it into file, usable for the main formfactor.calc()
             //All included in following function():

             calculate_all_qphv(qphv_bse, output_folder_qphv, Psquared, ksquared, kP,  "vector", which_vertex);

             //Resetting things in the bse amplitude.
             //Need to reset the flag for the quarks apparently
             //todo: Check where this gets change in course of the bse calc procedure, as it must be and think about changing it there. - no.
             qphv_bse.setWhat_to_calc_flag({1,1,0,0,0});
             qphv_bse.reset_inhomogenous();


         }



     }else{assert(false);}
     time(&dasende6);give_time(starting6,dasende6,"QPV");
     mout<<endl<<"|...........................................|"<<endl;


 */


    time(&dasende); give_time(starting,dasende,"the whole main");
    string system_output1 = "cp "+gethomdir()+"tff.out "+output_folder_standart;
    system(system_output1.c_str());


    return 0;



}
