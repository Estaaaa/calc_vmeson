//Gerneral includes
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>
#include <fenv.h>
#include <xmmintrin.h>

//Program related definitions

#define K_WRITEOUT_ANGPROJC
//#define K_WRITEOUT_NOPROJ

#include <extra_def.h>

//Program related inculdes
#include <typedefs.h>
#include <ps_amplitude.h>
#include <Routing/dependence_of_all_kernels.h>
#include <Meson/MesonBse.h>
#include <homdir.h>
#include <fv.h>
#include "QuarkDse.h"
#include <formfactor.h>


//---------------------------------------------------
//MY PROGRAM SUMMARY AND TODO- LIST:
//Comment: Weird thing happend with the fourvector class. As there are overload operators something with the linking wasnt working
// Now I just useed an inline in front of some of them and it worked.... but why?? Okay, because of multiple inclusion in different cpp files.
//Another solution could be to use a cpp file for the fv.h, but I already tried that and probably did something wrong.


//---------------------------------------------------



/*------------------------------------START MAIN-----------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
int main (int argc, char * const argv[]) {

    //Command to seek for Naan in the program, debugger will stop when Naans appear and tell you where.
    //Comment: doesn't work on the Mac, maybe find alternative.
//    feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
//    feenableexcept(FE_INVALID | FE_OVERFLOW);
    _MM_SET_EXCEPTION_MASK(_MM_GET_EXCEPTION_MASK() & ~_MM_MASK_INVALID);
//    short fpflags = 0x1332; // Default FP flags, change this however you want.
//    asm("fnclex");
//    asm("fldcw _fpflags");



    //Preprogram set-ups:

    //Setting up time and folder structure:
    time_t starting, dasende;
    time(&starting);
    // Convert now to tm struct for local timezone
    time_t now = time(0);
    tm* localtm = localtime(&now);
    cout << "This program was run @  " << asctime(localtm) << endl;

    string folder="41";

    string output_folder_standart =gethomdir()+"data/data"+folder+"/";
    string output_folder =gethomdir()+"data/data"+folder+"/output_0/";
    string output_folder_ps =gethomdir()+"data/data"+folder+"/output_ps/";
    string output_folder_ve =gethomdir()+"data/data"+folder+"/output_ve/";
    string output_folder_qphv =gethomdir()+"data/data"+folder+"/output_qphv/";
    string input_folder=gethomdir()+"data/data"+folder+"/input/";

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<

    //Pick the numerical variable, such as gridsizes etc pp:
    ///-----------------------------------------------------------------------//
    //Is not needed with input file.
    //Setting up the parameters in here and writing them in a file.
    int n_rad_dse =150; int n_ang_dse=32;

    double IRcutsq_dse= 1e-4;
    double UVcutsq_dse = 1e+4;

    double renorm_point = 19.0; double mass_at_renorm = 0.0037;

    //important: Watchout if I am using MTPV gluon or MT, they have a different - and PV cutoff to choose.
    //--------- MT gluon:
//    string which_gluon = "MT";
//    double solver_tolerance = 1e-8; double d_gluon_mt = 0.93312; double gluon_omega = 0.16;
    //-------- MTPV gluon:
    string which_gluon = "MTPV";
    double solver_tolerance = 1e-8; double d_gluon_mt = 1.01306; double gluon_omega = 0.16;
    double Pv_cutoff =4e+4;

    double IRcutsq= IRcutsq_dse;
    double UVcutsq = UVcutsq_dse;

    int n_ll = 32;
    int n_lz = 16;
    int n_ly = 10;

    int n_pp = n_ll;
    int n_zp = n_lz;
    int n_cheb =  4;

    int n_projector, n_basis;

    cout<<"In this session: "<<" n_ll="<<n_ll<<" , n_lz="<<n_lz<<" ,n_ly="<<n_ly<<" ,n_pp="<<n_pp<<" , n_zp="<<n_zp<<" , n_cheb="<<n_cheb<<
        " , IRcut²="<<IRcutsq<<" , UVcut²="<<UVcutsq<<" , quark mass="<<mass_at_renorm<<endl;


    //JUMP <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    //---------------- CALCULATION FLAGS ------------------------------------
    //---------> DSE
    /// The DSE is calculated or read_Inn. The parameter file has to match the read inn quark.
    // - false = ReadInn  , - true=calculate:
    bool calc_quark= false;

    //---------> BSE
    //Or read in amplitude and calcaute wavefunction.
    //If the amplitude is read in the quarks S(k1) and S(k2) need to be calcauted as well
    // - false = ReadInn  , - true=calculate:
    bool calc_bse=true;
    // 0. quark1, 1. quark2, rest not used... suboptimal todo: change.
    array<bool,5> what_to_calculate = {1,1,0,0,0};

    bool calculate_chi=true;

    //---------> S3, quark at k3: S(k3):
    bool calculate_S3_flag=false;

    //---------> quark-photon vertex QPHV:
    // 1  - redo the calcaution/interpolation using Gernots Fortran routine
    // 2  - just copy a previous interpolation for the interpolation folder
    // 3  - just read Inn already exsisting data.
    // (4 - my vertex calculation ?)
    int QPV_interpolation=3;



//    TFFParameter TFF_para(input_folder+"cons_tff.txt");
//    UVcutsq_dse=UVcutsq=TFF_para.UVcutsq;
//    IRcutsq_dse=IRcutsq=TFF_para.IRcutsq;
//    which_gluon=TFF_para.which_gluon;
//    mass_at_renorm=TFF_para.mass_at_renorm_pt;
//    renorm_point=TFF_para.renomalisation_point;



#ifdef K_NOT_USING_INPUT_FILE


    DseParameter p_dse(n_rad_dse, "leg" , "log", n_ang_dse, "leg", "linear",UVcutsq_dse, IRcutsq_dse,
                       renorm_point, mass_at_renorm,solver_tolerance ,
                       which_gluon, d_gluon_mt, gluon_omega, Pv_cutoff, input_folder );


    TFFParameter TFF_para(n_ll, n_lz, n_ly, n_cheb,
                          "leg", "log", "leg", "linear", "leg", "linear",
                          UVcutsq, IRcutsq, renorm_point, mass_at_renorm, which_gluon, input_folder,
                          calc_quark, calc_bse, calculate_chi, calculate_S3_flag, QPV_interpolation);


    BseParameter bse_para(TFF_para.n_ll, TFF_para.n_lz, TFF_para.n_ly, TFF_para.n_ll, TFF_para.n_lz,
                          TFF_para.n_cheb, n_basis, n_projector,
//    BseParameter bse_para(n_ll, n_lz, n_ly, n_pp, n_zp, n_cheb, n_basis, n_projector,
//                          "leg", "log", "tscheb_zeros", "linear", "leg", "linear",
//                          "leg", "log", "leg", "linear", "leg", "linear",
                          TFF_para.point_distri_rad, TFF_para.func_meassure_rad,
                          TFF_para.point_distri_ang1, TFF_para.func_meassure_ang1,
                          TFF_para.point_distri_ang2, TFF_para.func_meassure_ang2,
                          UVcutsq, IRcutsq, renorm_point, mass_at_renorm, which_gluon, input_folder);


#endif



#ifdef K_USING_INPUT_FILE
    BseParameter bse_para(input_folder+"cons_bse.txt");
#endif



    //###########################################################################################################
    //########### PROGRAM BODY ################

    //step 0: Initilaize the phasespace for the triangle decay/TFF:
    //In this case most of this is grid points for the loop momenta.
    TFF_ps ps (TFF_para);
    TFF_para.write(input_folder);

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<



    //step 1: calculate/read the real quark propagator

    //------------------------REAL QUARK------------------------------------------------------------------
    ///This is where the real quark is either calcualated or read inn.
    /// The  default folder is set to be in input/real_quark.txt.
    /// First the DSE is initalized with the parameters (cut-offs,..) those are read Inn @ cons_dse.txt
    /// Then it is calculated or read_Inn. The parameter file has to match the read inn quark.
    // - false = ReadInn  , - true=calculate:

    time_t inbetween1, inbetween2;
    time(&inbetween1);

    QuarkDse dse((input_folder+"cons_dse.txt"));
    if(calc_quark){
        dse.calc_quark();
    }else{
        dse.Fill_QuarkDse(input_folder+"real_quark.txt");
    }
    dse.getQuark().write_quark(input_folder+"real_quark.txt");

    time(&inbetween2); give_time(inbetween1,inbetween2,"real quark"); cout<<"-------------------------------------"<<endl;

    //-------------------REAl QUARK END-------------------------------------------------------------------




    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<



    //step 2: calculate/read the meson amplitude Gamma(l,P)
    //flags for choosing what to calculate are above:

    /////////////////////////////////////// important MESON MASS //////////////////////////////////////////////
    //either the mass is directly set to a value, or a range is used to get the right mass corresponding to EV 1.0
    double meson_mass =  0.7; //important MASS !!!!!!!!
    int basis_meson = 8;

    //you can choose between: matrix, matrix_cheb, vector, vector_cheb as the iterative method.
    string which_method = "vector";
//    string which_method = "vector_cheb";
    cout<<"In this run we are using:     "<<which_method<<"    to solve the meson BSE."<<endl;

    array<int,3> bse_index;
    if(which_method == "matrix_cheb" || which_method == "vector_cheb"){
        bse_index= {basis_meson, bse_para.n_pp, bse_para.n_cheb};
    }else{
        bse_index= {basis_meson, bse_para.n_pp, bse_para.n_pz};
    }

    ps_amplitude<3> meson_amp(bse_index);
    double decayconstant;

    //Calculation of the meson - here the previous dse is passed.
    //Here we have to choose by a string and the explicit chioce of the class,
    // which meson we are calcuating. ("ps", "ve", ...)
    vector_MesonBse bse(meson_amp, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_ve, false, what_to_calculate, "axial");
    if(calc_bse){
//        bse.solve_and_update_pion(- meson_mass *  meson_mass);
        meson_mass = bse.find_the_bs_mass(meson_mass, 0.05, 0.0001);
//        bse.norm_the_meson(); cout<<"--------------------> Meson is normed"<<endl;
//        decayconstant= bse.get_decay_constant(); //There is no decay constant in this case.
        bse.write_out_amplitude(output_folder_ve+"axial_meson_amplitude.txt");
    }else{
        bse.read_amplitude(output_folder_ve+"axial_meson_amplitude.txt");
    }

    double decayconstant_fixed=0.093;

    //important: setting the mass of the boundstate in the phase space.
    // Here cause it could be that you'll search the corresponding mass.
    ps.setMass(meson_mass);

    //Here I want to pass the information of the first and the second quark.
    //todo: Don't know if this is working jet!!! maybe cause I deleted the quarks after the first run?
    vector<array<Cdoub,2>> quark1;
    vector<array<Cdoub,2>> quark2;

    if(!calc_bse){
        bse.calculate_two_quarks(- meson_mass *  meson_mass);
    }
    bse.get_me_content_of_quarks(quark1, quark2);


    //important: DEBUGGIN Set chi=amp!
    //step 2(b): Calculating Chi.
    cout<<"calculation of Chi"<<endl<<"|...........................................|"<<endl;
    ps_amplitude<3> chi(bse_index);
    vector<int> amp_flag;
    if(calculate_chi){
        bse.calculate_chi(chi,amp_flag);
//        chi=bse.meson;
    }else{
        chi.read_amplitude(output_folder_ve,1,2);
    }
    cout<<"-------------------------------------"<<endl;



    //Free the memory by using the destructor but keep meson_amp
    //todo: Also can not free the memory like this... what now?
//    delete bse;

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<


    return 0;

    /////////////////////////////////////// important choose P //////////////////////////////////////////////
    //Now that the mass for the decay is set.

    //step 0: Choose the momenta for the relative momentum between the photons for this run (p):
    //----------> Choices for the two photon momenta so far:
    //For the anomaly point
//    VecCdoub Q = {0.0};
//    VecCdoub Qp = {0.0};

    //in the symmetric limit up to 100 GeV^2.
    VecCdoub Q, Qp;
    Line Qline(0.0, 1.0, 20, "linear", "leg", false, false);
    for (int i = 0; i < Qline.getNumber_of_grid_points(); ++i) {
        Q.push_back(Qline.getGrid()[i]);
    }
    Qp=Q;   //picking Symmetric!!!!!! momenta.

    int n_pvec = Q.size();
    cout<<"p values of this session: "<<endl;
    for (int i_pvec = 0; i_pvec < Q.size(); ++i_pvec) {
        ps.Q_Qp.push_back({Q[i_pvec], Qp[i_pvec]});
        fv<Cdoub> p=get_p_vec(Q[i_pvec], Qp[i_pvec], meson_mass);
        cout<<"p=( "<<p.content[0]<<" , "<<p.content[1]<<" , "<<p.content[2]<<" , "<<p.content[3]<<endl;
    }
    cout<<"|...........................................|"<<endl;

    cout<<"In this session: n_pvec= "<<n_pvec<<endl;


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<


//    cout<<quark1[0][0]<<endl;

    //step 4: Write out the interpolation values for the quark photon vertex
    //Now that the mass for rounting is set -
    //Here: Also write out the momentum files for the vertex:
    output_qphv_grid(output_folder_qphv, ps, 1);
    output_qphv_grid(output_folder_qphv, ps, 2);


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<

    //step---------------------------------------------------------------------------|||||||||||||||||||||||||||||||||||||||||||||<
    //step 3: calculate/read the complex quark S(k3)
    cout<<"calculation of the complex quark S(k3)"<<endl<<"|...........................................|"<<endl;


    time_t inbetween_s3s, inbetween_s3e;
    time(&inbetween_s3s);

    vector<array<Cdoub,2>> quark3;

    if(calculate_S3_flag){

       quark3 = calculate_S3(ps,output_folder, dse);
    }else{
        quark3 = read_S3(output_folder+"Sk3.txt");
    }

    time(&inbetween_s3e); give_time(inbetween_s3s,inbetween_s3e,"complex quark S(k3) done");
    cout<<endl<<"|...........................................|"<<endl;


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<


    //step---------------------------------------------------------------------------|||||||||||||||||||||||||||||||||||||||||||||<
    //step 5: Call the external Fortran interpolation of the quark-photon vertex.
    cout<<"-------------------------------------"<<endl;
    cout<<"Starting the vertex calcaution/interpolation"<<endl<<"|...........................................|"<<endl;


    time_t starting6, dasende6;
    time(&starting6);
    if(QPV_interpolation==1)
    {
        string helper = "cp " + output_folder_qphv +"quarkphoton_grid_1.txt " + gethomdir() + "scripts/interpolate_qpv/ ; " +
                        "cp "+ output_folder_qphv +"quarkphoton_grid_2.txt " + gethomdir() + "scripts/interpolate_qpv/; "
                                                   "echo copied the input to interpolation. ; ";


        int dummy=system(helper.c_str());

//        helper="cd interpolate_qpv/interpol/; ./interpolateprase.sh ";
        helper="cd "+gethomdir()+"scripts/interpolate_qpv/; ./a.out; echo frist vertex done; ./b.out; echo second vertex done;";
        dummy=system(helper.c_str());

        cout<<"|...........................................|"<<endl;

        helper="cd "+gethomdir()+"scripts/interpolate_qpv/; mv quarkphoton_grid_out_1.txt "+ output_folder_qphv +  ";" +
               "mv quarkphoton_grid_out_2.txt " +output_folder_qphv+";" + "echo interpolation done; rm quarkphoton_grid_1.txt; "
                                                                          "rm quarkphoton_grid_2.txt; ";

//        helper="cd "+gethomdir()+"scripts/interpolate_qpv/; mv quarkphoton_grid_out_1.txt "+ output_folder_qphv +  ";" +
//               "mv quarkphoton_grid_out_2.txt " +output_folder_qphv+";" + "echo interpolation done; rm quarkphoton_grid_1.txt; "
//                                                                          "rm quarkphoton_grid_2.txt; ";
        dummy=system(helper.c_str());


    }else if(QPV_interpolation==2){


        string helper="cd "+gethomdir()+"scripts/interpolate_qpv/; cp quarkphoton_grid_out_1.txt " +  output_folder_qphv+";" +
                      "cp quarkphoton_grid_out_2.txt " + output_folder_qphv+";" "echo copying interpolation done.;";
        int dummy=system(helper.c_str());

    } else{cout<<"QPV already calculated earlier. Done"<<endl;}
    time(&dasende6);give_time(starting6,dasende6,"QPV");
    cout<<endl<<"|...........................................|"<<endl;




    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<


    //step---------------------------------------------------------------------------|||||||||||||||||||||||||||||||||||||||||||||<
    //step 6: Calculate the form factor - putting together all components - final calcaution
    cout<<"-------------------------------------"<<endl;
    cout<<"Main calculation: Form factor"<<endl<<"|...........................................|"<<endl;

    time_t starting7, dasende7;
    time(&starting7);

    VecCdoub TFF_vec;

    formfactor TFF(ps, output_folder_qphv);
    TFF.set_kernel_func("axial");
    TFF_vec = TFF.calc_formfactor(quark1, quark2, quark3, chi );

    time(&dasende7);give_time(starting7,dasende7,"form factor");
    cout<<"|...........................................|"<<endl<<endl;

    TFF.write((1.0/real(TFF_vec[0])), TFF_vec, "F[0]_");

    cout<<"With the factor infront we obtain:    F= "<<4.0*M_PI*M_PI*decayconstant_fixed *TFF_vec[0]<<
        tab<< 4.0*M_PI*M_PI*decayconstant *TFF_vec[0]<<endl;

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<


    time(&dasende); give_time(starting,dasende,"the whole main");
    string system_output1 = "cp "+gethomdir()+"tff.out "+output_folder_standart;
    system(system_output1.c_str());

    return 0;



}

