//Gerneral includes
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>
#include <fenv.h>
#include <xmmintrin.h>

//Program related definitions

#define K_WRITEOUT_ANGPROJC
//#define K_WRITEOUT_NOPROJ

//specify which Maris-Tandy paramters are used in the code.
//#define USE_MT_PARA_FOLDER_30
#define USE_MT_PARA_FOLDER_28

#define K_NOT_USING_INPUT_FILE

#include <extra_def.h>

//Program related inculdes
#include <typedefs.h>
#include <ps_amplitude.h>
#include <Routing/dependence_of_all_kernels.h>
#include <Meson/MesonBse.h>
#include <homdir.h>
#include <fv.h>
#include "QuarkDse.h"
#include <formfactor.h>
#include <mystream.h>
#include <qphv_naive_method.h>
#include <tff_addons.h>


mstream mout(gethomdir()+"output_file.out");


//---------------------------------------------------
//MY PROGRAM SUMMARY AND TODO- LIST:
//Comment: Weird thing happend with the fourvector class. As there are overload operators something with the linking wasnt working
// Now I just useed an inline in front of some of them and it worked.... but why?? Okay, because of multiple inclusion in different cpp files.
//Another solution could be to use a cpp file for the fv.h, but I already tried that and probably did something wrong.


//---------------------------------------------------

//#define K_CALC_EACH_AMP_COMPONTENT

/*------------------------------------START MAIN-----------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
int main (int argc, char * const argv[]) {

    //Command to seek for Naan in the program, debugger will stop when Naans appear and tell you where.
    //Comment: doesn't work on the Mac, maybe find alternative.
//    feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
//    feenableexcept(FE_INVALID | FE_OVERFLOW);
    _MM_SET_EXCEPTION_MASK(_MM_GET_EXCEPTION_MASK() & ~_MM_MASK_INVALID);
//    short fpflags = 0x1332; // Default FP flags, change this however you want.
//    asm("fnclex");
//    asm("fldcw _fpflags");

    //General Comments often forgotten:
    // For the qphv vertices:
    //Q' -   V1(r+, Q') - Qfs - Qp
    //-Q -   V2(r-, -Q) - Qis - Q




    //Preprogram set-ups: -  All falgs and gridpoint choices -

    //Setting up time and folder structure:
    time_t starting, dasende;
    time(&starting);
    // Convert now to tm struct for local timezone
    time_t now = time(0);
    tm* localtm = localtime(&now);
    mout << "This program was run @  " << asctime(localtm) << endl;


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //All choices: flags points usw.

    //important: FIRST THING WHEN USING THE GIT BETWEEN MAC AND OLIVER
    //important: - Create the right b.out files for the interpolation!

    //Chose which folder:
    string folder="848";
    string flag_for_running_multipl_codes="0";

    //Pick the numerical variable, such as gridsizes etc pp:
    ///-----------------------------------------------------------------------//
    //Is not needed with input file.
    //Setting up the parameters in here and writing them in a file.
    //DSE
    int n_rad_dse =150; int n_ang_dse=64;
//    int n_rad_dse =150; int n_ang_dse=32;

    ///Changes the higher cutoff here.!
    double IRcutsq_dse= 1e-6;
//    double UVcutsq_dse = 1e+6;
    double UVcutsq_dse = 1e+6;

    //important: Watchout if I am using MTPV gluon or MT, they have a different - and PV cutoff to choose.
    //--------- MT gluon:    or   //-------- MTPV gluon:
    string which_gluon = "MT";
//    string which_gluon = "MTPV";

    string which_gluon_for_dse= "MTPV";

    //Main Program
//    double Pv_cutoff =0.2e+3;
    double Pv_cutoff =4e+4;
    //only used for the DSE part as there we use MTPV.

    double IRcutsq= IRcutsq_dse;
    double UVcutsq = UVcutsq_dse;
//    double UVcutsq = 1e+4;

//    int n_bse_ll= 60;
    int n_bse_ll= 48; // To read inn Gernots data file. This amount of rad point is needed.
    int n_bse_lz = 32;
    int n_bse_ly = 36;


//    int n_ll = 36;
//    int n_lz = 24;
//    int n_ly = 28;

//    int n_ll = 60; //40
//    int n_lz = 32; //24
//    int n_ly = 36; //28

//#first run for comparison 15.10.19- data833-data841
//#third run 22.10.19
    int n_ll = 40;
    int n_lz = 24;
    int n_ly = 28;

//#second run: 16.10.109 from data842
//    int n_ll = 60;
//    int n_lz = 36;
//    int n_ly = 32;

//    int n_ll = 80; //40
//    int n_lz = 36; //24
//    int n_ly = 40; //28

    int n_pp = n_ll;
    int n_zp = n_lz;
    int n_cheb =  4;
//
//    n_bse_ll=n_ll;
//    n_bse_lz=n_lz;
//    n_bse_ly=n_ly;


    //FLAGS <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    //---------------- CALCULATION FLAGS ------------------------------------
    //---------> DSE
    /// The DSE is calculated or read_Inn. The parameter file has to match the read inn quark.
    // - false = ReadInn  , - true=calculate:
    bool calc_quark= false;

    //---------> BSE
    //Or read in amplitude and calculated wavefunction.
    //If the amplitude is read in the quarks S(k1) and S(k2) need to be calculated as well
    // - 0 = ReadInn  ,
    // - 1= calculate (find mass for ev=1)  ,
    // - 2= calculate for chosen value mpi (ev irrelevant):
    // - 3= skip for now.
    // - 5= interpolate on an exsisting data-set. The data set will be called ..._precalc.txt afterwards.
    // - 6= combine 1 + 5
    // - 7 = Read in external data for meson -Gernot
    int calc_bse= 5;
    //if calc_bse=5: how many rad points does previous file have. -> Later this should be automated
    int previous_rad_points_for_interpolation = n_bse_ll;
    bool is_the_read_file_in_chebys = true;
    bool using_gernots_meson=true;


    //old- ignore
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // 0. quark1, 1. quark2, rest not used... suboptimal todo: change.
    array<bool,5> what_to_calculate = {1,1,0,0,0};

    //----Chi : --------------
    // 0 Read Inn
    //1 calculate
    //important- Watch out before changing, need to change stuff in the bse main too.
    //2 chi=bse amp. - in case when only using the Amlitude not the wavefunction(chi).
    //This choice is when working in a different frame alpa!=1000, as the chi rotation is only written in this frame.
    //(Remember: To change the same choice in the formfactor code as well - uncommenting.) important!!!!
    //Introduced check below when alpha is choosen.
    //2 - also do nothing - calculationwise as chi=amp.   -most of the times this is the best choice, as I don't really use chi anymore currently.
    //3 - do nothing.
    //important: DO NOT TOUCH! never change at the moment, old set-up choices. =2
    int calculate_chi=2;
    if(calc_bse==3){calculate_chi=3;}
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //---------> S3, quark at k3: S(k3):
    // 0 - ReadIn
    // 1 - Calculate
    // 3 -interpolate with external Fortran function
    // 4 - skip for debbuging purposes
    int calculate_S3_flag=3;
    int calculate_Spm_flag=3;
    //Setting the flag for Reading in the quark Sp, Sm
    //(0) Fortran interpolated quarks   (1) my quarks  (2) quark form the bse- my other folder.
    int read_quarks_where=0;
    //If 0 : For S(k1) and S(k2) we need to specify which file is read inn. Specify below.

    //---------> quark-photon vertex QPHV:
    // 1  - FORTRAN: redo the calculation/interpolation using Gernots Fortran routine
    // 2  - MY: my vertex interpolation - naive method.
    // 0  - NOTHING: if already done! - just do nothing, as it will readInn the correspodning data later.
    int QPV_interpolation=1;
    //When using my own vertex - case 2:
    bool precacualtion_of_vertex_already_done=true; //this flag is only relevant in case =2 above, when using my naive method.


    //TFF: form factor - inerpolation.
    // true - calc, false - do nothing
    bool flag_calculate_tff=true;


    //Frame parameter alpha changes the frames for the TFF from moving to rest frame.
    //important ALPHA
//    double alpha= 0.0;
//    double alpha= 0.5;
//    double alpha= - 3.0/2.0;
    double alpha= 1e+20;
//    if(alpha < 1e+10){calculate_chi=2; }//assert(false);} //one has to change which functions will be used in the form factor main!}
    //important: there is a flag at the beginning of the formfactor.cpp file, that takes care of this.

    /////////////////////////////////////// important MESON MASS //////////////////////////////////////////////
    //either the mass is directly set to a value, or a range is used to get the right mass corresponding to EV 1.0
    //Which meson - on this case it could be scalar or ps (pesudo-scalar)
//    string which_meson="ps";
//    double meson_mass =  0.135;
    string which_meson="scalar";
//    double meson_mass =  0.68635407299;
//    double meson_mass = 0.685;
    double meson_mass = 0.675;
//    double meson_mass = 0.655;
    Cdoub Psquared = - meson_mass*meson_mass;
//    bool flag_force_the_external_tff_mass_to_a_value=true; // is this variable used? delte
    int amount_of_q_grid_points = 60;



    //some random constant that are still in here: maybe delete at some point
    double decayconstant_fixed=0.092;

    //Also this is kinda outdated... are chaning the amount of basis elements doesnt really work. So never change this.
    //important!!!!! DO NOT CHANGE HERE!!!! doesnt work with less basis elements.
    int basis_meson = 4;


    //important: Where am I calculating: Q^2, Q'^2.
    //additional new falg to choose if symmetric or antisymmetric.
    //0: on-shell point, 1: symmetrisch, 2: anti-sym.
    // 3 : new - Grid in Q² and Q'².
    // 5 : Richard set-up with omega for comparison.
    // 6: One point. Atm eta^+ = 1 GeV^2.
    //7: symmetric limit set up with the omega and eta_+ paramters.
    //7: symmetric limit set up with the omega and eta_+ paramters.
    //(in princple this can get many new cases when further values are need. For testing purposes this cases are avialable)
    int grid_sym_flag=7;
    bool flag_using_outside_Qs_or_eta=false;
    // (0) deletes file and doesnt append, (1) appends,  (2) does  - no deleting, but also no appending.
    int append_to_older_calc= 0;

    //either interpolating on Richards or Gernots data:
    // "R" - Richard - folder 30 MT
    // "G" - Gernot - folder 30 MT
    // "old" - folder 28 M paramter quark propagator
    string which_quark_interpolation_data;  //which_vertex_interpolation_data;
#ifdef USE_MT_PARA_FOLDER_30
    which_quark_interpolation_data="R";
#endif
#ifdef USE_MT_PARA_FOLDER_28
    which_quark_interpolation_data="old";
//    which_vertex_interpolation_data ="old";
#endif




    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<

    //Which method to calcuate the meson:
    //you can choose between: matrix, matrix_cheb, vector, vector_cheb as the iterative method.
//    string which_method = "vector";
    string which_method = "vector_cheb";

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<




    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //Variable set-up at the beginning with choices above  - ignore

    //folders:
    string output_folder_standart =gethomdir()+"data/data"+folder+"/";
    string output_folder =gethomdir()+"data/data"+folder+"/output_0/";
    string output_folder_ps =gethomdir()+"data/data"+folder+"/output_ps/";
    string output_folder_ve =gethomdir()+"data/data"+folder+"/output_ve/";
    string output_folder_qphv =gethomdir()+"data/data"+folder+"/output_qphv/";
    string input_folder=gethomdir()+"data/data"+folder+"/input/";

    //Readin Inn quarks: If quarks are read in where are they found?
    //(0) Fortran interpolated quarks   (1) my quarks  (2) quark form the bse- my other folder.
    string where_is_quark12,where_is_quark3;
//    if (read_quarks_where ==0 ){where_is_quark12 = output_folder + "quark_grid_out_"; where_is_quark3=output_folder+"quark_grid_out_3.txt";}
    if (read_quarks_where ==0 ){where_is_quark12 = output_folder + "quark_grid_"; where_is_quark3=output_folder+"quark_grid_3.txt";}
    else if(read_quarks_where==1){where_is_quark12=output_folder+"Spm_"; where_is_quark3=output_folder+"my_quark_grid_out_3.txt";}
    else if (read_quarks_where ==2 ){where_is_quark12 = output_folder_ps + "Cquark";}


    //Mass at renormization point:
//        double renorm_point = 19.0; double mass_at_renorm = 0.0037;
    double renorm_point = 19.0; double mass_at_renorm = 0.0035748; //changed mass according to Gernots file.


    //Gluon
    double solver_tolerance; double d_gluon_mt; double gluon_omega_sq;
    if(which_gluon=="MT"){
//        solver_tolerance = 1e-8;  d_gluon_mt = 0.93312;  gluon_omega_sq = 0.16; //my usual set-up (old)
#ifdef USE_MT_PARA_FOLDER_30
        solver_tolerance = 1e-8;  gluon_omega_sq = 0.1296;  d_gluon_mt = pow(0.74,3.0)/(sqrt(gluon_omega_sq)); //for Gernots interpolation - folder 30
#endif
#ifdef USE_MT_PARA_FOLDER_28
        solver_tolerance = 1e-8;  gluon_omega_sq = 0.16;  d_gluon_mt = pow(0.74,3.0)/(sqrt(gluon_omega_sq)); //Gernot folder 28 of everything
#endif
    }else if(which_gluon == "MTPV"){
        //important  - changed omega here 16.01.19, due to using the other data-set (second line - inputData_02 & inputData_03): first line inputData_01_old
//        solver_tolerance = 1e-8;  d_gluon_mt = 1.01306;  gluon_omega_sq = 0.1296; //older set-up
//        solver_tolerance = 1e-8;  d_gluon_mt = 1.01306;  gluon_omega_sq = 0.16; //inputData_01 - folder 28
#ifdef USE_MT_PARA_FOLDER_30
        solver_tolerance = 1e-8;  gluon_omega_sq = 0.1296;  d_gluon_mt = pow(0.74,3.0)/(sqrt(gluon_omega_sq)); //inputData_02 - okay, i read off the wrong value for D - folder 30
#endif
#ifdef USE_MT_PARA_FOLDER_28
        solver_tolerance = 1e-8;  gluon_omega_sq = 0.16;  d_gluon_mt = pow(0.74,3.0)/(sqrt(gluon_omega_sq)); //folder 28
#endif //D=1.12562; This one is used with the iFortran interpoaltion sQ, sl (Feb 2019)
    }else{assert(false);}


    //Creating the folder
//    string system_output2 = "cp -r "+gethomdir()+"data/data00 "+gethomdir()+"data/data"+folder;
//    int pp_error= system(system_output2.c_str());

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<

    //Print to screen commands for overview:

    mout<<output_folder_standart<<endl;
    mout<<"In this session: "<<" n_ll="<<n_ll<<" , n_lz="<<n_lz<<" ,n_ly="<<n_ly<<" ,n_pp="<<n_pp<<" , n_zp="<<n_zp<<" , n_cheb="<<n_cheb<<
        " , IRcut²="<<IRcutsq<<" , UVcut²="<<UVcutsq<<" , quark mass="<<mass_at_renorm<<endl;

    mout<<"Selected flags: calc_quark="<<calc_quark<<" ,calc_bse="<<calc_bse<<" ,calc_chi="<<calculate_chi<<" ,calc_S3="<<calculate_S3_flag
        <<" ,QPV_interpol="<<QPV_interpolation<<", alpha="<<alpha<<" , grid_sym="<<grid_sym_flag<<endl;

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<


#ifdef K_NOT_USING_INPUT_FILE


//    DseParameter p_dse(n_rad_dse, "leg" , "log", n_ang_dse, "leg", "linear",UVcutsq_dse, IRcutsq_dse,
//    DseParameter p_dse(n_rad_dse, "leg_split" , "log", n_ang_dse, "leg", "zero_Pi",UVcutsq_dse, IRcutsq_dse,
    DseParameter p_dse(n_rad_dse, "leg_split" , "log", n_ang_dse, "leg", "linear",UVcutsq_dse, IRcutsq_dse,
                       renorm_point, mass_at_renorm,solver_tolerance ,
//                       which_gluon, d_gluon_mt, gluon_omega_sq, Pv_cutoff, input_folder );
                       which_gluon_for_dse, d_gluon_mt, gluon_omega_sq, Pv_cutoff, input_folder );


    TFFParameter TFF_para(n_ll, n_lz, n_ly, n_cheb,
                          "leg", "log", "leg", "linear", "leg", "linear",
//                          "leg_split", "log", "leg", "linear", "leg", "linear",
//                          "leg", "log", "sinh_tanh", "linear", "sinh_tanh", "linear",
                          UVcutsq, IRcutsq, renorm_point, mass_at_renorm, which_gluon, input_folder);
//                          calc_quark, calc_bse, calculate_chi, calculate_S3_flag, QPV_interpolation);


    BseParameter bse_para(n_bse_ll, n_bse_lz, n_bse_ly, n_bse_ll, n_bse_lz,
//    BseParameter bse_para(TFF_para.n_ll, TFF_para.n_lz, TFF_para.n_ly, TFF_para.n_ll, TFF_para.n_lz,
                          TFF_para.n_cheb, basis_meson, basis_meson,
//    BseParameter bse_para(n_ll, n_lz, n_ly, n_pp, n_zp, n_cheb, n_basis, n_projector,
//                          "leg", "log", "tscheb_zeros", "linear", "leg", "linear",
//                          "leg", "log", "leg", "linear", "leg", "linear",
//                          "leg_split", "log",
                          TFF_para.point_distri_rad, TFF_para.func_meassure_rad,
                          TFF_para.point_distri_ang1, TFF_para.func_meassure_ang1,
                          TFF_para.point_distri_ang2, TFF_para.func_meassure_ang2,
                          UVcutsq, IRcutsq, renorm_point, mass_at_renorm, which_gluon, input_folder, which_method);


    write_general_run_parameter(input_folder+"cons_run.txt", folder, calc_quark ,calc_bse ,using_gernots_meson , is_the_read_file_in_chebys , previous_rad_points_for_interpolation ,
                                which_method ,calculate_S3_flag , calculate_Spm_flag , read_quarks_where , QPV_interpolation ,flag_calculate_tff ,  alpha ,
                                which_meson ,meson_mass , amount_of_q_grid_points ,grid_sym_flag  , append_to_older_calc );

#endif

//comment: this version is not completed jet.
#ifdef K_USING_INPUT_FILE
    DseParameter p_dse(input_folder + "cons_dse.txt");
    BseParameter bse_para(input_folder+"cons_bse.txt");

    read_general_run_parameter(input_folder+"cons_run.txt", folder, calc_quark ,calc_bse ,using_gernot_meson , is_the_read_files_in_chebys , previous_rad_points_for_interpolation ,
                                 which_method ,calculate_S3_falg , calculate_Spm_flag , read_quarks_where , QPV_interpolation ,calculate_tff ,  alpha ,
                                 which_meson ,meson_mass , amount_of_q_grid_points ,grid_sym_falg , flag_using_outside_Qs_or_eta , append_to_older_calc )

    TFFParameter TFF_para(input_folder+"conse_tff.txt")
#endif

    bool tff_grid_equals_bse_grid=true;
    tff_grid_equals_bse_grid=tff_grid_equals_bse_grid*(bse_para.n_ll==TFF_para.n_ll)*(bse_para.n_lz==TFF_para.n_lz)
                             *(bse_para.point_distri_rad==TFF_para.point_distri_rad) * (bse_para.func_meassure_rad==TFF_para.func_meassure_rad )
                             * (bse_para.func_meassure_ang1==TFF_para.func_meassure_ang1);



    //###########################################################################################################
    //########### PROGRAM BODY ################

    //step 0: INTI - Initilaize the phasespace for the triangle decay/TFF:
    //In this case most of this is grid points for the loop momenta.
    TFF_ps ps (TFF_para);
    TFF_para.write(input_folder);

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<


    //step 1: QUARK - calculate/read the real quark propagator

    //------------------------REAL QUARK------------------------------------------------------------------
    ///This is where the real quark is either calcualated or read inn.
    /// The  default folder is set to be in input/real_quark.txt.
    /// First the DSE is initalized with the parameters (cut-offs,..) those are read Inn @ cons_dse.txt
    /// Then it is calculated or read_Inn. The parameter file has to match the read inn quark.
    // - false = ReadInn  , - true=calculate:

    time_t inbetween1, inbetween2;
    time(&inbetween1);

    QuarkDse dse((input_folder+"cons_dse.txt"));
    if(calc_quark){
        dse.calc_quark();
    }else{
        dse.Fill_QuarkDse(input_folder+"real_quark.txt");
    }
    dse.getQuark().write_quark(input_folder+"real_quark.txt");

    time(&inbetween2); give_time(inbetween1,inbetween2,"real quark"); mout<<"-------------------------------------"<<endl;

    //-------------------REAl QUARK END-------------------------------------------------------------------

//    return 0;

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<

    //step 2: MESON -calculate/read the meson amplitude Gamma(l,P)   - BSE -
    //------------------------MESON BSE------------------------------------------------------------------
    //flags for choosing what to calculate are above:


    mout<<"In this run we are using:     "<<which_method<<"    to solve the meson BSE."<<endl;

    array<int,3> bse_index;
    if(which_method == "matrix_cheb" || which_method == "vector_cheb"){
        bse_index= {basis_meson, bse_para.n_pp, bse_para.n_cheb};
    }else{
        bse_index= {basis_meson, bse_para.n_pp, bse_para.n_pz};
    }

    ps_amplitude<3> meson_amp(bse_index);
    double decayconstant;

    //These are the parameters for the actual size of the bse after possible interpolatione tc
    array<int,3> actual_bse_index;
    actual_bse_index= {basis_meson, TFF_para.n_ll, TFF_para.n_lz}; SuperIndex<3> actual_index_of_bse({basis_meson, TFF_para.n_ll, TFF_para.n_lz});

    //Calculation of the meson - here the previous dse is passed.
    //Here we have to choose by a string and the explicit chioce of the class,
    // which meson we are calcuating. ("ps", "ve", ...)
    ps_MesonBse bse(meson_amp, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_ps, false, what_to_calculate, which_meson);
    if(calc_bse==1 || calc_bse==6){
        meson_mass = bse.find_the_bs_mass(meson_mass, 0.005, 0.0001);
        bse.norm_the_meson(); mout<<"--------------------> Meson is normed"<<endl;
        decayconstant= bse.get_decay_constant(); //There is no decay constant in this case.
        bse.write_out_amplitude(output_folder_ps+ which_meson +"_meson_amplitude.txt");
        Psquared= - meson_mass*meson_mass;
    }else if (calc_bse==2){
        bse.solve_and_update_pion(- meson_mass *  meson_mass);
        bse.meson.setPsquared(- meson_mass * meson_mass);
        bse.norm_the_meson(); mout<<"--------------------> Meson is normed"<<endl;
        decayconstant= bse.get_decay_constant(); //There is no decay constant in this case.
        bse.write_out_amplitude(output_folder_ps+ which_meson +"_meson_amplitude.txt");
        Psquared= - meson_mass*meson_mass;
    }else if (calc_bse==0){
        //Changing the index for Read In.
        bse_index=actual_bse_index;
        bse.meson.setSi(actual_index_of_bse);

        Psquared = bse.read_amplitude(output_folder_ps+which_meson+"_meson_amplitude.txt");
//        bse.read_amplitude(output_folder_ps+"ps"+"_meson_amplitude.txt");
//        bse.setSolver("")
        //important: debug - changed something here: uncommented the calc_two_quarks. Usually using the quark1/2 interpolation later anyway.
//        bse.calculate_two_quarks(- meson_mass *  meson_mass);
//        bse.setAlready_deleted_quarks(true);
    }else if(calc_bse == 3){
        cout<<" -Skpipping the BSE calc"<<endl; calculate_chi=3; //Need to skip Chi calculation as well.
    }else if(calc_bse == 5){}

    if(calc_bse==6){
        cout<< "interpolation need due to different grids in BSE and TFF"<<endl;
        string helper = "mv "+ output_folder_ps+ which_meson +"_meson_amplitude.txt"+ " " +output_folder_ps+ which_meson +"_meson_amplitude_precalc.txt";
        int dummy =  system(helper.c_str()); }

    if(calc_bse ==5 || calc_bse == 6){

        //check if the cheby methode was used and if so if the file is already in chebys or not.

        //interpolation on exsisting data-set.
        mout<<"Starting interpolation for meson data."<<endl;
        interpolate_this_bse_data(output_folder_ps+which_meson+"_meson_amplitude",
                                  ps, previous_rad_points_for_interpolation,is_the_read_file_in_chebys );
        Psquared = bse.read_amplitude(output_folder_ps+which_meson+"_meson_amplitude.txt");

        //Furthermore also change the parameters behind the bse_amp object
        bse_index=actual_bse_index;
        bse.meson.setSi(actual_index_of_bse);



    } //Push in a extra statement below, if there is any need for interpolation.

    if(calc_bse != 2 && calc_bse !=1){
        bse.setAlready_deleted_quarks(true); //The quarks in the BSE are non-exsisting in this case.
        //This whole set-up is a bit out dated anyway. As the best way is to use an interpoaltion in the TFF code and calculate the bse external.
        //idea: Could set it up with an executable like the Fotran interpoaltion.
        }



    if(which_method=="vector_cheb" && calc_bse !=5){

        //I am working here.
         cin.get(); //Wait! not done..
//        cheby_expand_bse_amp( ps, n_pp, n_cheb, meson_amp)
    }
    //One last check and a warning message on screen if there is still a size miss match.
    if( (bse.meson.getRealSize() != (n_ll * n_lz * basis_meson))  &&
        (bse.meson.getRealSize() != (n_ll * n_cheb * basis_meson)) && calc_bse !=3){
        cout<<"WARNING - Meson: Interpolation needed? The data that was Read Inn was not the right size!"
              <<endl<<"Current Size= "<<bse.meson.getRealSize()<<tab<<endl<<"Did you want to interpolate. Choose case 5. "<<endl; cin.get(); }

//    //important: setting the mass of the boundstate in the phase space. - New change, let see if it works.
//    // Here cause it could be that you'll search the corresponding mass.
//    ps.setPsquared(meson_mass);


    //This is also outdated - I should delete it an see what happens. okay. let's uncomment it 30.04.19
//    //Here I want to pass the information of the first and the second quark.
//    //todo: Don't know if this is working jet!!! maybe cause I deleted the quarks after the first run?
//    vector<array<Cdoub,2>> quark1;
//    vector<array<Cdoub,2>> quark2;
//    //We can not read the quarks if we didnt calculate the bse here, as they are not safed (actually they are!?)
//    //If we are working in another alpha-frame we need to calcaute the quarks with the dependence on more variables way
//    // below and will use the amplitude as it is currently set-up thus in the folowing chi=amp
//    if(calc_bse!=3 && calculate_chi!=2){bse.get_me_content_of_quarks(quark1, quark2);} //this is kinda never used is it?



    //step 2c): If the TFF and the BSE grid arent the same it is nessercary to interpolate
//    SuperIndex<2> Si_meson({n_ll, n_lz});
//    ps_amplitude new_meson();


//    //stoped : DEBUGGING here, switiching the mass to be the scalar mass... Hmm..
//    Psquared = - 0.665*0.665;

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<


    //step 3: MOMENTA - Choose the total momentum for calculation.
    //important: Here I pick the new P² momentum. ////////////////////////////////////////////////////////////////////////////////
    //Setting it to be onshell and the mass of the meson, most of the times:
//    Cdoub Ps = -meson_mass * meson_mass;
//    Psquared = sqrt(Ps);


    //currently I am most of the time using case=2, when I do not actaully use Chi, as I am workign with the amp
    // more then the wavefunction.
    //WE ARE CURRNTLY NOT USING CHI ANYMORE; ONLY THE AMPLITUDE; THIS COULD BE TAKEN OUT AT SOME POINT?
    //step 2(b): Calculating Chi.
    //------------------------MESON WAVEFUNCTION CHI---------------------------------------------------------------
    mout << "calculation of Chi" << endl << "|...........................................|" << endl;
    ps_amplitude<3> chi(bse_index);
    chi.setPsquared(real(Psquared));
    vector<int> amp_flag;
    if (calculate_chi == 1) {

        bse.setPhaseSpacePsquared(Psquared);
        bse.calculate_chi(chi, amp_flag);

    } else if (calculate_chi == 0) {
        chi.read_amplitude(output_folder_ps, 1, 2);

    } else if (calculate_chi == 2) {
        chi = bse.meson; //important: Since we aren't working in the rest frame we wont use chi!! But Gamma (amp).
    } else if (calculate_chi == 3) {

    } else {
        cout << "Chi: option not defined" << endl;
        assert(false);
    }
    mout << "-------------------------------------" << endl;

    //When using Gernots data:
    if(using_gernots_meson && calc_bse ==5 ){

        for (int si_i = 0; si_i < chi.getSi().getNsuper_index(); ++si_i){
            array<int,3> exp= chi.getSi().are(si_i);

            Cdoub current_chi= chi.getAmp()[si_i]*1.0/(sqrt(6.0));
            double m= real(sqrt(-Psquared));

            if(exp[0]==0){chi.set(si_i,(-1.0)*current_chi);}
            if(exp[0]==1){chi.set(si_i, (-1.0)*current_chi*1.0/(m*m*m));}
            if(exp[0]==2){chi.set(si_i,(-1.0)*current_chi*1.0/(m));}
            if(exp[0]==3){chi.set(si_i, current_chi*1.0/(m*m));}
        }
    }



//    cin.get();



    ////////////////////////////////////// ----- important: Choose Qps value here  ------------------------ ///////////

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<

    //------------------------LOOP FOR DIFFERENT QPS VALUES-------------------------------------------------------

    //could change this such that the grid is actually choosen in the grid routine, only size of grid i chosen here.

    // Q' -   V1(r+, Q') - Qfs    =====    //-Q -   V2(r-, -Q) - Qis
    vector<double> Qps_values;
    int n_here=amount_of_q_grid_points;
//    int n_here=80;

    if(flag_using_outside_Qs_or_eta){

        Line Qpline(0.0,1.0, 22, "linear", "leg", false, false);
//        Line Qpline(1.0,100, n_here, "linear", "leg", false, false);
        Qps_values=Qpline.getGrid();

//        Line wline(-1.0,1.0, n_here, "linear", "leg", false, false);
//        Line etaline(1.0e-3,1.0, n_here, "linear", "leg", false, false);
//        Qps_values={0.01, 0.05, 0.1, 0.25, 0.5, 0.7, 1.0};

    }else{Qps_values={0.0};}

    if(append_to_older_calc > 0 ){
        //do not delete previous file.
    }else{
        //delte file that safes data if exsiting
        string system_output0 = "rm "+ output_folder_qphv + "All_formfactor.txt;" + "rm "+ output_folder_qphv + "All_denorm.txt;"
                                + "rm "+ output_folder_qphv + "All_formfactor_all.txt;" + "rm "+ output_folder_qphv + "All_formfactor_without_denorm.txt;";
        system(system_output0.c_str()); }


//----------------------loop----------------------------------------------start

//big loop over Qps-values - start
for (int i = 0; i < Qps_values.size(); ++i) {


    //step 3: MOMENTA - Choose the relative and total momentum for the triangle diagram.

    /////////////////////////////////////// important choose P, p, P.p  - Q², Q'²//////////////////////////////////////////////
    //Now that the mass for the decay is set.
    //Which implies choosing Q², Qp² and P²:
    VecCdoub Q, Qp;

    //important: Here I now choose depending on the choice for Q² and Q'²^which interpolation is used to calculate the QPHV's.
    // Q' -   V1(r+, Q') - Qfs    =====    //-Q -   V2(r-, -Q) - Qis
    // (In case of the asymmetric limit we used to pick Q'²=0.)
    //(1) "smallQ" = old interpolation - small value work, but not for all Q².
    //(2) "spacelike" = new interpolation - spacelike only and Q²>0.1. Small values do not work. Do not use for Q²=0.
    // V1 - Q'²      - V2 -Q²
    string which_interpolation_V1;
    string which_interpolation_V2;
// -> Now selected in creat_Qs_Q_grids();

    //Create the grid in the Q² and Q'² variable.
    create_Qs_Qps_grid_interpol_boundaries(Q, Qp, Psquared, alpha, which_interpolation_V1, which_interpolation_V2, ps,
                                           grid_sym_flag, Qps_values[i], n_here, which_meson);


    if(Q.size() !=0) {

    cout << "Vertex set_up:  V1= " << which_interpolation_V1 << "  , V2= " << which_interpolation_V2 << endl;

    //Print Q² and Q'² values to file
    string buffer = output_folder_qphv + "where_is_my_grid.txt";
    ofstream f(buffer.c_str());
    for (int j = 0; j < Q.size(); ++j) {

        f << real(Q[j]) << tab << imag(Q[j]) << tab << real(Qp[j]) << tab << imag(Qp[j]) <<tab << real(Psquared) << endl;
    }
    f.close();


    //Message on screen:
    //And now print out the p -vector and safe the values in the phasespace ps.
    int n_pvec = Q.size();
    ps.Q_Qp_P = vector<array<Cdoub, 3>>(); //resetting the vector for all the Q^2 and Q'^2 values.
    mout << "p values of this session: " << endl;
    for (int i_pvec = 0; i_pvec < Q.size(); ++i_pvec) {
        ps.Q_Qp_P.push_back({Q[i_pvec], Qp[i_pvec], Psquared});
        fv<Cdoub> p = get_p_vec(alpha, Q[i_pvec], Qp[i_pvec], Psquared);
        mout << "p=( " << p.content[0] << " , " << p.content[1] << " , " << p.content[2] << " , " << p.content[3]
                << tab << tab <<  " , Q² = "<< Q[i_pvec]<<tab<< " , Q'² = "<<Qp[i_pvec] <<tab <<"  , P²= "<<Psquared
             << endl;
    }
    mout << "|...........................................|" << endl;
    mout << "In this session: n_pvec= " << n_pvec << endl;


        //important: Debugging - writing dummy data:
//        dummy_write_S(ps, output_folder, dse, alpha);

    //Free the memory by using the destructor but keep meson_amp
    //todo: Also can not free the memory like this... what now?
//    delete bse;
    //comment: no, not like this. If even use the written functionbse.free_memory();

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<


    //step 4: QPHV - Write out the interpolation values for the quark photon vertex
    //Now that the mass for rounting is set -
    //Here: Also write out the momentum files for the vertex:
    //important: DEBUGGING HERE
        if(QPV_interpolation==0){}
        else{
            mout<<"Writing out the Grid for the vertex interpolation"<<endl;
            output_qphv_grid(output_folder_qphv, ps, 1, alpha);
            output_qphv_grid(output_folder_qphv, ps, 2, alpha);
        }



    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //step 5: QUARKS!!!
    //step 5: calculate/read the complex quark S(k3)  &  S(k1),S(k2) below b)
    mout << "Complex quark for triangle" << endl << "|...........................................|" << endl;
    time_t inbetween_s3s, inbetween_s3e;
    time(&inbetween_s3s);

    vector<array<Cdoub, 2>> quark3;
    vector<array<Cdoub,2>>  quark1;
    vector<array<Cdoub,2>>  quark2;

    //some string set-up when using the fortran interpolation - not needed if
//    string where_to_find_interpolation = gethomdir() + "scripts/interpolate_qpv/interpolators/" +
//                                           flag_for_running_multipl_codes + "/";


    string where_to_find_exe = gethomdir() + "scripts/interpolate_qpv/interpolators/Exe/";

    string where_to_find_interpolation_data = gethomdir() + "scripts/interpolate_qpv/interpolators/";

    if (calculate_S3_flag == 1) {
        //Calculate the Sk3 quark with the naive method.
        mout << "calculation of the complex quark S(k3)" << endl << "|...........................................|"
             << endl;
        quark3 = calculate_S3(ps, where_is_quark3, dse, alpha);
    } else if (calculate_S3_flag == 0) {
        //Read it in from a previous calculation.
//        quark3 = read_complex_Q(output_folder + "Sk3.txt");
//        quark3 = read_complex_Q_new(output_folder + "quark_grid_out_3.txt");
        quark3 = read_complex_Q_new(where_is_quark3);
    } else if (calculate_S3_flag == 3) {

        //////////////////////////////////////////////////////////////////////////////////////////////////
        //Using the external Fortran interpolation for obtaining results for Sk3.
        mout << "interpolation (via external Fortran code) of the complex quark S(k3)" << endl
             << "|...........................................|" << endl;

        //Use an external interpoaltion to obtain the quark.
        write_quark_grid_for_interpol(output_folder, ps, alpha, 3);

        //Copy the executable
        string helper = "cp " + where_to_find_exe + "quarkall.out "+ output_folder_standart;
        int dummy = system(helper.c_str());

        helper = "cd " + output_folder_standart+ "; ./quarkall.out "+where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_complex_1_parabola_edit.dat"+ " " +
                where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_info.dat" + " " +
                where_is_quark3 + " " + output_folder+"quark_grid_out_3.txt";
//        cout<<helper<<endl;
        dummy = system(helper.c_str());

        //ReadIn form the new interpolated file
        quark3 = read_complex_Q_new(output_folder + "quark_grid_out_3.txt");


        //old code:
/*        //Use an external interpoaltion to obtain the quark.
        write_quark_grid_for_interpol(output_folder, ps, alpha, 3);

        //Call the external Fortran function via the terminal
//        string helper = "cp " + output_folder + "quark_grid_3.txt " + where_to_find_interpolation + ";";
        string helper = "cp " + where_is_quark3 + " " + where_to_find_interpolation + ";";

        int dummy = system(helper.c_str());

        helper = "cd " + where_to_find_interpolation + "; ./Callq3_" + which_quark_interpolation_data +
                 ".sh; echo interpolated quark 3.";
        dummy = system(helper.c_str());

        helper = "cd " + where_to_find_interpolation + "; mv quark_grid_out_3.txt " + output_folder + " ;";
        dummy = system(helper.c_str());

        helper = "cd " + where_to_find_interpolation + "; rm quark_grid_3.txt;";
        dummy = system(helper.c_str());

        //ReadIn form the new interpolated file
        quark3 = read_complex_Q_new(output_folder + "quark_grid_out_3.txt");*/

    } else if (calculate_S3_flag == 4){
        // Do nothing...
    } else {
        cout << "S3: What about the quark3? " << endl;
        assert(false);
    }

    time(&inbetween_s3e);
    give_time(inbetween_s3s, inbetween_s3e, "complex quark S(k3) done");
    mout << endl << "|...........................................|" << endl;

    time_t inbetween_spms, inbetween_spme;
    time(&inbetween_spms);

    //step 5 b) : Calculating Spm, if not in the rest-frame alpha < 1.e+2 :
    //in case of asymmetric routing the quarks appearing in the diagram have to be calcuated once more.
    if ( calculate_Spm_flag ==1 ) {
        //My naive methode
        //Calculating the values with my naiive quark method.
        mout << "Using the naive method to calculate Spm." << endl;
        quark1 = calculate_Spm(ps, output_folder, dse, alpha, get_kp, "1");
        quark2 = calculate_Spm(ps, output_folder, dse, alpha, get_km, "2");
    } else if (calculate_Spm_flag==3) {
        //Fotran interpolation: S(k1) & S(k2)
        //Using the Fortran interpoaltion on gernots quark data.
        //The grid have already been written out above when starting the routine for Sk3.


        //Write out the grid-points for both Spm:
        write_quark_grid_for_interpol(output_folder, ps, alpha, 1);

        //Copy the executable
        string helper = "cp " + where_to_find_exe + "quarkall.out "+ output_folder_standart;
        int dummy = system(helper.c_str());

        //Call the executable for Sp
        helper = "cd " + output_folder_standart+ "; ./quarkall.out "+where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_complex_1_parabola_edit.dat"+ " " +
                 where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_info.dat" + " " +
                 where_is_quark12+"1.txt" + " " + output_folder+"quark_grid_out_1.txt";
        dummy = system(helper.c_str());

        //Call the executable for Sm
        helper = "cd " + output_folder_standart+ "; ./quarkall.out "+where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_complex_1_parabola_edit.dat"+ " " +
                 where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_info.dat" + " " +
                 where_is_quark12+"2.txt" + " " + output_folder+"quark_grid_out_2.txt";
        dummy = system(helper.c_str());

        //ReadIn from the interpolated file
        quark1 = read_complex_Q_new(output_folder + "quark_grid_out_1.txt");
        quark2 = read_complex_Q_new(output_folder + "quark_grid_out_2.txt");


        //old code:
/*
        //Use an external interpoaltion to obtain the quark.
        write_quark_grid_for_interpol(output_folder, ps, alpha, 1);

        mout << "Using Fortran interpolation to calculate Spm. On data set: "<<which_quark_interpolation_data << endl;

        //Call the external Fortran function via the terminal
        string helper = "cp " + output_folder + "quark_grid_1.txt " + where_to_find_interpolation + ";" +
                        "cp " + output_folder + "quark_grid_2.txt " + where_to_find_interpolation + ";";

        int dummy = system(helper.c_str());

        helper = "cd " + where_to_find_interpolation + "; ./Callqpm_" + which_quark_interpolation_data +
                 ".sh; echo interpolated quark 1 and 2.";
        dummy = system(helper.c_str());

        helper = "cd " + where_to_find_interpolation + "; mv quark_grid_out_1.txt " + output_folder + " ;" +
                 "cd " + where_to_find_interpolation + "; mv quark_grid_out_2.txt " + output_folder + " ;";
        dummy = system(helper.c_str());

        helper = "cd " + where_to_find_interpolation + "; rm quark_grid_1.txt; rm quark_grid_2.txt;";
        dummy = system(helper.c_str());

        //ReadIn from the interpolated file
        quark1 = read_complex_Q_new(output_folder + "quark_grid_out_1.txt");
        quark2 = read_complex_Q_new(output_folder + "quark_grid_out_2.txt");*/

    } else if (calculate_Spm_flag == 0) {
        //ReadIn from the interpolated file
        quark1 = read_complex_Q_new( where_is_quark12 + "1.txt");
        quark2 = read_complex_Q_new( where_is_quark12 + "2.txt");
    } else{cout<<"Nothing was done for quark S+, S-."<<endl;}
    time(&inbetween_spme);
    give_time(inbetween_spms, inbetween_spme, "complex quark S+ and S- done");



    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<


    //step---------------------------------------------------------------------------|||||||||||||||||||||||||||||||||||||||||||||<
    //step 6: Call the external Fortran interpolation of the quark-photon vertex.
    mout << "-------------------------------------" << endl;
    mout << "Starting the vertex calcaution/interpolation" << endl << "|...........................................|"
         << endl;


    time_t starting6, dasende6;
    time(&starting6);
    //If the interpolation outputfiles are already calculated and ready to use in your current folder: ==0.
    if (QPV_interpolation == 0) { mout << "QPV already calculated earlier. Done" << endl; }
    else if (QPV_interpolation == 1)
        //Gernots interpolation -
        //Calling Gernots Fortran program for the interpolation of the qphv vertex
        //comment: new: Need to make it possible to use different interpolations for the vertices - rewrite 22.01.19
        //b.out == V1 ; a.out == V2.
    {

        //Copy the executable
        string helper = "cp " + where_to_find_exe + "vertex.out "+ output_folder_standart;
        int dummy = system(helper.c_str());

        //Call the executable for V1 + time
        time(&inbetween_spms);
        cout<<"V1: "<<endl;
        helper = "cd " + output_folder_standart+ "; ./vertex.out " + where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_complex_1_parabola_edit.dat"+ " " +
                 where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_info.dat" + " " +
                 where_to_find_interpolation_data+"input_qphv_"+which_interpolation_V1+"/"+"__iBSE_QPV_complex_transverse.dat" + " " +
                 where_to_find_interpolation_data+"input_qphv_"+which_interpolation_V1+"/"+"par_ibse.dat" + " " +
                 output_folder_qphv +  "quarkphoton_grid_1.txt " + " " + output_folder_qphv +  "quarkphoton_grid_out_1.txt ";
//        cout<<helper<<endl;
        dummy = system(helper.c_str());
        time(&inbetween_spme);
        give_time(inbetween_spms, inbetween_spme, "V1- done");

        //Call the executable for V2 + time
        time(&inbetween_spms);
        cout<<"V2: "<<endl;
        helper = "cd " + output_folder_standart+ "; ./vertex.out " + where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_complex_1_parabola_edit.dat"+ " " +
                 where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_info.dat" + " " +
                 where_to_find_interpolation_data+"input_qphv_"+which_interpolation_V2+"/"+"__iBSE_QPV_complex_transverse.dat" + " " +
                 where_to_find_interpolation_data+"input_qphv_"+which_interpolation_V2+"/"+"par_ibse.dat" + " " +
                 output_folder_qphv +  "quarkphoton_grid_2.txt " + " " + output_folder_qphv +  "quarkphoton_grid_out_2.txt ";
        dummy = system(helper.c_str());
        time(&inbetween_spme);
        give_time(inbetween_spms, inbetween_spme, "V2- done");



//old code:

 /*       string helper = "cp " + output_folder_qphv + "quarkphoton_grid_1.txt " + where_to_find_interpolation +
                        "; cp " + output_folder_qphv + "quarkphoton_grid_2.txt " + where_to_find_interpolation +
                        " ;echo copied the input to interpolation. ; ";


        int dummy = system(helper.c_str());

//        helper="cd interpolate_qpv/interpol/; ./interpolateprase.sh ";
//        helper="cd "+gethomdir()+"scripts/interpolate_qpv/; ./a.out; echo first vertex done - V2(rm,Q); ./b.out; echo second vertex done - V1(rp,Qp);";
//        helper="cd "+gethomdir()+"scripts/interpolate_qpv/interpolator_"+ which_interpolation_V1 + "/;"+" ./Callb.sh";
        helper = "cd " + where_to_find_interpolation + "; ./Callb_" + which_interpolation_V1 + "_" +
                 which_quark_interpolation_data + ".sh";
        dummy = system(helper.c_str());

//        helper="cd "+gethomdir()+"scripts/interpolate_qpv/interpolator_"+ which_interpolation_V2 + "/;"+" ./Calla.sh";
        helper = "cd " + where_to_find_interpolation + "; ./Calla_" + which_interpolation_V2 + "_" +
                 which_quark_interpolation_data + ".sh";
        dummy = system(helper.c_str());
//        cout << "second Vertex done -V2" << endl;

        //comment: had problems with the shell script. Was complaining about previous direct call with ./a.out, thus change it with a file: Calla.sh.

        mout << "|...........................................|" << endl;

        helper = "cd " + where_to_find_interpolation + "; mv quarkphoton_grid_out_1.txt " + output_folder_qphv + " ;" +
                 "cd " + where_to_find_interpolation + "; mv quarkphoton_grid_out_2.txt " + output_folder_qphv + " ;" +
                 "echo interpolation done;";

        dummy = system(helper.c_str());

        helper = "rm " + where_to_find_interpolation + "quarkphoton_grid_1.txt;" +
                 "rm " + where_to_find_interpolation + "quarkphoton_grid_2.txt;";
//                "rm " + where_to_find_interpolation + "quarkphoton_grid_out_1.txt;" +
//                "rm " + where_to_find_interpolation + "quarkphoton_grid_out_2.txt;";

        dummy = system(helper.c_str());*/


        //--------------------------------------------------------------------------------------
//        //comment: If I want to compare I need to comment this inn.....it will change the name:
        //extra for comparison with Gernots interpolation:
/*        QPV_interpolation=2;
        helper = "cd "+output_folder_qphv+ "; cp quarkphoton_grid_out_1.txt G_quarkphoton_grid_out_1.txt; cp quarkphoton_grid_out_2.txt G_quarkphoton_grid_out_2.txt;" +
                                                   "rm quarkphoton_grid_out_1.txt; rm quarkphoton_grid_out_2.txt; ";
        dummy=system(helper.c_str());*/
        //--------------------------------------------------------------------------------------


    } else if (QPV_interpolation == 2) {
//    }if(QPV_interpolation==2){
        //interpolation with the naive method, using my BSE class. - MY CODE.

        //todo: important: Check on which varibales they depend on because of angle_project_out....
        VecCdoub ksquared;
        VecCdoub kP;
        VecDoub Psquared;

        //For now I am using the same grid points etc as for the bse here. Thus
        string dummy = "cp " + input_folder + "cons_bse.txt " + input_folder + "cons_qphv.txt;";
        system(dummy.c_str());

        //create the object that carries the meson qphv.
        BseParameter qphv_para(input_folder + "cons_qphv.txt");
        array<int, 3> qphv_bse_index;
        if (which_method == "matrix_cheb" || which_method == "vector_cheb") {
            qphv_bse_index = {12, qphv_para.n_pp, qphv_para.n_cheb};
        } else {
            qphv_bse_index = {12, qphv_para.n_pp, qphv_para.n_pz};
        }
        ps_amplitude<3> amp_qphv(qphv_bse_index);

//        qphv_MesonBse qphv_bse(amp_qphv, &dse, &dse, which_method , input_folder+"cons_qphv.txt", output_folder_qphv, false, what_to_calculate);


        //Precalculate ready to use qphv bses:
        //using the direct calculation of the vertex with my previous calculated qphv. qphv data must be in the folder
        //or calculated now.

        //loop for the 2 vertices . - calc 1 and 2.
        for (int which_vertex = 1; which_vertex < 3; ++which_vertex) {


            qphv_MesonBse qphv_bse(amp_qphv, &dse, &dse, which_method, input_folder + "cons_qphv.txt",
                                   output_folder_qphv, false, what_to_calculate);


            where_is_vertex_needed_read_inn(output_folder_qphv + "quarkphoton_grid_" + to_string(which_vertex) + ".txt",
                                            n_pvec, ksquared, kP, Psquared);

            if (!precacualtion_of_vertex_already_done) {
                //todo: Watch out - need to make sure that the method of the read in file is the same here!
                cout << "Starting precalculation of vertex" << endl;
                precalc_bses_for_qphv(Psquared, qphv_bse, output_folder_qphv, which_vertex);
            }

            //(1) Readin all the precalculated result. +
            //(2) Iterate qphv one more time at for all VALUES, to get our result
            //(3) Finally safe it into file, usable for the main formfactor.calc()
            //All included in following function():

            calculate_all_qphv(qphv_bse, output_folder_qphv, Psquared, ksquared, kP, "vector", which_vertex);

            //Resetting things in the bse amplitude.
            //Need to reset the flag for the quarks apparently
            //todo: Check where this gets change in course of the bse calc procedure, as it must be and think about changing it there. - no.
            qphv_bse.setWhat_to_calc_flag({1, 1, 0, 0, 0});
            qphv_bse.reset_inhomogenous();


        }


    } else { assert(false); }
    time(&dasende6);
    give_time(starting6, dasende6, "QPV");
    mout << endl << "|...........................................|" << endl;


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<

    //----------------------------- FINALE ----------------------------------
    //step---------------------------------------------------------------------------|||||||||||||||||||||||||||||||||||||||||||||<
    //step 7: Calculate the form factor - putting together all components - final calcaution
    mout << "-------------------------------------" << endl;
    mout << "Main calculation: Form factor" << endl << "|...........................................|" << endl;

    time_t starting7, dasende7;
    time(&starting7);

    VecCdoub TFF_vec;

    if (flag_calculate_tff) {

        formfactor TFF(ps, output_folder_qphv, alpha, which_meson);

//        TFF_vec = TFF.calc_formfactor(quark1, quark2, quark3, chi);
        //important: DEBUGGING - Watch out! usually not commented inn.
        //REMEBER: TO also change the trace function that is called inside the integral.

//        TFF_vec = TFF.debug_calc_formfactor(quark1, quark2, quark3, chi);
        TFF_vec = TFF.debug_calc_formfactor_lorenz(quark1, quark2, quark3, chi);

//        TFF_vec = TFF.calc_formfactor_lorenz(quark1, quark2, quark3, chi);
//        TFF.second_debug_calc_formfactor();

//        TFF.write(1.0, TFF_vec, "");

        //Debugging the easiest Form Factor: Analytic comparison:
//        TFF.debug_feynman_analytic();

        time(&dasende7);
        give_time(starting7, dasende7, "form factor");
        mout << "|...........................................|" << endl << endl;

        //WRITING THE TFF:
//    TFF.write((1.0/real(TFF_vec[0])), TFF_vec, "F[0]_");
//        TFF.write_normed(TFF_vec, "F[0]_");
        //If we do multiple runs we need to make sure that there is a remove statement at the beginning of this loop to delete the added file.

//        //important: DEBUGGING HERE
//        TFF.write_ad(1.0, TFF_vec, "All_");

        mout << "With the factor infront we obtain:  fixed fpi="<<decayconstant_fixed<<"  ;TTF= "
             << 4.0 * M_PI * M_PI * decayconstant_fixed * TFF_vec[0] <<
             tab << "calculated fpi="<<decayconstant<< "   ;TFF=" << 4.0 * M_PI * M_PI * decayconstant * TFF_vec[0] << endl;

    }
    cout << "|------------------------------------------------|" << endl;

}else{cout<<"No points in intervall, thus try next eta/Qs value.."<<endl;}
//----------------------loop----------------------------------------------end
//big loop over Qps-values
}
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<


#ifdef K_CALC_EACH_AMP_COMPONTENT


    mout<<"Now calculating the form factor once more for the individual components."<<endl;

    VecCdoub new_TFF_vec;

    for (int j = 0; j < chi.getSi().getMaxima_of_partial_indices()[0]; ++j) {


        //recalculate chi:
        vector<int> new_amp_flag = {j};
        bse.calculate_chi(chi, new_amp_flag);


        //and afterwards the whole form factor:
        new_TFF_vec = TFF.calc_formfactor(quark1, quark2, quark3, chi );
        TFF.write(1.0, new_TFF_vec, "amp_"+to_string(j)+"_");
        TFF.write((1.0/real(TFF_vec[0])), new_TFF_vec, "F[0]_amp_"+to_string(j)+"_");

    }



#endif


    time(&dasende); give_time(starting,dasende,"the whole main");

//    string system_output1 = "cp "+gethomdir()+"tff.out "+output_folder_standart;
//    system(system_output1.c_str());

    string system_output1 = "mv "+gethomdir()+"output_file_"+flag_for_running_multipl_codes+" "+ output_folder_standart + "/ ;";
    system(system_output1.c_str());


    return 0;



}
