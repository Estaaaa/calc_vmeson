//
// Created by esther on 12.02.18.
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>
#include <iostream>

using namespace std;



class mother{

public:
    mother(bool go){

        if(go){
            number = new double(3.0);
        }

    }

    double getNumber() const {
        return *number;
    }

protected:

    double* number = nullptr;

};


class daughter : public mother {

public:
    daughter(): mother(false){

        number = new double(10);
    }

};


int main (int argc, char * const argv[]) {

//    mother m(false);
//    cout<<"the number of mother is: "<<m.getNumber()<<endl;

    mother m2(true);
    cout<<"the number of mother is: "<<m2.getNumber()<<endl;

    daughter d;
    cout<<"the number of daughter is: "<<d.getNumber()<<endl;


    return 0;
}