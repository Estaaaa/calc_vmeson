//Gerneral includes
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>
#include <fenv.h>
#include <xmmintrin.h>

//Program related definitions

#define K_WRITEOUT_ANGPROJC
//#define K_WRITEOUT_NOPROJ

#include <extra_def.h>

//Program related inculdes
#include <typedefs.h>
#include <ps_amplitude.h>
#include <Routing/dependence_of_all_kernels.h>
#include <Meson/MesonBse.h>
#include <homdir.h>
#include <fv.h>
#include "QuarkDse.h"
//#include <formfactor.h>
#include <mystream.h>
#include <qphv_naive_method.h>


mstream mout(gethomdir()+"tff.out");




#define K_CALC_QPHV
#define K_TESTING_QPHV_ITER
//#define K_CALC_SCALAR

//#define K_CALC_EACH_AMP_COMPONTENT

/*------------------------------------START MAIN-----------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
int main (int argc, char * const argv[]) {

    //Command to seek for Naan in the program, debugger will stop when Naans appear and tell you where.
    //Comment: doesn't work on the Mac, maybe find alternative.
//    feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
//    feenableexcept(FE_INVALID | FE_OVERFLOW);
    _MM_SET_EXCEPTION_MASK(_MM_GET_EXCEPTION_MASK() & ~_MM_MASK_INVALID);
//    short fpflags = 0x1332; // Default FP flags, change this however you want.
//    asm("fnclex");
//    asm("fldcw _fpflags");

    //Setting up time and folder structure:
    time_t starting, dasende;
    time(&starting);
    // Convert now to tm struct for local timezone
    time_t now = time(0);
    tm* localtm = localtime(&now);
    cout << "This program was run @  " << asctime(localtm) << endl;

    string folder="133";

    string output_folder_standart =gethomdir()+"data/data"+folder+"/";


    ///-----------------------------------------------------------------------//
    //Is not needed with input file.
    //Setting up the parameters in here and writing them in a file.
    int n_rad_dse =150; int n_ang_dse=32;

    double IRcutsq_dse= 1e-4;
    double UVcutsq_dse = 1e+4;

    double renorm_point = 19.0; double mass_at_renorm = 0.0037;

    double IRcutsq= 1e-4;
    double UVcutsq = 1e+4;

    int n_ll = 50;
    int n_lz = 32;
    int n_ly = 38;

    int n_pp = n_ll;
    int n_zp = n_lz;
    int n_cheb =  4;

    int n_projector, n_basis;



    time(&dasende); give_time(starting,dasende,"the whole main");
    string system_output1 = "cp "+gethomdir()+"tff.out "+output_folder_standart;
    system(system_output1.c_str());


    return 0;



}
