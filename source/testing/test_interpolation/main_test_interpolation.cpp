//
// Created by paul on 16.08.17.
//

#include <typedefs.h>
#include <Vector_resizer.h>
#include <SuperIndex.h>
#include "BaseInterpolPE.h"
#include "Grids.h"
#include "const_bse.h"


void test_1d_splines();

void test_2d_splines();

int main(){

//    test_1d_splines();
    test_2d_splines();

//    SuperIndex<3> Super = SuperIndex<3>({2,5,10});
//    cout<<Super.is({0,0,1})<<tab<<Super.is({1,0,0})<<endl;
    //For the Superindex. It is enrolled such that the first index is the most inner in a loop.


    return 0;

    //creating a two Lines as a test object
    int N1=30; int N2=16;
    Line l1(1.e-4, 1.e+3, N1, "log", "leg", true, true);
    Line l2(-1.,             1.,             N2,                  "linear",   "leg",    true, true);

//    Line_Legendre Line;
//    Line.Get_w_interpolation( -1.0,  1.0,  N1, index_2 )

    array<Line*,2> grid_all = {&l1,&l2};

    SuperIndex<2> SI({N1,N2});


    auto lambda_test_xi = [&](int dim_i, int super_i){

        double value;
         value = (grid_all[dim_i]->getGrid()[SI.are(super_i)[dim_i]]);

        return value;

    };

    auto  lambda_test_wi = [&](int super_i){

        double result = 1.;
        for (int i = 0; i < 2; ++i) {
//            cout<<i<<tab<<SI.are(super_i)[i]<<tab<<grid_all[i]->getWeights_barycentric()[SI.are(super_i)[i]]<<endl;
            result *= grid_all[i]->getWeights_barycentric()[SI.are(super_i)[i]];
        }
        ///The weights in each direction get multiplied together!
        return result;
    };

    cout<<"Test: "<<lambda_test_xi(1,5)<<tab<<lambda_test_wi(5)<<tab<<lambda_test_xi(0,5)<<tab<<lambda_test_xi(0,0)<<endl;


    matCdoub fx; //using the function f(x,y) = sin(x)*y²;
    Vector_Resize(fx,2, SI.getNsuper_index());

    for (int i = 0; i < 2 ; ++i) {

        for (int j = 0; j < SI.getNsuper_index(); ++j) {

            fx[i][j] = 1.0/(grid_all[0]->getGrid()[SI.are(j)[0]])
                        *(grid_all[1]->getGrid()[SI.are(j)[1]]);

        }

    }


    //creating the interpolator
//
//    WaltersBarycentric<2,2 , Cdoub, double>
//            fun_fi( fx , SI.getNsuper_index(), lambda_test_xi, lambda_test_wi);
//
//
//    //result
//    VecCdoub result; result.resize(2);
//
//    fun_fi.getValue(result, {150, 1.0});
//    cout<<"This is the interpolated value: "; printVector(result);






//    return 0;
}

void test_2d_splines() {
//////////////// TEST 2D Spline class /////////////////////////
    const int vdim = 2; // vector dimension
    SplineInterpolator< 2, vdim, double, Cdoub> interpolator;

    /// f(x0, x1) = x0^2 + x1
    VecDoub x0 = { 1., 2., 3.};
    VecDoub x1 = { 1., 2., 3., 4.0};
    matDoub x_values = {x0, x1};
//    VecCdoub y_values = { 2., 6., 12., 2. , 6. , 12. ,2., 12., 6. }; // ! muessen sortiert sein in einem Vector nach normalem index roaling! vdim=1
//    VecCdoub y_values = {1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0 ,1.0,1.0,1.0,
//                         1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0 ,1.0,1.0,1.0}; // ! muessen sortiert sein in einem Vector nach normalem index roaling! vdim=2

    VecCdoub y_values(x0.size()*x1.size()*vdim);
    SuperIndex<3> SI_here = SuperIndex<3>({vdim, (int) x0.size(), (int) x1.size()});
    for (int i = 0; i < x0.size(); ++i) {
        for (int j = 0; j < x1.size(); ++j) {
            //for(int i=0; f_i)
            y_values[SI_here.is({0,i, j})] = (x0[i]*x0[i]+x1[j]);
            y_values[SI_here.is({1,i, j})] = (sin(x0[i])+x1[j]);
        }
    }

    interpolator.Update(x_values, y_values );

//    VecDoub x = {2.5,2.5};
//    VecCdoub y = {0.};
    array<double,2> x = {1.0,1.0};
    array<Cdoub,2> y; // = {0.};
    interpolator.getValue(y, x);

    printVector( y); // 20 if correct
    /// doesnt work

}

void test_1d_splines() {//////////////// TEST 1D Spline class /////////////////////////
    real_1d_array alglx;

    SplineInterpolator< 1, 1, double, double> interpolator;

    VecDoub x0 = { 1., 2., 3.};
    VecDoub y0 = { 1., 4., 9.};
    interpolator.Update(x0, y0 );

    VecDoub x = {6.};
    VecDoub y = {0.};
    interpolator.getValue(y, x);

    printVector( y); // 36 if correct
    //// works
}