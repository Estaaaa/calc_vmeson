//Gerneral includes
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>
#include <fenv.h>
#include <xmmintrin.h>

/////////////////////////////////////////////////////////////////////////////////////////


//Program related inculdes
#include <typedefs.h>
#include <ps_amplitude.h>
#include <Routing/dependence_of_all_kernels.h>
#include <Meson/MesonBse.h>
#include <homdir.h>
#include <fv.h>
#include "QuarkDse.h"
#include <formfactor.h>
#include <mystream.h>
#include <qphv_naive_method.h>
#include <tff_addons.h>

/////////////////////////////////////////////////////////////////////////////////////////

//inlcude the constant file:
#include "../../input_header/input_constants_bse.h"

//define a stream to safe the outout commands in a file:
mstream mout(gethomdir()+"output_file.out");

/////////////////////////////////////////////////////////////////////////////////////////
//define flags for which meson is calculated
//#define K_CALC_PS_SCALAR
//#define K_CALC_SCALAR
//#define K_CALC_SCALAR_MULTIPLE

//Comment: This program includes the scalar and pseudo-scalar and uncomments the part when I use my own vertex for since this isnt working so far.
/*------------------------------------START MAIN-----------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
int main (int argc, char * const argv[]) {

    //Command to seek for Naan in the program, debugger will stop when Naans appear and tell you where.
    //Comment: doesn't work on the Mac, maybe find alternative.
    _MM_SET_EXCEPTION_MASK(_MM_GET_EXCEPTION_MASK() & ~_MM_MASK_INVALID);


    //General Comments often forgotten:
    // For the qphv vertices:
    //Q' -   V1(r+, Q') - Qfs - Qp
    //-Q -   V2(r-, -Q) - Qis - Q



    //Preprogram set-ups: -  All falgs and gridpoint choices -

    //Setting up time and folder structure:
    time_t starting, dasende;
    time(&starting);
    // Convert now to tm struct for local timezone
    time_t now = time(0);
    tm* localtm = localtime(&now);
    mout << "This program was run @  " << asctime(localtm) << endl;



    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //All choices: flags points usw.

    //Chose which folder:
    string folder="84001";

    //Copying the input_constant file for later reference:
    string helper1 = "cp " + gethomdir() + "source/input_header/input_constants_bse.h "+gethomdir()+"/data/data"+folder+"/ ;";
    int dummy1 = system(helper1.c_str());

    //Pick the numerical variable, such as gridsizes etc pp:
    ///-----------------------------------------------------------------------//
    //Is not needed with input file.
    //Setting up the parameters in here and writing them in a file.
    //DSE
    int n_rad_dse =150; int n_ang_dse=64;

    double IRcutsq_dse= 1e-6;
    double UVcutsq_dse = 1e+6;

    //important: Watchout if I am using MTPV gluon or MT, they have a different - and PV cutoff to choose.
    //--------- MT gluon:    or   //-------- MTPV gluon:
    string which_gluon = "MT";
    string which_gluon_for_dse= "MTPV";

    //Main Program
    double Pv_cutoff =4e+4;
    //only used for the DSE part as there we use MTPV.

    double IRcutsq= IRcutsq_dse;
    double UVcutsq = UVcutsq_dse;

    int n_bse_ll= 50; // To read inn Gernots data file. This amount of rad point is needed.
    int n_bse_lz = 40;
    int n_bse_ly = 46;
    int n_bse_pp = n_bse_ll;
    int n_bse_pz = n_bse_lz;
    int n_cheb =  4;

    //chose set of MT parameter
    //Possible set currently: 28 , 30
#ifdef USE_MT_PARA_FOLDER_30
    int MT_para = 30;
#endif
#ifdef USE_MT_PARA_FOLDER_28
    int MT_para = 28;
#endif


    //FLAGS <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    //---------------- CALCULATION FLAGS ------------------------------------
    //---------> DSE
    /// The DSE is calculated or read_Inn. The parameter file has to match the read inn quark.
    // - false = ReadInn  , - true=calculate:
    bool calc_quark= true;

    //---------> BSE
    //Or read in amplitude and calculated wavefunction.
    //If the amplitude is read in the quarks S(k1) and S(k2) need to be calculated as well
    // - 1= calculate (find mass for ev=1)  ,
    // - 2= calculate for chosen value mpi (ev irrelevant):
    int calc_bse= 1;

    //Which method to calcuate the meson:
    //you can choose between: matrix, matrix_cheb, vector, vector_cheb as the iterative method.
//    string which_method_meson = "vector";
    string which_method_meson = "vector_cheb";



    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //Variable set-up at the beginning with choices above  - ignore

    //folders:
    string output_folder_standart =gethomdir()+"data/data"+folder+"/";
    string output_folder =gethomdir()+"data/data"+folder+"/output_0/";
    string output_folder_ps =gethomdir()+"data/data"+folder+"/output_ps/";
    string output_folder_ve =gethomdir()+"data/data"+folder+"/output_ve/";
    string output_folder_qphv =gethomdir()+"data/data"+folder+"/output_qphv/";
    string input_folder=gethomdir()+"data/data"+folder+"/input/";

/*
    //Readin Inn quarks: If quarks are read in where are they found?
    //(0) Fortran interpolated quarks   (1) my quarks  (2) quark form the bse- my other folder.
    string where_is_quark12,where_is_quark3;
//    if (read_quarks_where ==0 ){where_is_quark12 = output_folder + "quark_grid_out_"; where_is_quark3=output_folder+"quark_grid_out_3.txt";}
    if (read_quarks_where ==0 ){where_is_quark12 = output_folder + "quark_grid_"; where_is_quark3=output_folder+"quark_grid_3.txt";}
    else if(read_quarks_where==1){where_is_quark12=output_folder+"Spm_"; where_is_quark3=output_folder+"my_quark_grid_out_3.txt";}
    else if (read_quarks_where ==2 ){where_is_quark12 = output_folder_ps + "Cquark";}
*/



    //Gluon and MT parameter
    double solver_tolerance; double d_gluon_mt; double gluon_omega_sq;
    double renorm_point, mass_at_renorm;

    if(MT_para == 28){
        renorm_point = 19.0; mass_at_renorm = 0.0035748;
        solver_tolerance = 1e-8;  gluon_omega_sq = 0.16;  d_gluon_mt = pow(0.74,3.0)/(sqrt(gluon_omega_sq)); }
    if(MT_para == 30){
        renorm_point = 19.0; mass_at_renorm = 0.0035748;
        solver_tolerance = 1e-8;  gluon_omega_sq = 0.1296;  d_gluon_mt = pow(0.74,3.0)/(sqrt(gluon_omega_sq)); }



#ifdef K_NOT_USING_INPUT_FILE


    DseParameter p_dse(n_rad_dse, "leg_split" , "log", n_ang_dse, "leg", "linear",UVcutsq_dse, IRcutsq_dse,
                       renorm_point, mass_at_renorm,solver_tolerance ,
                       which_gluon_for_dse, d_gluon_mt, gluon_omega_sq, Pv_cutoff, input_folder );

//    BseParameter bse_para(n_bse_ll, n_bse_lz, n_bse_ly, n_bse_pp, n_bse_pz,
//                          n_cheb, basis_meson, basis_meson,
//                          "leg", "log", "leg", "linear", "leg", "linear",
//                          UVcutsq, IRcutsq, renorm_point, mass_at_renorm, which_gluon, input_folder, which_method_meson);



#endif



#ifdef K_USING_INPUT_FILE
    DseParameter p_dse(input_folder + "cons_dse.txt");
//    BseParameter bse_para(input_folder+"cons_bse.txt");
#endif


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<

    //Print to screen commands for overview:
    mout<<output_folder_standart<<endl;
    mout<<"In this session: "<<" n_ll="<<n_bse_ll<<" , n_lz="<<n_bse_lz<<" ,n_ly="<<n_bse_ly<<" ,n_pp="<<n_bse_pp<<" , n_zp="<<n_bse_pz<<" , n_cheb="<<n_cheb<<
        " , IRcut²="<<IRcutsq<<" , UVcut²="<<UVcutsq<<" , quark mass="<<mass_at_renorm<<endl;

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<

//########################################################################################################################################################################
//########################################################################################################################################################################
//########################################################################################################################################################################

    //########### PROGRAM BODY ################

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<


    //step 1: QUARK - calculate/read the real quark propagator

    //------------------------REAL QUARK------------------------------------------------------------------
    ///This is where the real quark is either calcualated or read inn.
    /// The  default folder is set to be in input/real_quark.txt.
    /// First the DSE is initalized with the parameters (cut-offs,..) those are read Inn @ cons_dse.txt
    /// Then it is calculated or read_Inn. The parameter file has to match the read inn quark.
    // - false = ReadInn  , - true=calculate:

    time_t time1, time2;
    time(&time1);

    QuarkDse dse((input_folder+"cons_dse.txt"));
    if(calc_quark){
        dse.calc_quark();
    }else{
        dse.Fill_QuarkDse(input_folder+"real_quark.txt");
    }
    dse.getQuark().write_quark(input_folder+"real_quark.txt");

    time(&time2); give_time(time1,time2,"real quark"); mout<<"-------------------------------------"<<endl;

    //-------------------REAl QUARK END-------------------------------------------------------------------


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<


    //step 2: MESON -calculate/read the meson amplitude Gamma(l,P)   - BSE -
    //------------------------MESON BSES------------------------------------------------------------------


#ifdef K_CALC_PS_SCALAR

    double meson_mass_ps = 0.137;
    string which_meson = "ps";
    Cdoub Psquared_ps = - meson_mass_ps*meson_mass_ps;
    int number_of_basis_elements =4;

 #ifdef K_NOT_USING_INPUT_FILE


    BseParameter bse_para(n_bse_ll, n_bse_lz, n_bse_ly, n_bse_pp, n_bse_pz,
                          n_cheb, number_of_basis_elements, number_of_basis_elements,
                          "leg", "log", "leg", "linear", "leg", "linear",
                          UVcutsq, IRcutsq, renorm_point, mass_at_renorm, which_gluon, input_folder, which_method_meson);

#endif

    array<int,3> bse_index_ps;
    if(which_method_meson == "matrix_cheb" || which_method_meson == "vector_cheb"){bse_index_ps= {number_of_basis_elements, bse_para.n_pp, bse_para.n_cheb};}else{bse_index_ps= {number_of_basis_elements, bse_para.n_pp, bse_para.n_pz};}
    ps_amplitude<3> amp_ps(bse_index_ps);

    cout<<"||    ----------------------------------------------     ||"<<endl<<"      ---------pseudo scalar meson----------------       "<<endl;
    ps_MesonBse bse_ps(amp_ps, &dse, &dse, which_method_meson , input_folder+"cons_bse.txt", output_folder_ps, false, {1,1,0,0,0}, "ps");


    if(calc_bse==1)
    {
        meson_mass_ps = bse_ps.find_the_bs_mass(meson_mass_ps, 0.005, 0.0001);
        bse_ps.norm_the_meson(); mout<<"--------------------> Meson is normed"<<endl;
        double calculated_decayconstant= bse_ps.get_decay_constant();

        bse_ps.write_out_amplitude(output_folder_ps+ which_method_meson +"_meson_amplitude.txt");
        Psquared_ps= - meson_mass_ps*meson_mass_ps;
    }else if (calc_bse==2){
        bse_ps.solve_and_update_pion(- meson_mass_ps *  meson_mass_ps);
        bse_ps.meson.setPsquared(- meson_mass_ps * meson_mass_ps);
        bse_ps.norm_the_meson(); mout<<"--------------------> Meson is normed"<<endl;
        double calculated_decayconstant= bse_ps.get_decay_constant();

        bse_ps.write_out_amplitude(output_folder_ps+ which_method_meson +"_meson_amplitude.txt");
        Psquared_ps= - meson_mass_ps*meson_mass_ps;



    }else{cout<<"Nothing chosen for meson calculation"<<endl;}

    bse_ps.free_memory();

    cout<<endl<<endl<<"____________________________________"<<endl<<endl;

#endif

    //------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------

#ifdef K_CALC_SCALAR
    //SCALAR:
    //Now I want to calcuate a scalar meson.
    cout<<"||    ----------------------------------------------     ||"<<endl<<"      ---------scalar meson----------------       "<<endl;

    string which_meson = "scalar";
    double meson_mass_s = 0.675;
    Cdoub Psquared_s = - meson_mass_s*meson_mass_s;
    int number_of_basis_elements =4;

#ifdef K_NOT_USING_INPUT_FILE


    BseParameter bse_para(n_bse_ll, n_bse_lz, n_bse_ly, n_bse_pp, n_bse_pz,
                          n_cheb, number_of_basis_elements, number_of_basis_elements,
                          "leg_split", "log", "leg", "linear", "leg", "linear",
                          UVcutsq, IRcutsq, renorm_point, mass_at_renorm, which_gluon, input_folder, which_method_meson);

#endif
#ifdef K_USING_INPUT_FILE
//    DseParameter p_dse(input_folder + "cons_dse.txt");
    BseParameter bse_para(input_folder+"cons_bse.txt");
#endif

    array<int,3> bse_index_s;
    if(which_method_meson == "matrix_cheb" || which_method_meson == "vector_cheb"){bse_index_s= {number_of_basis_elements, bse_para.n_pp, bse_para.n_cheb};}else{bse_index_s= {number_of_basis_elements, bse_para.n_pp, bse_para.n_pz};}
    ps_amplitude<3> amp_s(bse_index_s);

    ps_MesonBse bse_scalar(amp_s, &dse, &dse, which_method_meson , input_folder+"cons_bse.txt", output_folder_ps, false, {1,1,0,0,0}, "scalar");

    if(calc_bse==1)
    {
        //Finding the BS mass
        meson_mass_s = bse_scalar.find_the_bs_mass(meson_mass_s, 0.005, 0.0001);
        bse_scalar.norm_the_meson(); mout<<"--------------------> Meson is normed"<<endl;
        double calculated_decayconstant= bse_scalar.get_decay_constant();

        bse_scalar.write_out_amplitude(output_folder_ps+ which_meson +"_meson_amplitude.txt");
        Psquared_s= - meson_mass_s*meson_mass_s;
    }else if (calc_bse==2){
        //Solving for the eigenvalue:
        bse_scalar.solve_and_update_pion(- meson_mass_s *  meson_mass_s);
        bse_scalar.meson.setPsquared(- meson_mass_s * meson_mass_s);
        bse_scalar.norm_the_meson(); mout<<"--------------------> Meson is normed"<<endl;
        double calculated_decayconstant= bse_scalar.get_decay_constant();

        bse_scalar.write_out_amplitude(output_folder_ps+ which_method_meson +"_meson_amplitude.txt");
        Psquared_s= - meson_mass_s*meson_mass_s;



    }else{cout<<"Nothing chosen for meson calculation"<<endl;}

    bse_scalar.free_memory();

    cout<<endl<<endl<<"____________________________________"<<endl<<endl;
#endif


#ifdef K_CALC_MULTIPLE


#ifdef K_CALC_MULTIPLE_SCALAR
    string which_meson = "scalar";
    double meson_mass_s = 0.675;
    Cdoub Psquared_s = - meson_mass_s*meson_mass_s;
    int number_of_basis_elements =4;
#endif
#ifdef K_CALC_MULTIPLE_PS
    string which_meson = "ps";
    double meson_mass_s = 0.135;
    Cdoub Psquared_s = - meson_mass_s*meson_mass_s;
    int number_of_basis_elements =4;
#endif


    int amount_of_input_files=8;
    string system_output0 = "rm "+  input_folder + "runs/masses.txt";
    system(system_output0.c_str());
    ofstream write;

for (int j = 1; j <= amount_of_input_files; ++j) {

    vector<VecCdoub> result_matrix;

#ifdef K_NOT_USING_INPUT_FILE


    BseParameter bse_para(n_bse_ll, n_bse_lz, n_bse_ly, n_bse_pp, n_bse_pz,
                          n_cheb, number_of_basis_elements, number_of_basis_elements,
                          "leg_split", "log", "leg", "linear", "leg", "linear",
                          UVcutsq, IRcutsq, renorm_point, mass_at_renorm, which_gluon, input_folder, which_method_meson);

#endif
#ifdef K_USING_INPUT_FILE
    //    DseParameter p_dse(input_folder + "cons_dse.txt");
    BseParameter bse_para(input_folder+"runs/cons_bse_"+ to_string(j)+".txt");
#endif

    cout<<"||    ----------------------------------------------     ||"<<endl<<"      ---------scalar meson----------------       "<<endl;
    mout<<"In this run we are using:     "<<bse_para.which_method<<"    to solve the meson BSE."<<endl;

    array<int,3> bse_index_s;
    if(bse_para.which_method == "matrix_cheb" || bse_para.which_method == "vector_cheb"){bse_index_s= {number_of_basis_elements, bse_para.n_pp, bse_para.n_cheb};}
    else{bse_index_s= {number_of_basis_elements, bse_para.n_pp, bse_para.n_pz};}
    ps_amplitude<3> amp_s(bse_index_s);
#ifdef K_CALC_MULTIPLE_PS
    ps_MesonBse bse_scalar(amp_s, &dse, &dse, bse_para.which_method , input_folder+"runs/cons_bse_"+ to_string(j)+".txt" , output_folder_ps, false, {1,1,0,0,0}, "ps");
#endif
#ifdef K_CALC_MULTIPLE_SCALAR
    ps_MesonBse bse_scalar(amp_s, &dse, &dse, bse_para.which_method , input_folder+"runs/cons_bse_"+ to_string(j)+".txt" , output_folder_ps, false, {1,1,0,0,0}, "scalar");
#endif

    if(calc_bse==1)
    {
        //Finding the BS mass
        meson_mass_s = bse_scalar.find_the_bs_mass(meson_mass_s, 0.005, 0.0001);
        bse_scalar.norm_the_meson(); mout<<"--------------------> Meson is normed"<<endl;
        double calculated_decayconstant= bse_scalar.get_decay_constant();

        bse_scalar.write_out_amplitude(output_folder_ps+ which_meson +"_meson_amplitude_"+to_string(j)+".txt");
//        Psquared_s= - meson_mass_s*meson_mass_s;

        write.open( input_folder + "runs/masses.txt", fstream::app);
        write << j<< tab << meson_mass_s<<tab<<calculated_decayconstant<<endl;
        write.close();
    }else if (calc_bse==2){
        //Solving for the eigenvalue:
        bse_scalar.solve_and_update_pion(- meson_mass_s *  meson_mass_s);
        bse_scalar.meson.setPsquared(- meson_mass_s * meson_mass_s);
        bse_scalar.norm_the_meson(); mout<<"--------------------> Meson is normed"<<endl;
        double calculated_decayconstant= bse_scalar.get_decay_constant();

        bse_scalar.write_out_amplitude(output_folder_ps+ which_method_meson +"_meson_amplitude.txt");
        Psquared_s= - meson_mass_s*meson_mass_s;



    }else{cout<<"Nothing chosen for meson calculation"<<endl; }

/*    VecCdoub grid_vector;
    int ang_points;
    if(bse_para.which_method=="vector"){ang_points=bse_para.n_lz;}
    if(bse_para.which_method=="vector_cheb"){ang_points=bse_para.n_cheb;}
    for (int k = 0; k < ang_points; ++k) {
        for (int l = 0; l < bse_para.n_ll; ++l) {
            grid_vector.push_back(bse_scalar.getPs().ll.getGrid()[l]);
        }
    }
    result_matrix.push_back(grid_vector);
    result_matrix.push_back(bse_scalar.meson.getAmp());*/

    bse_scalar.free_memory();
}

    cout<<endl<<endl<<"____________________________________"<<endl<<endl;

#endif
    //------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------

#ifdef K_CALC_VECTOR
    //Now we can pick different Mesons, such as a vector.
    cout<<"||    ----------------------------------------------     ||"<<endl<<"      ---------vector meson----------------       "<<endl;

    array<int,3> bse_index_ve;

    vector_MesonBse bse_ve(amp_ve, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_ve, false, what_to_calculate, "vector");
//    bse_ve.solve_and_update_pion(- 0.754724331887*0.754724331887);
    bse_ve.find_the_bs_mass(0.7, 0.05, 0.001);
    bse_ve.write_out_amplitude(output_folder_ve+"vector_meson_amplitude.txt");
    bse_ve.norm_the_meson();
    bse_ve.write_out_amplitude(output_folder_ve+"normed_vector_meson.txt");
//    bse_ve.write_out_amplitude(output_folder_ve+"vector_meson_normed_withoutcheb.txt");
//    bse_ve.~vector_MesonBse();

#endif

    //------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------


#ifdef K_CALC_AXIAL
    //AXIAL:
    //Now I want to calcuate a axial vector meson.
    cout<<"||    ----------------------------------------------     ||"<<endl<<"      ---------axial meson----------------       "<<endl;
    vector_MesonBse bse_axial(amp_ve, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_ve, false, what_to_calculate, "axial");
//    bse_axial.solve_and_update_pion(- 0.92964*0.92964);
    bse_axial.find_the_bs_mass(0.8, 0.1, 0.0001);  //found mass= 0.929642311329 for (48, 24, ,10) // worng cause only included 4 basis elements
    bse_axial.write_out_amplitude(output_folder_ve+"axial_meson.txt");
    bse_axial.norm_the_meson();
    bse_axial.write_out_amplitude(output_folder_ve+"normed_axial_meson.txt");

    cout<<endl<<endl<<"____________________________________"<<endl<<endl;

#endif


    //------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------
    //-----------------------------QPHV VERTEX--------------------------------------------------------
    //Seperated or in one go with all the tensor structures.

#ifdef K_CALC_QPHV_SEPERATED

    time_t ib1, ib2;
    time(&ib1);

    cout<<endl<<"----------------------Calculating scalar qphv--------------------------------------------"<<endl;

    array<int,3> bse_index_qphv;

    //Or a scalar with vector quantum numbers
    ps_MesonBse bse_qphv_ps(amp_ps, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_ps, false, what_to_calculate, "qphv");
    bse_qphv_ps.calc_for_different_Psq({0.5},"longi_Qphv");
//    bse_qphv_ps.write_out_amplitude(output_folder_ps+"amp_"+which_method+".txt");

    cout<<endl<<endl<<"____________________________________"<<endl<<endl;


//    ------------------------------------------------------------------------------------------------
//    ------------------------------------------------------------------------------------------------

    cout<<endl<<"----------------------Calculating vector qphv--------------------------------------------"<<endl;


    //Or the vector part of the qphv
    vector_MesonBse bse_qphv_ve(amp_ve, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_ve, false, what_to_calculate, "qphv");
////    bse_qphv_ve.calc_for_different_Psq((IRcutsq), (UVcutsq), 5);
    bse_qphv_ve.calc_for_different_Psq({0.5},"trans_Qphv");

    //Now we can translate the dressing function of the seperated transverse part for the qphv to the transverse
    // components when calcuated directly with all 12 basis elements
#ifdef K_WRITEOUT_ANGPROJC
    //comment: check transvere_4 and this worked. I am not so sure about no2 anymore.
    translate_qphv_transverse_4( bse_qphv_ve, bse_qphv_ps); // (1) translate (2) project
//    translate_qphv_transverse_2( bse_qphv_ve, bse_qphv_ps); // (1) project  (2)translate
#endif

#ifdef K_WRITEOUT_NOPROJ
    translate_qphv_transverse_3( bse_qphv_ve, bse_qphv_ps);
#endif


    bse_qphv_ve.write_out_amplitude(output_folder_ve+"transverse_translated_amplitude_between.txt"); cout<<endl;

    bse_qphv_ps.free_memory();
    bse_qphv_ve.free_memory();

    time(&ib2); give_time(ib1,ib2,"seperated quark-photon vertex");cout<<endl;

#endif

//    //------------------------------------------------------------------------------------------------


#ifdef K_CALC_QPHV

    time_t ib3, ib4;
    time(&ib3);

    cout<<endl<<"----------------------Calculating full qphv--------------------------------------------"<<endl;
    //Normal Qphv calculation for one choosen value.

    //Or the whole quark photon vertex
    qphv_MesonBse bse_qphv(amp_qphv, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_qphv, false, what_to_calculate);
    bse_qphv.calc_for_different_Psq({0.5},"Qphv");
    bse_qphv.write_out_amplitude(output_folder_qphv+"Qphv.txt", false);

    bse_qphv.free_memory();
//    bse_qphv.free_more_memory();

    cout<<"------------------------------------------------------------------------------"<<endl;
    cout<<"------------------------------------------------------------------------------"<<endl;


#ifdef K_TESTING_QPHV_ITER

    cout<<"Testing the Qphv naive interpolation:"<<endl;
    //testing the new set-up for the naive qphv.
//    qphv_MesonBse bse_qphv_3(amp_qphv, &dse, &dse, which_method , input_folder+"cons_bse.txt", output_folder_qphv, false, what_to_calculate);

    //Now we read Inn the quark-photon vertex grid data from a file an pre calcaute all needed qphv bses.
    VecCdoub ksquared; VecCdoub kP; VecDoub Psquared;
/*    where_is_vertex_needed_read_inn(output_folder_qphv+"quarkphoton_grid_1.txt", 20, ksquared, kP, Psquared);
    //And precalculate the corresponding qphv bses at all Q² values.
    precalc_bses_for_qphv(Psquared,bse_qphv_3,output_folder_qphv,1);
    bse_qphv_3.free_memory();*/

    //Now testing the iterpolation using one of the precalcuated and read amplitude Amp0. Calcualting the function on real values.
    qphv_MesonBse bse_qphv_2(amp_qphv, &dse, &dse, which_method, input_folder+"cons_bse.txt", output_folder_qphv, false, what_to_calculate);
/*    bse_qphv_2.init_via_file(output_folder_qphv+"Amp0/",bse_qphv.getPhaseSpacePsquared(),1);
 *
    string methode_for_the_iteration="vector";
    testing_iteration_qphv(bse_qphv_2, output_folder_qphv, methode_for_the_iteration);

    bse_qphv_2.free_memory();*/
    //comment: add a line where one copies the cons_bse into the output folder and calls it qphv_cons, ready for further calcs


    //or alternativly exactly what should be in form factor main:
    int n_pvec=20; int which_vertex=1;
    where_is_vertex_needed_read_inn(output_folder_qphv+"quarkphoton_grid_1.txt", n_pvec , ksquared, kP, Psquared);

    bool precacualtion_of_vertex_already_done=true;
    if(!precacualtion_of_vertex_already_done){
        precalc_bses_for_qphv(Psquared,bse_qphv_2,output_folder_qphv,which_vertex);  }else{cout<<"precalculations are already done."<<endl;}

    //(1) Readin all the precalculated result. +
    //(2) Iterate qphv one more time at for all VALUES, to get our result
    //(3) Finally safe it into file, usable for the main formfactor.calc()
    //All included in following function():

    calculate_all_qphv(bse_qphv_2, output_folder_qphv, Psquared, ksquared, kP,  "vector", which_vertex);


#endif

    time(&ib4); give_time(ib3,ib4,"full quark photon vertex.");


#endif




    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<




    time(&dasende); give_time(starting,dasende,"the whole main");

//    string system_output1 = "cp "+gethomdir()+"tff.out "+output_folder_standart;
//    system(system_output1.c_str());

//    string system_output1 = "mv "+gethomdir()+"output_file_"+flag_for_running_multipl_codes+" "+ output_folder_standart + "/ ;";
//    system(system_output1.c_str());


    return 0;



}
