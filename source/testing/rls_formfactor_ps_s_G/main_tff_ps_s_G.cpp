//Gerneral includes
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>
#include <fenv.h>
#include <xmmintrin.h>

/////////////////////////////////////////////////////////////////////////////////////////


//Program related inculdes
#include <typedefs.h>
#include <ps_amplitude.h>
#include <Routing/dependence_of_all_kernels.h>
#include <Meson/MesonBse.h>
#include <homdir.h>
#include <fv.h>
#include "QuarkDse.h"
#include <formfactor.h>
#include <mystream.h>
#include <qphv_naive_method.h>
#include <tff_addons.h>

/////////////////////////////////////////////////////////////////////////////////////////

//inlcude the constant file:
#include "../../input_header/input_constants_tff.h"

//define a stream to safe the outout commands in a file:
mstream mout(gethomdir()+"output_file.out");

//Comment: This program includes the scalar and pseudo-scalar and uncomments the part when I use my own vertex for since this isnt working so far.
/*------------------------------------START MAIN-----------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------*/
int main (int argc, char * const argv[]) {

    //Command to seek for Naan in the program, debugger will stop when Naans appear and tell you where.
    //Comment: doesn't work on the Mac, maybe find alternative.
    _MM_SET_EXCEPTION_MASK(_MM_GET_EXCEPTION_MASK() & ~_MM_MASK_INVALID);


    //General Comments often forgotten:
    // For the qphv vertices:
    //Q' -   V1(r+, Q') - Qfs - Qp
    //-Q -   V2(r-, -Q) - Qis - Q



    //Preprogram set-ups: -  All falgs and gridpoint choices -

    //Setting up time and folder structure:
    time_t starting, dasende;
    time(&starting);
    // Convert now to tm struct for local timezone
    time_t now = time(0);
    tm* localtm = localtime(&now);
    mout << "This program was run @  " << asctime(localtm) << endl;


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //All choices: flags points usw.

    //Chose which folder:
    string folder="85002";

    //Copying the input_constant file for later reference:
    string helper1 = "cp " + gethomdir() + "source/input_header/input_constants_tff.h "+gethomdir()+"/data/data"+folder+"/ ;";
    int dummy1 = system(helper1.c_str());

    //Pick the numerical variable, such as gridsizes etc pp:
    ///-----------------------------------------------------------------------//
    //Is not needed with input file.
    //Setting up the parameters in here and writing them in a file.
    //DSE
    int n_rad_dse =150; int n_ang_dse=64;

    double IRcutsq_dse= 1e-6;
    double UVcutsq_dse = 1e+6;

    //important: Watchout if I am using MTPV gluon or MT, they have a different - and PV cutoff to choose.
    //--------- MT gluon:    or   //-------- MTPV gluon:
    string which_gluon = "MT";
    string which_gluon_for_dse= "MTPV";
    double alpha=1.0e+20; //set to the rest-frame by default.

    //Main Program
    double Pv_cutoff =4e+4;
    //only used for the DSE part as there we use MTPV.

    double IRcutsq= 1e+4;
    double UVcutsq = 1e-4;

    int n_bse_ll= 48; // To read inn Gernots data file. This amount of rad point is needed.
    int n_bse_lz = 32;
    int n_bse_ly = 36;

    int n_ll = 40;
    int n_lz = 24;
    int n_ly = 28;

    int n_pp = n_ll;
    int n_zp = n_lz;
    int n_cheb =  4;

    //chose set of MT parameter
    //Possible set currently: 28 , 30
#ifdef USE_MT_PARA_FOLDER_28
    int MT_para = 28;
#endif
#ifdef USE_MT_PARA_FOLDER_30
    int MT_para = 30;
#endif


    //FLAGS <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    //---------------- CALCULATION FLAGS ------------------------------------
    //---------> DSE
    /// The DSE is calculated or read_Inn. The parameter file has to match the read inn quark.
    // - false = ReadInn  , - true=calculate:
    bool calc_quark= false;

    //---------> BSE
    //Or read in amplitude and calculated wavefunction.
    //If the amplitude is read in the quarks S(k1) and S(k2) need to be calculated as well
    // - 0 = ReadInn  ,
    // - 1= calculate (find mass for ev=1)  ,
    // - 2= calculate for chosen value mpi (ev irrelevant):
    // - 3= skip for now.
    // - 5= interpolate on an exsisting data-set.
    // - 6= combine 1 + 5  (The data set will be called ..._precalc.txt afterwards)
    int calc_bse= 6;
    //if calc_bse=5: how many rad points does previous file have. -> Later this should be automated
    int previous_rad_points_for_meson_interpolation = n_bse_ll;
    bool is_the_meson_read_file_in_chebys = true;
    bool using_gernots_meson=true;

    //Which method to calcuate the meson:
    //you can choose between: matrix, matrix_cheb, vector, vector_cheb as the iterative method.
//    string which_method_meson = "vector";
    string which_method_meson = "vector_cheb";

    //---------> S3, quark at k3: S(k3):
    // 0 - ReadIn
    // 1 - Calculate
    // 3 -interpolate with external Fortran function
    // 4 - skip for debbuging purposes
    int calculate_S3_flag=1;
    int calculate_Spm_flag=1;
    //Setting the flag for Reading in the quark Sp, Sm
    //(0) Fortran interpolated quarks   (1) my quarks  (2) quark form the bse- my other folder.
    int read_quarks_where=1;
    //If 0 : For S(k1) and S(k2) we need to specify which file is read inn. Specify below.

    //---------> quark-photon vertex QPHV:
    // 1  - FORTRAN: redo the calculation/interpolation using Gernots Fortran routine
    // 2  - MY: my vertex interpolation - naive method.
    // 0  - NOTHING: if already done! - just do nothing, as it will readInn the correspodning data later.
    int QPV_interpolation=1;
#ifdef USE_MY_QPHV_VERTEX
    //When using my own vertex - case 2:
    bool precacualtion_of_vertex_already_done=true; //this flag is only relevant in case =2 above, when using my naive method.
#endif

    //TFF: form factor - inerpolation.
    // true - calc, false - do nothing
    bool flag_calculate_tff=true;


    /////////////////////////////////////// important MESON MASS //////////////////////////////////////////////
    //either the mass is directly set to a value, or a range is used to get the right mass corresponding to EV 1.0
    //Which meson - on this case it could be scalar or ps (pesudo-scalar)
//    string which_meson="ps";
//    double meson_mass =  0.13497;
    string which_meson="scalar";
    double meson_mass = 0.675;
    Cdoub Psquared = - meson_mass*meson_mass;
    int amount_of_Q_grid_points = 60;
    bool fix_the_meson_mass =false;
    Cdoub Psquared_fixed = - meson_mass*meson_mass;



    //some random constant that are still in here: maybe delete at some point
    double decayconstant_fixed=0.092;

    //important!!!!! DO NOT CHANGE HERE!!!! for scalar and pseudoscalar
    int basis_meson = 4;

    //additional new falg to choose if symmetric or antisymmetric.
    //0: on-shell point, 1: symmetrisch, 2: anti-sym.
    // 3 : new - Grid in Q² and Q'².
    // 5 : Richard set-up with omega for comparison.
    // 6: One point. Atm eta^+ = 1 GeV^2.
    //7: symmetric limit set up with the omega and eta_+ paramters.
    //7: symmetric limit set up with the omega and eta_+ paramters.
    //(in princple this can get many new cases when further values are need. For testing purposes this cases are avialable)
    int grid_sym_flag=7;
    // (0) deletes file and doesnt append, (1) appends,  (2) does  - no deleting, but also no appending.
    int append_to_older_calc= 0;

    //either interpolating on Richards or Gernots data:
    // "R" - Richard - folder 30 MT
    // "G" - Gernot - folder 30 MT
    // "old" - folder 28 M paramter quark propagator
    string which_quark_interpolation_data;  //which_vertex_interpolation_data;
    if(MT_para == 30){
    which_quark_interpolation_data="G"; }
    if(MT_para == 28){
        which_quark_interpolation_data="old"; }






    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //Variable set-up at the beginning with choices above  - ignore

    //folders:
    string output_folder_standart =gethomdir()+"data/data"+folder+"/";
    string output_folder =gethomdir()+"data/data"+folder+"/output_0/";
    string output_folder_ps =gethomdir()+"data/data"+folder+"/output_ps/";
    string output_folder_ve =gethomdir()+"data/data"+folder+"/output_ve/";
    string output_folder_qphv =gethomdir()+"data/data"+folder+"/output_qphv/";
    string input_folder=gethomdir()+"data/data"+folder+"/input/";

    //Readin Inn quarks: If quarks are read in where are they found?
    //(0) Fortran interpolated quarks   (1) my quarks  (2) quark form the bse- my other folder.
    string where_is_quark12,where_is_quark3;
//    if (read_quarks_where ==0 ){where_is_quark12 = output_folder + "quark_grid_out_"; where_is_quark3=output_folder+"quark_grid_out_3.txt";}
    if (read_quarks_where ==0 ){where_is_quark12 = output_folder + "quark_grid_"; where_is_quark3=output_folder+"quark_grid_3.txt";}
    else if(read_quarks_where==1){where_is_quark12=output_folder+"Spm_"; where_is_quark3=output_folder+"my_quark_grid_out_3.txt";}
    else if (read_quarks_where ==2 ){where_is_quark12 = output_folder_ps + "Cquark";}



    //Gluon and MT parameter
    double solver_tolerance; double d_gluon_mt; double gluon_omega_sq;
    double renorm_point, mass_at_renorm;

    if(MT_para == 28){
        renorm_point = 19.0; mass_at_renorm = 0.0035748;
        solver_tolerance = 1e-8;  gluon_omega_sq = 0.16;  d_gluon_mt = pow(0.74,3.0)/(sqrt(gluon_omega_sq)); }
    if(MT_para == 30){
        renorm_point = 19.0; mass_at_renorm = 0.0035748;
        solver_tolerance = 1e-8;  gluon_omega_sq = 0.1296;  d_gluon_mt = pow(0.74,3.0)/(sqrt(gluon_omega_sq)); }



#ifdef K_NOT_USING_INPUT_FILE


    DseParameter p_dse(n_rad_dse, "leg_split" , "log", n_ang_dse, "leg", "linear",UVcutsq_dse, IRcutsq_dse,
                       renorm_point, mass_at_renorm,solver_tolerance ,
                       which_gluon_for_dse, d_gluon_mt, gluon_omega_sq, Pv_cutoff, input_folder );


    TFFParameter TFF_para(n_ll, n_lz, n_ly, n_cheb,
                          "leg", "log", "leg", "linear", "leg", "linear",
//                          "leg_split", "log", "leg", "linear", "leg", "linear",
//                          "leg", "log", "sinh_tanh", "linear", "sinh_tanh", "linear",
                          UVcutsq, IRcutsq, renorm_point, mass_at_renorm, which_gluon, input_folder);


    BseParameter bse_para(n_bse_ll, n_bse_lz, n_bse_ly, n_bse_ll, n_bse_lz,
                          TFF_para.n_cheb, basis_meson, basis_meson,
                          "leg", "log", "leg", "linear", "leg", "linear",
                          UVcutsq, IRcutsq, renorm_point, mass_at_renorm, which_gluon, input_folder, which_method_meson);


    write_general_run_parameter(input_folder+"cons_run.txt", folder, calc_quark ,calc_bse ,using_gernots_meson , is_the_meson_read_file_in_chebys , previous_rad_points_for_meson_interpolation ,
                                which_method_meson ,calculate_S3_flag , calculate_Spm_flag , read_quarks_where , QPV_interpolation ,flag_calculate_tff ,  K_ALPHA ,
                                which_meson ,meson_mass , amount_of_Q_grid_points ,grid_sym_flag, append_to_older_calc );

#endif



#ifdef K_USING_INPUT_FILE

    DseParameter p_dse(input_folder + "cons_dse.txt");
    BseParameter bse_para(input_folder+"cons_bse.txt");

    read_general_run_parameter(input_folder+"cons_run.txt", folder, calc_quark ,calc_bse ,using_gernots_meson , is_the_meson_read_file_in_chebys , previous_rad_points_for_meson_interpolation ,
                               which_method_meson ,calculate_S3_flag , calculate_Spm_flag , read_quarks_where , QPV_interpolation ,flag_calculate_tff ,  alpha ,
                               which_meson ,meson_mass , amount_of_Q_grid_points ,grid_sym_flag, append_to_older_calc ); cout<<"alpha"<<tab<<alpha<<endl;

    which_method_meson=bse_para.which_method;

    TFFParameter TFF_para(input_folder+"cons_tff.txt");


    //folders:
     output_folder_standart =gethomdir()+"data/data"+folder+"/";
     output_folder =gethomdir()+"data/data"+folder+"/output_0/";
     output_folder_ps =gethomdir()+"data/data"+folder+"/output_ps/";
     output_folder_ve =gethomdir()+"data/data"+folder+"/output_ve/";
     output_folder_qphv =gethomdir()+"data/data"+folder+"/output_qphv/";
     input_folder=gethomdir()+"data/data"+folder+"/input/";

    //Readin Inn quarks: If quarks are read in where are they found?
    if (read_quarks_where ==0 ){where_is_quark12 = output_folder + "quark_grid_"; where_is_quark3=output_folder+"quark_grid_3.txt";}
    else if(read_quarks_where==1){where_is_quark12=output_folder+"Spm_"; where_is_quark3=output_folder+"my_quark_grid_out_3.txt";}
    else if (read_quarks_where ==2 ){where_is_quark12 = output_folder_ps + "Cquark";}


#endif


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<

    //Print to screen commands for overview:
    mout<<output_folder_standart<<endl;
    mout<<"In this session: "<<" n_ll="<<TFF_para.n_ll<<" , n_lz="<<TFF_para.n_lz<<" ,n_ly="<<TFF_para.n_ly<<" ,n_ll_bse="<<bse_para.n_ll<<" , n_z_bse="<<bse_para.n_lz<<" , n_cheb="<<n_cheb<<
        " , IRcut²="<<TFF_para.IRcutsq<<" , UVcut²="<<TFF_para.UVcutsq<<" , quark mass="<<TFF_para.mass_at_renorm_pt<<endl;

    mout<<"Selected flags: calc_quark="<<calc_quark<<" ,calc_bse="<<calc_bse<<" ,calc_S3="<<calculate_S3_flag
        <<" ,QPV_interpol="<<QPV_interpolation<<", alpha="<<alpha<<" , grid_sym="<<grid_sym_flag<<endl;

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<

//########################################################################################################################################################################
//########################################################################################################################################################################
//########################################################################################################################################################################

    //########### PROGRAM BODY ################

    //step 0: INTI - Initilaize the phasespace for the triangle decay/TFF:
    //In this case most of this is grid points for the loop momenta.
    TFF_ps ps (TFF_para);
#ifdef K_NOT_USING_INPUT_FILE
    TFF_para.write(input_folder);
#endif

    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<


    //step 1: QUARK - calculate/read the real quark propagator

    //------------------------REAL QUARK------------------------------------------------------------------
    ///This is where the real quark is either calcualated or read inn.
    /// The  default folder is set to be in input/real_quark.txt.
    /// First the DSE is initalized with the parameters (cut-offs,..) those are read Inn @ cons_dse.txt
    /// Then it is calculated or read_Inn. The parameter file has to match the read inn quark.
    // - false = ReadInn  , - true=calculate:

    time_t time1, time2;
    time(&time1);

    QuarkDse dse((input_folder+"cons_dse.txt"));
    if(calc_quark){
        dse.calc_quark();
    }else{
        dse.Fill_QuarkDse(input_folder+"real_quark.txt");
    }
    dse.getQuark().write_quark(input_folder+"real_quark.txt");

    time(&time2); give_time(time1,time2,"real quark"); mout<<"-------------------------------------"<<endl;

    //-------------------REAl QUARK END-------------------------------------------------------------------


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<


    //step 2: MESON -calculate/read the meson amplitude Gamma(l,P)   - BSE -
    //------------------------MESON BSE------------------------------------------------------------------
    //flags for choosing what to calculate are above:


    mout<<"In this run we are using:     "<<which_method_meson<<"    to solve the meson BSE."<<endl;

    array<int,3> bse_index;
    if(which_method_meson == "matrix_cheb" || which_method_meson == "vector_cheb"){
        bse_index= {basis_meson, bse_para.n_pp, bse_para.n_cheb};
    }else{
        bse_index= {basis_meson, bse_para.n_pp, bse_para.n_pz};
    }

    ps_amplitude<3> meson_amp(bse_index);
    double calculated_decayconstant;

    //These are the parameters for the actual size of the bse after possible interpolatione tc
    array<int,3> actual_bse_index;
    actual_bse_index= {basis_meson, TFF_para.n_ll, TFF_para.n_lz}; SuperIndex<3> actual_index_of_bse({basis_meson, TFF_para.n_ll, TFF_para.n_lz});

    //Calculation of the meson - here the previous dse is passed.
    //Here we have to choose by a string and the explicit chioce of the class,
    // which meson we are calcuating. ("ps", "ve", ...)
    ps_MesonBse bse(meson_amp, &dse, &dse, which_method_meson , input_folder+"cons_bse.txt", output_folder_ps, false, {1,1,0,0,0}, which_meson);
    if(calc_bse==1 || calc_bse==6)
    {
        meson_mass = bse.find_the_bs_mass(meson_mass, 0.005, 0.0001);
        bse.norm_the_meson(); mout<<"--------------------> Meson is normed"<<endl;

        if(which_meson=="ps")
        {
            calculated_decayconstant= bse.get_decay_constant();
        }

        bse.write_out_amplitude(output_folder_ps+ which_meson +"_meson_amplitude.txt");
        Psquared= - meson_mass*meson_mass;
    }else if (calc_bse==2){
        bse.solve_and_update_pion(- meson_mass *  meson_mass);
        bse.meson.setPsquared(- meson_mass * meson_mass);
        bse.norm_the_meson(); mout<<"--------------------> Meson is normed"<<endl;

        if(which_meson=="ps")
        {
            calculated_decayconstant= bse.get_decay_constant();
        }

        bse.write_out_amplitude(output_folder_ps+ which_meson +"_meson_amplitude.txt");
        Psquared= - meson_mass*meson_mass;



    }else if (calc_bse==0){
        //Changing the index for Read In.
        bse_index=actual_bse_index;
        bse.meson.setSi(actual_index_of_bse);

        Psquared = bse.read_amplitude(output_folder_ps+which_meson+"_meson_amplitude.txt");


    }else if(calc_bse == 3){
        cout<<" -Skpipping the BSE calc"<<endl;


    }else if(calc_bse == 5){}

    if(calc_bse==6){
        cout<< "interpolation need due to different grids in BSE and TFF"<<endl;
        string helper = "mv "+ output_folder_ps+ which_meson +"_meson_amplitude.txt"+ " " +output_folder_ps+ which_meson +"_meson_amplitude_precalc.txt";
        int dummy =  system(helper.c_str());


    }
    if(calc_bse ==5 || calc_bse == 6){

        //important: INTERPOLATION HERE

        //check if the cheby methode was used and if so if the file is already in chebys or not.

        //interpolation on exsisting data-set.
        mout<<"Starting interpolation for meson data."<<endl;
        interpolate_this_bse_data(output_folder_ps+which_meson+"_meson_amplitude",
                                  ps, previous_rad_points_for_meson_interpolation,is_the_meson_read_file_in_chebys );
        Psquared = bse.read_amplitude(output_folder_ps+which_meson+"_meson_amplitude.txt");

        //Furthermore also change the parameters behind the bse_amp object
        bse_index=actual_bse_index;
        bse.meson.setSi(actual_index_of_bse);

    } //Push in a extra statement below, if there is any need for interpolation.

    if(calc_bse != 2 && calc_bse !=1){
        bse.setAlready_deleted_quarks(true);
    }



    if(which_method_meson=="vector_cheb" && (calc_bse !=5 || calc_bse != 6)){

        //I am working here.
//         cin.get(); //Wait! not done..
        if(is_the_meson_read_file_in_chebys=true){}//move on.
        else{cin.get();}
//        cheby_expand_bse_amp( ps, n_pp, n_cheb, meson_amp)
    }


    //One last check and a warning message on screen if there is still a size miss match.
    if( (bse.meson.getRealSize() != (TFF_para.n_ll * TFF_para.n_lz * basis_meson))  &&
        (bse.meson.getRealSize() != (TFF_para.n_ll * TFF_para.n_cheb * basis_meson)) ){
        cout<<"WARNING - Meson: Interpolation needed? The data that was Read Inn was not the right size!"
              <<endl<<"Current Size= "<<bse.meson.getRealSize()<<tab<<"constant: "<<(n_ll * n_lz * basis_meson)<<tab
            <<(n_ll * n_cheb * basis_meson)<<endl<<"Did you want to interpolate. Choose case 5. "<<endl;
        return 0; }//cin.get(); }


    //When using Gernots data:
    if(using_gernots_meson && calc_bse ==5 ){

        if(which_meson =="ps"){

            for (int si_i = 0; si_i < bse.meson.getSi().getNsuper_index(); ++si_i){
                array<int,3> exp= bse.meson.getSi().are(si_i);

                Cdoub new_meson= bse.meson.getAmp()[si_i]*1.0/(sqrt(6.0));
                double m= real(sqrt(-Psquared));

                if(exp[0]==0){bse.meson.set(si_i,(1.0)*new_meson);}
                if(exp[0]==1){bse.meson.set(si_i, (1.0)*new_meson*1.0/(m));}
                if(exp[0]==2){bse.meson.set(si_i,(1.0)*new_meson*1.0/(m*m*m));}
                if(exp[0]==3){bse.meson.set(si_i, new_meson*1.0/(m*m));}
            }

        }else if(which_meson =="scalar"){

            for (int si_i = 0; si_i < bse.meson.getSi().getNsuper_index(); ++si_i){
                array<int,3> exp= bse.meson.getSi().are(si_i);

                Cdoub new_meson= bse.meson.getAmp()[si_i]*1.0/(sqrt(6.0));
                double m= real(sqrt(-Psquared));

                if(exp[0]==0){bse.meson.set(si_i,(-1.0)*new_meson);}
                if(exp[0]==1){bse.meson.set(si_i, (-1.0)*new_meson*1.0/(m*m*m));}
                if(exp[0]==2){bse.meson.set(si_i,(-1.0)*new_meson*1.0/(m));}
                if(exp[0]==3){bse.meson.set(si_i, new_meson*1.0/(m*m));}
            }
        }


    }
    if(fix_the_meson_mass){Psquared=Psquared_fixed;}

    //important: DEBUGGING;
//    Psquared=-0.1349740*0.1349740;


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<



    //step 3: MOMENTA - Choose the total momentum for calculation. Q² Q'²


    //------------------------LOOP FOR DIFFERENT QPS VALUES-------------------------------------------------------

    //could change this such that the grid is actually choosen in the grid routine, only size of grid i chosen here.
    // Q' -   V1(r+, Q') - Qfs    =====    //-Q -   V2(r-, -Q) - Qis
#ifdef USE_MY_QPHV_VERTEX
    vector<double> Qps_values;


        Line Qpline(0.0,1.0, 22, "linear", "leg", false, false);
        Qps_values=Qpline.getGrid();

#endif

    if(append_to_older_calc > 0 ){
        //do not delete previous file.
    }else{
        //delte file that safes data if exsiting
        string system_output0 = "rm "+ output_folder_qphv + "All_formfactor.txt;" + "rm "+ output_folder_qphv + "All_denorm.txt;"
                                + "rm "+ output_folder_qphv + "All_formfactor_all.txt;" + "rm "+ output_folder_qphv + "All_formfactor_without_denorm.txt;";
        system(system_output0.c_str()); }


//----------------------loop----------------------------------------------start

//big loop over Qps-values - start
#ifdef USE_MY_QPHV_VERTEX

for (int i = 0; i < Qps_values.size(); ++i) {

#endif


    VecCdoub Q, Qp;

    //important: Here I now choose depending on the choice for Q² and Q'²^which interpolation is used to calculate the QPHV's.
    // Q' -   V1(r+, Q') - Qfs    =====    //-Q -   V2(r-, -Q) - Qis
    // (In case of the asymmetric limit we used to pick Q'²=0.)
    //(1) "smallQ" = old interpolation - small value work, but not for all Q².
    //(2) "spacelike" = new interpolation - spacelike only and Q²>0.1. Small values do not work. Do not use for Q²=0.
    // V1 - Q'²      - V2 -Q²
    string which_interpolation_V1;
    string which_interpolation_V2;
// -> Now selected in creat_Qs_Q_grids();

    //Create the grid in the Q² and Q'² variable.
#ifdef USE_MY_QPHV_VERTEX
    create_Qs_Qps_grid_interpol_boundaries(Q, Qp, Psquared, alpha, which_interpolation_V1, which_interpolation_V2, ps,
                                           grid_sym_flag, Qps_values[i], amount_of_Q_grid_points, which_meson);
#endif
#ifndef USE_MY_QPHV_VERTEX
    create_Qs_Qps_grid_interpol_boundaries(Q, Qp, Psquared, alpha, which_interpolation_V1, which_interpolation_V2, ps,
                                           grid_sym_flag, 0.0, amount_of_Q_grid_points, which_meson);
#endif


    //Print out all Q² values to screen and file:
    if(Q.size() !=0) {

    cout << "Vertex set_up:  V1= " << which_interpolation_V1 << "  , V2= " << which_interpolation_V2 << endl;

    //Print Q² and Q'² values to file
    string buffer = output_folder_qphv + "where_is_my_grid.txt";
    ofstream f(buffer.c_str());
    for (int j = 0; j < Q.size(); ++j) {

        f << real(Q[j]) << tab << imag(Q[j]) << tab << real(Qp[j]) << tab << imag(Qp[j]) <<tab << real(Psquared) << endl;
    }
    f.close();


    //Message on screen:
    //And now print out the p -vector and safe the values in the phasespace ps.
    int n_pvec = Q.size();
    ps.Q_Qp_P = vector<array<Cdoub, 3>>(); //resetting the vector for all the Q^2 and Q'^2 values.
    mout << "p values of this session: " << endl;
    for (int i_pvec = 0; i_pvec < Q.size(); ++i_pvec) {
        ps.Q_Qp_P.push_back({Q[i_pvec], Qp[i_pvec], Psquared});
        fv<Cdoub> p = get_p_vec(alpha, Q[i_pvec], Qp[i_pvec], Psquared);
        mout << "p=( " << p.content[0] << " , " << p.content[1] << " , " << p.content[2] << " , " << p.content[3]
                << tab << tab <<  " , Q² = "<< Q[i_pvec]<<tab<< " , Q'² = "<<Qp[i_pvec] <<tab <<"  , P²= "<<Psquared
             << endl;
    }
    mout << "|...........................................|" << endl;
    mout << "In this session: n_pvec= " << n_pvec << endl;


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<



    //step 5: QUARKS!!!
    //step 5: a) calculate/read the complex quark S(k3)  &  S(k1),S(k2) below b)
    mout << "Complex quark for triangle" << endl << "|...........................................|" << endl;
    time(&time1);

    vector<array<Cdoub, 2>> quark3;
    vector<array<Cdoub,2>>  quark1;
    vector<array<Cdoub,2>>  quark2;

    string where_to_find_exe = gethomdir() + "scripts/interpolate_qpv/interpolators/Exe/";
    string where_to_find_interpolation_data = gethomdir() + "scripts/interpolate_qpv/interpolators/";

    if (calculate_S3_flag == 1) {
        //Calculate the Sk3 quark with the naive method.
        mout << "calculation of the complex quark S(k3)" << endl << "|...........................................|"
             << endl;
        quark3 = calculate_S3(ps, where_is_quark3, dse, alpha);
    } else if (calculate_S3_flag == 0) {
        //Read it in from a previous calculation.
        quark3 = read_complex_Q_new(where_is_quark3);

    } else if (calculate_S3_flag == 3) {

        //////////////////////////////////////////////////////////////////////////////////////////////////
        //Using the external Fortran interpolation for obtaining results for Sk3.
        mout << "interpolation (via external Fortran code) of the complex quark S(k3)" << endl
             << "|...........................................|" << endl;

        //Use an external interpoaltion to obtain the quark.
        write_quark_grid_for_interpol(output_folder, ps, alpha, 3);

        //Copy the executable
        string helper = "cp " + where_to_find_exe + "quarkall.out "+ output_folder_standart;
        int dummy = system(helper.c_str());

        helper = "cd " + output_folder_standart+ "; ./quarkall.out "+where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_complex_1_parabola_edit.dat"+ " " +
                where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_info.dat" + " " +
                where_is_quark3 + " " + output_folder+"quark_grid_out_3.txt";
//        cout<<helper<<endl;
        dummy = system(helper.c_str());

        //remove executables:
        helper = "rm "+output_folder_standart+"quarkall.out";
        dummy = system(helper.c_str());

        //ReadIn form the new interpolated file
        quark3 = read_complex_Q_new(output_folder + "quark_grid_out_3.txt");


    } else if (calculate_S3_flag == 4){
        // Do nothing...
    } else {
        cout << "S3: What about the quark3? " << endl;
        assert(false);
    }

    time(&time2);
    give_time(time1, time2, "complex quark S(k3) done");
    mout << endl << "|...........................................|" << endl;

    time(&time1);

    //step 5 b) : Calculating Spm, if not in the rest-frame alpha < 1.e+2 :
    //in case of asymmetric routing the quarks appearing in the diagram have to be calcuated once more.
    if ( calculate_Spm_flag ==1 ) {
        //My naive methode
        //Calculating the values with my naiive quark method.
        mout << "Using the naive method to calculate Spm." << endl;
        quark1 = calculate_Spm(ps, output_folder, dse, alpha, get_kp, "1");
        quark2 = calculate_Spm(ps, output_folder, dse, alpha, get_km, "2");
    } else if (calculate_Spm_flag==3) {
        //Fotran interpolation: S(k1) & S(k2)
        //Using the Fortran interpoaltion on gernots quark data.
        //The grid have already been written out above when starting the routine for Sk3.


        //Write out the grid-points for both Spm:
        write_quark_grid_for_interpol(output_folder, ps, alpha, 1);

        //Copy the executable
        string helper = "cp " + where_to_find_exe + "quarkall.out "+ output_folder_standart;
        int dummy = system(helper.c_str());

        //Call the executable for Sp
        helper = "cd " + output_folder_standart+ "; ./quarkall.out "+where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_complex_1_parabola_edit.dat"+ " " +
                 where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_info.dat" + " " +
                 where_is_quark12+"1.txt" + " " + output_folder+"quark_grid_out_1.txt";
        dummy = system(helper.c_str());

        //Call the executable for Sm
        helper = "cd " + output_folder_standart+ "; ./quarkall.out "+where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_complex_1_parabola_edit.dat"+ " " +
                 where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_info.dat" + " " +
                 where_is_quark12+"2.txt" + " " + output_folder+"quark_grid_out_2.txt";
        dummy = system(helper.c_str());

        //remove executables:
        helper = "rm "+output_folder_standart+"quarkall.out";
        dummy = system(helper.c_str());

        //ReadIn from the interpolated file
        quark1 = read_complex_Q_new(output_folder + "quark_grid_out_1.txt");
        quark2 = read_complex_Q_new(output_folder + "quark_grid_out_2.txt");


    } else if (calculate_Spm_flag == 0) {
        //ReadIn from the interpolated file
        quark1 = read_complex_Q_new( where_is_quark12 + "1.txt");
        quark2 = read_complex_Q_new( where_is_quark12 + "2.txt");
    } else{cout<<"Nothing was done for quark S+, S-."<<endl;}
    time(&time2);
    give_time(time1, time2, "complex quark S+ and S- done");



    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<


    //step---------------------------------------------------------------------------|||||||||||||||||||||||||||||||||||||||||||||<
    //step 6: Call the external Fortran interpolation of the quark-photon vertex.
    mout << "-------------------------------------" << endl;
    mout << "Starting the vertex calcaution/interpolation" << endl << "|...........................................|"
         << endl;

    // QPHV - Write out the interpolation values for the quark photon vertex
    if(QPV_interpolation==0){}
    else{
        mout<<"Writing out the Grid for the vertex interpolation"<<endl;
        output_qphv_grid(output_folder_qphv, ps, 1, alpha);
        output_qphv_grid(output_folder_qphv, ps, 2, alpha);
    }


    time(&time1);
    //If the interpolation outputfiles are already calculated and ready to use in your current folder: ==0.
    if (QPV_interpolation == 0) { mout << "QPV already calculated earlier. Done" << endl; }
    else if (QPV_interpolation == 1)
    {
        //Gernots interpolation -
        //Calling Gernots Fortran program for the interpolation of the qphv vertex
        time_t time3, time4;

        //Copy the executable
        string helper = "cp " + where_to_find_exe + "vertex.out "+ output_folder_standart;
        int dummy = system(helper.c_str());

/* ./interpolators/Exe/vertex.out  \
../input_Cquark_old/__QP_complex_1_parabola_edit.dat \
../input_Cquark_old/__QP_info.dat \
../input_qphv_old/__iBSE_QPV_complex_transverse.dat \
../input_qphv_old/par_ibse.dat \
../../../data/data852/output_qphv/quarkphoton_grid_1.txt \
../../../data/data852/output_qphv/quarkphoton_grid_out_1.txt*/

        //Call the executable for V1 + time
        time(&time3);
        cout<<"V1: "<<endl;
        helper = "cd " + output_folder_standart+ "; ./vertex.out " + where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_complex_1_parabola_edit.dat"+ " " +
                 where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_info.dat" + " " +
                 where_to_find_interpolation_data+"input_qphv_"+which_interpolation_V1+"/"+"__iBSE_QPV_complex_transverse.dat" + " " +
                 where_to_find_interpolation_data+"input_qphv_"+which_interpolation_V1+"/"+"par_ibse.dat" + " " +
                 output_folder_qphv +  "quarkphoton_grid_1.txt " + " " + output_folder_qphv +  "quarkphoton_grid_out_1.txt ";
//        cout<<helper<<endl;
        dummy = system(helper.c_str());
        time(&time4);
        give_time(time3, time4, "V1- done");

        //Call the executable for V2 + time
        time(&time3);
        cout<<"V2: "<<endl;
        helper = "cd " + output_folder_standart+ "; ./vertex.out " + where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_complex_1_parabola_edit.dat"+ " " +
                 where_to_find_interpolation_data+"input_Cquark_"+which_quark_interpolation_data+"/"+"__QP_info.dat" + " " +
                 where_to_find_interpolation_data+"input_qphv_"+which_interpolation_V2+"/"+"__iBSE_QPV_complex_transverse.dat" + " " +
                 where_to_find_interpolation_data+"input_qphv_"+which_interpolation_V2+"/"+"par_ibse.dat" + " " +
                 output_folder_qphv +  "quarkphoton_grid_2.txt " + " " + output_folder_qphv +  "quarkphoton_grid_out_2.txt ";
        dummy = system(helper.c_str());
        time(&time4);
        give_time(time3, time4, "V2- done");


        //remove executables:
        helper = "rm "+output_folder_standart+"vertex.out";
        dummy = system(helper.c_str());

    }

#ifdef USE_MY_QPHV_VERTEX
    else if (QPV_interpolation == 2) {


        //todo: important: Check on which varibales they depend on because of angle_project_out....
        VecCdoub ksquared;
        VecCdoub kP;
        VecDoub Psquared;

        //For now I am using the same grid points etc as for the bse here. Thus
        string dummy = "cp " + input_folder + "cons_bse.txt " + input_folder + "cons_qphv.txt;";
        system(dummy.c_str());

        //create the object that carries the meson qphv.
        BseParameter qphv_para(input_folder + "cons_qphv.txt");
        array<int, 3> qphv_bse_index;
        if (which_method_meson == "matrix_cheb" || which_method_meson == "vector_cheb") {
            qphv_bse_index = {12, qphv_para.n_pp, qphv_para.n_cheb};
        } else {
            qphv_bse_index = {12, qphv_para.n_pp, qphv_para.n_pz};
        }
        ps_amplitude<3> amp_qphv(qphv_bse_index);

//        qphv_MesonBse qphv_bse(amp_qphv, &dse, &dse, which_method_meson , input_folder+"cons_qphv.txt", output_folder_qphv, false, what_to_calculate);


        //Precalculate ready to use qphv bses:
        //using the direct calculation of the vertex with my previous calculated qphv. qphv data must be in the folder
        //or calculated now.

        //loop for the 2 vertices . - calc 1 and 2.
        for (int which_vertex = 1; which_vertex < 3; ++which_vertex) {


            qphv_MesonBse qphv_bse(amp_qphv, &dse, &dse, which_method_meson, input_folder + "cons_qphv.txt",
                                   output_folder_qphv, false, {1,1,0,0,0});


            where_is_vertex_needed_read_inn(output_folder_qphv + "quarkphoton_grid_" + to_string(which_vertex) + ".txt",
                                            n_pvec, ksquared, kP, Psquared);

            if (!precacualtion_of_vertex_already_done) {
                //todo: Watch out - need to make sure that the method of the read in file is the same here!
                cout << "Starting precalculation of vertex" << endl;
                precalc_bses_for_qphv(Psquared, qphv_bse, output_folder_qphv, which_vertex);
            }

            //(1) Readin all the precalculated result. +
            //(2) Iterate qphv one more time at for all VALUES, to get our result
            //(3) Finally safe it into file, usable for the main formfactor.calc()
            //All included in following function():

            calculate_all_qphv(qphv_bse, output_folder_qphv, Psquared, ksquared, kP, "vector", which_vertex);

            //Resetting things in the bse amplitude.
            //Need to reset the flag for the quarks apparently
            //todo: Check where this gets change in course of the bse calc procedure, as it must be and think about changing it there. - no.
            qphv_bse.setWhat_to_calc_flag({1, 1, 0, 0, 0});
            qphv_bse.reset_inhomogenous();


        }


    }
#endif

    else { assert(false); }
    time(&time2);
    give_time(time1, time2, "QPV");
    mout << endl << "|...........................................|" << endl;


    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<

    //----------------------------- FINALE ----------------------------------
    //step---------------------------------------------------------------------------|||||||||||||||||||||||||||||||||||||||||||||<
    //step 7: Calculate the form factor - putting together all components - final calcaution
    mout << "-------------------------------------" << endl;
    mout << "Main calculation: Form factor" << endl << "|...........................................|" << endl;

    time_t starting7, dasende7;
    time(&starting7);

    VecCdoub TFF_vec;

    if (flag_calculate_tff) {

        formfactor TFF(ps, output_folder_qphv, alpha, which_meson);

        if(which_meson=="ps"){
            TFF_vec = TFF.debug_calc_formfactor(quark1, quark2, quark3, bse.meson);
        }else{
//            TFF_vec = TFF.debug_calc_formfactor_lorenz(quark1, quark2, quark3, bse.meson);
            TFF_vec = TFF.debug_calc_formfactor(quark1, quark2, quark3, bse.meson);
        }




        mout << "With the factor infront we obtain:  fixed fpi="<<decayconstant_fixed<<"  ;TTF= "
             << 4.0 * M_PI * M_PI * decayconstant_fixed * TFF_vec[0] <<
             tab << "calculated fpi="<<calculated_decayconstant<< "   ;TFF=" << 4.0 * M_PI * M_PI * calculated_decayconstant * TFF_vec[0] << endl;

    }
    time(&dasende7);
    give_time(starting7, dasende7, "form factor");
    mout << "|...........................................|" << endl << endl;

    cout << "|------------------------------------------------|" << endl;

#ifdef USE_MY_QPHV_VERTEX
}else{cout<<"No points in intervall, thus try next eta/Qs value.."<<endl;}
#endif
//----------------------loop----------------------------------------------end
//big loop over Qps-values
}
    //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||<


#ifdef K_CALC_EACH_AMP_COMPONTENT
    mout<<"Now calculating the form factor once more for the individual components."<<endl;

    VecCdoub new_TFF_vec;

    for (int j = 0; j < chi.getSi().getMaxima_of_partial_indices()[0]; ++j) {


        //recalculate chi:
        vector<int> new_amp_flag = {j};
        bse.calculate_chi(chi, new_amp_flag);


        //and afterwards the whole form factor:
        new_TFF_vec = TFF.calc_formfactor(quark1, quark2, quark3, chi );
        TFF.write(1.0, new_TFF_vec, "amp_"+to_string(j)+"_");
        TFF.write((1.0/real(TFF_vec[0])), new_TFF_vec, "F[0]_amp_"+to_string(j)+"_");

    }
#endif


    time(&dasende); give_time(starting,dasende,"the whole main");

//    string system_output1 = "cp "+gethomdir()+"tff.out "+output_folder_standart;
//    system(system_output1.c_str());

//    string system_output1 = "mv "+gethomdir()+"output_file_"+flag_for_running_multipl_codes+" "+ output_folder_standart + "/ ;";
//    system(system_output1.c_str());


    return 0;



}
