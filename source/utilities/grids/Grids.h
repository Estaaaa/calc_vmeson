//
// Created by paul on 11/05/16.
//

#ifndef DEVELOPER_GRIDS_H
#define DEVELOPER_GRIDS_H

#include <vector>
#include <assert.h>
#include <string>
#include <iostream>
#include <complex>
//#include <bits/algorithmfwd.h>


using namespace std;

// this class is a grid class, that can construct integration weights and the matching interpolation weights
// for log measure the functional determinant is included in the weigts, so Int f(x) = w[i] f[x_i]
class Line{

public:
    Line(double lower_boundary, double upper_boundary, int number_of_grid_points, string functional_measure,
                   string point_distribution, bool quadrature = false, bool barycentric = false);

    Line(vector<complex<double>>& outside_grid);
    Line(vector<double>& outside_grid);

    Line(double lower_boundary, double upper_boundary, int number_of_grid_points, string functional_measure,
             string point_distribution, bool quadrature, bool barycentric, int splits);

    virtual ~Line();

    Line();

    //public functions:
    void print();
    void print_weights();
    void print_barycentric_weights();

    const string & getPoint_distribution() const;
    const string & getFunctional_measure() const;
    double getLower_boundary() const;
    double getUpper_boundary() const;
    int getNumber_of_grid_points() const;
    const vector<double> & getGrid() const;
    const vector<complex<double>> & getCGrid() const;
    const vector<double> & getWeights_integration() const;
    const vector<double> & getWeights_barycentric() const;


    //Getter and Setter
    void setLower_boundary(double lower_boundary);
    void setUpper_boundary(double upper_boundary);

protected:
    // variables
    vector< double> weights_integration;
    vector< double> weights_barycentric;

    std::string point_distribution;
    std::string functional_measure;
    double lower_boundary;
    double upper_boundary;
    int number_of_grid_points;
    vector<double> grid;
    vector<complex<double>> c_grid;

    bool quadrature;
    bool barycentric;

    // functions
    void fill_line_with_data(vector<complex<double>>& outside_grid);
    void fill_line_with_data(vector<double>& outside_grid);

    void fill_equally_spaced();
    void fill_tscheb();
    void fill_legendre();
    void fill_leg_split(int nsplit);
    void fill_log_grid();
    void fill_sinh_tanh();
    void fill_tscheb_zeros();


    void linear_trafo_from_minusone_one_to(double a, double b);
    void linear_trafo_from_to(double c, double d, double a, double b);

    void func_trafo_log();
    void func_trafo_arcsinh();
    void func_trafo_new_sinh();
    void func_trafo_bumb();
    void func_trafo_angcos();
    void func_trafo_zero_Pi();
    void func_trafo_MT_log();
    //don't work!!
    void func_trafo_G_BSE();

    void fill_barycentric();

    void fill_equally_centered();
};

typedef std::vector<Line*> lineVec;


#endif //DEVELOPER_GRIDS_H
