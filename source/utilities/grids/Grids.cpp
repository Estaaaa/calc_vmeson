//
// Created by paul on 11/05/16.
//

// if log grid is chosen, the grid will start at lower boundary, not at log( lower boundary)
// Comment

#include "Grids.h"
#include "gauleg.h"
#include "quadrule.h"

//Constructors of the class:
Line::Line(double lower_boundary, double upper_boundary, int number_of_grid_points, string functional_measure,
               string point_distribution, bool quadrature, bool barycentric)
        : lower_boundary(lower_boundary), upper_boundary(upper_boundary),
          number_of_grid_points(number_of_grid_points), functional_measure(functional_measure),
          point_distribution(point_distribution), quadrature(quadrature), barycentric(barycentric) {

    if( number_of_grid_points == 1){

        //cout << "A one point grid is chosen, the value is set to midpoint, is this ok?" << endl;
        grid.resize(1);
//        grid[ 0] = lower_boundary + (upper_boundary - lower_boundary)/2.;
        //Careful changed things here: Now it is the upper value when choosing one point!!
        grid[ 0] = upper_boundary;
    }
    else {


        //todo: Note that I had to make sure that the Line constructor is not called with quadratur=false and barycentric=true.
        // Case is taken care of by this, but maybe we want to make it an assert?
        if (barycentric) {
            this->quadrature=true;
//            cout<<"Grid.cpp::We must define weights in order to get barycentrics."<<endl;
        }

        grid.resize( number_of_grid_points);

//        if (functional_measure == "zero_Pi"){ setLower_boundary(0.0); setUpper_boundary(M_PI);}

        if (point_distribution == "tscheb") {
            fill_tscheb();
        }
        else if (point_distribution == "leg") {
            fill_legendre();
        }
        else if (point_distribution == "leg_split") {
            //Here we do an atumatic chioce of 2;
            fill_leg_split(2);
        }
        else if (point_distribution == "equal") {
            fill_equally_spaced();
        }
        else if( point_distribution == "equal_centered_around_midpoint"){
            fill_equally_centered();
        }
        else if (point_distribution == "log") {
            fill_log_grid();
            assert( false); // This works but its not clean. In the future I want the other rout to be taken, also since the functional determinant is included when one chooses
                            // legendre, and functional trafo to log.
        }
        else if (point_distribution == "sinh_tanh") {
            fill_sinh_tanh();
        }
        else if (point_distribution == "tscheb_zeros") {
            cout<<"Watch out we are using tscheb_zeros, which amount is your z?"<<endl; cin.get();
            fill_tscheb_zeros();
        }
        else {
            assert(0 == 1);
        }


        if (functional_measure == "log") {
            func_trafo_log();
        }
        else if (functional_measure == "linear") {

        }
//        else if (functional_measure == "arcsinh") {
//            func_trafo_arcsinh();
//            assert( false);
//        }
        else if (functional_measure == "arcsinh" ) { //&& point_distribution == "leg") {
            func_trafo_new_sinh();
        }
        else if (functional_measure == "ang_cos" ) { //&& point_distribution == "leg") {
            func_trafo_angcos();
        }
        else if (functional_measure == "bumb") {
            func_trafo_bumb();
        }
        else if (functional_measure == "zero_Pi") {
            func_trafo_zero_Pi();
        }
        else if (functional_measure == "MT_log") {
            func_trafo_MT_log();
        }
        else if (functional_measure == "G_BSE") {
            func_trafo_G_BSE();
        }
        else {
            assert(false);
        }

        if (barycentric) {
            fill_barycentric();
        }


    }

}

//2nd constructor to pass n of splits:
Line::Line(double lower_boundary, double upper_boundary, int number_of_grid_points, string functional_measure,
           string point_distribution, bool quadrature, bool barycentric, int splits)
        : lower_boundary(lower_boundary), upper_boundary(upper_boundary),
          number_of_grid_points(number_of_grid_points), functional_measure(functional_measure),
          point_distribution(point_distribution), quadrature(quadrature), barycentric(barycentric) {

    if( number_of_grid_points == 1){

        //cout << "A one point grid is chosen, the value is set to midpoint, is this ok?" << endl;
        grid.resize(1);
//        grid[ 0] = lower_boundary + (upper_boundary - lower_boundary)/2.;
        //Careful changed things here: Now it is the upper value when choosing one point!!
        grid[ 0] = upper_boundary;
    }
    else {


        //todo: Note that I had to make sure that the Line constructor is not called with quadratur=false and barycentric=true.
        // Case is taken care of by this, but maybe we want to make it an assert?
        if (barycentric) {
            this->quadrature=true;
//            cout<<"Grid.cpp::We must define weights in order to get barycentrics."<<endl;
        }

        grid.resize( number_of_grid_points);

//        if (functional_measure == "zero_Pi"){ setLower_boundary(0.0); setUpper_boundary(M_PI);}

        if (point_distribution == "tscheb") {
            fill_tscheb();
        }
        else if (point_distribution == "leg") {
            fill_legendre();
        }
        else if (point_distribution == "leg_split") {
            fill_leg_split(splits);
        }
        else if (point_distribution == "equal") {
            fill_equally_spaced();
        }
        else if( point_distribution == "equal_centered_around_midpoint"){
            fill_equally_centered();
        }
        else if (point_distribution == "log") {
            fill_log_grid();
            assert( false); // This works but its not clean. In the future I want the other rout to be taken, also since the functional determinant is included when one chooses
            // legendre, and functional trafo to log.
        }
        else if (point_distribution == "sinh_tanh") {
            fill_sinh_tanh();
        }
        else if (point_distribution == "tscheb_zeros") {
            cout<<"Watch out we are using tscheb_zeros, which amount is your z?"<<endl; cin.get();
            fill_tscheb_zeros();
        }
        else {
            assert(0 == 1);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        if (functional_measure == "log") {
            func_trafo_log();
        }
        else if (functional_measure == "linear") {

        }
//        else if (functional_measure == "arcsinh") {
//            func_trafo_arcsinh();
//            assert( false);
//        }
        else if (functional_measure == "arcsinh" ) { //&& point_distribution == "leg") {
            func_trafo_new_sinh();
        }
        else if (functional_measure == "ang_cos" ) { //&& point_distribution == "leg") {
            func_trafo_angcos();
        }
        else if (functional_measure == "bumb") {
            func_trafo_bumb();
        }
        else if (functional_measure == "zero_Pi") {
            func_trafo_zero_Pi();
        }
        else if (functional_measure == "MT_log") {
            func_trafo_MT_log();
        }
        else if (functional_measure == "G_BSE") {
            func_trafo_G_BSE();
        }
        else {
            assert(false);
        }

        if (barycentric) {
            fill_barycentric();
        }


    }

}
Line::Line() { }

///------------------------------------------------------------
//transforming the weights and gridpoints to another disribution:
void Line::linear_trafo_from_minusone_one_to(double a, double b) {

    double m = (b - a) / 2.;
    double c = (b + a) / 2.;

    for (int i = 0; i < number_of_grid_points; ++i) {
        grid[ i] = m * grid[ i] + c;
    }
}

void Line::linear_trafo_from_to(double c, double d, double a, double b) {
    double m = ( a-b)/( c-d);
    double e = ( a - c * ( a -b)/(c-d));

    for (int i = 0; i < number_of_grid_points; ++i) {
        grid[ i] = m * grid[ i] + e;
    }
}

void Line::func_trafo_log() {

    double m = ( lower_boundary -upper_boundary) / ( log( lower_boundary/upper_boundary));
//    double e = lower_boundary - ( lower_boundary -upper_boundary)/( log( lower_boundary/upper_boundary)) * log( lower_boundary);

    linear_trafo_from_to(lower_boundary, upper_boundary, log(lower_boundary), log(upper_boundary));

    for (int i = 0; i < number_of_grid_points; ++i) {
//        grid[ i] = exp(( grid[ i] - e) / m) ;
        grid[ i] = exp( grid[ i] ) ;
        if( quadrature) {
            weights_integration[i] = grid[i] * 1. / m * weights_integration[i];
        }
    }

}

void Line::func_trafo_arcsinh() {

    double m = ( lower_boundary -upper_boundary) / (asinh( lower_boundary/upper_boundary));
    double e = lower_boundary - ( lower_boundary -upper_boundary)/(asinh( lower_boundary/upper_boundary)) *asinh( lower_boundary);

    for (int i = 0; i < number_of_grid_points; ++i) {
        grid[ i] = sinh( ( grid[ i] - e) / m );
        if( quadrature) {
            weights_integration[i] = cosh( ( grid[ i] - e) / m) * 1. / m * weights_integration[i];
        }
    }

    cout << "Warning! arcsinh mapping not checked for correctness yet! Press button to continue"; cin.get();

}

void Line::func_trafo_new_sinh() {

    VecDoub x(number_of_grid_points), w(number_of_grid_points);
    gauleg(sinh(lower_boundary), sinh(upper_boundary), x,w );

    for (int i = 0; i < number_of_grid_points; ++i) {

        grid[ i] = asinh(x[i]);

        if( quadrature) {

            weights_integration[i] = (1.0 / sqrt( 1.0 + pow(x[i],2.0))) * w[i];
        }
    }

//    cout << "New Sinh mapping form Esther 05.03.18!"<<endl;

}

void Line::func_trafo_bumb() {


    for (int i = 0; i < number_of_grid_points; ++i) {

        grid[ i] = 1.0 - pow( (grid[i]/2.0 + (1.0/2.0)), 2.0);

        if( quadrature) {

            weights_integration[i] = 2.0 * (grid[i]/2.0 + 1.0/(2.0)) * weights_integration[i];
        }
    }

//    cout << "New Mapping for issue with bumb at the edeges form angular dependence" <<endl;

}

void Line::func_trafo_angcos() {

    VecDoub x(number_of_grid_points), w(number_of_grid_points);
    gauleg(acos(lower_boundary), acos(upper_boundary), x,w );

    for (int i = 0; i < number_of_grid_points; ++i) {

        grid[ i] = cos(x[i]);

        if( quadrature) {

            weights_integration[i] = (-sin(x[i])) * w[i];
        }
    }


}

void Line::func_trafo_zero_Pi(){


    double m = ( lower_boundary -upper_boundary) / ( acos( - lower_boundary)- acos(- upper_boundary) );
    double e = lower_boundary - m  * acos( - lower_boundary);

    for (int i = 0; i < number_of_grid_points; ++i) {

        grid[ i] = - cos( (grid[ i] - e)/m );

        if( quadrature) {

            weights_integration[i] = 1.0/m * sin( acos(-grid[i]) ) *weights_integration[i];
        }



    }

}

void Line::func_trafo_MT_log(){

    double a=1.0;

    double m = ( lower_boundary -upper_boundary) / ( log((lower_boundary*M_E+a)/(lower_boundary+a))
                                                     - log((upper_boundary*M_E+a)/(upper_boundary+a)) );
    double e = lower_boundary - m  * log((lower_boundary*M_E+a)/(lower_boundary+a));

    for (int i = 0; i < number_of_grid_points; ++i) {

        if( quadrature) {

            weights_integration[i] = a * (M_E - 1.0) * exp( (grid[i]+e) /m )* 1./(pow((exp(1.0+e/m)-exp(grid[i]/m)),2.0)* m ) * weights_integration[i];

//            weights_integration[i] = (a * exp( (grid[i]-e) /m ) * 1./(M_E-exp((grid[i]-e)/m) *m ) +
//                                     a * exp( (grid[i]-e) /m ) * (exp( (grid[i]-e) /m )-1.0) * 1.0/(pow(M_E-exp((grid[i]-e)/m),2.0) * m)
//                                     )* weights_integration[i];
        }

        grid[ i] =  a * ( exp( (grid[i]-e) /m ) -1.0)/(M_E-exp( (grid[i]-e) /m ));


    }

}

void Line::func_trafo_G_BSE() {

    //important: Seem to only work for lots of grid points! nl=120, 24.. werid. Need to check this 28.06.19
    double L1 = sqrt(10);
    double L0= lower_boundary;
    double L2= upper_boundary;

    //inverse function
//    double new_lower_boundary = -(x0 * xn*(L0-L2)*(L1-lower_boundary) )/
//            ( L0*(L1*x0-L2*x0+ L2*xn-xn*lower_boundary)
//              - L1*(L2*xn + x0*lower_boundary - xn *lower_boundary)
//            +L2*x0 *lower_boundary);
//
//    double new_upper_boundary = -(x0 * xn*(L0-L2)*(L1-upper_boundary) )/
//                                ( L0*(L1*x0-L2*x0+ L2*xn-xn*upper_boundary)
//                                  - L1*(L2*xn + x0*upper_boundary - xn *upper_boundary)
//                                  +L2*x0 *upper_boundary);

    double new_lower_boundary = -1.0;
    double new_upper_boundary = 1.0;
    double m = ( lower_boundary -upper_boundary) / ( ( new_lower_boundary -new_upper_boundary));
    linear_trafo_from_to(lower_boundary, upper_boundary, new_lower_boundary, new_upper_boundary);

    double x0=grid[0];
    double xn=grid[number_of_grid_points-1];


        for (int i = 0; i < number_of_grid_points; ++i) {


/*        if( quadrature) {

//            weights_integration[i] = ((L1-L0)*(L1-L2)*(L0-L2)*grid[0]*(grid[0]-grid[number_of_grid_points-1])*grid[number_of_grid_points-1]  / pow(
//                    ( L1*grid[i]*(grid[0]-grid[number_of_grid_points-1]) + L0*(grid[i]-grid[0])*grid[number_of_grid_points-1] + L2*grid[0]*(grid[number_of_grid_points-1]-grid[i]) ),2.0))
//                                     * weights_integration[i]* 1/m ;

            // new grid func = z(x) = a(x)/b(x), thus z'(x) = (a'(x)*b(x) - a(x)*b'(x))/(b²(x)) and weights: w'= w* z'(x) * x'(x_rescale) =  w* z'(x) * 1/m;
            weights_integration[i] =  (( (L0*L2*(grid[number_of_grid_points-1] - grid[0]) + L1*L0*grid[0] - L2*L1*grid[number_of_grid_points-1])
                    * ( L1*grid[i]*(grid[0]-grid[number_of_grid_points-1]) + L0*(grid[i]-grid[0])*grid[number_of_grid_points-1] + L2*grid[0]*(grid[number_of_grid_points-1]-grid[i]) )
                    - ( L0*L2*grid[i]*(grid[number_of_grid_points-1]-grid[0]) + L1*( L0*grid[0]*(grid[i]-grid[number_of_grid_points-1]) + L2*(grid[0]-grid[i])*grid[number_of_grid_points-1] ) )
                    * (L1*(grid[0]-grid[number_of_grid_points-1])+ L0*grid[number_of_grid_points-1] - L2*grid[0]) )
                     / pow( ( L1*grid[i]*(grid[0]-grid[number_of_grid_points-1]) + L0*(grid[i]-grid[0])*grid[number_of_grid_points-1] + L2*grid[0]*(grid[number_of_grid_points-1]-grid[i]) ),2.0))
                                     * weights_integration[i]* 1/m ;
        }*/

        if( quadrature) {
            weights_integration[i] = ((L1-L0)*(L1-L2)*(L0-L2)*x0*(x0-xn)*xn  / pow(
                    ( L1*grid[i]*(x0-xn) + L0*(grid[i]-x0)*xn + L2*x0*(xn-grid[i]) ),2.0))
                                     * weights_integration[i] * 1/m ;
        }

        grid[ i] = ( L0*L2*grid[i]*(xn-x0) + L1*( L0*x0*(grid[i]-xn) + L2*(x0-grid[i])*xn ) ) /
                ( L1*grid[i]*(x0-xn) + L0*(grid[i]-x0)*xn + L2*x0*(xn-grid[i]) );


//        cout<<"x ="<<grid[i]<<tab<<"w ="<<weights_integration[i]<<endl;
    }
    cout<<endl;

}

//-------------------------------------------------------
//Filling grid with different distributions, always form [-1,1]:
void Line::fill_equally_spaced() {

    grid.resize((unsigned long) number_of_grid_points);

    for (int i = 0; i < number_of_grid_points; ++i) {
        grid[ i] = lower_boundary + i * (upper_boundary - lower_boundary) / (number_of_grid_points - 1);
    }

    if (quadrature){
        weights_integration.resize( number_of_grid_points);
        weights_integration.assign( number_of_grid_points, 1.);
        cout << "class Line:: not tested - equally spaced." <<endl;
    }

}

void Line::fill_tscheb() {

    grid.resize((unsigned long) number_of_grid_points);

    if (quadrature){

        weights_integration.resize((unsigned long) number_of_grid_points);
        double xx[ number_of_grid_points];
        double ww[ number_of_grid_points];
        cdgqf(number_of_grid_points, 2, -1., 1., xx, ww);

        double m = 2./( lower_boundary - upper_boundary);

        for (int i = 0; i < number_of_grid_points; ++i) {
            weights_integration[ i] = ww[ i] * m;
            grid[ i]                = xx[ i] ;
        }

        cout << "careful with quadrature, doesnt really work" << endl;
        assert( false);
    }
    else {
        for (int i = 0; i < number_of_grid_points; ++i) {
//            grid[number_of_grid_points - 1 - i] = cos(M_PI * ((i + 1) - 0.5) / number_of_grid_points);
            grid[ i] = cos(M_PI * (i - 0.5+1.0 ) / (double) number_of_grid_points);

            //Comment: This is Paul's old vs my Cheby expression. When I change between the two the imaginary part of my result slightly changes.
            // Should look into that?!
        }
    }
    linear_trafo_from_minusone_one_to(lower_boundary, upper_boundary);
}

void Line::fill_legendre() {

    grid.resize((unsigned long) number_of_grid_points);

    if (quadrature){
        weights_integration.resize( number_of_grid_points);
        gauleg(lower_boundary, upper_boundary, grid, weights_integration);
    }
    else {
        vector< double> dummie( number_of_grid_points);
        gauleg(lower_boundary, upper_boundary, grid, dummie);
    }
}

void Line::fill_leg_split(int nsplit) {

    //important: works only with log-distribution after so far..
    // or the grid distribution for the bse qould be so bad with linear that it is not advisorable
    int dim_of_split_fit = number_of_grid_points / nsplit;
    if((number_of_grid_points % nsplit)!=0){cout<<"Dimension missmatch in grid distribution."<<endl;assert(false);}

    double size_of_splits=(upper_boundary-lower_boundary)/nsplit;

    grid.resize((unsigned long) number_of_grid_points);

    if (quadrature){
        weights_integration.resize( number_of_grid_points);

        for (int i = 0; i < nsplit; ++i) {

            VecDoub x(dim_of_split_fit); VecDoub w(dim_of_split_fit);
            gauleg(lower_boundary + i*size_of_splits, lower_boundary + (i+1)*size_of_splits, x, w);

            for (int j = 0; j < dim_of_split_fit; ++j) {
                grid[j +  i*dim_of_split_fit] = x[j];
                weights_integration[j +  i*dim_of_split_fit] = w[j];
            }
        }

    }
    else {
        for (int i = 0; i < nsplit; ++i) {

            VecDoub x(dim_of_split_fit); VecDoub w(dim_of_split_fit);
            gauleg(lower_boundary + i*size_of_splits, lower_boundary + (i+1)*size_of_splits, x, w);

            for (int j = 0; j < dim_of_split_fit; ++j) {
                grid[j +  i*dim_of_split_fit] = x[j];
            }
        }
    }
}

void Line::fill_log_grid() {

    weights_integration.resize( number_of_grid_points);
    gauleg_log( lower_boundary, upper_boundary, grid, weights_integration);


}

void Line::fill_equally_centered() {

    assert( false);
}

void Line::fill_sinh_tanh() { //important: Is this routine even working? Check this....got rubish when using it for the scalarTFF.

    grid.resize((unsigned long) number_of_grid_points);
    double h =  4.0/(number_of_grid_points+1.0);

    if (quadrature){
        weights_integration.resize( number_of_grid_points);
    }

    for (int i = 0; i < number_of_grid_points; ++i) {

        double k =  double(2 * i - number_of_grid_points +1)/2.0;

        grid[i] = tanh( M_PI/2.0* sinh( k * h ));

        if (quadrature){
            weights_integration[i] = M_PI * (h/2.0) * (cosh( k *h)/ pow( cosh(M_PI/2.0* sinh( k *h )) ,2.0));
        }
    }

    //Normalize the weights
    double weight_sum=0.0;
    for (int i = 0; i < number_of_grid_points; ++i) {
        weight_sum += weights_integration[i];
    }

    for (int i = 0; i < number_of_grid_points; ++i) {
        weights_integration[i] = 2.0 * weights_integration[i]/weight_sum;
    }





}

void Line::fill_tscheb_zeros() {

    grid.resize(0); weights_integration.resize(0);
    double Tscheb_zeoros[number_of_grid_points+2];
    Tscheb_zeoros[0] = lower_boundary; Tscheb_zeoros[number_of_grid_points+1] = upper_boundary;
    for (int i = 1; i < number_of_grid_points+1; ++i) {
        Tscheb_zeoros[i] =  - cos(M_PI * ((i-1) - 0.5+1.0 ) / number_of_grid_points);
    }

    int n_between= 15; VecDoub x(n_between); VecDoub w(n_between);
    for (int i = 0; i < number_of_grid_points+1; ++i) {
        gauleg(Tscheb_zeoros[i], Tscheb_zeoros[i+1], x, w);

        for (int j = 0; j < n_between; ++j) {
            grid.push_back(x[j]);
            weights_integration.push_back(w[j]);
        }
    }

    number_of_grid_points= n_between*(number_of_grid_points+1);


}

//-------------------------------------------------------
//printing &  getter/setter & and other service functions:
void Line::print() {
    cout <<
            "Point distribution: " << point_distribution << endl <<
            "Measure is: "         << functional_measure << endl <<
            "Grid is: " << endl;

    for (int i = 0; i < number_of_grid_points; ++i) {
        cout << grid[ i] << endl;
    }
}

const string &Line::getPoint_distribution() const {
    return point_distribution;
}

const string &Line::getFunctional_measure() const {
    return functional_measure;
}

double Line::getLower_boundary() const {
    return lower_boundary;
}

double Line::getUpper_boundary() const {
    return upper_boundary;
}

int Line::getNumber_of_grid_points() const {
    return number_of_grid_points;
}

const vector<double> &Line::getGrid() const {
    return grid;
}

const vector<double> &Line::getWeights_integration() const {
    return weights_integration;
}

const vector<double> &Line::getWeights_barycentric() const {
    return weights_barycentric;
}

void Line::print_weights() {

    cout << "weights of integration are: " << endl ;
    for (int i = 0; i < number_of_grid_points; ++i) {
        cout << weights_integration[ i ] << endl;
    }
}

void Line::setLower_boundary(double lower_boundary) {
    Line::lower_boundary = lower_boundary;
}

void Line::setUpper_boundary(double upper_boundary) {
    Line::upper_boundary = upper_boundary;
}

const vector<complex<double>> &Line::getCGrid() const {
    return c_grid;
}

//fill barycentric weights if needed: fill and print.
void Line::fill_barycentric() {

    weights_barycentric.resize( (unsigned long) number_of_grid_points);

    if ( point_distribution == "leg"){

        VecDoub bary_x(number_of_grid_points);
        VecDoub bary_w(number_of_grid_points);

        gauleg(-1.,1., bary_x, bary_w);
        for (int i = 0; i < number_of_grid_points; ++i) {

            weights_barycentric[ i] = pow( -1., i) * sqrt(bary_w[ i] * (1.0-bary_x[ i]*bary_x[ i]));
//            weights_barycentric[ i] = pow( -1., i) * sqrt( weights_integration[ i] *grid[ i]);
        }
//todo: unchecked
   }
    else if( point_distribution == "tscheb"){ assert(false);
        for (int i = 0; i < number_of_grid_points; ++i) {
            weights_barycentric[ i] = pow( -1., i) * sin( (2. * i + 1)/( 2. * (number_of_grid_points -1.) + 2) * M_PI);
        }
//todo: unchecked
    }
    else if (point_distribution == "equal"){ assert(false);
        cout << "barycentric weights not available for equal grid spacing yet" << endl;
        cin.get();
    }
    else{
        assert( false);
    }
}
void Line::print_barycentric_weights() {

    cout << endl << "Barycentric weights are: " << endl ;
    for (int i = 0; i < number_of_grid_points; ++i) {
        cout << weights_barycentric[ i] << endl;
    }

}


//destructor of class:
Line::~Line() {
//    cout << "destroying line!" << endl;
}


//Functions to fill Line values form the outside for the special BSE calculation, extra iteration.
Line::Line(vector<complex<double>>& outside_grid) {
    fill_line_with_data(outside_grid);
}

Line::Line(vector<double>& outside_grid) {
    fill_line_with_data(outside_grid);
}

void Line::fill_line_with_data(vector<complex<double>>& outside_grid) {

    c_grid=outside_grid;
    number_of_grid_points= c_grid.size();

    //Setting all the other things back to no initail value.
    weights_integration=vector<double>();
    weights_barycentric=vector<double>();
    point_distribution= functional_measure=string();
    lower_boundary = upper_boundary=double();
}

void Line::fill_line_with_data(vector<double>& outside_grid) {

    grid=outside_grid;
    number_of_grid_points= grid.size();

    //Setting all the other things back to no initail value.
    weights_integration=vector<double>();
    weights_barycentric=vector<double>();
    point_distribution= functional_measure=string();
    lower_boundary = upper_boundary=double();
}
