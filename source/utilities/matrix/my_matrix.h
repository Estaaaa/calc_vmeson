//
// Created by esther on 08.11.17.
//

#ifndef CALC_VMESON_MY_MATRIX_H
#define CALC_VMESON_MY_MATRIX_H
/**
 * @param[in]	n
 * @param[in]	m
 * @return
 */
template <class T>
T** new2D(unsigned int n, unsigned int m){
    T** array = new T*[n]; // das erzeugt ein Array aus Pointern, die jeweils wieder initialisiert werden muessen
    for(int i=0; i<n; i++){
        array[i] = new T[m]; // jede Spalte des 2D-Arrays muss genauso erzeugt werden wie das Array selbst
        for(int j=0; j<m; j++){
            array[i][j] = 0.0; // und wenn wir schon dabei sind, koennen wir die Spalten auch mit 0 fuellen
        }
    }
    return array;
}

/**
 * @param[in]	n
 * @param[in]	m
 * @return
 */
template <class T>
T** new2D(unsigned int n, unsigned int m, const T fill){
    T** array = new T*[n];
    for(int i=0; i<n; i++){
        array[i] = new T[m];
        for(int j=0; j<m; j++){
            array[i][j] = fill;
        }
    }
    return array;
}


/**
 * @param[in]	n
 * @param[in]	m
 * @param[in]	array
 */
template <class T>
void delete2D(unsigned int n, unsigned int m, T** array){
    for(int i=0; i<n; i++){
        delete[] array[i]; // und jede Spalte muss auch wieder freigegeben werden
    }
    delete[] array;
}

#endif //CALC_VMESON_MY_MATRIX_H
