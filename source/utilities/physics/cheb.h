//
// Created by esther on 20.12.17.
//

#ifndef CALC_VMESON_CHEB_H
#define CALC_VMESON_CHEB_H


#include <cmath>

class cheb {

public:

    cheb(int n_cheb) : n_cheb(n_cheb){};

    cheb(){};

    double get(const int index, const double& angle_z){

        double Chebychev= 0.0;
        if(index==0){Chebychev=1.0/sqrt(2.0);}
        else{Chebychev=cos(double(index)*acos(angle_z));}
//        cout<<"Cheby: "<<Chebychev<<tab<<angle_z<<tab<<index<<endl;

        return Chebychev;

    }

    double get_z(const int& index_cheb){

        return  ( cos((index_cheb-0.5+1.0)*M_PI/(double) n_cheb) );
    }

    double get(const int& i, const int& j){

        double Chebychev= 0.0;
        double angle_z = get_z(j);
        if(i==0){Chebychev=1.0/sqrt(2.0);}
        else{Chebychev=cos(double(i)*acos(angle_z));}

        return Chebychev;

    }

private:

    int n_cheb;

};


#endif //CALC_VMESON_CHEB_H
