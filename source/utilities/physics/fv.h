//
// Created by paul on 11.07.17.
//

#ifndef CALC_BSE_FV_H
#define CALC_BSE_FV_H

#include <typedefs.h>
#include <array>
#include <numeric>
#include <algorithm>

//#include <bits/algorithmfwd.h>
//#include <bits/stl_numeric.h>
//#include <binders.h>



template <class T> class fv {


public:    
    array<T,4> content;

    T operator [](const int at){
        return content[at];
    }

    void set_spherical(T pp, T phi, T y, T z){

        Cdoub p = sqrt( pp);
        content[0] = p * sqrt( 1. - z*z) *  sqrt( 1. - y*y) * sin( phi);
        content[1] = p * sqrt( 1. - z*z) *  sqrt( 1. - y*y) * cos( phi);
        content[2] = p * sqrt( 1. - z*z) *  y;
        content[3] = p * z;
    };

    void get_p_cosphi_y_z( const vector<T> &result){


        T psquared = pp();
        double one_over_psquared = real( 1./sqrt( psquared));

        result[0] =  psquared;
        result[1] =  content[3] * one_over_psquared                                               ;
        result[2] =  one_over_psquared * content[2] / sqrt(1. - (content[3]* content[3]) * one_over_psquared * one_over_psquared) ;
        result[3] =  content[1] * one_over_psquared * 1./sqrt(1. - result[3]*result[3]) * 1./sqrt(1. - result[2]*result[2]) ;
    };

    T pp(){

        return std::inner_product( content.begin(), content.end(), content.begin(), 0);
    }

    friend ostream& operator<<(ostream& os, fv& f)
    {
        os << f[0] << tab << f[1] << tab << f[2] << tab << f[3] <<endl;
        return os;
    }

//    void operator +( const fv& f){
//        for (int i = 0; i < 4; ++i) {
//            content[i] += f.content[i];
//        }
//    }
//
//    T operator %( const T c){
//        for (int i = 0; i < 4; ++i) {
//            content[i] *= c;
//        }
//    }
//
//    T operator *( const fv &f){
//        return std::inner_product( content.begin(), content.end(), f.content.begin(), 0);
//    }
};

/////// * /////////////////////////////////////////
template <class T, class T2> T operator *( const fv<T> &f, const fv<T2> &p){
    T result=0.0;
    for (int i = 0; i < 4; ++i) {
        result += f.content[i]*p.content[i];
    }
    return result;

//        return std::inner_product( p.content.begin(), p.content.end(), f.content.begin(), 0);
    }

template <class T, class T2> fv<T> operator *( const T2 &c ,  const fv<T> &f){
    fv<T> result =f;
    for (int i = 0; i < 4; ++i) {

        result.content[i] *= c;
    }
    return result;

}



/////// % /////////////////////////////////////////
inline fv<Cdoub> operator % (const fv<Cdoub> &A, double c){

    fv<Cdoub> res = A;

    for (int i = 0; i < 4; ++i) {

        res.content[i] *= c;
    }

    return res;
};

inline fv<Cdoub> operator % (double c, const fv<Cdoub> &A){

    fv<Cdoub> res = A;

    for (int i = 0; i < 4; ++i) {

        res.content[i] *= c;
    }

    return res;
};


template <class T> fv<T> operator % (const fv<T> &A, T c){

    fv<T> res = A;

    for (int i = 0; i < 4; ++i) {

        res.content[i] *= c;
    }

    return res;
};

template <class T> fv<T> operator % ( T c, const fv<T> &A){

    fv<T> res = A;

    for (int i = 0; i < 4; ++i) {

        res.content[i] *= c;
    }

    return res;
};
/////////////// +- /////////////////////////////////////////////////////////////////
template <class T, class T2> fv<Cdoub> operator + (const fv<T> &A, const fv<T2> &B){

    fv<Cdoub> res;
    for (int i = 0; i < 4; ++i) {

        res.content[i] = A.content[i] + B.content[i];
    }

    return res;
};


template <class T> fv<T> operator + (const fv<T> &A, const fv<T> &B){

    fv<T> res;
    for (int i = 0; i < 4; ++i) {

      res.content[i] = A.content[i] + B.content[i];
    }

    return res;
};

template <class T> fv<T> operator -(const fv<T> &A, const fv<T> &B){

    fv<T> res;
    for (int i = 0; i < 4; ++i) {

        res.content[i] = A.content[i] -B.content[i];
    }

    return res;
};

template <class T, class T2> fv<Cdoub> operator -(const fv<T> &A,const fv<T2> &B){

    fv<Cdoub> res;
    for (int i = 0; i < 4; ++i) {

        res.content[i] = A.content[i] -B.content[i];
    }

    return res;
};


typedef std::vector< fv<double> > Vecfvdoub;
typedef std::vector< fv<Cdoub> > VecfvCdoub;



#endif //CALC_BSE_FV_H
