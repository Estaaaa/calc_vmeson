//
// Created by esther on 09.10.17.
//

#ifndef CALC_BSE_TIME_H
#define CALC_BSE_TIME_H

#include <typedefs.h>
#include <time.h>
#include "mystream.h"

using namespace std;

template <typename  T> void give_time(const time_t a, const time_t b, const T& name)
{
    cout<<name<<"  :"<<round(difftime(b,a)/(3600))<<"h  , "<<round((difftime(b,a)/(3600)-trunc(difftime(b,a)/3600))*60)<<"m  , "<<
        ((difftime(b,a)/(3600)-trunc(difftime(b,a)/3600))*60-trunc((difftime(b,a)/(3600)-trunc(difftime(b,a)/3600))*60))*60<<"s"<<endl;
}

template <typename  T> void give_time(mstream& mout, const time_t a, const time_t b, const T& name)
{
    mout<<name<<"  :"<<round(difftime(b,a)/(3600))<<"h  , "<<round((difftime(b,a)/(3600)-trunc(difftime(b,a)/3600))*60)<<"m  , "<<
        ((difftime(b,a)/(3600)-trunc(difftime(b,a)/3600))*60-trunc((difftime(b,a)/(3600)-trunc(difftime(b,a)/3600))*60))*60<<"s"<<endl;
}


#endif //CALC_BSE_TIME_H
