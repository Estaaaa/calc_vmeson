//
// Created by paul on 22.08.17.
//

#ifndef CALC_BSE_AMPINTER_H
#define CALC_BSE_AMPINTER_H

#include <Vector_resizer.h>
#include "SuperIndex.h"

/**
 * This class implements an amplitude object f_{i1,..,in}(ix1,...,ixm) i and ix are integers
 * which can on paper also be written as     f_(i1,....,in)(z1, ...zl)
 * with an on board router       R( ix1,...,ixn) -> (z1, ..., zl) where z's are doubles or Cdoubs
 * with an on board interpolator I( z1 ... zl) -> f_i1..in(z1, ...zl)
 * The object can be called with the integers i1...in and ix1...ixn
 * @tparam NumberOfPartialIndices1 (would be n)
 * @tparam NumberOfPartialIndices2 (would be m)
 * @tparam TypeOfNumbersStored double or Cdoub usually
 * @tparam Functor  This is the type of the interpolator passed. I( z1 ... zl) -> f_i1..in(z1, ...zl)
 * @tparam Router   This maps I( z1 ... zl) -> f_i1..in(z1, ...zl)
 * @tparam store_vector if this is true the full f_{i1,..,in}(ix1,...,ixm) is precalculated and stored in a vector
 */
template <int NumberOfPartialIndices1, int NumberOfPartialIndices2, class TypeOfNumbersStored, class Functor, 
        class Router, bool store_vector>
class Amplitude{};

/**
 * This class is used for storage only, f.e. to store the amplitude of a pion(f1,f2,..) or the amplitudes of a quark(A,B)
 * @tparam NumberOfPartialIndices1 i index discrete
 * @tparam NumberOfPartialIndices2 x index continouus
 * @tparam TypeOfNumbersStored double or cdoub
 */
template <int NumberOfPartialIndices1, int NumberOfPartialIndices2, class TypeOfNumbersStored>
class AmplitudeStore {

public:

    /// Constructor:
    /**
     * fill with constant value, no F R passed!!! Use this for f_extern
     * @param s1_discrete_indices
     * @param s2_cont_indices
     * @param set_content_to_value
     */
    AmplitudeStore(const SuperIndex<NumberOfPartialIndices1> &s1_discrete_indices,
               const SuperIndex<NumberOfPartialIndices2> &s2_cont_indices,
               TypeOfNumbersStored set_content_to_value)
            : s1(s1_discrete_indices), s2(s2_cont_indices) {

        Si = new SuperIndex<2>( {s1_discrete_indices.getNsuper_index(), s2_cont_indices.getNsuper_index()});
        Si2 = new SuperIndex<NumberOfPartialIndices1+NumberOfPartialIndices2>(
                combine_arrays(s1_discrete_indices.getMaxima_of_partial_indices(), s2_cont_indices.getMaxima_of_partial_indices()));


        content.resize((unsigned long) Si->getNsuper_index());

        for (int i = 0; i < Si->getNsuper_index(); ++i) {

            content[i] = set_content_to_value;
        }
    }

    /// destructor
    virtual ~AmplitudeStore() {

        delete Si;
        delete Si2;
    }

    /**
     * scalar multiplication of content
     * @param scalar
     */
    void operator * (TypeOfNumbersStored scalar){

        for (int i = 0; i < content.size(); ++i) {
            content[i] *= scalar;
        }
    }

    /**
     * this function should print out the Amplitude but it is not working jet!
     * @param os
     * @param store
     * @return
     */
//    ostream& operator << (ostream &os, const AmplitudeStore<NumberOfPartialIndices1, NumberOfPartialIndices2, TypeOfNumbersStored> & store){
//
//        for (int i = 0; i < s2.getNsuper_index(); ++i) {
//            for (int j = 0; j < s1.getNsuper_index(); ++j) {
//                os <<content[Si->is({j,i})] << tab;
//            }
//            os<<endl;
//        }
//        return os;
//
//    };
    /**
     * returns one specific dressing function at a specific grid value
     * @param i combined super index
     * @return f(i)
     */
    TypeOfNumbersStored& operator ()(const int i){

        return content[i];
    }

    /**
     * returns f_ivec at all grid points x_i
     * @param i_vec is the cont. index (i=0 -> E, i=1, -> F ...)
     * @return returns {f_i(x0) .. f_i(xN)}
     */
    vector<TypeOfNumbersStored> operator [] (const int i_vec) const{

        vector<TypeOfNumbersStored> f_x( s2.getNsuper_index());

            for (int j = 0; j < s2.getNsuper_index(); ++j) {

                f_x[j] = content[ Si->is({i_vec,j})];
            }

        return f_x;
    }


    TypeOfNumbersStored& operator ()(array<int, NumberOfPartialIndices1+ NumberOfPartialIndices2> is){

        return content[ Si2->is( is)];
    }
    TypeOfNumbersStored& operator ()(const int si_discrete, const int si_continuous){

        return this->operator()( Si->is({si_discrete, si_continuous}));
    }
    /**
     * this function returns all dressing functions for a given x grid point
     * @param cont_indices x-indices integers
     * @return f_ijk...(x) - all dressing function!
     */
    vector<TypeOfNumbersStored> operator[]( array<int, NumberOfPartialIndices2> cont_indices) const{

        vector<TypeOfNumbersStored> res(s1.getNsuper_index());

        int si2 = s2.is( cont_indices);

        for (int i = 0; i < s1.getNsuper_index(); ++i) {

            res[i] = ( content[ Si->is( {i, si2})]);
        }

        return res;
        // todo: smarter would be not to copy but return pointers to the parts of content
    }

    //// Get and Set the content vector
    const vector<TypeOfNumbersStored> &getContent() const {

        return content;
    }
    void setContent(const vector<TypeOfNumbersStored> &contentvec) {

        assert( content.size() == contentvec.size());
        AmplitudeStore::content = contentvec;
    }
    void setContent(const TypeOfNumbersStored &value) {

        std::fill(content.begin(), content.end(), value);
    }
    unsigned long getSize() const{
        return (unsigned long) Si->getNsuper_index();
    }
    int gets1() const{
        return s1.getNsuper_index();
    }
    int gets2() const{
        return s2.getNsuper_index();
    }

    SuperIndex<NumberOfPartialIndices2> get_si_2() const{
        return s2;
    }
    /**
     * This function is ment to call the update function on the interpolator
     * @return the adress of the interpolator
     */

private:
    /// s1 discrete ind, (dirac, col, fla), s2 continious ind (momenta, angles) Si combination of s1, s2.
    SuperIndex<NumberOfPartialIndices1> s1;
    SuperIndex<NumberOfPartialIndices2> s2;
    SuperIndex<2> *Si;
    SuperIndex<NumberOfPartialIndices1+NumberOfPartialIndices2> *Si2;

    std::vector< TypeOfNumbersStored>  content;
};

/**
 * This is used in context of a BSE, DSE etc. where object needs to have an interpolator AND storage
 * Comment: Since the interpolators getValue functions want the result vector passed and amplitudes only ever have
 * interpolators as functors, we pass getValue a result container
 * @tparam NumberOfPartialIndices1 
 * @tparam NumberOfPartialIndices2 
 * @tparam TypeOfNumbersStored 
 * @tparam Functor 
 * @tparam Router 
 */
template <int NumberOfPartialIndices1, int NumberOfPartialIndices2, class TypeOfNumbersStored, class Functor, class Router>
class Amplitude<NumberOfPartialIndices1, NumberOfPartialIndices2, TypeOfNumbersStored, Functor, Router, true> {

public:

    /// Constructors:
    /**
     * fill with interpolator
     * @param s1_discrete_indices
     * @param s2_cont_indices
     * @param F
     * @param R
     */
    Amplitude(const SuperIndex<NumberOfPartialIndices1> &s1_discrete_indices,
           const SuperIndex<NumberOfPartialIndices2> &s2_cont_indices,
           const Functor &F,
           const Router  &R)
            : s1(s1_discrete_indices), s2(s2_cont_indices), F(F), R(R) {

        Si = new SuperIndex<2>( {
                s1_discrete_indices.getNsuper_index(),
                s2_cont_indices.getNsuper_index()});

        Si2 = new SuperIndex<NumberOfPartialIndices1+NumberOfPartialIndices2>(
                combine_arrays(s1_discrete_indices.getMaxima_of_partial_indices(), s2_cont_indices.getMaxima_of_partial_indices()));

        content.resize((unsigned long) Si->getNsuper_index());
        fillcontent();

    }

    void fillcontent()  {
        vector<TypeOfNumbersStored> dummie(s1.getMaxima_of_partial_indices()[0]);
        // Interpolate and save precalculated values in vector
        for (int si2 = 0; si2 < s2.getNsuper_index(); ++si2) {

            // MultiDim Interpolator F returns all Values of f(x) for given x gridpoint
            F.getValue( dummie, R.map(s2.are(si2)));

            for (int si1 = 0; si1 < s1.getNsuper_index(); ++si1) {

                content[ Si->is({si1, si2})] = dummie[ si1];
            }
        }

    }

    /**
     * fill with constant value, no F R passed!!! Use this for f_extern
     * @param s1_discrete_indices
     * @param s2_cont_indices
     * @param set_content_to_value
     */
    Amplitude(const SuperIndex<NumberOfPartialIndices1> &s1_discrete_indices,
           const SuperIndex<NumberOfPartialIndices2> &s2_cont_indices,
              const Functor &F,
              const Router  &R,
           TypeOfNumbersStored set_content_to_value)
            : s1(s1_discrete_indices), s2(s2_cont_indices), F(F), R(R) {

        Si = new SuperIndex<2>( {s1_discrete_indices.getNsuper_index(), s2_cont_indices.getNsuper_index()});

        Si2 = new SuperIndex<NumberOfPartialIndices1+NumberOfPartialIndices2>(
                combine_arrays(s1_discrete_indices.getMaxima_of_partial_indices(), s2_cont_indices.getMaxima_of_partial_indices()));

        content.resize((unsigned long) Si->getNsuper_index());

        for (int i = 0; i < Si->getNsuper_index(); ++i) {

            content[i] = set_content_to_value;
        }
    }

    /// destructor
    virtual ~Amplitude() {

        delete Si;
        delete Si2;

    }

    /**
     * scalar multiplication of content
     * @param scalar
     */
    void operator * (TypeOfNumbersStored scalar){

        for (int i = 0; i < content.size(); ++i) {
            content[i] *= scalar;
        }
    }
    /**
     * returns one specific dressing function at a specific grid value
     * @param i combined super index
     * @return f(i)
     */
    const TypeOfNumbersStored& operator ()(const int i) const{

        return content[i];
    }
    const TypeOfNumbersStored& operator ()(array<int, NumberOfPartialIndices1+ NumberOfPartialIndices2> is) const{

        return content[ Si2->is( is)];
    }
    TypeOfNumbersStored& operator ()(const int si_discrete, const int si_continuous) const{

        return this->operator()( Si->is({si_discrete, si_continuous}));
    }
    /**
     * this function returns all dressing functions for a given x grid point
     * @param cont_indices x-indices integers
     * @return f_ijk...(x) - all dressing function!
     */
    vector<TypeOfNumbersStored> operator[]( array<int, NumberOfPartialIndices2> cont_indices) const{

        vector<TypeOfNumbersStored> res(s1.getNsuper_index());

        int si2 = s2.is( cont_indices);

        for (int i = 0; i < s1.getNsuper_index(); ++i) {

            res[i] = ( content[ Si->is( {i, si2})]);
        }

        return res;
        // todo: smarter would be not to copy but return pointers to the parts of content
    }

    //// Get and Set the content vector
    const vector<TypeOfNumbersStored> &getContent() const {

        return content;
    }
    void setContent(const vector<TypeOfNumbersStored> &contentvec) {

        assert( content.size() == contentvec.size());
        Amplitude::content = contentvec;
    }
    void setContent(const TypeOfNumbersStored &value) {

        std::fill(content.begin(), content.end(), value);
    }
    unsigned long getSize() const{
        return (unsigned long) Si->getNsuper_index();
    }
    int gets1() const{
        return s1.getNsuper_index();
    }
    int gets2() const{
        return s2.getNsuper_index();
    }

    SuperIndex<NumberOfPartialIndices2> get_si_2() const{
        return s2;
    }
    /**
     * This function is ment to call the update function on the interpolator
     * @return the adress of the interpolator
     */
    Functor& getF(){
        return F;
    }

private:
    /// s1 discrete ind, (dirac, col, fla), s2 continious ind (momenta, angles) Si combination of s1, s2.
    SuperIndex<NumberOfPartialIndices1> s1;
    SuperIndex<NumberOfPartialIndices2> s2;
    SuperIndex<2> *Si;
    SuperIndex<NumberOfPartialIndices1+NumberOfPartialIndices2> *Si2;

    Functor F; // Interpolator
    Router  R;
    std::vector< TypeOfNumbersStored>  content;
};

/**
 * This is used in context of a BSE, DSE etc. where object needs to have an interpolator WITHOUT storage
 * @tparam NumberOfPartialIndices1 
 * @tparam NumberOfPartialIndices2 
 * @tparam TypeOfNumbersStored 
 * @tparam Functor 
 * @tparam Router 
 */
template <int NumberOfPartialIndices1, int NumberOfPartialIndices2, class TypeOfNumbersStored, class Functor, class Router>
class Amplitude<NumberOfPartialIndices1, NumberOfPartialIndices2, TypeOfNumbersStored, Functor, Router, false> {

public:

    //// Constructor with two super indices (get combined into one later) and a fill functor
    Amplitude(const SuperIndex<NumberOfPartialIndices1> &s1_discrete_indices,
           const SuperIndex<NumberOfPartialIndices2> &s2_cont_indices,
           const Functor &F, const Router &R)
            : s1(s1_discrete_indices), s2(s2_cont_indices), F(F), R(R) {
        assert(false); // todo: verbessere noch () operatoren!
        Si = new SuperIndex<2>( {s1_discrete_indices.getNsuper_index(), s2_cont_indices.getNsuper_index()});
        Si2 = new SuperIndex<NumberOfPartialIndices1+NumberOfPartialIndices2>(
                combine_arrays(s1_discrete_indices.getMaxima_of_partial_indices(), s2_cont_indices.getMaxima_of_partial_indices()));
    }

    /// destructor
    virtual ~Amplitude() {

        delete Si;
        delete Si2;

    }

    unsigned long getSize() const{
        return (unsigned long) Si->getNsuper_index();
    }
    int gets1() const{
        return s1.getNsuper_index();
    }
    int gets2() const{
        return s2.getNsuper_index();
    }

    SuperIndex<NumberOfPartialIndices2> get_si_2(){
        return s2;
    }

    Functor& getF(){
        return F;
    }

private:
    /// s1 discrete ind, (dirac, col, fla), s2 continious ind (momenta, angles) Si combination of s1, s2.
    SuperIndex<NumberOfPartialIndices1> s1;
    SuperIndex<NumberOfPartialIndices2> s2;
    SuperIndex<2> *Si;
    SuperIndex<NumberOfPartialIndices1+NumberOfPartialIndices2> *Si2;


    Functor F;      // could be interpolator
    Router  R;
};
#endif //CALC_BSE_AMPINTER_H
