//
// Created by esther on 24.10.17.
//

#ifndef CALC_VMESON_CONTAINER_H
#define CALC_VMESON_CONTAINER_H


#include "SuperIndex.h"

template <int NumberofPartialIndices, class TypeOfStored, bool stored > class Container {};

template <int NumberofPartialIndices, class TypeOfStored >
class Container<int NumberofPartialIndices, class TypeOfStored, bool true > {

public:

    Container(const array<int, NumberofPartialIndices> SuperIndexInit): Si(SuperIndexInit){

    }



    //Getter&Setter
    const SuperIndex<NumberofPartialIndices> &getSi() const {
        return Si;
    }

    void setSi(const SuperIndex<NumberofPartialIndices> &Si) {
        Container::Si = Si;
    }

    const vector<TypeOfStored> &getContent() const {
        return content;
    }

    void setContent(const vector<TypeOfStored> &content) {
        Container::content = content;
    }

private:

    SuperIndex<NumberofPartialIndices> Si;
    std::vector<TypeOfStored> content;

};


#endif //CALC_VMESON_CONTAINER_H
