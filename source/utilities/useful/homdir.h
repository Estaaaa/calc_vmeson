//
// Created by Esther Weil on 24.04.18.
//

#ifndef CALC_VMESON_HOMDIR_H
#define CALC_VMESON_HOMDIR_H

#include <typedefs.h>
#include <assert.h>
#include <vector>
#include <string>
#include <limits.h>
#include <unistd.h>
#include <libgen.h>



std::string getexepath()
{
    char result[ PATH_MAX ];
    ssize_t count = readlink( "/proc/self/exe", result, PATH_MAX );
//    return std::string( result, (count > 0) ? count : 0 );


    const char *path;
    if (count != -1) {
        path = dirname(result);
    }
    return path;

}

std::string gethomdir()
{
    string file_path = __FILE__;
    string dir_path = file_path.substr(0, file_path.rfind("calc_vmeson"))+"calc_vmeson/";


    return dir_path;

}






#endif //CALC_VMESON_HOMDIR_H
