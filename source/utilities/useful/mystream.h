//
// Created by esther on 07.08.18.
//

#ifndef CALC_VMESON_MYSTREAM_H
#define CALC_VMESON_MYSTREAM_H

#include <typedefs.h>
#include <fstream>

class mstream
{
public:

    mstream(string folder): coss(folder){};

    ofstream coss;
//    ~mstream(void);


    mstream& operator<< (ostream& (*pfun)(ostream&))
    {
        pfun(coss);
        pfun(cout);
        return *this;
    }
};

template <class T>
mstream& operator<< (mstream& st, T val)
{
    st.coss << val;
    cout << val;
    return st;
};





#endif //CALC_VMESON_MYSTREAM_H
