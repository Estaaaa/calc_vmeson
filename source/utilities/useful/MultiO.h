//
// Created by esther on 14.08.17.
//

#ifndef CALC_BSE_MULTIO_H
#define CALC_BSE_MULTIO_H

#include <Vector_resizer.h>
#include "SuperIndex.h"

/**
 * This class is used for Kernels, etc. that store a vector of matrices, vectors, etc. not just doubles or Cdoubs
 * @tparam NumberOfPartialIndices
 * @tparam TypeOfNumbersStored
 * @tparam Functor
 * @tparam Router
 * @tparam store_vector
 */

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <int NumberOfPartialIndices, class TypeOfNumbersStored, class Functor, class Router, bool store_vector>
class MultiO{

};


/// implementation for store_vector = true
template <int NumberOfPartialIndices, class TypeOfNumbersStored, class Functor, class Router>
class MultiO< NumberOfPartialIndices, TypeOfNumbersStored, Functor, Router, true> {

public:

    //// Constructor with two super indices (get combined into one later) and a fill functor
    MultiO(const SuperIndex<NumberOfPartialIndices> &Si,
           const Functor &F,
           const Router  &R)
            : Si(Si) {

        content.resize((unsigned long) Si.getNsuper_index());

        for (int i = 0; i < Si.getNsuper_index(); ++i) {

            content[i] = F.getValue( R.map(Si.are( i)));
        }
    }

    /// destructor
    virtual ~MultiO() {

        cout<<"destroying MultiO"<<endl;
    }

    /// operators return content, because it was prefilled in the constructor
    void operator * (TypeOfNumbersStored scalar){

        for (int i = 0; i < content.size(); ++i) {
            content[i] *= scalar;
        }
    }
    const TypeOfNumbersStored& operator ()(const int i) const{

        return content[i];
    }
    const TypeOfNumbersStored& operator ()(array<int,NumberOfPartialIndices> is) const{

        return content[ Si.is( is)];
    }
    TypeOfNumbersStored& operator ()(const int si_discrete, const int si_continuous) const{

        return this->operator()( Si.is({si_discrete, si_continuous}));
    }


    //// Get and Set the content vector
    const vector<TypeOfNumbersStored> &getContent() const {

        return content;
    }
    void setContent(const vector<TypeOfNumbersStored> &contentvec) {

        assert( content.size() == contentvec.size());
        MultiO::content = contentvec;
    }
    void setContent(const TypeOfNumbersStored &value) {

        std::fill(content.begin(), content.end(), value);
    }

    unsigned long getSize() const{
        return (unsigned long) Si.getNsuper_index();
    }


    const SuperIndex<NumberOfPartialIndices> &getSi() const {
        return Si;
    }

//    Functor &getFunctor(){
//        return F;
//    }

private:
    SuperIndex<NumberOfPartialIndices> Si;

//    Functor F;      // could be interpolator
//    Router  R;
    std::vector< TypeOfNumbersStored>  content;
};


/// implementation for store_vector = false
template <int NumberOfPartialIndices, class TypeOfNumbersStored, class Functor, class Router>
class MultiO< NumberOfPartialIndices, TypeOfNumbersStored, Functor, Router, false> {

public:

    //// Constructor with two super indices (get combined into one later) and a fill functor
    MultiO(const SuperIndex<NumberOfPartialIndices> &Si,
           const Functor &F,
           const Router  &R)
            : Si(Si), F(F), R(R) {}

    /// operators has to calculate the value, cause it was not prefilled in the constructor.
    TypeOfNumbersStored operator ()(array<int, NumberOfPartialIndices> is){

        return  F.getValue( R.map(is));
    }
    TypeOfNumbersStored operator ()(const int i){

        return this->operator()(Si.are(i));
       // return F.getValue( R.map(Si.are( i)));
    }




    //// Get and Set the content vector
    unsigned long getSize() const{
        return (unsigned long) Si.getNsuper_index();
    }

    const SuperIndex<NumberOfPartialIndices> &getSi() const {
        return Si;
    }

    Functor& getFunctor(){
        return F;
    }


private:
    SuperIndex<NumberOfPartialIndices> Si;

    Functor F;      // could be interpolator
    Router  R;
};



#endif //CALC_BSE_MULTIO_H
