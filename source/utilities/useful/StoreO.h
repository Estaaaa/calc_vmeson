//
// Created by esther on 24.10.17.
//

#ifndef CALC_VMESON_STOREO_H
#define CALC_VMESON_STOREO_H


#include "SuperIndex.h"

template <int NumberofPartialIndices, class TypeOfStored > class StoreO {

public:

    StoreO(const array<int, NumberofPartialIndices> SuperIndexInit): Si(SuperIndexInit){

    }



    //Getter&Setter
    const SuperIndex<NumberofPartialIndices> &getSi() const {
        return Si;
    }

    void setSi(const SuperIndex<NumberofPartialIndices> &Si) {
        StoreO::Si = Si;
    }

    const vector<TypeOfStored> &getContent() const {
        return content;
    }

    void setContent(const vector<TypeOfStored> &content) {
        StoreO::content = content;
    }

private:

    SuperIndex<NumberofPartialIndices> Si;
    std::vector<TypeOfStored> content;

};


#endif //CALC_VMESON_STOREO_H
