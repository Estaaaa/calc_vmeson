//
// Created by paul on 07.07.17.
//

#ifndef CALC_BSE_SUPERINDEX_H
#define CALC_BSE_SUPERINDEX_H


#include <iostream>
#include <complex>
#include <assert.h>
#include <numeric>
#include <vector>
#include <array>

typedef std::complex<double> Cdoub;

using namespace std;


#define CONTROL_SIS
#define OUT_OF_BOUNDS_CHECK

//comment:
//For the Superindex. It is enrolled such that the first index is the most inner in a loop.



/// create a super index that can roal and unroal itself fast.
template <int N_partial_indices> class SuperIndex{


public:
    //// constructor with an array of the maxima the partial indices will have
//    SuperIndex(){};

    SuperIndex(array<int, N_partial_indices> maxima_of_partial_indices) : maxima_of_partial_indices(maxima_of_partial_indices) {

        fill_multiply_to_get_si();
        calc_NSI();
    }

    //setter
//    void setmaxima_of_partial_indices(array<int, N_partial_indices> &maxima_of_partial_indices){
//
//        maxima_of_partial_indices=maxima_of_partial_indices; cout<<maxima_of_partial_indices[1]<<endl;
//
//
//        fill_multiply_to_get_si();
//
//
//        calc_NSI();
//    }

    /// getters
    const int getNPartial_indices() const{
        return N_partial_indices;
    }

    const int getNsuper_index() const {
        return Nsuper_index;
    }

    const array<int, N_partial_indices> &getMaxima_of_partial_indices() const {
        return maxima_of_partial_indices;
    }

    /// role partial indices to si
    int is(const array<int, N_partial_indices> &partial_inds) const{

#ifdef CONTROL_SIS
        assert( partial_inds.size() == multiply_to_get_si.size());
#endif
#ifdef OUT_OF_BOUNDS_CHECK
        for (int i = 0; i < N_partial_indices; ++i) {
            assert(partial_inds[i] >= 0);
            assert(partial_inds[i] < maxima_of_partial_indices[i]);
        }
#endif

        return std::inner_product( partial_inds.begin(), partial_inds.end(), multiply_to_get_si.begin(), 0);
    }

    /// unroll si to partial indices
    array<int, N_partial_indices> are(const int super_index) const {
//   void are(const int &super_index, array<int, N_partial_indices> &partial_indices) {

        array<int, N_partial_indices> partial_indices;
        for(int s = 0; s <N_partial_indices; s++)
        {
            partial_indices[s]= ( super_index / multiply_to_get_si[s] ) % maxima_of_partial_indices[s];
        }


        return partial_indices;
    }

    friend ostream &operator<<(ostream &os, const SuperIndex &index) {
        os << "Nsuper_index: " << index.Nsuper_index << " maxima_of_partial_indices: "
           << index.maxima_of_partial_indices << " multiply_to_get_si: " << index.multiply_to_get_si;
        return os;
    }

private:
    void calc_NSI() {

        Nsuper_index = 1;

        for (int i = 0; i < N_partial_indices; ++i) {

            Nsuper_index *= maxima_of_partial_indices[i];
        }
    }
    void fill_multiply_to_get_si(){

        int product = 1;

        for (int j = 0; j < N_partial_indices; ++j) {

            multiply_to_get_si[j] = product;
            product *= maxima_of_partial_indices.at( j);
        }
    }

    int Nsuper_index;

    array<int, N_partial_indices> maxima_of_partial_indices;
    array<int, N_partial_indices> multiply_to_get_si;


};


/// run this one for a simple test of the si class
//void test_SupIndex() {
//    array<int,4> bla= {4, 2, 5, 1}, pis;
//    SuperIndex<4> Sind( bla);
//
//    vector<int> collect_sis;
//
//    cout << "partial indices ||| SI from aprtial |||  partial indices" << endl;
//    for (int i = 0; i < 4; ++i) {
//        for (int j = 0; j < 2; ++j) {
//            for (int k = 0; k < 5; ++k) {
//                for (int l = 0; l < 1; ++l) {
//
//                    // collect indices
//                    pis = {i, j, k, l};
//                    // call super index
//                    Sind.is(pis );
//
//                    cout << i << ", " << j << ", " << ", " << k << ", " << l << " ||| ";
//                    cout << Sind.is(pis ) << " ||| ";
//
//                    for (int m = 0; m < 4; ++m) {
//                        cout << Sind.are(Sind.is(pis))[m] << ", ";
//                        assert(pis[m] == Sind.are(Sind.is(pis))[m] );
//                    }
//                    cout << endl;
//
//
//                    collect_sis.push_back(Sind.is(pis ));
//
//                }
//            }
//        }
//    }
//
//
//    for (int i1 = 0; i1 < Sind.getNsuper_index(); ++i1) {
//
//        Sind.are(i1);
//    }
//
//    sort(collect_sis.begin(), collect_sis.end());
//
//    for (int n = 0; n < Sind.getNsuper_index(); ++n) {
//
//        assert( n == collect_sis[n] );
//    }
//}


#endif //CALC_BSE_SUPERINDEX_H
