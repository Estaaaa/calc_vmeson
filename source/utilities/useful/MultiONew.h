//
// Created by esther on 14.08.17.
//

#ifndef CALC_BSE_MULTIONEW_H
#define CALC_BSE_MULTIONEW_H

#include <Vector_resizer.h>
#include "SuperIndex.h"

/**
 * This class is used for Kernels, etc. that store a vector of matrices, vectors, etc. not just doubles or Cdoubs
 * @tparam NumberOfPartialIndices
 * @tparam TypeOfNumbersStored
 * @tparam Functor
 * @tparam Router
 * @tparam store_vector
 */

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <int NumberOfPartialIndices, class TypeOfNumbersStored, class Functor, bool store_vector>
class MultiONew{

};


/// implementation for store_vector = true
template <int NumberOfPartialIndices, class TypeOfNumbersStored, class Functor>
class MultiONew< NumberOfPartialIndices, TypeOfNumbersStored, Functor, true> {

public:

    //// Constructor with two super indices (get combined into one later) and a fill functor
    template<class Router> MultiONew(const SuperIndex<NumberOfPartialIndices> &Si,
           const Functor &F,
           const Router  &R)
            : Si(Si) {

        content.resize((unsigned long) Si.getNsuper_index());

        for (int i = 0; i < Si.getNsuper_index(); ++i) {

            content[i] = F.getValue( R.map(Si.are( i)));
        }
    }

    /// destructor
    virtual ~MultiONew() {

        content.clear();
        cout<<"destroying MultiONew"<<endl;
    }

    /// operators return content, because it was prefilled in the constructor
    void operator * (TypeOfNumbersStored scalar){

        for (int i = 0; i < content.size(); ++i) {
            content[i] *= scalar;
        }
    }
    const TypeOfNumbersStored& operator ()(const int i) const{

        return content[i];
    }
    const TypeOfNumbersStored& operator ()(array<int,NumberOfPartialIndices> is) const{

        return content[ Si.is( is)];
    }
    TypeOfNumbersStored& operator ()(const int si_discrete, const int si_continuous) const{

        return this->operator()( Si.is({si_discrete, si_continuous}));
    }


    //// Get and Set the content vector
    const vector<TypeOfNumbersStored> &getContent() const {

        return content;
    }
    void setContent(const vector<TypeOfNumbersStored> &contentvec) {

        assert( content.size() == contentvec.size());
        MultiONew::content = contentvec;
    }
    void setContent(const TypeOfNumbersStored &value) {

        std::fill(content.begin(), content.end(), value);
    }

    unsigned long getSize() const{
        return (unsigned long) Si.getNsuper_index();
    }


    const SuperIndex<NumberOfPartialIndices> &getSi() const {
        return Si;
    }

//    Functor &getFunctor(){
//        return F;
//    }

    template< class Router> void write(string filename, const Router &R) {}
//    template <int NumberOfPartialIndices, class TypeOfNumbersStored, class Functor>

private:
    SuperIndex<NumberOfPartialIndices> Si;
    std::vector< TypeOfNumbersStored>  content;
};

//Implementation for the quarks in particular. This whole struct need to be over thought.
template <int NumberOfPartialIndices, class Functor>
class MultiONew<NumberOfPartialIndices, array<Cdoub,2>, Functor,true>{

public:

    //// Constructor with two super indices (get combined into one later) and a fill functor
    MultiONew(const SuperIndex<NumberOfPartialIndices> &Si)
            : Si(Si) {

        content.resize((unsigned long) Si.getNsuper_index());
    }

//    template<class Router> MultiONew(const SuperIndex<NumberOfPartialIndices> &Si,
//                                     const Functor &F,
//                                     const Router  &R)
//            : Si(Si) {
//
//        content.resize((unsigned long) Si.getNsuper_index());
//
//    #pragma omp parallel for
//        for (int i = 0; i < Si.getNsuper_index(); ++i) {
//
//            content[i] = F.getValue( R.map(Si.are( i)));
//        }
//    }
//

    template<class Router> void calc( const Functor &F,
               const Router  &R){

        #pragma omp parallel for
        for (int i = 0; i < Si.getNsuper_index(); ++i) {

            content[i] = F.getValue( R.map(Si.are( i)));
        }

    }

    /// destructor
    virtual ~MultiONew() {

        content.clear();
//        cout<<"destroying MultiONew"<<endl;
    }

    /// operators return content, because it was prefilled in the constructor
    void operator * (array<Cdoub,2> scalar){

        for (int i = 0; i < content.size(); ++i) {
            content[i][0] *= scalar[0];
            content[i][1] *= scalar[1];
        }
    }
    const array<Cdoub,2>& operator ()(const int i) const{

        return content[i];
    }
    const array<Cdoub,2>& operator ()(array<int,NumberOfPartialIndices> is) const{
        
        return content[ Si.is( is)];
    }
//    array<Cdoub,2>& operator ()(const int si_discrete, const int si_continuous) const{
//
//        return this->operator()( Si.is({si_discrete, si_continuous}));
//    }


    //// Get and Set the content vector
    const vector<array<Cdoub,2>> &getContent() const {

        return content;
    }
    void setContent(const vector<array<Cdoub,2>> &contentvec) {

        assert( content.size() == contentvec.size());
        MultiONew::content = contentvec;
    }
    void setContent(const array<Cdoub,2> &value) {

        std::fill(content.begin(), content.end(), value);
    }

    unsigned long getSize() const{
        return (unsigned long) Si.getNsuper_index();
    }


    const SuperIndex<NumberOfPartialIndices> &getSi() const {
        return Si;
    }


    template< class Router> void write(string filename, const Router &R)
    {
        ofstream write(filename);
        assert( write.is_open());
        int n = R.getWhich_scalar_prod().size();

        for (int j = 0; j < Si.getNsuper_index(); ++j) {

            auto interm = R.map(Si.are( j));

            for (int i = 0; i < n; ++i) {

                write<<scientific<<setprecision(10)<<real(interm[i])<<tab<<imag(interm[i])<<tab;
            }

            write<<scientific<<setprecision(10)<<real(content[j][0])<<tab<<imag(content[j][0])<<tab
                    <<real(content[j][1])<<tab<<imag(content[j][1])<<endl;

        }
        write<<endl;
        write.close();

    }

    void read(string filename){

        double Rgrid,Igrid;
        double RAfunc, IAfunc,  RBfunc, IBfunc;
        array<Cdoub,2> RIquark;

        content.resize(0);
        ifstream read;
        read.open(filename, ios::in);           //DSEreal
        if(read.is_open())
        {
            string daten;
            while(getline(read,daten))
            {

                if(daten=="") {continue;}
                stringstream(daten) >> Rgrid >> Igrid >> RAfunc >> IAfunc >> RBfunc >> IBfunc;
                if(daten=="") {continue;}

                RIquark[0] =  RAfunc+ i_* IAfunc;
                RIquark[1] =  RBfunc + i_ * IBfunc;
                content.push_back(RIquark);

            }
        }else{cout<<"Comquark::readIn: No data file with real quark found!"<<endl; assert( false);}
        read.close();

        cout<<"A Cquark propagator has been read inn with: n_total="<<content.size()<<" @"<<filename<<endl;
    }




private:
    SuperIndex<NumberOfPartialIndices> Si;
    std::vector< array<Cdoub,2>>  content;


};


//extra function for writing out two quark into one file.
template <int NumberOfPartialIndices, class Functor1, class Functor2, class Router1, class Router2>
void write_out_2_quarks(MultiONew<NumberOfPartialIndices, array<Cdoub,2>, Functor1,true>& quark1,
                        MultiONew<NumberOfPartialIndices, array<Cdoub,2>, Functor2,true>& quark2,
                        const Router1& rq1, const Router2& rq2, string filename ){

        ofstream write(filename);
        assert( write.is_open());
        int n = rq1.getWhich_scalar_prod().size();

    write<<"#p+²    p-²     (p+²- p-²)  Ap  Bp  Am  Bm"<<endl<<endl;

        for (int j = 0; j < quark1.getSi().getNsuper_index(); ++j) {

            auto interm1 = rq1.map(quark1.getSi().are( j));
            auto interm2 = rq2.map(quark1.getSi().are( j));

            Cdoub SigmaA = (quark1.getContent()[j][0]+ quark2.getContent()[j][0])/2.0;
            Cdoub DeltaA = 2.0* (quark1.getContent()[j][0]- quark2.getContent()[j][0])/(interm1[0]-interm2[0]);
            Cdoub DeltaB = 2.0* i_* (quark1.getContent()[j][1]- quark2.getContent()[j][1])/(interm1[0]-interm2[0]);

            for (int i = 0; i < n; ++i) {

                write<<scientific<<setprecision(10)<<real(interm1[i])<<tab<<imag(interm1[i])<<tab
                     <<real(interm2[i])<<tab<<imag(interm2[i])<<tab;
            }

            write<<scientific<<setprecision(10)<<real(quark1.getContent()[j][0])<<tab<<imag(quark1.getContent()[j][0])<<tab
                 <<real(quark1.getContent()[j][1])<<tab<<imag(quark1.getContent()[j][1])<<tab
                 <<real(quark2.getContent()[j][0])<<tab<<imag(quark2.getContent()[j][0])<<tab
                 <<real(quark2.getContent()[j][1])<<tab<<imag(quark2.getContent()[j][1])<<tab
                 <<real(SigmaA) << tab << imag(SigmaA) << tab << real(DeltaA) << tab << imag(DeltaA) << tab
                 <<real(DeltaB) << tab << imag(DeltaB)<<endl;

        }
        write<<endl;
        write.close();

    cout<<"Both quark written into file."<<endl;


};




/// implementation for store_vector = false
template <int NumberOfPartialIndices,  class TypeOfNumbersStored, class Functor>
class MultiONew< NumberOfPartialIndices, TypeOfNumbersStored, Functor, false> {

public:

    //// Constructor with two super indices (get combined into one later) and a fill functor
    MultiONew(const Functor &F)
            : F(F) {}

    /// operators return content, because it was prefilled in the constructor
    template <size_t NumberofScalarP> TypeOfNumbersStored operator ()(array<Cdoub, NumberofScalarP> scalarp){

        return  F.getValue( scalarp);
    }



    //// Get and Set the content vector
//    unsigned long getSize() const{
//        return (unsigned long) Si.getNsuper_index();
//    }
//
//    const SuperIndex<NumberOfPartialIndices> &getSi() const {
//        return Si;
//    }

    Functor& getFunctor(){
        return F;
    }


private:
//    SuperIndex<NumberOfPartialIndices> Si;
    Functor F;      // could be interpolator
};




#endif //CALC_BSE_MULTIONEW_H
