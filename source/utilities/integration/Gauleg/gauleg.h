//
// Created by paul on 13/04/16.
//

#ifndef CALC_BSE_GAULEG
#define CALC_BSE_GAULEG


#include <vector>
#include "../../../types/typedefs.h"

#define K_EPS 3.0e-14


// for gauleg_log do integral f = f(x) w(x) x
// functional determinant not included in weights
void gauleg_log(double x1, double x2, VecDoub &x, VecDoub &w);

// for gauleg do integral f = f(x) w(x)
// new version from internet
void gauleg(double x1, double x2, VecDoub &x, VecDoub &w);


#endif
