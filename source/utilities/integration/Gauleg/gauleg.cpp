//
// Created by paul on 03/05/16.
//

#include <math.h>
#include <stdlib.h>
#include "gauleg.h"

#define K_EPS 3.0e-14

// this is the routine with better result for linear integration
void gauleg_log(double x1, double x2, VecDoub &x, VecDoub &w)
{
    int n = (int) x.size();
    int m,j,i;
    double z1,z,xm,xl,pp,p3,p2,p1;
    m=(n+1)/2;

    for (i=0;i<m;i++) {		//calculate zeros (only half of it, because P_n(z) = (-1)^n P_n(-z))
        z=cos(M_PI*(i+0.75)/(n+0.5));	// estimate for i`th zero (starting value for Newton)
        do {	// Newtons algorithm
            p1=1.0;			// calculate P_n(z)
            p2=0.0;
            for (j=0;j<n;j++) {
                p3=p2;
                p2=p1;
                p1=((2.0*j+1.0)*z*p2-j*p3)/(j+1);
            }
            pp=n*(z*p1-p2)/(z*z-1.0); // derivative
            z1=z;
            z=z1-p1/pp;
        } while (fabs(z-z1) > K_EPS);
        x[i]=pow(x2,-z*0.5+0.5)*pow(x1,z*0.5+0.5);
        x[n-1-i]=pow(x1,-z*0.5+0.5)*pow(x2,z*0.5+0.5);
        w[i]=   2.0/((1.0-z*z)*pp*pp)* 0.5*(log(x2)-log(x1)) ;
        w[n-1-i]=w[i];
    }
}


void gauleg(double x1, double x2, VecDoub &x, VecDoub &w) {
    double z1,z,xm,xl,pp,p3,p2,p1;
    int n= (int) x.size();
    int m=(n+1)/2;
    xm=0.5*(x2+x1);
    xl=0.5*(x2-x1);

    for (int i=0; i<m;i++) {		//calculate zeros (only half of it, because P_n(z) = (-1)^n P_n(-z))
        z=cos(M_PI*(i+0.75)/(n+0.5));	// estimate for i`th zero (starting value for Newton)
        do {	// Newtons algorithm
            p1=1.0;			// calculate P_n(z)
            p2=0.0;
            for (int j=0; j<n;j++) {
                p3=p2;
                p2=p1;
                p1=((2.0*j+1.0)*z*p2-j*p3)/(j+1);
            }
            pp=n*(z*p1-p2)/(z*z-1.0); // derivative
            z1=z;
            z=z1-p1/pp;
        } while (fabs(z-z1) > K_EPS);
        x[i]=xm-xl*z;
        x[n-1-i]=xm+xl*z;
        w[i]=2.0*xl/((1.0-z*z)*pp*pp);
        w[n-1-i]=w[i];
    }
}



#undef K_EPS
