//
// Created by paul on 11.07.17.
//

#ifndef CALC_BSE_VECTOR_RESIZER_H
#define CALC_BSE_VECTOR_RESIZER_H


#include <algorithm>
#include <iostream>
#include <iterator>
#include <array>


using namespace std;

//template<typename T> void  Vector_Resize(T & Storage, int extension_actual){
//    Storage.resize(extension_actual);
//}
//template<typename T, typename  ... Extensions> void  Vector_Resize(T & Storage, int extension_actual, Extensions... extensions){
//    Storage.resize(extension_actual);
//    for (int i = 0; i < extension_actual; ++i)
//    {
//        Vector_Resize(Storage[i],  extensions ... );
//    }
//}

template<typename T> void printVector(const T& t) {
    std::copy(t.cbegin(), t.cend(), std::ostream_iterator<typename T::value_type>(std::cout, ", "));
    cout << endl;
}
template<typename T> void printVectorInVector(const T& t) {
    std::for_each(t.cbegin(), t.cend(), printVector<typename T::value_type>);
}


template <class T, unsigned long N1, unsigned long N2>
array<T, N1 + N2> combine_arrays(const array<T, N1> &a1, const array<T, N2> &a2) {

    std::array<T, N1 + N2> res;

    for (int i = 0; i < N1; ++i) {
        res[i] = a1[i];
    }
    for (int j = 0; j < N2; ++j) {
        res[N1 + j] = a2[j];
    }
    return res;
}

#endif //CALC_BSE_VECTOR_RESIZER_H
