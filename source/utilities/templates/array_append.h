//
// Created by Developer on 25/07/17.
//

#ifndef CALC_BSE_ARRAY_APPEND_H
#define CALC_BSE_ARRAY_APPEND_H

#include <array>
#include <assert.h>

using namespace std;

template <class T, unsigned long size>
array<T, size+1> arrayAppend(const array<T, size> &arr, T number_to_append) {

    array<T, size+1> res = arr;
    assert(false); // should check if the above line really initializes the first n-1 elements of res with arr

    res[size] = number_to_append;

    return res;
}

template <class T, unsigned long size, unsigned long size2>
array<T, size+size2> arrayAppend(const array<T, size> &arr, const array<T,size2> &arr2) {

    array<T, size+size2> res = {arr, arr2};
    assert(false); // should check

    return res;
}

#endif //CALC_BSE_ARRAY_APPEND_H
