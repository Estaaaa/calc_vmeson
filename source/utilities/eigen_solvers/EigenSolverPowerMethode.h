//
// Created by Developer on 25/07/17.
//

#ifndef CALC_BSE_EIGENSOLVERPOWERMETHODE_H
#define CALC_BSE_EIGENSOLVERPOWERMETHODE_H

#include "BaseSolver.h"
#include <math.h>
#include <array>
#include <algorithm>
#include <iomanip>
#include <assert.h>
//#include <BaseInterpolPE.h>

using namespace std;


//#define K_POWER_METHOD_DEBUG




/// implements a power methode. Create an instance of this object, pass it an update vector routiene and an initial vector.
template <typename Functor>
class EigenSolverPowerMethode : public IterativeMatrixSolver<Functor> {

public:
    /// der letzte Vector in der Uebergabe ist der startguess und wird durch iteriert!
    EigenSolverPowerMethode(Functor &f, void (Functor::*updateVector)(const VecCdoub &, VecCdoub &), const int size_eigenvector)
            : IterativeMatrixSolver<Functor>(f), updateVector(updateVector), size_eigenvector(size_eigenvector) {
        this->eigenvector.resize(size_eigenvector);
    }

    void solve( double tolerance = 1.e-10, int max_iterations = 100){

        double current_tolerance=100;
        VecCdoub eigenvector_plus_one;
        eigenvector_plus_one.resize(this->size_eigenvector);
        Cdoub eigenvalue_plus_one=0.0;
        int steps=0;

        set_startguess();



        while (current_tolerance> tolerance)
        {

            steps++;

            (this->f.*this->updateVector)(this->eigenvector, eigenvector_plus_one);


            eigenvalue_plus_one=calc_eigenwert(this->eigenvector,eigenvector_plus_one);

            current_tolerance= convergence_check(biggest_eigenvalue, eigenvalue_plus_one);

            norm_Eigenvector(eigenvector_plus_one);

            this->eigenvector=eigenvector_plus_one;
            biggest_eigenvalue=eigenvalue_plus_one;

#ifdef K_POWER_METHOD_DEBUG
            //More cout for debugging or more detail purposes.
            cout << "////////////////////////////////////" << endl ;
            cout << " This round " << steps << endl;
            cout << " abs( lambda) : " << abs(biggest_eigenvalue) << endl;
            cout << " lambda (Reyleigh Quot): " << scientific << biggest_eigenvalue << endl;
//            cout << " mass is:"  << Mpi << endl;
            cout << "////////////////////////////////////" << endl << endl;
#endif

        }

        cout<<setprecision(12)<<"Eigenvalue calc stopped after ["<<steps<<"] iterations."<<endl<<" The biggest eigenvalue is: "
                <<biggest_eigenvalue<<endl;


        this->f.meson.setEigenvalue(biggest_eigenvalue);
        this->f.meson.setAmplitude(this->eigenvector);
    }

    const VecCdoub &getEigenvector() const {
        return this->eigenvector;
    }

    Cdoub getBiggest_eigenvalue() const {
        return biggest_eigenvalue;
    }

private:

    void set_startguess(){

        biggest_eigenvalue = 0.0;
        /// Der eigenvector wird jetzt von aussen uebergeben
        VecCdoub initial(this->size_eigenvector , 1.0);
        this->eigenvector=initial;
    }

    void norm_Eigenvector(VecCdoub &n){
        //Here any norm can be used. I choose to normalize to the first value of the first amplitude, such that this starts at 1.
        Cdoub norm = n[0];

        for (int i = 0, n_size=n.size(); i <n_size ; ++i) {
            n[i]=n[i]/norm;
        }
    }

    Cdoub calc_eigenwert(const VecCdoub &n, const VecCdoub & n_plus_one ){

        Cdoub n_nstart=0.0;
        Cdoub nstar_n_plus_one=0.0;

        for (int i = 0, n_size=n.size(); i <n_size ; ++i) {

            n_nstart += conj(n[i])*n[i];
            nstar_n_plus_one += conj(n[i])*n_plus_one[i];

        }

        return (nstar_n_plus_one/n_nstart);
    }

    double convergence_check(Cdoub oldval, Cdoub newval){

        return std::abs((oldval-newval)/(oldval+newval));
    }



    Cdoub biggest_eigenvalue;
    int size_eigenvector;
    void (Functor::*updateVector)( const VecCdoub &, VecCdoub &);

};


// todo: Has wrong sign in imaginary part for easy test example!!
template <typename Functor>
class EigenSolverPowerMethodePaul : public IterativeMatrixSolver<Functor> {

public:
    /// der letzte Vector in der Uebergabe ist der startguess und wird durch iteriert!
    EigenSolverPowerMethodePaul(Functor &f, void (Functor::*updateVector)(const VecCdoub &, VecCdoub &), const int size_eigenvector)
            : IterativeMatrixSolver<Functor>(f), updateVector(updateVector), size_eigenvector(size_eigenvector) {
        this->eigenvector.resize(size_eigenvector);
    }

    void solve( double tolerance = 1.e-10, int max_iterations = 100) {

        assert(false); // see todo above
        VecCdoub eigenvector_plus_one( size_eigenvector);
        set_startguess(); // todo: was originally set_sg_meson

        int run_number = 0;
        double epsilon = 100.;
        double largest_ev = 11.;
        bool go_on = true;
        Cdoub Rey = 0.;
        while (go_on) {

            run_number++;

            std::fill(eigenvector_plus_one.begin(), eigenvector_plus_one.end(), 0.);


/////////////////////////////
            (this->f.*this->updateVector)(this->eigenvector, eigenvector_plus_one);

//            this->f.*this->updateVector(this->eigenvector, eigenvector_plus_one);
/////////////////////////////

            double norm = vec_norm(eigenvector_plus_one);
            if (abs(norm) <= 1.e-10) {
                cout << "norm is to small: " << norm << endl;
                //printVector(eigenvector_plus_one);
                cin.get();
            };

            // Reyleigh-Quotient
            Rey = scalar_prod( this->eigenvector, eigenvector_plus_one) / scalar_prod(this->eigenvector, this->eigenvector);

            // ev_updated = ev_updated/ norm
            std::transform(eigenvector_plus_one.begin(), eigenvector_plus_one.end(), eigenvector_plus_one.begin(),
                           std::bind1st(std::multiplies<Cdoub>(), 1. / norm));

            // check lamd - norm
            epsilon = abs(largest_ev - norm);

            // update
            largest_ev = norm;
            this->eigenvector = eigenvector_plus_one;

            if (abs(epsilon) < tolerance) {
                go_on = false;
            }

            cout << setprecision(20);
            cout << "////////////////////////////////////" << endl;
            cout << " This round " << run_number << endl;
            cout << " abs( lambda) : " << largest_ev << endl;
            cout << " lambda (Reyleigh Quot): " << Rey << endl;
            cout << "////////////////////////////////////" << endl << endl;

            if (run_number >= max_iterations) {

                go_on = false;
            }

        }
    }
        

    const VecCdoub &getEigenvector() const {
        return this->eigenvector;
    }

    Cdoub getBiggest_eigenvalue() const {
        return biggest_eigenvalue;
    }

private:

        Cdoub scalar_prod(VecCdoub &x, VecCdoub &y) {

            assert( x.size() == y.size());

            Cdoub res = 0.;
            for (int i = 0; i < x.size(); ++i) {

                res += x[ i] * std::conj( y[ i]);
            }

            return res;
        }
    double vec_norm(VecCdoub &vector) {

        Cdoub norm =  scalar_prod( vector, vector);

        if( imag( norm) >= 1e-15){
            cout << "imag norm is quiet big" << imag( norm) << endl;
            cin.get();
        };

        return sqrt(real( norm));

    }

    void set_startguess(){

        biggest_eigenvalue = 0.0;
        /// Der eigenvector wird jetzt von aussen uebergeben
        VecCdoub initial(this->size_eigenvector , 1.0);
        this->eigenvector=initial;
    }

    Cdoub biggest_eigenvalue;
    int size_eigenvector;
    void (Functor::*updateVector)( const VecCdoub &, VecCdoub &);
};


template <typename Functor>
class OldPowerMethod : public IterativeMatrixSolver<Functor>{

public:

    OldPowerMethod(Functor &f, matCdoub (Functor::*getMatrix)()) : IterativeMatrixSolver<Functor> (f), getMatrix(getMatrix){}


    void solve(double tolerance, int max_iterations){

        //Using the getMatrix() to obtain the matrix used for the method.
        // This is a function that is passed to the class OldPowerMethod in the constructor.
        // It is part of another class and thus a Functor (class) needs to be passed as well to the mother class IterativeMatrixSolver.
        // Here the Functor is called and passes the function pointer to the function as part of the Functor, which might have a different Name as getMatrix,
        // but since it is passed as a function pointer we can identitfy it with getMatrix.

//        matCdoub Me = (this->f.*this->getMatrix)();
        matCdoub Me = (this->f.*getMatrix)();


        //Reading out the matrix size to check on screen.
        cout<<"-------------//Power iteration//"<<endl;
        int ng= Me.size();cout<<"Let's see... ng="<<ng<<endl;

        //Settign the Startguess for the eigenvector of the power method.
        eigenvector.resize(ng);
        set_set_start_guess();

        //todo: what am i doing about the interpolation? Implement a function that only get the right handside of the function!

/*        //Checking if we need to interpolate and setting up the interpolation.
//        bool are_we_interpol=false;
//        if(ng != Me[0].size()) {
//            are_we_interpol=true;
//            cout<<"We need to interpolate since grids are different size: "<<ng<<"!="<<Me[0].size()<<endl;
//            SplineInterpolator<2, 1, double, Cdoub > Spinterpol;
//            matCdoub x_values;
//
//            for (int j = 0; j < this->f.ps.pp.getNumber_of_grid_points(); ++j) {
//
//            }
//
//        }*/

        //initalizing all arrays that safe the eigenvectors during the iteration of power method.
        complex<double> G0[ng];
        complex<double> KG[ng];
//        complex<double>* G0 = new  complex<double>[ng];
//        complex<double>* KG = new  complex<double>[ng];
        complex<double> prev_eigenwert=1.0;
        complex<double> norm=1.0;
        complex<double> normG=1.0;

        double eps2=1.0;
        double h2=0;
        int steps=0;

        while (eps2>=1e-10)
        {
            complex<double> GG0=0.0;
            complex<double> sh=0.0;
            eps2=0.0;

            for (int l=0; l<ng; l++)
            {
                G0[l]=eigenvector[l];
            }
            prev_eigenwert=eigenwert;

            for(int k=0; k<ng; k++)
            {
                complex<double> kgsum=0.0;
                for(int l=0; l<ng; l++)
                {
                    kgsum+= conj(Me[k][l])*G0[l];

                }
                KG[k]=kgsum;
            }


            //Jede beliebige Norm mögich hier: vec norm, bestimmter eintrag von gamma, ..
            norm=0.0;
            normG=KG[0];

            //cout<<"BSA solved for: "<<steps<<" iteration! Norm: "<<normG<<endl;

            for(int i =0; i<ng; i++)
            {
                eigenvector[i]=KG[i]/(normG);
                GG0 += conj(G0[i])*G0[i];
                sh += conj(G0[i])*KG[i];
            }

            eigenwert=sh/GG0;
            h2 = abs((prev_eigenwert-eigenwert)/(prev_eigenwert+eigenwert));
            eps2=h2;

//            cout<<"Norm["<<steps<<"]: "<<normG<<" , EV: "<<eigenwert<<endl;
//            cout<<"--------------------->"<<"Eps2: "<<eps2<<endl;
            steps++;

        }


        cout<<"Norm["<<steps<<"]: EV: "<<eigenwert<<"......  with M="<<sqrt(this->f.meson.getPsquared())<<endl;
        //cout<<"Stutzstellen:	"<<"nz: "<<nzBSA<<", m:"<<mBSA<<", nc:"<<nnBSA<<", ny:"<<nyBSA<<endl;

        this->f.meson.setEigenvalue(eigenwert);
        this->f.meson.setAmplitude(eigenvector);


    }

    Cdoub getBiggest_eigenvalue() const {
        return eigenwert;
    }

private:

    void set_set_start_guess(){
//        fill(eigenvector,begin(eigenvector), end(eigenvector), 1.0);
        for (int i = 0; i < eigenvector.size(); i++) {
            eigenvector[i] = 1.0;
//            eigenvector[i+1] = 0.0;
//            eigenvector[i+2] = 0.0;
//            eigenvector[i+3] = 0.0;
        }
    }

    VecCdoub eigenvector;
    Cdoub eigenwert;
    matCdoub (Functor::*getMatrix)();


};


//Power method not to solve an eigenvalue problem but an Iterative matrix.
template <typename Functor>
class InhomogenousPowerMethod : public IterativeMatrixSolver<Functor> {

public:
    /// der letzte Vector in der Uebergabe ist der startguess und wird durch iteriert!
    InhomogenousPowerMethod(Functor &f, void (Functor::*updateVector)(const VecCdoub &, VecCdoub &), const int size_eigenvector)
            : IterativeMatrixSolver<Functor>(f), updateVector(updateVector), size_eigenvector(size_eigenvector) {
        this->eigenvector.resize(size_eigenvector);
    }

    void solve( double tolerance = 1.e-10, int max_iterations = 100){

        double current_tolerance=100;
        VecCdoub eigenvector_plus_one;
        eigenvector_plus_one.resize(this->size_eigenvector);
        Cdoub eigenvalue_plus_one=0.0;
        int steps=0;

        //in case of the inhomogenous this often choosen to be the inhomogneous part.
        set_startguess();



        while (current_tolerance> tolerance)
        {

            steps++;

            //Gamma' = K * Gamma
            (this->f.*this->updateVector)(this->eigenvector, eigenvector_plus_one);

            //------------------INHOMOGENEOUS PART----------------------
            //add the in homogenous Part : Gamma = inh + int K Gamma
            //where inh =  Z2 * (Pi* gamma^mu) , the part in the bracket is precalcuated.
            if(eigenvector_plus_one.size() == this->f.getInhomogenous_part().size() ){}else{assert(false);}
            for (int i = 0; i <eigenvector_plus_one.size() ; ++i) {
                eigenvector_plus_one[i] = eigenvector_plus_one[i] + sqrt(this->f.getZ2_Z2())*this->f.getInhomogenous_part()[i];
            }


            eigenvalue_plus_one=calc_eigenwert(this->eigenvector,eigenvector_plus_one);

            current_tolerance= convergence_check(biggest_eigenvalue, eigenvalue_plus_one);

            //Comment: In case of the inhomogneous BSE the amplitude is not normed while using the power method. -commented out form old power method here.
//            norm_Eigenvector(eigenvector_plus_one);

            this->eigenvector=eigenvector_plus_one;
            biggest_eigenvalue=eigenvalue_plus_one;


            //More cout for debugging or more detail purposes.
//            cout << "////////////////////////////////////" << endl ;
//            cout << " This round " << steps << endl;
//            cout << " abs( lambda) : " << abs(biggest_eigenvalue) << endl;
//            cout << " lambda (Reyleigh Quot): " << scientific << biggest_eigenvalue << endl;
////            cout << " mass is:"  << Mpi << endl;
//            cout << "////////////////////////////////////" << endl << endl;


        }

        cout<<setprecision(12)<<"Power Method stopped after ["<<steps<<"] iterations. With EV="<<biggest_eigenvalue<<endl;


//        this->f.meson.setEigenvalue(biggest_eigenvalue);
        this->f.meson.setAmplitude(this->eigenvector);
    }

    const VecCdoub &getEigenvector() const {
        return this->eigenvector;
    }

    Cdoub getBiggest_eigenvalue() const {
        return biggest_eigenvalue;
    }

private:

    void set_startguess(){

        biggest_eigenvalue = 0.0;
        VecCdoub initial;
        /// Der eigenvector wird jetzt von aussen uebergeben
        if( this->f.meson.isIs_calculated()){initial = this->f.meson.getAmp(); cout<<"SolvePowerMethod:: StartGuess set to previous amp."<<endl;}
        else{initial = this->f.getInhomogenous_part();cout<<"SolvePowerMethod:: StartGuess set to inhomogenous part."<<endl;}

//        for (int i = 0; i < size_eigenvector; ++i) {
//            initial[i] =1.0;
//        }
//        cout<<"SolverPowerMethod:: Initial value set to 1."<<endl;


        this->eigenvector=initial;
    }

    void norm_Eigenvector(VecCdoub &n){
        //Here any norm can be used. I choose to normalize to the first value of the first amplitude, such that this starts at 1.
        Cdoub norm = n[0];

        for (int i = 0, n_size=n.size(); i <n_size ; ++i) {
            n[i]=n[i]/norm;
        }
    }

    Cdoub calc_eigenwert(const VecCdoub &n, const VecCdoub & n_plus_one ){

        Cdoub n_nstart=0.0;
        Cdoub nstar_n_plus_one=0.0;

        for (int i = 0, n_size=n.size(); i <n_size ; ++i) {

            n_nstart += conj(n[i])*n[i];
            nstar_n_plus_one += conj(n[i])*n_plus_one[i];

        }

        return (nstar_n_plus_one/n_nstart);
    }

    double convergence_check(Cdoub oldval, Cdoub newval){

        return std::abs((oldval-newval)/(oldval+newval));
    }



    Cdoub biggest_eigenvalue;
    int size_eigenvector;
    void (Functor::*updateVector)( const VecCdoub &, VecCdoub &);

};



#endif //CALC_BSE_EIGENSOLVERPOWERMETHODE_H
