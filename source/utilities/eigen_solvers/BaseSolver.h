//
// Created by Developer on 25/07/17.
//

#ifndef CALC_BSE_BASESOLVER_H
#define CALC_BSE_BASESOLVER_H

#include <typedefs.h>

class BaseEigenSolver{

protected:
    VecCdoub eigenvector;

};

template <typename Functor>
class IterativeEigenSolver : public BaseEigenSolver{

public:
    IterativeEigenSolver(Functor &f,void (Functor::*updateVector)(const VecCdoub &, VecCdoub &) , const int size_eigenvector) :
            size_eigenvector(size_eigenvector),f(f), updateVector(updateVector){


        eigenvector.resize(size_eigenvector);
    };

    virtual void solve(double tolerance, int max_iterations) = 0;
    virtual const VecCdoub &getEigenvector() const =0;
    virtual Cdoub getBiggest_eigenvalue() const =0;

protected:
    /// Macht aus dem n ten Vektor den n+1 ten Vektor, wird für jede iterative Methode benötigt
    /// Eine Matrix wird nicht benötigt! Vorteil.

    int size_eigenvector;
    VecCdoub eigenvector;
    void (Functor::*updateVector)( const VecCdoub &, VecCdoub &);
    Functor &f;

};

template <typename Functor>
class IterativeMatrixSolver : public BaseEigenSolver{

public:
    IterativeMatrixSolver(Functor &f) : f(f){};

    virtual void solve(double tolerance, int max_iterations ) = 0;
    //virtual const VecCdoub &getEigenvector() const =0;
    virtual Cdoub getBiggest_eigenvalue() const =0;

protected:


    VecCdoub eigenvector;
    Functor &f;

};


class MatrixEigenSolver : public BaseEigenSolver{

    void (*constructMatrix)() = 0;

};


#endif //CALC_BSE_BASESOLVER_H
