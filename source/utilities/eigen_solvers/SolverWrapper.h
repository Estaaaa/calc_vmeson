//
// Created by Developer on 25/07/17.
//

#ifndef CALC_BSE_SOLVERWRAPPER_H
#define CALC_BSE_SOLVERWRAPPER_H


#include <assert.h>
#include "BaseSolver.h"
#include "EigenSolverPowerMethode.h"


template <typename Functor>
class SolverWrapper {

public:
    /**
     *
     * @param which_solver
     * @param updateVector
     * @param size_eigenvector
     */
    SolverWrapper(string which_solver, Functor &f,void (Functor::*updateVector)(const VecCdoub &, VecCdoub &), const int size_eigenvector) {

        if (which_solver == "power_methode"){

            basesolver = new EigenSolverPowerMethode<Functor>( f, updateVector, size_eigenvector);
        }
        else if(which_solver == "power_methode_paul"){

            basesolver = new EigenSolverPowerMethodePaul<Functor>( f, updateVector, size_eigenvector);
        }
        else if(which_solver == "power_methode_qphv"){

            basesolver = new InhomogenousPowerMethod<Functor>( f, updateVector, size_eigenvector);
        }
        else{
            assert(false);
        }
    }

    SolverWrapper(string which_solver, Functor &f, matCdoub (Functor::*getMatrix)() ) {

        if(which_solver == "Old_method"){

            basesolver = new OldPowerMethod<Functor>( f, getMatrix);
        }
        else{
            assert(false);
        }
    }

    virtual ~SolverWrapper() {
        delete basesolver;

    }

//        IterativeEigenSolver<Functor> *basesolver;
//        BaseEigenSolver *basesolver;
        IterativeMatrixSolver<Functor> *basesolver;

};


#endif //CALC_BSE_SOLVERWRAPPER_H
