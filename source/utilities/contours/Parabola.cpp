//
// Created by paul on 04.07.17.
//

#include <cmath>
#include "Grids.h"
#include "Parabola.h"
#include "../../types/typedefs.h"





void Parabola::init() {

    double p_max = sqrt(p_sq_max);

    weights_z.resize( (2. * number_of_grid_pts_on_ast));
    zvec     .resize( (2. * number_of_grid_pts_on_ast));

    //////// Fill grid and weights on parabola
    Line branch ( p_sq_min, p_sq_max, number_of_grid_pts_on_ast, "log"   , "leg", true, false);
    Line line   (    -1.,    1. , number_of_grid_pts_gerade, "linear", "leg", true, false);

    for (int i = 0; i < number_of_grid_pts_on_ast; ++i) {

        int li = number_of_grid_pts_on_ast - 1 -i;

        zvec [i]  = branch.getGrid()[i] - M*M + sqrt( branch.getGrid()[i]) * M * i_ * abs_zp;
        zvec [li] = conj(zvec[i]);

        weights_z[ i]  =  branch.getWeights_integration()[ i] * ( 1. + i_ * M /( 2. * sqrt( branch.getGrid()[ i])));
        weights_z[ li] =  conj( weights_z[i]);
    }


    for (int i = 0; i < number_of_grid_pts_gerade; ++i) {

        zvec.push_back( p_sq_max - M * M - i_ * branch.getGrid()[ i] * p_max * M);

        weights_z.push_back( line.getWeights_integration()[ i] * -1. * i_ * p_max * M);
    }

}

const double Parabola::getP_sq_min() const {
    return p_sq_min;
}

const double Parabola::getP_sq_max() const {
    return p_sq_max;
}

const double Parabola::getM() const {
    return M;
}

const double Parabola::getAbs_zp() const {
    return abs_zp;
}

const int Parabola::getNumber_of_grid_pts_on_ast() const {
    return number_of_grid_pts_on_ast;
}

const int Parabola::getNumber_of_grid_pts_gerade() const {
    return number_of_grid_pts_gerade;
}


const std::vector<Cdoub> &Parabola::getZvec() const {
    return zvec;
}

const std::vector<Cdoub> &Parabola::getWeights_z() const {
    return weights_z;
}

Parabola::Parabola(double p_sq_min, double p_sq_max, double M, double abs_zp, int number_of_grid_pts_on_ast,
                   int number_of_grid_pts_gerade) : p_sq_min(p_sq_min), p_sq_max(p_sq_max), M(M), abs_zp(abs_zp),
                                                    number_of_grid_pts_on_ast(number_of_grid_pts_on_ast),
                                                    number_of_grid_pts_gerade(number_of_grid_pts_gerade) {

    init();
}


void dummie(){

    cout << "dumm haefgh???" << endl;
}
