//
// Created by paul on 04.07.17.
//

#ifndef CALC_BSE_PARABOLA_H
#define CALC_BSE_PARABOLA_H


#include "typedefs.h"

void dummie();

//// parabola from squaring four vector of the form (p1, p2, p3, p4 + I M)
//// fill parabola with p^2 - M^2 + 2 I M p z, real part from pmin^2 - M^2 - pmax^2 - M^2
class Parabola {

public:


    Parabola(double p_sq_min, double p_sq_max, double M, double abs_zp, int number_of_grid_pts_on_ast,
             int number_of_grid_pts_gerade);


    const VecCdoub &getZvec() const;

    const VecCdoub &getWeights_z() const;

    const double getP_sq_min() const;

    const double getP_sq_max() const;

    const double getM() const;

    const double getAbs_zp() const;

    const int getNumber_of_grid_pts_on_ast() const;

    const int getNumber_of_grid_pts_gerade() const;

    void init();

protected:
    VecCdoub zvec;
    VecCdoub weights_z;

    double p_sq_min;
    double p_sq_max;
    double M;

    double abs_zp;
    int number_of_grid_pts_on_ast ;
    int number_of_grid_pts_gerade ;


};


#endif //CALC_BSE_PARABOLA_H
