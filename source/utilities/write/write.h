//
// Created by esther on 15.01.18.
//

#ifndef CALC_VMESON_WRITE_H
#define CALC_VMESON_WRITE_H

#include <typedefs.h>
#include <assert.h>
#include <iomanip>
#include <fstream>

void write(matCdoub& data, string& filename){

    ofstream write(filename);
    assert( write.is_open());

//        cout << write.is_open() << endl;
    write<<"#p2   A		B"<<endl;
    for(int i=0; i<data[0].size(); i++)
    {
        for (int j = 0; j < data.size(); ++j) {

            write<<scientific<<setprecision(10)<<real(data[j][i])<<"\t"<<imag(data[j][i])<<"\t";
        }
        write<<endl;
    }
    write.close();
}

#endif //CALC_VMESON_WRITE_H
