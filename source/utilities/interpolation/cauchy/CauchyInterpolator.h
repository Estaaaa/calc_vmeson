#ifndef CAUCHY_INTERPOLATOR_H
#define CAUCHY_INTERPOLATOR_H

#include <assert.h>
#include "typedefs.h"

class CauchyInterpolator{

public:
    CauchyInterpolator(const VecCdoub &z, const VecCdoub &f_of_z, const VecCdoub &weights_on_ct) :
            z_on_contour(z),
            f_of_z_on_contour(f_of_z),
            weights_on_contour( weights_on_ct)
    {
    }


    CauchyInterpolator( FunPtrCdoubOfConstCdoub fill_contour_with_function,  const VecCdoub &z, const VecCdoub &weights_on_ct) :
            z_on_contour(z),
            fill_contour(fill_contour_with_function),
            weights_on_contour( weights_on_ct)
    {
        for (int i = 0; i < f_of_z_on_contour.size(); ++i) {

            f_of_z_on_contour[i] = fill_contour( z[i]);
        }
    }


    bool out_of_bounds_check(Cdoub z) const {

        // todo: hier kann ich nur fuer Parabel momentan iimplementieren...
    }

    Cdoub getValue(Cdoub z) const{

        assert( out_of_bounds_check(z));

        Cdoub integral = 0.;
        Cdoub norm     = 0.;

        for (int j = 0; j < z_on_contour.size(); ++j) {

            norm     += weights_on_contour[j]                        / ( z_on_contour[j] - z);
            integral += weights_on_contour[j] * f_of_z_on_contour[j] / ( z_on_contour[j] - z);
        }
    }


protected:
    FunPtrCdoubOfConstCdoub fill_contour = NULL;
    const VecCdoub z_on_contour;
    const VecCdoub  weights_on_contour;
    VecCdoub f_of_z_on_contour;

};

#endif
