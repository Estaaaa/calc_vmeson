#pragma once
#include "Grid_rules.h"


class Log10Line_Legendre{
	public:
    // Internal functions
    auto Nodes (  int n, int j){
	  	return 	Nodes_Legendre(-1.,1.,j,n);
	};

	auto Linear_map (double x, double IR, double UV){
		return log10(IR) + (x+1.)/2.*(log10(UV) - log10(IR) );
	};

	double Get_x( double IR, double UV, int order, int p_i){

		return pow(10.,Linear_map(Nodes(order, p_i), IR, UV ));
	}

	double Get_w_interpolation( double IR, double UV, int order, int p_i){
		auto Weights = [&](int n, int j){
				//return Barycentric_weights_Berrut_new  ([&](int jj){return Nodes_Legendre(-1.,1.,jj,n);}, 4, n,  j) ;
				return Barycentric_weights_Legendre(j,n);
				//return Barycentric_weights_Berrut_new  ([&](int jj){return Nodes_Tscheb_2kind(-1.,1.,jj,n);}, 4, n,  j) ;
				//return Barycentric_weights_Equal_dist(j,n);
		};
		return Weights(order, p_i);
	}

    double Get_w_integration( double IR, double UV, int order, int p_i){
		auto Weights = [&](int n, int j){
				return Gauss_weights_Legendre(-1.,1.,  j, n )*log(10.)*pow(10.,Linear_map(Nodes(order, p_i), IR, UV ))
                       *(0.5*(log10(UV) - log10(IR) ));
		};
        // Beware linear mapping factor:
        // t = a + x*c
        // dx = dt/c;
        // c = 0.5*(log10(UV) - log10(IR) )
		return Weights(order, p_i);
	}

	double Get_w_integration_quadratic( double IR, double UV, int order, int p_i){
		auto Weights = [&](int n, int j){
				// cout << setprecision(16) << pow(10.,Linear_map(Nodes(order, p_i), IR, UV )) <<" " <<
				// Gauss_weights_Legendre(-1.,1.,  j, n )*log(10.)*pow(10.,2.*Linear_map(Nodes(order, p_i), IR, UV ))*0.5
				// 	   *(0.5*(log10(UV) - log10(IR) ))
				// << endl;cin.get();
				return Gauss_weights_Legendre(-1.,1.,  j, n )*log(10.)*pow(10.,2.*Linear_map(Nodes(order, p_i), IR, UV ))*0.5
					   *(0.5*(log10(UV) - log10(IR) ));
		};
		// Beware linear mapping factor:
		// t = a + x*c
		// dx = dt/c;
		// c = 0.5*(log10(UV) - log10(IR) )

		// measure for radial part (quadratic!!)
		// x = p^2
		// dx  = 2 p dp => dp = 0.5/p*dx =>  p^3 dp = 0.5*x*dx

		//r^3 dr => 0.5 (r^2) d(r^2) = 0.5 R dR
		//10^x = R
		//dx ln(10) 10^x = dR
		// 0.5 R dR = ln(10) 10^(2x) dx
		return Weights(order, p_i);
	}
};


//	dx; t = IR + 1/2*(UV - IR)*x => dt = 1/2*(UV - IR) dx => dx = dt*2/(UV - IR)
//	t = IR + x/2*(UV - IR) => a = 1/2*(UV - IR) = 0.5*(UV - IR)
//

class Line_Legendre{
	public:

	double Get_x( double IR, double UV, int order, int p_i){
		auto Nodes = [&](  int n, int j){

		  	return 	Nodes_Legendre(-1.,1.,j,n);
		};

		auto Linear_map = [&](double x, double IR, double UV){
			return IR + (x+1.)/2.*(UV - IR );
		};
		return Linear_map(Nodes(order, p_i), IR, UV );
	}

	double Get_w_interpolation( double IR, double UV, int order, int p_i){
		auto Weights = [&](int n, int j){
				//return Barycentric_weights_Berrut_new  ([&](int jj){return Nodes_Legendre(-1.,1.,jj,n);}, 4, n,  j) ;
				return Barycentric_weights_Legendre(j,n);
				//return Barycentric_weights_Berrut_new  ([&](int jj){return Nodes_Tscheb_2kind(-1.,1.,jj,n);}, 4, n,  j) ;
				//return Barycentric_weights_Equal_dist(j,n);
		};
		return Weights(order, p_i);
	}

    double Get_w_integration( double IR, double UV, int order, int p_i){
        auto Weights = [&](int n, int j){
				//cout << std::setprecision(16) <<  Gauss_weights_Legendre(-1.,1.,  j, n )*(0.5*(UV - IR) << endl;;cin.get();
				return Gauss_weights_Legendre(-1.,1.,  j, n )
                        *(0.5*(UV - IR ));
		};
        // Beware linear mapping factor:
        // t = a + x*c
        // dt = dt*c;
        // c = 0.5*(log10(UV) - log10(IR) )
		return Weights(order, p_i);
    }
};



class Log10Line_Equi{
	public:
    // Internal functions
    auto Nodes (  int n, int j){
	  	return 	Nodes_Equal_dist(-1.,1.,j,n);
	};

	auto Linear_map (double x, double IR, double UV){
		return log10(IR) + (x+1.)/2.*(log10(UV) - log10(IR) );
	};

	double Get_x( double IR, double UV, int order, int p_i){

		return pow(10.,Linear_map(Nodes(order, p_i), IR, UV ));
	}

	double Get_w_interpolation( double IR, double UV, int order, int p_i){
		auto Weights = [&](int n, int j){
				//return Barycentric_weights_Berrut_new  ([&](int jj){return Nodes_Legendre(-1.,1.,jj,n);}, 4, n,  j) ;
				//return Barycentric_weights_Legendre(j,n);
				//return Barycentric_weights_Berrut_new  ([&](int jj){return Nodes_Tscheb_2kind(-1.,1.,jj,n);}, 4, n,  j) ;
				return Barycentric_weights_Equal_dist(j,n);
		};
		return Weights(order, p_i);
	}
};



class Line_Equi{
	public:
    // Internal functions
    auto Nodes (  int n, int j){
	  	return 	Nodes_Equal_dist(-1.,1.,j,n);
	};

	auto Linear_map (double x, double IR, double UV){
		return (IR) + (x+1.)/2.*((UV) - (IR) );
	};

	double Get_x( double IR, double UV, int order, int p_i){

		return Linear_map(Nodes(order, p_i), IR, UV );
	}

	double Get_w_interpolation( double IR, double UV, int order, int p_i){
		auto Weights = [&](int n, int j){
				//return Barycentric_weights_Berrut_new  ([&](int jj){return Nodes_Legendre(-1.,1.,jj,n);}, 4, n,  j) ;
				//return Barycentric_weights_Legendre(j,n);
				//return Barycentric_weights_Berrut_new  ([&](int jj){return Nodes_Tscheb_2kind(-1.,1.,jj,n);}, 4, n,  j) ;
				return Barycentric_weights_Equal_dist(j,n);
		};
		return Weights(order, p_i);
	}
};




class Cauchy_simple{
	Line_Legendre      Line;
	public:

	_complex Get_x( double UV, double Apex, int N_Cauchy, int N_line , int p_i){
		if(p_i < N_Cauchy){
			int p_i_eff = p_i;
			double t_max = 2.*sqrt(Apex)*sqrt(UV + Apex);
			_complex t = Line.Get_x( 0.,  t_max,  N_Cauchy, p_i_eff);
			_complex z = 0.25/Apex*pow<2>(t) - Apex - II*t;

			return z;
		}
		else if(p_i>=N_Cauchy and  p_i < N_Cauchy + N_line  ){
			int p_i_eff = p_i - N_Cauchy;

			_complex t = Line.Get_x( -1.,  1.,  N_line, p_i_eff);
			_complex z = UV + II*(2.*sqrt(Apex)*sqrt(UV + Apex))*t;
			return z;
		}
		else{
			int p_i_eff = p_i - N_line - N_Cauchy;

			double t_max = 2.*sqrt(Apex)*sqrt(UV + Apex);
			_complex t = Line.Get_x( 0.,  t_max,  N_Cauchy, p_i_eff);
			_complex z = 0.25/Apex*pow<2>(t) - Apex + II*t;

			return z;
		}
	}

	_complex Get_w_interpolation( double UV, double Apex, int N_Cauchy, int N_line , int p_i){
		if(p_i < N_Cauchy){
			int p_i_eff = p_i;
			double t_max = 2.*sqrt(Apex)*sqrt(UV + Apex);
			_complex t = Line.Get_x( 0.,  t_max, N_Cauchy,    p_i_eff);

			_complex w_integral =  Gauss_weights_Legendre(-1.,1.,  p_i_eff, N_Cauchy )
								*(0.5/Apex*t - II);

			return w_integral;
		}
		else if(p_i>=N_Cauchy and p_i < N_Cauchy + N_line  ){
			int p_i_eff = p_i - N_Cauchy;

			_complex w_integral =  Gauss_weights_Legendre(-1.,1.,  p_i_eff, N_line )
								*II*(2.*sqrt(Apex)*sqrt(UV + Apex));

			return w_integral;
		}
		else{
			int p_i_eff = p_i - N_line - N_Cauchy;

			double t_max = 2.*sqrt(Apex)*sqrt(UV + Apex);
			_complex t = Line.Get_x( 0.,  t_max,  N_Cauchy, p_i_eff);

			_complex w_integral =   Gauss_weights_Legendre(-1.,1., p_i_eff, N_Cauchy )
								  *(0.5/Apex*t + II)
								  *(-1.);//Because the integral-boundaries are reversed!
			return w_integral;
		}
	}

};






class Line_Cheb_U{
	public:

	double Get_x( double IR, double UV, int order, int p_i){
		auto Nodes = [&](  int n, int j){

		  	return 	Nodes_Tscheb_2kind(-1.,1.,j,n);
		};

		auto Linear_map = [&](double x, double IR, double UV){
			return IR + (x+1.)/2.*(UV - IR );
		};
		return Linear_map(Nodes(order, p_i), IR, UV );
	}

	double Get_w_interpolation( double IR, double UV, int order, int p_i){
		auto Weights = [&](int n, int j){
				//return Barycentric_weights_Berrut_new  ([&](int jj){return Nodes_Legendre(-1.,1.,jj,n);}, 4, n,  j) ;
				return Barycentric_weights_Tscheb_2kind(j,n);
				//return Barycentric_weights_Berrut_new  ([&](int jj){return Nodes_Tscheb_2kind(-1.,1.,jj,n);}, 4, n,  j) ;
				//return Barycentric_weights_Equal_dist(j,n);
		};
		return Weights(order, p_i);
	}

    double Get_w_integration( double IR, double UV, int order, int p_i){
        auto Weights = [&](int n, int j){
				//cout << std::setprecision(16) <<  Gauss_weights_Legendre(-1.,1.,  j, n )*(0.5*(UV - IR) << endl;;cin.get();
				return Gauss_weights_Tscheb_2kind(-1.,1.,  j, n )
                        *(0.5*(UV - IR ));
		};
        // Beware linear mapping factor:
        // t = a + x*c
        // dt = dt*c;
        // c = 0.5*(log10(UV) - log10(IR) )
		return Weights(order, p_i);
    }
};
