//
// Created by paul on 16.08.17.
//

#ifndef CALC_BSE_BASEINTERPOLPE_H
#define CALC_BSE_BASEINTERPOLPE_H


#include <assert.h>
#include <memory>

/// Walter includes
#include "./Allgemein.h"
#include "./Global_utility/Vector_resize.h"
#include "./Global_utility/Comparison.h"
#include "./Interpolation/Interpolation_polynomial.h"


/// alglib includes
#include "alglib/src/interpolation.h"


using namespace alglib;

//#define DEBUG_SPLINES

//void set_alglib_array_to_real_part_of_vector(real_1d_array array, auto &values) {
//
////    double * r = &reinterpret_cast<double(&)[2]>( values[0] )[0];
//
//    VecDoub r(values.size());
//    for (int i = 0; i < values.size(); ++i) {
//
//        r[i] = real(values[i]);
//    }
//    array.setcontent( values.size(), &(r[0]));
//
//}
//void set_alglib_array_to_imag_part_of_vector(real_1d_array array, auto &values) {
//
////    double * i = &reinterpret_cast<double(&)[2]>( values[0] )[1];
////    array.setcontent( values.size(), i);
//
////    VecDoub r(values.size());
////    for (int i = 0; i < values.size(); ++i) {
////
////        r[i] = imag(values[i]);
////    }
////    array.setcontent( values.size(), &r[0]);
//}




template<int x_dim, int vec_dim,  typename y_type, typename x_type>
class BaseInterpol {

public:
    typedef vector< y_type> y_vec;
    typedef array< x_type, x_dim>   x_point;

public:
    /**
     * returns the itnerpolated values (f0, .., f_vecdim) for one grid point (x1,..,x_dim)
     * @param y_result the resulting f(x) values (vector)
     * @param x_values one grid point
     */
    virtual void getValue(y_vec & y_result, const x_point & x_value)  const                           = 0;
    

    /**
     * builds the model for the interpolation. 
     * Barycentric will store x, f(x), calc and store weights(x) in constructor, and update y values only.
     * Spline will store x, f(x), calc and store model in constructor and update y values only.
     * @param x_values Vector of grid points
     * @param y_values Vector of f(x) values. (can be vectors as well) {(f_0, f_1)(x1), (f_0,f_1)(x_2), ...}
     */
    virtual void Update( const std::vector< x_point> &x_values,  const std::vector< y_vec> &y_values) = 0;
};

template<int x_dim, int vec_dim,  typename y_type, typename x_type>
class SplineInterpolator : public BaseInterpol<x_dim, vec_dim, y_type, x_type>{


};

// todo: das kann unterscheiden welchen spline man nimmt.
//class FunctorSplineBuilds{
//
//    FunctorSplineBuilds(int which_spline){
//
//        bind_to_function ...
//    }
//
//    update();
//    spline1dbuildcubic( alglx, algly_real, *interpolant);
//    spline1dbuildcatmullrom( alglx, algly_real, *interpolant);
//    spline1dbuildakima( alglx, algly_real, *interpolant);
//    spline1dbuildlinear( alglx, algly_real, *interpolant);
//
////        spline1dbuildhermite( alglx, algly, *interpolant); // needs derivatives
//
//};


template<>
class SplineInterpolator< 1, 1, double, double> {

public:
    SplineInterpolator() {

        cout << "does cubic splines at the moment, go to constructor if you want to change it" << endl;
        interpolant = std::make_shared< spline1dinterpolant>(spline1dinterpolant());
    };

    void Update( const VecDoub &x_values, const VecDoub &y_values) {

        alglx.setcontent(x_values.size(), &x_values[0]);
        algly_real.setcontent(y_values.size(), &y_values[0]);

        spline1dbuildcubic( alglx, algly_real, *interpolant);
//        spline1dbuildcatmullrom( alglx, algly_real, *interpolant);
//        spline1dbuildakima( alglx, algly_real, *interpolant);
//        spline1dbuildlinear( alglx, algly_real, *interpolant);
//        spline1dbuildhermite( alglx, algly_real, *interpolant); // needs derivatives
    };

    void getValue( auto &y_result, auto &x_values) const{

        y_result[0] = spline1dcalc(*interpolant, x_values[0]);
    };

private:
//    FunctorSplineBuilds F;
    real_1d_array alglx;
    real_1d_array algly_real;
    std::shared_ptr<spline1dinterpolant> interpolant;
};


template<int v_dim>
class SplineInterpolator< 2, v_dim, double, Cdoub> {

public:
    SplineInterpolator() {

        cout << "does cubic splines at the moment, go to constructor if you want to change it" << endl;
        interpolant      = std::make_shared< spline2dinterpolant>(spline2dinterpolant());
        interpolant_imag = std::make_shared< spline2dinterpolant>(spline2dinterpolant());
    };

/**
         * builds the spline model, has to be called befor Interpolate function
         * @param x_values { x0_vec, x1_vec}
         * @param y_values { }
         */
    void Update( const matDoub &x_values, const VecDoub &y_values) {

        VecDoub x_v = x_values[0];
        VecDoub x1_v = x_values[1];

        alglx0.setcontent( x_v.size(),  &( x_v[0]));
        alglx1.setcontent( x1_v.size(), &(x1_v[0]));


        VecDoub yr(y_values.size());
        VecDoub yi(y_values.size());
        for (int i = 0; i < y_values.size(); ++i) {

            yr[i] = real(y_values[i]);
            yi[i] = imag(y_values[i]);
        }
        cout << v_dim << endl;

        algly_real.setcontent( yr.size(), &(yr[0]));
        algly_imag.setcontent( yr.size(), &(yi[0]));

        spline2dbuildbicubicv( alglx0, alglx0.length(), alglx1, alglx1.length(), algly_real, v_dim, *interpolant);
//        spline2dbuildbicubicv( alglx0, x_values[0].size(), alglx1, x_values[1].size(), algly_imag, v_dim, *interpolant_imag);


//        spline1dbuildcatmullrom( alglx, algly_real, *interpolant);
//        spline1dbuildakima( alglx, algly_real, *interpolant);
//        spline1dbuildlinear( alglx, algly_real, *interpolant);
//        spline1dbuildhermite( alglx, algly_real, *interpolant); // needs derivatives
#ifdef DEBUG_SPLINES
        model_updated = true;
#endif
    };

    /// {x_0, x_1}, {y_0, ..., y_dim}
    void getValue( auto &y_result, auto &x_values) const{


        for (int i = 0; i < v_dim; ++i) {

            y_result [i] = spline2dcalc(*interpolant, x_values[0], x_values[1])
                           + i_ * spline2dcalc(*interpolant_imag, x_values[0], x_values[1]);
        }

#ifdef DEBUG_SPLINES
        assert(model_updated);
        model_updated = false;
#endif


    };

private:
//    FunctorSplineBuilds F;
    real_1d_array alglx0;
    real_1d_array alglx1;
    real_1d_array algly_real;
    real_1d_array algly_imag;

    std::shared_ptr<spline2dinterpolant> interpolant;
    std::shared_ptr<spline2dinterpolant> interpolant_imag;
#ifdef DEBUG_SPLINES
    bool model_updated = false;
#endif
};

template<int x_dim, int vec_dim,  typename y_type, typename x_type>
class WaltersBarycentric : public BaseInterpol<x_dim, vec_dim, y_type, x_type>{

    typedef vector< y_type> y_vec;
    typedef array< x_type, x_dim>   x_point;

public:
    WaltersBarycentric(const std::vector< y_vec> &y_values, int total_number_of_grid_points,
    auto && x_i, auto && w_i) : total_number_of_grid_points(total_number_of_grid_points){

        Interpolator = new Cartesin_Barycentric_interpolator<x_dim, vec_dim, y_type, x_type>;

        Interpolator->Initialize(total_number_of_grid_points, std::move(x_i), std::move(w_i));

        for (int i = 0; i < vec_dim; ++i) {

            Interpolator->Set_y(i, y_values[i]);
        }


    }

    void Update(const std::vector<x_point> &x_values, const std::vector<y_vec> &y_values ) override {

        for (int i = 0; i < vec_dim; ++i) {

            Interpolator->Set_y(i, y_values[i]);
        }

//        cout << "Updated y-values." << endl;
    }


    void Update2(const std::vector<x_point> &x_values, const AmplitudeStore<vec_dim, x_dim, y_type> &amp ){

        for (int i = 0; i < vec_dim; ++i) {

            auto bla = amp [ i];
            Interpolator->Set_y(i, bla);
        }
//        cout << "Updated y-values." << endl;
    }


    virtual void getValue(y_vec &y_result, const x_point &x_value) const {

        auto xp( x_value);
        Interpolator->Interpolate(y_result, xp);
    }


//    /// This is hard coded for the getValue of the MultiO object.
//    std::array<y_type, vec_dim> getValue( const x_point &x_value) const{
//
//        std::array<y_type, vec_dim> y_result;
//        vector<y_type> resvec;
//        Interpolator->Interpolate(resvec, x_value);
//        std::copy_n( resvec.begin(), vec_dim, y_result.begin());
//
//        return y_result;
//    }

private:
    Cartesin_Barycentric_interpolator<x_dim, vec_dim, y_type, x_type> *Interpolator;
    int total_number_of_grid_points;
};


//public:
//SplineInterpolator() {
//    if( x_dim == 1){
//
//        spline1d interpolant();
//    }
//    else if( x_dim == 2){
//
//        spline2d interpolant();
//    }
//    else if( x_dim == 3){
//
//        spline3d interpolant();
//    }
//    else{
//        assert(false);
//    }
//
//}
//
//private:
//spline1dinterpolant* int1DReal
//spline2dinterpolant* int2DReal
//spline3dinterpolant* int3DReal
////Spline<dim, vecdim, double, double> spline( );
////spline.initialize( (w), x, fx);
////spline.getValue( y) -> fy;









#endif //CALC_BSE_BASEINTERPOLPE_H
