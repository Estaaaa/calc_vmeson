#pragma once
#include "../Grids/Grid_rules.h"
#include "../Grids/Paths.h"
//#include "Comparison.h"

//bool isApproximatelyEqual ( double a, double b) {
//	if(abs(a)<1.e-13 and abs(b)<1.e-13){
//		//treat them both as zero
//		return true;
//	}
//	double maxRelDiff =1.0e-14;
//	double diff = fabs(a - b);
//	a = fabs(a);
//	b = fabs(b);
//	// Find the largest
//	double largest = (b > a) ? b : a;
//
//	if (diff <= largest * maxRelDiff)
//		return true;
//	return false;
//}


template<int Dim, int MDim,  typename y_type, typename x_type>
class  Interpolationn_grid{
	public:
	//&&&Variables&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	vector<y_type> y_values;
	vector<x_type> x_values;
	vector<x_type> w_values;


	long int N;

	//***Set_Grid***************************************************************
	//
	//  Input: length
	//	Output:
	//
	//	Description: Resizes the vectors
	//				 In the case of a cartesian grid: N = n1*n2*...*nd
	//				 Except for w.
	//
	//	Data model:
	//					ind = 1 | x_1^1    ind = 1 | y_1^1   ind = 1 | w^1
	//  				ind = 1 | x_2^1    ind = 1 | y_2^1   ind = 2 | w^1
	//					...                ...               ...
	//					ind = 1 | x_d^1    ind = 1 | y_md^1   ind = N | x^N
	//					ind = 2 | x_1^2    ind = 2 | y_1^2
	//					...                ...
	//					ind = N | x_d^N    ind = N | y_md^N
	//**************************************************************************
	void Set_Grid(long int N_init){
		N = N_init;
		Vector_Resize(w_values, N);
		Vector_Resize(x_values, N*Dim);
		Vector_Resize(y_values, N*MDim);
	}
	//***Set/Get_y**************************************************************
	//
	//  Input: Full vector/array
	//	Output:
	//
	//	Description: Sets/Gets the y-values. Setting only by passing
	//				 of a full vector! To hide the not intuitional
	//				 storage logic, the passed y_values have a fixed mdim_i
	//				 index
	//**************************************************************************
	void Set_y(int mdim, auto & values){
		// checks sizes. The copying should never be the bottleneck,
		// so no loss of performance is expected
		assert(values.size()==y_values.size()/MDim );
		for (int p_i = 0; p_i < N; ++p_i){
			long int idx = MDim*p_i + mdim;
			y_values[idx] = values[p_i];
		}
	}
	y_type Get_y(int mdim, long int p_i) {
		long int idx = MDim*p_i + mdim;
		return y_values[idx];
	}
	//***Set/Get_x**************************************************************
	//
	//  Input:
	//	Output: Setting is pointwise because the setting process is done
	//		    at the initialization stage
	//
	//	Description: Sets/Gets the x-values.
	//**************************************************************************
	void Set_x(int dim, long int p_i, x_type value){
		long int idx = Dim*p_i + dim;
		x_values[idx] = value;
	}
	x_type Get_x(int dim, long int p_i) {
		long int idx = Dim*p_i + dim;
		return x_values[idx];
	}
	//***Set/Get_w**************************************************************
	//
	//  Input:
	//	Output:Setting is pointwise because the setting process is done
	//		   at the initialization stage
	//
	//	Description: Sets/Gets the x-values.
	//**************************************************************************
	void Set_w( long int p_i, x_type value){
		w_values[p_i] = value;
	}
	x_type Get_w( long int p_i){
		return w_values[p_i];
	}
	//***Get_N/Dim/MDim*********************************************************
	//
	//  Input:
	//	Output:
	//
	//	Description:  returns various sizes
	//**************************************************************************
	long int Get_N(){
		return N;
	}
	int Get_Dim(){
		return Dim;
	}
	int Get_MDim(){
		return MDim;
	}
};

template<int Dim, int MDim, typename  y_type, typename  x_type>
class Cartesin_Barycentric_interpolator
{
	public:
	//&&&Variables&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	array<int, Dim> periodic_rule;
	vector<array<x_type, Dim>>			 Poles; //	Single poles, higher
												//  dimensional objects!

	Interpolationn_grid<Dim, MDim, y_type, x_type> grid;
	//&&&Functions&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	//***Initialize*************************************************************
	//
	//  Input: N, x/w lambda
	//	Output:
	//
	//	Description: Resizes N dependent vectors
	//				 and calls externally defined x and w lambdas to fill
	//				 them
	//**************************************************************************
	void Initialize(long int N_init, auto && x_i, auto && w_i ){
		//Resize N dependent storage
		std::fill(periodic_rule.begin(),periodic_rule.end(),0);//default
		grid.Set_Grid(N_init);
		//Initialize grid
		for (int p_i = 0; p_i < Get_N(); ++p_i){
			//Set x
			for (int dim_i = 0; dim_i < Dim; ++dim_i){
				grid.Set_x(dim_i, p_i, x_i(dim_i, p_i));
			}
			//Set w
			grid.Set_w(p_i, w_i( p_i));
		}
	}
	//***Set/Get_y**************************************************************
	//
	//  Input: Full vector/array
	//	Output:
	//
	//	Description: Sets/Gets the y-values. Setting only by passing
	//				 of a full vector! To hide the not intuitional
	//				 storage logic, the passed y_values have a fixed mdim_i
	//				 index. Calls routine in grid.
	//**************************************************************************
	void Set_y(int mdim,  auto & values){
		grid.Set_y(mdim, values);
	}
	y_type Get_y(int mdim, long int p_i){
		return grid.Get_y(mdim, p_i);
	}
	//***Get_x******************************************************************
	//
	//  Input:
	//	Output:
	//
	//	Description: Sets/Gets the x-values.
	//**************************************************************************
	x_type Get_x(int dim, long int p_i){
		return grid.Get_x(dim, p_i);
	}
	//***Set_periodic***********************************************************
	//
	//  Input: dim_i, periodic code
	//	Output:
	//
	//	Description: Sets the ith dimension to be treated periodic
	//	Additional: Coding:			0: not periodic (default)
	//								1: odd number of points
	//								2: even number of points
	//**************************************************************************
	void Set_periodic(int dim_i, int periodic_code){
		periodic_rule[dim_i]=periodic_code;
	}
	//***Add_poles**************************************************************
	//
	//  Input:  pole vec
	//	Output:
	//
	//	Description: Adds pole to pole list (push_back)
	//	Additional:
	//**************************************************************************
	void Add_pole(auto & pole_vec){
		Poles.push_back(pole_vec);
	};
	//***x_periodic_map*********************************************************
	//
	//  Input: dim_i, periodic code
	//	Output:
	//
	//	Description: returns the identy if not periodic
	//				 or the sin/tan if periodic
	//**************************************************************************
	x_type x_periodic_map(int dim_i, x_type x){
		if(     periodic_rule[dim_i] ==0 ){return x;}
		else if(periodic_rule[dim_i] ==1 ){return sin(x*0.5);}//odd  number
		else if(periodic_rule[dim_i] ==2 ){return tan(x*0.5);}//even number
		else{
			assert(1==2);
		}
	}
	//***Interpolate************************************************************
	//
	//  Input: result vector, various lambdas
	//	Output:
	//
	//	Description: Calculates the interpolator, ands stores it in y_result
	//				 Thread safe
	//**************************************************************************
	void Interpolate(auto & y_result, auto & x_values) {

		// reset local storage
		x_type denominator = 0.;
		//if result is guranteed to be init to zero, this can be eliminated
		for (int mdim_i = 0; mdim_i < MDim; ++mdim_i){
			y_result[mdim_i] = 0.;
		}

		for (int p_i = 0; p_i < Get_N(); ++p_i){
			// Calc x_product
			x_type x_prod = 1.;
			for (int dim_i = 0; dim_i < Dim; ++dim_i){
				//if a grid point is hit, the x_value is shifted slightly
				if(isApproximatelyEqual(x_values[dim_i] , grid.Get_x(dim_i, p_i))){
					if(p_i!=Get_N()-1){
						x_values[dim_i] += 0.01*(grid.Get_x(p_i+1, dim_i) - grid.Get_x(dim_i, p_i));
					}
					else{
						x_values[dim_i] -= 0.01*(grid.Get_x(dim_i, p_i) - grid.Get_x(p_i-1, dim_i));
					}
				}
				//cout << grid.Get_x(dim_i, p_i)  << " " <<  x_values[dim_i] << endl; cin.get();
				x_prod *= x_periodic_map(dim_i,  grid.Get_x(dim_i, p_i) - x_values[dim_i] );
			}

			// Calc pole_product
			x_type pole_prod = 1.;
			for (int dim_i = 0; dim_i < Dim; ++dim_i){
				for (size_t pole_i = 0; pole_i < Poles.size(); pole_i++) {
					pole_prod*=(Poles[pole_i][dim_i] - x_values[dim_i] );
				}
			}
			// Calc Summands
			x_type W = grid.Get_w(p_i)*pole_prod/x_prod;
			denominator += W;


			for (int mdim_i = 0; mdim_i < MDim; ++mdim_i){
				y_result[mdim_i] += W * grid.Get_y(mdim_i, p_i);
				//cout << W * grid.Get_y(mdim_i, p_i) << endl;
			}

		}
		// Divide by global denominator
		for (int mdim_i = 0; mdim_i < MDim; ++mdim_i){
			 y_result[mdim_i] /= denominator;
		}

		//cout << " y_result " << y_result[0] << " " << denominator << endl; cin.get();

	}
	//***Interpolate************************************************************
	//
	//  Input: result vector_rule, various lambdas
	//	Output:
	//
	//	Description: Calculates the interpolator rule and store it in result_w
	//**************************************************************************
	void Calc_Rule(auto & result_w, auto & x_values){
		// reset local storage
		x_type denominator = 0.;

		for (int p_i = 0; p_i < Get_N(); ++p_i){
			// Calc x_product
			x_type x_prod = 1.;
			for (int dim_i = 0; dim_i < Dim; ++dim_i){
				//if a grid point is hit, the x_value is shifted slightly
				if(isApproximatelyEqual(x_values[dim_i] , grid.Get_x(dim_i, p_i))){
					if(p_i!=Get_N()-1){
						x_values[dim_i] += 0.01*(grid.Get_x(p_i+1, dim_i) - grid.Get_x(dim_i, p_i));
					}
					else{
						x_values[dim_i] -= 0.01*(grid.Get_x(dim_i, p_i) - grid.Get_x(p_i-1, dim_i));
					}
				}

				x_prod *= x_map(dim_i,  grid.Get_x(dim_i, p_i) - x_values[dim_i] );
			}
			// Calc pole_product
			x_type pole_prod = 1.;
			for (int dim_i = 0; dim_i < Dim; ++dim_i){
				for (size_t pole_i = 0; pole_i < Poles.size(); pole_i++) {
					pole_prod*=(Poles[pole_i][dim_i] - x_values[dim_i] );
				}
			}
			// Calc Summands
			x_type W = grid.Get_w(p_i)*pole_prod/x_prod;
			result_w[p_i] =  W;
			denominator   += W;
		}

		// Divide by global denominator
		for (int p_i = 0; p_i < Get_N(); ++p_i){
			result_w[p_i] /= denominator;
		}
	}
	//***Get_N/Dim/MDim*********************************************************
	//
	//  Input:
	//	Output:
	//
	//	Description:  returns various sizes
	//**************************************************************************
	long int Get_N(){
		return grid.Get_N();
	}
	int Get_Dim(){
		return grid.Get_Dim();
	}
	int Get_MDim(){
		return grid.Get_MDim();
	}
};
