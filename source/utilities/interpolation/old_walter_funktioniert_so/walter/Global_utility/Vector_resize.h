#pragma once

// Purpose:
// Resizes vectors of arbitrary dimension via functional programming.


// Attention: Dependencies on standard library is not resolved here
// and has to be included elsewhere

//Vector resize - head and tail
template<typename T> void  Vector_Resize(T & Storage, int extension_actual){
	Storage.resize(extension_actual);
}
template<typename T, typename  ... Extensions> void  Vector_Resize(T & Storage, int extension_actual, Extensions... extensions){
	Storage.resize(extension_actual);
	for (int i = 0; i < extension_actual; ++i)
	{
		Vector_Resize(Storage[i],  extensions ... );
	}
}

//Vector resize with initialization value - head and tail
template<typename T, typename t> void  Vector_Resize_init(T & Storage,t init_value, int extension_actual){
	Storage.resize(extension_actual,init_value);
}
template<typename T,typename t, typename  ... Extensions> void  Vector_Resize_init(T & Storage,t init_value, int extension_actual, Extensions... extensions){
	Storage.resize(extension_actual);
	for (int i = 0; i < extension_actual; ++i)
	{
		Vector_Resize_init(Storage[i],init_value,  extensions ... );
	}
}

//Vector reseting all elements to one value - head and tail
template<typename T, typename t> void  Vector_Reset(T & Storage,t init_value, int extension_actual){
	for (int i = 0; i < extension_actual; ++i){
		Storage[i]=init_value;
	}
}
template<typename T,typename t, typename  ... Extensions> void  Vector_Reset(T & Storage,t init_value, int extension_actual, Extensions... extensions){
	for (int i = 0; i < extension_actual; ++i){
		Vector_Reset(Storage[i],init_value,  extensions ... );
	}
}
