#pragma once
#define MAX_THREADS_USED_LOCAL (16)

// Purpose:
// Starts a timer and executes a callable object


// Attention: Dependencies on standard library is not resolved here
// and has to be included elsewhere
/* check if double a is approximately equal to double b. */
// bool
// isApproximatelyEqual (const double a, const double b) {
//     // return fabs (a - b) <= ((fabs (a) < fabs (b) ? fabs (b) : fabs (a)) *
//     //     std::numeric_limits<double>::epsilon ());
//     return std::fabs(a - b) < std::numeric_limits<double>::epsilon()*8.;
// }

void Time_it (std::function<void(void)> const& f )
{
	std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    f();
    end = std::chrono::system_clock::now();

    int elapsed_seconds_mili = std::chrono::duration_cast<std::chrono::milliseconds>
                             (end-start).count();
    int seconds 	=    elapsed_seconds_mili/1000;
    int minutes	 	=    seconds/(60);

	int output_seconds = seconds%60;
	int output_miliseconds = elapsed_seconds_mili%1000;
	std::cout <<"time: " << minutes << " min " << output_seconds + 0.001*output_miliseconds << " sec"<< std::endl;
}
// prints an additional text
void Time_it (std::function<void(void)> const& f, std::string Text)
{
	std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    f();
    end = std::chrono::system_clock::now();
    int elapsed_seconds_mili = std::chrono::duration_cast<std::chrono::milliseconds>
                             (end-start).count();
    int seconds 	=    elapsed_seconds_mili/1000;
    int minutes	 	=    seconds/(60);

	int output_seconds = seconds%60;
	int output_miliseconds = elapsed_seconds_mili%1000;
	std::cout<< Text << "time: " << minutes << " min " << output_seconds + 0.001*output_miliseconds << " sec"<< std::endl;
}
