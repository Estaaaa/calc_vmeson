#pragma once


// Purpose:
// Select nth argument from variadic template function


// Attention: Dependencies on standard library is not resolved here
// and has to be included elsewhere




//***Select nth argument from variadic template function
	namespace detail
	{
	  struct any { template<typename T> any(T &&) {} };

	  template<typename T, typename U> struct first { typedef T type; };

	  template<typename ...Ts>
	  struct select_impl
	  {
	    template<typename U, typename ...Vs>
	 static U &&select(typename first<any, Ts>::type..., U &&u, Vs &&...)
	    {
	    return static_cast<U&&>(u);
	    }
	  };

	  template<std::size_t... Idx, typename... Ts>
	  static auto select(const std::index_sequence<Idx...>&, Ts&&... ts)
	  {
	     return select_impl<decltype(Idx)...>::select(static_cast<Ts&&>(ts)...);
	  }
	}

	template<std::size_t N, typename ...Ts>
	auto nth(Ts &&...ts)
	{
	  return detail::select(std::make_index_sequence<N>(), static_cast<Ts&&>(ts)...);
	}
	//use: auto x = ::nth<7>(0,"1",'2',3,"4",'5',6,"7"); // prints 7
//***Select nth argument from variadic template function
