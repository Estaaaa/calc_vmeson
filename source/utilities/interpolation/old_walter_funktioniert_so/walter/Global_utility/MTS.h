#pragma once


// Purpose:
// Sstruct of arrays for usage in threaded environment


// Attention: Dependencies on standard library is not resolved here
// and has to be included elsewhere




template<typename Datatype> class MTS__
{
	int length=16;
	public:
		Datatype Storage0;
		Datatype Storage1;
		Datatype Storage2;
		Datatype Storage3;
		Datatype Storage4;
		Datatype Storage5;
		Datatype Storage6;
		Datatype Storage7;
		Datatype Storage8;
		Datatype Storage9;
		Datatype Storage10;
		Datatype Storage11;
		Datatype Storage12;
		Datatype Storage13;
		Datatype Storage14;
		Datatype Storage15;

		Datatype & operator [] (int index)
		{
			switch(index)
			{
				case 0:{return Storage0;}
				case 1:{return Storage1;}
				case 2:{return Storage2;}
				case 3:{return Storage3;}
				case 4:{return Storage4;}
				case 5:{return Storage5;}
				case 6:{return Storage6;}
				case 7:{return Storage7;}
				case 8:{return Storage8;}
				case 9:{return Storage9;}
				case 10:{return Storage10;}
				case 11:{return Storage11;}
				case 12:{return Storage12;}
				case 13:{return Storage13;}
				case 14:{return Storage14;}
				case 15:{return Storage15;}
				default:{std::cout <<"Do" << std::endl;return Storage0;}
			}
		}
		int Get_max_thread_num(){
			return length;
		}
		template<typename T>
		void Do_for_all(T && action){
			action(Storage0);
			action(Storage1);
			action(Storage2);
			action(Storage3);
			action(Storage4);
			action(Storage5);
			action(Storage6);
			action(Storage7);
			action(Storage8);
			action(Storage9);
			action(Storage10);
			action(Storage11);
			action(Storage12);
			action(Storage13);
			action(Storage14);
			action(Storage15);
		}
};
