#pragma once
// Standard libabries***********************************************************
#include <cmath>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <limits>
#include <algorithm>
#include <sstream>
#include <string>
#include <functional>
#include <vector>
#include <array>
#include <utility>
#include <atomic>
#include <complex>
#include <type_traits>
#include <sys/types.h>
#include <tuple>
// boost libraries + namespaces*************************************************
#include <boost/iterator/counting_iterator.hpp>
#include "./boost/math/special_functions/binomial.hpp"
#include "./boost/math/special_functions/round.hpp"
#include "./boost/math/special_functions/pow.hpp"
#include "./boost/math/special_functions/asinh.hpp"


// Specific using convention - std**********************************************
using std::array;
using std::vector;
using std::cout;
using std::cerr;
using std::endl;
using std::cin;
using std::string;
using std::complex;
using std::exception;
using std::setprecision;
using std::ios;
using std::fstream;
using std::ifstream;
using std::ofstream;
using std::tuple;
using std::max;
using std::min;
using std::pair;
using std::abs;
using std::pow;
using std::real;
using std::imag;
using std::exp;
using std::log;
using std::log10;
using std::sqrt;
using std::cos;
using std::sin;
using std::tan;
// Specific using convention - boost********************************************
using boost::math::pow;
using boost::math::asinh;
using boost::math::double_constants::pi;
// Aliases**********************************************************************
using _complex = std::complex<double>;
// Complex i********************************************************************
static std::complex<double> II (0.,1.);
