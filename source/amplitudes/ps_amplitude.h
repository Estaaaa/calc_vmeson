//
// Created by esther on 20.10.17.
//

#ifndef CALC_VMESON_PS_AMPLITUDE_H
#define CALC_VMESON_PS_AMPLITUDE_H


#include <SuperIndex.h>
#include <typedefs.h>
#include <ostream>
#include <Phasespace.h>
#include <MultiONew.h>
#include "../parameter/Parameter.h"

template <int NumberOfPartialIndices> class ps_amplitude {

public:


    ps_amplitude(const array<int , NumberOfPartialIndices> &partial_indices): Si(partial_indices)
    {

        eigenvalue=Psquared=NAN; //Setting the parameter to Nan to notices later that they haven't been initialized
        amplitude.resize(Si.getNsuper_index());

//        grid.resize(NumberOfPartialIndices);
//        for (int i = 0; i < NumberOfPartialIndices; ++i) {
//            grid[i].resize(partial_indices[i]);
//        }
    };

    //Operator
    Cdoub& operator ()(array<int,NumberOfPartialIndices> is) {

        return amplitude[ Si.is( is)];
    }

    Cdoub& operator ()(int is) {

        return amplitude[ is];
    }

    bool operator==(const ps_amplitude &rhs) const {
        return amplitude == rhs.amplitude;
    }

    bool operator!=(const ps_amplitude &rhs) const {
        return !(rhs == *this);
    }

    //Getter&Setter
    //Get
    const SuperIndex<NumberOfPartialIndices> &getSi() const {
        return Si;
    }

    const VecCdoub &getAmp() const {
        return amplitude;
    }

    double getPsquared() const {
        return Psquared;
    }

    const Cdoub &getEigenvalue() const {
        return eigenvalue;
    }

    const int getSize() const {
        return Si.getNsuper_index();
    }

    const int getRealSize() const{
       return amplitude.size();
    }


    const int getParticalIndices() const{
        return Si.getNPartial_indices();
    };

    //Set
    void setPsquared(double Psquared) {
        ps_amplitude::Psquared = Psquared;
    }

    void setEigenvalue(const Cdoub &eigenvalue) {
        ps_amplitude::eigenvalue = eigenvalue;
    }

    void setAmplitude(const VecCdoub &amplitude){
//        assert ( Si.getNsuper_index() == amplitude.size());
        if(Si.getNsuper_index() == amplitude.size()){cout<<"Amplitude: Sizes match!"<<endl;}
        ps_amplitude::amplitude = amplitude;
    }
    void set(int i, Cdoub a){
        ps_amplitude::amplitude[i] = a;
    }

    bool isIs_calculated() const {
        return is_calculated;
    }

    void setIs_calculated(bool is_calculated) {
        ps_amplitude::is_calculated = is_calculated;
    }


    void setSi(const SuperIndex<NumberOfPartialIndices> &Si) {
        ps_amplitude::Si = Si;
    }

    //others:
    void resize_amplitude(int size){
        amplitude.resize(size);
    }

    //add and sub represent the amount of collums in the data file that I don't want to include into the data that is read inn..
    //So if the first two rows are the momentum (as usual) then add=1; Since in the loop k=add .. size()/2
    //and if I don't want to inldude the last two collums (standart) then sub=2;
    void read_amplitude(string filename, const int add, const int sub){

        cout<<filename<<endl;
        VecCdoub safe;
        double Psquared_here;

        ifstream read;
        read.open(filename, ios::in);



        if(read.is_open())
        {
            string daten;
            while(getline(read,daten))
            {

                VecDoub dummy; string s = ""; int pos;
                //Here reading in the mass from the output file.
                if(daten[0] == '#') {
                    if(daten[1] == '#')
                    {
                        pos= daten.find("\t");
                        for (int j = pos; j < daten.size(); ++j) {
                            s +=daten[j]; }
                        Psquared_here = stod(s); /// updates the mass in phase space.cpp
                    }else{continue;}
                }else{

                if(daten=="") {continue;}

                //Reading in the amplitude: here I am subtituting 2 because I adddes Psquared and the z at the end of the file.
                for(int j = 0; j <daten.size() ; ++j) {
                    s += daten[j];
                    if(daten[j] == '\t')
                    {
                        dummy.push_back(stod(s));
                        s = "";
                    }
//                if( c <= 'a'  ){}//Check if all of the read in data are numbers.
                }
//            cout<<dummy.size()<<endl;

                for (int k = add; k < (dummy.size()/2 - sub/2 ); ++k) {
                    safe.push_back(dummy[2*k]+i_*dummy[(2*k+1)]);
                }

                if(daten=="") {continue;}
                if(daten[0] == '#') {continue;}



            } }
        }else{cout<<"ps_amplitude::readIn: No data file found!"<<endl; assert( false);}
        read.close();
//        cout<<mass_here<<endl;

        setAmplitude(safe);
        setPsquared(Psquared_here);

        cout<<"A amplitude(meson) has been read inn:"<<filename<<"  with: n_total="<<safe.size()<<"  and P^2="<< getPsquared()
                        <<" ; "<<sqrt(abs(getPsquared()))<<endl;




    }

private:

//    BseParameter parameter;
    SuperIndex<NumberOfPartialIndices> Si;

    VecCdoub amplitude;

    //important: mass here should be Psquared!
    //Note that I recently changed this... 04.08.19
    double Psquared;
    Cdoub eigenvalue;

    //comment: todo do you still need this grid here? Check, get rid of it.
//    matCdoub grid;

    bool is_calculated;


};


//template <int NumberOfPartialIndices> void read_amplitude(string filename, double& mass, ps_amplitude<NumberOfPartialIndices>& amp);


#endif //CALC_VMESON_PS_AMPLITUDE_H
