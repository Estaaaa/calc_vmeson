//
// Created by esther on 04.07.17.
//

#ifndef CALC_BSE_GLUON_H
#define CALC_BSE_GLUON_H


#include "typedefs.h"
#include <string>
#include <array>
#include <assert.h>
#include <memory>

using namespace std;

/**
 * Fuer ein anderes Gluon: Neue Klasse NeuesGluon erstellen, dass von BaseGluon erbt. Dort
 * zwei Funktionen coden. Im Konstruktor vom GluonWrapper die alternative "NeuesGluon" hinzufuegen
 * Fertig.
 */


////////////////////////////////////// BASE GLUON ////////////////////////////////////////
/**
 * Base class. Derive from this for new Gluon
 * Das Gluon braucht die Funktion als double und Cdoub, damit es dynamisch entscheiden kann was zurueck gegeben wird
 */
class BaseGluon{

public:
    virtual ~BaseGluon();

    virtual Cdoub getValue( Cdoub pp) const= 0;
    virtual double getValue( double pp) const = 0;
};

///////////////////////////////////////// DIFFERENT GLUONS ////////////////////////////////
/**
 * MarisTandy Gluon G(q^2)/q^2 und omega = omega_mt^2
 */
class MTGluon : public BaseGluon{

public:
    MTGluon(const vector<double> &params){

        assert( params.size() == 3);

        D        = params[0];
        omega    = params[1];
    }

    MTGluon(double D, double omega) : D(D), omega(omega) {
    }

    Cdoub getValue( Cdoub pp) const{
        return 4.0*M_PI*M_PI*((D/(omega * omega * omega))* exp(-pp/omega) * pp + Fuv(pp)  ) ;
    }

    double getValue( double pp) const{
        return 4.0*M_PI*M_PI*((D/(omega * omega * omega))* exp(-pp/omega)*pp  + Fuv(pp)  ) ;
    }

    Cdoub Fuv(Cdoub pp) const{
        return (gm*((1.0-exp(-pp))/pp))/(0.5*log(tau+(1.0+pp/lamQCD2)*(1.0+pp/lamQCD2))); // hier hat Paul e^-(pp/(4. m_tau^2)), but m_tau = 0.5
    }

    double Fuv(double pp) const{
        return (gm*((1.0-exp(-pp))/pp))/(0.5*log(tau+(1.0+pp/lamQCD2)*(1.0+pp/lamQCD2)));
    }

protected:

    /**
     * model parameters
     */
    double D;
    double omega;

    /**
     * constants, dont touch these
     */
    const double gm=(12.0/(33.0-8.0));
    const double tau=(2.718281828459045235*2.718281828459045235-1.0);
    const double lamQCD2=(0.234*0.234);


};

/**
 * MarisTandy Gluon G(q^2)/q^2 und omega = omega_mt^2 with Pauli Villars regulator
 */
class MtPvGluon : public BaseGluon{

public:
    MtPvGluon(const vector<double> &params){

        assert( params.size() == 3);

        D        = params[0];
        omega    = params[1];
        Cutoff   = params[2];
    }

    MtPvGluon(double D, double omega, double Cutoff) : D(D), omega(omega), Cutoff(Cutoff) {
    }

    Cdoub getValue( Cdoub pp) const{
        if(D==1.0){cin.get();}
        return 4.0*M_PI*M_PI*((D/(omega * omega * omega))* exp(-pp/omega) * pp + Fuv(pp)  ) *1.0/(1.0+pp/Cutoff);
    }

    double getValue( double pp) const{
        if(D==1.0){cin.get();}
        return 4.0*M_PI*M_PI*((D/(omega * omega * omega))* exp(-pp/omega)*pp  + Fuv(pp)  ) *1.0/(1.0+pp/Cutoff);
    }

    Cdoub Fuv(Cdoub pp) const {
        return (gm*((1.0-exp(-pp))/pp))/(0.5*log(tau+(1.0+pp/lamQCD2)*(1.0+pp/lamQCD2)));
    }

    double Fuv(double pp) const {
        return (gm*((1.0-exp(-pp))/pp))/(0.5*log(tau+(1.0+pp/lamQCD2)*(1.0+pp/lamQCD2)));
    }

protected:

    double D;
    double omega;
    double Cutoff;

    const double gm=(12.0/(33.0-8.0));
    const double tau=(2.718281828459045235*2.718281828459045235-1.0);
    const double lamQCD2=(0.234*0.234);

//    const double gm, tau, lamQCD2;

};


//////////////////////////////////////// WRAPPER TO CHOOSE FROM DIFF GLUONS //////////////
class GluonWrapper{

public:
    GluonWrapper(const VecDoub& params, string which_gluon) {

        if( which_gluon == "MT"){

//            gluon = new MTGluon(params);
            gluon = std::make_shared<MTGluon>(MTGluon(params));
        }
        else
         if (which_gluon == "MTPV"){

//            gluon = new MtPvGluon( params);
             gluon = std::make_shared<MtPvGluon>(MtPvGluon(params));
        }
        else{
            assert(false);
        }
    }

    ~GluonWrapper(){

        //delete gluon;
    }

//    array<Cdoub,1> getValue(Cdoub pp)const {
//
//        return {gluon->getValue( pp)};
//    }
//    array<double,1> getValue(double pp)const{
//
//        return {gluon->getValue( pp)};
//    }

    Cdoub getValue(array<Cdoub,1> pp)const {

        return gluon->getValue( pp[0]);
    }
    double getValue(array<double,1> pp)const{

        return gluon->getValue( pp[0]);
    }


    Cdoub getValue(Cdoub pp)const {

        return gluon->getValue( pp);
    }
    double getValue(double pp)const{

        return gluon->getValue( pp);
    }

private:

    shared_ptr<BaseGluon> gluon;
//    BaseGluon  *gluon;
};

#endif //CALC_BSE_GLUON_H

//    GluonWrapper g({0.1, 0.1}, "MT");
