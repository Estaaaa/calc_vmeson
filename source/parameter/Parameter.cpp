//
// Created by esther on 19.10.17.
//

#include <typedefs.h>
#include "Parameter.h"

void DseParameter::read(const string file) {

    string dummy;
    ifstream inn;
    inn.open(file, ios::in);           //DSEreal
    if(inn.is_open())
    {
            inn >> scientific >>dummy >> IRcutsq;
            inn >> scientific >>dummy >> UVcutsq;
            inn >>dummy >> renomalisation_point;
            inn >>dummy >> mass_at_renorm_pt;
            inn >>dummy >> n_rad;
            inn >>dummy >> point_distri_rad;
            inn >>dummy >> func_meassure_rad;
            inn >>dummy >> n_angle;
            inn >>dummy >> point_distri_ang;
            inn >>dummy >> func_meassure_ang;
            inn >>dummy >> solver_tolerance;
            inn >>dummy >> which_gluon;
            inn >>dummy >> d_mt;
            inn >>dummy >> omega_mt;
            inn >>dummy >> pv_cutoff;



    }else{cout<<"dse_parameter::Read: No data file found!"<<endl; assert( false);}
    inn.close();


    cout<<"The prameter file: "<<file<<" inn. "<<" nrad="<<n_rad<<" nangle="<<n_angle<<endl;




}

void DseParameter::write(const string file) {


    ofstream write(file+"cons_dse.txt");
    assert( write.is_open());

    write  <<  scientific << "IRcutsq" << "    " <<  IRcutsq << endl;
    write  <<  scientific << "UVcutsq" << "    " <<  UVcutsq << endl;
    write  << endl;
    write  << "renomalisation_point" << "   " <<  renomalisation_point << endl;
    write  << "mass_at_renorm_pt" << "   " <<  mass_at_renorm_pt << endl;
    write  << endl;
    write  << "n_rad" << "   " <<  n_rad << endl;
    write  << "point_distri_rad" << "   " <<  point_distri_rad << endl;
    write  << "func_rad" << "   " <<  func_meassure_rad << endl;
    write  << endl;
    write  << "n_angle" << "   " <<  n_angle << endl;
    write  << "point_distri_ang" << "   " <<  point_distri_ang << endl;
    write  << "func_ang" << "   " <<  func_meassure_ang << endl;
    write  << endl;
    write  << "solver_tolerance" << "   " <<  solver_tolerance << endl;
    write  << endl;
    write  << "which_gluon" << "   " <<  which_gluon << endl;
    write  << "d_mt" << "   " <<  d_mt << endl;
    write  << "omega_mt" << "   " <<  omega_mt << endl;
    write  << endl;
    write  << "pv_cutoff" << "   " <<  pv_cutoff << endl;







}


void BseParameter::read(const string file) {

    string dummy;
    ifstream inn;
    inn.open(file, ios::in);           //DSEreal
    if(inn.is_open())
    {
        inn >> scientific >>dummy >> IRcutsq;
        inn >> scientific >>dummy >> UVcutsq;

        inn >>dummy >> point_distri_rad;
        inn >>dummy >> func_meassure_rad;

        inn >>dummy >> point_distri_ang1;
        inn >>dummy >> func_meassure_ang1;

        inn >>dummy >> point_distri_ang2;
        inn >>dummy >> func_meassure_ang2;

        inn >>dummy >> n_ll;
        inn >>dummy >> n_lz;
        inn >>dummy >> n_ly;

        inn >>dummy >> n_pp;
        inn >>dummy >> n_pz;

        inn >>dummy >> renomalisation_point;
        inn >>dummy >> mass_at_renorm_pt;
        inn >>dummy >> which_gluon;

        inn >>dummy >> n_basis;
        inn >>dummy >> n_projector;

        inn >>dummy >> n_cheb;

        inn >>dummy >> which_method;


        allgrids_size={n_pp,n_pz,n_ll,n_lz,n_ly};

    }else{cout<<"bse_parameter::Read: No data file found!"<<endl; assert( false);}
    inn.close();


    cout<<"The prameter file: "<<file<<" inn." <<endl;

}

void BseParameter::write(const string folder) {

    ofstream write(folder+"cons_bse.txt");
    assert( write.is_open());


        write <<  scientific << "IRcutsq" << " " <<  IRcutsq << endl;
        write <<  scientific << "UVcutsq" << " " <<  UVcutsq << endl;

        write << endl ;
        write << "point_distri_rad" << "    " <<  point_distri_rad << endl;
        write << "func_meassure_rad" << "    " <<  func_meassure_rad << endl;

        write << endl ;
        write << "point_distri_ang_z" << "    " <<  point_distri_ang1 << endl;
        write << "func_meassure_ang_z" << "    " <<  func_meassure_ang1 << endl;


        write << endl ;
        write << "point_distri_ang_y" << "    " <<  point_distri_ang2 << endl;
        write << "func_meassure_ang_y" << "    " <<  func_meassure_ang2 << endl;

        write << endl ;
        write << "n_ll" << "    " <<  n_ll << endl;
        write << "n_lz" << "    " <<  n_lz << endl;
        write << "n_ly" << "    " <<  n_ly << endl;

        write << "n_pp" << "    " <<  n_pp << endl;
        write << "n_pz" << "    " <<  n_pz << endl;

        write << endl ;
        write << "renorm_point" << "    " <<  renomalisation_point << endl;
        write << "mass_at_renorm_pt" << "    " <<  mass_at_renorm_pt << endl;

        write << endl ;
        write << "which_gluon" << "    " <<  which_gluon << endl;

        write << endl ;
        write << "n_basis" << "    " <<  n_basis << endl;
        write << "n_proj" << "    " <<  n_projector << endl;

        write << endl ;
        write << "n_cheb" << "    " <<  n_cheb << endl;
        write << endl ;
        write << "which_method" << "    " <<  which_method << endl;




}


void TFFParameter::read(const string file) {

    string dummy;
    ifstream inn;
    inn.open(file, ios::in);
    if(inn.is_open())
    {
        inn >> scientific >>dummy >> IRcutsq;
        inn >> scientific >>dummy >> UVcutsq;

        inn >>dummy >> point_distri_rad;
        inn >>dummy >> func_meassure_rad;

        inn >>dummy >> point_distri_ang1;
        inn >>dummy >> func_meassure_ang1;

        inn >>dummy >> point_distri_ang2;
        inn >>dummy >> func_meassure_ang2;

        inn >>dummy >> n_ll;
        inn >>dummy >> n_lz;
        inn >>dummy >> n_ly;

        inn >>dummy >> renomalisation_point;
        inn >>dummy >> mass_at_renorm_pt;
        inn >>dummy >> which_gluon;

        inn >>dummy >> n_cheb;

//        inn >> dummy >>  calc_quark;
//        inn >> dummy >>  calc_bse;
//        inn >> dummy >> calculate_chi;
//        inn >> dummy >>   calculate_S3_flag;
//        inn >> dummy >>  QPV_interpolation;



    }else{cout<<"tff_paramter::Read: No data file found!"<<endl; assert( false);}
    inn.close();


    cout<<"The prameter file: "<<file<<" inn." <<endl;

}

void TFFParameter::write(const string folder) {

    ofstream write(folder+"cons_tff.txt");
    assert( write.is_open());


    write <<  scientific << "IRcutsq" << " " <<  IRcutsq << endl;
    write <<  scientific << "UVcutsq" << " " <<  UVcutsq << endl;

    write << endl ;
    write << "point_distri_rad" << "    " <<  point_distri_rad << endl;
    write << "func_meassure_rad" << "    " <<  func_meassure_rad << endl;

    write << endl ;
    write << "point_distri_ang_z" << "    " <<  point_distri_ang1 << endl;
    write << "func_meassure_ang_z" << "    " <<  func_meassure_ang1 << endl;


    write << endl ;
    write << "point_distri_ang_y" << "    " <<  point_distri_ang2 << endl;
    write << "func_meassure_ang_y" << "    " <<  func_meassure_ang2 << endl;

    write << endl ;
    write << "n_ll" << "    " <<  n_ll << endl;
    write << "n_lz" << "    " <<  n_lz << endl;
    write << "n_ly" << "    " <<  n_ly << endl;

    write << endl ;
    write << "renorm_point" << "    " <<  renomalisation_point << endl;
    write << "mass_at_renorm_pt" << "    " <<  mass_at_renorm_pt << endl;

    write << endl ;
    write << "which_gluon" << "    " <<  which_gluon << endl;

    write << endl ;
    write << "n_cheb" << "    " <<  n_cheb << endl;

    write << endl ;
//    write << "calc_quark" << "    " <<  calc_quark << endl;
//    write << "calc_bse" << "    " <<  calc_bse << endl;
//    write << "calc_chi" << "    " <<  calculate_chi << endl;
//    write << "calc_S3" << "    " <<  calculate_S3_flag << endl;
//    write << "QPV" << "    " <<  QPV_interpolation << endl;

    write.close();



}

//void TFFParameter::read_flags(const string file) {
//
//    string dummy;
//    ifstream inn;
//    inn.open(file, ios::in);
//    if(inn.is_open())
//    {
//        inn >> dummy >>  calc_quark;
//        inn >> dummy >>  calc_bse;
//        inn >> dummy >> calculate_chi;
//        inn >> dummy >>   calculate_S3_flag;
//        inn >> dummy >>  QPV_interpolation;
//
//
//
//    }else{cout<<"flag_file::Read: No data file found!"<<endl; assert( false);}
//    inn.close();
//
//}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void read_general_run_parameter(string file, string& folder, bool& calc_quark ,int& calc_bse , bool& using_gernot_meson , bool& is_the_read_files_in_chebys , int& previous_rad_points_for_interpolation ,
                                 string& which_method , int& calculate_S3_falg , int& calculate_Spm_flag , int& read_quarks_where , int& QPV_interpolation , bool& calculate_tff , double& alpha ,
                                 string& which_meson , double& meson_mass , int& amount_of_q_grid_points , int& grid_sym_falg  , int& append_to_older_calc ){

    string empty;
    ifstream inn;
    inn.open(file, ios::in);           //DSEreal
    if(inn.is_open())
    {

        inn >> empty >> folder;
        inn >> empty >> calc_quark;
        inn >> empty >> calc_bse;
        inn >> empty >> using_gernot_meson;
        inn >> empty >> is_the_read_files_in_chebys;
        inn >> empty >> previous_rad_points_for_interpolation;
        inn >> empty >> which_method;

        inn >> empty >> calculate_S3_falg;
        inn >> empty >> calculate_Spm_flag;
        inn >> empty >> read_quarks_where;
        inn >> empty >> QPV_interpolation;
        inn >> empty >> calculate_tff;
        inn >> scientific >> empty >> alpha;
        inn >> empty >> which_meson;
        inn >> scientific >> empty >> meson_mass;
        inn >> empty >> amount_of_q_grid_points;
        inn >> empty >> grid_sym_falg;
        inn >> empty >> append_to_older_calc;




    }else{cout<<"dse_parameter::Read: No data file found!"<<endl; assert( false);}
    inn.close();


    cout<<"The prameter file: "<<file<<" inn. "<<endl;

}

void write_general_run_parameter(string file, string& folder, bool& calc_quark ,int& calc_bse , bool& using_gernot_meson , bool& is_the_read_files_in_chebys , int& previous_rad_points_for_interpolation ,
                            string& which_method , int& calculate_S3_falg , int& calculate_Spm_flag , int& read_quarks_where , int& QPV_interpolation , bool& calculate_tff , double alpha ,
                            string& which_meson , double& meson_mass , int& amount_of_q_grid_points , int& grid_sym_falg  , int& append_to_older_calc ){

    ofstream write(file);
    assert( write.is_open());
    write << "folder" << tab << folder <<endl<<endl;

    write << "calc_quark" << tab << calc_quark  <<endl<<endl;

    write << "calc_bse" << tab << calc_bse <<endl;
    write << "using_gernot_meson" << tab <<  using_gernot_meson<<endl;
    write << "is_the_read_files_in_chebys" << tab << is_the_read_files_in_chebys <<endl;
    write << "previous_rad_points_for_interpolation" << tab << previous_rad_points_for_interpolation <<endl;
    write << "which_method" << tab << which_method <<endl<<endl;

    write << "calculate_S3_falg" << tab <<  calculate_S3_falg<<endl;
    write << "calculate_Spm_flag" << tab << calculate_Spm_flag <<endl;
    write << "read_quarks_where" << tab << read_quarks_where <<endl<<endl;

    write << "QPV_interpolation" << tab << QPV_interpolation <<endl<<endl;

    write << "calculate_tff" << tab << calculate_tff<<endl<<endl;

    write << scientific <<"alpha" << tab << alpha<<endl<<endl;

    write << "which_meson" << tab << which_meson<<endl;
    write << scientific << "meson_mass" << tab << meson_mass<<endl<<endl;

    write << "amount_of_q_grid_points" << tab << amount_of_q_grid_points<<endl<<endl;

    write << "grid_sym_falg" << tab << grid_sym_falg<<endl;
    write << "append_to_older_calc" << tab << append_to_older_calc <<endl;

    write.close();

}
