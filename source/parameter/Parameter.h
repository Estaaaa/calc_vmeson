//
// Created by esther on 19.10.17.
//

#ifndef CALC_VMESON_PARAMETER_H
#define CALC_VMESON_PARAMETER_H

#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <assert.h>
#include <array>

using namespace std;



class DseParameter{

public:

//    DseParameter(){};

    DseParameter(string file){
        read(file);
    }

    DseParameter(int& n_rad, string point_distri_rad, string func_meassure_rad,
                 int& n_angle, string point_distri_ang, string func_meassure_ang,
                double& UVcutsq, double& IRcutsq, double& renomalisation_point, double& mass_at_renorm_pt,
                double& solver_tolerance, string which_gluon, double& d_mt, double& omega_mt, double& pv_cutoff,
                const string folder ) :

    n_rad(n_rad), point_distri_rad(point_distri_rad), func_meassure_rad(func_meassure_rad),
    n_angle(n_angle), point_distri_ang(point_distri_ang), func_meassure_ang(func_meassure_ang),
    UVcutsq(UVcutsq), IRcutsq(IRcutsq), renomalisation_point(renomalisation_point), mass_at_renorm_pt(mass_at_renorm_pt),
    solver_tolerance(solver_tolerance), which_gluon(which_gluon), d_mt(d_mt), omega_mt(omega_mt), pv_cutoff(pv_cutoff){

        write(folder);
    }

    int n_rad;
    string point_distri_rad, func_meassure_rad;
    int n_angle;
    string point_distri_ang, func_meassure_ang;

    double UVcutsq, IRcutsq;
    double renomalisation_point, mass_at_renorm_pt;

    double solver_tolerance;

    string which_gluon;
    double d_mt;
    double omega_mt;
    double pv_cutoff;

    void read(const string file);
    void write(const string file);


}
;

class BseParameter{

public:

    BseParameter(){};

    BseParameter(const string file){
        read(file);
    }
    BseParameter(int& n_ll, int& n_lz, int& n_ly, int& n_pp, int& n_pz, int& n_cheb, int n_basis, int n_projector,
                string point_distri_rad, string func_meassure_rad, string point_distri_ang1, string func_meassure_ang1,
                 string point_distri_ang2, string func_meassure_ang2,
                double& UVcutsq, double& IRcutsq, double& renomalisation_point, double& mass_at_renorm_pt,
                string  which_gluon, const string folder , const string which_method ) :
    n_ll(n_ll), n_lz(n_lz), n_ly(n_ly), n_pp(n_pp), n_pz(n_pz), n_cheb(n_cheb), n_basis(n_basis), n_projector(n_projector),
    point_distri_rad(point_distri_rad), func_meassure_rad(func_meassure_rad),
    point_distri_ang1(point_distri_ang1), func_meassure_ang1(func_meassure_ang1),
    point_distri_ang2(point_distri_ang2), func_meassure_ang2(func_meassure_ang2),
    UVcutsq(UVcutsq), IRcutsq(IRcutsq), renomalisation_point(renomalisation_point), mass_at_renorm_pt(mass_at_renorm_pt),
    which_gluon(which_gluon), which_method(which_method) {
        write(folder);
    }

    int n_ll, n_lz, n_ly;
    int n_pp, n_pz;
    int n_cheb;

    array<int,5> allgrids_size;

    int n_basis, n_projector;


    string point_distri_rad, func_meassure_rad;
    string point_distri_ang1, func_meassure_ang1;
    string point_distri_ang2, func_meassure_ang2;

    double UVcutsq, IRcutsq;
    double renomalisation_point, mass_at_renorm_pt;


    string which_gluon;
    string which_method;

    void read(const string file);
    void write(const string folder);


}
;


class TFFParameter{

public:

    TFFParameter(){};

    TFFParameter(const string file){
        read(file);
    }

    TFFParameter(int& n_ll, int& n_lz, int& n_ly, int& n_cheb,
                 string point_distri_rad, string func_meassure_rad, string point_distri_ang1, string func_meassure_ang1,
                 string point_distri_ang2, string func_meassure_ang2,
                 double& UVcutsq, double& IRcutsq, double& renomalisation_point, double& mass_at_renorm_pt,
                 string  which_gluon, const string folder) :
//                 const bool calc_quark, const bool calc_bse,
//                 const bool calculate_chi, const bool calculate_S3_flag, const int QPV_interpolation  ) :
            n_ll(n_ll), n_lz(n_lz), n_ly(n_ly), n_cheb(n_cheb),
            point_distri_rad(point_distri_rad), func_meassure_rad(func_meassure_rad),
            point_distri_ang1(point_distri_ang1), func_meassure_ang1(func_meassure_ang1),
            point_distri_ang2(point_distri_ang2), func_meassure_ang2(func_meassure_ang2),
            UVcutsq(UVcutsq), IRcutsq(IRcutsq), renomalisation_point(renomalisation_point), mass_at_renorm_pt(mass_at_renorm_pt),
            which_gluon(which_gluon){
//            calc_quark(calc_quark), calc_bse(calc_bse), calculate_chi(calculate_chi),
//            calculate_S3_flag(calculate_S3_flag), QPV_interpolation(QPV_interpolation){
        write(folder);
    }

//    TFFParameter(int& n_ll, int& n_lz, int& n_ly, int& n_cheb,
//                 string point_distri_rad, string func_meassure_rad, string point_distri_ang1, string func_meassure_ang1,
//                 string point_distri_ang2, string func_meassure_ang2,
//                 double& UVcutsq, double& IRcutsq, double& renomalisation_point, double& mass_at_renorm_pt,
//                 string  which_gluon, const string folder, const string flag_folder ) :
//            n_ll(n_ll), n_lz(n_lz), n_ly(n_ly), n_cheb(n_cheb),
//            point_distri_rad(point_distri_rad), func_meassure_rad(func_meassure_rad),
//            point_distri_ang1(point_distri_ang1), func_meassure_ang1(func_meassure_ang1),
//            point_distri_ang2(point_distri_ang2), func_meassure_ang2(func_meassure_ang2),
//            UVcutsq(UVcutsq), IRcutsq(IRcutsq), renomalisation_point(renomalisation_point), mass_at_renorm_pt(mass_at_renorm_pt),
//            which_gluon(which_gluon){
//        read_flags(flag_folder);
//        write(folder);
//    }

    int n_ll, n_lz, n_ly;
    int n_cheb;



    string point_distri_rad, func_meassure_rad;
    string point_distri_ang1, func_meassure_ang1;
    string point_distri_ang2, func_meassure_ang2;

    double UVcutsq, IRcutsq;
    double renomalisation_point, mass_at_renorm_pt;


    string which_gluon;

    void read(const string file);
    void write(const string folder);

//    void read_flags(const string file);


    //FLAGS

  /*  bool calc_quark;

    //---------> BSE
    bool calc_bse;
    // 0. quark1, 1. quark2, rest not used... suboptimal todo: change.
    array<bool,5> what_to_calculate;

    int calculate_chi;

    //---------> S3, quark at k3: S(k3):
    int calculate_S3_flag;

    //---------> quark-photon vertex QPHV:
    // 1  - redo the calcaution/interpolation using Gernots Fortran routine
    // 2  - just copy a previous interpolation for the interpolation folder
    // 3  - just read Inn already exsisting data.
    // (4 - my vertex calculation ?)
    int QPV_interpolation;*/


}
;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void read_general_run_parameter(string file, string& folder, bool& calc_quark ,int& calc_bse , bool& using_gernot_meson , bool& is_the_read_files_in_chebys , int& previous_rad_points_for_interpolation ,
                                string& which_method , int& calculate_S3_falg , int& calculate_Spm_flag , int& read_quarks_where , int& QPV_interpolation , bool& calculate_tff , double& alpha ,
                                string& which_meson , double& meson_mass , int& amount_of_q_grid_points , int& grid_sym_falg  , int& append_to_older_calc );

void write_general_run_parameter(string file, string& folder, bool& calc_quark ,int& calc_bse , bool& using_gernot_meson , bool& is_the_read_files_in_chebys , int& previous_rad_points_for_interpolation ,
                                string& which_method , int& calculate_S3_falg , int& calculate_Spm_flag , int& read_quarks_where , int& QPV_interpolation , bool& calculate_tff , double alpha ,
                                string& which_meson , double& meson_mass , int& amount_of_q_grid_points , int& grid_sym_falg  , int& append_to_older_calc );

#endif //CALC_VMESON_PARAMETER_H
