//
// Created by Esther Weil on 07.05.18.
//

#ifndef CALC_VMESON_FORMFACTOR_H
#define CALC_VMESON_FORMFACTOR_H


#include <Phasespace.h>
#include <ps_amplitude.h>
#include <fv.h>


typedef void (*FunPtrTFFKernelAmp)( VecCdoub& , const fv<Cdoub>& , const fv<Cdoub>& , const fv<Cdoub>& ,
                                 const fv<Cdoub>& , const fv<Cdoub>& , const fv<Cdoub>& , const fv<Cdoub>&, const fv<Cdoub>&,
                                 const VecCdoub&, const Cdoub&, const Cdoub&, const Cdoub&, const Cdoub&, const Cdoub&, const Cdoub& );

typedef void (*FunPtrTFFKernel)( VecCdoub& , const fv<Cdoub>& , const fv<Cdoub>& , const fv<Cdoub>& ,
                                 const fv<Cdoub>& , const fv<Cdoub>& , const fv<Cdoub>& , const fv<Cdoub>&, const fv<Cdoub>&,
                                 const VecCdoub&, const Cdoub&, const Cdoub& );

typedef void (*FunPtrTFFDenom)(VecCdoub& , const fv<Cdoub>& , const fv<Cdoub>& );


class formfactor {

public:

    formfactor(TFF_ps& ps, string& folder, const double&  alpha, string& which_TFF): ps(ps), folder(folder), alpha(alpha), which_TFF(which_TFF){
                            set_kernel_func(); };

    VecCdoub calc_formfactor(vector<array<Cdoub,2>>& quark1,vector<array<Cdoub,2>>& quark2,
                         vector<array<Cdoub,2>>& quark3, ps_amplitude<3>& bse);

    VecCdoub calc_formfactor_lorenz(vector<array<Cdoub,2>>& quark1,vector<array<Cdoub,2>>& quark2,
                             vector<array<Cdoub,2>>& quark3, ps_amplitude<3>& bse);

    //Debuggin:
    VecCdoub debug_calc_formfactor(vector<array<Cdoub,2>>& quark1,vector<array<Cdoub,2>>& quark2,
                                   vector<array<Cdoub,2>>& quark3, ps_amplitude<3>& bse);

    VecCdoub debug_calc_formfactor_lorenz(vector<array<Cdoub,2>>& quark1,vector<array<Cdoub,2>>& quark2,
            vector<array<Cdoub,2>>& quark3, ps_amplitude<3>& bse);

    void debug_feynman_analytic();

    void second_debug_calc_formfactor();

    void set_kernel_func();

    void write(double Vorfac, VecCdoub& Result, string name);
    void write_ad(double Vorfac, VecCdoub& Result, string name);
    void write_normed(VecCdoub& Result, string name);

public:
    string which_TFF;

private:

    void write(VecCdoub& Result);


    TFF_ps ps;
    FunPtrTFFKernel kernel_func;
    FunPtrTFFKernelAmp kernel_func_amp_mf;
    FunPtrTFFKernelAmp kernel_func_amp_lorenz;
    FunPtrTFFDenom denom_func;
    string folder;
    int number_amps;
    double alpha;


};


/*//functions realted to the form factor but not as part of the class
fv<Cdoub> get_p_vec(Cdoub& Qs, Cdoub& Qps, Cdoub& Ps);

fv<Cdoub> get_p_vec(double& alpha, Cdoub& Qs, Cdoub& Qps, Cdoub& Ps);
fv<Cdoub> get_P_vec(double& alpha, Cdoub& Qs, Cdoub& Qps, Cdoub& Ps);*/



//-------------------------------------------------------------------------------------------------------------------//
//-------------------------------------------------------------------------------------------------------------------//
//Functions in this file, but outside the formfactor class
//-------------------------------------------------------------------------------------------------------------------//



//Additional functions needed for the calcaution of the form factor, but not part of the class.

//Write out the momentum grid on which the vertex should be interpolated.
void output_qphv_grid(string folder,TFF_ps& ps, int which_vertex, double alpha);

//Write out the points in Q² and Q'² where this should be calculated: -the normal version can be found in tff_addons
//This version of settign the grid futhermore takes the limitation of the vertex in, and limits the values of choice to a set that won't complain int he vertex.
void create_Qs_Qps_grid_interpol_boundaries(VecCdoub& Q, VecCdoub& Qp, Cdoub& Ps, double alpha, string& which_interpolation_V1, string& which_interpolation_V2,
                                            TFF_ps& ps, int which_case, double Qps_value, int n_here, string& which_TFF);


//ReadInn the the interpolated vertex data:
bool qphv_interpol_ReadInn(matCdoub &QPV, int m, int which, string folder);


//Calculation of the third quark:
vector<array<Cdoub,2>> calculate_S3(TFF_ps& ps, string& folder, QuarkDse& dse, double alpha);
//Read in complex quark of the form as Sk3:
vector<array<Cdoub,2>> read_complex_Q(string filename);
vector<array<Cdoub,2>> read_complex_Q_new(string filename);

//Writing out a grid for the three quark in the traingle for later use of interpolation in the tff calc.
void write_quark_grid_for_interpol(string& folder, TFF_ps& ps, double alpha, int which_grid);

typedef fv<Cdoub> (*FuncPtrGetMom) (fv<Cdoub>&, fv<Cdoub>&) ;

//Calculation of the quark at k+ and k-:
vector<array<Cdoub,2>> calculate_Spm(TFF_ps& ps, string& folder, QuarkDse& dse, double alpha, FuncPtrGetMom get_m, string tag);


//Corresponding functions for k+ and k-
fv<Cdoub> get_kp(fv<Cdoub>& ll, fv<Cdoub>& P);
fv<Cdoub> get_km(fv<Cdoub>& ll, fv<Cdoub>& P);
fv<Cdoub> get_k3(fv<Cdoub>& ll, fv<Cdoub>& p);
fv<Cdoub> get_rp(fv<Cdoub>& P, fv<Cdoub>& p, fv<Cdoub>& ll);
fv<Cdoub> get_rm(fv<Cdoub>& P, fv<Cdoub>& p, fv<Cdoub>& ll);



//interpolation for the meson data:
void interpolate_this_bse_data(string filename, TFF_ps& ps, int& rad_points_in_readInn_file, bool& is_cheb_file);
VecCdoub cheby_expand_bse_amp(TFF_ps& ps, int& rad_points_in_readInn_file, VecDoub& ang_values, VecCdoub& amps);

//debugging: delete later on.
void dummy_write_S(TFF_ps& ps, string& folder, QuarkDse& dse, double& alpha);


#endif //CALC_VMESON_FORMFACTOR_H
