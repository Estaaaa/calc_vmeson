//
// Created by esther on 24.01.19.
//

#include <Grids.h>
#include "tff_addons.h"


//important: REST FRAME
//get momenta P, p:
//calculate the relative four vector p: (only rest-frame, no alpha parameter)
fv<Cdoub> get_p_vec(Cdoub& Qs, Cdoub& Qps, Cdoub& Ps){

    Cdoub omega = 0.5*(Qs-Qps);
    Cdoub sigma = 0.5*(Qs+Qps) - 0.25*Ps;

    Cdoub a =  sqrt(sigma - omega / Ps);
    Cdoub b= - omega/sqrt(-Ps);

//    Cdoub a =  0.5*sqrt(2.0*(Qs+Qps)-Ps-(Qs*Qs+Qps*Qps)/(Ps)+2.0*(Qs*Qps)/(Ps));
//    Cdoub b=(Qs-Qps)/(2.0*sqrt(-Ps));

    fv<Cdoub> result;
    result.content[2] =  2.0 *a;
    result.content[3] = 2.0 * i_ * b;
//    cout<< "p = ("<<result.content[0]<<","<<result.content[1]<< "," << result.content[2]<<","<<result.content[3]<<")"<<endl;

    return result;
}

//calculating p and P vector and through z,sigma: (only rest-frame!) //important: WATCH OUT THIS IS ONLY VALID IN THE REST FRAME.
fv<Cdoub> get_p_vec(double& z, double& sigma){

    fv<Cdoub> result;
    result.content[2] =  2.0 * sqrt(sigma) *sqrt(1.0-z*z);
    result.content[3] =  2.0 * sqrt(sigma) * z ;

    return result;
}
fv<Cdoub> get_P_vec(Cdoub& Ps){

    fv<Cdoub> result;
    result.content[3] =  sqrt(Ps) ;

    return result;
}


//important: alpha frame
//calculating the p and P vector in different frames: (alpha)
fv<Cdoub> get_p_vec(double alpha, Cdoub& Qs, Cdoub& Qps, Cdoub& Ps){
    // p = 2.0 *Sigma , Sigma = p/2.0;
    // m_pi² = - Ps

    Cdoub omega = 0.5*(Qs-Qps);
    Cdoub sigma = 0.5*(Qs+Qps) - 0.25*Ps;
    Cdoub N = sqrt( sigma + 2.0*alpha*omega + alpha*alpha *Ps);

    fv<Cdoub> result;
    result.content[2] =  2.0/(N) * i_ * alpha * sqrt( omega*omega - sigma*Ps);
    result.content[3] =  2.0/(N)* (sigma +  alpha * omega ) ;
//    cout<< "p = ("<<result.content[0]<<","<<result.content[1]<< "," << result.content[2]<<","<<result.content[3]<<")"<<endl;

    return result;
}
fv<Cdoub> get_P_vec(double alpha, Cdoub& Qs, Cdoub& Qps, Cdoub& Ps){
    // m_pi² = - Ps

    Cdoub omega = 0.5*(Qs-Qps);
    Cdoub sigma = 0.5*(Qs+Qps) - 0.25*Ps;
    Cdoub N = sqrt( sigma + 2.0*alpha*omega + alpha*alpha *Ps);

    fv<Cdoub> result;
    result.content[2] =  - 1.0/(N) * i_  * sqrt( omega*omega - sigma*Ps);
    result.content[3] =  1.0/(N)* (omega + alpha * Ps ) ;
//    cout<< "P = ("<<result.content[0]<<","<<result.content[1]<< "," << result.content[2]<<","<<result.content[3]<<")"<<endl;

    return result;
}


//calculating Q² and Q'² from omega and eta_+
//Cdoub get_Qs(Cdoub& omega, Cdoub& etap){
//    return (omega + etap);
//}


//Getting Q and Q' from P and p - the otherway around:
fv<Cdoub> get_Q(fv<Cdoub>& P,fv<Cdoub>& p ){
    return (0.5 * (p+P));
}
fv<Cdoub> get_Qp(fv<Cdoub>& P,fv<Cdoub>& p ){
    return (0.5 * (p-P));
}



//void create_Qs_Qps_grid(VecCdoub& Q, VecCdoub& Qp, Cdoub& Ps, double alpha, int grid_sym_flag){
//
//    //grid_flag:
//    //(0) : anomaly point, (1) symmetric limit , (2) asymmetric limit
//    if(grid_sym_flag==0){
//        //(0)
//        //For the anomaly point
//        Q.push_back(1e-6);
//        Qp.push_back(1e-6);
//    } else if(grid_sym_flag==1){
//        //(1)
//        //in the symmetric limit up to 100 GeV^2.
////        Line Qline(0.0, 1.0, 20, "linear", "leg", false, false);
//        Line Qline(0.2, 2.0, 20, "linear", "leg", false, false);
//        for (int i = 0; i < Qline.getNumber_of_grid_points(); ++i) {
//            Q.push_back(Qline.getGrid()[i]);
//        }
//        Qp=Q;   //Symmetric!!!!!
//    }else if(grid_sym_flag==2){
//
//        //(2)
//        //asymmetric limit
//        //    Line Qline(0.0, 0.14, 20, "linear", "leg", false, false);
//        //    Line Qline(0.0, meson_mass*meson_mass-0.001, 20, "linear", "leg", false, false);
//
//        //comment: With the new interpolation from Gernot I can not access smaller Momenta anymore.. Watch out.
//
//        Line Qline(0.2, 2.0, 20, "linear", "leg", false, false);
//        for (int i = 0; i < Qline.getNumber_of_grid_points(); ++i) {
//            Q.push_back(Qline.getGrid()[i]);
//            Qp.push_back(0.0);      //setting Q'² to be 0!!!!!
//        }
//    }else if(grid_sym_flag==3){
//
//        //(3) Grid in Q² and Q'²
//
//        //choose amount of Q² points in grid. Will be n_Q x n_Q - Criterial_fails.
//        int n_Q=14;
//        double mps=1.0/4.0;
//
//        Line Qline1(0.2, 2.0, n_Q, "linear", "leg", false, false);
//        Line Qline2(0.25, 2.2, n_Q, "linear", "leg", false, false);
//
//        for (int i = 0; i < n_Q; ++i) {
//
//            for (int j = 0; j < n_Q; ++j) {
//
//
//                Cdoub Qs=Qline1.getGrid()[i];
//                Cdoub Qps=Qline2.getGrid()[j];
//                fv<Cdoub> p = get_p_vec( alpha, Qs,  Qps,  Ps);
//                fv<Cdoub> P = get_P_vec( alpha, Qs,  Qps,  Ps);
//                bool Crit1 = pow(imag(p.content[2])*0.5,2.0)+pow(imag(p.content[3])*0.5,2.0) < mps;
//                bool Crit2 = pow(imag(P.content[2])*0.5,2.0)+pow(imag(P.content[3])*0.5,2.0) < mps;
//                bool Crit3 = Qs != Qps;
//                double epsilon=1.0e-1;
//                bool Crit4 = (real(Qs) < real(Qps*(1.0-epsilon))) || (real(Qs) > real(Qps*(1.0+epsilon)) );
//
//                cout<<epsilon<<tab<<Qs<<tab<< Qps<< tab <<Crit4<<endl;
//
////                cout<<pow(imag(p.content[2])*0.5,2.0)+pow(imag(p.content[3])*0.5,2.0)<<tab<<pow(imag(P.content[2])*0.5,2.0)+pow(imag(P.content[3])*0.5,2.0)<<endl
////                        <<Crit1<<tab<<Crit2<<tab<< (pow(imag(p.content[2])*0.5,2.0)+pow(imag(p.content[3])*0.5,2.0) < mps )<<endl;
//                if(Crit1 && Crit2 && Crit3 && Crit4){
//                    Q.push_back(Qline1.getGrid()[i]);
//                    Qp.push_back(Qline2.getGrid()[j]);
//                }
//
//
//            }
//
//        }
//        cout<<"A Q²/Q² grid was created with "<<Q.size()<<" out of "<<n_Q*n_Q<<" original points and alpha="<<alpha<<endl;
//
//    }
//    else{assert(false);}
//
//
//
//}
//void create_Qs_Qps_grid(VecCdoub& Q, VecCdoub& Qp, Cdoub& Ps, double alpha, int grid_sym_flag, string& which_interpolation_V1, string& which_interpolation_V2){
//
//    //grid_flag:
//    //(0) : anomaly point, (1) symmetric limit , (2) asymmetric limit
//    if(grid_sym_flag==0){
//        //(0)
//        //For the anomaly point
//        Q.push_back(1e-6);
//        Qp.push_back(1e-6);
//
//         which_interpolation_V1= "sQ";
//         which_interpolation_V2= "sQ";
//
//    } else if(grid_sym_flag==1){
//        //(1)
//        //in the symmetric limit up to 100 GeV^2.
////        Line Qline(0.0, 1.0, 20, "linear", "leg", false, false);
//        Line Qline(0.2, 2.0, 20, "linear", "leg", false, false);
//        for (int i = 0; i < Qline.getNumber_of_grid_points(); ++i) {
//            Q.push_back(Qline.getGrid()[i]);
//        }
//        Qp=Q;   //Symmetric!!!!!
//
//        which_interpolation_V1= "sQ";
//        which_interpolation_V2= "sQ";
//        cout<<"Watch out! : choosen symmetric - nt possible with the scalar. "<<endl;
//
//    }else if(grid_sym_flag==2){
//
//        //(2)
//        //asymmetric limit
//        //    Line Qline(0.0, 0.14, 20, "linear", "leg", false, false);
//        //    Line Qline(0.0, meson_mass*meson_mass-0.001, 20, "linear", "leg", false, false);
//
//        //comment: With the new interpolation from Gernot I can not access smaller Momenta anymore.. Watch out.
//
//        Line Qline(0.2, 2.0, 20, "linear", "leg", false, false);
//        for (int i = 0; i < Qline.getNumber_of_grid_points(); ++i) {
//            Q.push_back(Qline.getGrid()[i]);
//            Qp.push_back(0.0);      //setting Q'² to be 0!!!!!
//        }
//
//        which_interpolation_V1= "sQ";
//        which_interpolation_V2= "sl";
//
//    }else if(grid_sym_flag==3){
//
//        //(3) Grid in Q² and Q'²
//
//        //choose amount of Q² points in grid. Will be n_Q x n_Q - Criterial_fails.
//        int n_Q=14;
//        double mps=1.0/4.0;
//
//        Line Qline1(0.4, 2.2, n_Q, "linear", "leg", false, false);
//        Line Qline2(0.35, 2.0, n_Q, "linear", "leg", false, false);
//
//        double epsilon=0.8e-1; double epsilon2=1.0e-1;
//
//        for (int i = 0; i < n_Q; ++i) {
//
//            for (int j = 0; j < n_Q; ++j) {
//
//
//                Cdoub Qs=Qline1.getGrid()[i];
//                Cdoub Qps=Qline2.getGrid()[j];
//                fv<Cdoub> p = get_p_vec( alpha, Qs,  Qps,  Ps);
//                fv<Cdoub> P = get_P_vec( alpha, Qs,  Qps,  Ps);
//                bool Crit1 = pow(imag(p.content[2])*0.5,2.0)+pow(imag(p.content[3])*0.5,2.0) < mps;
//                bool Crit2 = pow(imag(P.content[2])*0.5,2.0)+pow(imag(P.content[3])*0.5,2.0) < mps;
//                bool Crit3 = Qs != Qps;
//                bool Crit4 = (real(Qs) < real(Qps*(1.0-epsilon))) || (real(Qs) > real(Qps*(1.0+epsilon)) );
//                if(real(Qs)<0.6 || real(Qps)<0.6){Crit4 =  (real(Qs) < real(Qps*(1.0-epsilon2))) || (real(Qs) > real(Qps*(1.0+epsilon2)) ); }
//
////                cout<<epsilon<<tab<<Qs<<tab<< Qps<< tab <<Crit4<<tab<< real(Qps*(1.0-epsilon)) << tab <<  real(Qps*(1.0+epsilon))<<endl;
//
////                cout<<pow(imag(p.content[2])*0.5,2.0)+pow(imag(p.content[3])*0.5,2.0)<<tab<<pow(imag(P.content[2])*0.5,2.0)+pow(imag(P.content[3])*0.5,2.0)<<endl
////                        <<Crit1<<tab<<Crit2<<tab<< (pow(imag(p.content[2])*0.5,2.0)+pow(imag(p.content[3])*0.5,2.0) < mps )<<endl;
//                if(Crit1 && Crit2 && Crit3 && Crit4){
//                    Q.push_back(Qline1.getGrid()[i]);
//                    Qp.push_back(Qline2.getGrid()[j]);
//                }
//
//
//            }
//
//        }
//        cout<<"A Q²/Q² grid was created with "<<Q.size()<<" out of "<<n_Q*n_Q<<" original points and alpha="<<alpha<<"  , epsilon="<< epsilon<<" ("<<epsilon2<<")"<<endl;
//
//        which_interpolation_V1= "sl";
//        which_interpolation_V2= "sl";
//
//    }else if(grid_sym_flag==4){
//
//        //(4) Grid in Q² and Q'²
//
//        //choose amount of Q² points in grid. Will be n_Q x n_Q - Criterial_fails.
//        int n_Q=10;
//        double mps=1.0/4.0;
//
//        Line Qline1(1.0e-4, 0.4, n_Q, "linear", "leg", false, false);
//        Line Qline2(1.0e-4, 0.35, n_Q, "linear", "leg", false, false);
//
//        double epsilon=0.8e-1; double epsilon2=1.0e-1;
//
//        for (int i = 0; i < n_Q; ++i) {
//
//            for (int j = 0; j < n_Q; ++j) {
//
//
//                Cdoub Qs=Qline1.getGrid()[i];
//                Cdoub Qps=Qline2.getGrid()[j];
//                fv<Cdoub> p = get_p_vec( alpha, Qs,  Qps,  Ps);
//                fv<Cdoub> P = get_P_vec( alpha, Qs,  Qps,  Ps);
//                bool Crit1 = pow(imag(p.content[2])*0.5,2.0)+pow(imag(p.content[3])*0.5,2.0) < mps;
//                bool Crit2 = pow(imag(P.content[2])*0.5,2.0)+pow(imag(P.content[3])*0.5,2.0) < mps;
//                bool Crit3 = Qs != Qps;
//                bool Crit4 = (real(Qs) < real(Qps*(1.0-epsilon))) || (real(Qs) > real(Qps*(1.0+epsilon)) );
//                if(real(Qs)<0.6 || real(Qps)<0.6){Crit4 =  (real(Qs) < real(Qps*(1.0-epsilon2))) || (real(Qs) > real(Qps*(1.0+epsilon2)) ); }
//
////                cout<<epsilon<<tab<<Qs<<tab<< Qps<< tab <<Crit4<<tab<< real(Qps*(1.0-epsilon)) << tab <<  real(Qps*(1.0+epsilon))<<endl;
//
////                cout<<pow(imag(p.content[2])*0.5,2.0)+pow(imag(p.content[3])*0.5,2.0)<<tab<<pow(imag(P.content[2])*0.5,2.0)+pow(imag(P.content[3])*0.5,2.0)<<endl
////                        <<Crit1<<tab<<Crit2<<tab<< (pow(imag(p.content[2])*0.5,2.0)+pow(imag(p.content[3])*0.5,2.0) < mps )<<endl;
//                if(Crit1 && Crit2 && Crit3 && Crit4){
//                    Q.push_back(Qline1.getGrid()[i]);
//                    Qp.push_back(Qline2.getGrid()[j]);
//                }
//
//
//            }
//
//        }
//        cout<<"A Q²/Q² grid was created with "<<Q.size()<<" out of "<<n_Q*n_Q<<" original points and alpha="<<alpha<<"  , epsilon="<< epsilon<<" ("<<epsilon2<<")"<<endl;
//
//        which_interpolation_V1= "sQ";
//        which_interpolation_V2= "sQ";
//
//   /* }else if(grid_sym_flag==5){
//
//        //(4) Grid in Q² and Q'²
//
//        //choose amount of Q² points in grid. Will be n_Q x n_Q - Criterial_fails.
//        int n_Q=10;
//        double mps=1.0/4.0;
//
//        Line Qline1(1.0e-4, 0.4, n_Q, "linear", "leg", false, false);
//        Line Qline2(1.0e-4, 0.35, n_Q, "linear", "leg", false, false);
//
//        double epsilon=0.8e-1; double epsilon2=1.0e-1;
//        double max_value_of_interpolation = -0.5908602e-01;  //this has to be read off in Gernots Fortran interpoaltion file : in small Q² folder: __iBSE_QPV_complex_transverse.dat
//
//        for (int i = 0; i < n_Q; ++i) {
//
//            for (int j = 0; j < n_Q; ++j) {
//
//
//                Cdoub Qs=Qline1.getGrid()[i];
//                Cdoub Qps=Qline2.getGrid()[j];
//                fv<Cdoub> p = get_p_vec( alpha, Qs,  Qps,  Ps);
//                fv<Cdoub> P = get_P_vec( alpha, Qs,  Qps,  Ps);
//
//                fv<Cdoub> r+= ll+0.25*P+0.25*p;
//                fv<Cdoub> r+= ll+0.25*P-0.25*p;
//
//                bool Crit1 = pow(imag(p.content[2])*0.5,2.0)+pow(imag(p.content[3])*0.5,2.0) < mps;
//                bool Crit2 = pow(imag(P.content[2])*0.5,2.0)+pow(imag(P.content[3])*0.5,2.0) < mps;
//                bool Crit3 = Qs != Qps;
//                bool Crit4 = (real(Qs) < real(Qps*(1.0-epsilon))) || (real(Qs) > real(Qps*(1.0+epsilon)) );
//                if(real(Qs)<0.6 || real(Qps)<0.6){Crit4 =  (real(Qs) < real(Qps*(1.0-epsilon2))) || (real(Qs) > real(Qps*(1.0+epsilon2)) ); }
//                bool Crit5 = real((p*p)) > max_value_of_interpolation;
//
////                cout<<epsilon<<tab<<Qs<<tab<< Qps<< tab <<Crit4<<tab<< real(Qps*(1.0-epsilon)) << tab <<  real(Qps*(1.0+epsilon))<<endl;
//
////                cout<<pow(imag(p.content[2])*0.5,2.0)+pow(imag(p.content[3])*0.5,2.0)<<tab<<pow(imag(P.content[2])*0.5,2.0)+pow(imag(P.content[3])*0.5,2.0)<<endl
////                        <<Crit1<<tab<<Crit2<<tab<< (pow(imag(p.content[2])*0.5,2.0)+pow(imag(p.content[3])*0.5,2.0) < mps )<<endl;
//                if(Crit1 && Crit2 && Crit3 && Crit4 && Crit5){
//                    Q.push_back(Qline1.getGrid()[i]);
//                    Qp.push_back(Qline2.getGrid()[j]);
//                }
//
//
//            }
//
//        }
//        cout<<"A Q²/Q² grid was created with "<<Q.size()<<" out of "<<n_Q*n_Q<<" original points and alpha="<<alpha<<"  , epsilon="<< epsilon<<" ("<<epsilon2<<")"<<endl;
//
//        which_interpolation_V1= "sQ";
//        which_interpolation_V2= "sQ";*/
//
//    }
//    else{assert(false);}
//
//}
