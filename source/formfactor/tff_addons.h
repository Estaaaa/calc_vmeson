//
// Created by esther on 24.01.19.
//

#ifndef CALC_VMESON_TFF_ADDONS_H
#define CALC_VMESON_TFF_ADDONS_H

#include <typedefs.h>
#include <fv.h>


//functions realted to the form factor but not as part of the class

//rest frame only:
fv<Cdoub> get_p_vec(Cdoub& Qs, Cdoub& Qps, Cdoub& Ps);

fv<Cdoub> get_p_vec(double& z, double& sigma);
fv<Cdoub> get_P_vec(Cdoub& Ps);


//alpha frames
fv<Cdoub> get_p_vec(double alpha, Cdoub& Qs, Cdoub& Qps, Cdoub& Ps);
fv<Cdoub> get_P_vec(double alpha, Cdoub& Qs, Cdoub& Qps, Cdoub& Ps);


//Getting Q and Q' fourvectors from P and p fourvectors:
fv<Cdoub> get_Q(fv<Cdoub>& P,fv<Cdoub>& p );
fv<Cdoub> get_Qp(fv<Cdoub>& P,fv<Cdoub>& p );


//Function to create the grid on where to calculate. // -> Are now in the formfactor.cpp
//void create_Qs_Qps_grid(VecCdoub& Q, VecCdoub& Qp, Cdoub& Ps, double alpha, int grid_sym_flag);
//void create_Qs_Qps_grid(VecCdoub& Q, VecCdoub& Qp, Cdoub& Ps, double alpha, int grid_sym_flag, string& which_interpolation_V1, string& which_interpolation_V2);


#endif //CALC_VMESON_TFF_ADDONS_H
