#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
#include "tff_kernel_bare.h"
#include <fv.h>

void kernel_formfactor_pion(VecCdoub& kern,
const fv<Cdoub>& P, const fv<Cdoub>& Q, const fv<Cdoub>& Qp,
const fv<Cdoub>& p, const fv<Cdoub>& l, const fv<Cdoub>& rp,
const fv<Cdoub>& rm ,
const Cdoub& f1, const Cdoub& f2, const Cdoub& f3,
const Cdoub& f4, const Cdoub& A3, const Cdoub& B3, 
const Cdoub& A1, const Cdoub& B1, const Cdoub& A2,
const Cdoub& B2){


//fourvector products
Cdoub P_P = P*P; Cdoub P_Q = P*Q; Cdoub P_Qp = P*Qp;
Cdoub P_p = P*p; Cdoub P_l = P*l; Cdoub P_rp = P*rp;
Cdoub P_rm = P*rm; Cdoub Q_Q = Q*Q; Cdoub Q_Qp = Q*Qp;
Cdoub p_Q = Q*p; Cdoub l_Q = Q*l; Cdoub rp_Q = Q*rp;
Cdoub rm_Q = Q*rm; Cdoub Qp_Qp = Qp*Qp; Cdoub p_Qp = Qp*p;
Cdoub l_Qp = Qp*l; Cdoub rp_Qp = Qp*rp; Cdoub rm_Qp = Qp*rm;
Cdoub p_p = p*p; Cdoub p_l = p*l; Cdoub p_rp = p*rp;
Cdoub p_rm = p*rm; Cdoub l_l = l*l; Cdoub l_rp = l*rp;
Cdoub l_rm = l*rm; Cdoub rp_rp = rp*rp; Cdoub rp_rm = rp*rm;
Cdoub rm_rm = rm*rm; 


    // local variables
    array<Cdoub, 13> w;


    w[1]=P_P;
    w[2]=p_p;
    w[3]=p_l;
    w[4]=P_p;
    w[5]=P_l;
   w[6]=w[4] + w[5];
   w[7]=2.E+0*w[3];
   w[8]=w[6] + w[7];
   w[8]=w[8]*w[4];
   w[9]=w[1]*w[2];
   w[10]=w[3]*w[1];
   w[9]=w[9] + w[10];
   w[11]=w[5]*w[2];
   w[11]=2.E+0*w[11];
   w[8]=w[8] - w[9] - w[11];
   w[8]=w[8]*A3*B2;
   w[12]=w[4]*w[5];
   w[10]=w[12] - w[10];
   w[12]=B3*A2;
   w[12]=2.E+0*w[12];
   w[10]=w[10]*w[12];
   w[8]=w[8] - w[10];
   w[8]=w[8]*A1;
   w[6]=w[6] - w[7];
   w[6]=w[6]*w[4];
   w[6]=w[6] - w[9] + w[11];
   w[6]=w[6]*A2*A3*B1;
   w[6]=w[8] + w[6];
   w[6]=f1*w[6];
   w[6]=2.E+0*w[6];

    kern[0] =   - w[6];
 
    kern[1] =   - w[6];
 
    kern[2] =   - w[6];
 
    kern[3] =   - w[6];
 
    kern[4] =   - w[6];
 
    kern[5] =   - w[6];
 
    kern[6] =   - w[6];
 
    kern[7] =   - w[6];
 
    kern[8] =   - w[6];
 
    kern[9] =   - w[6];
 
    kern[10] =   - w[6];
 
    kern[11] =   - w[6];
 
    kern[12] =   - w[6];
 
    kern[13] =   - w[6];
 
    kern[14] =   - w[6];
 
    kern[15] =   - w[6];
 
    kern[16] =   - w[6];
 
    kern[17] =   - w[6];
 
    kern[18] =   - w[6];
 
    kern[19] =   - w[6];
 
    kern[20] =   - w[6];
 
    kern[21] =   - w[6];
 
    kern[22] =   - w[6];
 
    kern[23] =   - w[6];
 
    kern[24] =   - w[6];
 
    kern[25] =   - w[6];
 
    kern[26] =   - w[6];
 
    kern[27] =   - w[6];
 
    kern[28] =   - w[6];
 
    kern[29] =   - w[6];
 
    kern[30] =   - w[6];
 
    kern[31] =   - w[6];
 
    kern[32] =   - w[6];
 
    kern[33] =   - w[6];
 
    kern[34] =   - w[6];
 
    kern[35] =   - w[6];
 
    kern[36] =   - w[6];
 
    kern[37] =   - w[6];
 
    kern[38] =   - w[6];
 
    kern[39] =   - w[6];
 
    kern[40] =   - w[6];
 
    kern[41] =   - w[6];
 
    kern[42] =   - w[6];
 
    kern[43] =   - w[6];
 
    kern[44] =   - w[6];
 
    kern[45] =   - w[6];
 
    kern[46] =   - w[6];
 
    kern[47] =   - w[6];
 
    kern[48] =   - w[6];
 
    kern[49] =   - w[6];
 
    kern[50] =   - w[6];
 
    kern[51] =   - w[6];
 
    kern[52] =   - w[6];
 
    kern[53] =   - w[6];
 
    kern[54] =   - w[6];
 
    kern[55] =   - w[6];
 
    kern[56] =   - w[6];
 
    kern[57] =   - w[6];
 
    kern[58] =   - w[6];
 
    kern[59] =   - w[6];
 
    kern[60] =   - w[6];
 
    kern[61] =   - w[6];
 
    kern[62] =   - w[6];
 
    kern[63] =   - w[6];
 
    kern[64] =   - w[6];
 
    kern[65] =   - w[6];
 
    kern[66] =   - w[6];
 
    kern[67] =   - w[6];
 
    kern[68] =   - w[6];
 
    kern[69] =   - w[6];
 
    kern[70] =   - w[6];
 
    kern[71] =   - w[6];
 
    kern[72] =   - w[6];
 
    kern[73] =   - w[6];
 
    kern[74] =   - w[6];
 
    kern[75] =   - w[6];
 
    kern[76] =   - w[6];
 
    kern[77] =   - w[6];
 
    kern[78] =   - w[6];
 
    kern[79] =   - w[6];
 
    kern[80] =   - w[6];
 
    kern[81] =   - w[6];
 
    kern[82] =   - w[6];
 
    kern[83] =   - w[6];
 
    kern[84] =   - w[6];
 
    kern[85] =   - w[6];
 
    kern[86] =   - w[6];
 
    kern[87] =   - w[6];
 
    kern[88] =   - w[6];
 
    kern[89] =   - w[6];
 
    kern[90] =   - w[6];
 
    kern[91] =   - w[6];
 
    kern[92] =   - w[6];
 
    kern[93] =   - w[6];
 
    kern[94] =   - w[6];
 
    kern[95] =   - w[6];
 
    kern[96] =   - w[6];
 
    kern[97] =   - w[6];
 
    kern[98] =   - w[6];
 
    kern[99] =   - w[6];
 
    kern[100] =   - w[6];
 
    kern[101] =   - w[6];
 
    kern[102] =   - w[6];
 
    kern[103] =   - w[6];
 
    kern[104] =   - w[6];
 
    kern[105] =   - w[6];
 
    kern[106] =   - w[6];
 
    kern[107] =   - w[6];
 
    kern[108] =   - w[6];
 
    kern[109] =   - w[6];
 
    kern[110] =   - w[6];
 
    kern[111] =   - w[6];
 
    kern[112] =   - w[6];
 
    kern[113] =   - w[6];
 
    kern[114] =   - w[6];
 
    kern[115] =   - w[6];
 
    kern[116] =   - w[6];
 
    kern[117] =   - w[6];
 
    kern[118] =   - w[6];
 
    kern[119] =   - w[6];
 
    kern[120] =   - w[6];
 
    kern[121] =   - w[6];
 
    kern[122] =   - w[6];
 
    kern[123] =   - w[6];
 
    kern[124] =   - w[6];
 
    kern[125] =   - w[6];
 
    kern[126] =   - w[6];
 
    kern[127] =   - w[6];
 
    kern[128] =   - w[6];
 
    kern[129] =   - w[6];
 
    kern[130] =   - w[6];
 
    kern[131] =   - w[6];
 
    kern[132] =   - w[6];
 
    kern[133] =   - w[6];
 
    kern[134] =   - w[6];
 
    kern[135] =   - w[6];
 
    kern[136] =   - w[6];
 
    kern[137] =   - w[6];
 
    kern[138] =   - w[6];
 
    kern[139] =   - w[6];
 
    kern[140] =   - w[6];
 
    kern[141] =   - w[6];
 
    kern[142] =   - w[6];
 
    kern[143] =   - w[6];
 

}

