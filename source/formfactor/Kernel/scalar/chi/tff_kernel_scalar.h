//
// Created by Esther Weil on 08.05.18.
//

#ifndef CALC_VMESON_TFF_KERNEL_SCALAR_H
#define CALC_VMESON_TFF_KERNEL_SCALAR_H


#include <typedefs.h>
#include <fv.h>


void kernel_formfactor_scalar(VecCdoub& kern,
                              const fv<Cdoub>& P, const fv<Cdoub>& Q, const fv<Cdoub>& Qp,
                              const fv<Cdoub>& p, const fv<Cdoub>& l, const fv<Cdoub>& rp,
                              const fv<Cdoub>& rm , const fv<Cdoub>& k3,
                              const VecCdoub& meson,
                              const Cdoub& A3, const Cdoub& B3 );


void get_donorminator_scalar(VecCdoub& denom,
                             const fv<Cdoub>& Q, const fv<Cdoub>& Qp);

#endif //CALC_VMESON_TFF_KERNEL_SCALAR_H
