//
// Created by Esther Weil on 08.05.18.
//

#ifndef CALC_VMESON_TFF_KERNEL_SCALAR_AMP_LORENZ_H
#define CALC_VMESON_TFF_KERNEL_SCALAR_AMP_LORENZ_H


#include <typedefs.h>
#include <fv.h>

void kernel_formfactor_scalar_amp_lorenz(VecCdoub& kern,
                            const fv<Cdoub>& P, const fv<Cdoub>& Q, const fv<Cdoub>& Qp,
                            const fv<Cdoub>& p, const fv<Cdoub>& l, const fv<Cdoub>& rp,
                            const fv<Cdoub>& rm , const fv<Cdoub>& k3,
                            const VecCdoub& meson,
                            const Cdoub& A3, const Cdoub& B3,
                            const Cdoub& A1, const Cdoub& B1,
                            const Cdoub& A2, const Cdoub& B2);

void realting_f_with_lorenz(VecCdoub& fprime, const fv<Cdoub>& p1, const fv<Cdoub>& p2, VecCdoub& f);

#endif //CALC_VMESON_TFF_KERNEL_SCALAR_AMP_LORENZ_H
