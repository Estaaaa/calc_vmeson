//
// Created by Esther Weil on 07.05.18.
//

#include <typedefs.h>
#include <fv.h>
#include <SuperIndex.h>
#include <QuarkDse.h>
#include <Kernel/ps/tff_kernel_ps.h>
#include <Kernel/ps/tff_kernel_ps_amp_mf.h>
#include <Kernel/axial/tff_kernel_axial.h>
#include <Kernel/scalar/chi/tff_kernel_scalar.h>
#include <Kernel/scalar/amp/tff_kernel_scalar_amp_mf.h>
#include <Kernel/scalar/easy/tff_kernel_scalar_amp_mf.h>
//#include <Kernel/bare/tff_kernel_bare.h>
#include <Kernel/scalar/lorenz/tff_kernel_scalar_amp_lorenz.h>
#include "formfactor.h"
#include "tff_addons.h"
#include <BaseInterpolPE.h>
#include <cheb.h>


///FLAGS all in here:

#include "../input_header/input_constants_tff.h"

VecCdoub formfactor::calc_formfactor(vector<array<Cdoub,2>>& quark1,vector<array<Cdoub,2>>& quark2,
                                         vector<array<Cdoub,2>>& quark3, ps_amplitude<3>& bse){


    //Variables of this function:
    array<Cdoub, 12> V1;
    array<Cdoub, 12> V2;

    int n_p=ps.Q_Qp_P.size();

    //important: Debuggin
    vector<vector<Cdoub>> all_denom(n_p,VecCdoub(number_amps,0.0));

    //Superindices:
    SuperIndex<4> Si_quark3({ps.ll.getNumber_of_grid_points(), ps.zl.getNumber_of_grid_points(),
                             ps.yl.getNumber_of_grid_points(), n_p});

    SuperIndex<2> Si_quark12({ps.ll.getNumber_of_grid_points(), ps.zl.getNumber_of_grid_points()});

    SuperIndex<4> Si_vertex({n_p, ps.yl.getNumber_of_grid_points(),  ps.zl.getNumber_of_grid_points(),
                             ps.ll.getNumber_of_grid_points()  });


    //important: DEBUGGING here- not used atm
    //obtaining the vertex values from Gernots interpolation -later mine
    matCdoub QPV1;
    matCdoub QPV2;

    int all_N= ps.ll.getNumber_of_grid_points()* ps.zl.getNumber_of_grid_points()*ps.yl.getNumber_of_grid_points()*n_p;

//    int test=qphv_interpol_ReadInn(QPV1,all_N,1,folder);
//    if(test==0){cout<<"Calction Aborted QPV not found"<<endl;assert(false);}
//    test=qphv_interpol_ReadInn(QPV2,all_N,2,folder);
//    if(test==0){cout<<"Calction Aborted QPV not found"<<endl; assert(false);}




    //Prefactor of the form factor integral, comes form the integration factor     ->  the symmetry factors of 2 are included is now below because of the differnt mesons.
    double Vorfac=1.0/((2*M_PI)*(2*M_PI)*(2*M_PI));

    SuperIndex<2> Si_tff({n_p,number_amps});
    VecCdoub Result; Result.resize(Si_tff.getNsuper_index());


////-------------------- start of loops ------------------------------------
    //Sum over all values of the relative momentum p. To safe all values
    for (int i_p = 0; i_p < n_p ; ++i_p) {


        //Calling the fourvectors from Q², Q'², P²:
        fv<Cdoub> p = get_p_vec(alpha, ps.Q_Qp_P[i_p][0],ps.Q_Qp_P[i_p][1], ps.Q_Qp_P[i_p][2]);
        fv<Cdoub> P = get_P_vec(alpha, ps.Q_Qp_P[i_p][0],ps.Q_Qp_P[i_p][1], ps.Q_Qp_P[i_p][2]);

        //important:
        //////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //----------- Watch out: Setting the realtion for Q and Q' here -----------
        //(maybe get into a function somewhere external at some points?)
        fv<Cdoub> Q, Qp;
        Q= 0.5*(p+P);
        Qp= 0.5*(p-P);
        //////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


        VecCdoub z_integration_sum(number_amps,0.0); //init with 0
        //INTEGRATION- loop start: zl
        //angular integration form the loop integration in l. (z_l)
        for (int i_zl = 0; i_zl < ps.zl.getNumber_of_grid_points(); ++i_zl) {


            VecCdoub ll_integration_sum(number_amps,0.0); //init with 0
            //INTEGRATION- loop start: ll
            //radial integration for the loop integration in l. (ll)
            for (int i_ll = 0; i_ll < ps.ll.getNumber_of_grid_points() ; ++i_ll) {


#ifndef K_USING_ALPHA_FRAME

                //important: Changed call for quark here cause now they depend on more. See above. In alpha!=big case.
                Cdoub Ak1= quark1[Si_quark12.is({i_ll, i_zl})][0];
                Cdoub Bk1= quark1[Si_quark12.is({i_ll, i_zl})][1];
                Cdoub Ak2= quark2[Si_quark12.is({i_ll, i_zl})][0];
                Cdoub Bk2= quark2[Si_quark12.is({i_ll, i_zl})][1];
#endif


                //Call + safe the amplitudes from the meson1
                //The loop runs till the size of Tensor Elements that the meson contains. (scalar 4, vector 8)
                VecCdoub meson;
                for (int amp = 0; amp < bse.getSi().getMaxima_of_partial_indices()[0]; ++amp) {
                    meson.push_back(bse( bse.getSi().is({amp,i_ll, i_zl}) ));
                }



                VecCdoub y_integration_sum(number_amps,0.0); //init with 0

                //INTEGRATION- loop start: yl
                //angular integration for the loop integration in l. (y_l)
                for (int i_yl = 0; i_yl < ps.yl.getNumber_of_grid_points(); ++i_yl) {


                    //Setting up the four vectors
                    fv<Cdoub> ll, rp, rm, k3, k1, k2;
                    ll.set_spherical(ps.ll.getGrid()[i_ll],0.0,ps.yl.getGrid()[i_yl],ps.zl.getGrid()[i_zl]);
                    rp =  get_rp(P,p,ll);
                    rm =  get_rm(P,p,ll);
                    k1=get_kp(ll, P);
                    k2=get_km(ll, P);
                    k3=get_k3(ll,p);
//                    rp =  ll + 0.25*p + 0.25*P;
//                    rm =  ll + 0.25*p - 0.25*P;
//                    k3 = ll + 0.5*p;
//                    k1 = ll + 0.5*P;
//                    k2 = ll - 0.5*P;


#ifdef K_USING_ALPHA_FRAME
                    //important the other change is here: when using alpha use the following -
                    Cdoub Ak1= quark1[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][0];
                    Cdoub Bk1= quark1[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][1];
                    Cdoub Ak2= quark2[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][0];
                    Cdoub Bk2= quark2[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][1];
#endif

                    Cdoub Ak3 = quark3[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][0];
                    Cdoub Bk3 = quark3[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][1];


                    //QUARK DENORMINATORS: - (TFFs ones below, as no dependence on inside loops.)
                    //Calculate the Denorminators of the quark propagators included!!!
                    //Frist is the denorminato of the 3rd Quark propagator,
                    // second the denorminator with form TFF projector, including the photon momenta.
                    //(The denorminator of S1 and S2 is already included in Chi.) - not anymore - changed that due to comparison.
                    //frist we start with the terms form the quark propagators
                    Cdoub quark_denom;
                    quark_denom = (1.0/((k3*k3)*Ak3*Ak3+Bk3*Bk3)) *   //(1.0/(2.0*((Q*Q)*(Qp*Qp)-pow(Q*Qp,2))))
                                  (1.0/((k1*k1)*Ak1*Ak1+Bk1*Bk1))*(1.0/((k2*k2)*Ak2*Ak2+Bk2*Bk2));

//                    quark_denom = 1.0;


                    //----------------------------------------------------------------------------
                    //Get the vertex dressing functions
                    Cdoub x1= 0.5*(Ak3+Ak1);
                    Cdoub x2= 0.5*(Ak3+Ak2);
                    Cdoub y1= (Ak3-Ak1)/(k3*k3-k1*k1);
                    Cdoub y2= (Ak2-Ak3)/(k2*k2-k3*k3);
                    Cdoub z1= (Bk3-Bk1)/(k3*k3-k1*k1);
                    Cdoub z2= (Bk2-Bk3)/(k2*k2-k3*k3);

//                    x1=1.0; y1=1.0; z1=1.0;
//                    x2=1.0; y2=1.0; z2=1.0;

                    //these are only the transverse components.
                    // The longitudinal are taken form the quark propagators aka Ball Chui part with WTI.
                    //-change to the array V1 and V2
                    V1[0] =  x1;
                    V1[1] =  2.0 *y1;
                    V1[2] =  2.0 *z1;
                    V1[3] = 0.0;

//                    V1[4]=QPV1[0][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
//                    V1[5]=QPV1[1][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
//                    V1[6]=QPV1[2][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
//                    V1[7]=QPV1[3][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
//                    V1[8]=QPV1[4][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
//                    V1[9]=QPV1[5][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
//                    V1[10]=QPV1[6][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
//                    V1[11]=QPV1[7][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];

                    //These are only relevant for the new trace expression as the old one uses x,y,z directly
                    V2[0] = x2;
                    V2[1] =  2.0 *y2;
                    V2[2] =  2.0 *z2;
                    V2[3] = 0.0;

//                    V2[4]=QPV2[0][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
//                    V2[5]=QPV2[1][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
//                    V2[6]=QPV2[2][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
//                    V2[7]=QPV2[3][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
//                    V2[8]=QPV2[4][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
//                    V2[9]=QPV2[5][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
//                    V2[10]=QPV2[6][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
//                    V2[11]=QPV2[7][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];

                    //----------------------------------------------------------------------------

                    //important: DEBUGGING HERE - setting values to 1.0:
                    V1[4]=V1[5]=V1[6]=V1[7]=V1[8]=V1[9]=V1[10]=V1[11]=V1[12]=0.0;
                    V2[4]=V2[5]=V2[6]=V2[7]=V2[8]=V2[9]=V2[10]=V2[11]=V2[12]=0.0;

//                    V1[1]=V1[2]=V1[3]=0; V1[0] = 1.0; //V1[0] =0.9843;
//                    V2[1]=V2[2]=V2[3]=0; V2[0] = 1.0; //V2[0] =0.9843;


                    //Calling the final trace expression inlcuding all the components together.
                    //The information will be safed in a long vector order in terms of the two quark-photon vertices
                    // Such that the total vector has 12x12= 144 compontens
                    // Inner loop is V2 (rm, -Q) and the outter V1 (rp,Qp) : V(1.....12) = f1/0*f2/1....f2/12
                    VecCdoub kern; kern.resize(12*12*number_amps);

/*

//                    cout<<Ak1<<tab<<Bk1<<tab<<Ak2<<tab<<Bk2<<tab<<k3*k3<<tab<<k1*k1<<endl;
//                    cout<<x1<<tab<<y2<<tab<<V1[4]<<tab<<Ak3<<tab<<meson_f1 <<endl;
//                    cout<<y1<<tab<<Ak1<<tab<<Ak3<<tab<<k1*k1<<tab<<k3*k3<<tab<<endl;
//                    cout<<y2<<tab<<Ak2<<tab<<Ak3<<tab<<k2*k2<<tab<<k3*k3<<tab<<endl;
//                    cout<<z1<<tab<<Bk1<<tab<<Bk3<<tab<<k1*k1<<tab<<k3*k3<<tab<<endl;
//                    cout<<z2<<tab<<Bk2<<tab<<Bk3<<tab<<k2*k2<<tab<<k3*k3<<tab<<endl;
//                    cout<<"---> "<<V1[11]<<tab<<V2[7]<<tab<<V1[6]<<tab<<V2[10]<<endl;
//                    cout<< k3*ll <<tab<< Q*Qp <<tab<<rm* rm <<tab<<rm *rp <<tab<< rp*rp <<endl;

//                    cout<<x1<<tab<<y1<<tab<<z1<<tab<<x2<<tab<<y2<<tab<<z2<<tab<<V1[4]<<tab<<V1[5]<<tab<<V1[6]<<tab<<V1[7]<<tab<<V1[8]<<tab<<V1[9]<<tab<<V1[10]<<tab<<V1[11]<<tab<<V2[4]<<tab<<V2[5]<<tab<<V2[6]<<tab<<V2[7]<<tab<<V2[8]<<tab<<V2[9]<<tab<<V2[10]<<tab<<V2[11]<<tab<<
//                            Ak3<<tab<<Bk3<<tab<< meson_f1<<tab<< meson_f2<<tab<< meson_f3<<tab<< meson_f4<<endl;

//                    a_k3.write();
//                    a_Qp.write();
//                    a_Q.write();
//                    a_P.write();
//                    a_ll.write();
//                    a_p.write();
//                    a_rp.write();
//                    a_rm.write();

//                    cout<<V1[4]<<tab<<V1[5]<<tab<<V1[6]<<tab<<V1[7]<<tab<<V1[8]<<tab<<V1[9]<<tab<<V1[10]<<tab<<V1[11]<<tab<<V2[4]<<tab<<V2[5]<<tab<<V2[6]<<tab<<V2[7]<<
//                        tab<<V2[8]<<tab<<V2[9]<<tab<<V2[10]<<tab<<V2[11]<<endl;

//                    cout<<"-----------------------"<<endl
//                        <<k3*k3<<tab<<k1*k1<<tab<<k2*k2<<tab<<Ak1<<tab<<Bk1<<tab<<Ak2<<tab<<Bk2<<endl<<endl;
*/


                    //#########################################################################################

                    //important !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    //important !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    //original function:  ---- normally used ---------
                    //in order to get the right answer the pharsing of the vertex elements has to be right p yl,zl,ll
                    //and then also accessing of the vertex data in V1[Si(0)], V2[Si(1)]. Aka kern[i]. i<122.
//                    kernel_func(kern, P, Q, Qp, p, ll, rp, rm, k3, meson, Ak3, Bk3);
                    kernel_func_amp_mf(kern, P, Q, Qp, p, ll, rp, rm, k3, meson, Ak3, Bk3, Ak1, Bk1, Ak2, Bk2);
                    //Gave the right result on 01.06.2018 for the anomaly value.


                    //------------------------OTHER VERSION----------------------

                    //for the bare version - A1, B1.. are also needed:
//                    kernel_func(kern, P, Q, Qp, p, ll, rp, rm, meson_f1, meson_f2, meson_f3, meson_f4, Ak3, Bk3,
//                    Ak1, Bk1, Ak2, Bk2);


                    //Old trace:
                    //Old trace expression form old program for comparison : - worked 29.05.18 and then not anymore 01.07 :/
                    //worked again: problem was the read inn routine form the bse. - fix this
                    //furthermore now the vertex gets accessed the right way, such that the new and the old version give the same answer.
//                    trace_decay_qph(Chi,x1,y1,z1,x2,y2,z2,V1[4],V1[5],V1[6],V1[7],V1[8],V1[9],V1[10],V1[11],V2[4],V2[5],V2[6],V2[7],V2[8],V2[9],V2[10],V2[11],
//                                    Ak3,Bk3,a_k3,a_Qp,a_Q,a_P,a_ll,a_p,a_rp,a_rm, meson_f1, meson_f2, meson_f3, meson_f4);

                    //Old trace debug:
                    //for debuggin set to 1.0:
//                    Cdoub v=1.0; Cdoub v2=0.5;
//                    trace_decay_qph(Chi,v,v2,v2,v,v2,v2, v,v,v,v,v,v,v,v,v,v,v,v,v,v,v,v,
//                                    Ak3,Bk3,a_k3,a_Qp,a_Q,a_P,a_ll,a_p,a_rp,a_rm, meson_f1, meson_f2, meson_f3, meson_f4);

                    //#########################################################################################



                    //New: If there is more then one scalar form factor amplitude then here number_amps takes care of that.
                    VecCdoub sum_over_all_elemts(number_amps,0.0); //int with 0
                    SuperIndex<3> Si({number_amps,12,12});

                    //important: DEBUGGIN HERE
//                    cout<< ps.ll.getGrid()[i_ll]<<tab<<ps.yl.getGrid()[i_yl]<< tab<<ps.zl.getGrid()[i_zl]<<tab <<tab<<
//                        ps.Q_Qp_P[i_p][0]<<tab<<ps.Q_Qp_P[i_p][1]<<tab<<ps.Q_Qp_P[i_p][2]<<tab<<tab<<kern[Si.is({0,0,0})]<<endl;


                    //Watch out for the form code, starts form 0 or 1? check.. - should be alright. Kern start at 0.
                    //Now we have to add up the contributions and combine them with the right vertex dressing function
                    for (int i = 0; i < kern.size(); ++i) {
                        array<int,3> ex_i =  Si.are(i);
                        //important: This is the right ordering and the right call!! Never change it. V1[1] V2[2]
                        sum_over_all_elemts[ex_i[0]] += kern[i] * V1[ex_i[1]] * V2[ex_i[2]];
                    }


/*//                    cout for debuggin purposes:
//                    for (int j = 0; j < 12 ; ++j) {
//                        for (int i = 0; i < 12 ; ++i) {
//                            if(i!=3 && j!=3){
//                                int ki, kj;
//                                if(i>2){ki=i-1;}else{ki=i;}
//                                if(j>2){kj=j-1;}else{kj=j;}
//                                cout<<i<<" : "<<j<<tab<<Chi(kj,ki)<<tab<<kern[Si.is({j,i})]<<endl;
//                                cout<<i<<" : "<<j<<tab<<"Diff:  "<< (Chi(kj,ki) - kern[Si.is({j,i})] )<<endl;
//                            }
//                        }
//                    }
//                    cout<<endl;
//
//                    summing up the martix when using the old trace.
//                    Cdoub ChiA=0.0;
//                    for(int mat_i=0; mat_i<11; mat_i++)
//                    {
//                        for(int mat_j=0; mat_j<11; mat_j++)
//                        {
//                            ChiA += Chi(mat_i, mat_j);
//
//                        }
//                    }*/


                    for (int amp = 0; amp < number_amps; ++amp) {

                        //frist sum - for y-integration: inculding the weights and denorminators of the quarks:
                        y_integration_sum[amp] += sum_over_all_elemts[amp]* ps.yl.getWeights_integration()[i_yl] * quark_denom;

                    }

                }//end y-integration loop

                for (int amp = 0; amp < number_amps; ++amp) {
                    // second sum - for l-integration: including the weights and jacobian of radial integral:
                    ll_integration_sum[amp] += y_integration_sum[amp] * 0.5* ps.ll.getWeights_integration()[i_ll]* ps.ll.getGrid()[i_ll]; //Jacobian (1/2* l^2) and weights.
                }

            }//end of l-integration loop

            for (int amp = 0; amp < number_amps; ++amp) {
                //thirds and last sum - for zl-integration: including weights and jacobian of z:
                z_integration_sum[amp] += ll_integration_sum[amp] * ps.zl.getWeights_integration()[i_zl] *    //Weight
                                          sqrt(1.0-ps.zl.getGrid()[i_zl]*ps.zl.getGrid()[i_zl]); // Jacobian sqrt(1-z^2)// )
            }


        }//end of z-integration


        //------------TFF DENORMINATORS:
        //There is a difference of the denorminator for the different mesons:
        //(1) scalar: in this case most of the projector is more complicated, as different for each amplitude. Below I calcaute it.
        //(2) pseudo: need to divide by the projector denorminator 1/(Q^2Q'^2 .... as given above. Nothing done below.
        //(3) axial: Still needs to be figured out. Above is old and not right.

        //important: Think about the pre-factors once more, why is there a minus in case of the ps? 1.0/2.0 ?
        VecCdoub denom(number_amps);
        if(which_TFF=="ps" ){
            //only one form factor amplitude with the following denorminator:
            denom[0] = ( - 2.0/(2.0 *( (Q*Q)*(Qp*Qp)-pow(Q*Qp,2)) ));
            // inculding a symmetry factor of 2.0 and then Projector norm
        }else if(which_TFF=="scalar" ){

            //Here I implemented an extra From code to provide me with the expression for the denorminator
            // as in case of the scalar they are more complicated. Here number_amps=5.
            denom_func(denom,Q,Qp);

            for (int denom_i = 0; denom_i < number_amps; ++denom_i) {
                denom[denom_i] = 2.0 *denom[denom_i];
            } //same symmetry factor as above in the pseudo-scalar case.


            //---------------------------------------------------------------
            //DEBUGGING HERE 08.02.19 important
            all_denom[i_p][0] = denom[0]; all_denom[i_p][1] = denom[1]; all_denom[i_p][2] = denom[2]; all_denom[i_p][3] = denom[3]; all_denom[i_p][4] = denom[4];

            //Debug
            //DEBUGGING HERE 08.02.19 important
            for (int denom_i = 0; denom_i < number_amps; ++denom_i) {
                denom[denom_i] = 1.0;
            }


        }else if(which_TFF=="axial"){
            //important: Not jet ready!!! Need to implement it.
        }



        //Sum to safe all the amplitude multiplied with the denorminator & Prefactor (Vorfactor) of the TFF projectors
        // in the according result vector if number_amps>1:
        //(denom is already 1.0/denorminator, so it needs to be multiplied as done below. )
        for (int amp = 0; amp < number_amps; ++amp) {
            Result[Si_tff.is({i_p,amp})] =  z_integration_sum[amp] * Vorfac * denom[amp];
        }

    }//p- loobsep to save the values in corresponding result vector.



    cout<<endl<<"|...........................................|"<<endl;
    cout<<"The form factor was calcauted!  ------------>    F[0]="<< Result[0]<<tab << Result[Si_tff.is({0,0})]*all_denom[0][0]<<endl;

    //important: DEBUG
//for debuggin write out the denorminator.
    ofstream write;
    ofstream write2;
//    write.open(folder+ "denorm.txt");
    write.open(folder+ "All_denorm.txt", fstream::app);
    write2.open(folder+ "All_formfactor_all.txt", fstream::app);
    for (int i_p = 0; i_p < n_p ; ++i_p) {

        write<< scientific <<  real(ps.Q_Qp_P[i_p][0])<< tab << imag(ps.Q_Qp_P[i_p][0]) << tab << real(ps.Q_Qp_P[i_p][1]) << tab << imag(ps.Q_Qp_P[i_p][1]) << tab;
        write2<< scientific <<  real(ps.Q_Qp_P[i_p][0])<< tab << imag(ps.Q_Qp_P[i_p][0]) << tab << real(ps.Q_Qp_P[i_p][1]) << tab << imag(ps.Q_Qp_P[i_p][1]) << tab;
        for (int i = 0; i < number_amps; ++i) {
            write<< scientific<<  real(all_denom[i_p][i])<< tab << imag(all_denom[i_p][i])<< tab;
            write2<< scientific<<  real(Result[Si_tff.is({i_p,i})]*all_denom[i_p][i])<< tab << imag(Result[Si_tff.is({i_p,i})]*all_denom[i_p][i])<< tab;
        }
        write<<endl;
        write2<<endl;
    }
    write<<endl<<endl;
    write2<<endl<<endl;

    return Result;


//old stuff:
    /*  int m_dec=bse.m; int nz=bse.nz; int ny=g.ny; int numAm=bse.Ampnumber;
      double sig=sigma;
      complex<double> Sum=0;
      complex<double> Sum2=0;
      complex<double> Sum3=0;
      complex<double> Ak1, Bk1, Ak2, Bk2, Ak3, Bk3, L, ChiA;
      complex<double> fl10, fl11, fl12, fl13, fl14, fl15 ,fl16, fl17,fl20, fl21, fl22, fl23, fl24, fl25 ,fl26, fl27;
      four_vec k1,k2,k3,k4,k5,P,p,q,l1,l2;
      MatrixXcd Chi(11,11);

      vecvecdx QPV1;
      vecvecdx QPV2;

      quark_photon_vertex_interpol_ReadInn(QPV1,m_dec*nz*ny,1,rundef);
      quark_photon_vertex_interpol_ReadInn(QPV2,m_dec*nz*ny,2,rundef);






  // string stringh;
  // if(tensortag==0){stringh="data/test_data_qphv.txt";}else{stringh="trash.txt";}
  // ofstream outa(stringh.c_str());

      //äußeres Winkelgrid zq
      for(int i_p=0; i_p<g.np; i_p++)
      {

          p=g.p_vector[i_p];
          k4=0.5*(p-P);
          k5=0.5*(p+P);


          Sum3=0;
          //inneres Winkelgrid z
          for(int i=0; i<nz; i++)
          {
              complex<double> z=bse.zqp[i]; complex<double> z_weigth=bse.w_z[i];
              Sum2=0;
              //inneres radial Grid l1
              for(int i_ll=0; i_ll<m_dec; i_ll++)
              {
                  complex<double> q_q=bse.p2[i_ll];
                  complex<double> rad_weigths=bse.radw[i_ll];

                  complex<double> E=bse.Amp[0][i_ll+i*bse.m];
                  complex<double> F,G,H=0;
                  if(numAm>1){ F=bse.Amp[1][i_ll+i*bse.m];}
                  if(numAm>2){ G=bse.Amp[2][i_ll+i*bse.m];}
                  if(numAm>3){ H=bse.Amp[3][i_ll+i*bse.m];}


                  Sum=0.0;
                  //2 Winkelgrid bse.zy
                  for(int y_sum=0; y_sum<ny; y_sum++)
                  {
                      complex<double> y=g.y[y_sum]; complex<double> y_weigth=g.y_weight[y_sum];
                      q.inhalt[0]=0; q.inhalt[1]=sqrt(q_q)*sqrt(1.0-z*z)*sqrt(1.0-y*y);
                      q.inhalt[2]=sqrt(q_q)*y*sqrt(1.0-z*z); q.inhalt[3]=sqrt(q_q)*z;

                      four_vec Q1, Q2;
                      complex<double> x1, y1, z1, x2, y2, z2;

                      k1=q+sig*P;
                      k2=q-(1.0-sig)*P;
                      k3=k1+k4;

                      l1=0.5*(k3+k1);
                      l2=0.5*(k2+k3);

                      Q1=k4;
                      Q2=k5;



                      Ak1=A1(i_ll+m_dec*i);
                      Bk1=B1(i_ll+m_dec*i);
                      Ak2=A2(i_ll+m_dec*i);
                      Bk2=B2(i_ll+m_dec*i);

                      Ak3=A3(i_p,y_sum+ny*i_ll+m_dec*ny*i);
                      Bk3=B3(i_p,y_sum+ny*i_ll+m_dec*ny*i);


                      x1= 0.5*(Ak3+Ak1);
                      x2= 0.5*(Ak3+Ak2);
                      y1= (Ak3-Ak1)/(k3*k3-k1*k1);
                      y2= (Ak2-Ak3)/(k2*k2-k3*k3);
                      z1= (Bk3-Bk1)/(k3*k3-k1*k1);
                      z2= (Bk2-Bk3)/(k2*k2-k3*k3);

                      fl10=QPV1[0][y_sum+ny*i+ny*nz*i_ll];
                      fl11=QPV1[1][y_sum+ny*i+ny*nz*i_ll];
                      fl12=QPV1[2][y_sum+ny*i+ny*nz*i_ll];
                      fl13=QPV1[3][y_sum+ny*i+ny*nz*i_ll];
                      fl14=QPV1[4][y_sum+ny*i+ny*nz*i_ll];
                      fl15=QPV1[5][y_sum+ny*i+ny*nz*i_ll];
                      fl16=QPV1[6][y_sum+ny*i+ny*nz*i_ll];
                      fl17=QPV1[7][y_sum+ny*i+ny*nz*i_ll];

                      fl20=QPV2[0][y_sum+ny*i+ny*nz*i_ll];
                      fl21=QPV2[1][y_sum+ny*i+ny*nz*i_ll];
                      fl22=QPV2[2][y_sum+ny*i+ny*nz*i_ll];
                      fl23=QPV2[3][y_sum+ny*i+ny*nz*i_ll];
                      fl24=QPV2[4][y_sum+ny*i+ny*nz*i_ll];
                      fl25=QPV2[5][y_sum+ny*i+ny*nz*i_ll];
                      fl26=QPV2[6][y_sum+ny*i+ny*nz*i_ll];
                      fl27=QPV2[7][y_sum+ny*i+ny*nz*i_ll];

                      *//*{                  if(tensortag==4){fl10=fl11=fl12=fl14=fl15=fl16=fl17=0.0;fl20=fl21=fl22=fl24=fl25=fl26=fl27=0.0;}
                                      if(tensortag==5){fl10=fl11=fl12=fl13=fl15=fl16=fl17=0.0;fl20=fl21=fl22=fl23=fl25=fl26=fl27=0.0;}
                                      if(tensortag==8){fl10=fl11=fl12=fl14=fl15=fl16=fl13=0.0;fl20=fl21=fl22=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==1){fl17=fl11=fl12=fl14=fl15=fl16=fl13=0.0;fl27=fl21=fl22=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==6){fl10=fl11=fl12=fl14=fl17=fl16=fl13=0.0;fl20=fl21=fl22=fl24=fl27=fl26=fl23=0.0;}
                                      if(tensortag==7){fl10=fl11=fl12=fl14=fl15=fl17=fl13=0.0;fl20=fl21=fl22=fl24=fl25=fl27=fl23=0.0;}
                                      if(tensortag==3){fl10=fl11=fl17=fl14=fl15=fl16=fl13=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==2){fl10=fl12=fl17=fl14=fl15=fl16=fl13=0.0;fl20=fl22=fl27=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==12){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;}
                                      if(tensortag==13){fl20=fl21=fl27=fl24=fl25=fl26=fl23=fl22=0.0;}
                                      if(tensortag==14){fl20=fl21=fl27=fl24=fl25=fl26=fl23=fl22=0.0;fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;}
                                      if(tensortag==20){fl20=fl21=fl27=fl24=fl25=fl26=fl23=fl22=0.0;fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0; x1=x2=1.0; z2=z1=y1=y2=0;}
                                      if(tensortag==19){fl20=fl21=fl24=fl25=fl26=fl23=fl22=0.0; x1=x2=ii; y1=z1=y2=z2=0;fl10=fl11=fl13=fl14=fl15=fl16=fl12=0.0;}
                                      if(tensortag==21){fl20=fl21=fl27=fl24=fl25=fl26=fl23=fl22=0.0; x1=1.0; y1=z1=0;fl10=fl11=fl14=fl13=fl15=fl16=fl12=0.0;}
                                      if(tensortag==22){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0; x2=1.0; y2=z2=0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==23){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==24){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl22=0.0;}
                                      if(tensortag==25){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;fl20=fl21=fl22=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==26){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;}
                                      if(tensortag==27){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=x2=1.0; y1=z1=y2=z2=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==28){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=x2=1.0; y1=z1=y2=z2=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl22=0.0;}
                                      if(tensortag==29){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=x2=1.0; y1=z1=y2=z2=0.0;fl20=fl21=fl22=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==30){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=x2=y2=z2=0.0;}
                                      if(tensortag==32){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=0.0;}
                                      if(tensortag==31){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=fl22=0.0;}
                                      if(tensortag==33){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=y2=z2=x2=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==34){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=y2=z2=x2=0.0;fl20=fl21=fl27=fl24=fl25=fl26=fl22=0.0;}
                                      if(tensortag==35){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=y2=z2=x2=0.0;fl20=fl21=fl22=fl24=fl25=fl26=fl23=0.0;}
                                      if(tensortag==36){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0; y1=z1=y2=z2=x2=0.0;}
                                      if(tensortag==37){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0;x1=1.0;fl20=fl21=fl24=fl25=fl26=fl23=0.0; fl22=fl27=1.0; y1=z1=y2=z2=x2=0.0;}
                                      if(tensortag==38){
                                          fl12=0.33/((1.0+l1*l1/pow(0.7,2))*(1.0+l1*l1/pow(0.7,2)))*ii;
                                          fl22=0.33/((1.0+l2*l2/pow(0.7,2))*(1.0+l2*l2/pow(0.7,2)))*ii;
                                          fl13=5.4/((1.0+l1*l1/pow(0.7,2))*(1.0+l1*l1/pow(0.7,2)))*ii;
                                          fl23=5.4/((1.0+l2*l2/pow(0.7,2))*(1.0+l2*l2/pow(0.7,2)))*ii;
                                          fl17=1.53/((1.0+l1*l1/pow(0.7,2))*(1.0+l1*l1/pow(0.7,2)))*ii;
                                          fl27=1.53/((1.0+l2*l2/pow(0.7,2))*(1.0+l2*l2/pow(0.7,2)))*ii;
                                          fl10=fl11=fl14=fl15=fl16=0.0;fl20=fl21=fl24=fl25=fl26=0.0;  y1=z1=y2=z2=0.0; x1=x2=ii;
                                      }
                                      if(tensortag==39){fl10=fl11=fl17=fl14=fl15=fl16=fl13=fl12=0.0; fl20=fl21=fl24=fl25=fl26=fl23=fl22=fl27=0.0; y1=z1=y2=z2=ii; x2=x1=ii;
                                      }
                                      if(tensortag==40){fl20=fl21=fl24=fl25=fl26=fl23=fl22=0.0;fl10=fl11=fl14=fl15=fl16=fl13=fl12=0.0; y1=y2=z1=z2=0.0; x1=x2=ii;}
                                      if(tensortag==41){
                                          complex<double> x11= (l1*l1/3.0+Q1*Q1/4.0)/pow(0.65,2);
                                          complex<double> x22= (l2*l2/3.0+Q2*Q2/4.0)/pow(0.65,2);
                                          fl12=0.33/((1.0+x11)*(1.0+x11))*ii;
                                          fl22=0.33/((1.0+x22)*(1.0+x22))*ii;
                                          fl13=fl23=fl27=fl17=0.0;
                                          fl10=fl11=fl14=fl15=fl16=0.0;fl20=fl21=fl24=fl25=fl26=0.0;  y1=z1=y2=z2=0.0; x1=x2=ii;
                                      }
                                      if(tensortag==42){
                                          complex<double> x11= (l1*l1/3.0+Q1*Q1/4.0)/pow(0.65,2);
                                          complex<double> x22= (l2*l2/3.0+Q2*Q2/4.0)/pow(0.65,2);
                                          fl13=5.4/((1.0+x11)*(1.0+x11))*ii;
                                          fl23=5.4/((1.0+x22)*(1.0+x22))*ii;
                                          fl12=fl22=fl27=fl17=0.0;
                                          fl10=fl11=fl14=fl15=fl16=0.0;fl20=fl21=fl24=fl25=fl26=0.0;  y1=z1=y2=z2=0.0; x1=x2=ii;
                                      }
                                      if(tensortag==43){
                                          complex<double> x11= (l1*l1/3.0+Q1*Q1/4.0)/pow(0.65,2);
                                          complex<double> x22= (l2*l2/3.0+Q2*Q2/4.0)/pow(0.65,2);
                                          fl17=1.53/((1.0+x11)*(1.0+x11))*ii;
                                          fl27=1.53/((1.0+x22)*(1.0+x22))*ii;
                                          fl13=fl23=fl22=fl12=0.0;
                                          fl10=fl11=fl14=fl15=fl16=0.0;fl20=fl21=fl24=fl25=fl26=0.0;  y1=z1=y2=z2=0.0; x1=x2=ii;
                                      }
                                      if(tensortag==44){
                                          complex<double> x11= (l1*l1/3.0+Q1*Q1/4.0)/pow(0.65,2);
                                          complex<double> x22= (l2*l2/3.0+Q2*Q2/4.0)/pow(0.65,2);
                                          fl12=0.33/((1.0+x11)*(1.0+x11))*ii;
                                          fl22=0.33/((1.0+x22)*(1.0+x22))*ii;
                                          fl13=5.4/((1.0+x11)*(1.0+x11))*ii;
                                          fl23=5.4/((1.0+x22)*(1.0+x22))*ii;
                                          fl17=1.53/((1.0+x11)*(1.0+x11))*ii;
                                          fl27=1.53/((1.0+x22)*(1.0+x22))*ii;
                                          fl10=fl11=fl14=fl15=fl16=0.0;fl20=fl21=fl24=fl25=fl26=0.0;  y1=z1=y2=z2=0.0; x1=x2=ii;
                                      }
                  }*//*

                    L=-(1.0/(k1*k1*Ak1*Ak1+Bk1*Bk1))*(1.0/(k2*k2*Ak2*Ak2+Bk2*Bk2))
                      *(1.0/((k3*k3)*Ak3*Ak3+Bk3*Bk3))*(1.0/(2.0*((k4*k4)*(k5*k5)-pow(k4*k5,2))));

//                    if((i_p==0 && y_sum==0 && i==0 && i_ll==0)||(i_p==1 && y_sum==0 && i==0 && i_ll==10)){k3.write();k4.write();k5.write(); l2.write();
//                        cout<<"Other stuff: "<<L<<tab<<k1*k1<<tab<<Ak1<<tab<<Bk1<<tab<<k2*k2<<tab<<(1.0/(k1*k1*Ak1*Ak1+Bk1*Bk1))<<tab<<(1.0/(k2*k2*Ak2*Ak2+Bk2*Bk2))<<endl;}



                    //#########################################################################################

                    trace_decay_qph(Chi,x1,y1,z1,x2,y2,z2,fl10,fl11,fl12,fl13,fl14,fl15,fl16,fl17,fl20,fl21,fl22,fl23,fl24,fl25,fl26,fl27,
                                    Ak3,Bk3,k3,k4,k5,P,q,p,l1,l2, E, F, G, H);


                    //#########################################################################################

                    ChiA=0.0;
                    for(int mat_i=0; mat_i<11; mat_i++)
                    {
                        for(int mat_i_ll=0; mat_i_ll<11; mat_i_ll++)
                        {
                            ChiA += Chi(mat_i, mat_i_ll);

                        }
                    }


                    Sum += ChiA*L*y_weigth;
                }



                Sum2 +=rad_weigths*(0.5)*q_q*q_q*Sum;

            }

            Sum3 += sqrt(1.0-z*z)*z_weigth*Sum2;
        }

        Re(i_p)=Vorfac*Sum3;

    }

//outa.close();



    //Writing out Results
    stringstream ss; ss<<round; string sss=ss.str();  stringstream ssa; ssa<<tensortag; string sssa=ssa.str();string buffer;

    buffer = rundef+"decay_"+name+".txt";
    if(tensortag!=0){buffer = rundef+"decay_qph_para_"+name+"_t"+sssa+".txt";}
    ofstream outb(buffer.c_str());
    outb<<"#   m="<<m_dec<<" nz:"<<nz<<" ny_BSE:"<<bse.ny<<" ny_Decay:"<<ny<<"  pionM:"<<M<<endl;
    outb<<"#--------------------------------------------------# "<<endl;
    outb<<"#   Re(p2) imag(p2)  Re(Lambda)  imag(Lambda)    real(k4) real(k5)  p"<<endl;
    for(int i_p=0; i_p<g.np; i_p++)
    {
        p=g.p_vector[i_p];
        k4=0.5*(p-P);
        k5=0.5*(p+P);
        k1=q+sig*P;
        k2=q-(1.0-sig)*P;
        k3=k1+k4;

        outb<<real(p*p)<<"\t"<<imag(p*p)<<"\t"<<(4.0*Pi*Pi*fpi)*real(Re(i_p))<<"\t"<<(4.0*Pi*Pi*fpi)*imag(Re(i_p))<<"\t"
            <<real(k4*k4)<<"\t"<<imag(k4*k4)<<"\t"<<real(k5*k5)<<"\t"<<imag(k5*k5)<<"\t"<<k3*k3<<endl;
    }
    outb.close();

*/

}

VecCdoub formfactor::debug_calc_formfactor(vector<array<Cdoub,2>>& quark1,vector<array<Cdoub,2>>& quark2,
                                           vector<array<Cdoub,2>>& quark3,ps_amplitude<3>& bse) {


    //Variables of this function:
    array<Cdoub, 12> V1;
    array<Cdoub, 12> V2;


    int n_p=ps.Q_Qp_P.size();

    //important: Debuggin
    vector<vector<Cdoub>> all_denom(n_p,VecCdoub(number_amps,0.0));

    //Superindices:
    SuperIndex<4> Si_quark3({ps.ll.getNumber_of_grid_points(), ps.zl.getNumber_of_grid_points(),
                             ps.yl.getNumber_of_grid_points(), n_p});

    SuperIndex<2> Si_quark12({ps.ll.getNumber_of_grid_points(), ps.zl.getNumber_of_grid_points()});

    SuperIndex<4> Si_vertex({n_p, ps.yl.getNumber_of_grid_points(),  ps.zl.getNumber_of_grid_points(),
                             ps.ll.getNumber_of_grid_points()  });

#ifdef K_WITH_FULL_QPHV
    //obtaining the vertex values from Gernots interpolation -later mine
    matCdoub QPV1;
    matCdoub QPV2;

    int all_N= ps.ll.getNumber_of_grid_points()* ps.zl.getNumber_of_grid_points()*ps.yl.getNumber_of_grid_points()*n_p;

    int test=qphv_interpol_ReadInn(QPV1,all_N,1,folder);
    if(test==0){cout<<"Calction Aborted QPV not found"<<endl;assert(false);}
    test=qphv_interpol_ReadInn(QPV2,all_N,2,folder);
    if(test==0){cout<<"Calction Aborted QPV not found"<<endl; assert(false);}

#endif


    //Prefactor of the form factor integral, comes form the integration factor     ->  the symmetry factors of 2 are included is now below because of the differnt mesons.
    double Vorfac=1.0/(pow((2*M_PI),3.0));

    SuperIndex<2> Si_tff({n_p,number_amps});
    VecCdoub Result; Result.resize(Si_tff.getNsuper_index());


////-------------------- start of loops ------------------------------------
    //Sum over all values of the relative momentum p. To safe all values

//#pragma omp parallel for default(shared)
//#pragma omp parallel for schedule (dynamic, 1)  default(shared)
    for (int i_p = 0; i_p < n_p ; ++i_p) {


        //important: DEBUG---

        fv<Cdoub> p = get_p_vec(alpha, ps.Q_Qp_P[i_p][0],ps.Q_Qp_P[i_p][1], ps.Q_Qp_P[i_p][2]);
        fv<Cdoub> P = get_P_vec(alpha, ps.Q_Qp_P[i_p][0],ps.Q_Qp_P[i_p][1], ps.Q_Qp_P[i_p][2]);

//        cout << "p=( " << p.content[0] << " , " << p.content[1] << " , " << p.content[2] << " , " << p.content[3] << endl;

        //important:
        //////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //----------- Watch out: Setting the realtion for Q and Q' here -----------
        //(maybe get into a function somewhere external at some points?)
        fv<Cdoub> Q, Qp;
        Q =  get_Q(P,p);
        Qp = get_Qp(P,p);

        //////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


        VecCdoub z_integration_sum(number_amps,0.0); //init with 0
        //INTEGRATION- loop start: zl
        //angular integration form the loop integration in l. (z_l)
        for (int i_zl = 0; i_zl < ps.zl.getNumber_of_grid_points(); ++i_zl) {


            VecCdoub ll_integration_sum(number_amps,0.0); //init with 0
            //INTEGRATION- loop start: ll
            //radial integration for the loop integration in l. (ll)
            for (int i_ll = 0; i_ll < ps.ll.getNumber_of_grid_points() ; ++i_ll) {


#ifndef K_USING_ALPHA_FRAME
#ifndef K_USING_QUARK_MODEL
                //important: Changed call for quark here cause now they depend on more. See above. In alpha!=big case.
                Cdoub Ak1= quark1[Si_quark12.is({i_ll, i_zl})][0];
                Cdoub Bk1= quark1[Si_quark12.is({i_ll, i_zl})][1];
                Cdoub Ak2= quark2[Si_quark12.is({i_ll, i_zl})][0];
                Cdoub Bk2= quark2[Si_quark12.is({i_ll, i_zl})][1];
#endif
#endif


                //Call + safe the amplitudes from the meson
                //The loop runs till the size of Tensor Elements that the meson contains. (scalar 4, vector 8) //important: hard-coded meson number_amps
                VecCdoub meson(4.0,0.0);

#ifndef K_USING_MESON_MODEL
                for (int amp = 0; amp < bse.getSi().getMaxima_of_partial_indices()[0]; ++amp) {
                    meson[amp] = (bse( bse.getSi().is({amp,i_ll, i_zl}) ));
                }
#endif

//DEBUGGING
//    if(real(meson[0])==0 || real(meson[1])==0 || real(meson[2])==0 || real(meson[3])==0 ||
//            real(Ak1)==0 || real(Bk1)==0 || real(P*P)==0 || real(Q*Q)==0 ){
//            cout<<i_p<<tab<<i_ll<<tab<<i_zl<<" :   "<<meson[0]<<tab<<meson[1]<<tab<<meson[2]<<tab<<meson[3]<<tab
//                <<Ak1<<tab<<Bk1<<tab<<Ak2<<tab<<Bk2<<tab<<Q*Q<<tab<<Qp*Qp<<tab<<p*p<<tab<<P*P<<endl; cin.get(); }



                VecCdoub y_integration_sum(number_amps,0.0); //init with 0

                //INTEGRATION- loop start: yl
                //angular integration for the loop integration in l. (y_l)
                for (int i_yl = 0; i_yl < ps.yl.getNumber_of_grid_points(); ++i_yl) {


                    //Setting up the four vectors
                    fv<Cdoub> ll, rp, rm, k3, k1, k2;
                    ll.set_spherical(ps.ll.getGrid()[i_ll],0.0,ps.yl.getGrid()[i_yl],ps.zl.getGrid()[i_zl]);
                    rp =  get_rp(P,p,ll);
                    rm =  get_rm(P,p,ll);
                    k1=get_kp(ll, P);
                    k2=get_km(ll, P);
                    k3=get_k3(ll,p);
//                    rp = ll + 0.25*p + 0.25*P;
//                    rm = ll + 0.25*p - 0.25*P;
//                    k3 = ll + 0.5*p;
//                    k1 = ll + 0.5*P;
//                    k2 = ll - 0.5*P;


#ifdef K_USING_ALPHA_FRAME
#ifndef K_USING_QUARK_MODEL
                    //important the other change is here: when using alpha use the following -
                    Cdoub Ak1= quark1[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][0];
                    Cdoub Bk1= quark1[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][1];
                    Cdoub Ak2= quark2[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][0];
                    Cdoub Bk2= quark2[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][1];
#endif
#endif

#ifndef K_USING_QUARK_MODEL
                    Cdoub Ak3 = quark3[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][0];
                    Cdoub Bk3 = quark3[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][1];
#endif
#ifdef K_USING_QUARK_MODEL

//                    Cdoub Ak1= 1.0+ 0.6/(1.0 + (k1*k1));
//                    Cdoub Bk1= 0.8/(1.0 + (k1*k1)) ;
//                    Cdoub Ak2= 1.0+ 0.6/(1.0 + (k2*k2));
//                    Cdoub Bk2= 0.8/(1.0 + (k2*k2));
//                    Cdoub Ak3 =  1.0+ 0.6/(1.0 + (k3*k3));
//                    Cdoub Bk3 = 0.8/(1.0 + (k3*k3));

//                    Cdoub Ak1= 1.0;
//                    Cdoub Bk1= 0.0035748;
//                    Cdoub Ak2= 1.0;
//                    Cdoub Bk2= 0.0035748;
//                    Cdoub Ak3 =  1.0;
//                    Cdoub Bk3 = 0.0035748;

//                    Cdoub Ak1= 1.0;
//                    Cdoub Bk1= 0.55;
//                    Cdoub Ak2= 1.0;
//                    Cdoub Bk2= 0.55;
//                    Cdoub Ak3 = 1.0;
//                    Cdoub Bk3 = 0.55;

                    Cdoub Ak1= 1.0;
                    Cdoub Bk1= 0.35;
                    Cdoub Ak2= 1.0;
                    Cdoub Bk2= 0.35;
                    Cdoub Ak3 = 1.0;
                    Cdoub Bk3 = 0.35;

//                    Cdoub Ak1= 1.0;
//                    Cdoub Bk1= 0.5534;
//                    Cdoub Ak2= 1.0;
//                    Cdoub Bk2= 0.5534;
//                    Cdoub Ak3 = 1.0;
//                    Cdoub Bk3 = 0.5534;



//                    Cdoub Ak1= 1.0;
//                    Cdoub Bk1= 1.0;
//                    Cdoub Ak2= 1.0;
//                    Cdoub Bk2= 1.0;
//                    Cdoub Ak3 = 1.0;
//                    Cdoub Bk3 = 1.0;

#endif

                    //DEBUGGING
//                    if(real(Ak3)==0 || real(Bk3)==0){cout<<i_p<<tab<<i_ll<<tab<<i_zl<<tab<<i_yl<<" :   "<<Ak3<<tab<<Bk3<<endl;cin.get();}


                    //QUARK DENORMINATORS: - (TFFs ones below, as no dependence on inside loops.)
                    //Calculate the Denorminators of the quark propagators included!!!
                    //Frist is the denorminato of the 3rd Quark propagator,
                    // second the denorminator with form TFF projector, including the photon momenta.
                    //(The denorminator of S1 and S2 is already included in Chi.) - not anymore - changed that due to comparison.
                    //frist we start with the terms form the quark propagators
                    Cdoub quark_denom;
                    quark_denom = (1.0/((k3*k3)*Ak3*Ak3+Bk3*Bk3)) *
                                 (1.0/((k1*k1)*Ak1*Ak1+Bk1*Bk1))*(1.0/((k2*k2)*Ak2*Ak2+Bk2*Bk2));

//                    quark_denom = 1.0;

#ifndef K_WITH_BARE_VERTEX
                    //----------------------------------------------------------------------------
                    //Get the vertex dressing functions
                    Cdoub x1= 0.5*(Ak3+Ak1);
                    Cdoub x2= 0.5*(Ak3+Ak2);
                    Cdoub y1= (Ak3-Ak1)/(k3*k3-k1*k1);
                    Cdoub y2= (Ak2-Ak3)/(k2*k2-k3*k3);
                    Cdoub z1= (Bk3-Bk1)/(k3*k3-k1*k1);
                    Cdoub z2= (Bk2-Bk3)/(k2*k2-k3*k3);

                    //these are only the transverse components.
                    // The longitudinal are taken form the quark propagators aka Ball Chui part with WTI.
                    //-change to the array V1 and V2
                    V1[0] =  x1;
                    V1[1] =  2.0 *y1;
                    V1[2] =  2.0 *z1;
                    V1[3] = 0.0;

                    //These are only relevant for the new trace expression as the old one uses x,y,z directly
                    V2[0] = x2;
                    V2[1] =  2.0 *y2;
                    V2[2] =  2.0 *z2;
                    V2[3] = 0.0;

#endif

#ifdef K_WITH_FULL_QPHV
                    V1[4]=QPV1[0][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
                    V1[5]=QPV1[1][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
                    V1[6]=QPV1[2][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
                    V1[7]=QPV1[3][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
                    V1[8]=QPV1[4][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
                    V1[9]=QPV1[5][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
                    V1[10]=QPV1[6][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
                    V1[11]=QPV1[7][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];

                    V2[4]=QPV2[0][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
                    V2[5]=QPV2[1][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
                    V2[6]=QPV2[2][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
                    V2[7]=QPV2[3][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
                    V2[8]=QPV2[4][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
                    V2[9]=QPV2[5][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
                    V2[10]=QPV2[6][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
                    V2[11]=QPV2[7][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
#endif

                    //----------------------------------------------------------------------------

#ifndef K_WITH_FULL_QPHV
                    V1[4]=V1[5]=V1[6]=V1[7]=V1[8]=V1[9]=V1[10]=V1[11]=0.0;
                    V2[4]=V2[5]=V2[6]=V2[7]=V2[8]=V2[9]=V2[10]=V2[11]=0.0;
#ifdef K_WITH_BARE_VERTEX

                    V1[1]=V1[2]=V1[3]=0; V1[0] = 1.0; //V1[0] = 0.972241; //28 //V1[0] =0.9843; // 30 for Z2 value
                    V2[1]=V2[2]=V2[3]=0; V2[0] = 1.0; //V2[0] = 0.972241; //28//V2[0] =0.9843; //30

#endif
#endif


#ifdef K_USING_MESON_MODEL

//                    meson[0]= 1.0/(ps.ll.getGrid()[i_ll] + 0.55*0.55);
                    meson[0]= 1.0/(ps.ll.getGrid()[i_ll] + 0.35*0.35);
                    meson[1]=meson[2]=meson[3]=0.0;

                    //model used when comparing to Richard.
//                    meson[0]= 6.0/(1.0+ps.ll.getGrid()[i_ll]);
//                    meson[1]= 6.0/(1.0+ps.ll.getGrid()[i_ll]*ps.ll.getGrid()[i_ll]);
//                    meson[2]= 3.0/(1.0+ps.ll.getGrid()[i_ll]*ps.ll.getGrid()[i_ll]);
//                    meson[3]= -2.0/(1.0+ps.ll.getGrid()[i_ll]*ps.ll.getGrid()[i_ll]);
#endif
#ifdef K_USING_ONLY_FIRST_AMP_MESON
                    meson[1]=meson[2]=meson[3]=0.0;
#endif



                    //Calling the final trace expression including all the components together.
                    //The information will be safed in a long vector order in terms of the two quark-photon vertices
                    // Such that the total vector has 12x12= 144 compontens
                    // Inner loop is V2 (rm, -Q) and the outter V1 (rp,Qp) : V(1.....12) = f1/0*f2/1....f2/12
//                    VecCdoub kern; kern.resize(12*12*number_amps);
                    VecCdoub kern(12*12*number_amps,0.0);

/*

//                    cout<<Ak1<<tab<<Bk1<<tab<<Ak2<<tab<<Bk2<<tab<<k3*k3<<tab<<k1*k1<<endl;
//                    cout<<x1<<tab<<y2<<tab<<V1[4]<<tab<<Ak3<<tab<<meson_f1 <<endl;
//                    cout<<y1<<tab<<Ak1<<tab<<Ak3<<tab<<k1*k1<<tab<<k3*k3<<tab<<endl;
//                    cout<<y2<<tab<<Ak2<<tab<<Ak3<<tab<<k2*k2<<tab<<k3*k3<<tab<<endl;
//                    cout<<z1<<tab<<Bk1<<tab<<Bk3<<tab<<k1*k1<<tab<<k3*k3<<tab<<endl;
//                    cout<<z2<<tab<<Bk2<<tab<<Bk3<<tab<<k2*k2<<tab<<k3*k3<<tab<<endl;
//                    cout<<"---> "<<V1[11]<<tab<<V2[7]<<tab<<V1[6]<<tab<<V2[10]<<endl;
//                    cout<< k3*ll <<tab<< Q*Qp <<tab<<rm* rm <<tab<<rm *rp <<tab<< rp*rp <<endl;

//                    cout<<x1<<tab<<y1<<tab<<z1<<tab<<x2<<tab<<y2<<tab<<z2<<tab<<V1[4]<<tab<<V1[5]<<tab<<V1[6]<<tab<<V1[7]<<tab<<V1[8]<<tab<<V1[9]<<tab<<V1[10]<<tab<<V1[11]<<tab<<V2[4]<<tab<<V2[5]<<tab<<V2[6]<<tab<<V2[7]<<tab<<V2[8]<<tab<<V2[9]<<tab<<V2[10]<<tab<<V2[11]<<tab<<
//                            Ak3<<tab<<Bk3<<tab<< meson_f1<<tab<< meson_f2<<tab<< meson_f3<<tab<< meson_f4<<endl;

//                    a_k3.write();
//                    a_Qp.write();
//                    a_Q.write();
//                    a_P.write();
//                    a_ll.write();
//                    a_p.write();
//                    a_rp.write();
//                    a_rm.write();

//                    cout<<V1[4]<<tab<<V1[5]<<tab<<V1[6]<<tab<<V1[7]<<tab<<V1[8]<<tab<<V1[9]<<tab<<V1[10]<<tab<<V1[11]<<tab<<V2[4]<<tab<<V2[5]<<tab<<V2[6]<<tab<<V2[7]<<
//                        tab<<V2[8]<<tab<<V2[9]<<tab<<V2[10]<<tab<<V2[11]<<endl;

//                    cout<<"-----------------------"<<endl
//                        <<k3*k3<<tab<<k1*k1<<tab<<k2*k2<<tab<<Ak1<<tab<<Bk1<<tab<<Ak2<<tab<<Bk2<<endl<<endl;
*/


                    //#########################################################################################

                //important !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                //important !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    //original function:  ---- normally used ---------
                    //in order to get the right answer the pharsing of the vertex elements has to be right p yl,zl,ll
                    //and then also accessing of the vertex data in V1[Si(0)], V2[Si(1)]. Aka kern[i]. i<122.
//                    kernel_func(kern, P, Q, Qp, p, ll, rp, rm, k3, meson, Ak3, Bk3);
                    //important Debuggin here: uncommented for comparison
                    kernel_func_amp_mf(kern, P, Q, Qp, p, ll, rp, rm, k3, meson, Ak3, Bk3, Ak1, Bk1, Ak2, Bk2);
                    //Gave the right result on 01.06.2018 for the anomaly value.


                    //------------------------OTHER VERSION----------------------

                    //for the bare version - A1, B1.. are also needed:
//                    kernel_func(kern, P, Q, Qp, p, ll, rp, rm, meson_f1, meson_f2, meson_f3, meson_f4, Ak3, Bk3,
//                    Ak1, Bk1, Ak2, Bk2);


                    //Old trace:
                    //Old trace expression form old program for comparison : - worked 29.05.18 and then not anymore 01.07 :/
                    //worked again: problem was the read inn routine form the bse. - fix this
                    //furthermore now the vertex gets accessed the right way, such that the new and the old version give the same answer.
//                    trace_decay_qph(Chi,x1,y1,z1,x2,y2,z2,V1[4],V1[5],V1[6],V1[7],V1[8],V1[9],V1[10],V1[11],V2[4],V2[5],V2[6],V2[7],V2[8],V2[9],V2[10],V2[11],
//                                    Ak3,Bk3,a_k3,a_Qp,a_Q,a_P,a_ll,a_p,a_rp,a_rm, meson_f1, meson_f2, meson_f3, meson_f4);

                    //Old trace debug:
                    //for debuggin set to 1.0:
//                    Cdoub v=1.0; Cdoub v2=0.5;
//                    trace_decay_qph(Chi,v,v2,v2,v,v2,v2, v,v,v,v,v,v,v,v,v,v,v,v,v,v,v,v,
//                                    Ak3,Bk3,a_k3,a_Qp,a_Q,a_P,a_ll,a_p,a_rp,a_rm, meson_f1, meson_f2, meson_f3, meson_f4);

                    //#########################################################################################


                    //New: If there is more then one scalar form factor amplitude then here number_amps takes care of that.
                    VecCdoub sum_over_all_elemts(number_amps,0.0); //int with 0
                    SuperIndex<3> Si({number_amps,12,12});

//                    cout<< (Q*Q) << tab << (Qp* Qp) << tab << (Q*Qp) <<tab <<
//                        Bk1 << tab << Bk2 << tab << Bk3 << tab << meson[0] << tab<< kern[Si.is({0,0,0})]<<endl;

                    //important: Debugging
                    //termn in principle with Denorminator, but after all I took the denorminator away by hand, see below.
                    //This one agreed with the Form-Trace-Kernel 21.03.19 - for the leading contributions.

//                    kern[Si.is({0,0,0})]= 4.0*((Ak1*((-((k1*k3)*((Q*Q) - (Qp*Qp))*(-pow((Q*Qp),2.0) + (Q*Q)*(Qp*Qp))) + (Qp*k1)*((Q*Qp)*(Q*k3)*(-(Q*Q) + (Qp*Qp)) +
//                                         2.0*pow((Q*Q),2.0)*(Qp*k3)) + (Q*k1)*(-2.0*(Q*k3)*pow((Qp*Qp),2.0) + (Q*Qp)*(-(Q*Q) + (Qp*Qp))*(Qp*k3)))*Ak3*Bk2
//                                       + ((Q*Q) - (Qp*Qp))*((k1*k2)*(-pow((Q*Qp),2.0) + (Q*Q)*(Qp*Qp)) + (Q*Qp)*(-((Q*k2)*(Qp*k1)) + (Q*k1)*(Qp*k2)))*Ak2*Bk3)
//                                        +Bk1*((-((k2*k3)*((Q*Q) - (Qp*Qp))*(-pow((Q*Qp),2.0) + (Q*Q)*(Qp*Qp))) + (Qp*k2)*((Q*Qp)*(Q*k3)*(-(Q*Q) + (Qp*Qp))
//                                        + 2.0*pow((Q*Q),2.0)*(Qp*k3)) + (Q*k2)*(-2.0*(Q*k3)*pow((Qp*Qp),2.0) + (Q*Qp)*(-(Q*Q) + (Qp*Qp))*(Qp*k3)))*Ak2*Ak3
//                                      - ((Q*Q) - (Qp*Qp))*(-pow((Q*Qp),2.0) + (Q*Q)*(Qp*Qp))*Bk2*Bk3))*meson[0]);

//                    cout<< scientific<< (Q*Q) << tab << (Qp* Qp) << tab << (Q*Qp) <<tab <<
//                        Bk1 << tab << Bk2 << tab << Bk3 << tab << meson[0] << tab<< kern[Si.is({0,0,0})]<<tab<<test<<tab<<test-kern[Si.is({0,0,0})]<<endl<<endl;

//                    kern[Si.is({0,0,0})]=kern[Si.is({0,0,0})]/(4.0*(((Q*Q) - (Qp*Qp))*(-pow((Q*Qp),2) + (Q*Q)*(Qp*Qp))));
                    //okay one last question in this term there is in principle a 1/4 that is still divided out,
                    //in my current Projector there is no 4.0, so is this over compensation? Is something missing. Check!

                    //important: DEBUGGING HERE
//                    cout<< ps.ll.getGrid()[i_ll]<<tab<<ps.yl.getGrid()[i_yl]<< tab<<ps.zl.getGrid()[i_zl]<<tab <<tab<<
//                    ps.Q_Qp_P[i_p][0]<<tab<<ps.Q_Qp_P[i_p][1]<<tab<<ps.Q_Qp_P[i_p][2]<<tab<<tab<<kern[Si.is({0,0,0})]<<endl;


                    //important: Watch out for the form code, starts form 0 or 1? check..
                    //Now we have to add up the contributions and combine them with the right vertex dressing function
                    for (int i = 0; i < kern.size(); ++i) {
                        array<int,3> ex_i =  Si.are(i);
                        //important: This is the right ordering and the right call!! Never change it. V1[1] V2[2]
                        sum_over_all_elemts[ex_i[0]] += kern[i] * V1[ex_i[1]] * V2[ex_i[2]];
                    }


/*//                    cout for debuggin purposes:
//                    for (int j = 0; j < 12 ; ++j) {
//                        for (int i = 0; i < 12 ; ++i) {
//                            if(i!=3 && j!=3){
//                                int ki, kj;
//                                if(i>2){ki=i-1;}else{ki=i;}
//                                if(j>2){kj=j-1;}else{kj=j;}
//                                cout<<i<<" : "<<j<<tab<<Chi(kj,ki)<<tab<<kern[Si.is({j,i})]<<endl;
//                                cout<<i<<" : "<<j<<tab<<"Diff:  "<< (Chi(kj,ki) - kern[Si.is({j,i})] )<<endl;
//                            }
//                        }
//                    }
//                    cout<<endl;
//
//                    summing up the martix when using the old trace.
//                    Cdoub ChiA=0.0;
//                    for(int mat_i=0; mat_i<11; mat_i++)
//                    {
//                        for(int mat_j=0; mat_j<11; mat_j++)
//                        {
//                            ChiA += Chi(mat_i, mat_j);
//
//                        }
//                    }*/


                    for (int amp = 0; amp < number_amps; ++amp) {

                        //first sum - for y-integration: inculding the weights and denorminators of the quarks:
                        y_integration_sum[amp] += sum_over_all_elemts[amp]* ps.yl.getWeights_integration()[i_yl] * quark_denom; // weights * quark denorminator (1/(A^2p^2+B^2))

                    }

                }//end y-integration loop

                for (int amp = 0; amp < number_amps; ++amp) {
                    // second sum - for l-integration: including the weights and jacobian of radial integral:
                    ll_integration_sum[amp] += y_integration_sum[amp] * 0.5* ps.ll.getWeights_integration()[i_ll]* ps.ll.getGrid()[i_ll]; //Jacobian (1/2* l^2) and weights.
                }

            }//end of l-integration loop

            for (int amp = 0; amp < number_amps; ++amp) {
                //thirds and last sum - for zl-integration: including weights and jacobian of z:
                z_integration_sum[amp] += ll_integration_sum[amp] * ps.zl.getWeights_integration()[i_zl] *    //Weight
                                     sqrt(1.0-ps.zl.getGrid()[i_zl]*ps.zl.getGrid()[i_zl]); // Jacobian sqrt(1-z^2)// )
            }


        }//end of z-integration


        //------------TFF DENORMINATORS:
        //There is a difference of the denorminator for the different mesons:
        //(1) scalar: in this case most of the projector is more complicated, as different for each amplitude. Below I calcaute it.
        //(2) pseudo: need to divide by the projector denorminator 1/(Q^2Q'^2 .... as given above. Nothing done below.
        //(3) axial: Still needs to be figured out. Above is old and not right.

        //important: Think about the pre-factors once more, why is there a minus in case of the ps? 1.0/2.0 ?
        VecCdoub denom(number_amps);
        if(which_TFF=="ps" ){
            //only one form factor amplitude with the following denorminator:
            denom[0] = ( - 2.0/(2.0 *( (Q*Q)*(Qp*Qp)-pow(Q*Qp,2)) ));

            //DEBUGGING HERE 08.02.19 / 08.08.19
            all_denom[i_p][0] = denom[0];
            denom[0]=1.0;
            // inculding a symmetry factor of 2.0 and then Projector norm
        }else if(which_TFF=="scalar" ){

            //Here I implemented an extra From code to provide me with the expression for the denorminator
            // as in case of the scalar they are more complicated. Here number_amps=5.
            denom_func(denom,Q,Qp);

            for (int denom_i = 0; denom_i < number_amps; ++denom_i) {
                denom[denom_i] =  2.0 * denom[denom_i];
            } //same symmetry factor as above in the pseudo-scalar case. --> Check this. Shouldn't this be *2.0 instead of the division, see above.


            //---------------------------------------------------------------
            //DEBUGGING HERE 08.02.19
            all_denom[i_p][0] = denom[0]; all_denom[i_p][1] = denom[1]; all_denom[i_p][2] = denom[2]; all_denom[i_p][3] = denom[3]; all_denom[i_p][4] = denom[4];

            //Debug
            //DEBUGGING HERE 08.02.19 important
            for (int denom_i = 0; denom_i < number_amps; ++denom_i) {
                denom[denom_i] = 1.0;
            }


        }else if(which_TFF=="axial"){
            //important: Not jet ready!!! Need to implement it.
        }



        //Sum to safe all the amplitude multiplied with the denorminator & Prefactor (Vorfactor) of the TFF projectors
        // in the according result vector if number_amps>1:
        //(denom is already 1.0/denorminator, so it needs to be multiplied as done below. )
        for (int amp = 0; amp < number_amps; ++amp) {
            Result[Si_tff.is({i_p,amp})] =  z_integration_sum[amp] * Vorfac * denom[amp];
        }

    }//p- loop to save the values in corresponding result vector.



    //important: DEBUG
//for debuggin write out the denorminator.
    ofstream write;
    ofstream write2;
    ofstream write3;
//    write.open(folder+ "denorm.txt");
    write.open(folder+ "All_denorm.txt", fstream::app);
    write2.open(folder+ "All_formfactor_all.txt", fstream::app);
    write3.open(folder+ "All_formfactor_without_denorm.txt", fstream::app);
    write2<<"# Q^2"<<tab<<"Q'^2"<<tab<<tab<<"f_i"<<endl;
    write3<<"# Q^2"<<tab<<"Q'^2"<<tab<<tab<<"f_i"<<endl;
    for (int i_p = 0; i_p < n_p ; ++i_p) {

//        int n_sigma=25;
//        Line sigma_line(1.e-4,1.e+4,n_sigma,"log", "leg", false, false);
//        double z_here=0.5;
//        Cdoub P_here= -0.685*0.685;
//        double sigma_here=sigma_line.getGrid()[i_p];
//        fv<Cdoub> p = get_p_vec(z_here,sigma_here);

        fv<Cdoub> p = get_p_vec(alpha, ps.Q_Qp_P[i_p][0],ps.Q_Qp_P[i_p][1], ps.Q_Qp_P[i_p][2]);
        fv<Cdoub> P = get_P_vec(alpha, ps.Q_Qp_P[i_p][0],ps.Q_Qp_P[i_p][1], ps.Q_Qp_P[i_p][2]);

        fv<Cdoub> Q, Qp;
        Q =  get_Q(P,p);
        Qp = get_Qp(P,p);

        write<< scientific <<  real(ps.Q_Qp_P[i_p][0])<< tab << imag(ps.Q_Qp_P[i_p][0]) << tab << real(ps.Q_Qp_P[i_p][1]) << tab << imag(ps.Q_Qp_P[i_p][1]) << tab;
        write2<< scientific <<  real(ps.Q_Qp_P[i_p][0])<< tab << imag(ps.Q_Qp_P[i_p][0]) << tab << real(ps.Q_Qp_P[i_p][1]) << tab << imag(ps.Q_Qp_P[i_p][1]) << tab;
        write3<< scientific <<  real(ps.Q_Qp_P[i_p][0])<< tab << imag(ps.Q_Qp_P[i_p][0]) << tab << real(ps.Q_Qp_P[i_p][1]) << tab << imag(ps.Q_Qp_P[i_p][1]) << tab;
        //debugging: This was used when using the set-up with sigma for the comparison with Ricahrds. Case=5
//        write<< scientific <<  0.25*real(p*p)<< tab << 0.25*imag(p*p) << tab << 0.0 << tab <<0.0 << tab;
//        write2<< scientific <<  0.25*real(p*p)<< tab << 0.25*imag(p*p) << tab << 0.0 << tab <<0.0 << tab;

        for (int i = 0; i < number_amps; ++i) {
            write<< scientific<<  real(all_denom[i_p][i])<< tab << imag(all_denom[i_p][i])<< tab;
            write2<< scientific<<  real(Result[Si_tff.is({i_p,i})]*all_denom[i_p][i])<< tab << imag(Result[Si_tff.is({i_p,i})]*all_denom[i_p][i])<< tab;
            write3<< scientific<<  real(Result[Si_tff.is({i_p,i})])<< tab << imag(Result[Si_tff.is({i_p,i})])<< tab;

            Result[Si_tff.is({i_p,i})] = Result[Si_tff.is({i_p,i})]*all_denom[i_p][i];
        }
        write2<< scientific<<  real(Q*Qp) << tab << imag(Q*Qp)<< tab;
        write<<endl; write2<<endl; write3<<endl;
    }
    write<<endl<<endl; write2<<endl<<endl; write3<<endl<<endl;

    cout<<endl<<"|...........................................|"<<endl;
    cout<<"The form factor was calcauted!  ------------>    F[0]="<< Result[0]<<endl;

    return Result;


}

//Routine for creating solving the easiest set-up with Feynmann paramters for debugging:
void formfactor::debug_feynman_analytic(){

    int n_p=40;
    Line etapL(1.e-4,1.e+4,n_p,"log", "leg", false, false);


    SuperIndex<2> SI({5,n_p});
    VecDoub Result(5*n_p,0.0);

    double mM= (0.35)/(0.655);
    double prefactor = -1.0/(4.0*M_PI*M_PI*0.655) * mM;

    int n_a=60; int n_b=n_a; int n_c=n_a;
    Line aL(0.0,1.0, n_a, "linear", "leg", true, false);

    for (int i_p = 0; i_p < n_p; ++i_p){
        double etap= etapL.getGrid()[i_p];

        for (int i_a = 0; i_a < n_a; ++i_a) {
            double a= aL.getGrid()[i_a];

//            Line bL(-a ,a , n_b, "linear", "leg", true, false);
//            Line cL(0.0,(1-a)/2.0, n_c, "linear", "leg", true, false);

            VecDoub Sum_b(5*n_p,0.0);
            for (int i_b = 0; i_b < n_b; ++i_b) {
                double b= (2.0 * aL.getGrid()[i_b]) *a;

                VecDoub Sum_c(5*n_p,0.0);
                for (int i_c = 0; i_c < n_c; ++i_c) {

                    double c= (1.0-a)*aL.getGrid()[i_c];

                    double delta0  = c*(1.0-c) * etap  + (c*(1.0-c)+ b*b-a)/ (4.0) + mM*mM;
                    double beta = 2.0*b*b*c/(delta0);

                    double weights = aL.getWeights_integration()[i_c] * (1.0)/(delta0*delta0);
                    double jacobian = (2.0 * a * (1.0 - a));

                    Sum_c[SI.is({0,i_p})] += (-(1-2*c)*beta*etap*etap + b*b*etap+ (b*b-(1.0-c)*(1.0-c))/(4.0) + mM*mM) *weights *jacobian;
                    Sum_c[SI.is({1,i_p})] += (-(1-2*c)*beta*etap*etap+ b*b -c*c - 0.5*(1-2.0*c)*beta )*weights *jacobian;
                    Sum_c[SI.is({2,i_p})] += ((1-2*c)*beta*etap*etap + c*(1.0-c) - b*b)* weights *jacobian;
                    Sum_c[SI.is({3,i_p})] += (c*(c-2.0)-b*b) * weights * jacobian;
                    Sum_c[SI.is({4,i_p})] += ((1.0-2.0*c)*beta) * weights *jacobian;


                }

                for (int i = 0; i < 5; ++i) {
                    Sum_b[ SI.is({i,i_p}) ] += Sum_c[ SI.is({i,i_p}) ] * aL.getWeights_integration()[i_b];
                }
            }

            for (int j = 0; j < 5; ++j) {
                Result[ SI.is({j,i_p}) ] += Sum_b[ SI.is({j,i_p}) ] * aL.getWeights_integration()[i_a]*prefactor;
            }
        }

    }


    ofstream write(folder+ "feynman_calc.txt");
    assert( write.is_open());
    for (int i_p = 0; i_p < n_p ; ++i_p) {

//        int n_sigma=25;
//        Line sigma_line(1.e-4,1.e+4,n_sigma,"log", "leg", false, false);
//        double z_here=0.5;
//        Cdoub P_here= -0.685*0.685;
//        double sigma_here=sigma_line.getGrid()[i_p];
//        fv<Cdoub> p = get_p_vec(z_here,sigma_here);



//        fv<Cdoub> p = get_p_vec(alpha, ps.Q_Qp_P[i_p][0],ps.Q_Qp_P[i_p][1], ps.Q_Qp_P[i_p][2]);
//        fv<Cdoub> P = get_P_vec(alpha, ps.Q_Qp_P[i_p][0],ps.Q_Qp_P[i_p][1], ps.Q_Qp_P[i_p][2]);
//
//        fv<Cdoub> Q, Qp;
//        Q =  get_Q(P,p);
//        Qp = get_Qp(P,p);

        write<< scientific <<  etapL.getGrid()[i_p] << tab << 0.0 << tab << 0.0 << tab << 0.0 << tab;

        for (int i = 0; i < number_amps; ++i) {
            write<< scientific<<  real(Result[SI.is({i,i_p})])<< tab << 0.0 << tab;
        }
        write<<endl;
    }
    write<<endl<<endl;


    //Now I want to calculate the value for etap = 0.0.

    double etap=0.0;
    VecDoub Result0(5,0.0);

    for (int i_a = 0; i_a < n_a; ++i_a) {
        double a= aL.getGrid()[i_a];

        VecDoub Sum_b(5,0.0);
        for (int i_b = 0; i_b < n_b; ++i_b) {
            double b= (2.0 * aL.getGrid()[i_b]) *a;

            VecDoub Sum_c(5,0.0);
            for (int i_c = 0; i_c < n_c; ++i_c) {

                double c= (1.0-a)*aL.getGrid()[i_c];

                double delta0  = c*(1.0-c) * etap  + (c*(1.0-c)+ b*b-a)/ (4.0) + mM*mM;
                double beta = 2.0*b*b*c/(delta0);

                double jacobian = (2.0 * a * (1.0 - a));

                double weights = aL.getWeights_integration()[i_c] * (1.0)/(delta0*delta0);

                Sum_c[0] += (-(1-2*c)*beta*etap*etap + b*b*etap+ (b*b-(1.0-c)*(1.0-c))/(4.0) + mM*mM) *weights*jacobian;
                Sum_c[1] += (-(1-2*c)*beta*etap*etap+ b*b -c*c - 0.5*(1-2.0*c)*beta )*weights*jacobian;
                Sum_c[2] += ((1-2*c)*beta*etap*etap + c*(1.0-c) - b*b)* weights *jacobian;
                Sum_c[3] += (c*(c-2.0)-b*b) * weights * jacobian;
                Sum_c[4] += ((1.0-2.0*c)*beta) * weights * jacobian;


            }

            for (int i = 0; i < 5; ++i) {
                Sum_b[ i ] += Sum_c[ i ] * aL.getWeights_integration()[i_b];
            }
        }

        for (int j = 0; j < 5; ++j) {
            Result0[ j ] += Sum_b[ j ] * aL.getWeights_integration()[i_a]*prefactor;
        }
    }

    cout<<"The value fpr etap=0.0  is... = "<<Result0[0]<<tab<<Result0[1]<<tab<<Result0[2]<<tab<<Result0[3]<<tab<<Result0[4]<<endl;



}

//Checking the loop structure, aka is my integration really working? Seems like it. At least Mathematica values and C++ agreed with a sufficient amount of grid-points when comparing fourvector products.
//can be deleted soon.
//comment: This rountine was used to check if my integration rountine would actually r
void formfactor::second_debug_calc_formfactor() {


    int n_p=ps.Q_Qp_P.size();

    //important: Debuggin
    vector<vector<Cdoub>> all_denom(n_p,VecCdoub(number_amps,0.0));


    //Prefactor of the form factor integral, comes form the integration factor     ->  the symmetry factors of 2 are included is now below because of the differnt mesons.
    double Vorfac=1.0/((2*M_PI)*(2*M_PI)*(2*M_PI));

    SuperIndex<2> Si_tff({n_p,number_amps});
    VecCdoub Result; Result.resize(Si_tff.getNsuper_index());



////-------------------- start of loops ------------------------------------
//Sum over all values of the relative momentum p. To safe all values
for (int i_p = 0; i_p < n_p ; ++i_p) {

    //Getting relative momenta p and total momentum P form phase space.
    fv<Cdoub> p = get_p_vec(alpha, ps.Q_Qp_P[i_p][0],ps.Q_Qp_P[i_p][1], ps.Q_Qp_P[i_p][2]);
    fv<Cdoub> P = get_P_vec(alpha, ps.Q_Qp_P[i_p][0],ps.Q_Qp_P[i_p][1], ps.Q_Qp_P[i_p][2]);

    //important:
    //////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //----------- Watch out: Setting the realtion for Q and Q' here -----------
    fv<Cdoub> Q, Qp;
    Q =  get_Q(P,p);
    Qp = get_Qp(P,p);


    VecCdoub z_integration_sum(number_amps, 0.0); //init with 0
    //INTEGRATION- loop start: zl
    //angular integration form the loop integration in l. (z_l)
    for (int i_zl = 0; i_zl < ps.zl.getNumber_of_grid_points(); ++i_zl) {


        VecCdoub ll_integration_sum(number_amps, 0.0); //init with 0
        //INTEGRATION- loop start: ll
        //radial integration for the loop integration in l. (ll)
        for (int i_ll = 0; i_ll < ps.ll.getNumber_of_grid_points(); ++i_ll) {


            VecCdoub y_integration_sum(number_amps, 0.0); //init with 0

            //INTEGRATION- loop start: yl
            //angular integration for the loop integration in l. (y_l)
            for (int i_yl = 0; i_yl < ps.yl.getNumber_of_grid_points(); ++i_yl) {

                //Setting up the four vectors
                fv<Cdoub> ll, rp, rm, k3, k1, k2;
                ll.set_spherical(ps.ll.getGrid()[i_ll],0.0,ps.yl.getGrid()[i_yl],ps.zl.getGrid()[i_zl]);
                rp =  get_rp(P,p,ll);
                rm =  get_rm(P,p,ll);
                k1=get_kp(ll, P);
                k2=get_km(ll, P);
                k3=get_k3(ll,p);


                VecCdoub kern(12 * 12 * number_amps, 0.0);

                //New: If there is more then one scalar form factor amplitude then here number_amps takes care of that.
                VecCdoub sum_over_all_elemts(number_amps, 0.0); //int with 0

                sum_over_all_elemts[0] += (0.5 * (p*P) + (k3*k3) +  (k1*k2))/(ll*ll) ;


                for (int amp = 0; amp < number_amps; ++amp) {

                    //first sum - for y-integration: inculding the weights and denorminators of the quarks:
                    y_integration_sum[amp] +=
                            sum_over_all_elemts[amp] * ps.yl.getWeights_integration()[i_yl];// * quark_denom;

                }

            }//end y-integration loop

            for (int amp = 0; amp < number_amps; ++amp) {
                // second sum - for l-integration: including the weights and jacobian of radial integral:
                ll_integration_sum[amp] += y_integration_sum[amp] * 0.5 * ps.ll.getWeights_integration()[i_ll] *
                                           ps.ll.getGrid()[i_ll]; //Jacobian (1/2* l^2) and weights.
            }

        }//end of l-integration loop

        for (int amp = 0; amp < number_amps; ++amp) {
            //thirds and last sum - for zl-integration: including weights and jacobian of z:
            z_integration_sum[amp] += ll_integration_sum[amp] * ps.zl.getWeights_integration()[i_zl] *    //Weight
                                      sqrt(1.0 - ps.zl.getGrid()[i_zl] * ps.zl.getGrid()[i_zl]); // Jacobian sqrt(1-z^2)// )
        }


    }//end of z-integration



    cout << "The Result is:  " << z_integration_sum[0] * Vorfac << endl;

 }


}

//Using the Lorenz projectors instead:
//This form factor calc function is using the Lorenz elements as projectors.
// important: Make sure to use the corresponding kernel here!!!
VecCdoub formfactor::calc_formfactor_lorenz(vector<array<Cdoub,2>>& quark1,vector<array<Cdoub,2>>& quark2,
                                            vector<array<Cdoub,2>>& quark3, ps_amplitude<3>& bse){

    cout<<"Using function:  calc_formfactor_lorenz()"<<endl;
    //Variables of this function:
    array<Cdoub, 12> V1;
    array<Cdoub, 12> V2;

    int n_p=ps.Q_Qp_P.size();

    //important: Debuggin
    vector<vector<Cdoub>> all_denom(n_p,VecCdoub(number_amps,0.0));

    //Superindices:
    SuperIndex<4> Si_quark3({ps.ll.getNumber_of_grid_points(), ps.zl.getNumber_of_grid_points(),
                             ps.yl.getNumber_of_grid_points(), n_p});

    SuperIndex<2> Si_quark12({ps.ll.getNumber_of_grid_points(), ps.zl.getNumber_of_grid_points()});

    SuperIndex<4> Si_vertex({n_p, ps.yl.getNumber_of_grid_points(),  ps.zl.getNumber_of_grid_points(),
                             ps.ll.getNumber_of_grid_points()  });


    //important: DEBUGGING here- not used atm
    //obtaining the vertex values from Gernots interpolation -later mine
    matCdoub QPV1;
    matCdoub QPV2;

    int all_N= ps.ll.getNumber_of_grid_points()* ps.zl.getNumber_of_grid_points()*ps.yl.getNumber_of_grid_points()*n_p;

/*
    int test=qphv_interpol_ReadInn(QPV1,all_N,1,folder);
    if(test==0){cout<<"Calction Aborted QPV not found"<<endl;assert(false);}
    test=qphv_interpol_ReadInn(QPV2,all_N,2,folder);
    if(test==0){cout<<"Calction Aborted QPV not found"<<endl; assert(false);}
*/




    //Prefactor of the form factor integral, comes form the integration factor     ->  the symmetry factors of 2 are included is now below because of the differnt mesons.
    double Vorfac=1.0/((2*M_PI)*(2*M_PI)*(2*M_PI));

    SuperIndex<2> Si_tff({n_p,number_amps});
    VecCdoub Result; Result.resize(Si_tff.getNsuper_index());


////-------------------- start of loops ------------------------------------
    //Sum over all values of the relative momentum p. To safe all values
    for (int i_p = 0; i_p < n_p ; ++i_p) {


        //Calling the fourvectors from Q², Q'², P²:
        fv<Cdoub> p = get_p_vec(alpha, ps.Q_Qp_P[i_p][0],ps.Q_Qp_P[i_p][1], ps.Q_Qp_P[i_p][2]);
        fv<Cdoub> P = get_P_vec(alpha, ps.Q_Qp_P[i_p][0],ps.Q_Qp_P[i_p][1], ps.Q_Qp_P[i_p][2]);

        //important:
        //////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //----------- Watch out: Setting the realtion for Q and Q' here -----------
        //(maybe get into a function somewhere external at some points?)
        fv<Cdoub> Q, Qp;
        Q =  get_Q(P,p);
        Qp = get_Qp(P,p);
        //////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


        VecCdoub z_integration_sum(number_amps,0.0); //init with 0
        //INTEGRATION- loop start: zl
        //angular integration form the loop integration in l. (z_l)
        for (int i_zl = 0; i_zl < ps.zl.getNumber_of_grid_points(); ++i_zl) {


            VecCdoub ll_integration_sum(number_amps,0.0); //init with 0
            //INTEGRATION- loop start: ll
            //radial integration for the loop integration in l. (ll)
            for (int i_ll = 0; i_ll < ps.ll.getNumber_of_grid_points() ; ++i_ll) {


#ifndef K_USING_ALPHA_FRAME

                //important: Changed call for quark here cause now they depend on more. See above. In alpha!=big case.
                Cdoub Ak1= quark1[Si_quark12.is({i_ll, i_zl})][0];
                Cdoub Bk1= quark1[Si_quark12.is({i_ll, i_zl})][1];
                Cdoub Ak2= quark2[Si_quark12.is({i_ll, i_zl})][0];
                Cdoub Bk2= quark2[Si_quark12.is({i_ll, i_zl})][1];
#endif


                //Call + safe the amplitudes from the meson1
                //The loop runs till the size of Tensor Elements that the meson contains. (scalar 4, vector 8)
                VecCdoub meson;
                for (int amp = 0; amp < bse.getSi().getMaxima_of_partial_indices()[0]; ++amp) {
                    meson.push_back(bse( bse.getSi().is({amp,i_ll, i_zl}) ));
                }



                VecCdoub y_integration_sum(number_amps,0.0); //init with 0

                //INTEGRATION- loop start: yl
                //angular integration for the loop integration in l. (y_l)
                for (int i_yl = 0; i_yl < ps.yl.getNumber_of_grid_points(); ++i_yl) {


                    //Setting up the four vectors
                    fv<Cdoub> ll, rp, rm, k3, k1, k2;
                    ll.set_spherical(ps.ll.getGrid()[i_ll],0.0,ps.yl.getGrid()[i_yl],ps.zl.getGrid()[i_zl]);
                    rp =  get_rp(P,p,ll);
                    rm =  get_rm(P,p,ll);
                    k1=get_kp(ll, P);
                    k2=get_km(ll, P);
                    k3=get_k3(ll,p);


#ifdef K_USING_ALPHA_FRAME
                    //important the other change is here: when using alpha use the following -
                    Cdoub Ak1= quark1[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][0];
                    Cdoub Bk1= quark1[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][1];
                    Cdoub Ak2= quark2[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][0];
                    Cdoub Bk2= quark2[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][1];
#endif

                    Cdoub Ak3 = quark3[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][0];
                    Cdoub Bk3 = quark3[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][1];


                    //QUARK DENORMINATORS: - (TFFs ones below, as no dependence on inside loops.)
                    //Calculate the Denorminators of the quark propagators included!!!
                    //Frist is the denorminato of the 3rd Quark propagator,
                    // second the denorminator with form TFF projector, including the photon momenta.
                    //(The denorminator of S1 and S2 is already included in Chi.) - not anymore - changed that due to comparison.
                    //frist we start with the terms form the quark propagators
                    Cdoub quark_denom;
                    quark_denom = (1.0/((k3*k3)*Ak3*Ak3+Bk3*Bk3)) *   //(1.0/(2.0*((Q*Q)*(Qp*Qp)-pow(Q*Qp,2))))
                                  (1.0/((k1*k1)*Ak1*Ak1+Bk1*Bk1))*(1.0/((k2*k2)*Ak2*Ak2+Bk2*Bk2));

//                    quark_denom = 1.0;


                    //----------------------------------------------------------------------------
                    //Get the vertex dressing functions
                    Cdoub x1= 0.5*(Ak3+Ak1);
                    Cdoub x2= 0.5*(Ak3+Ak2);
                    Cdoub y1= (Ak3-Ak1)/(k3*k3-k1*k1);
                    Cdoub y2= (Ak2-Ak3)/(k2*k2-k3*k3);
                    Cdoub z1= (Bk3-Bk1)/(k3*k3-k1*k1);
                    Cdoub z2= (Bk2-Bk3)/(k2*k2-k3*k3);

//                    x1=1.0; y1=1.0; z1=1.0;
//                    x2=1.0; y2=1.0; z2=1.0;

                    //these are only the transverse components.
                    // The longitudinal are taken form the quark propagators aka Ball Chui part with WTI.
                    //-change to the array V1 and V2
                    V1[0] =  x1;
                    V1[1] =  2.0 *y1;
                    V1[2] =  2.0 *z1;
                    V1[3] = 0.0;

                    //important: Debuggin here  needs to be commented back inn.
//                    V1[4]=QPV1[0][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
//                    V1[5]=QPV1[1][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
//                    V1[6]=QPV1[2][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
//                    V1[7]=QPV1[3][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
//                    V1[8]=QPV1[4][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
//                    V1[9]=QPV1[5][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
//                    V1[10]=QPV1[6][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
//                    V1[11]=QPV1[7][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];

                    //These are only relevant for the new trace expression as the old one uses x,y,z directly
                    V2[0] = x2;
                    V2[1] =  2.0 *y2;
                    V2[2] =  2.0 *z2;
                    V2[3] = 0.0;

//                    V2[4]=QPV2[0][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
//                    V2[5]=QPV2[1][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
//                    V2[6]=QPV2[2][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
//                    V2[7]=QPV2[3][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
//                    V2[8]=QPV2[4][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
//                    V2[9]=QPV2[5][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
//                    V2[10]=QPV2[6][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
//                    V2[11]=QPV2[7][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];

                    //----------------------------------------------------------------------------

//                    //important: DEBUGGING HERE - setting values to 1.0:
//                    V1[0]=1.0; V1[1]=V1[2]=V1[3]=V1[4]=V1[5]=V1[6]=V1[7]=V1[8]=V1[9]=V1[10]=V1[11]=V1[12]=0.0;
//                    V2[0]=1.0; V2[1]=V2[2]=V2[3]=V2[4]=V2[5]=V2[6]=V2[7]=V2[8]=V2[9]=V2[10]=V2[11]=V2[12]=0.0;
                     V1[4]=V1[5]=V1[6]=V1[7]=V1[8]=V1[9]=V1[10]=V1[11]=V1[12]=0.0;
                     V2[4]=V2[5]=V2[6]=V2[7]=V2[8]=V2[9]=V2[10]=V2[11]=V2[12]=0.0;



                    //Calling the final trace expression inlcuding all the components together.
                    //The information will be safed in a long vector order in terms of the two quark-photon vertices
                    // Such that the total vector has 12x12= 144 compontens
                    // Inner loop is V2 (rm, -Q) and the outter V1 (rp,Qp) : V(1.....12) = f1/0*f2/1....f2/12
                    VecCdoub kern; kern.resize(12*12*number_amps);


                    //#########################################################################################

                    //important !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    //important !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    //important: Make sure the right kernel is choosen in set_kernel.
                    //original function:  ---- normally used ---------
                    //in order to get the right answer the pharsing of the vertex elements has to be right p yl,zl,ll
                    //and then also accessing of the vertex data in V1[Si(0)], V2[Si(1)]. Aka kern[i]. i<122.
//                    kernel_func(kern, P, Q, Qp, p, ll, rp, rm, k3, meson, Ak3, Bk3);
//                    kernel_func_amp_mf(kern, P, Q, Qp, p, ll, rp, rm, k3, meson, Ak3, Bk3, Ak1, Bk1, Ak2, Bk2);
                    kernel_func_amp_lorenz(kern, P, Q, Qp, p, ll, rp, rm, k3, meson, Ak3, Bk3, Ak1, Bk1, Ak2, Bk2);
                    //Gave the right result on 01.06.2018 for the anomaly value.


                    //#########################################################################################



                    //New: If there is more then one scalar form factor amplitude then here number_amps takes care of that.
                    VecCdoub sum_over_all_elemts(number_amps,0.0); //int with 0
                    SuperIndex<3> Si({number_amps,12,12});

                    //important: DEBUGGIN HERE
//                    cout<< ps.ll.getGrid()[i_ll]<<tab<<ps.yl.getGrid()[i_yl]<< tab<<ps.zl.getGrid()[i_zl]<<tab <<tab<<
//                        ps.Q_Qp_P[i_p][0]<<tab<<ps.Q_Qp_P[i_p][1]<<tab<<ps.Q_Qp_P[i_p][2]<<tab<<tab<<kern[Si.is({0,0,0})]<<endl;


                    //Watch out for the form code, starts form 0 or 1? check.. - should be alright. Kern start at 0.
                    //Now we have to add up the contributions and combine them with the right vertex dressing function
                    for (int i = 0; i < kern.size(); ++i) {
                        array<int,3> ex_i =  Si.are(i);
                        //important: This is the right ordering and the right call!! Never change it. V1[1] V2[2]
                        sum_over_all_elemts[ex_i[0]] += kern[i] * V1[ex_i[1]] * V2[ex_i[2]];

                    }


/*//                    cout for debuggin purposes:
//                    for (int j = 0; j < 12 ; ++j) {
//                        for (int i = 0; i < 12 ; ++i) {
//                            if(i!=3 && j!=3){
//                                int ki, kj;
//                                if(i>2){ki=i-1;}else{ki=i;}
//                                if(j>2){kj=j-1;}else{kj=j;}
//                                cout<<i<<" : "<<j<<tab<<Chi(kj,ki)<<tab<<kern[Si.is({j,i})]<<endl;
//                                cout<<i<<" : "<<j<<tab<<"Diff:  "<< (Chi(kj,ki) - kern[Si.is({j,i})] )<<endl;
//                            }
//                        }
//                    }
//                    cout<<endl;
//
//                    summing up the martix when using the old trace.
//                    Cdoub ChiA=0.0;
//                    for(int mat_i=0; mat_i<11; mat_i++)
//                    {
//                        for(int mat_j=0; mat_j<11; mat_j++)
//                        {
//                            ChiA += Chi(mat_i, mat_j);
//
//                        }
//                    }*/


                    for (int amp = 0; amp < number_amps; ++amp) {

                        //frist sum - for y-integration: inculding the weights and denorminators of the quarks:
                        y_integration_sum[amp] += sum_over_all_elemts[amp]* ps.yl.getWeights_integration()[i_yl] * quark_denom;

                    }

                }//end y-integration loop

                for (int amp = 0; amp < number_amps; ++amp) {
                    // second sum - for l-integration: including the weights and jacobian of radial integral:
                    ll_integration_sum[amp] += y_integration_sum[amp] * 0.5* ps.ll.getWeights_integration()[i_ll]* ps.ll.getGrid()[i_ll]; //Jacobian (1/2* l^2) and weights.
                }

            }//end of l-integration loop

            for (int amp = 0; amp < number_amps; ++amp) {
                //thirds and last sum - for zl-integration: including weights and jacobian of z:
                z_integration_sum[amp] += ll_integration_sum[amp] * ps.zl.getWeights_integration()[i_zl] *    //Weight
                                          sqrt(1.0-ps.zl.getGrid()[i_zl]*ps.zl.getGrid()[i_zl]); // Jacobian sqrt(1-z^2)// )
            }


        }//end of z-integration


        //------------TFF DENORMINATORS: is not nessercary anymore as we are furthermore multiplying with the rotation matrix.
        //Do not forget the symmetry factor of 2.0 though -> below in final result multiplication.
        VecCdoub denom(number_amps);
        if(which_TFF=="ps" ){
            //only one form factor amplitude with the following denorminator:
            all_denom[i_p][0] = ( - 2.0/(2.0 *( (Q*Q)*(Qp*Qp)-pow(Q*Qp,2)) ));
            // inculding a symmetry factor of 2.0 and then Projector norm
        }else if(which_TFF=="scalar" ){

            //Here I implemented an extra From code to provide me with the expression for the denorminator
            // as in case of the scalar they are more complicated. Here number_amps=5.
            denom_func(denom,Q,Qp);
            //---------------------------------------------------------------
            //DEBUGGING HERE 08.02.19
            all_denom[i_p][0] = denom[0]; all_denom[i_p][1] = denom[1]; all_denom[i_p][2] = denom[2]; all_denom[i_p][3] = denom[3]; all_denom[i_p][4] = denom[4];


        }else if(which_TFF=="axial"){
            //important: Not jet ready!!! Need to implement it.
        }

        all_denom[i_p][0] = 1.0; all_denom[i_p][1] = 1.0; all_denom[i_p][2] = 1.0; all_denom[i_p][3] = 1.0; all_denom[i_p][4] = 1.0;



        //Perform rotation to obtain the wanted dressing functions f[i]:

        VecCdoub rotated_fi(number_amps,0.0); //init with 0
        realting_f_with_lorenz(rotated_fi, Q, Qp, z_integration_sum);
//        rotated_fi=z_integration_sum;

        //Sum to safe all the amplitude multiplied with the denorminator & Prefactor (Vorfactor) of the TFF projectors and the symmetry factor here.
        for (int amp = 0; amp < number_amps; ++amp) {
            Result[Si_tff.is({i_p,amp})] =  rotated_fi[amp] * Vorfac * 2.0;
        }

    }//p- loobsep to save the values in corresponding result vector.



    cout<<endl<<"|...........................................|"<<endl;
    cout<<"The form factor was calcauted!  ------------>    F[0]="<< Result[0]<<endl;


    //Writing out the form factor with the append function such that we safe the result for all outside Q^2 values.
    ofstream write;
    write.open(folder+ "All_formfactor_all.txt", fstream::app);
    for (int i_p = 0; i_p < n_p ; ++i_p) {

//        fv<Cdoub> p = get_p_vec(alpha, ps.Q_Qp_P[i_p][0],ps.Q_Qp_P[i_p][1], ps.Q_Qp_P[i_p][2]);
//        write<< scientific <<  0.25*real(p*p)<< tab << 0.25*imag(p*p) << tab << 0.0 << tab <<0.0 << tab;

        write<< scientific <<  real(ps.Q_Qp_P[i_p][0])<< tab << imag(ps.Q_Qp_P[i_p][0]) << tab << real(ps.Q_Qp_P[i_p][1]) << tab << imag(ps.Q_Qp_P[i_p][1]) << tab;
        for (int i = 0; i < number_amps; ++i) {
            write<< scientific<<  real(Result[Si_tff.is({i_p,i})]*all_denom[i_p][i])<< tab << imag(Result[Si_tff.is({i_p,i})]*all_denom[i_p][i])<< tab;
        }
        write<<endl;
    }
    write<<endl<<endl;

    return Result;

}

//important: this can soon be deleted if the above works.
//first function for debuggingg - agreed with the debug_calc_formfactor() on 25.04.19. for the easy set-up with omega values and bar vertices.
VecCdoub formfactor::debug_calc_formfactor_lorenz(vector<array<Cdoub,2>>& quark1,vector<array<Cdoub,2>>& quark2,
                                     vector<array<Cdoub,2>>& quark3, ps_amplitude<3>& bse){

    cout<<"Using function:  calc_formfactor_lorenz()"<<endl;
    //Variables of this function:
    array<Cdoub, 12> V1;
    array<Cdoub, 12> V2;

    int n_p=ps.Q_Qp_P.size();

    //important: Debuggin
    vector<vector<Cdoub>> all_denom(n_p,VecCdoub(number_amps,0.0));

    //Superindices:
    SuperIndex<4> Si_quark3({ps.ll.getNumber_of_grid_points(), ps.zl.getNumber_of_grid_points(),
                             ps.yl.getNumber_of_grid_points(), n_p});

    SuperIndex<2> Si_quark12({ps.ll.getNumber_of_grid_points(), ps.zl.getNumber_of_grid_points()});

    SuperIndex<4> Si_vertex({n_p, ps.yl.getNumber_of_grid_points(),  ps.zl.getNumber_of_grid_points(),
                             ps.ll.getNumber_of_grid_points()  });


#ifdef K_WITH_FULL_QPHV
    //important: DEBUGGING here- not used atm
    //obtaining the vertex values from Gernots interpolation -later mine
    matCdoub QPV1;
    matCdoub QPV2;

    int all_N= ps.ll.getNumber_of_grid_points()* ps.zl.getNumber_of_grid_points()*ps.yl.getNumber_of_grid_points()*n_p;

    int test=qphv_interpol_ReadInn(QPV1,all_N,1,folder);
    if(test==0){cout<<"Calction Aborted QPV not found"<<endl;assert(false);}
    test=qphv_interpol_ReadInn(QPV2,all_N,2,folder);
    if(test==0){cout<<"Calction Aborted QPV not found"<<endl; assert(false);}
#endif



    //Prefactor of the form factor integral, comes form the integration factor     ->  the symmetry factors of 2 are included is now below because of the differnt mesons.
    double Vorfac=1.0/((2*M_PI)*(2*M_PI)*(2*M_PI));

    SuperIndex<2> Si_tff({n_p,number_amps});
    VecCdoub Result; Result.resize(Si_tff.getNsuper_index());


////-------------------- start of loops ------------------------------------
    //Sum over all values of the relative momentum p. To safe all values
    for (int i_p = 0; i_p < n_p ; ++i_p) {


        //Calling the fourvectors from Q², Q'², P²:
        fv<Cdoub> p = get_p_vec(alpha, ps.Q_Qp_P[i_p][0],ps.Q_Qp_P[i_p][1], ps.Q_Qp_P[i_p][2]);
        fv<Cdoub> P = get_P_vec(alpha, ps.Q_Qp_P[i_p][0],ps.Q_Qp_P[i_p][1], ps.Q_Qp_P[i_p][2]);

        //important:
        //////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //----------- Watch out: Setting the realtion for Q and Q' here -----------
        //(maybe get into a function somewhere external at some points?)
        fv<Cdoub> Q, Qp;
        Q =  get_Q(P,p);
        Qp = get_Qp(P,p);
        //////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


        VecCdoub z_integration_sum(number_amps,0.0); //init with 0
        //INTEGRATION- loop start: zl
        //angular integration form the loop integration in l. (z_l)
        for (int i_zl = 0; i_zl < ps.zl.getNumber_of_grid_points(); ++i_zl) {


            VecCdoub ll_integration_sum(number_amps,0.0); //init with 0
            //INTEGRATION- loop start: ll
            //radial integration for the loop integration in l. (ll)
            for (int i_ll = 0; i_ll < ps.ll.getNumber_of_grid_points() ; ++i_ll) {


#ifndef K_USING_ALPHA_FRAME
#ifndef K_USING_QUARK_MODEL
                //important: Changed call for quark here cause now they depend on more. See above. In alpha!=big case.
                Cdoub Ak1= quark1[Si_quark12.is({i_ll, i_zl})][0];
                Cdoub Bk1= quark1[Si_quark12.is({i_ll, i_zl})][1];
                Cdoub Ak2= quark2[Si_quark12.is({i_ll, i_zl})][0];
                Cdoub Bk2= quark2[Si_quark12.is({i_ll, i_zl})][1];
#endif
#endif


                //Call + safe the amplitudes from the meson1
                //The loop runs till the size of Tensor Elements that the meson contains. (scalar 4, vector 8)
                VecCdoub meson(4,0.0);

#ifndef K_USING_MESON_MODEL
                for (int amp = 0; amp < bse.getSi().getMaxima_of_partial_indices()[0]; ++amp) {
                    meson[amp] = (bse( bse.getSi().is({amp,i_ll, i_zl}) ));
                }
#endif



                VecCdoub y_integration_sum(number_amps,0.0); //init with 0

                //INTEGRATION- loop start: yl
                //angular integration for the loop integration in l. (y_l)
                for (int i_yl = 0; i_yl < ps.yl.getNumber_of_grid_points(); ++i_yl) {


                    //Setting up the four vectors
                    fv<Cdoub> ll, rp, rm, k3, k1, k2;
                    ll.set_spherical(ps.ll.getGrid()[i_ll],0.0,ps.yl.getGrid()[i_yl],ps.zl.getGrid()[i_zl]);
                    rp =  get_rp(P,p,ll);
                    rm =  get_rm(P,p,ll);
                    k1=get_kp(ll, P);
                    k2=get_km(ll, P);
                    k3=get_k3(ll,p);

#ifdef K_USING_ALPHA_FRAME
#ifndef K_EVERYTHING_BARE
                    //important the other change is here: when using alpha use the following -
                    Cdoub Ak1= quark1[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][0];
                    Cdoub Bk1= quark1[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][1];
                    Cdoub Ak2= quark2[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][0];
                    Cdoub Bk2= quark2[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][1];
#endif
#endif

#ifndef K_USING_QUARK_MODEL
                    Cdoub Ak3 = quark3[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][0];
                    Cdoub Bk3 = quark3[Si_quark3.is({i_ll, i_zl, i_yl,i_p})][1];
#endif

#ifdef K_USING_QUARK_MODEL

//                    Cdoub Ak1= 1.0+ 0.6/(1.0 + (k1*k1));
//                    Cdoub Bk1= 0.8/(1.0 + (k1*k1)) ;
//                    Cdoub Ak2= 1.0+ 0.6/(1.0 + (k2*k2));
//                    Cdoub Bk2= 0.8/(1.0 + (k2*k2));
//                    Cdoub Ak3 =  1.0+ 0.6/(1.0 + (k3*k3));
//                    Cdoub Bk3 = 0.8/(1.0 + (k3*k3));

//                    Cdoub Ak1= 1.0;
//                    Cdoub Bk1= 0.0035748;
//                    Cdoub Ak2= 1.0;
//                    Cdoub Bk2= 0.0035748;
//                    Cdoub Ak3 =  1.0;
//                    Cdoub Bk3 = 0.0035748;

                    //This part comes form comparing with the Julia integral: For this quark mass the integral works
//                    Cdoub Ak1= 1.0;
//                    Cdoub Bk1= 0.35;
//                    Cdoub Ak2= 1.0;
//                    Cdoub Bk2= 0.35;
//                    Cdoub Ak3 = 1.0;
//                    Cdoub Bk3 = 0.35;

                    //This part comes from comparing with the easy Gernot set-up.
                    Cdoub Ak1= 1.0;
                    Cdoub Bk1= 0.5534;
                    Cdoub Ak2= 1.0;
                    Cdoub Bk2= 0.5534;
                    Cdoub Ak3 = 1.0;
                    Cdoub Bk3 = 0.5534;


#endif


                    //QUARK DENORMINATORS: - (TFFs ones below, as no dependence on inside loops.)
                    //Calculate the Denorminators of the quark propagators included!!!
                    //Frist is the denorminato of the 3rd Quark propagator,
                    // second the denorminator with form TFF projector, including the photon momenta.
                    //(The denorminator of S1 and S2 is already included in Chi.) - not anymore - changed that due to comparison.
                    //frist we start with the terms form the quark propagators
                    Cdoub quark_denom;
                    quark_denom = (1.0/((k3*k3)*Ak3*Ak3+Bk3*Bk3)) *
                                  (1.0/((k1*k1)*Ak1*Ak1+Bk1*Bk1))*(1.0/((k2*k2)*Ak2*Ak2+Bk2*Bk2));

//                    quark_denom = 1.0;


                    //----------------------------------------------------------------------------
                    //Get the vertex dressing functions - Version (a)
                    Cdoub x1= 0.5*(Ak3+Ak1);
                    Cdoub x2= 0.5*(Ak3+Ak2);
                    Cdoub y1= (Ak3-Ak1)/(k3*k3-k1*k1);
                    Cdoub y2= (Ak2-Ak3)/(k2*k2-k3*k3);
                    Cdoub z1= (Bk3-Bk1)/(k3*k3-k1*k1);
                    Cdoub z2= (Bk2-Bk3)/(k2*k2-k3*k3);

                    // Debugging here... 28.09.18 but makes no difference if consistent - Version (b)

//                    Cdoub x2= 0.5*(Ak3+Ak1);
//                    Cdoub x1= 0.5*(Ak3+Ak2);
//                    Cdoub y2= (Ak3-Ak1)/(k3*k3-k1*k1);
//                    Cdoub y1= (Ak2-Ak3)/(k2*k2-k3*k3);
//                    Cdoub z2= (Bk3-Bk1)/(k3*k3-k1*k1);
//                    Cdoub z1= (Bk2-Bk3)/(k2*k2-k3*k3);

                    //these are only the transverse components.
                    // The longitudinal are taken form the quark propagators aka Ball Chui part with WTI.
                    //-change to the array V1 and V2
                    V1[0] =  x1;
                    V1[1] =  2.0 *y1;
                    V1[2] =  2.0 *z1;
                    V1[3] = 0.0;

#ifdef K_WITH_FULL_QPHV
                    V1[4]=QPV1[0][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
                    V1[5]=QPV1[1][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
                    V1[6]=QPV1[2][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
                    V1[7]=QPV1[3][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
                    V1[8]=QPV1[4][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
                    V1[9]=QPV1[5][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
                    V1[10]=QPV1[6][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
                    V1[11]=QPV1[7][Si_vertex.is({i_p, i_yl, i_zl, i_ll})];
#endif
                    //These are only relevant for the new trace expression as the old one uses x,y,z directly
                    V2[0] = x2;
                    V2[1] =  2.0 *y2;
                    V2[2] =  2.0 *z2;
                    V2[3] = 0.0;

#ifdef K_WITH_FULL_QPHV
                    V2[4]=QPV2[0][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
                    V2[5]=QPV2[1][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
                    V2[6]=QPV2[2][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
                    V2[7]=QPV2[3][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
                    V2[8]=QPV2[4][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
                    V2[9]=QPV2[5][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
                    V2[10]=QPV2[6][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];
                    V2[11]=QPV2[7][Si_vertex.is({ i_p, i_yl, i_zl, i_ll})];

//                    V1[4]=V2[4]=0.0;
//                    V1[5]=V2[5]=0.0;
//                    V1[6]=V2[6]=0.0;
//                    V1[7]=V2[7]=0.0;
//                    V1[8]=V2[8]=0.0;
//                    V1[9]=V2[9]=0.0;
//                    V1[10]=V2[10]=0.0;
//                    V1[11]=V2[11]=0.0;
#endif
                    //----------------------------------------------------------------------------

#ifndef K_WITH_FULL_QPHV
                    //important: DEBUGGING HERE - setting values to 0.0: for BC
                    V1[4]=V1[5]=V1[6]=V1[7]=V1[8]=V1[9]=V1[10]=V1[11]=0.0;
                    V2[4]=V2[5]=V2[6]=V2[7]=V2[8]=V2[9]=V2[10]=V2[11]=0.0;
#ifdef K_WITH_BARE_VERTEX
                    V1[1]=V1[2]=V1[3]=0; V1[0] = 1.0; //V1[0] = 0.972241; //28 //V1[0] =0.9843; // 30 for Z2 value
                    V2[1]=V2[2]=V2[3]=0; V2[0] = 1.0; //V2[0] = 0.972241; //28//V2[0] =0.9843; //30
#endif
#endif


#ifdef K_USING_MESON_MODEL

                    meson[0]= 1.0/(ps.ll.getGrid()[i_ll] + 0.55*0.55);
//                    meson[0]= 1.0/(ps.ll.getGrid()[i_ll] + 0.35*0.35);
                    meson[1]=meson[2]=meson[3]=0.0;

                    //model used when comparing to Richard.
//                    meson[0]= 6.0/(1.0+ps.ll.getGrid()[i_ll]);
//                    meson[1]= 6.0/(1.0+ps.ll.getGrid()[i_ll]*ps.ll.getGrid()[i_ll]);
//                    meson[2]= 3.0/(1.0+ps.ll.getGrid()[i_ll]*ps.ll.getGrid()[i_ll]);
//                    meson[3]= -2.0/(1.0+ps.ll.getGrid()[i_ll]*ps.ll.getGrid()[i_ll]);
#endif
#ifdef K_USING_ONLY_FIRST_AMP_MESON
                meson[1]=meson[2]=meson[3]=0.0;
#endif

                    //Calling the final trace expression inlcuding all the components together.
                    //The information will be safed in a long vector order in terms of the two quark-photon vertices
                    // Such that the total vector has 12x12= 144 compontens
                    // Inner loop is V2 (rm, -Q) and the outter V1 (rp,Qp) : V(1.....12) = f1/0*f2/1....f2/12
                    VecCdoub kern; kern.resize(12*12*number_amps);


                    //#########################################################################################

                    //important !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    //important !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    //important: Make sure the right kernel is choosen in set_kernel.
                    //original function:  ---- normally used ---------
                    //in order to get the right answer the pharsing of the vertex elements has to be right p yl,zl,ll
                    //and then also accessing of the vertex data in V1[Si(0)], V2[Si(1)]. Aka kern[i]. i<122.
//                    kernel_func(kern, P, Q, Qp, p, ll, rp, rm, k3, meson, Ak3, Bk3);
//                    kernel_func_amp_mf(kern, P, Q, Qp, p, ll, rp, rm, k3, meson, Ak3, Bk3, Ak1, Bk1, Ak2, Bk2);
                    //important: Chnanges ordering hrer.
                    kernel_func_amp_lorenz(kern, P, Q, Qp, p, ll, rp, rm, k3, meson, Ak3, Bk3, Ak1, Bk1, Ak2, Bk2);
                    //Gave the right result on 01.06.2018 for the anomaly value.


                    //#########################################################################################



                    //New: If there is more then one scalar form factor amplitude then here number_amps takes care of that.
                    VecCdoub sum_over_all_elemts(number_amps,0.0); //int with 0
                    SuperIndex<3> Si({number_amps,12,12});

                    //important: DEBUGGIN HERE
//                    cout<< ps.ll.getGrid()[i_ll]<<tab<<ps.yl.getGrid()[i_yl]<< tab<<ps.zl.getGrid()[i_zl]<<tab <<tab<<
//                        ps.Q_Qp_P[i_p][0]<<tab<<ps.Q_Qp_P[i_p][1]<<tab<<ps.Q_Qp_P[i_p][2]<<tab<<tab<<kern[Si.is({0,0,0})]<<endl;


                    //Watch out for the form code, starts form 0 or 1? check.. - should be alright. Kern start at 0.
                    //Now we have to add up the contributions and combine them with the right vertex dressing function
                    for (int i = 0; i < kern.size(); ++i) {
                        array<int,3> ex_i =  Si.are(i);
                        //important: This is the right ordering and the right call!! Never change it. V1[1] V2[2]
                        sum_over_all_elemts[ex_i[0]] += kern[i] * V1[ex_i[1]] * V2[ex_i[2]]; // Verion (a) not according to form but consistent if V1(k1,k3) and V2(k2,k3) as choosen above
//                        sum_over_all_elemts[ex_i[0]] += kern[i] * V1[ex_i[2]] * V2[ex_i[1]]; //Version (b): V1(k2,k3) V2(k1,k3) both works just consisitncy is important!
                    }


/*//                    cout for debuggin purposes:
//                    for (int j = 0; j < 12 ; ++j) {
//                        for (int i = 0; i < 12 ; ++i) {
//                            if(i!=3 && j!=3){
//                                int ki, kj;
//                                if(i>2){ki=i-1;}else{ki=i;}
//                                if(j>2){kj=j-1;}else{kj=j;}
//                                cout<<i<<" : "<<j<<tab<<Chi(kj,ki)<<tab<<kern[Si.is({j,i})]<<endl;
//                                cout<<i<<" : "<<j<<tab<<"Diff:  "<< (Chi(kj,ki) - kern[Si.is({j,i})] )<<endl;
//                            }
//                        }
//                    }
//                    cout<<endl;
//
//                    summing up the martix when using the old trace.
//                    Cdoub ChiA=0.0;
//                    for(int mat_i=0; mat_i<11; mat_i++)
//                    {
//                        for(int mat_j=0; mat_j<11; mat_j++)
//                        {
//                            ChiA += Chi(mat_i, mat_j);
//
//                        }
//                    }*/


                    for (int amp = 0; amp < number_amps; ++amp) {

                        //frist sum - for y-integration: inculding the weights and denorminators of the quarks:
                        y_integration_sum[amp] += sum_over_all_elemts[amp]* ps.yl.getWeights_integration()[i_yl] * quark_denom;

                    }

                }//end y-integration loop

                for (int amp = 0; amp < number_amps; ++amp) {
                    // second sum - for l-integration: including the weights and jacobian of radial integral:
                    ll_integration_sum[amp] += y_integration_sum[amp] * 0.5* ps.ll.getWeights_integration()[i_ll]* ps.ll.getGrid()[i_ll]; //Jacobian (1/2* l^2) and weights.
                }

            }//end of l-integration loop

            for (int amp = 0; amp < number_amps; ++amp) {
                //thirds and last sum - for zl-integration: including weights and jacobian of z:
                z_integration_sum[amp] += ll_integration_sum[amp] * ps.zl.getWeights_integration()[i_zl] *    //Weight
                                          sqrt(1.0-ps.zl.getGrid()[i_zl]*ps.zl.getGrid()[i_zl]); // Jacobian sqrt(1-z^2)// )
            }


        }//end of z-integration


        //------------TFF DENORMINATORS: is not nessercary anymore as we are furthermore multiplying with the rotation matrix.
        //Do not forget the symmetry factor of 2.0 though -> below in final result multiplication.
        VecCdoub denom(number_amps);
        if(which_TFF=="ps" ){
            //only one form factor amplitude with the following denorminator:
            all_denom[i_p][0] = ( - 1.0/(2.0 *( (Q*Q)*(Qp*Qp)-pow(Q*Qp,2)) ));
            // inculding a symmetry factor of 2.0 and then Projector norm
        }else if(which_TFF=="scalar" ){

            //Here I implemented an extra From code to provide me with the expression for the denorminator
            // as in case of the scalar they are more complicated. Here number_amps=5.
            //important: Changed ordering here.
            //new - uncomment on 22.10.19
//            denom_func(denom,Q,Qp);

            //old
//            denom_func(denom,Qp,Q);
            //---------------------------------------------------------------
            //DEBUGGING HERE 08.02.19
            all_denom[i_p][0] = denom[0]; all_denom[i_p][1] = denom[1]; all_denom[i_p][2] = denom[2]; all_denom[i_p][3] = denom[3]; all_denom[i_p][4] = denom[4];


        }else if(which_TFF=="axial"){
            //important: Not jet ready!!! Need to implement it.
        }

        all_denom[i_p][0] = 1.0; all_denom[i_p][1] = 1.0; all_denom[i_p][2] = 1.0; all_denom[i_p][3] = 1.0; all_denom[i_p][4] = 1.0;



        //Perform rotation to obtain the wanted dressing functions f[i]:

        VecCdoub rotated_fi(number_amps,0.0); //init with 0
        realting_f_with_lorenz(rotated_fi, Q, Qp, z_integration_sum);
//        rotated_fi=z_integration_sum;

        //Sum to safe all the amplitude multiplied with the denorminator & Prefactor (Vorfactor) of the TFF projectors and the symmetry factor here.
        for (int amp = 0; amp < number_amps; ++amp) {
            Result[Si_tff.is({i_p,amp})] =  rotated_fi[amp] * Vorfac * 2.0;
        }

    }//p- loobsep to save the values in corresponding result vector.



    cout<<endl<<"|...........................................|"<<endl;
    cout<<"The form factor was calcauted!  ------------>    F[0]="<< Result[0]<<endl;


    //Writing out the form factor with the append function such that we safe the result for all outside Q^2 values.
    ofstream write;
    write.open(folder+ "All_formfactor_all.txt", fstream::app);
    for (int i_p = 0; i_p < n_p ; ++i_p) {

//        fv<Cdoub> p = get_p_vec(alpha, ps.Q_Qp_P[i_p][0],ps.Q_Qp_P[i_p][1], ps.Q_Qp_P[i_p][2]);


        write<< scientific <<  real(ps.Q_Qp_P[i_p][0])<< tab << imag(ps.Q_Qp_P[i_p][0]) << tab << real(ps.Q_Qp_P[i_p][1]) << tab << imag(ps.Q_Qp_P[i_p][1]) << tab;
        //this line was used for the previous comparison in sigma: case=5.
//        write<< scientific <<  0.25*real(p*p)<< tab << 0.25*imag(p*p) << tab << 0.0 << tab <<0.0 << tab;
        for (int i = 0; i < number_amps; ++i) {
            write<< scientific<<  real(Result[Si_tff.is({i_p,i})]*all_denom[i_p][i])<< tab << imag(Result[Si_tff.is({i_p,i})]*all_denom[i_p][i])<< tab;
        }
        write<<endl;
    }
    write<<endl<<endl;

    return Result;

}


//choosing which formfactor kernel: scalar, pseudo-scalar, axial..
void formfactor::set_kernel_func() {

    if(which_TFF == "ps" ){

        kernel_func= kernel_formfactor_pion;
        kernel_func_amp_mf=kernel_formfactor_pion_amp_mf;
        number_amps=1;

    }else if(which_TFF == "scalar")
    {
        kernel_func = kernel_formfactor_scalar;

        //important: DEBUGGING HERE!
        //The normal kernel with using the explicit amplitude instead of the wavefunction:´
        // ---- this is currently check if this delivers the right result.
        kernel_func_amp_mf=kernel_formfactor_scalar_amp_mf;

        //This kernel was used for cross checking the form code of the scalar as this was an easier version that Richard put together
//        kernel_func_amp_mf=easy_kernel_formfactor_scalar_amp_mf;

        //This kernel is using the Lorenz structure as Projectors,
        // this is used in the calc_formfactor where the special trafo in the ende is needed.
        kernel_func_amp_lorenz=kernel_formfactor_scalar_amp_lorenz;

        denom_func=get_donorminator_scalar;
        number_amps=5;

    }else if(which_TFF == "axial"){

        //not jet implemented
        kernel_func = kernel_formfactor_axial;
        number_amps=1;

    }
    else{cout<<"This form factor kernel is not exsisting - which_kernel =? "<<endl;  assert(false); }
}


//write functions:
void formfactor::write(double Vorfac, VecCdoub& Result, string name) {

//    cout<<"Hello in write: "<<Result.size()<<endl;
    int size_Qp = ps.Q_Qp_P.size();
    SuperIndex<2> tff_si({size_Qp,number_amps});
    ofstream write;
    write.open(folder+ name + "formfactor.txt");

//    fv<Cdoub> P;
//    P.content[3] = i_ * ps.getPhaseSpacePsquared();

    for (int i_p = 0; i_p < size_Qp; ++i_p) {

//        fv<Cdoub> p= get_p_vec(ps.Q_Qp[i_p][0],ps.Q_Qp[i_p][1], ps.getPhaseSpacePsquared());
//        fv<Cdoub> Q, Qp;
//        Q= 0.5*(p+P);
//        Qp= 0.5*(p-P);

        write<< scientific <<real(ps.Q_Qp_P[i_p][0])<<tab<<imag(ps.Q_Qp_P[i_p][0])<<tab
             <<real(ps.Q_Qp_P[i_p][1])<<tab<<imag(ps.Q_Qp_P[i_p][1]) << tab;
//             << real((ps.Q_Qp[i_p][0]+ps.Q_Qp[i_p][1])*0.5) << tab << imag((ps.Q_Qp[i_p][0]+ps.Q_Qp[i_p][1])*0.5)<< tab;

        for (int amp = 0; amp < number_amps; ++amp) {
            write<< scientific<< real(Result[ tff_si.is({i_p,amp}) ] ) * Vorfac <<tab<<imag(Result[tff_si.is({i_p,amp})]) * Vorfac << tab;
        }

//        write<<real(Q*Qp)<<tab<<imag(Q*Qp)<<tab;

        write<<endl;
    }

    write.close();

}

void formfactor::write_ad(double Vorfac, VecCdoub& Result, string name) {

//    cout<<"Hello in write: "<<Result.size()<<endl;
    int size_Qp = ps.Q_Qp_P.size();
    SuperIndex<2> tff_si({size_Qp,number_amps});
    ofstream write;
    write.open(folder+ name + "formfactor.txt", fstream::app);

//    fv<Cdoub> P;
//    P.content[3] = i_ * ps.getPhaseSpacePsquared();

    for (int i_p = 0; i_p < size_Qp; ++i_p) {

        write<< scientific <<real(ps.Q_Qp_P[i_p][0])<<tab<<imag(ps.Q_Qp_P[i_p][0])<<tab
             <<real(ps.Q_Qp_P[i_p][1])<<tab<<imag(ps.Q_Qp_P[i_p][1]) << tab;
//             << real((ps.Q_Qp[i_p][0]+ps.Q_Qp[i_p][1])*0.5) << tab << imag((ps.Q_Qp[i_p][0]+ps.Q_Qp[i_p][1])*0.5)<< tab;

        for (int amp = 0; amp < number_amps; ++amp) {
            write<< scientific<< real(Result[ tff_si.is({i_p,amp}) ] ) * Vorfac <<tab<<imag(Result[tff_si.is({i_p,amp})]) * Vorfac << tab;
        }

//        write<<real(Q*Qp)<<tab<<imag(Q*Qp)<<tab;

        write<<endl;
    }
    write<<endl<<endl;

    write.close();

}

void formfactor::write_normed(VecCdoub &Result, string name) {

    int size_Qp = ps.Q_Qp_P.size();
    Cdoub Vorfac;
    SuperIndex<2> tff_si({size_Qp,number_amps});
    ofstream write;
    write.open(folder+ name + "formfactor.txt");

    for (int i_p = 0; i_p < size_Qp; ++i_p) {
        write<< scientific <<real(ps.Q_Qp_P[i_p][0])<<tab<<imag(ps.Q_Qp_P[i_p][0])<<tab
             <<real(ps.Q_Qp_P[i_p][1])<<tab<<imag(ps.Q_Qp_P[i_p][1]) << tab;

        for (int amp = 0; amp < number_amps; ++amp) {
            Vorfac= 1.0/Result[tff_si.is({0,amp})];
            write<< scientific<< real(Result[ tff_si.is({i_p,amp}) ]  * Vorfac ) <<tab<<imag(Result[tff_si.is({i_p,amp})] * Vorfac ) << tab;
        }

        write<<endl;
    }

    write.close();

}


//------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
//Functions that realted to the form factor but not as part of the class
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Write out the value for the quark-photon vertex:
void output_qphv_grid(string folder,TFF_ps& ps, int which_vertex, double alpha)
{

    fv<Cdoub> p ,Q, P ,ll, r;
    Cdoub rr, QQ, Qr;

//    P.content[3]=i_*ps.getPhaseSpacePsquared();
    int nll=ps.ll.getNumber_of_grid_points(); int nz=ps.zl.getNumber_of_grid_points();
    int ny=ps.yl.getNumber_of_grid_points(); int np = ps.Q_Qp_P.size();

    stringstream help;
    help<<which_vertex;
    string buffer = folder+"quarkphoton_grid_"+help.str()+".txt";
    ofstream f(buffer.c_str());


//    f<<"#   pionts="<<n*m*nz*ny<<"  pionM:"<<M<<"   np="<<n<<" , nrad="<<m<<" , nz="<<nz<<" , ny="<<ny<<endl;
//    f<<"#--------------------------------------------------# "<<endl;
//    f<<"#real(p)    imag(p) real(Q) imag(Q)"<<endl;

//    f<<endl<<tab<<tab<<nll*nz*ny<<endl<<endl<<tab<<tab<<"k2"<<tab<<tab<<tab<<"Q2"<<endl<<endl;
    f<<endl<<tab<<tab<<nll*nz*ny*np<<endl<<endl<<tab<<tab<<"k2"<<tab<<tab<<tab<<"kQ"<<tab<<tab<<tab<<"Q2"<<endl<<endl;


        for (int i_ll = 0; i_ll < nll; ++i_ll) {
            for (int i_zl = 0; i_zl < nz; ++i_zl) {
                for (int i_yl = 0; i_yl < ny; ++i_yl) {


                ll.set_spherical(ps.ll.getGrid()[i_ll],0.0,ps.yl.getGrid()[i_yl],ps.zl.getGrid()[i_zl]);


                //todo: Change the order here.. could be faster, but now my next outine to read in these relies on the oder so watch out.
                for (int i_pp = 0; i_pp < np; ++i_pp)
                {

//                    p=get_p_vec(ps.Q_Qp_P[i_pp][0], ps.Q_Qp_P[i_pp][1],ps.Q_Qp_P[i_pp][2]);
//                    P.content[3] = sqrt(ps.Q_Qp_P[i_pp][2]);

                    p=get_p_vec(alpha, ps.Q_Qp_P[i_pp][0], ps.Q_Qp_P[i_pp][1],ps.Q_Qp_P[i_pp][2]);
                    P=get_P_vec(alpha, ps.Q_Qp_P[i_pp][0], ps.Q_Qp_P[i_pp][1],ps.Q_Qp_P[i_pp][2]);
//                    cout<<p; cout<<ps.getPhaseSpacePsquared()<<endl;

                    if(which_vertex==1)
                    {
                        //Q' -   V1(r+, Q') - Qfs         //In case of the asymmetric limit Q'²=0.
                        Q= get_Qp(P,p);
                        r= get_rp(P,p,ll);
//                        r=ll+0.25*P+0.25*p;

                    }else if(which_vertex==2)
                    {
                        //-Q -   V2(r-, -Q) - Qis
                        Q= -(1.0)* get_Q(P,p);
                        r=get_rm(P,p,ll);
//                        r=ll+0.25*p-0.25*P;
                        //rm = ll + Sigma /2 - Delta/4 , p= 2*Sigma -> rm = ll + p/4 - P/4

                    }

                    rr=r*r; QQ=Q*Q; Qr=Q*r;
//                    if(real(rr) < -0.046 || real(QQ) < -0.16){cout<<tab<<"V"<<which_vertex<<tab<<"r²= "<<rr<<",   rQ= "<<Qr<<",   Q²= "<<QQ<<"   ,       Q²="<<ps.Q_Qp_P[i_pp][0]<<" , Q'²="<<ps.Q_Qp_P[i_pp][1]<<" , P²="<<ps.Q_Qp_P[i_pp][2]<<endl
//                    if(real(rr) < -0.05909 || real(QQ) < -0.16){cout<<tab<<"V"<<which_vertex<<tab<<"r²= "<<rr<<",   rQ= "<<Qr<<",   Q²= "<<QQ<<"   ,       Q²="<<ps.Q_Qp_P[i_pp][0]<<" , Q'²="<<ps.Q_Qp_P[i_pp][1]<<" , P²="<<ps.Q_Qp_P[i_pp][2]<<endl
//                    if(real(rr) < -0.32847 || real(QQ) < -0.16){cout<<tab<<"V"<<which_vertex<<tab<<"r²= "<<rr<<",   rQ= "<<Qr<<",   Q²= "<<QQ<<"   ,       Q²="<<ps.Q_Qp_P[i_pp][0]<<" , Q'²="<<ps.Q_Qp_P[i_pp][1]<<" , P²="<<ps.Q_Qp_P[i_pp][2]<<endl
//                                                                  <<"  Quark-photon Vertex will be out of bounds!   "<<
//                              ps.ll.getGrid()[i_ll]<<" , "<< ps.yl.getGrid()[i_yl]<<" , "<<ps.zl.getGrid()[i_zl]<<endl;
//                    cout<<"p="<<p; cout<<"P="<<P; cout<<"Q="<<Q; cout<<"r="<<r; cout<<"l="<<ll; //cin.get();//}

//                    if((pow(imag(P.content[2])/2.0,2.0)+pow(imag(P.content[3])/2.0,2.0))> 0.25){

//                    cout<< (pow(imag(r.content[2]),2.0)+pow(imag(r.content[3]),2.0))<<endl;
//                    cout<< tab << (pow(imag(P.content[2])/2.0,2.0)+pow(imag(P.content[3])/2.0,2.0))<<endl;
//                    cout<< tab << (pow(imag(p.content[2])/2.0,2.0)+pow(imag(p.content[3])/2.0,2.0))<<endl;

//                    rr=QQ=Qr=0.0;


//                    }
//                                << "A=" << ( sqrt( pow(real(rr),2.0)/(1.0-0.25*pow(imag(rr),2.0)) ) )<<
//                                    " , B="<< (imag(rr)*0.5 * pow( sqrt( pow(real(rr),2.0)/(1.0-0.25*pow(imag(rr),2.0)) ),-1.0)) <<endl;}
//                    cout<<"Test: "<<QQ<<tab<<ps.Q_Qp_P[i_pp][0]<<tab<<ps.Q_Qp_P[i_pp][1]<<endl;

                    f<<scientific<<tab<<real(rr)<<tab<<imag(rr)<<tab<<real(Qr)<<tab<<imag(Qr)<<tab<<real(QQ)<<endl;
//                    f<<scientific<<tab<<real(pp)<<tab<<imag(pp)<<tab<<real(QQ)<<endl;
                }


            }

        }

    }

    f.close();


}

//ReadInn form Gernots the interpolation (or mine hopefully)
bool qphv_interpol_ReadInn(matCdoub &QPV, int m, int which, string folder)
{

    //Reading inn Parameter form file
    stringstream ss; ss<<which;
    string buffer=folder+"quarkphoton_grid_out_"+ss.str()+".txt";   //D2 has a reason, never take D as a constant ever again!
    double k1, k1c, Q,f1, f1c, f2, f2c, f3, f3c, f4, f4c, f5, f5c, f6, f6c, f7, f7c, f8, f8c, kq,kqc;
    VecDoub d1, d1c, d2, d2c, d3, d3c, d4, d4c, d5, d5c, d6, d6c, d7, d7c, d8, d8c;
    VecCdoub d1v, d2v, d3v, d4v, d5v, d6v, d7v, d8v;

    ifstream infile;
    infile.open(buffer.c_str(), ios::in);           //DSEreal
    if(infile.is_open())
    {
        string daten;
        while(getline(infile,daten))
        {
            //the following line trims white space from the beginning of the string
            daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

            if(daten[0] == '#') {continue;}
            if(daten=="") {continue;}
            stringstream(daten) >> k1 >> k1c >> kq >> kqc >> Q >> f1 >> f1c >> f2 >> f2c >> f3 >> f3c >> f4 >> f4c >> f5 >> f5c >> f6 >> f6c >> f7 >> f7c >> f8 >> f8c;
            if(daten=="") {continue;}

            d1.push_back(f1);
            d1c.push_back(f1c);
            d2.push_back(f2);
            d2c.push_back(f2c);
            d3.push_back(f3);
            d3c.push_back(f3c);
            d4.push_back(f4);
            d4c.push_back(f4c);
            d5.push_back(f5);
            d5c.push_back(f5c);
            d6.push_back(f6);
            d6c.push_back(f6c);
            d7.push_back(f7);
            d7c.push_back(f7c);
            d8.push_back(f8);
            d8c.push_back(f8c);


        }
    }else{cout<<"No data file found! qph-vertex parameter"<<endl; return 0;}

    infile.close();
    int mv=d1.size();

    d1v.resize(mv);d2v.resize(mv);d3v.resize(mv);d4v.resize(mv);
    d5v.resize(mv);d6v.resize(mv);d7v.resize(mv);d8v.resize(mv);

    if(mv==m)
    {
        for(int j=0; j<m; j++)
        {
            d1v[j]=d1[j]+ i_* d1c[j];
            d2v[j]=d2[j]+ i_* d2c[j];
            d3v[j]=d3[j]+ i_* d3c[j];
            d4v[j]=d4[j]+ i_* d4c[j];
            d5v[j]=d5[j]+ i_* d5c[j];
            d6v[j]=d6[j]+ i_* d6c[j];
            d7v[j]=d7[j]+ i_* d7c[j];
            d8v[j]=d8[j]+ i_* d8c[j];


        }

        QPV.push_back(d1v);QPV.push_back(d2v);QPV.push_back(d3v);QPV.push_back(d4v);
        QPV.push_back(d5v);QPV.push_back(d6v);QPV.push_back(d7v);QPV.push_back(d8v);

        cout<<"ReadInn quark-photon form interpolation with: m="<<m<<"  @"<<buffer<<endl;

    }else{cout<<"ffactor_qphv_addon::QPV ReadInn: the dimension do not fit   "<<mv<<tab<<m<<"   @"<<buffer<<endl; return 0;}

    return 1;


}

//Calcuating S3:
vector<array<Cdoub,2>> calculate_S3(TFF_ps& ps, string& folder, QuarkDse& dse, double alpha){

    vector<array<Cdoub,2>> S3;

//    FuncPtrGetMom get_k3_vec = get_k3;

    int n_ll=ps.ll.getNumber_of_grid_points();
    int n_lz= ps.zl.getNumber_of_grid_points();
    int n_ly =  ps.yl.getNumber_of_grid_points();
    int n_pvec =  ps.Q_Qp_P.size();

cout<<"We need to calcaute for n="<<n_pvec<<" p values."<<endl; //cout<<n_ll<<tab<<n_lz<<tab<<n_ly<<endl;
    SuperIndex<4> Si_quark3({n_ll, n_lz, n_ly, n_pvec});

//using parallelization - so it should work? same as in Spm.
    S3.resize(Si_quark3.getNsuper_index());
#pragma omp parallel for schedule (dynamic, 1)  default(shared)
//#pragma omp parallel for default(shared)
    for (int i_s3 = 0; i_s3 < Si_quark3.getNsuper_index(); ++i_s3) {
        array<int, 4> part_i = Si_quark3.are(i_s3);
        fv<Cdoub> ll; ll.set_spherical(ps.ll.getGrid()[part_i[0]],0.0, ps.yl.getGrid()[part_i[2]], ps.zl.getGrid()[part_i[1]]);
        fv<Cdoub> k3 =  ll + 0.5 * get_p_vec(alpha, ps.Q_Qp_P[part_i[3]][0], ps.Q_Qp_P[part_i[3]][1], ps.Q_Qp_P[part_i[3]][2]);

        //alternative version with the function pointer. Out of consistency, but not really needed.
//        fv<Cdoub> p = get_p_vec(alpha, ps.Q_Qp_P[part_i[3]][0], ps.Q_Qp_P[part_i[3]][1], ps.Q_Qp_P[part_i[3]][2]);
//        fv<Cdoub> k3 = get_k3_vec(ll, p);



//        fv<Cdoub> k3 =  ll + 0.5 * get_p_vec(ps.Q_Qp_P[part_i[3]][0], ps.Q_Qp_P[part_i[3]][1], ps.Q_Qp_P[part_i[3]][2]);


        //Check:
//        double imk3=imag(k3.content[0])*imag(k3.content[0])+imag(k3.content[1])*imag(k3.content[1])+imag(k3.content[2])*imag(k3.content[2])+imag(k3.content[3])*imag(k3.content[3]);
//        if(imk3>0.25){cout<<"Quark out of safe region:  "<<k3*k3<<endl;cin.get();}

        array<Cdoub, 2>  value = dse.getValue({k3*k3});
        S3[i_s3] = value;
//        S3.push_back(value);
    }


    //writing it into a file:
    ofstream write;
//    write.open(folder+"Sk3.txt");
//    write.open(folder+"quark_grid_out_3.txt");
    write.open(folder);
    for (int i_s3 = 0; i_s3 < Si_quark3.getNsuper_index(); ++i_s3) {
        array<int, 4> part_i = Si_quark3.are(i_s3);
        fv<Cdoub> ll; ll.set_spherical(ps.ll.getGrid()[part_i[0]],0.0, ps.yl.getGrid()[part_i[2]], ps.zl.getGrid()[part_i[1]]);
        fv<Cdoub> k3 =  ll + 0.5 *  get_p_vec(alpha, ps.Q_Qp_P[part_i[3]][0], ps.Q_Qp_P[part_i[3]][1], ps.Q_Qp_P[part_i[3]][2]);

//        write<< scientific <<setprecision(7) << real(k3*k3)<<tab<<imag(k3*k3)<<tab<<real(S3[i_s3][0])<<tab<<imag(S3[i_s3][0])<<tab<<real(S3[i_s3][1])<<tab<<imag(S3[i_s3][1])<<tab<<endl;
        write<< scientific <<setprecision(7) << real(k3*k3)<<tab<<imag(k3*k3)<<tab<<real(S3[i_s3][0])<<tab<<imag(S3[i_s3][0])<<tab<<real(S3[i_s3][1])<<tab<<imag(S3[i_s3][1])<<tab<<
           ps.ll.getGrid()[part_i[0]] <<tab << ps.yl.getGrid()[part_i[2]]<< tab << ps.zl.getGrid()[part_i[1]] << tab
             << real(ps.Q_Qp_P[part_i[3]][0]) << tab << real(ps.Q_Qp_P[part_i[3]][1]) <<endl;

    }

    write.close();

    return S3;
}

//Calcuating S1 & S2:
vector<array<Cdoub,2>> calculate_Spm(TFF_ps& ps, string& folder, QuarkDse& dse, double alpha, FuncPtrGetMom get_k_vec, string tag){

    vector<array<Cdoub,2>> S3;

    int n_ll=ps.ll.getNumber_of_grid_points();
    int n_lz= ps.zl.getNumber_of_grid_points();
    int n_ly =  ps.yl.getNumber_of_grid_points();
    int n_pvec =  ps.Q_Qp_P.size();

    cout<<"calculating Spm "<<endl;

    SuperIndex<4> Si_quark3({n_ll, n_lz, n_ly, n_pvec});
    SuperIndex<2> Si_quarkpm({n_ll, n_lz});

    int n;

    if(alpha < 1e+10){

        //implemented parallelization -works?
        S3.resize(Si_quark3.getNsuper_index());
        n=Si_quark3.getNsuper_index();

#pragma omp parallel for default(shared)
        for (int i_s3 = 0; i_s3 < n; ++i_s3) {
            array<int, 4> part_i = Si_quark3.are(i_s3);
            fv<Cdoub> ll; ll.set_spherical(ps.ll.getGrid()[part_i[0]],0.0, ps.yl.getGrid()[part_i[2]], ps.zl.getGrid()[part_i[1]]);
            fv<Cdoub> P = get_P_vec(alpha, ps.Q_Qp_P[part_i[3]][0], ps.Q_Qp_P[part_i[3]][1], ps.Q_Qp_P[part_i[3]][2]); //new
            fv<Cdoub> ki = get_k_vec(ll, P);

            //Check:
//        double imk3=imag(k3.content[0])*imag(k3.content[0])+imag(k3.content[1])*imag(k3.content[1])+imag(k3.content[2])*imag(k3.content[2])+imag(k3.content[3])*imag(k3.content[3]);
//        if(imk3>0.25){cout<<"Quark out of safe region:  "<<k3*k3<<endl;cin.get();}


            array<Cdoub, 2>  value = dse.getValue({ki*ki});
            S3[i_s3] = value;
        }

        //writing it into a file:
        ofstream write;
        write.open(folder+"Spm_"+tag+".txt");
        for (int i_s3 = 0; i_s3 < n; ++i_s3) {
            array<int, 4> part_i = Si_quark3.are(i_s3);
            fv<Cdoub> ll; ll.set_spherical(ps.ll.getGrid()[part_i[0]],0.0, ps.yl.getGrid()[part_i[2]], ps.zl.getGrid()[part_i[1]]);
            fv<Cdoub> P = get_P_vec(alpha, ps.Q_Qp_P[part_i[3]][0], ps.Q_Qp_P[part_i[3]][1], ps.Q_Qp_P[part_i[3]][2]); //new
            fv<Cdoub> k3 = get_k_vec(ll, P);



            write<< scientific <<setprecision(7) << real(k3*k3)<<tab<<imag(k3*k3)<<tab<<real(S3[i_s3][0])<<tab<<imag(S3[i_s3][0])<<tab<<real(S3[i_s3][1])<<tab<<imag(S3[i_s3][1])<<tab<<
                         ps.ll.getGrid()[part_i[0]] <<tab << ps.yl.getGrid()[part_i[2]]<< tab << ps.zl.getGrid()[part_i[1]] << tab
                          << real(ps.Q_Qp_P[part_i[3]][0]) << tab << real(ps.Q_Qp_P[part_i[3]][1]) <<endl;
        }

        write.close();


    }else{

        S3.resize(Si_quarkpm.getNsuper_index());
        n=Si_quarkpm.getNsuper_index();

#pragma omp parallel for default(shared)
        for (int i_s3 = 0; i_s3 < n; ++i_s3) {
            array<int, 2> part_i = Si_quarkpm.are(i_s3);
            // Here it doesn't depend on y anymore, so the y dependece is set to 1.0.
            fv<Cdoub> ll; ll.set_spherical(ps.ll.getGrid()[part_i[0]],0.0, 1.0, ps.zl.getGrid()[part_i[1]]);
            fv<Cdoub> P = get_P_vec( ps.Q_Qp_P[0][2]);
            fv<Cdoub> ki = get_k_vec(ll, P);


            array<Cdoub, 2>  value = dse.getValue({ki*ki});
            S3[i_s3] = value;
        }

        //writing it into a file:
        ofstream write;
        write.open(folder+"Spm_"+tag+".txt");
        for (int i_s3 = 0; i_s3 < n; ++i_s3) {
            array<int, 2> part_i = Si_quarkpm.are(i_s3);
            fv<Cdoub> ll; ll.set_spherical(ps.ll.getGrid()[part_i[0]],0.0, 1.0, ps.zl.getGrid()[part_i[1]]);
            fv<Cdoub> P = get_P_vec( ps.Q_Qp_P[0][2]);
            fv<Cdoub> k3 = get_k_vec(ll, P);



            write<< scientific <<setprecision(7) << real(k3*k3)<<tab<<imag(k3*k3)<<tab<<real(S3[i_s3][0])<<tab<<imag(S3[i_s3][0])<<tab<<real(S3[i_s3][1])<<tab<<imag(S3[i_s3][1])<<tab<<
                 endl;
//                          ps.ll.getGrid()[part_i[0]] <<tab << ps.yl.getGrid()[part_i[2]]<< tab << ps.zl.getGrid()[part_i[1]] << tab
//                          << real(ps.Q_Qp_P[part_i[3]][0]) << tab << real(ps.Q_Qp_P[part_i[3]][1]) <<endl;
        }

        write.close();


    }





    return S3;
}

//writing the Sk1 Sk2 Sk3 grid for using a interpolation for the complex quarks:
void write_quark_grid_for_interpol(string& folder, TFF_ps& ps, double alpha, int which_grid){


    int n_ll=ps.ll.getNumber_of_grid_points();
    int n_lz= ps.zl.getNumber_of_grid_points();
    int n_ly =  ps.yl.getNumber_of_grid_points();
    int n_pvec =  ps.Q_Qp_P.size();

    SuperIndex<4> Si_quark3({n_ll, n_lz, n_ly, n_pvec});
    SuperIndex<2> Si_quarkpm({n_ll, n_lz});

    if(which_grid==3){

        //writing S(k3)  into a file:
        ofstream write3;
        write3.open(folder+"quark_grid_3.txt");


        write3<<"# Re(p²)    Im(p²)"<<endl;
        write3<<Si_quark3.getNsuper_index()<<endl;

        for (int i_s3 = 0; i_s3 < Si_quark3.getNsuper_index(); ++i_s3) {

            array<int, 4> part_i = Si_quark3.are(i_s3);

            fv<Cdoub> ll; ll.set_spherical(ps.ll.getGrid()[part_i[0]],0.0, ps.yl.getGrid()[part_i[2]], ps.zl.getGrid()[part_i[1]]);
            fv<Cdoub> P = get_P_vec(alpha, ps.Q_Qp_P[part_i[3]][0], ps.Q_Qp_P[part_i[3]][1], ps.Q_Qp_P[part_i[3]][2]);
            fv<Cdoub> p = get_p_vec(alpha, ps.Q_Qp_P[part_i[3]][0], ps.Q_Qp_P[part_i[3]][1], ps.Q_Qp_P[part_i[3]][2]);
            fv<Cdoub> k3 =  get_k3(ll,p);

//        write<< scientific <<setprecision(7) << real(k3*k3)<<tab<<imag(k3*k3)<<tab<<real(S3[i_s3][0])<<tab<<imag(S3[i_s3][0])<<tab<<real(S3[i_s3][1])<<tab<<imag(S3[i_s3][1])<<tab<<endl;
//        write1<< scientific <<setprecision(7) << real(k1*k1)<<tab<<imag(k1*k1)<<tab<<endl;
//        write2<< scientific <<setprecision(7) << real(k2*k2)<<tab<<imag(k2*k2)<<tab<<endl;
            write3<< scientific <<setprecision(7) << real(k3*k3)<<tab<<imag(k3*k3)<<tab<<endl;

        }

        write3.close();

    }

    if(which_grid ==1 || which_grid ==2 ){

        //writing S(k2),S(k1)
        ofstream write1, write2;
        write1.open(folder+"quark_grid_1.txt");
        write2.open(folder+"quark_grid_2.txt");

        write1<<"# Re(p²)    Im(p²)"<<endl; write2<<"# Re(p²)    Im(p²)"<<endl;

        if(alpha < 1e+10){

            write1<<Si_quark3.getNsuper_index()<<endl; write2<<Si_quark3.getNsuper_index()<<endl;

            for (int i_s3 = 0; i_s3 < Si_quark3.getNsuper_index(); ++i_s3) {

                array<int, 4> part_i = Si_quark3.are(i_s3);

                fv<Cdoub> ll; ll.set_spherical(ps.ll.getGrid()[part_i[0]],0.0, ps.yl.getGrid()[part_i[2]], ps.zl.getGrid()[part_i[1]]);
                fv<Cdoub> P = get_P_vec(alpha, ps.Q_Qp_P[part_i[3]][0], ps.Q_Qp_P[part_i[3]][1], ps.Q_Qp_P[part_i[3]][2]);
                fv<Cdoub> k1 = get_kp(ll, P);
                fv<Cdoub> k2 = get_km(ll, P);

//        write<< scientific <<setprecision(7) << real(k3*k3)<<tab<<imag(k3*k3)<<tab<<real(S3[i_s3][0])<<tab<<imag(S3[i_s3][0])<<tab<<real(S3[i_s3][1])<<tab<<imag(S3[i_s3][1])<<tab<<endl;
                write1<< scientific <<setprecision(7) << real(k1*k1)<<tab<<imag(k1*k1)<<tab<<endl;
                write2<< scientific <<setprecision(7) << real(k2*k2)<<tab<<imag(k2*k2)<<tab<<endl;

            }
        }else{

            write1<<Si_quarkpm.getNsuper_index()<<endl; write2<<Si_quarkpm.getNsuper_index()<<endl;
            for (int i_s3 = 0; i_s3 < Si_quarkpm.getNsuper_index(); ++i_s3) {

                array<int, 2> part_i = Si_quarkpm.are(i_s3);

                fv<Cdoub> ll; ll.set_spherical(ps.ll.getGrid()[part_i[0]],0.0, 1.0, ps.zl.getGrid()[part_i[1]]);
                //comment: Here we need to pic a value for P, to calculate k+ k-.
                // The current set-up wouldnt work if there are multiple P values anymore
//            fv<Cdoub> P = get_P_vec(alpha, ps.Q_Qp_P[0][0], ps.Q_Qp_P[0][1], ps.Q_Qp_P[0][2]);
                fv<Cdoub> P = get_P_vec(ps.Q_Qp_P[0][2]);
                fv<Cdoub> k1 = get_kp(ll, P);
                fv<Cdoub> k2 = get_km(ll, P);

//        write<< scientific <<setprecision(7) << real(k3*k3)<<tab<<imag(k3*k3)<<tab<<real(S3[i_s3][0])<<tab<<imag(S3[i_s3][0])<<tab<<real(S3[i_s3][1])<<tab<<imag(S3[i_s3][1])<<tab<<endl;
                write1<< scientific <<setprecision(7) << real(k1*k1)<<tab<<imag(k1*k1)<<tab<<endl;
                write2<< scientific <<setprecision(7) << real(k2*k2)<<tab<<imag(k2*k2)<<tab<<endl;

            }
        }

        write1.close();  write2.close();

    }

}



//This is for all the vector and kinematic routing used in triangle diagram - TFF!
//more can be found in the tff_addons.h file, eg. the general set-up with the alpha paramter and Q^2 and Q'^2.
//fourvector defintion of kp and km for a general use throughout the formfactor main:
fv<Cdoub> get_kp(fv<Cdoub>& ll, fv<Cdoub>& P){
    return (ll + 0.5*P);
}
fv<Cdoub> get_km(fv<Cdoub>& ll, fv<Cdoub>& P){
    return (ll - 0.5*P);
}
fv<Cdoub> get_k3(fv<Cdoub>& ll, fv<Cdoub>& p){
    return (ll + 0.5*p);
}
fv<Cdoub> get_rp(fv<Cdoub>& P, fv<Cdoub>& p, fv<Cdoub>& ll){
    return (ll + 0.25*P + 0.25*p);
}
fv<Cdoub> get_rm(fv<Cdoub>& P, fv<Cdoub>& p, fv<Cdoub>& ll){
    return (ll - 0.25*P + 0.25*p);
}

//Reading S3 and other quarks Spm: this second routine is the one currently used for all quarks in trinagle.
vector<array<Cdoub,2>> read_complex_Q(string filename){

    vector<array<Cdoub,2>> S3;

//    cout<<filename<<endl;
    double mass_here;

    ifstream read;
    read.open(filename, ios::in);



    if(read.is_open())
    {
        string daten;
        while(getline(read,daten))
        {

            VecDoub dummy; string s = ""; int pos;
            //Here reading in the mass from the output file.
            if(daten[0] == '#') {
                if(daten[1] == '#')
                {
                    pos= daten.find("\t");
                    for (int j = pos; j < daten.size(); ++j) {
                        s +=daten[j]; }
                    mass_here = stod(s); /// updates the mass in phase space.cpp
                }else{continue;}
            }

            if(daten=="") {continue;}

            //Reading in the amplitude: here I am subtituting 2 because I adddes Psquared and the z at the end of the file.
            for (int j = 0; j <daten.size()  ; ++j) {
                s += daten[j];
                if(daten[j] == '\t' || daten[j] =='  ')
                {
                    dummy.push_back(stod(s));
                    s = "";
                }
//                if( c <= 'a'  ){}//Check if all of the read in data are numbers.
            }


            //Here I use the fact that I know exactly how the file looks like with 2 dressing functions.
            S3.push_back({dummy[2]+i_*dummy[3],dummy[4]+i_*dummy[5] });

//            for (int k = 1; k < (dummy.size()/2) ; ++k) {
//                safe.push_back(dummy[2*k]+i_*dummy[(2*k+1)]);
//            }

            if(daten=="") {continue;}
            if(daten[0] == '#') {continue;}



        }
    }else{cout<<"S3 ReadInn::readIn: No data file found!"<<endl; assert( false);}
    read.close();
//    cout<<mass_here<<endl;

    cout<<"quark 3 has been read inn:"<<filename<<"  with: n_total="<<S3.size()<<endl;

    return S3;

}

vector<array<Cdoub,2>> read_complex_Q_new(string filename){

    vector<array<Cdoub,2>> S3;

    double k1, k1c, Q,f1, f1c, f2, f2c;
    Cdoub d1,d2;
    VecCdoub d1v, d2v, d3v, d4v, d5v, d6v, d7v, d8v;


    ifstream infile;
    infile.open(filename.c_str(), ios::in);           //DSEreal
    if(infile.is_open())
    {
        string daten;
        while(getline(infile,daten))
        {
            //the following line trims white space from the beginning of the string
            daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

            if(daten[0] == '#') {continue;}
            if(daten=="") {continue;}
            stringstream(daten) >> k1 >> k1c >> f1 >> f1c >> f2 >> f2c ;
            if(daten=="") {continue;}

            d1 = f1 + i_*f1c;
            d2 = f2 + i_*f2c;

            S3.push_back({d1,d2});


        }
    }else{cout<<"S3 ReadInn::readIn: No data file found!"<<endl; assert( false);}
    infile.close();

    cout<<"A Cquark has been read in:"<<filename<<"  with: n_total="<<S3.size()<<endl;

    return S3;

}



//Q², Q'² grid:
//Write out the points in Q² and Q'² where this should be calculated: -the normal version can be found in tff_addons
//This version of settign the grid futhermore takes the limitation of the vertex in, and limits the values of choice to a set that won't complain int he vertex.
void create_Qs_Qps_grid_interpol_boundaries(VecCdoub& Q, VecCdoub& Qp, Cdoub& Ps, double alpha, string& which_interpolation_V1, string& which_interpolation_V2,
                                            TFF_ps& ps, int which_case, double Qps_value_outside, int n_here, string& which_TFF)
{

    // Q' -   V1(r+, Q') - Qfs    =====    //-Q -   V2(r-, -Q) - Qis

    //-----------------------------------------------------------------------------------------------------------------
    //variables needed here: the maximun complex quark appex
    double mps=1.0/4.0;

    //Limitations of Fortran interpoaltion:
    //this has to be read off in Gernots Fortran interpoaltion file : in small Q² folder: __iBSE_QPV_complex_transverse.dat
    // relative momentum !< value:
    // sl - associated with min_in_Qs_2  & sQ - assosciated with min_in_Qs
    double max_value_relative_q_inter =  -0.3284747; // 30 MT paramters.
    double max_value_relative_q_inter_sQ =  -0.05908602; //both folder 30 MT paramters.
#ifdef USE_OLDER_DATA_SET
    double max_value_of_interpolation_old =  -0.04697545; //Watch out the old set works on folder 28 MT paramter. -OLD!!! Changed to data-set below
#endif
#ifndef USE_OLDER_DATA_SET
    double max_value_of_interpolation_old =  -0.3077353; //Watch out the old set works on folder 28 MT paramter.
#endif

    //Q² > value;
    double min_in_Qs_sl= 0.04;
    double min_in_Qs_sQ= -0.16; //important: Don't use this set-up as long as it is not matched in the interpolation routine!!
    double max_in_Qs_old = 1000000.0;
#ifdef USE_OLDER_DATA_SET
    double min_in_Qs_old= -0.16; //old file found in my folders. Change to Gernots data from Everything.zip
#endif
#ifndef USE_OLDER_DATA_SET
    double min_in_Qs_old= 0.04;
#endif
    //in the smallQ² code. In the spacelike interpolation Qmin²=0.04 and Qmax²=1.0e+6
    //-----------------------------------------------------------------------------------------------------------------


    //values of Q and Q':
    VecCdoub Qs_value, Qps_value; //Qps_value could come from outside.

    //epsilon for how far form the symmetric limit. - not really used anymore - get rid of this.
    double epsilon = 1.0e-1; //double epsilon=5.0e-2; //double epsilon2=1.0e-1; epsilon2=epsilon;

    //n's: numbers of grid points for later.
    int nll=ps.ll.getNumber_of_grid_points(); int nz=ps.zl.getNumber_of_grid_points();
    int ny=ps.yl.getNumber_of_grid_points();

    double min_in_Qs ,min_in_Qps;
    double min_value_qs, min_value_qps;

#ifdef  USE_MT_PARA_FOLDER_30
//        which_interpolation_V1= "sl";
//        which_interpolation_V2= "sl";

        which_interpolation_V1= "sQ";
        which_interpolation_V2= "sQ";
#endif

#ifdef USE_MT_PARA_FOLDER_28
    which_interpolation_V1= "old";
    which_interpolation_V2= "old";
#endif

    //which_case:
    //(0) : anomaly point, (1) symmetric limit , (2) asymmetric limit, (3) smaller asymmetric, (4) different combinations
    if(which_case==0){
        //(0)
        //For the anomaly point F(0,0)
        //values_from_outside=false;

        cout<<"Calculating the anomaly point"<<endl;
        Qs_value.push_back(1e-6);
        Qps_value.push_back(1e-6);

//        Qs_value.push_back(3.4247e-3);
//        Qps_value.push_back(3.4247e-3);

        n_here=1;

        which_interpolation_V1= "sQ";
        which_interpolation_V2= "sQ";


    }else if(which_case == 2)
    {
        // Grid in Q² and Q'²: In asymmetric case - (nope: small Q' values coming form outside!) But can change this again.
        //values_from_outside=false; at the moment

        cout<<"Calculating in the asymmetric limit"<<endl; //. Starting at Q²=0.2"<<endl;
        Line Qline(0.0, 2.5, n_here, "linear", "leg", false, false);
        for (int i = 0; i <n_here; ++i) {
            Qs_value.push_back(Qline.getGrid()[i]);
            Qps_value.push_back(0.0);
        }

        //Choosign the interpolator for the Vertices: (V1-Qp, V2-Q)
//        which_interpolation_V1= "sQ"; //Q'² = 0
//        //important:!!!!!!!!!!!!! changed sQ.
//        which_interpolation_V2= "sl";

//        Line etaline(1e-6,1.2, n_here, "log", "leg", false, false);
//        Cdoub omega;
////        for (int j = 0; j < n_here; ++j) {
//            for (int i = 0; i <n_here; ++i) {
//                omega= etaline.getGrid()[i];
//                Qs_value.push_back(etaline.getGrid()[i]+ omega);
//                Qps_value.push_back(etaline.getGrid()[i]- omega);
//            }
//        }


    }else if(which_case == 7)
    {
        //in the symmetric limit but setting it up with omega and eta_+:
        //values_from_outside=false;

        cout<<"Calculating in the (almost) symmetric limit"<<endl;

        Line etaline(1e-6, 1e+4, n_here, "log", "leg", false, false);
        VecDoub R = {0.05,0.5,0.95}; Cdoub omega;
        for (int j = 0; j < R.size(); ++j) {
            for (int i = 0; i <n_here; ++i) {
                omega= (1.0/4.0)* sqrt(-Ps) *0.99*R[j];
                Qs_value.push_back(etaline.getGrid()[i]+ omega);
                Qps_value.push_back(etaline.getGrid()[i]- omega);
            }
        }


    }else if(which_case==1){

        //in the symmetric limit
        //values_from_outside=false;
        double which_procentage_for_Qp=0.2;

        cout<<"Calculating in the (almost) symmetric limit"<<endl;

        Line Qline(1e-6, 1e+4, n_here, "log", "leg", false, false);
//        Line Qline(0.00001, 1000000, n_here, "log", "leg", false, false);
        for (int i = 0; i <n_here; ++i) {
            Qs_value.push_back(Qline.getGrid()[i]);

            if(which_TFF == "ps"){Qps_value.push_back(Qline.getGrid()[i]);}
            else{ Qps_value.push_back(Qline.getGrid()[i]+(0.005)*Qline.getGrid()[i]*pow(-1.0,i)); }
//                if(Qline.getGrid()[i] > 1.0){
//                    Qps_value.push_back(Qline.getGrid()[i]+(which_procentage_for_Qp*0.5)*Qline.getGrid()[i]*pow(-1.0,i));}
//                else{
//                    Qps_value.push_back(Qline.getGrid()[i]+(which_procentage_for_Qp)*Qline.getGrid()[i]*pow(-1.0,i));}
//                }
        }



    }else if(which_case==3){

        //asymmetric for very small values:
        //values_from_outside=false; at the moment

        cout<<"Calculating in the small asymmetric limit"<<endl;
        Line Qline(0.0001, 0.21, n_here, "linear", "leg", false, false);
        for (int i = 0; i <n_here; ++i) {
            Qs_value.push_back(Qline.getGrid()[i]);
            Qps_value.push_back(0.0);
        }

        which_interpolation_V1= "sQ"; //Q'² almost 0
        which_interpolation_V2= "sl";


    }else if(which_case==4){

        //between symmetric and asymmetric - activate values from outside: using_outside_qps=true!
        //values_from_outside=true;

        cout<<"Calculating in between symmetric and asymmetric"<<endl;
//        Line Qline(0.001, Qps_value_outside*(1.0-epsilon), n_here, "linear", "leg", false, false);
        Line Qline(0.0, 1.0, n_here, "linear", "leg", false, false);
        for (int i = 0; i <n_here; ++i) {
            Qs_value.push_back(Qline.getGrid()[i]);
            Qps_value.push_back(Qps_value_outside);
        }

        which_interpolation_V1= "sl"; //Q'² almost 0
        which_interpolation_V2= "sl";


    }else if(which_case == 5){
        //See below..
    }else if(which_case == 6){
        cout<<"Calculating for random point"<<endl;
        Qs_value.push_back(0.95);
        Qps_value.push_back(1.05);
        n_here=1;

        which_interpolation_V1= "sQ";
        which_interpolation_V2= "sQ";

    }
    else{assert(false);}

    //------------------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    if(which_case!=5){

        //Checkign which vertex criteria apply.

        if(which_interpolation_V1=="sQ"){min_in_Qps = min_in_Qs_sQ;}else if(which_interpolation_V1=="sl"){min_in_Qps= min_in_Qs_sl;}
        else if(which_interpolation_V1=="old"){min_in_Qps= min_in_Qs_old;}
        if(which_interpolation_V1=="sQ"){min_value_qps = max_value_relative_q_inter_sQ;}else if(which_interpolation_V1=="sl"){min_value_qps= max_value_relative_q_inter;}
        else if(which_interpolation_V1=="old"){min_value_qps= max_value_of_interpolation_old;}

        if(which_interpolation_V2=="sQ"){min_in_Qs = min_in_Qs_sQ;}else if(which_interpolation_V2=="sl"){min_in_Qs= min_in_Qs_sl;}else if(which_interpolation_V2=="sl"){min_in_Qs= min_in_Qs_sl;}
        if(which_interpolation_V2=="sQ"){min_value_qs = max_value_relative_q_inter_sQ;}else if(which_interpolation_V2=="old"){min_value_qs= max_value_of_interpolation_old;}
        else if(which_interpolation_V2=="old"){min_value_qs= max_value_of_interpolation_old;}



        //Now we only selected the points that fullfill our Criteria to not hit poles and work with the vertex.

        cout<<"Crit:"<<tab<<"1"<<tab<<"2"<<tab<<"3"<<tab<<"4"<<tab<<"5"<<tab<<"6"<<tab<<"7"<<tab<<"8"<<tab<<"9"<<tab<<"10"<<endl<<
            "-------------------------------------------------------------------"<<endl;
        for (int i = 0; i < n_here; ++i) {



            Cdoub Qs =  Qs_value[i];
            Cdoub Qps = Qps_value[i];

            fv<Cdoub> p = get_p_vec( alpha, Qs,  Qps,  Ps);
            fv<Cdoub> P = get_P_vec( alpha, Qs,  Qps,  Ps);

        //------------CRITERIA: --------------------------------------------
        //Crit1 & Crit2 make sure to not hit the quark poles.
        bool Crit1 = pow(imag(p.content[2])*0.5,2.0)+pow(imag(p.content[3])*0.5,2.0) < mps; //where mps is the appex of the cquark.
        bool Crit2 = pow(imag(P.content[2])*0.5,2.0)+pow(imag(P.content[3])*0.5,2.0) < mps;
        //Crit3 & Crit4: are all to make sure we do not directly hit the symmetric limit as in principle the basis is not defined in this region.
        bool Crit3 = Qs != Qps;  //if(which_case ==1){Crit3 =true;} //symmetric limit.
        bool Crit4 = (real(Qs) <= real(Qps*(1.0-epsilon))) || (real(Qs) >= real(Qps*(1.0+epsilon))  || real(Qps) <= real(Qs*(1.0-epsilon))) || (real(Qps) >= real(Qs*(1.0+epsilon)) );
        if( which_TFF == "ps"){Crit4=true; Crit3=true;}
        //Crit5 & Crit6 check if the realtive momentum is in the boundaries of the interpolation
        bool Crit5 = true; int counterp=0;
        bool Crit6 = true; int counterm=0;
        //total momenta of Qphv can not be smaller than interpolation value.
        bool Crit7 = (real(Qs) > min_in_Qs);
        bool Crit8 = (real(Qps) > min_in_Qps);
        //addtional Criteria here to make sure not to be to close to 0: (not used recently)
//        bool Crit9 = (real(Qs) < -epsilon) || (real(Qs) > epsilon );
//        bool Crit10 = (real(Qps) < -epsilon) || (real(Qps) > epsilon );

            for (int i_ll = 0; i_ll < nll; ++i_ll) {
                for (int i_zl = 0; i_zl < nz; ++i_zl) {
                    for (int i_yl = 0; i_yl < ny; ++i_yl) {

                        fv<Cdoub> ll;
                        ll.set_spherical(ps.ll.getGrid()[i_ll],0.0,ps.yl.getGrid()[i_yl],ps.zl.getGrid()[i_zl]);


                        fv<Cdoub> rp = ll+0.25*P+0.25*p;
                        fv<Cdoub> rm = ll+0.25*P-0.25*p;

                        //additional checks for the relativ momenta- for real Q²>0, r > rmin = max_value_Q, for real Q²<0 or imaginary r>0:
                        if(real(Qs) < 0 || imag(Qs)!=0 ){
                            if(real((rm*rm)) <0.0 ){counterm++;}
                        }else{
                            if(real((rm*rm)) < min_value_qs || real((rm*rm)) < -mps ){counterm++;}
                        }

                        if(real(Qps) < 0 || imag(Qps)!=0 ){
                            if(real((rp*rp)) < 0.0 ){counterp++;}
                        }else{
                            if(real((rp*rp)) < min_value_qps || real((rp*rp)) < -mps){counterp++;}
                        }

//                        if(real((rp*rp)) < min_value_qps){counterp++;}
//                        if(real((rm*rm)) < min_value_qs){counterm++;}


                    }}}

            if(counterp>0){Crit5=false;}
            if(counterm>0){Crit6=false;}

//Different criteria when vertex is included or not.
            cout<<"Crit:"<<tab<<Crit1<<tab<<Crit2<<tab<<Crit3<<tab<<Crit4<<tab<<Crit5<<tab<<Crit6<<tab<<Crit7<<tab<<Crit8<<endl; //tab<<Crit9<<tab<<Crit10<<endl;
#ifdef K_WITH_FULL_QPHV
//            if(Crit1 && Crit2 && Crit6 && Crit7 && Crit8 && Crit5 && Crit3 && Crit4){

//            if(Crit1 && Crit2 && Crit6 && Crit7 && Crit8 && Crit5 && Crit3){
//            if(Crit1 && Crit2 && Crit7 && Crit8 && Crit5 && Crit3){
            if(Crit1 && Crit2 && Crit7 && Crit8 && Crit3){

            //after talked to Gernot, execluded the arguments that one can not calculate above the Q² value. Rutine extrapolates
//            if(Crit1 && Crit2 && Crit6  && Crit5 && Crit3){ //or not!!! This causes trouble with the vertex as it does nothign at this point.
                Q.push_back(Qs_value[i]);
                Qp.push_back(Qps_value[i]);
            }
#endif

#ifdef K_WITH_BARE_VERTEX
            if(  Crit1 && Crit2  && Crit3){ //when using no interpolation
                Q.push_back(Qs_value[i]);
                Qp.push_back(Qps_value[i]);
            }
#ifdef K_USING_QUARK_MODEL
//            else if(Crit3){
//                Q.push_back(Qs_value[i]);
//                Qp.push_back(Qps_value[i]);
//            }
#endif
#endif

#ifdef K_WITH_BC_VERTEX
            if(  Crit1 && Crit2  && Crit3 ){ //when using no interpolation
                Q.push_back(Qs_value[i]);
                Qp.push_back(Qps_value[i]);
            }
#endif


        }

        if(Q.size() ==0){cout<<"NO Q² POINTS FOUND IN THE SPECIFIED REGION ------ Q'²="<<Qps_value_outside<<endl;}//assert(false);}


    }

    //-----------------------------------------------------------------------------------------------------------------


    //important: DEBUGGING HERE:
    if(which_case == 5){


        int n_sigma=40;
        Line sigma_line(1.e-4,1.e+4,n_sigma,"log", "leg", false, false);
//        int n_sigma=1;
//        Line sigma_line(0.0,0.0,n_sigma,"linear", "leg", false, false);

        double z_here=0.5;
        Cdoub P_here= - (0.685*0.685);

        for (int i = 0; i < n_sigma ; ++i) {

            //important: DEBUG---
            double sigma_here = sigma_line.getGrid()[i];
            fv<Cdoub> p = get_p_vec(z_here, sigma_here);
//            cout << "p=( " << p.content[0] << " , " << p.content[1] << " , " << p.content[2] << " , " << p.content[3] << endl;
            fv<Cdoub> P = get_P_vec(P_here);


            fv<Cdoub> Qfv, Qpfv;
            Qfv = get_Q(P,p);
            Qpfv = get_Qp(P,p);

            Q.push_back((Qfv*Qfv));
            Qp.push_back((Qpfv*Qpfv));
        }

    }

}


void interpolate_this_bse_data(string filename, TFF_ps& ps, int& rad_points_in_readInn_file, bool& is_cheb_file){

    //Read Inn the meson data: One needs to know how much the amount of radial points is.

    VecCdoub amp_new;

    double k1, k1c, Q,f1, f1c, f2, f2c, f3, f3c, f4, f4c, fP, fz;
    VecCdoub amps;
    VecDoub rad_value, ang_value;
    matDoub x_values;
    double mass_here;

    int index_i=0;

    //Reading in a previous data file to perform an interpolation on this.
    ifstream infile;
    string filename_inn = filename+"_precalc.txt";
    infile.open(filename_inn.c_str(), ios::in);
    if(infile.is_open())
    {
        string daten;
        while(getline(infile,daten))
        {
            //the following line trims white space from the beginning of the string
            daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

            VecDoub dummy; string s = ""; int pos;
            //Here reading in the mass from the output file.
            if(daten[0] == '#') {
                if(daten[1] == '#')
                {
                    pos= daten.find("\t");
                    for (int j = pos; j < daten.size(); ++j) {
                        s +=daten[j]; }
                    mass_here = stod(s); /// updates the mass in phase space.cpp
                }else{continue;}
            }else {


                if (daten == "") { continue; }
                stringstream(daten) >> k1 >> k1c >> f1 >> f1c >> f2 >> f2c >> f3 >> f3c >> f4 >> f4c
                                    >> fP >> fz;
                if (daten == "") { continue; }

                amps.push_back(f1 + i_ * f1c);
                amps.push_back(f2 + i_ * f2c);
                amps.push_back(f3 + i_ * f3c);
                amps.push_back(f4 + i_ * f4c);

                index_i++;
                rad_value.push_back(k1); // Do I need to do log here????//important
                if ((index_i % rad_points_in_readInn_file) == 0) { ang_value.push_back(fz); }


            }

        }
    }else{cout<<"Meson Interpolation ReadInn::readIn: No data file found! here: "<<filename_inn<<endl; assert( false);}
    infile.close();

    cout<<"A meson has been read inn:"<<filename_inn<<"  with: n_total="<<amps.size()<<endl;

    SplineInterpolator<2, N_SCALAR_B, double, Cdoub> Interpol;

    //if there is a cheby dependence we need to expand into the angular dependece first
    if(is_cheb_file){

        amps = cheby_expand_bse_amp(ps, rad_points_in_readInn_file, ang_value, amps);


      /*  SuperIndex<3> Si_I_file({N_SCALAR_B,rad_points_in_readInn_file, ps.chebs.getNumber_of_grid_points()});
        SuperIndex<3> Si_I_wanted({N_SCALAR_B,rad_points_in_readInn_file, ps.zl.getNumber_of_grid_points()});
        VecCdoub amp_cheb(Si_I_wanted.getNsuper_index(), 0.0);

        ang_value = ps.zl.getGrid();

        cheb Cheby( ps.chebs.getNumber_of_grid_points());

        for (int i_ang = 0; i_ang < ang_value.size(); ++i_ang) {

            for (int i_rad = 0; i_rad < rad_points_in_readInn_file; ++i_rad) {

                for (int j = 0; j < N_SCALAR_B; ++j) {

                    for (int i_cheb = 0; i_cheb <  ps.chebs.getNumber_of_grid_points(); ++i_cheb) {
                        amp_cheb[Si_I_wanted.is({j, i_rad, i_ang})] +=   Cheby.get(i_cheb,ps.zl.getGrid()[i_cheb]) * amps[Si_I_file.is({j, i_rad, i_cheb})];
                    }

                }


            }

        }

        amps=amp_cheb;*/

    }


   /* ///TESSSSTtttt

    SuperIndex<3> Si_I_2({N_SCALAR_B,rad_points_in_readInn_file, ps.zl.getNumber_of_grid_points()});

    string filename_out_2=filename+"_TEST.txt";
    ofstream write_2(filename_out_2);
    assert( write_2.is_open());

    write_2<<"# safed as: p2   ti (real/imag) i="<<N_SCALAR_B<<endl;
    write_2<<"# #gridpoints: rad= "<<rad_points_in_readInn_file<<", ang= "<<ps.zl.getNumber_of_grid_points()<<endl;
    write_2<<"## mass="<<tab<<mass_here<<endl;

    for (int i_ang = 0; i_ang < ps.zl.getNumber_of_grid_points(); ++i_ang) {

        for (int i_rad = 0; i_rad < rad_points_in_readInn_file; ++i_rad) {

            write_2<<scientific<<setprecision(8)<< rad_value[i_rad]<<tab<<0.0
                 << tab ;

            for (int j = 0; j < N_SCALAR_B; ++j) {

                Cdoub value = amps[Si_I_2.is({j, i_rad, i_ang})];
                write_2 <<scientific<<setprecision(10)<< real( value ) << tab << imag (value) <<tab;
            }

            write_2<<scientific<<setprecision(5)<< ps.zl.getGrid()[i_ang] << tab;
            write_2<<scientific<<setprecision(7)<< 0.0 << tab << endl;

        }

        write_2<<endl<<endl;

    }

    write_2<<endl<<endl<<endl;
    write_2.close();

    ///////////////////////////////////*/

    rad_value.resize(rad_points_in_readInn_file);
    x_values.push_back(rad_value); x_values.push_back(ang_value);

    //This function interpolate the values of amplitude on the wanted grid points x' safed in xvalues_interpol.
    //The corresponding x-values for f(x) are already set in interpolation_start() above.
    Interpol.Update( x_values, amps);

    array<Cdoub, N_SCALAR_B> result_y;
    int new_numer_of_rad_points= ps.ll.getNumber_of_grid_points();
    int new_numer_of_ang_points=ps.zl.getNumber_of_grid_points();

    SuperIndex<3> Si_I({N_SCALAR_B,new_numer_of_rad_points, new_numer_of_ang_points});
    amp_new.resize(Si_I.getNsuper_index());

    for (int i_ang = 0; i_ang < new_numer_of_ang_points; ++i_ang) {

        for (int i_rad = 0; i_rad < new_numer_of_rad_points; ++i_rad) {

    //important
            array<double,2> new_x_values ={ps.ll.getGrid()[i_rad],ps.zl.getGrid()[i_ang]};

            //performing the interpolation only one time for all amplitudes t[i]. E,F,G,H.
            Interpol.getValue(result_y,new_x_values );

            for (int j = 0; j < N_SCALAR_B; ++j) {
                amp_new[Si_I.is({j, i_rad, i_ang})] = result_y[j];
            }
        }

    }


    //if there is a cheby dependence we need to expand into the angular dependece first
//    if(want_cheb_file){
//
//
//        SuperIndex<3> Si_I_file({N_SCALAR_B,rad_points_in_readInn_file, ps.chebs.getNumber_of_grid_points()});
//        SuperIndex<3> Si_I_wanted({N_SCALAR_B,rad_points_in_readInn_file, ps.zl.getNumber_of_grid_points()});
//        VecCdoub amp_cheb(Si_I_wanted.getNsuper_index(), 0.0);
//
//        cheb Cheby( ps.chebs.getNumber_of_grid_points());
//
//        for (int i_ang = 0; i_ang <  ps.chebs.getNumber_of_grid_points(); ++i_ang) {
//
//            for (int i_rad = 0; i_rad < rad_points_in_readInn_file; ++i_rad) {
//
//                for (int j = 0; j < N_SCALAR_B; ++j) {
//
//                    for (int i_m = 0; i_m < ang_value.size(); ++i_m) {
//                        amp_cheb[Si_I_wanted.is({j, i_rad, i_ang})] +=   Cheby.get(i_m,ps.zl.getGrid()[i_m]) * amps[Si_I_file.is({j, i_rad, i_m})];
//                    }
//
//                }
//
//
//            }
//
//        }
//
//        amps=amp_cheb;
//
//    }



    //and last: Write out the amplitude:
    string filename_out=filename+".txt";
    ofstream write(filename_out);
    assert( write.is_open());

    write<<"# safed as: p2   ti (real/imag) i="<<N_SCALAR_B<<endl;
    write<<"# #gridpoints: rad= "<<new_numer_of_rad_points<<", ang= "<<new_numer_of_ang_points<<endl;
    write<<"## mass="<<tab<<mass_here<<endl;

    for (int i_ang = 0; i_ang < new_numer_of_ang_points; ++i_ang) {

        for (int i_rad = 0; i_rad < new_numer_of_rad_points; ++i_rad) {

            write<<scientific<<setprecision(8)<< ps.ll.getGrid()[i_rad]<<tab<<0.0
                 << tab ;

            for (int j = 0; j < N_SCALAR_B; ++j) {

                Cdoub value = amp_new[Si_I.is({j, i_rad, i_ang})];
                write<<scientific<<setprecision(10)<< real( value ) << tab << imag (value) <<tab;
            }

            write<<scientific<<setprecision(5)<< ps.zl.getGrid()[i_ang] << tab;
            write<<scientific<<setprecision(7)<< 0.0 << tab << endl;

        }

        write<<endl<<endl;

    }

    write<<endl<<endl<<endl;
    write.close();

    cout<<"The interpolation for the meson has been performed."<<endl<<" The result is now written in: "<< filename_out
        <<"  with: n_total="<<amp_new.size()<<endl;


}




VecCdoub cheby_expand_bse_amp(TFF_ps& ps, int& rad_points_in_readInn_file, VecDoub& ang_values, VecCdoub& amps)
{
    int n_cheb=ang_values.size();
    SuperIndex<3> Si_I_file({N_SCALAR_B,rad_points_in_readInn_file, n_cheb});
    SuperIndex<3> Si_I_wanted({N_SCALAR_B,rad_points_in_readInn_file, ps.zl.getNumber_of_grid_points()});
    VecCdoub amp_cheb(Si_I_wanted.getNsuper_index(), 0.0);

    cheb Cheby( n_cheb);
    ang_values.resize(ps.zl.getNumber_of_grid_points());

    for (int i_ang = 0; i_ang <  ps.zl.getNumber_of_grid_points(); ++i_ang) {
//    for (int i_ang = 0; i_ang <  ps.chebs.getNumber_of_grid_points(); ++i_ang) {

        for (int i_rad = 0; i_rad < rad_points_in_readInn_file; ++i_rad) {

            for (int j = 0; j < N_SCALAR_B; ++j) {

                for (int i_m = 0; i_m < n_cheb; ++i_m) {
                    amp_cheb[Si_I_wanted.is({j, i_rad, i_ang})] +=   Cheby.get(i_m,ps.zl.getGrid()[i_ang]) * amps[Si_I_file.is({j, i_rad, i_m})];
                    if(i_m==0){amp_cheb[Si_I_wanted.is({j, i_rad, i_ang})] *= sqrt(2.0);}
                }
            }
        }

        ang_values[i_ang] = ps.zl.getGrid()[i_ang];
    }

    return amp_cheb;
//    amps=amp_cheb;

}
/*void get_cheby_version_of_bse(string filename, int& Cheby_points, int& rad_points_in_readInn_file, TFF_ps& ps){

    //Read Inn the meson data: One needs to know how much the amount of radial points is.
//    int n_rad_points = 40;

    double k1, k1c, Q,f1, f1c, f2, f2c, f3, f3c, f4, f4c, fP, fz;
    VecCdoub amps;
    VecDoub rad_value, ang_value;
    matDoub x_values;
    double mass_here;

    int index_i=0;

    //Reading in a previous data file to perform an interpolation on this.
    ifstream infile;
    string filename_inn = filename+"_precalc.txt";
    infile.open(filename_inn.c_str(), ios::in);
    if(infile.is_open())
    {
        string daten;
        while(getline(infile,daten))
        {
            //the following line trims white space from the beginning of the string
            daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

            VecDoub dummy; string s = ""; int pos;
            //Here reading in the mass from the output file.
            if(daten[0] == '#') {
                if(daten[1] == '#')
                {
                    pos= daten.find("\t");
                    for (int j = pos; j < daten.size(); ++j) {
                        s +=daten[j]; }
                    mass_here = stod(s); /// updates the mass in phase space.cpp
                }else{continue;}
            }else {


                if (daten == "") { continue; }
                stringstream(daten) >> k1 >> k1c >> f1 >> f1c >> f2 >> f2c >> f3 >> f3c >> f4 >> f4c
                                    >> fP >> fz;
                if (daten == "") { continue; }

                amps.push_back(f1 + i_ * f1c);
                amps.push_back(f2 + i_ * f2c);
                amps.push_back(f3 + i_ * f3c);
                amps.push_back(f4 + i_ * f4c);

                index_i++;
                rad_value.push_back(k1); // Do I need to do log here????
                if ((index_i % rad_points_in_readInn_file) == 0) { ang_value.push_back(fz); }


            }

        }
    }else{cout<<"Meson Interpolation ReadInn::readIn: No data file found! here: "<<filename_inn<<endl; assert( false);}
    infile.close();

    rad_value.resize(rad_points_in_readInn_file);
    x_values.push_back(rad_value); x_values.push_back(ang_value);
//    cout<<"Test: "<<rad_value.size()<<tab<<ang_value.size()<<endl;

    //changing the old file name:
    cout<<"A Cquark has been read inn:"<<filename_inn<<"  with: n_total="<<amps.size()<<endl;
    string helper="mv "+filename+"_precalc.txt "+ filename+"_precalc_original.txt";
    int dummy = system(helper.c_str());


    array<Cdoub, N_SCALAR_B> result_y;

    SuperIndex<3> Si_I({N_SCALAR_B,rad_points_in_readInn_file, Cheby_points});
    VecCdoub amp_new(Si_I.getNsuper_index(), 0.0);

    cheb Cheby(Cheby_points);

    for (int i_ang = 0; i_ang < Cheby_points; ++i_ang) {

        for (int i_rad = 0; i_rad < rad_points_in_readInn_file; ++i_rad) {

            for (int j = 0; j < N_SCALAR_B; ++j) {

                for (int i_m = 0; i_m < ang_value.size(); ++i_m) {
                    amp_new[Si_I.is({j, i_rad, i_ang})] +=   Cheby.get(i_m,ps.zl.getGrid()[i_m]);
                }

            }


        }

    }



    //and last: Write out the amplitude:
    string filename_out=filename+"_precalc.txt";
    ofstream write(filename_out);
    assert( write.is_open());

    write<<"# safed as: p2   ti (real/imag) i="<<N_SCALAR_B<<endl;
    write<<"# #gridpoints: rad= "<<rad_points_in_readInn_file<<", ang= "<<Cheby_points<<endl;
    write<<"## mass="<<tab<<mass_here<<endl;

    for (int i_ang = 0; i_ang < Cheby_points; ++i_ang) {

        for (int i_rad = 0; i_rad < rad_points_in_readInn_file; ++i_rad) {

            write<<scientific<<setprecision(8)<<real(ps.ll.getGrid()[i_rad])<<tab<<0.0
                 << tab ;

            for (int j = 0; j < N_SCALAR_B; ++j) {

                Cdoub value = amp_new[Si_I.is({j, i_rad, i_ang})];
                write <<scientific<<setprecision(10)<< real( value ) << tab << imag (value) <<tab;
            }

            write<<scientific<<setprecision(5)<<real(ps.zl.getGrid()[i_ang])<< tab;
            write<<scientific<<setprecision(7)<< 0.0 << tab << endl;

        }

        write<<endl<<endl;

    }

    write<<endl<<endl<<endl;
    write.close();

    cout<<"The interpolation for the meson has been performed."<<endl<<" The result is now written in: "<< filename_out
        <<"  with: n_total="<<amp_new.size()<<endl;


}*/
//Debug: Dummy write:
void dummy_write_S(TFF_ps& ps, string& folder, QuarkDse& dse, double& alpha){

    vector<array<Cdoub,2>> S3;

    cout<<"Writting out the dummy variables for the quarks here... Those should be read in a bit."<<endl;

    int n_ll=ps.ll.getNumber_of_grid_points();
    int n_lz= ps.zl.getNumber_of_grid_points();
    int n_ly =  ps.yl.getNumber_of_grid_points();
    int n_pvec =  ps.Q_Qp_P.size();

    SuperIndex<4> Si_quark3({n_ll, n_lz, n_ly, n_pvec});
    SuperIndex<2> Si_quarkpm({n_ll, n_lz});

    int n;
    n=Si_quarkpm.getNsuper_index();


    //writing it into a file:
    ofstream write;
    write.open(folder+"quark_grid_out_3.txt");
    for (int i_s3 = 0; i_s3 < Si_quark3.getNsuper_index(); ++i_s3) {
        array<int, 4> part_i = Si_quark3.are(i_s3);
        fv<Cdoub> ll; ll.set_spherical(ps.ll.getGrid()[part_i[0]],0.0, ps.yl.getGrid()[part_i[2]], ps.zl.getGrid()[part_i[1]]);
        fv<Cdoub> k3 =  ll + 0.5 *  get_p_vec(alpha, ps.Q_Qp_P[part_i[3]][0], ps.Q_Qp_P[part_i[3]][1], ps.Q_Qp_P[part_i[3]][2]);

        Cdoub quark_A = 1.0+ 0.6/(1.0 + (k3*k3));
        Cdoub quark_B = 0.8/(1.0 + (k3*k3));

//        write<< scientific <<setprecision(7) << real(k3*k3)<<tab<<imag(k3*k3)<<tab<<real(S3[i_s3][0])<<tab<<imag(S3[i_s3][0])<<tab<<real(S3[i_s3][1])<<tab<<imag(S3[i_s3][1])<<tab<<endl;
        write<< scientific <<setprecision(7) << real(k3*k3)<<tab<<imag(k3*k3)<<tab

             <<real(quark_A)<<tab<<imag(quark_A)<<tab<<real(quark_B)<<tab<<imag(quark_B)
             <<tab<< endl;
//             ps.ll.getGrid()[part_i[0]] <<tab << ps.yl.getGrid()[part_i[2]]<< tab << ps.zl.getGrid()[part_i[1]] << tab
//             << real(ps.Q_Qp_P[part_i[3]][0]) << tab << real(ps.Q_Qp_P[part_i[3]][1]) <<endl;

    }

    write.close();


    for (int i = 1; i < 3; ++i) {

        //writing it into a file:
        ofstream write;
        write.open(folder+"quark_grid_out_"+to_string(i)+".txt");
        for (int i_s3 = 0; i_s3 < n; ++i_s3) {
            array<int, 2> part_i = Si_quarkpm.are(i_s3);
            fv<Cdoub> ll; ll.set_spherical(ps.ll.getGrid()[part_i[0]],0.0, 1.0, ps.zl.getGrid()[part_i[1]]);
            fv<Cdoub> P = get_P_vec( ps.Q_Qp_P[0][2]);
            fv<Cdoub> k3;
            if(i==1){
                 k3 = get_kp(ll, P);
            }else{
                 k3 = get_km(ll, P);
            }

            Cdoub quark_A = 1.0+ 0.6/(1.0 + (k3*k3));
            Cdoub quark_B = 0.8/(1.0 + (k3*k3));


            write<< scientific <<setprecision(7) << real(k3*k3)<<tab<<imag(k3*k3)<<tab
                 <<real(quark_A)<<tab<<imag(quark_A)<<tab<<real(quark_B)<<tab<<imag(quark_B)<<tab<<
                 endl;
//                          ps.ll.getGrid()[part_i[0]] <<tab << ps.yl.getGrid()[part_i[2]]<< tab << ps.zl.getGrid()[part_i[1]] << tab
//                          << real(ps.Q_Qp_P[part_i[3]][0]) << tab << real(ps.Q_Qp_P[part_i[3]][1]) <<endl;
        }

        write.close();

    }


}
