//
// Created by esther on 17.10.17.
//

#ifndef CALC_VMESON_PHASESPACE_H
#define CALC_VMESON_PHASESPACE_H

#include <array>
#include <Parameter.h>
#include "typedefs.h"
#include "Grids.h"
#include <Routing/routing_amount_scalars.h>

//class line_info
//{
//public:
//    line_info(const double lower_boundary, const double upper_boundary, const int number_of_grid_points,
//    const string point_distribution, const string functional_meassure,
//    const bool quadrature, const bool barycentric):lower_boundary(lower_boundary), upper_boundary(upper_boundary),
//    number_of_grid_points(number_of_grid_points), point_distribution(point_distribution),
//    functional_meassure(functional_meassure), quadrature(quadrature), barycentric(barycentric){};
//
//    double lower_boundary, upper_boundary;
//    int number_of_grid_points;
//    bool barycentric, quadrature;
//    string functional_meassure, point_distribution;
//
//};



//typedef std::array<string,7>    LineInfo;


template <int dim> class Phasespace {

//    typedef std::array<Line*, dim>    PSLines;
//    typedef std::array<line_info , dim>    PSLineInfo;
//    using PSLineInfo =  std::array<line_info , dim>;
//    typedef std::array<LineInfo , dim>    PSLineInfo;
    using  PSLines = std::array<Line*, dim>   ;


public:


//    Phasespace(){};

//    Phasespace(const PSLineInfo &Info){
//
//        for (int i = 0; i < dim; ++i) {
//
//
//            AllGrid[i] = new Line(Info[i].lower_boundary, Info[i].upper_boundary, Info[i].number_of_grid_points,
//                                  Info[i].point_distribution,Info[i].functional_meassure,
//                                  Info[i].quadrature,Info[i].barycentric);
//        }
//
//    }

    /// destructor
//    virtual ~Phasespace() {
//        for (int i = 0; i < dim; ++i) {
//            delete(AllGrid[i]);
//        }
//    }


//    virtual  array<double, dim> get_phase_space_doubles(array<int, dim> &int_values) =0 ;


    PSLines AllGrid;

};

class Dse_ps : public  Phasespace<2>{

public:

    Dse_ps(const DseParameter& pa){

        //todo: the option of bayricentric and quadrature could still beincluded in const file.
        pp =  Line(pa.IRcutsq, pa.UVcutsq, pa.n_rad,
                    pa.func_meassure_rad, pa.point_distri_rad, true, false);

        zp =  Line(-1.0,1.0, pa.n_angle, pa.func_meassure_ang, pa.point_distri_ang, true, false);

        AllGrid = {&pp,&zp};

    }

    array<double, 2> get_phase_space_doubles(array<int, 2> &int_values) const{

        return {pp.getGrid()[int_values[0]], zp.getGrid()[int_values[1]]};
    }

    Line pp;
    Line zp;


};

//typedef array<double, 5> ArrayBsePhaseSpaceDoub;
//typedef array<Cdoub, 10> ArrayScalarProducts;
//typedef Cdoub (*FunPtrForScalarProducts)( const ArrayBsePhaseSpaceDoub & , double );
//typedef vector< FunPtrForScalarProducts> CollectionOfSps;


class Bse_ps : public  Phasespace<6>{

public:

    Bse_ps( BseParameter& pa){

        pp =  Line(pa.IRcutsq, pa.UVcutsq, pa.n_pp, pa.func_meassure_rad,
                   pa.point_distri_rad,true, false);

        zp =  Line(-1.0,1.0, pa.n_pz,  pa.func_meassure_ang1, pa.point_distri_ang1, true, false);


        ll =  Line(pa.IRcutsq, pa.UVcutsq, pa.n_ll, pa.func_meassure_rad, pa.point_distri_rad, true, false);

        //If you choose the point distribution to be between the zeros of the Chebys with n=5 points in between.
        if(pa.point_distri_ang1 == "tscheb_zeros"){
            zl =  Line(-1.0,1.0, pa.n_cheb, pa.func_meassure_ang1, pa.point_distri_ang1, true, false);
            pa.n_lz = zl.getGrid().size();
        }else{
            zl =  Line(-1.0,1.0, pa.n_lz, pa.func_meassure_ang1, pa.point_distri_ang1, true, false);
        }

        yl =  Line(-1.0,1.0, pa.n_ly, pa.func_meassure_ang2, pa.point_distri_ang2, true, false);

        chebs =  Line(-1.0,1.0, pa.n_cheb, "linear" , "tscheb", false, false);


        AllGrid = {&pp,&zp, &ll, &zl, &yl, &chebs};
//        Mass=NAN;

    }

    template <size_t T> ArrayBsePhaseSpaceDoub get_phase_space_doubles(const array<int, T> &int_values, const array<int, T>& which_sphericals ) const{

        ArrayBsePhaseSpaceDoub res;
        res.fill(NAN);

/*        for (int i = 0; i < T; ++i) {

            res[which_sphericals[i]] = AllGrid[which_sphericals[i]]->getGrid()[int_values[i]];
//            cout<<"ps: "<<res[which_sphericals[i]]<<endl;
        }

        return res;*/

//important: Change back!!!1 HERE
        for (int i = 0; i < T; ++i) {

            //(important: Hard coded that we only call the complex version for the first. If this stays like this, I could put the if staement outside the loop.)
            if(grid_replace && (which_sphericals[i]<2 )){
                res[which_sphericals[i]] = AllGrid[which_sphericals[i]]->getCGrid()[int_values[i]];
            }else{
                res[which_sphericals[i]] = AllGrid[which_sphericals[i]]->getGrid()[int_values[i]];
            }

        }


        return res;
    }


    //The same function as above but here with the special implementation for Chebychevs.
    template <size_t T> ArrayBsePhaseSpaceDoub get_phase_space_doubles_cheb(const array<int, T> &int_values, const array<int, T>& which_sphericals ) const{

        ArrayBsePhaseSpaceDoub res;
        res.fill(NAN);

        if(grid_replace){assert(false);}//We can not use the Cheby version when using the iteration to obtain a certain value, we need to project out the angle first!!!!

        for (int i = 0; i < T; ++i) {

            res[which_sphericals[i]] = AllGrid[which_sphericals[i]]->getGrid()[int_values[i]];
//            cout<<res[which_sphericals[i]]<<endl;
        }

        //in this special case we are using Chebys for the outside angle, thus we change the values for z_p here to z_cheb.
        //such that f(zp)= Sum_beta c_beta Cheby_beta(zp), when we are projecting out we use a projector such that P^alpha(z_p)= sum_m T_alpha(z_m)
        //important: very bad fix here with the Cheby, what if it is not int_values[1], so not the second entry????
        res[1] =  AllGrid[5]->getGrid()[int_values[1]];

        return res;
    }

    //Setter&getter for Mass
    const Cdoub &getPsquared() const {
        return Psquared;
    }

    void setPsquared(const Cdoub &Psquared) {
        Bse_ps::Psquared = Psquared;
    }

    void replace_ps_grid(Line &grid, int replace_which_grid ){
        if(replace_which_grid==0){
            pp = grid;
        }else if(replace_which_grid==1){
            zp = grid;
        }else if(replace_which_grid ==2){
            ll = grid;
        }else if(replace_which_grid==3){
            zl = grid;
        }else if(replace_which_grid==4){
            yl = grid;
        }
        grid_replace=true;
    }


    Line ll;
    Line zl;
    Line yl;

    Line pp;
    Line zp;

    Line chebs;

private:

    Cdoub Psquared;
    bool grid_replace=false;
};



class TFF_ps : public  Phasespace<4>{

public:

    TFF_ps( TFFParameter& pa){


        ll =  Line(pa.IRcutsq, pa.UVcutsq, pa.n_ll, pa.func_meassure_rad, pa.point_distri_rad, true, false);

        //If you choose the point distribution to be between the zeros of the Chebys with n=5 points in between.
        if(pa.point_distri_ang1 == "tscheb_zeros"){
            zl =  Line(-1.0,1.0, pa.n_cheb, pa.func_meassure_ang1, pa.point_distri_ang1, true, false);
            pa.n_lz = zl.getGrid().size();
        }else{
            zl =  Line(-1.0,1.0, pa.n_lz, pa.func_meassure_ang1, pa.point_distri_ang1, true, false);
        }

        yl =  Line(-1.0,1.0, pa.n_ly, pa.func_meassure_ang2, pa.point_distri_ang2, true, false);

        chebs =  Line(-1.0,1.0, pa.n_cheb, "linear" , "tscheb", false, false);


        AllGrid = {&ll, &zl, &yl, &chebs};

    }

    /*template <size_t T> ArrayBsePhaseSpaceDoub get_phase_space_doubles(const array<int, T> &int_values, const array<int, T>& which_sphericals ) const{

        ArrayBsePhaseSpaceDoub res;
        res.fill(NAN);

        for (int i = 0; i < T; ++i) {

            res[which_sphericals[i]] = AllGrid[which_sphericals[i]]->getGrid()[int_values[i]];
//            cout<<res[which_sphericals[i]]<<endl;
        }

        return res;
    }


    //The same function as above but here with the special implementation for Chebychevs.
    template <size_t T> ArrayBsePhaseSpaceDoub get_phase_space_doubles_cheb(const array<int, T> &int_values, const array<int, T>& which_sphericals ) const{

        ArrayBsePhaseSpaceDoub res;
        res.fill(NAN);

        for (int i = 0; i < T; ++i) {

            res[which_sphericals[i]] = AllGrid[which_sphericals[i]]->getGrid()[int_values[i]];
//            cout<<res[which_sphericals[i]]<<endl;
        }

        //in this special case we are using Chebys for the outside angle, thus we change the values for z_p here to z_cheb.
        res[1] =  AllGrid[5]->getGrid()[int_values[1]];

        return res;
    }*/


    Line ll;
    Line zl;
    Line yl;

    vector<array<Cdoub, 3> > Q_Qp_P;

    Line chebs;

};


#endif //CALC_VMESON_PHASESPACE_H
