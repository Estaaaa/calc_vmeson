//
// Created by paul on 06.07.17.
//

#ifndef CALC_BSE_QUARK_INTERPOLATOR_UTILITIES_H
#define CALC_BSE_QUARK_INTERPOLATOR_UTILITIES_H


#include <Quark.h>
#include "QuarkInterpolator.h"

template <class TypeQuark> void q_real_to_complex(Quark<TypeQuark> &OutQuark, const ComplexQuarkInterpolator<TypeQuark,TypeQuark> &Int){


    for (int i = 0; i < OutQuark.getGrid().size(); ++i) {

        OutQuark.getA()[i] = Int.interpolateAfunat( OutQuark.getGrid()[i]);
        OutQuark.getB()[i] = Int.interpolateBfunat( OutQuark.getGrid()[i]);
    }
}

#endif //CALC_BSE_QUARK_INTERPOLATOR_UTILITIES_H
