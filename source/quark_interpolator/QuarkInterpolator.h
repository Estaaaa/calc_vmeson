#ifndef QUARKINTERPOLATOR_H
#define TYPEDEFS_H

#include <QuarkDse.h>
#include <contours/Parabola.h>
#include "../types/typedefs.h"
#include "CauchyInterpolator.h"
//#include "../quark/Quark.h"

using namespace std;


//class QuarkDse;
//class Quark;


//    std::array bla = {i, pp};
//    K_i_pp.insert({i,pp}, f(i, pp));
//
//
//    Parabola par( 0.01, 10., 0.1, 1., 40, 2 );
//    QuarkDse mydse("", "");
//    ComplexQuarkInterpolatorNaive naive_quark(mydse);
//    ComplexQuarkInterpolatorCauchy cauchy_quark( dse, par)



/**
 * Diese Klasse ist eine Schnittstelle fuer Interpolatoren fuer Quarks
 * Sie kann Interpolatoren haben, die von R->R oder von C->C oder von R->C gehen
 * @tparam InputTypeOfFunctionAB
 * @tparam OutputTypeOfFunctionAB
 */
template <class InputTypeOfFunctionAB, class OutputTypeOfFunctionAB> class ComplexQuarkInterpolator {

   public:
      virtual OutputTypeOfFunctionAB interpolateAfunat(InputTypeOfFunctionAB z) = 0;
      virtual OutputTypeOfFunctionAB interpolateBfunat(InputTypeOfFunctionAB z) = 0;

};

typedef ComplexQuarkInterpolator<double, double> ComplexQuarkInterpolatorRealReal;
typedef ComplexQuarkInterpolator<double, Cdoub> ComplexQuarkInterpolatorRealImag;
typedef ComplexQuarkInterpolator<Cdoub, Cdoub> ComplexQuarkInterpolatorImagImag;



class ComplexQuarkInterpolatorNaive : public ComplexQuarkInterpolatorImagImag{

public:

    ComplexQuarkInterpolatorNaive(QuarkDse &a_dse) : dse(a_dse){

  };

  Cdoub interpolateAfunat(Cdoub z){

    dse.getRhsA( z);
  };

  Cdoub interpolateBfunat(Cdoub z){

    dse.getRhsB( z);
  };


protected:
    QuarkDse dse;

};

class ComplexQuarkInterpolatorCauchy : public ComplexQuarkInterpolatorImagImag{

public:

    void fill_A_B_on_parabola(VecCdoub &A_on_par, VecCdoub &B_on_par) {

      for (int i = 0; i < par.getZvec().size(); ++i) {

        A_on_par[i] = dse.getRhsA( par.getZvec()[i]);
        B_on_par[i] = dse.getRhsB( par.getZvec()[i]);
      }

    }

    ComplexQuarkInterpolatorCauchy(QuarkDse a_dse, Parabola a_par) : dse(a_dse), par(a_par){
    //  : A_on_parabola(fill()), CIntBfun(new CauchyInterpolator ( par.getZvec(), A_on_parabola, par.getZvec()))

    ComplexQuarkInterpolatorNaive naive_interpolator( dse);


      VecCdoub A_on_parabola, B_on_parabola;
      fill_A_B_on_parabola(A_on_parabola, B_on_parabola);

      CIntAfun = new CauchyInterpolator ( par.getZvec(), A_on_parabola, par.getWeights_z());
      CIntBfun = new CauchyInterpolator ( par.getZvec(), B_on_parabola, par.getWeights_z());

    }

    ~ComplexQuarkInterpolatorCauchy(){

        delete CIntAfun;
        delete CIntBfun;
       // delete point_to_A;
       // delete point_to_B;
    }

  virtual Cdoub interpolateAfunat(Cdoub z){

    CIntAfun->interpolate( z);
  };

  virtual Cdoub interpolateBfunat(Cdoub z){

    CIntBfun->interpolate( z);
  };

protected:

  QuarkDse dse;
  Parabola par;
  CauchyInterpolator *CIntAfun = NULL;
  CauchyInterpolator *CIntBfun = NULL;
  //unique_ptr<CauchyInterpolator> CIntAfun;
};


class RealInterpolatorQuarkSpline : public ComplexQuarkInterpolatorRealReal{




};



#endif
