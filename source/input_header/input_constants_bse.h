//
// Created by esther on 01.11.19.
//

#ifndef CALC_VMESON_INPUT_CONSTANTS_H
#define CALC_VMESON_INPUT_CONSTANTS_H


#include <typedefs.h>

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//Program related defines:

#define K_ORDER_OF_ANGLE                    4

//#define K_WRITEOUT_NOPROJ
#ifndef K_WRITEOUT_ANGPROJC
#define K_WRITEOUT_ANGPROJC
#endif


//specify which Maris-Tandy paramters are used in the code.
#ifndef USE_MT_PARA_FOLDER_30
#define USE_MT_PARA_FOLDER_30
#endif

//#ifndef USE_MT_PARA_FOLDER_28
//#define USE_MT_PARA_FOLDER_28
//#endif



//Read in from external file or directly with paramters in main
#ifndef K_USING_INPUT_FILE
#define K_USING_INPUT_FILE
#endif

//#ifndef K_NOT_USING_INPUT_FILE
//#define K_NOT_USING_INPUT_FILE
//#endif

#ifndef K_CALC_MULTIPLE
#define K_CALC_MULTIPLE
#endif

#ifndef K_CALC_MULTIPLE_SCALAR
#define K_CALC_MULTIPLE_SCALAR
#endif

//#ifndef K_CALC_MULTIPLE_PS
//#define K_CALC_MULTIPLE_PS
//#endif


/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////


//amount of basis elements in case of the scalar meson
#ifndef N_SCALAR_B
#define N_SCALAR_B 4
#endif

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////







#endif //CALC_VMESON_INPUT_CONSTANTS_H
