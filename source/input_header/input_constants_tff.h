//
// Created by esther on 01.11.19.
//

#ifndef CALC_VMESON_INPUT_CONSTANTS_H
#define CALC_VMESON_INPUT_CONSTANTS_H


#include <typedefs.h>

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
//Program related defines:

#define K_ORDER_OF_ANGLE                    4

//#define K_WRITEOUT_NOPROJ
#ifndef K_WRITEOUT_ANGPROJC
#define K_WRITEOUT_ANGPROJC
#endif


//specify which Maris-Tandy paramters are used in the code.
//#ifndef USE_MT_PARA_FOLDER_28
//#define USE_MT_PARA_FOLDER_28
//#endif
//- entweder oder-
#ifndef USE_MT_PARA_FOLDER_30
#define USE_MT_PARA_FOLDER_30
#endif

#ifndef USE_OLDER_DATA_SET
#define USE_OLDER_DATA_SET
#endif



//If my vertex is used or not
#ifndef NOT_USE_MY_QPHV_VERTEX
#define NOT_USE_MY_QPHV_VERTEX
#endif


//Read in from external file or directly with paramters in main
//#ifndef K_NOT_USING_INPUT_FILE
//#define K_NOT_USING_INPUT_FILE
//#endif

#ifndef K_USING_INPUT_FILE
#define K_USING_INPUT_FILE
#endif




/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////

//for the formfactor part of the program

//alpha frame paramter
//#ifndef K_ALPHA
//#define K_ALPHA 1.0e+20
//#endif

//#ifndef K_USING_ALPHA_FRAME
//#define K_USING_ALPHA_FRAME
//#endif



//How is the TFF calculated: vertex - full, BC, bare.

//quarks and mesons
#ifndef K_NOT_USING_MESON_MODEL
#define K_NOT_USING_MESON_MODEL
#endif

#ifndef K_NOT_USING_ONLY_FIRST_AMP_MESON
#define K_NOT_USING_ONLY_FIRST_AMP_MESON
#endif

#ifndef K_NOT_USING_QUARK_MODEL
#define K_NOT_USING_QUARK_MODEL
#endif



//vertex;

#ifndef K_WITH_FULL_QPHV
#define K_WITH_FULL_QPHV
#endif

//#ifndef K_WITH_BARE_VERTEX
//#define K_WITH_BARE_VERTEX
//#endif

//#ifndef K_WITH_BC_VERTEX
//#define K_WITH_BC_VERTEX
//#endif

//amount of basis elements in case of the scalar meson
#ifndef N_SCALAR_B
#define N_SCALAR_B 4
#endif

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////







#endif //CALC_VMESON_INPUT_CONSTANTS_H
