#ifndef CALC_BSE_TYPEDEFS_H
#define CALC_BSE_TYPEDEFS_H

#include <iostream>
#include <vector>
#include <complex>


using namespace std;

typedef std::complex<double> Cdoub;

typedef std::vector<double> VecDoub;
typedef std::vector<Cdoub>  VecCdoub;
typedef std::vector<int>    VecInt;

typedef std::vector< std::vector<double> > matDoub;
typedef std::vector< std::vector<Cdoub> >  matCdoub;
typedef Cdoub**  matCdoubArray;


typedef Cdoub (*FunPtrCdoubOfConstCdoub)( const Cdoub);
typedef Cdoub (*FunPtrCdoubOfCdoub)( Cdoub);

/// Types for Kernels
typedef matCdoub (*FunPtrKernelOneFunktion)( const VecDoub &, const VecCdoub &); // real scalar products, complex propagators etc
typedef vector< FunPtrKernelOneFunktion > FunPtrKernelFullMatrix;


#ifndef tab
#define tab "\t"
#endif

#ifndef i_
#define i_ std::complex<double> (0.,1.)
#endif


#endif
