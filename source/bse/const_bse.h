////
//// Created by paul on 10.07.17.
////
//
//#ifndef CALC_BSE_CONST_BSE_H
//#define CALC_BSE_CONST_BSE_H
//
//#include <typedefs.h>
//
////////////////////////////// debugging options //////////////////////////////
////#define DEBUG_QUARKS_EQUAL_1
////#define SCALAR_PROD_EQUAL_1
//
///////////////////////////// Interpolation //////////////////////////////////]
//#define INTERPOLATOR_WALTER_BAR
//
//
////////////////////////////// precalc objects ///////////////////////////////////
//#define K_PRECALC_QUARK          true
//#define K_PRECALC_GLUON          false
//#define K_PRECALC_KERNEL_FULL    false
//#define K_PRECALC_KSTAR          true
//#define K_PRECALC_GMAT           true
//#define K_STORE_CONTENT_F_INT    true      // false not really implemented yet
//
///////////////////////////// Color, Dirac, Flavor base /////////////////////////////]
//#define K_MAX_PROJECTORS_DIRAC              4
//#define K_MAX_BASE_ELEMENTS_DIRAC           4
//#define K_N_PROJECTORS_DIRAC                4
//#define K_N_BASE_ELEMENTS_DIRAC             4
//#define K_N_DIAG                            1
//
///////////////////////////// integration  //////////////////////////////////////////
//const string K_LSQUARED_PT_DISTRIBUTION   = "leg";
//const string K_LSQUARED_MEASURE           = "log";    // this initializes a grid an than does func. trafo to log
//#define      K_IR_CUTOFF_LSQUARED            1.e-6
//#define      K_UV_CUTOFF_LSQUARED            1.e+6
//
//#define      K_N_LSQUARED                    2
//#define      K_N_ZL                          2
//#define      K_N_YL                          2
//#define      K_N_PHIL                        1
//
//
////////////////////////////  External Grid //////////////////////////////////////
//const string K_PSQUARED_PT_DISTRIBUTION    = "leg";
//const string K_PSQUARED_MEASURE            = "log";
//#define K_PSQUARED_IR_CUTOFF                  K_IR_CUTOFF_LSQUARED
//#define K_PSQUARED_UV_CUTOFF                  K_UV_CUTOFF_LSQUARED
//
//#define K_N_PSQUARED                         3
//#define   K_N_ZP                             K_N_ZL
//#define K_N_YP                               K_N_YL
//#define K_N_PHIP                             K_N_PHIL
//
//const string K_ANGLES_PT_DISTRIBUTION      = "leg";
//const string K_ANGLES_MEASURE              = "linear"; // always linear
//
//
////////////////////////// Debugging ///////////////
//#define DEBUG_PAUL
//
//
//
//#endif //CALC_BSE_CONST_BSE_H
