#include "./eta_routing_scalar_products.h" 


Cdoub sp_0 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = pp_zp_ll_zl_yl[2] + pp_zp_ll_zl_yl[0] - 2.*sqrt(pp_zp_ll_zl_yl[2]*pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[3]*pp_zp_ll_zl_yl[1] - 2.*pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_1 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = -1.*pp_zp_ll_zl_yl[2] + eta*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1] + pp_zp_ll_zl_yl[3]*(-1.*eta*sqrt(pp_zp_ll_zl_yl[2])*Mpi + sqrt(pp_zp_ll_zl_yl[2]*pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_2 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = pp_zp_ll_zl_yl[2] + pow(eta,2)*pow(Mpi,2) + 2.*eta*sqrt(pp_zp_ll_zl_yl[2])*Mpi*pp_zp_ll_zl_yl[3];
 
 return result;
 } 


Cdoub sp_3 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = pp_zp_ll_zl_yl[0] + eta*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1] - 1.*pp_zp_ll_zl_yl[3]*(eta*sqrt(pp_zp_ll_zl_yl[2])*Mpi + sqrt(pp_zp_ll_zl_yl[2]*pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) - 1.*pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_4 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = (eta*Mpi + sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3])*(eta*Mpi + sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_5 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = pow(eta,2)*pow(Mpi,2) + pp_zp_ll_zl_yl[0] + 2.*eta*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1];
 
 return result;
 } 


Cdoub sp_6 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = -1.*pp_zp_ll_zl_yl[2] + (-1. + eta)*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1] + pp_zp_ll_zl_yl[3]*(-1.*(-1. + eta)*sqrt(pp_zp_ll_zl_yl[2])*Mpi + sqrt(pp_zp_ll_zl_yl[2]*pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_7 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = pp_zp_ll_zl_yl[2] + (-1. + eta)*eta*pow(Mpi,2) + (-1. + 2.*eta)*sqrt(pp_zp_ll_zl_yl[2])*Mpi*pp_zp_ll_zl_yl[3];
 
 return result;
 } 


Cdoub sp_8 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = ((-1. + eta)*Mpi + sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3])*(eta*Mpi + sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_9 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = pp_zp_ll_zl_yl[2] + pow(-1. + eta,2)*pow(Mpi,2) + 2.*(-1. + eta)*sqrt(pp_zp_ll_zl_yl[2])*Mpi*pp_zp_ll_zl_yl[3];
 
 return result;
 } 


Cdoub sp_10 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = pp_zp_ll_zl_yl[0] + (-1. + eta)*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1] - 1.*sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3]*((-1. + eta)*Mpi + sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) - 1.*pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_11 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = (eta*Mpi + sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3])*((-1. + eta)*Mpi + sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_12 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = -1.*eta*pow(Mpi,2) + pow(eta,2)*pow(Mpi,2) + pp_zp_ll_zl_yl[0] + (-1. + 2.*eta)*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1];
 
 return result;
 } 


Cdoub sp_13 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = ((-1. + eta)*Mpi + sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3])*((-1. + eta)*Mpi + sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_14 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = pow(-1. + eta,2)*pow(Mpi,2) + pp_zp_ll_zl_yl[0] + 2.*(-1. + eta)*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1];
 
 return result;
 } 


Cdoub sp_15 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = 2.*(-1. + eta)*eta*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1] + sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3]*(-2.*(-1. + eta)*eta*Mpi + (1. - 2.*eta)*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + (-1. + 2.*eta)*(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2)));
 
 return result;
 } 


Cdoub sp_16 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = (eta*Mpi + sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3])*(2.*(-1. + eta)*eta*Mpi + (-1. + 2.*eta)*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + (-1. + 2.*eta)*pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_17 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = -2.*pow(eta,2)*pow(Mpi,2) + 2.*pow(eta,3)*pow(Mpi,2) - 1.*pp_zp_ll_zl_yl[0] + 2.*eta*pp_zp_ll_zl_yl[0] + eta*(-3. + 4.*eta)*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1];
 
 return result;
 } 


Cdoub sp_18 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = ((-1. + eta)*Mpi + sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3])*(2.*(-1. + eta)*eta*Mpi + (-1. + 2.*eta)*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + (-1. + 2.*eta)*pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_19 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = -4.*pow(eta,2)*pow(Mpi,2) + 2.*pow(eta,3)*pow(Mpi,2) - 1.*pp_zp_ll_zl_yl[0] + 2.*eta*(pow(Mpi,2) + pp_zp_ll_zl_yl[0]) + (1. - 5.*eta + 4.*pow(eta,2))*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1];
 
 return result;
 } 


Cdoub sp_20 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = pow(2.*(-1. + eta)*eta*Mpi + (-1. + 2.*eta)*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1],2) + pow(1. - 2.*eta,2)*(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_21 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = pp_zp_ll_zl_yl[0] - 1.*sqrt(pp_zp_ll_zl_yl[2]*pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[3]*pp_zp_ll_zl_yl[1] - 1.*pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_22 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = sqrt(pp_zp_ll_zl_yl[0])*(eta*Mpi + sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3])*pp_zp_ll_zl_yl[1] + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_23 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = pp_zp_ll_zl_yl[0] + eta*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1];
 
 return result;
 } 


Cdoub sp_24 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = sqrt(pp_zp_ll_zl_yl[0])*((-1. + eta)*Mpi + sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3])*pp_zp_ll_zl_yl[1] + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_25 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = sqrt(pp_zp_ll_zl_yl[0])*(sqrt(pp_zp_ll_zl_yl[0]) + (-1. + eta)*Mpi*pp_zp_ll_zl_yl[1]);
 
 return result;
 } 


Cdoub sp_26 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = (-1. + 2.*eta)*pp_zp_ll_zl_yl[0] + 2.*(-1. + eta)*eta*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1];
 
 return result;
 } 


Cdoub sp_27 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = pp_zp_ll_zl_yl[0];
 
 return result;
 } 


Cdoub sp_28 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = Mpi*(-1.*sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3] + sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]);
 
 return result;
 } 


Cdoub sp_29 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = Mpi*(eta*Mpi + sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3]);
 
 return result;
 } 


Cdoub sp_30 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = Mpi*(eta*Mpi + sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]);
 
 return result;
 } 


Cdoub sp_31 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = Mpi*((-1. + eta)*Mpi + sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3]);
 
 return result;
 } 


Cdoub sp_32 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = Mpi*((-1. + eta)*Mpi + sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]);
 
 return result;
 } 


Cdoub sp_33 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = Mpi*(2.*(-1. + eta)*eta*Mpi + (-1. + 2.*eta)*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]);
 
 return result;
 } 


Cdoub sp_34 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1];
 
 return result;
 } 


Cdoub sp_35 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = pow(Mpi,2);
 
 return result;
 } 


Cdoub sp_36 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = -1.*pp_zp_ll_zl_yl[2] + sqrt(pp_zp_ll_zl_yl[2]*pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[3]*pp_zp_ll_zl_yl[1] + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_37 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = pp_zp_ll_zl_yl[2] + eta*sqrt(pp_zp_ll_zl_yl[2])*Mpi*pp_zp_ll_zl_yl[3];
 
 return result;
 } 


Cdoub sp_38 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3]*(eta*Mpi + sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_39 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = sqrt(pp_zp_ll_zl_yl[2])*(sqrt(pp_zp_ll_zl_yl[2]) + (-1. + eta)*Mpi*pp_zp_ll_zl_yl[3]);
 
 return result;
 } 


Cdoub sp_40 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3]*((-1. + eta)*Mpi + sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_41 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3]*(2.*(-1. + eta)*eta*Mpi + (-1. + 2.*eta)*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + (-1. + 2.*eta)*pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_42 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = sqrt(pp_zp_ll_zl_yl[2]*pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[3]*pp_zp_ll_zl_yl[1] + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_43 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = sqrt(pp_zp_ll_zl_yl[2])*Mpi*pp_zp_ll_zl_yl[3];
 
 return result;
 } 


Cdoub sp_44 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta){

 Cdoub result = pp_zp_ll_zl_yl[2];
 
 return result;
 } 


