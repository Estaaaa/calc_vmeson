#include "./routing_scalar_products.h" 


Cdoub sp_0 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = pp_zp_ll_zl_yl[2] + pp_zp_ll_zl_yl[0] - 2.*sqrt(pp_zp_ll_zl_yl[2]*pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[3]*pp_zp_ll_zl_yl[1] - 2.*pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_1 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = -1.*pp_zp_ll_zl_yl[2] + 0.5*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1] + pp_zp_ll_zl_yl[3]*(-0.5*sqrt(pp_zp_ll_zl_yl[2])*Mpi + sqrt(pp_zp_ll_zl_yl[2]*pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_2 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = pp_zp_ll_zl_yl[2] + 0.25*pow(Mpi,2) + sqrt(pp_zp_ll_zl_yl[2])*Mpi*pp_zp_ll_zl_yl[3];
 
 return result;
 } 


Cdoub sp_3 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = pp_zp_ll_zl_yl[0] + 0.5*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1] - 0.5*pp_zp_ll_zl_yl[3]*(sqrt(pp_zp_ll_zl_yl[2])*Mpi + 2.*sqrt(pp_zp_ll_zl_yl[2]*pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) - 1.*pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_4 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = 0.25*(Mpi + 2.*sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3])*(Mpi + 2.*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_5 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = 0.25*pow(Mpi,2) + pp_zp_ll_zl_yl[0] + Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1];
 
 return result;
 } 


Cdoub sp_6 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = -1.*pp_zp_ll_zl_yl[2] - 0.5*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1] + pp_zp_ll_zl_yl[3]*(0.5*sqrt(pp_zp_ll_zl_yl[2])*Mpi + sqrt(pp_zp_ll_zl_yl[2]*pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_7 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = pp_zp_ll_zl_yl[2] - 0.25*pow(Mpi,2);
 
 return result;
 } 


Cdoub sp_8 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = -0.25*(Mpi - 2.*sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3])*(Mpi + 2.*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_9 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = pp_zp_ll_zl_yl[2] + 0.25*pow(Mpi,2) - 1.*sqrt(pp_zp_ll_zl_yl[2])*Mpi*pp_zp_ll_zl_yl[3];
 
 return result;
 } 


Cdoub sp_10 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = pp_zp_ll_zl_yl[0] - 0.5*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1] + 0.5*pp_zp_ll_zl_yl[3]*(sqrt(pp_zp_ll_zl_yl[2])*Mpi - 2.*sqrt(pp_zp_ll_zl_yl[2]*pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) - 1.*pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_11 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = -0.25*(Mpi + 2.*sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3])*(Mpi - 2.*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_12 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = -0.25*pow(Mpi,2) + pp_zp_ll_zl_yl[0];
 
 return result;
 } 


Cdoub sp_13 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = 0.25*(Mpi - 2.*sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3])*(Mpi - 2.*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_14 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = 0.25*pow(Mpi,2) + pp_zp_ll_zl_yl[0] - 1.*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1];
 
 return result;
 } 


Cdoub sp_15 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = 0.5*Mpi*(sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3] - 1.*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]);
 
 return result;
 } 


Cdoub sp_16 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = -0.25*Mpi*(Mpi + 2.*sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3]);
 
 return result;
 } 


Cdoub sp_17 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = -0.25*Mpi*(Mpi + 2.*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]);
 
 return result;
 } 


Cdoub sp_18 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = 0.25*Mpi*(Mpi - 2.*sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3]);
 
 return result;
 } 


Cdoub sp_19 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = 0.25*Mpi*(Mpi - 2.*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]);
 
 return result;
 } 


Cdoub sp_20 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = 0.25*pow(Mpi,2);
 
 return result;
 } 


Cdoub sp_21 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = pp_zp_ll_zl_yl[0] - 1.*sqrt(pp_zp_ll_zl_yl[2]*pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[3]*pp_zp_ll_zl_yl[1] - 1.*pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_22 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = 0.5*sqrt(pp_zp_ll_zl_yl[0])*(Mpi + 2.*sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3])*pp_zp_ll_zl_yl[1] + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_23 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = pp_zp_ll_zl_yl[0] + 0.5*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1];
 
 return result;
 } 


Cdoub sp_24 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = sqrt(pp_zp_ll_zl_yl[0])*(-0.5*Mpi + sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3])*pp_zp_ll_zl_yl[1] + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_25 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = pp_zp_ll_zl_yl[0] - 0.5*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1];
 
 return result;
 } 


Cdoub sp_26 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = -0.5*Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1];
 
 return result;
 } 


Cdoub sp_27 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = pp_zp_ll_zl_yl[0];
 
 return result;
 } 


Cdoub sp_28 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = Mpi*(-1.*sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3] + sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]);
 
 return result;
 } 


Cdoub sp_29 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = 0.5*Mpi*(Mpi + 2.*sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3]);
 
 return result;
 } 


Cdoub sp_30 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = 0.5*Mpi*(Mpi + 2.*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]);
 
 return result;
 } 


Cdoub sp_31 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = Mpi*(-0.5*Mpi + sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3]);
 
 return result;
 } 


Cdoub sp_32 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = Mpi*(-0.5*Mpi + sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]);
 
 return result;
 } 


Cdoub sp_33 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = -0.5*pow(Mpi,2);
 
 return result;
 } 


Cdoub sp_34 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = Mpi*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1];
 
 return result;
 } 


Cdoub sp_35 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = pow(Mpi,2);
 
 return result;
 } 


Cdoub sp_36 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = -1.*pp_zp_ll_zl_yl[2] + sqrt(pp_zp_ll_zl_yl[2]*pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[3]*pp_zp_ll_zl_yl[1] + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_37 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = pp_zp_ll_zl_yl[2] + 0.5*sqrt(pp_zp_ll_zl_yl[2])*Mpi*pp_zp_ll_zl_yl[3];
 
 return result;
 } 


Cdoub sp_38 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = 0.5*sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3]*(Mpi + 2.*sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_39 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = pp_zp_ll_zl_yl[2] - 0.5*sqrt(pp_zp_ll_zl_yl[2])*Mpi*pp_zp_ll_zl_yl[3];
 
 return result;
 } 


Cdoub sp_40 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = sqrt(pp_zp_ll_zl_yl[2])*pp_zp_ll_zl_yl[3]*(-0.5*Mpi + sqrt(pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[1]) + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_41 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = -0.5*sqrt(pp_zp_ll_zl_yl[2])*Mpi*pp_zp_ll_zl_yl[3];
 
 return result;
 } 


Cdoub sp_42 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = sqrt(pp_zp_ll_zl_yl[2]*pp_zp_ll_zl_yl[0])*pp_zp_ll_zl_yl[3]*pp_zp_ll_zl_yl[1] + pp_zp_ll_zl_yl[4]*sqrt(pp_zp_ll_zl_yl[2] - 1.*pp_zp_ll_zl_yl[2]*pow(pp_zp_ll_zl_yl[3],2))*sqrt(pp_zp_ll_zl_yl[0] - 1.*pp_zp_ll_zl_yl[0]*pow(pp_zp_ll_zl_yl[1],2));
 
 return result;
 } 


Cdoub sp_43 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = sqrt(pp_zp_ll_zl_yl[2])*Mpi*pp_zp_ll_zl_yl[3];
 
 return result;
 } 


Cdoub sp_44 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi){

 Cdoub result = pp_zp_ll_zl_yl[2];
 
 return result;
 } 


