//
// Created by esther on 25.10.17.
//

#ifndef CALC_VMESON_SCALARP_H
#define CALC_VMESON_SCALARP_H


#include <array>
#include <typedefs.h>
#include <Phasespace.h>
#include <routing_scalar_products.h>
#include <cheb.h>
#include "Routing/dependence_of_all_kernels.h"

using namespace std;

template <size_t Scalar, size_t Spherical> array<Cdoub, Scalar> get_scalar_prod(const array<int, Spherical>& int_of_sphericals,
                                                                                const array<int, Spherical>& depends_on_sphericals,
                                                                                const array<int, Scalar> &which_scalar_prod, const Bse_ps &ps)
{
    array<Cdoub, Scalar> res;

    ArrayBsePhaseSpaceDoub doubOfSphericals = ps.get_phase_space_doubles(int_of_sphericals, depends_on_sphericals);

    for (int i = 0; i < Scalar; ++i) {
        res[i] =  (*AllSps[ which_scalar_prod[i] ])(doubOfSphericals, sqrt(ps.getPsquared()));
//        cout<<res[i]<<endl;
    }


    return res;

};

template <size_t Scalar, size_t Spherical> void get_all_scalar_prod(const array<int, Spherical>& int_of_sphericals,
                                                                const array<int, Spherical>& depends_on_sphericals,
                                                                const array<int, Scalar> &which_scalar_prod, const Bse_ps &ps,
                                                                ArrayScalarProducts& scalar_p                )
{

    ArrayBsePhaseSpaceDoub doubOfSphericals = ps.get_phase_space_doubles(int_of_sphericals, depends_on_sphericals);

    for (int i = 0; i < Scalar; ++i) {
        scalar_p[ which_scalar_prod[i]] =  (*AllSps[ which_scalar_prod[i] ])(doubOfSphericals, sqrt(ps.getPsquared()));
//        cout<<scalar_p[ which_scalar_prod[i]]<<endl;
    }

};

template <size_t Scalar, size_t Spherical> void get_all_scalar_prod(const array<int, Spherical>& int_of_sphericals,
                                                                    const array<int, Spherical>& depends_on_sphericals,
                                                                    const array<int, Scalar> &which_scalar_prod, const Bse_ps &ps,
                                                                    ArrayScalarProducts& scalar_p ,
                                                                    double mass)
{

    ArrayBsePhaseSpaceDoub doubOfSphericals = ps.get_phase_space_doubles(int_of_sphericals, depends_on_sphericals);

    for (int i = 0; i < Scalar; ++i) {
        scalar_p[ which_scalar_prod[i]] =  (*AllSps[ which_scalar_prod[i] ])(doubOfSphericals, i_*mass);
//        cout<<res[i]<<endl;
    }

};

template <size_t Scalar, size_t Spherical> void get_all_scalar_prod_cheb(const array<int, Spherical>& int_of_sphericals,
                                                                    const array<int, Spherical>& depends_on_sphericals,
                                                                    const array<int, Scalar> &which_scalar_prod, const Bse_ps &ps,
                                                                    ArrayScalarProducts& scalar_p)
{

    //Here we are passing the Chebychev index such that we get the double values with z_p =  z_cheb!.
    ArrayBsePhaseSpaceDoub doubOfSphericals = ps.get_phase_space_doubles_cheb(int_of_sphericals, depends_on_sphericals);


    for (int i = 0; i < Scalar; ++i) {
        scalar_p[ which_scalar_prod[i]] =  (*AllSps[ which_scalar_prod[i] ])(doubOfSphericals, sqrt(ps.getPsquared()));
    }





};

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

template <int NumberOfSphericals, int NScalarProducts> class scalarP {

public:

    scalarP(const array<int, NScalarProducts>& depends_on_scalar_prod, const array<int, NumberOfSphericals>& depends_on_sphericals,
    Bse_ps& ps): which_scalar_prod(depends_on_scalar_prod),
                 depends_on_sphericals(depends_on_sphericals),
                 ps(ps){
    };

    array<Cdoub, NScalarProducts> map ( const array<int, NumberOfSphericals>& int_of_sphericals) const {

/*        array<Cdoub, NScalarProducts> res;

        ArrayBsePhaseSpaceDoub doubOfSphericals = ps.get_phase_space_doubles(int_of_sphericals, depends_on_sphericals);
        for( auto in : which_scalar_prod){
            res[in] =  (*AllSps[ in ])(doubOfSphericals, sqrt(ps.getPhaseSpacePsquared()));
        }

        return res;*/

        return get_scalar_prod(int_of_sphericals, depends_on_sphericals, which_scalar_prod, ps);
    };

    Bse_ps ps;

    const array<int, NScalarProducts> &getWhich_scalar_prod() const {
        return which_scalar_prod;
    }

private:

    const array<int, NScalarProducts> which_scalar_prod;
    const array<int, NumberOfSphericals> depends_on_sphericals;

};



#endif //CALC_VMESON_SCALARP_H
