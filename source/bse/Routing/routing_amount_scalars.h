#ifndef CALC_BSE_ROUTING_AMOUNT_SCALARS_H
#define CALC_BSE_ROUTING_AMOUNT_SCALARS_H  
 

#include <typedefs.h>
#include <array>

//important: Check if this didnt destroy thing: I switched to complex double in the definition of the array here!!

typedef array<Cdoub, 5> ArrayBsePhaseSpaceDoub;
//typedef array<double, 5> ArrayBsePhaseSpaceDoub;
typedef array<Cdoub, 45> ArrayScalarProducts; //={NAN};

typedef Cdoub (*FunPtrForScalarProducts)( const ArrayBsePhaseSpaceDoub & , Cdoub );
typedef vector< FunPtrForScalarProducts> CollectionOfSps;

typedef Cdoub (*FunPtrForScalarProductsEta)( const ArrayBsePhaseSpaceDoub & , Cdoub , double);
typedef vector< FunPtrForScalarProductsEta> EtaCollectionOfSps;


#endif //CALC_BSE_ROUTING_AMOUNT_SCALARS_H 

// sp[0] = gmugmu
// sp[1] = gmup1mu
// sp[2] = p1mup1mu
// sp[3] = gmup1barmu
// sp[4] = p1mup1barmu
// sp[5] = p1barmup1barmu
// sp[6] = gmup2mu
// sp[7] = p1mup2mu
// sp[8] = p1barmup2mu
// sp[9] = p2mup2mu
// sp[10] = gmup2barmu
// sp[11] = p1mup2barmu
// sp[12] = p1barmup2barmu
// sp[13] = p2mup2barmu
// sp[14] = p2barmup2barmu
// sp[15] = gmupbarmu
// sp[16] = p1mupbarmu
// sp[17] = p1barmupbarmu
// sp[18] = p2mupbarmu
// sp[19] = p2barmupbarmu
// sp[20] = pbarmupbarmu
// sp[21] = gmupmu
// sp[22] = p1mupmu
// sp[23] = p1barmupmu
// sp[24] = p2mupmu
// sp[25] = p2barmupmu
// sp[26] = pbarmupmu
// sp[27] = pmupmu
// sp[28] = gmuPmu
// sp[29] = p1muPmu
// sp[30] = p1barmuPmu
// sp[31] = p2muPmu
// sp[32] = p2barmuPmu
// sp[33] = pbarmuPmu
// sp[34] = pmuPmu
// sp[35] = PmuPmu
// sp[36] = gmulmu
// sp[37] = p1mulmu
// sp[38] = p1barmulmu
// sp[39] = p2mulmu
// sp[40] = p2barmulmu
// sp[41] = pbarmulmu
// sp[42] = pmulmu
// sp[43] = Pmulmu
// sp[44] = lmulmu
