#pragma once 
 
 #include <Phasespace.h> 
 #include "typedefs.h" 
 
////{pp -> pp_zp_ll_zl_yl[0], zp -> pp_zp_ll_zl_yl[1], ll -> pp_zp_ll_zl_yl[2], zl -> pp_zp_ll_zl_yl[3], yl -> pp_zp_ll_zl_yl[4]}



 Cdoub sp_0 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// gmugmu
Cdoub sp_1 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// gmup1mu
Cdoub sp_2 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p1mup1mu
Cdoub sp_3 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// gmup1barmu
Cdoub sp_4 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p1mup1barmu
Cdoub sp_5 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p1barmup1barmu
Cdoub sp_6 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// gmup2mu
Cdoub sp_7 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p1mup2mu
Cdoub sp_8 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p1barmup2mu
Cdoub sp_9 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p2mup2mu
Cdoub sp_10 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// gmup2barmu
Cdoub sp_11 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p1mup2barmu
Cdoub sp_12 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p1barmup2barmu
Cdoub sp_13 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p2mup2barmu
Cdoub sp_14 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p2barmup2barmu
Cdoub sp_15 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// gmupbarmu
Cdoub sp_16 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p1mupbarmu
Cdoub sp_17 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p1barmupbarmu
Cdoub sp_18 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p2mupbarmu
Cdoub sp_19 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p2barmupbarmu
Cdoub sp_20 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// pbarmupbarmu
Cdoub sp_21 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// gmupmu
Cdoub sp_22 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p1mupmu
Cdoub sp_23 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p1barmupmu
Cdoub sp_24 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p2mupmu
Cdoub sp_25 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p2barmupmu
Cdoub sp_26 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// pbarmupmu
Cdoub sp_27 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// pmupmu
Cdoub sp_28 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// gmuPmu
Cdoub sp_29 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p1muPmu
Cdoub sp_30 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p1barmuPmu
Cdoub sp_31 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p2muPmu
Cdoub sp_32 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p2barmuPmu
Cdoub sp_33 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// pbarmuPmu
Cdoub sp_34 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// pmuPmu
Cdoub sp_35 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// PmuPmu
Cdoub sp_36 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// gmulmu
Cdoub sp_37 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p1mulmu
Cdoub sp_38 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p1barmulmu
Cdoub sp_39 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p2mulmu
Cdoub sp_40 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// p2barmulmu
Cdoub sp_41 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// pbarmulmu
Cdoub sp_42 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// pmulmu
Cdoub sp_43 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// Pmulmu
Cdoub sp_44 (const ArrayBsePhaseSpaceDoub &pp_zp_ll_zl_yl, Cdoub Mpi, double eta); //// lmulmu


 ///// {gmugmu, gmup1mu, p1mup1mu, gmup1barmu, p1mup1barmu, p1barmup1barmu, gmup2mu, p1mup2mu, p1barmup2mu, p2mup2mu, gmup2barmu, p1mup2barmu, p1barmup2barmu, p2mup2barmu, p2barmup2barmu, gmupbarmu, p1mupbarmu, p1barmupbarmu, p2mupbarmu, p2barmupbarmu, pbarmupbarmu, gmupmu, p1mupmu, p1barmupmu, p2mupmu, p2barmupmu, pbarmupmu, pmupmu, gmuPmu, p1muPmu, p1barmuPmu, p2muPmu, p2barmuPmu, pbarmuPmu, pmuPmu, PmuPmu, gmulmu, p1mulmu, p1barmulmu, p2mulmu, p2barmulmu, pbarmulmu, pmulmu, Pmulmu, lmulmu}


const EtaCollectionOfSps AllSps={sp_0, sp_1, sp_2, sp_3, sp_4, sp_5, sp_6, sp_7, sp_8, sp_9, sp_10, sp_11, sp_12, sp_13, sp_14, sp_15, sp_16, sp_17, sp_18, sp_19, sp_20, sp_21, sp_22, sp_23, sp_24, sp_25, sp_26, sp_27, sp_28, sp_29, sp_30, sp_31, sp_32, sp_33, sp_34, sp_35, sp_36, sp_37, sp_38, sp_39, sp_40, sp_41, sp_42, sp_43, sp_44};