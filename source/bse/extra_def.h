//
// Created by esther on 08.11.17.
//

#ifndef CALC_VMESON_EXTRA_DEF_H
#define CALC_VMESON_EXTRA_DEF_H


#include <typedefs.h>


//Program related definitions

//#define K_PSCALAR_MESON
#define K_VECTOR_MESON
//#define K_NOT_USING_INPUT_FILE

//Amount of precalcuations in the angle to the order of #
#define K_ORDER_OF_ANGLE                    4



//////////////////////// Debugging ///////////////
//either one of these flags has to be definded.
//#define L_CALC_ANGLE
// or choose
//#define L_CALC_ALL

#define K_N_DEFINED_IN_MAIN
//#define K_N_NOT_IN_MAIN



#endif //CALC_VMESON_EXTRA_DEF_H
