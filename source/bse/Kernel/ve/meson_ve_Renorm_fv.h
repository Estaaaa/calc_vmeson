#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>

void normalisation_notopti_ve(matCdoub& kern,
                              Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
                              Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
                              const Cdoub& svp, const Cdoub& ssp,
                              const Cdoub& svm, const Cdoub& ssm ){




    kern[0][0] =  - 1.2E+1*ssp*ssm - 8.E+0*pow(P_P,-1)*P_qm*P_qp*svp*
                                     svm - 4.E+0*qm_qp*svp*svm;

    kern[0][1] = 1.2E+1*P_qm*ssp*svm - 1.2E+1*P_qp*svp*ssm;

    kern[0][2] =  - 4.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*ssp*svm + 4.E+0*
                                                                pow(P_P,-1)*pow(P_l,2)*P_qp*svp*ssm - 8.E+0*P_l*l_qm*ssp*svm +
                  8.E+0*P_l*l_qp*svp*ssm;

    kern[0][3] = 1.6E+1*P_qm*l_qp*svp*svm - 1.6E+1*P_qp*l_qm*svp*svm;

    kern[0][4] =  - 4.E+0*pow(P_P,-1)*P_l*P_qm*ssp*svm - 4.E+0*pow(
            P_P,-1)*P_l*P_qp*svp*ssm + 4.E+0*l_qm*ssp*svm + 4.E+0*l_qp*svp*
                                                            ssm;

    kern[0][5] =  - 8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*P_qp*svp*svm +
                  4.E+0*P_l*P_qm*l_qp*svp*svm + 4.E+0*P_l*P_qp*l_qm*svp*svm;

    kern[0][6] = 4.E+0*pow(P_P,-1)*P_l*P_qm*l_qp*svp*svm + 4.E+0*pow(
            P_P,-1)*P_l*P_qp*l_qm*svp*svm - 4.E+0*pow(P_P,-1)*pow(P_l,2)*
                                            qm_qp*svp*svm - 4.E+0*pow(P_P,-1)*pow(P_l,2)*ssp*ssm + 4.E+0*l_l*
                                                                                                   qm_qp*svp*svm + 4.E+0*l_l*ssp*ssm - 8.E+0*l_qm*l_qp*svp*svm;

    kern[0][7] = 8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*ssp*svm - 8.E+0*pow(
            P_P,-1)*pow(P_l,2)*P_qp*svp*ssm - 8.E+0*P_qm*l_l*ssp*svm + 8.E+0*
                                                                       P_qp*l_l*svp*ssm;


    kern[1][0] = 1.2E+1*P_qm*ssp*svm - 1.2E+1*P_qp*svp*ssm;

    kern[1][1] =  - 4.E+0*P_P*qm_qp*svp*svm + 1.2E+1*P_P*ssp*ssm +
                  1.6E+1*P_qm*P_qp*svp*svm;

    kern[1][2] =  - 8.E+0*P_l*P_qm*l_qp*svp*svm - 8.E+0*P_l*P_qp*l_qm*
                                                  svp*svm + 4.E+0*pow(P_l,2)*qm_qp*svp*svm - 1.2E+1*pow(P_l,2)*ssp*
                                                                                             ssm;

    kern[1][3] = 1.6E+1*P_P*l_qm*ssp*svm + 1.6E+1*P_P*l_qp*svp*ssm -
                 1.6E+1*P_l*P_qm*ssp*svm - 1.6E+1*P_l*P_qp*svp*ssm;

    kern[1][4] =  - 4.E+0*P_qm*l_qp*svp*svm + 4.E+0*P_qp*l_qm*svp*svm;

    kern[1][5] =  - 4.E+0*P_P*P_l*l_qm*ssp*svm + 4.E+0*P_P*P_l*l_qp*svp
                                                 *ssm + 4.E+0*pow(P_l,2)*P_qm*ssp*svm - 4.E+0*pow(P_l,2)*P_qp*svp*
                                                                                        ssm;

    kern[1][6] = 4.E+0*P_l*l_qm*ssp*svm - 4.E+0*P_l*l_qp*svp*ssm -
                 4.E+0*P_qm*l_l*ssp*svm + 4.E+0*P_qp*l_l*svp*ssm;

    kern[1][7] = 8.E+0*P_P*l_l*qm_qp*svp*svm - 8.E+0*P_P*l_l*ssp*ssm -
                 1.6E+1*P_P*l_qm*l_qp*svp*svm + 1.6E+1*P_l*P_qm*l_qp*svp*svm +
                 1.6E+1*P_l*P_qp*l_qm*svp*svm - 8.E+0*pow(P_l,2)*qm_qp*svp*svm +
                 8.E+0*pow(P_l,2)*ssp*ssm - 1.6E+1*P_qm*P_qp*l_l*svp*svm;


    kern[2][0] =  - 4.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*ssp*svm + 4.E+0*
                                                                pow(P_P,-1)*pow(P_l,2)*P_qp*svp*ssm - 8.E+0*P_l*l_qm*ssp*svm +
                  8.E+0*P_l*l_qp*svp*ssm;

    kern[2][1] =  - 8.E+0*P_l*P_qm*l_qp*svp*svm - 8.E+0*P_l*P_qp*l_qm*
                                                  svp*svm + 4.E+0*pow(P_l,2)*qm_qp*svp*svm - 1.2E+1*pow(P_l,2)*ssp*
                                                                                             ssm;

    kern[2][2] =  - 8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*P_qp*l_l*svp*svm
                  + 8.E+0*pow(P_P,-1)*pow(P_l,3)*P_qm*l_qp*svp*svm + 8.E+0*pow(
            P_P,-1)*pow(P_l,3)*P_qp*l_qm*svp*svm - 4.E+0*pow(P_P,-1)*pow(
            P_l,4)*qm_qp*svp*svm + 4.E+0*pow(P_P,-1)*pow(P_l,4)*ssp*ssm +
                  8.E+0*pow(P_l,2)*l_l*ssp*ssm + 8.E+0*pow(P_l,2)*l_qm*l_qp*svp*svm
            ;

    kern[2][3] = 1.6E+1*P_l*P_qm*l_l*ssp*svm + 1.6E+1*P_l*P_qp*l_l*svp*
                                               ssm - 1.6E+1*pow(P_l,2)*l_qm*ssp*svm - 1.6E+1*pow(P_l,2)*l_qp*svp
                                                                                      *ssm;

    kern[2][4] = 4.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*l_qp*svp*svm - 4.E+0
                                                                  *pow(P_P,-1)*pow(P_l,2)*P_qp*l_qm*svp*svm;

    kern[2][5] =  - 4.E+0*pow(P_P,-1)*pow(P_l,4)*P_qm*ssp*svm + 4.E+0*
                                                                pow(P_P,-1)*pow(P_l,4)*P_qp*svp*ssm + 4.E+0*pow(P_l,3)*l_qm*ssp*
                                                                                                      svm - 4.E+0*pow(P_l,3)*l_qp*svp*ssm;

    kern[2][6] = 4.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*l_l*ssp*svm - 4.E+0*
                                                                 pow(P_P,-1)*pow(P_l,2)*P_qp*l_l*svp*ssm - 4.E+0*pow(P_P,-1)*pow(
            P_l,3)*l_qm*ssp*svm + 4.E+0*pow(P_P,-1)*pow(P_l,3)*l_qp*svp*ssm;

    kern[2][7] = 1.6E+1*pow(P_P,-1)*pow(P_l,2)*P_qm*P_qp*l_l*svp*svm -
                 1.6E+1*pow(P_P,-1)*pow(P_l,3)*P_qm*l_qp*svp*svm - 1.6E+1*pow(
            P_P,-1)*pow(P_l,3)*P_qp*l_qm*svp*svm + 8.E+0*pow(P_P,-1)*pow(
            P_l,4)*qm_qp*svp*svm - 8.E+0*pow(P_P,-1)*pow(P_l,4)*ssp*ssm -
                 8.E+0*pow(P_l,2)*l_l*qm_qp*svp*svm + 8.E+0*pow(P_l,2)*l_l*ssp*ssm
                 + 1.6E+1*pow(P_l,2)*l_qm*l_qp*svp*svm;


    kern[3][0] = 1.6E+1*P_qm*l_qp*svp*svm - 1.6E+1*P_qp*l_qm*svp*svm;

    kern[3][1] = 1.6E+1*P_P*l_qm*ssp*svm + 1.6E+1*P_P*l_qp*svp*ssm -
                 1.6E+1*P_l*P_qm*ssp*svm - 1.6E+1*P_l*P_qp*svp*ssm;

    kern[3][2] = 1.6E+1*P_l*P_qm*l_l*ssp*svm + 1.6E+1*P_l*P_qp*l_l*svp*
                                               ssm - 1.6E+1*pow(P_l,2)*l_qm*ssp*svm - 1.6E+1*pow(P_l,2)*l_qp*svp
                                                                                      *ssm;

    kern[3][3] =  - 3.2E+1*P_P*l_l*ssp*ssm + 3.2E+1*P_P*l_qm*l_qp*svp*
                                             svm - 3.2E+1*P_l*P_qm*l_qp*svp*svm - 3.2E+1*P_l*P_qp*l_qm*svp*svm
                  + 3.2E+1*pow(P_l,2)*ssp*ssm + 3.2E+1*P_qm*P_qp*l_l*svp*svm;

    kern[3][4] =  0;

    kern[3][5] =  0;

    kern[3][6] =  0;

    kern[3][7] =  0;


    kern[4][0] =  - 4.E+0*pow(P_P,-1)*P_l*P_qm*ssp*svm - 4.E+0*pow(
            P_P,-1)*P_l*P_qp*svp*ssm + 4.E+0*l_qm*ssp*svm + 4.E+0*l_qp*svp*
                                                            ssm;

    kern[4][1] =  - 4.E+0*P_qm*l_qp*svp*svm + 4.E+0*P_qp*l_qm*svp*svm;

    kern[4][2] = 4.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*l_qp*svp*svm - 4.E+0
                                                                  *pow(P_P,-1)*pow(P_l,2)*P_qp*l_qm*svp*svm;

    kern[4][3] =  0;

    kern[4][4] = 4.E+0*pow(P_P,-1)*pow(P_l,2)*qm_qp*svp*svm - 4.E+0*
                                                              pow(P_P,-1)*pow(P_l,2)*ssp*ssm - 4.E+0*l_l*qm_qp*svp*svm + 4.E+0*
                                                                                                                         l_l*ssp*ssm;

    kern[4][5] =  - 4.E+0*pow(P_P,-1)*pow(P_l,3)*P_qm*ssp*svm - 4.E+0*
                                                                pow(P_P,-1)*pow(P_l,3)*P_qp*svp*ssm + 4.E+0*P_l*P_qm*l_l*ssp*svm
                  + 4.E+0*P_l*P_qp*l_l*svp*ssm;

    kern[4][6] = 4.E+0*pow(P_P,-1)*pow(P_l,2)*l_qm*ssp*svm + 4.E+0*pow(
            P_P,-1)*pow(P_l,2)*l_qp*svp*ssm - 4.E+0*l_l*l_qm*ssp*svm - 4.E+0*
                                                                       l_l*l_qp*svp*ssm;

    kern[4][7] =  - 8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*l_qp*svp*svm +
                  8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qp*l_qm*svp*svm + 8.E+0*P_qm*l_l*
                                                                   l_qp*svp*svm - 8.E+0*P_qp*l_l*l_qm*svp*svm;


    kern[5][0] =  - 8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*P_qp*svp*svm +
                  4.E+0*P_l*P_qm*l_qp*svp*svm + 4.E+0*P_l*P_qp*l_qm*svp*svm;

    kern[5][1] =  - 4.E+0*P_P*P_l*l_qm*ssp*svm + 4.E+0*P_P*P_l*l_qp*svp
                                                 *ssm + 4.E+0*pow(P_l,2)*P_qm*ssp*svm - 4.E+0*pow(P_l,2)*P_qp*svp*
                                                                                        ssm;

    kern[5][2] =  - 4.E+0*pow(P_P,-1)*pow(P_l,4)*P_qm*ssp*svm + 4.E+0*
                                                                pow(P_P,-1)*pow(P_l,4)*P_qp*svp*ssm + 4.E+0*pow(P_l,3)*l_qm*ssp*
                                                                                                      svm - 4.E+0*pow(P_l,3)*l_qp*svp*ssm;

    kern[5][3] =  0;

    kern[5][4] =  - 4.E+0*pow(P_P,-1)*pow(P_l,3)*P_qm*ssp*svm - 4.E+0*
                                                                pow(P_P,-1)*pow(P_l,3)*P_qp*svp*ssm + 4.E+0*P_l*P_qm*l_l*ssp*svm
                  + 4.E+0*P_l*P_qp*l_l*svp*ssm;

    kern[5][5] =  - 8.E+0*pow(P_P,-1)*pow(P_l,4)*P_qm*P_qp*svp*svm -
                  4.E+0*P_P*pow(P_l,2)*l_l*qm_qp*svp*svm - 4.E+0*P_P*pow(P_l,2)*l_l
                                                           *ssp*ssm + 8.E+0*pow(P_l,2)*P_qm*P_qp*l_l*svp*svm + 4.E+0*pow(
            P_l,4)*qm_qp*svp*svm + 4.E+0*pow(P_l,4)*ssp*ssm;

    kern[5][6] = 4.E+0*pow(P_P,-1)*pow(P_l,3)*P_qm*l_qp*svp*svm + 4.E+0
                                                                  *pow(P_P,-1)*pow(P_l,3)*P_qp*l_qm*svp*svm - 4.E+0*pow(P_P,-1)*
                                                                                                              pow(P_l,4)*qm_qp*svp*svm - 4.E+0*pow(P_P,-1)*pow(P_l,4)*ssp*ssm
                 - 4.E+0*P_l*P_qm*l_l*l_qp*svp*svm - 4.E+0*P_l*P_qp*l_l*l_qm*svp*
                                                     svm + 4.E+0*pow(P_l,2)*l_l*qm_qp*svp*svm + 4.E+0*pow(P_l,2)*l_l*
                                                                                                ssp*ssm;

    kern[5][7] = 8.E+0*pow(P_P,-1)*pow(P_l,4)*P_qm*ssp*svm - 8.E+0*pow(
            P_P,-1)*pow(P_l,4)*P_qp*svp*ssm + 8.E+0*P_P*P_l*l_l*l_qm*ssp*svm
                 - 8.E+0*P_P*P_l*l_l*l_qp*svp*ssm - 8.E+0*pow(P_l,2)*P_qm*l_l*ssp
                                                    *svm + 8.E+0*pow(P_l,2)*P_qp*l_l*svp*ssm - 8.E+0*pow(P_l,3)*l_qm*
                                                                                               ssp*svm + 8.E+0*pow(P_l,3)*l_qp*svp*ssm;


    kern[6][0] = 4.E+0*pow(P_P,-1)*P_l*P_qm*l_qp*svp*svm + 4.E+0*pow(
            P_P,-1)*P_l*P_qp*l_qm*svp*svm - 4.E+0*pow(P_P,-1)*pow(P_l,2)*
                                            qm_qp*svp*svm - 4.E+0*pow(P_P,-1)*pow(P_l,2)*ssp*ssm + 4.E+0*l_l*
                                                                                                   qm_qp*svp*svm + 4.E+0*l_l*ssp*ssm - 8.E+0*l_qm*l_qp*svp*svm;

    kern[6][1] = 4.E+0*P_l*l_qm*ssp*svm - 4.E+0*P_l*l_qp*svp*ssm -
                 4.E+0*P_qm*l_l*ssp*svm + 4.E+0*P_qp*l_l*svp*ssm;

    kern[6][2] = 4.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*l_l*ssp*svm - 4.E+0*
                                                                 pow(P_P,-1)*pow(P_l,2)*P_qp*l_l*svp*ssm - 4.E+0*pow(P_P,-1)*pow(
            P_l,3)*l_qm*ssp*svm + 4.E+0*pow(P_P,-1)*pow(P_l,3)*l_qp*svp*ssm;

    kern[6][3] =  0;

    kern[6][4] = 4.E+0*pow(P_P,-1)*pow(P_l,2)*l_qm*ssp*svm + 4.E+0*pow(
            P_P,-1)*pow(P_l,2)*l_qp*svp*ssm - 4.E+0*l_l*l_qm*ssp*svm - 4.E+0*
                                                                       l_l*l_qp*svp*ssm;

    kern[6][5] = 4.E+0*pow(P_P,-1)*pow(P_l,3)*P_qm*l_qp*svp*svm + 4.E+0
                                                                  *pow(P_P,-1)*pow(P_l,3)*P_qp*l_qm*svp*svm - 4.E+0*pow(P_P,-1)*
                                                                                                              pow(P_l,4)*qm_qp*svp*svm - 4.E+0*pow(P_P,-1)*pow(P_l,4)*ssp*ssm
                 - 4.E+0*P_l*P_qm*l_l*l_qp*svp*svm - 4.E+0*P_l*P_qp*l_l*l_qm*svp*
                                                     svm + 4.E+0*pow(P_l,2)*l_l*qm_qp*svp*svm + 4.E+0*pow(P_l,2)*l_l*
                                                                                                ssp*ssm;

    kern[6][6] = 4.E+0*pow(P_P,-1)*pow(P_l,2)*l_l*qm_qp*svp*svm + 4.E+0
                                                                  *pow(P_P,-1)*pow(P_l,2)*l_l*ssp*ssm - 8.E+0*pow(P_P,-1)*pow(
            P_l,2)*l_qm*l_qp*svp*svm + 8.E+0*l_l*l_qm*l_qp*svp*svm - 4.E+0*
                                                                     pow(l_l,2)*qm_qp*svp*svm - 4.E+0*pow(l_l,2)*ssp*ssm;

    kern[6][7] =  - 8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*l_l*ssp*svm +
                  8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qp*l_l*svp*ssm + 8.E+0*pow(P_P,-1)
                                                                  *pow(P_l,3)*l_qm*ssp*svm - 8.E+0*pow(P_P,-1)*pow(P_l,3)*l_qp*svp*
                                                                                             ssm - 8.E+0*P_l*l_l*l_qm*ssp*svm + 8.E+0*P_l*l_l*l_qp*svp*ssm +
                  8.E+0*P_qm*pow(l_l,2)*ssp*svm - 8.E+0*P_qp*pow(l_l,2)*svp*ssm;


    kern[7][0] = 8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*ssp*svm - 8.E+0*pow(
            P_P,-1)*pow(P_l,2)*P_qp*svp*ssm - 8.E+0*P_qm*l_l*ssp*svm + 8.E+0*
                                                                       P_qp*l_l*svp*ssm;

    kern[7][1] = 8.E+0*P_P*l_l*qm_qp*svp*svm - 8.E+0*P_P*l_l*ssp*ssm -
                 1.6E+1*P_P*l_qm*l_qp*svp*svm + 1.6E+1*P_l*P_qm*l_qp*svp*svm +
                 1.6E+1*P_l*P_qp*l_qm*svp*svm - 8.E+0*pow(P_l,2)*qm_qp*svp*svm +
                 8.E+0*pow(P_l,2)*ssp*ssm - 1.6E+1*P_qm*P_qp*l_l*svp*svm;

    kern[7][2] = 1.6E+1*pow(P_P,-1)*pow(P_l,2)*P_qm*P_qp*l_l*svp*svm -
                 1.6E+1*pow(P_P,-1)*pow(P_l,3)*P_qm*l_qp*svp*svm - 1.6E+1*pow(
            P_P,-1)*pow(P_l,3)*P_qp*l_qm*svp*svm + 8.E+0*pow(P_P,-1)*pow(
            P_l,4)*qm_qp*svp*svm - 8.E+0*pow(P_P,-1)*pow(P_l,4)*ssp*ssm -
                 8.E+0*pow(P_l,2)*l_l*qm_qp*svp*svm + 8.E+0*pow(P_l,2)*l_l*ssp*ssm
                 + 1.6E+1*pow(P_l,2)*l_qm*l_qp*svp*svm;

    kern[7][3] =  0;

    kern[7][4] =  - 8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*l_qp*svp*svm +
                  8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qp*l_qm*svp*svm + 8.E+0*P_qm*l_l*
                                                                   l_qp*svp*svm - 8.E+0*P_qp*l_l*l_qm*svp*svm;

    kern[7][5] = 8.E+0*pow(P_P,-1)*pow(P_l,4)*P_qm*ssp*svm - 8.E+0*pow(
            P_P,-1)*pow(P_l,4)*P_qp*svp*ssm + 8.E+0*P_P*P_l*l_l*l_qm*ssp*svm
                 - 8.E+0*P_P*P_l*l_l*l_qp*svp*ssm - 8.E+0*pow(P_l,2)*P_qm*l_l*ssp
                                                    *svm + 8.E+0*pow(P_l,2)*P_qp*l_l*svp*ssm - 8.E+0*pow(P_l,3)*l_qm*
                                                                                               ssp*svm + 8.E+0*pow(P_l,3)*l_qp*svp*ssm;

    kern[7][6] =  - 8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*l_l*ssp*svm +
                  8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qp*l_l*svp*ssm + 8.E+0*pow(P_P,-1)
                                                                  *pow(P_l,3)*l_qm*ssp*svm - 8.E+0*pow(P_P,-1)*pow(P_l,3)*l_qp*svp*
                                                                                             ssm - 8.E+0*P_l*l_l*l_qm*ssp*svm + 8.E+0*P_l*l_l*l_qp*svp*ssm +
                  8.E+0*P_qm*pow(l_l,2)*ssp*svm - 8.E+0*P_qp*pow(l_l,2)*svp*ssm;

    kern[7][7] =  - 3.2E+1*pow(P_P,-1)*pow(P_l,2)*P_qm*P_qp*l_l*svp*svm
                  + 3.2E+1*pow(P_P,-1)*pow(P_l,3)*P_qm*l_qp*svp*svm + 3.2E+1*pow(
            P_P,-1)*pow(P_l,3)*P_qp*l_qm*svp*svm - 1.6E+1*pow(P_P,-1)*pow(
            P_l,4)*qm_qp*svp*svm + 1.6E+1*pow(P_P,-1)*pow(P_l,4)*ssp*ssm +
                  3.2E+1*P_P*l_l*l_qm*l_qp*svp*svm - 1.6E+1*P_P*pow(l_l,2)*qm_qp*
                                                     svp*svm + 1.6E+1*P_P*pow(l_l,2)*ssp*ssm - 3.2E+1*P_l*P_qm*l_l*
                                                                                               l_qp*svp*svm - 3.2E+1*P_l*P_qp*l_l*l_qm*svp*svm + 3.2E+1*pow(
            P_l,2)*l_l*qm_qp*svp*svm - 3.2E+1*pow(P_l,2)*l_l*ssp*ssm - 3.2E+1
                                                                       *pow(P_l,2)*l_qm*l_qp*svp*svm + 3.2E+1*P_qm*P_qp*pow(l_l,2)*svp*
                                                                                                       svm;


}

void normalisation_ve(matCdoub& kern,
                      Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
                      Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
                      const Cdoub& svp, const Cdoub& ssp,
                      const Cdoub& svm, const Cdoub& ssm ){


    // local variables
    array<Cdoub, 61> w;


    w[1]=pow(P_P,-1);
    w[2]=P_qm;
    w[3]=P_qp;
    w[4]=qm_qp;
    w[5]=P_l;
    w[6]=l_qm;
    w[7]=l_qp;
    w[8]=l_l;
    w[9]=P_P;
    w[10]=ssp*ssm;
    w[11]=3.E+0*w[10];
    w[12]=svp*svm;
    w[13]=w[12]*w[4];
    w[14]=w[12]*w[3];
    w[15]=w[14]*w[2];
    w[16]=2.E+0*w[15];
    w[17]= - w[1]*w[16];
    w[17]=w[17] - w[13] - w[11];
    w[17]=4.E+0*w[17];
    w[18]=ssm*svp;
    w[19]=w[3]*w[18];
    w[20]=ssp*svm;
    w[21]=w[2]*w[20];
    w[22]=w[19] - w[21];
    w[23]=1.2E+1*w[22];
    w[24]=pow(w[5],2);
    w[25]=w[24]*w[1];
    w[26]=w[25]*w[22];
    w[18]=w[7]*w[18];
    w[20]=w[6]*w[20];
    w[27]=w[18] - w[20];
    w[28]=w[27]*w[5];
    w[29]=w[26] + 2.E+0*w[28];
    w[29]=4.E+0*w[29];
    w[14]=w[14]*w[6];
    w[12]=w[12]*w[7];
    w[30]=w[2]*w[12];
    w[31]=w[14] - w[30];
    w[32]=1.6E+1*w[31];
    w[19]=w[19] + w[21];
    w[21]=w[19]*w[1];
    w[33]=w[21]*w[5];
    w[18]=w[18] + w[20];
    w[20]=w[33] - w[18];
    w[20]=4.E+0*w[20];
    w[14]=w[14] + w[30];
    w[30]=w[14]*w[5];
    w[33]=w[16]*w[25];
    w[30]=w[30] - w[33];
    w[30]=4.E+0*w[30];
    w[33]=w[13] + w[10];
    w[34]=w[33]*w[5];
    w[34]=w[34] - w[14];
    w[35]=w[34]*w[1];
    w[36]=w[35]*w[5];
    w[37]=w[33]*w[8];
    w[12]=w[6]*w[12];
    w[38]=2.E+0*w[12];
    w[37]=w[37] - w[38];
    w[36]=w[36] - w[37];
    w[36]=4.E+0*w[36];
    w[39]=w[22]*w[8];
    w[26]=w[26] - w[39];
    w[26]=8.E+0*w[26];
    w[11]=w[11] - w[13];
    w[40]=w[9]*w[11];
    w[40]=w[40] + 4.E+0*w[15];
    w[40]=4.E+0*w[40];
    w[11]=w[11]*w[5];
    w[41]=2.E+0*w[14];
    w[11]=w[11] + w[41];
    w[42]=4.E+0*w[5];
    w[11]=w[11]*w[42];
    w[43]=w[18]*w[9];
    w[44]=w[19]*w[5];
    w[43]=w[43] - w[44];
    w[43]=1.6E+1*w[43];
    w[44]=4.E+0*w[31];
    w[45]=w[22]*w[5];
    w[46]=w[27]*w[9];
    w[45]=w[45] - w[46];
    w[42]=w[45]*w[42];
    w[28]=w[39] - w[28];
    w[39]=4.E+0*w[28];
    w[13]=w[13] - w[10];
    w[46]=w[13]*w[5];
    w[41]=w[41] - w[46];
    w[47]=w[41]*w[5];
    w[48]=w[13]*w[9];
    w[48]=w[48] - w[16];
    w[48]=w[48]*w[8];
    w[49]=w[38]*w[9];
    w[47]= - w[48] + w[49] - w[47];
    w[47]=8.E+0*w[47];
    w[49]=pow(w[5],3);
    w[41]=w[41]*w[49];
    w[50]=w[24]*w[8];
    w[51]=w[16]*w[50];
    w[41]=w[41] - w[51];
    w[41]=w[41]*w[1];
    w[51]=w[24]*w[12];
    w[52]=w[10]*w[50];
    w[51]=w[51] + w[52];
    w[51]=2.E+0*w[51] + w[41];
    w[51]=4.E+0*w[51];
    w[52]=w[18]*w[24];
    w[53]=w[8]*w[5];
    w[19]=w[53]*w[19];
    w[52]=w[52] - w[19];
    w[52]=1.6E+1*w[52];
    w[54]=w[31]*w[25];
    w[55]=4.E+0*w[54];
    w[56]=pow(w[5],4);
    w[57]=w[56]*w[1];
    w[58]=w[57]*w[22];
    w[27]=w[27]*w[49];
    w[58]=w[58] - w[27];
    w[59]=4.E+0*w[58];
    w[22]=w[50]*w[22];
    w[22]=w[22] - w[27];
    w[22]=w[22]*w[1];
    w[27]=4.E+0*w[22];
    w[24]=w[38]*w[24];
    w[38]=w[41] - w[24];
    w[41]=w[50]*w[13];
    w[41]=w[41] + w[38];
    w[41]=8.E+0*w[41];
    w[60]=w[5]*w[10];
    w[60]=w[60] - w[14];
    w[60]=w[5]*w[60];
    w[10]= - w[9]*w[10];
    w[10]=w[10] + w[15];
    w[10]=w[8]*w[10];
    w[12]=w[12]*w[9];
    w[10]=w[10] + w[12] + w[60];
    w[10]=3.2E+1*w[10];
    w[15]=w[25] - w[8];
    w[13]=4.E+0*w[13]*w[15];
    w[21]=w[21]*w[49];
    w[19]=w[21] - w[19];
    w[19]=4.E+0*w[19];
    w[15]=w[15]*w[18];
    w[15]=4.E+0*w[15];
    w[18]=w[31]*w[8];
    w[18]=w[18] - w[54];
    w[18]=8.E+0*w[18];
    w[21]=w[33]*w[56];
    w[25]= - w[9]*w[33];
    w[25]=w[25] + w[16];
    w[25]=w[25]*w[50];
    w[16]= - w[16]*w[57];
    w[16]=w[16] + w[21] + w[25];
    w[16]=4.E+0*w[16];
    w[21]=w[34]*w[53];
    w[25]=w[35]*w[49];
    w[21]=w[21] - w[25];
    w[21]=4.E+0*w[21];
    w[25]=w[45]*w[53];
    w[25]=w[25] - w[58];
    w[25]=8.E+0*w[25];
    w[31]=w[33]*w[50];
    w[24]= - w[24] + w[31];
    w[24]=w[1]*w[24];
    w[31]= - w[8]*w[37];
    w[24]=w[31] + w[24];
    w[24]=4.E+0*w[24];
    w[28]=w[28]*w[8];
    w[22]=w[28] - w[22];
    w[22]=8.E+0*w[22];
    w[14]=w[46] - w[14];
    w[14]=w[5]*w[14];
    w[12]=w[12] + w[14];
    w[12]=2.E+0*w[12] - w[48];
    w[12]=w[8]*w[12];
    w[12]=w[12] + w[38];
    w[12]=1.6E+1*w[12];


    kern[0][0] = w[17];

    kern[0][1] =  - w[23];

    kern[0][2] = w[29];

    kern[0][3] =  - w[32];

    kern[0][4] =  - w[20];

    kern[0][5] = w[30];

    kern[0][6] =  - w[36];

    kern[0][7] =  - w[26];


    kern[1][0] =  - w[23];

    kern[1][1] = w[40];

    kern[1][2] =  - w[11];

    kern[1][3] = w[43];

    kern[1][4] = w[44];

    kern[1][5] =  - w[42];

    kern[1][6] = w[39];

    kern[1][7] =  - w[47];


    kern[2][0] = w[29];

    kern[2][1] =  - w[11];

    kern[2][2] = w[51];

    kern[2][3] =  - w[52];

    kern[2][4] =  - w[55];

    kern[2][5] = w[59];

    kern[2][6] =  - w[27];

    kern[2][7] =  - w[41];


    kern[3][0] =  - w[32];

    kern[3][1] = w[43];

    kern[3][2] =  - w[52];

    kern[3][3] = w[10];

    kern[3][4] =  0;

    kern[3][5] =  0;

    kern[3][6] =  0;

    kern[3][7] =  0;


    kern[4][0] =  - w[20];

    kern[4][1] = w[44];

    kern[4][2] =  - w[55];

    kern[4][3] =  0;

    kern[4][4] = w[13];

    kern[4][5] =  - w[19];

    kern[4][6] = w[15];

    kern[4][7] =  - w[18];


    kern[5][0] = w[30];

    kern[5][1] =  - w[42];

    kern[5][2] = w[59];

    kern[5][3] =  0;

    kern[5][4] =  - w[19];

    kern[5][5] = w[16];

    kern[5][6] = w[21];

    kern[5][7] = w[25];


    kern[6][0] =  - w[36];

    kern[6][1] = w[39];

    kern[6][2] =  - w[27];

    kern[6][3] =  0;

    kern[6][4] = w[15];

    kern[6][5] = w[21];

    kern[6][6] = w[24];

    kern[6][7] =  - w[22];


    kern[7][0] =  - w[26];

    kern[7][1] =  - w[47];

    kern[7][2] =  - w[41];

    kern[7][3] =  0;

    kern[7][4] =  - w[18];

    kern[7][5] = w[25];

    kern[7][6] =  - w[22];

    kern[7][7] = w[12];


}

