#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>

void normalisation_scalar(matCdoub& kern,
const ArrayScalarProducts& sp,  const Cdoub& svp,
const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


// local variables
array<Cdoub, 27> w;


w[1]=sp[12];
w[2]=sp[33];
w[3]=sp[32];
w[4]=sp[30];
w[5]=sp[19];
w[6]=sp[17];
w[7]=sp[35];
w[8]=sp[20];
w[9]=ssp*ssm;
w[10]=svp*svm;
w[11]=w[10]*w[1];
w[12]=w[9] - w[11];
w[13]=4.E+0*w[12];
w[14]=svp*ssm;
w[15]=w[4]*w[14];
w[16]=ssp*svm;
w[17]=w[3]*w[16];
w[18]=w[15] + w[17];
w[19]=4.E+0*w[2];
w[18]=w[18]*w[19];
w[14]=w[6]*w[14];
w[16]=w[5]*w[16];
w[20]=w[14] + w[16];
w[20]=4.E+0*w[20];
w[21]=w[10]*w[4];
w[22]=w[21]*w[5];
w[10]=w[10]*w[6];
w[23]=w[10]*w[3];
w[24]=w[22] - w[23];
w[24]=8.E+0*w[24];
w[9]=w[9] + w[11];
w[11]=w[7]*w[9];
w[21]=w[21]*w[3];
w[21]=2.E+0*w[21];
w[11]= - w[21] + w[11];
w[25]=pow(w[2],2);
w[11]=4.E+0*w[25]*w[11];
w[26]=w[9]*w[2];
w[22]=w[22] + w[23];
w[23]=w[26] - w[22];
w[19]=w[23]*w[19];
w[14]=w[14] - w[16];
w[14]=w[14]*w[2];
w[16]=w[14]*w[7];
w[15]=w[15] - w[17];
w[17]=w[15]*w[25];
w[16]=w[16] - w[17];
w[16]=8.E+0*w[16];
w[9]=w[8]*w[9];
w[10]=w[10]*w[5];
w[10]=2.E+0*w[10];
w[9]= - w[10] + w[9];
w[9]=4.E+0*w[9];
w[15]=w[15]*w[8];
w[14]=w[15] - w[14];
w[14]=8.E+0*w[14];
w[15]= - w[2]*w[12];
w[15]= - 2.E+0*w[22] + w[15];
w[15]=w[2]*w[15];
w[12]=w[8]*w[12];
w[10]=w[10] + w[12];
w[10]=w[7]*w[10];
w[12]=w[8]*w[21];
w[10]=w[10] + w[15] + w[12];
w[10]=1.6E+1*w[10];


kern[0][0] = w[13];

kern[0][1] =  - w[18];

kern[0][2] =  - w[20];

kern[0][3] =  - w[24];


kern[1][0] = w[18];

kern[1][1] = w[11];

kern[1][2] = w[19];

kern[1][3] =  - w[16];


kern[2][0] = w[20];

kern[2][1] = w[19];

kern[2][2] = w[9];

kern[2][3] = w[14];


kern[3][0] =  - w[24];

kern[3][1] = w[16];

kern[3][2] =  - w[14];

kern[3][3] = w[10];


}
