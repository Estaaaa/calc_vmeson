#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
//#include <math_ops.h>

void kernelL_notopti_scalar(matCdoub& kern,
const ArrayScalarProducts& sp,
const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){


// local variables
Cdoub denom=0.0;


denom = 4.E+0;

kern[0][0] =  1.2E+1*ang[1];

kern[0][0] = (1.0/ denom)* kern[0][0];
kern[0][1] =   0;

kern[0][1] = (1.0/ denom)* kern[0][1];
kern[0][2] =   0;

kern[0][2] = (1.0/ denom)* kern[0][2];
kern[0][3] =   0;

kern[0][3] = (1.0/ denom)* kern[0][3];

denom =  - 4.E+0*sp[35]*sp[34]*sp[27] + 4.E+0*pow(sp[34],3);

kern[1][0] =   0;

kern[1][0] = (1.0/ denom)* kern[1][0];
kern[1][1] =  4.E+0*sp[35]*sp[43]*sp[27]*ang[1] - 1.2E+1*sp[34]*pow(sp[43],2)*sp[27]*
ang[0] - 4.E+0*sp[34]*pow(sp[43],2)*sp[44]*ang[0] + 4.E+0*sp[34]*pow(sp[43],2)*ang[1]
+ 4.E+0*pow(sp[34],2)*sp[43]*sp[27]*ang[0] + 4.E+0*pow(sp[34],2)*sp[43]*sp[44]*ang[0]
- 8.E+0*pow(sp[34],2)*sp[43]*ang[1] + 8.E+0*pow(sp[43],3)*sp[27]*ang[0];

kern[1][1] = (1.0/ denom)* kern[1][1];
kern[1][2] =   - 6.E+0*sp[34]*sp[27]*ang[1] + 2.E+0*sp[34]*pow(sp[27],2)*ang[0] -
2.E+0*sp[34]*sp[44]*ang[1] - 2.E+0*sp[34]*pow(sp[44],2)*ang[0] + 4.E+0*sp[34]*ang[2]
+ 4.E+0*sp[43]*sp[27]*sp[44]*ang[0] + 8.E+0*sp[43]*sp[27]*ang[1] - 4.E+0*sp[43]*pow(
sp[27],2)*ang[0];

kern[1][2] = (1.0/ denom)* kern[1][2];
kern[1][3] =   0;

kern[1][3] = (1.0/ denom)* kern[1][3];

denom = 4.E+0*sp[35]*sp[27] - 4.E+0*pow(sp[34],2);

kern[2][0] =   0;

kern[2][0] = (1.0/ denom)* kern[2][0];
kern[2][1] =   - 4.E+0*sp[35]*sp[34]*sp[43]*sp[27]*ang[0] + 4.E+0*sp[35]*sp[34]*sp[43]*sp[44]
*ang[0] - 4.E+0*sp[35]*sp[34]*sp[43]*ang[1] + 4.E+0*sp[35]*pow(sp[43],2)*sp[27]*ang[0] -
4.E+0*sp[35]*pow(sp[43],2)*sp[44]*ang[0] + 4.E+0*sp[35]*pow(sp[43],2)*ang[1] + 8.E+0
*sp[34]*pow(sp[43],3)*ang[0] - 1.6E+1*pow(sp[34],2)*pow(sp[43],2)*ang[0] + 8.E+0*
pow(sp[34],3)*sp[43]*ang[0];

kern[2][1] = (1.0/ denom)* kern[2][1];
kern[2][2] =  4.E+0*sp[35]*sp[27]*sp[44]*ang[0] - 2.E+0*sp[35]*sp[27]*ang[1] - 2.E+0*
sp[35]*pow(sp[27],2)*ang[0] - 2.E+0*sp[35]*sp[44]*ang[1] - 2.E+0*sp[35]*pow(sp[44],2)*
ang[0] + 4.E+0*sp[35]*ang[2] - 4.E+0*sp[34]*sp[43]*sp[27]*ang[0] + 4.E+0*sp[34]*sp[43]*
sp[44]*ang[0] + 8.E+0*sp[34]*sp[43]*ang[1] + 4.E+0*pow(sp[34],2)*sp[27]*ang[0] - 4.E+0
*pow(sp[34],2)*sp[44]*ang[0] - 4.E+0*pow(sp[34],2)*ang[1];

kern[2][2] = (1.0/ denom)* kern[2][2];
kern[2][3] =   0;

kern[2][3] = (1.0/ denom)* kern[2][3];

denom =  - 1.6E+1*sp[35]*sp[27] + 1.6E+1*pow(sp[34],2);

kern[3][0] =   0;

kern[3][0] = (1.0/ denom)* kern[3][0];
kern[3][1] =   0;

kern[3][1] = (1.0/ denom)* kern[3][1];
kern[3][2] =   0;

kern[3][2] = (1.0/ denom)* kern[3][2];
kern[3][3] =  1.6E+1*sp[35]*sp[27]*sp[44]*ang[0] + 8.E+0*sp[35]*sp[27]*ang[1] - 8.E+0*
sp[35]*pow(sp[27],2)*ang[0] + 8.E+0*sp[35]*sp[44]*ang[1] - 8.E+0*sp[35]*pow(sp[44],2)*
ang[0] + 3.2E+1*sp[34]*sp[43]*sp[27]*ang[0] + 3.2E+1*sp[34]*sp[43]*sp[44]*ang[0] - 1.6E+1
*sp[34]*sp[43]*ang[1] - 3.2E+1*pow(sp[34],2)*sp[44]*ang[0] - 3.2E+1*pow(sp[43],2)*
sp[27]*ang[0];

kern[3][3] = (1.0/ denom)* kern[3][3];

}


void kernelY_noopti_scalar(matCdoub& kern,
const ArrayScalarProducts& sp,  const Cdoub& svp,
const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


// local variables
Cdoub denom;


denom = 4.E+0;

kern[0][0] =  4.E+0*ssp*ssm - 4.E+0*sp[7]*svp*svm;

kern[0][0] = (1.0/ denom)* kern[0][0];
kern[0][1] =   - 4.E+0*sp[43]*sp[31]*ssp*svm - 4.E+0*sp[43]*sp[29]*svp*ssm;

kern[0][1] = (1.0/ denom)* kern[0][1];
kern[0][2] =   - 4.E+0*sp[39]*ssp*svm - 4.E+0*sp[37]*svp*ssm;

kern[0][2] = (1.0/ denom)* kern[0][2];
kern[0][3] =  8.E+0*sp[31]*sp[37]*svp*svm - 8.E+0*sp[29]*sp[39]*svp*svm;

kern[0][3] = (1.0/ denom)* kern[0][3];

denom =  - 4.E+0*sp[35]*sp[43]*sp[44] + 4.E+0*pow(sp[43],3);

kern[1][0] =  4.E+0*sp[43]*sp[39]*ssp*svm + 4.E+0*sp[43]*sp[37]*svp*ssm -
4.E+0*sp[31]*sp[44]*ssp*svm - 4.E+0*sp[29]*sp[44]*svp*ssm;

kern[1][0] = (1.0/ denom)* kern[1][0];
kern[1][1] =   - 4.E+0*sp[35]*sp[43]*sp[44]*sp[7]*svp*svm - 4.E+0*sp[35]*sp[43]*
sp[44]*ssp*ssm + 8.E+0*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm - 4.E+0*pow(sp[43],2)*
sp[31]*sp[37]*svp*svm - 4.E+0*pow(sp[43],2)*sp[29]*sp[39]*svp*svm + 4.E+0*
pow(sp[43],3)*sp[7]*svp*svm + 4.E+0*pow(sp[43],3)*ssp*ssm;

kern[1][1] = (1.0/ denom)* kern[1][1];
kern[1][2] =   - 8.E+0*sp[43]*sp[39]*sp[37]*svp*svm + 4.E+0*sp[31]*sp[44]*sp[37]*
svp*svm + 4.E+0*sp[29]*sp[44]*sp[39]*svp*svm;

kern[1][2] = (1.0/ denom)* kern[1][2];
kern[1][3] =   - 8.E+0*sp[35]*sp[44]*sp[39]*ssp*svm + 8.E+0*sp[35]*sp[44]*sp[37]*
svp*ssm + 8.E+0*pow(sp[43],2)*sp[39]*ssp*svm - 8.E+0*pow(sp[43],2)*sp[37]*
svp*ssm;

kern[1][3] = (1.0/ denom)* kern[1][3];

denom = 4.E+0*sp[35]*sp[44] - 4.E+0*pow(sp[43],2);

kern[2][0] =  4.E+0*sp[35]*sp[39]*ssp*svm + 4.E+0*sp[35]*sp[37]*svp*ssm -
4.E+0*sp[43]*sp[31]*ssp*svm - 4.E+0*sp[43]*sp[29]*svp*ssm;

kern[2][0] = (1.0/ denom)* kern[2][0];
kern[2][1] =   - 4.E+0*sp[35]*sp[43]*sp[31]*sp[37]*svp*svm - 4.E+0*sp[35]*sp[43]*
sp[29]*sp[39]*svp*svm + 8.E+0*pow(sp[43],2)*sp[31]*sp[29]*svp*svm;

kern[2][1] = (1.0/ denom)* kern[2][1];
kern[2][2] =  4.E+0*sp[35]*sp[44]*sp[7]*svp*svm + 4.E+0*sp[35]*sp[44]*ssp*ssm
- 8.E+0*sp[35]*sp[39]*sp[37]*svp*svm + 4.E+0*sp[43]*sp[31]*sp[37]*svp*svm +
4.E+0*sp[43]*sp[29]*sp[39]*svp*svm - 4.E+0*pow(sp[43],2)*sp[7]*svp*svm -
4.E+0*pow(sp[43],2)*ssp*ssm;

kern[2][2] = (1.0/ denom)* kern[2][2];
kern[2][3] =   - 8.E+0*sp[35]*sp[31]*sp[44]*ssp*svm + 8.E+0*sp[35]*sp[29]*sp[44]*
svp*ssm + 8.E+0*pow(sp[43],2)*sp[31]*ssp*svm - 8.E+0*pow(sp[43],2)*sp[29]*
svp*ssm;

kern[2][3] = (1.0/ denom)* kern[2][3];

denom =  - 1.6E+1*sp[35]*sp[44] + 1.6E+1*pow(sp[43],2);

kern[3][0] =   - 8.E+0*sp[31]*sp[37]*svp*svm + 8.E+0*sp[29]*sp[39]*svp*svm;

kern[3][0] = (1.0/ denom)* kern[3][0];
kern[3][1] =  8.E+0*sp[35]*sp[43]*sp[39]*ssp*svm - 8.E+0*sp[35]*sp[43]*sp[37]*svp*
ssm - 8.E+0*pow(sp[43],2)*sp[31]*ssp*svm + 8.E+0*pow(sp[43],2)*sp[29]*svp*
ssm;

kern[3][1] = (1.0/ denom)* kern[3][1];
kern[3][2] =  8.E+0*sp[43]*sp[39]*ssp*svm - 8.E+0*sp[43]*sp[37]*svp*ssm -
8.E+0*sp[31]*sp[44]*ssp*svm + 8.E+0*sp[29]*sp[44]*svp*ssm;

kern[3][2] = (1.0/ denom)* kern[3][2];
kern[3][3] =  1.6E+1*sp[35]*sp[44]*sp[7]*svp*svm - 1.6E+1*sp[35]*sp[44]*ssp*ssm
- 3.2E+1*sp[35]*sp[39]*sp[37]*svp*svm + 3.2E+1*sp[43]*sp[31]*sp[37]*svp*svm +
3.2E+1*sp[43]*sp[29]*sp[39]*svp*svm - 1.6E+1*pow(sp[43],2)*sp[7]*svp*svm +
1.6E+1*pow(sp[43],2)*ssp*ssm - 3.2E+1*sp[31]*sp[29]*sp[44]*svp*svm;

kern[3][3] = (1.0/ denom)* kern[3][3];


}


void kernelL_scalar(matCdoub& kern,
const ArrayScalarProducts& sp,
const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){


// local variables
array<Cdoub, 25> w;
Cdoub denom;


w[1]=sp[35];
w[2]=sp[43];
w[3]=sp[27];
w[4]=sp[34];
w[5]=sp[44];
w[6]=1.2E+1*ang[1];
w[7]=2.E+0*ang[1];
w[8]=w[3]*ang[0];
w[9]=w[5]*ang[0];
w[10]=w[8] - w[7] + w[9];
w[11]=pow(w[4],2);
w[10]=w[10]*w[11];
w[12]=2.E+0*w[2];
w[13]=w[12]*w[8];
w[14]=w[9] - ang[1];
w[15]= - 3.E+0*w[8] - w[14];
w[15]=w[4]*w[15];
w[15]=w[15] + w[13];
w[15]=w[2]*w[15];
w[16]=w[1]*w[3];
w[17]=ang[1]*w[16];
w[10]=w[15] + w[17] + w[10];
w[15]=4.E+0*w[2];
w[10]=w[10]*w[15];
w[17]=w[9] + ang[1];
w[17]=w[17]*w[5];
w[17]= - w[17] + 2.E+0*ang[2];
w[18]= - 3.E+0*ang[1] + w[8];
w[18]=w[3]*w[18];
w[18]=w[18] + w[17];
w[18]=w[4]*w[18];
w[19]=w[8] - w[9];
w[7]=w[7] - w[19];
w[7]=w[7]*w[12];
w[20]=w[3]*w[7];
w[18]=w[18] + w[20];
w[18]=2.E+0*w[18];
w[20]=w[19] + ang[1];
w[20]=w[20]*w[1];
w[21]=2.E+0*ang[0];
w[22]=w[11]*w[21];
w[22]= - w[20] + w[22];
w[22]=w[4]*w[22];
w[23]=4.E+0*w[11];
w[24]= - ang[0]*w[23];
w[21]=w[2]*w[4]*w[21];
w[20]=w[21] + w[20] + w[24];
w[20]=w[2]*w[20];
w[20]=w[22] + w[20];
w[15]=w[20]*w[15];
w[20]=2.E+0*w[9];
w[21]=w[20] - w[8];
w[22]= - ang[1] + w[21];
w[22]=w[3]*w[22];
w[17]=w[22] + w[17];
w[17]=w[1]*w[17];
w[19]= - ang[1] + w[19];
w[19]=w[19]*w[11];
w[7]=w[4]*w[7];
w[7]=w[7] + w[17] + 2.E+0*w[19];
w[7]=2.E+0*w[7];
w[8]=2.E+0*w[8] + w[20] - ang[1];
w[8]=w[4]*w[8];
w[8]=w[8] - w[13];
w[8]=w[8]*w[12];
w[12]=ang[1] + w[21];
w[12]=w[3]*w[12];
w[13]= - w[5]*w[14];
w[12]=w[13] + w[12];
w[12]=w[1]*w[12];
w[9]= - w[9]*w[23];
w[8]=w[8] + w[12] + w[9];
w[8]=8.E+0*w[8];
w[9]=w[16] - w[11];
w[11]=4.E+0*w[9];
w[12]= - w[4]*w[11];
w[9]= - 1.6E+1*w[9];


denom = 4.E+0;

kern[0][0] = (1.0/ denom)* w[6];

kern[0][1] = (1.0/ denom)*0.0;

kern[0][2] = (1.0/ denom)*0.0;

kern[0][3] = (1.0/ denom)*0.0;


denom = w[12];

kern[1][0] = (1.0/ denom)*0.0;

kern[1][1] = (1.0/ denom)* w[10];

kern[1][2] = (1.0/ denom)* w[18];

kern[1][3] = (1.0/ denom)*0.0;


denom = w[11];

kern[2][0] = (1.0/ denom)*0.0;

kern[2][1] = (1.0/ denom)* w[15];

kern[2][2] = (1.0/ denom)* w[7];

kern[2][3] = (1.0/ denom)*0.0;


denom = w[9];

kern[3][0] = (1.0/ denom)*0.0;

kern[3][1] = (1.0/ denom)*0.0;

kern[3][2] = (1.0/ denom)*0.0;

kern[3][3] = (1.0/ denom)* w[8];


}


void kernelY_scalar(matCdoub& kern,
const ArrayScalarProducts& sp,  const Cdoub& svp,
const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


// local variables
array<Cdoub, 32> w;
Cdoub denom;


w[1]=sp[7];
w[2]=sp[43];
w[3]=sp[31];
w[4]=sp[29];
w[5]=sp[39];
w[6]=sp[37];
w[7]=sp[44];
w[8]=sp[35];
w[9]=ssm*ssp;
w[10]=svm*svp;
w[11]=w[10]*w[1];
w[12]=w[9] - w[11];
w[13]=4.E+0*w[12];
w[14]=ssp*svm;
w[15]=w[3]*w[14];
w[16]=ssm*svp;
w[17]=w[4]*w[16];
w[18]=w[15] + w[17];
w[19]=4.E+0*w[2];
w[20]= - w[18]*w[19];
w[14]=w[5]*w[14];
w[16]=w[6]*w[16];
w[21]=w[14] + w[16];
w[22]= - 4.E+0*w[21];
w[23]=w[10]*w[5];
w[24]=w[23]*w[4];
w[10]=w[10]*w[3];
w[25]=w[10]*w[6];
w[26]=w[24] - w[25];
w[26]=8.E+0*w[26];
w[27]= - w[7]*w[18];
w[28]=w[2]*w[21];
w[27]=w[27] + w[28];
w[27]=4.E+0*w[27];
w[9]=w[9] + w[11];
w[11]=w[9]*w[2];
w[24]=w[24] + w[25];
w[11]=w[11] - w[24];
w[11]=w[11]*w[2];
w[25]=w[8]*w[7];
w[28]= - w[9]*w[25];
w[10]=w[10]*w[4];
w[10]=2.E+0*w[10];
w[29]=w[10]*w[7];
w[28]=w[11] + w[29] + w[28];
w[28]=w[28]*w[19];
w[30]=w[7]*w[24];
w[23]=w[23]*w[6];
w[23]=2.E+0*w[23];
w[31]= - w[2]*w[23];
w[30]=w[30] + w[31];
w[30]=4.E+0*w[30];
w[31]=pow(w[2],2);
w[25]=w[31] - w[25];
w[31]=8.E+0*w[25];
w[14]=w[14] - w[16];
w[16]=w[14]*w[31];
w[21]=w[8]*w[21];
w[18]= - w[2]*w[18];
w[18]=w[21] + w[18];
w[18]=4.E+0*w[18];
w[21]= - w[8]*w[24];
w[10]=w[2]*w[10];
w[10]=w[21] + w[10];
w[10]=w[10]*w[19];
w[9]=w[7]*w[9];
w[9]= - w[23] + w[9];
w[9]=w[8]*w[9];
w[9]=w[9] - w[11];
w[9]=4.E+0*w[9];
w[11]=w[15] - w[17];
w[15]=w[11]*w[31];
w[17]=w[8]*w[14];
w[21]= - w[2]*w[11];
w[17]=w[17] + w[21];
w[17]=8.E+0*w[2]*w[17];
w[11]= - w[7]*w[11];
w[14]=w[2]*w[14];
w[11]=w[11] + w[14];
w[11]=8.E+0*w[11];
w[14]=w[2]*w[12];
w[14]=2.E+0*w[24] + w[14];
w[14]=w[2]*w[14];
w[12]= - w[7]*w[12];
w[12]= - w[23] + w[12];
w[12]=w[8]*w[12];
w[12]=w[14] - w[29] + w[12];
w[12]=1.6E+1*w[12];
w[14]=w[25]*w[19];
w[19]= - 4.E+0*w[25];
w[21]=1.6E+1*w[25];


denom = 4.E+0;

kern[0][0] = (1.0/ denom)* w[13];

kern[0][1] = (1.0/ denom)* w[20];

kern[0][2] = (1.0/ denom)* w[22];

kern[0][3] = (1.0/ denom)*  - w[26];


denom = w[14];

kern[1][0] = (1.0/ denom)* w[27];

kern[1][1] = (1.0/ denom)* w[28];

kern[1][2] = (1.0/ denom)* w[30];

kern[1][3] = (1.0/ denom)* w[16];


denom = w[19];

kern[2][0] = (1.0/ denom)* w[18];

kern[2][1] = (1.0/ denom)* w[10];

kern[2][2] = (1.0/ denom)* w[9];

kern[2][3] = (1.0/ denom)* w[15];


denom = w[21];

kern[3][0] = (1.0/ denom)* w[26];

kern[3][1] = (1.0/ denom)* w[17];

kern[3][2] = (1.0/ denom)* w[11];

kern[3][3] = (1.0/ denom)* w[12];



}

