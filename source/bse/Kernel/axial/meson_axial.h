#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
//#include <math_ops.h>

void kernelL_notopti_axial(matCdoub& kern,
const ArrayScalarProducts& sp,
const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){


// local variables
Cdoub denom=0.0;


denom =  - 8.E+0*sp[35]*sp[27] + 8.E+0*pow(sp[34],2);

kern[0][0] =   - 4.E+0*sp[35]*sp[27]*sp[44]*ang[0] - 1.2E+1*sp[35]*sp[27]*ang[1] +
2.E+0*sp[35]*pow(sp[27],2)*ang[0] - 4.E+0*sp[35]*sp[44]*ang[1] + 2.E+0*sp[35]*pow(
sp[44],2)*ang[0] + 2.E+0*sp[35]*ang[2] - 8.E+0*sp[34]*sp[43]*sp[27]*ang[0] - 8.E+0*sp[34]
*sp[43]*sp[44]*ang[0] + 8.E+0*sp[34]*sp[43]*ang[1] + 8.E+0*pow(sp[34],2)*sp[44]*ang[0] +
8.E+0*pow(sp[34],2)*ang[1] + 8.E+0*pow(sp[43],2)*sp[27]*ang[0];

kern[0][0] = (1.0/ denom)* kern[0][0];
kern[0][1] =   0;

kern[0][1] = (1.0/ denom)* kern[0][1];
kern[0][2] =   0;

kern[0][2] = (1.0/ denom)* kern[0][2];
kern[0][3] =   - 8.E+0*sp[35]*sp[34]*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*sp[34]*sp[27]*
ang[1] + 4.E+0*sp[35]*sp[34]*pow(sp[27],2)*ang[0] - 8.E+0*sp[35]*sp[34]*sp[44]*ang[1] +
4.E+0*sp[35]*sp[34]*pow(sp[44],2)*ang[0] + 4.E+0*sp[35]*sp[34]*ang[2] + 8.E+0*sp[35]*
sp[43]*sp[27]*sp[44]*ang[0] + 8.E+0*sp[35]*sp[43]*sp[27]*ang[1] - 4.E+0*sp[35]*sp[43]*pow(
sp[27],2)*ang[0] + 8.E+0*sp[35]*sp[43]*sp[44]*ang[1] - 4.E+0*sp[35]*sp[43]*pow(sp[44],2)*
ang[0] - 4.E+0*sp[35]*sp[43]*ang[2] + 3.2E+1*sp[34]*pow(sp[43],2)*sp[27]*ang[0] +
1.6E+1*sp[34]*pow(sp[43],2)*sp[44]*ang[0] - 1.6E+1*sp[34]*pow(sp[43],2)*ang[1] -
1.6E+1*pow(sp[34],2)*sp[43]*sp[27]*ang[0] - 3.2E+1*pow(sp[34],2)*sp[43]*sp[44]*ang[0]
+ 1.6E+1*pow(sp[34],2)*sp[43]*ang[1] + 1.6E+1*pow(sp[34],3)*sp[44]*ang[0] -
1.6E+1*pow(sp[43],3)*sp[27]*ang[0];

kern[0][3] = (1.0/ denom)* kern[0][3];
kern[0][4] =   0;

kern[0][4] = (1.0/ denom)* kern[0][4];
kern[0][5] =  4.E+0*sp[35]*sp[34]*sp[43]*sp[27]*sp[44]*ang[0] + 4.E+0*sp[35]*sp[34]*sp[43]*
sp[27]*ang[1] - 2.E+0*sp[35]*sp[34]*sp[43]*pow(sp[27],2)*ang[0] + 4.E+0*sp[35]*sp[34]*sp[43]*
sp[44]*ang[1] - 2.E+0*sp[35]*sp[34]*sp[43]*pow(sp[44],2)*ang[0] - 2.E+0*sp[35]*sp[34]*sp[43]*
ang[2] - 4.E+0*sp[35]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 4.E+0*sp[35]*pow(sp[43],2)*
sp[27]*ang[1] + 2.E+0*sp[35]*pow(sp[43],2)*pow(sp[27],2)*ang[0] - 4.E+0*sp[35]*pow(
sp[43],2)*sp[44]*ang[1] + 2.E+0*sp[35]*pow(sp[43],2)*pow(sp[44],2)*ang[0] + 2.E+0*
sp[35]*pow(sp[43],2)*ang[2] - 1.6E+1*sp[34]*pow(sp[43],3)*sp[27]*ang[0] - 8.E+0*sp[34]*
pow(sp[43],3)*sp[44]*ang[0] + 8.E+0*sp[34]*pow(sp[43],3)*ang[1] + 8.E+0*pow(
sp[34],2)*pow(sp[43],2)*sp[27]*ang[0] + 1.6E+1*pow(sp[34],2)*pow(sp[43],2)*sp[44]*
ang[0] - 8.E+0*pow(sp[34],2)*pow(sp[43],2)*ang[1] - 8.E+0*pow(sp[34],3)*sp[43]*
sp[44]*ang[0] + 8.E+0*pow(sp[43],4)*sp[27]*ang[0];

kern[0][5] = (1.0/ denom)* kern[0][5];
kern[0][6] =  4.E+0*sp[35]*sp[27]*sp[44]*ang[1] + 3.E+0*sp[35]*sp[27]*pow(sp[44],2)*
ang[0] + 5.E+0*sp[35]*sp[27]*ang[2] - 3.E+0*sp[35]*pow(sp[27],2)*sp[44]*ang[0] - 4.E+0
*sp[35]*pow(sp[27],2)*ang[1] + sp[35]*pow(sp[27],3)*ang[0] + 3.E+0*sp[35]*sp[44]*ang[2]
- sp[35]*pow(sp[44],3)*ang[0] - 2.E+0*sp[35]*ang[3] + 1.2E+1*sp[34]*sp[43]*sp[27]*ang[1]
- 4.E+0*sp[34]*sp[43]*pow(sp[27],2)*ang[0] + 4.E+0*sp[34]*sp[43]*sp[44]*ang[1] + 4.E+0
*sp[34]*sp[43]*pow(sp[44],2)*ang[0] - 8.E+0*sp[34]*sp[43]*ang[2] + 4.E+0*pow(sp[34],2)*
sp[27]*sp[44]*ang[0] - 8.E+0*pow(sp[34],2)*sp[44]*ang[1] - 4.E+0*pow(sp[34],2)*pow(
sp[44],2)*ang[0] - 4.E+0*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 8.E+0*pow(sp[43],2)*
sp[27]*ang[1] + 4.E+0*pow(sp[43],2)*pow(sp[27],2)*ang[0];

kern[0][6] = (1.0/ denom)* kern[0][6];
kern[0][7] =   0;

kern[0][7] = (1.0/ denom)* kern[0][7];

denom =  - 1.6E+1*sp[35]*pow(sp[34],3) + 1.6E+1*pow(sp[35],2)*sp[34]*sp[27];

kern[1][0] =   0;

kern[1][0] = (1.0/ denom)* kern[1][0];
kern[1][1] =  3.2E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*ang[0] - 1.6E+1*sp[35]*pow(
sp[34],2)*sp[43]*sp[27]*ang[0] - 1.6E+1*sp[35]*pow(sp[43],3)*sp[27]*ang[0] - 8.E+0*pow(
sp[35],2)*sp[43]*sp[27]*sp[44]*ang[0] + 8.E+0*pow(sp[35],2)*sp[43]*sp[27]*ang[1] + 4.E+0*
pow(sp[35],2)*sp[43]*pow(sp[27],2)*ang[0] - 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*ang[1] +
4.E+0*pow(sp[35],2)*sp[43]*pow(sp[44],2)*ang[0] + 4.E+0*pow(sp[35],2)*sp[43]*ang[2];

kern[1][1] = (1.0/ denom)* kern[1][1];
kern[1][2] =   - 8.E+0*sp[35]*sp[34]*sp[27]*ang[1] + 8.E+0*sp[35]*sp[34]*pow(sp[27],2)*
ang[0] + 8.E+0*sp[35]*sp[34]*sp[44]*ang[1] - 8.E+0*sp[35]*sp[34]*pow(sp[44],2)*ang[0] +
1.6E+1*sp[35]*sp[43]*sp[27]*sp[44]*ang[0] - 1.6E+1*sp[35]*sp[43]*pow(sp[27],2)*ang[0];

kern[1][2] = (1.0/ denom)* kern[1][2];
kern[1][3] =   0;

kern[1][3] = (1.0/ denom)* kern[1][3];
kern[1][4] =   0;

kern[1][4] = (1.0/ denom)* kern[1][4];
kern[1][5] =   0;

kern[1][5] = (1.0/ denom)* kern[1][5];
kern[1][6] =   0;

kern[1][6] = (1.0/ denom)* kern[1][6];
kern[1][7] =  1.6E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*ang[1] - 1.6E+1*sp[35]*sp[34]*
pow(sp[43],2)*pow(sp[27],2)*ang[0] - 1.6E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[44]*ang[1]
+ 1.6E+1*sp[35]*sp[34]*pow(sp[43],2)*pow(sp[44],2)*ang[0] + 1.6E+1*sp[35]*pow(
sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0] - 1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[44],2)*
ang[0] - 1.6E+1*sp[35]*pow(sp[43],3)*sp[27]*sp[44]*ang[0] + 1.6E+1*sp[35]*pow(sp[43],3)
*pow(sp[27],2)*ang[0] + 1.2E+1*pow(sp[35],2)*sp[43]*sp[27]*pow(sp[44],2)*ang[0] +
4.E+0*pow(sp[35],2)*sp[43]*sp[27]*ang[2] - 1.2E+1*pow(sp[35],2)*sp[43]*pow(sp[27],2)*
sp[44]*ang[0] - 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[27],2)*ang[1] + 4.E+0*pow(
sp[35],2)*sp[43]*pow(sp[27],3)*ang[0] - 4.E+0*pow(sp[35],2)*sp[43]*sp[44]*ang[2] +
8.E+0*pow(sp[35],2)*sp[43]*pow(sp[44],2)*ang[1] - 4.E+0*pow(sp[35],2)*sp[43]*pow(
sp[44],3)*ang[0];

kern[1][7] = (1.0/ denom)* kern[1][7];

denom =  - 8.E+0*sp[35]*sp[27] + 8.E+0*pow(sp[34],2);

kern[2][0] =   0;

kern[2][0] = (1.0/ denom)* kern[2][0];
kern[2][1] =   - 8.E+0*sp[35]*sp[34]*sp[43]*sp[27]*ang[0] + 8.E+0*sp[35]*sp[34]*sp[43]*sp[44]
*ang[0] - 8.E+0*sp[35]*sp[34]*sp[43]*ang[1] + 8.E+0*sp[35]*pow(sp[43],2)*sp[27]*ang[0] -
8.E+0*sp[35]*pow(sp[43],2)*sp[44]*ang[0] + 8.E+0*sp[35]*pow(sp[43],2)*ang[1] +
1.6E+1*sp[34]*pow(sp[43],3)*ang[0] - 3.2E+1*pow(sp[34],2)*pow(sp[43],2)*ang[0] +
1.6E+1*pow(sp[34],3)*sp[43]*ang[0];

kern[2][1] = (1.0/ denom)* kern[2][1];
kern[2][2] =   - 4.E+0*sp[35]*sp[27]*sp[44]*ang[0] + 2.E+0*sp[35]*pow(sp[27],2)*ang[0]
+ 2.E+0*sp[35]*pow(sp[44],2)*ang[0] - 2.E+0*sp[35]*ang[2] + 1.6E+1*sp[34]*sp[43]*
sp[27]*ang[0] - 8.E+0*sp[34]*sp[43]*ang[1] - 8.E+0*pow(sp[34],2)*sp[27]*ang[0] + 8.E+0
*pow(sp[34],2)*ang[1] - 8.E+0*pow(sp[43],2)*sp[27]*ang[0];

kern[2][2] = (1.0/ denom)* kern[2][2];
kern[2][3] =   0;

kern[2][3] = (1.0/ denom)* kern[2][3];
kern[2][4] =   0;

kern[2][4] = (1.0/ denom)* kern[2][4];
kern[2][5] =   0;

kern[2][5] = (1.0/ denom)* kern[2][5];
kern[2][6] =   0;

kern[2][6] = (1.0/ denom)* kern[2][6];
kern[2][7] =  8.E+0*sp[35]*sp[34]*sp[43]*sp[27]*sp[44]*ang[0] + 8.E+0*sp[35]*sp[34]*sp[43]*
sp[27]*ang[1] - 4.E+0*sp[35]*sp[34]*sp[43]*pow(sp[27],2)*ang[0] + 8.E+0*sp[35]*sp[34]*sp[43]*
sp[44]*ang[1] - 4.E+0*sp[35]*sp[34]*sp[43]*pow(sp[44],2)*ang[0] - 4.E+0*sp[35]*sp[34]*sp[43]*
ang[2] - 8.E+0*sp[35]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*pow(sp[43],2)*
sp[27]*ang[1] + 4.E+0*sp[35]*pow(sp[43],2)*pow(sp[27],2)*ang[0] - 8.E+0*sp[35]*pow(
sp[43],2)*sp[44]*ang[1] + 4.E+0*sp[35]*pow(sp[43],2)*pow(sp[44],2)*ang[0] + 4.E+0*
sp[35]*pow(sp[43],2)*ang[2] - 3.2E+1*sp[34]*pow(sp[43],3)*sp[27]*ang[0] - 1.6E+1*sp[34]
*pow(sp[43],3)*sp[44]*ang[0] + 1.6E+1*sp[34]*pow(sp[43],3)*ang[1] + 1.6E+1*pow(
sp[34],2)*pow(sp[43],2)*sp[27]*ang[0] + 3.2E+1*pow(sp[34],2)*pow(sp[43],2)*sp[44]*
ang[0] - 1.6E+1*pow(sp[34],2)*pow(sp[43],2)*ang[1] - 1.6E+1*pow(sp[34],3)*sp[43]*
sp[44]*ang[0] + 1.6E+1*pow(sp[43],4)*sp[27]*ang[0];

kern[2][7] = (1.0/ denom)* kern[2][7];

denom =  - 4.8E+1*sp[35]*pow(sp[34],2) + 4.8E+1*pow(sp[35],2)*sp[27];

kern[3][0] =   0;

kern[3][0] = (1.0/ denom)* kern[3][0];
kern[3][1] =   0;

kern[3][1] = (1.0/ denom)* kern[3][1];
kern[3][2] =   0;

kern[3][2] = (1.0/ denom)* kern[3][2];
kern[3][3] =  4.8E+1*sp[35]*sp[34]*sp[43]*ang[1] - 1.6E+1*sp[35]*pow(sp[34],2)*sp[27]*
ang[0] + 1.6E+1*sp[35]*pow(sp[34],2)*sp[44]*ang[0] + 1.6E+1*sp[35]*pow(sp[34],2)*
ang[1] + 1.6E+1*sp[35]*pow(sp[43],2)*sp[27]*ang[0] - 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]
*ang[0] + 1.6E+1*sp[35]*pow(sp[43],2)*ang[1] - 1.6E+1*pow(sp[35],2)*sp[27]*sp[44]*
ang[0] - 4.E+1*pow(sp[35],2)*sp[27]*ang[1] + 8.E+0*pow(sp[35],2)*pow(sp[27],2)*
ang[0] - 4.E+1*pow(sp[35],2)*sp[44]*ang[1] + 8.E+0*pow(sp[35],2)*pow(sp[44],2)*
ang[0] + 3.2E+1*pow(sp[35],2)*ang[2] + 3.2E+1*sp[34]*pow(sp[43],3)*ang[0] -
6.4E+1*pow(sp[34],2)*pow(sp[43],2)*ang[0] + 3.2E+1*pow(sp[34],3)*sp[43]*ang[0];

kern[3][3] = (1.0/ denom)* kern[3][3];
kern[3][4] =   0;

kern[3][4] = (1.0/ denom)* kern[3][4];
kern[3][5] =   0;

kern[3][5] = (1.0/ denom)* kern[3][5];
kern[3][6] =   0;

kern[3][6] = (1.0/ denom)* kern[3][6];
kern[3][7] =   0;

kern[3][7] = (1.0/ denom)* kern[3][7];

denom =  - 4.E+0*sp[35]*sp[34]*sp[27] + 4.E+0*pow(sp[34],3);

kern[4][0] =   0;

kern[4][0] = (1.0/ denom)* kern[4][0];
kern[4][1] =   0;

kern[4][1] = (1.0/ denom)* kern[4][1];
kern[4][2] =   0;

kern[4][2] = (1.0/ denom)* kern[4][2];
kern[4][3] =   0;

kern[4][3] = (1.0/ denom)* kern[4][3];
kern[4][4] =  6.E+0*sp[35]*sp[43]*sp[27]*ang[1] + 6.E+0*sp[35]*sp[43]*sp[44]*ang[1] -
6.E+0*sp[35]*sp[43]*ang[2] - 1.2E+1*sp[34]*pow(sp[43],2)*ang[1];

kern[4][4] = (1.0/ denom)* kern[4][4];
kern[4][5] =   0;

kern[4][5] = (1.0/ denom)* kern[4][5];
kern[4][6] =   0;

kern[4][6] = (1.0/ denom)* kern[4][6];
kern[4][7] =   0;

kern[4][7] = (1.0/ denom)* kern[4][7];

denom = 2.4E+1*sp[35]*pow(sp[34],5) - 4.8E+1*pow(sp[35],2)*pow(sp[34],3)*sp[27] +
2.4E+1*pow(sp[35],3)*sp[34]*pow(sp[27],2);

kern[5][0] =   - 7.2E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*ang[0] + 9.6E+1*sp[35]*
pow(sp[34],2)*sp[43]*sp[27]*ang[0] + 4.8E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[0] -
4.8E+1*sp[35]*pow(sp[34],2)*sp[43]*ang[1] - 2.4E+1*sp[35]*pow(sp[34],3)*sp[27]*ang[0]
- 4.8E+1*sp[35]*pow(sp[34],3)*sp[44]*ang[0] + 2.4E+1*sp[35]*pow(sp[34],3)*ang[1] +
1.2E+1*pow(sp[35],2)*sp[34]*sp[27]*sp[44]*ang[0] + 1.2E+1*pow(sp[35],2)*sp[34]*sp[27]*
ang[1] + 6.E+0*pow(sp[35],2)*sp[34]*pow(sp[27],2)*ang[0] + 3.6E+1*pow(sp[35],2)*
sp[34]*sp[44]*ang[1] - 1.8E+1*pow(sp[35],2)*sp[34]*pow(sp[44],2)*ang[0] - 1.8E+1*
pow(sp[35],2)*sp[34]*ang[2] + 2.4E+1*pow(sp[35],2)*sp[43]*sp[27]*sp[44]*ang[0] - 2.4E+1
*pow(sp[35],2)*sp[43]*sp[27]*ang[1] - 2.4E+1*pow(sp[35],2)*sp[43]*pow(sp[27],2)*ang[0];

kern[5][0] = (1.0/ denom)* kern[5][0];
kern[5][1] =   0;

kern[5][1] = (1.0/ denom)* kern[5][1];
kern[5][2] =   0;

kern[5][2] = (1.0/ denom)* kern[5][2];
kern[5][3] =  1.76E+2*sp[35]*sp[34]*pow(sp[43],3)*sp[27]*ang[0] - 3.2E+2*sp[35]*pow(
sp[34],2)*pow(sp[43],2)*sp[27]*ang[0] - 8.E+1*sp[35]*pow(sp[34],2)*pow(sp[43],2)*sp[44]*
ang[0] + 8.E+1*sp[35]*pow(sp[34],2)*pow(sp[43],2)*ang[1] + 1.28E+2*sp[35]*pow(
sp[34],3)*sp[43]*sp[27]*ang[0] + 1.44E+2*sp[35]*pow(sp[34],3)*sp[43]*sp[44]*ang[0] -
1.92E+2*sp[35]*pow(sp[34],3)*sp[43]*ang[1] + 1.6E+1*sp[35]*pow(sp[34],4)*sp[27]*ang[0]
- 6.4E+1*sp[35]*pow(sp[34],4)*sp[44]*ang[0] - 1.6E+1*sp[35]*pow(sp[34],4)*ang[1] +
7.2E+1*pow(sp[35],2)*sp[34]*sp[43]*sp[27]*sp[44]*ang[0] - 2.4E+1*pow(sp[35],2)*sp[34]*
sp[43]*sp[27]*ang[1] + 8.4E+1*pow(sp[35],2)*sp[34]*sp[43]*pow(sp[27],2)*ang[0] - 7.2E+1
*pow(sp[35],2)*sp[34]*sp[43]*sp[44]*ang[1] + 3.6E+1*pow(sp[35],2)*sp[34]*sp[43]*pow(
sp[44],2)*ang[0] + 3.6E+1*pow(sp[35],2)*sp[34]*sp[43]*ang[2] + 8.E+0*pow(sp[35],2)*
pow(sp[34],2)*sp[27]*sp[44]*ang[0] + 1.52E+2*pow(sp[35],2)*pow(sp[34],2)*sp[27]*ang[1]
- 6.E+1*pow(sp[35],2)*pow(sp[34],2)*pow(sp[27],2)*ang[0] + 1.36E+2*pow(
sp[35],2)*pow(sp[34],2)*sp[44]*ang[1] - 4.4E+1*pow(sp[35],2)*pow(sp[34],2)*pow(
sp[44],2)*ang[0] - 9.2E+1*pow(sp[35],2)*pow(sp[34],2)*ang[2] - 6.4E+1*pow(
sp[35],2)*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 6.4E+1*pow(sp[35],2)*pow(sp[43],2)*sp[27]
*ang[1] - 3.2E+1*pow(sp[35],2)*pow(sp[43],2)*pow(sp[27],2)*ang[0] - 6.4E+1*
pow(sp[35],3)*sp[27]*sp[44]*ang[1] + 8.E+0*pow(sp[35],3)*sp[27]*pow(sp[44],2)*ang[0] +
5.6E+1*pow(sp[35],3)*sp[27]*ang[2] - 1.6E+1*pow(sp[35],3)*pow(sp[27],2)*sp[44]*
ang[0] - 6.4E+1*pow(sp[35],3)*pow(sp[27],2)*ang[1] + 8.E+0*pow(sp[35],3)*pow(
sp[27],3)*ang[0] - 3.2E+1*pow(sp[34],3)*pow(sp[43],3)*ang[0] + 6.4E+1*pow(
sp[34],4)*pow(sp[43],2)*ang[0] - 3.2E+1*pow(sp[34],5)*sp[43]*ang[0];

kern[5][3] = (1.0/ denom)* kern[5][3];
kern[5][4] =   0;

kern[5][4] = (1.0/ denom)* kern[5][4];
kern[5][5] =   - 7.2E+1*sp[35]*sp[34]*pow(sp[43],4)*sp[27]*ang[0] + 1.2E+2*sp[35]*
pow(sp[34],2)*pow(sp[43],3)*sp[27]*ang[0] + 4.8E+1*sp[35]*pow(sp[34],2)*pow(sp[43],3)
*sp[44]*ang[0] - 4.8E+1*sp[35]*pow(sp[34],2)*pow(sp[43],3)*ang[1] - 4.8E+1*sp[35]*
pow(sp[34],3)*pow(sp[43],2)*sp[27]*ang[0] - 7.2E+1*sp[35]*pow(sp[34],3)*pow(sp[43],2)
*sp[44]*ang[0] + 7.2E+1*sp[35]*pow(sp[34],3)*pow(sp[43],2)*ang[1] + 2.4E+1*sp[35]*
pow(sp[34],4)*sp[43]*sp[44]*ang[0] - 3.6E+1*pow(sp[35],2)*sp[34]*pow(sp[43],2)*sp[27]*
sp[44]*ang[0] + 3.6E+1*pow(sp[35],2)*sp[34]*pow(sp[43],2)*sp[27]*ang[1] - 4.2E+1*
pow(sp[35],2)*sp[34]*pow(sp[43],2)*pow(sp[27],2)*ang[0] + 3.6E+1*pow(sp[35],2)*sp[34]
*pow(sp[43],2)*sp[44]*ang[1] - 1.8E+1*pow(sp[35],2)*sp[34]*pow(sp[43],2)*pow(
sp[44],2)*ang[0] - 1.8E+1*pow(sp[35],2)*sp[34]*pow(sp[43],2)*ang[2] + 1.2E+1*pow(
sp[35],2)*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0] - 4.8E+1*pow(sp[35],2)*pow(sp[34],2)
*sp[43]*sp[27]*ang[1] + 1.8E+1*pow(sp[35],2)*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0]
- 4.8E+1*pow(sp[35],2)*pow(sp[34],2)*sp[43]*sp[44]*ang[1] + 1.8E+1*pow(sp[35],2)*
pow(sp[34],2)*sp[43]*pow(sp[44],2)*ang[0] + 3.E+1*pow(sp[35],2)*pow(sp[34],2)*sp[43]*
ang[2] + 2.4E+1*pow(sp[35],2)*pow(sp[43],3)*sp[27]*sp[44]*ang[0] - 2.4E+1*pow(
sp[35],2)*pow(sp[43],3)*sp[27]*ang[1] + 2.4E+1*pow(sp[35],2)*pow(sp[43],3)*pow(
sp[27],2)*ang[0] + 1.2E+1*pow(sp[35],3)*sp[43]*sp[27]*sp[44]*ang[1] - 1.2E+1*pow(
sp[35],3)*sp[43]*sp[27]*ang[2] + 1.2E+1*pow(sp[35],3)*sp[43]*pow(sp[27],2)*ang[1];

kern[5][5] = (1.0/ denom)* kern[5][5];
kern[5][6] =  3.6E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 7.2E+1*sp[35]*
sp[34]*pow(sp[43],2)*sp[27]*ang[1] - 3.6E+1*sp[35]*sp[34]*pow(sp[43],2)*pow(sp[27],2)*
ang[0] - 7.2E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*ang[1] + 2.4E+1*sp[35]*pow(sp[34],2)
*sp[43]*pow(sp[27],2)*ang[0] - 2.4E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[1] -
2.4E+1*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[44],2)*ang[0] + 4.8E+1*sp[35]*pow(sp[34],2)
*sp[43]*ang[2] - 1.2E+1*sp[35]*pow(sp[34],3)*sp[27]*sp[44]*ang[0] + 2.4E+1*sp[35]*pow(
sp[34],3)*sp[44]*ang[1] + 1.2E+1*sp[35]*pow(sp[34],3)*pow(sp[44],2)*ang[0] + 1.2E+1*
pow(sp[35],2)*sp[34]*sp[27]*sp[44]*ang[1] - 3.E+0*pow(sp[35],2)*sp[34]*sp[27]*pow(sp[44],2)
*ang[0] - 4.5E+1*pow(sp[35],2)*sp[34]*sp[27]*ang[2] + 3.E+0*pow(sp[35],2)*sp[34]*
pow(sp[27],2)*sp[44]*ang[0] + 3.6E+1*pow(sp[35],2)*sp[34]*pow(sp[27],2)*ang[1] -
9.E+0*pow(sp[35],2)*sp[34]*pow(sp[27],3)*ang[0] - 2.7E+1*pow(sp[35],2)*sp[34]*sp[44]*
ang[2] + 9.E+0*pow(sp[35],2)*sp[34]*pow(sp[44],3)*ang[0] + 1.8E+1*pow(sp[35],2)*
sp[34]*ang[3] - 1.2E+1*pow(sp[35],2)*sp[43]*sp[27]*sp[44]*ang[1] - 1.2E+1*pow(sp[35],2)
*sp[43]*sp[27]*pow(sp[44],2)*ang[0] + 2.4E+1*pow(sp[35],2)*sp[43]*sp[27]*ang[2] -
3.6E+1*pow(sp[35],2)*sp[43]*pow(sp[27],2)*ang[1] + 1.2E+1*pow(sp[35],2)*sp[43]*
pow(sp[27],3)*ang[0];

kern[5][6] = (1.0/ denom)* kern[5][6];
kern[5][7] =   0;

kern[5][7] = (1.0/ denom)* kern[5][7];

denom =  - 1.6E+1*sp[35]*pow(sp[34],2)*sp[27] + 8.E+0*pow(sp[35],2)*pow(sp[27],2)
+ 8.E+0*pow(sp[34],4);

kern[6][0] =   - 8.E+0*sp[35]*sp[34]*sp[43]*sp[27]*ang[0] + 2.4E+1*sp[35]*sp[34]*sp[43]*
sp[44]*ang[0] - 2.4E+1*sp[35]*sp[34]*sp[43]*ang[1] + 1.6E+1*sp[35]*pow(sp[34],2)*sp[27]*
ang[0] - 2.4E+1*sp[35]*pow(sp[34],2)*sp[44]*ang[0] + 1.6E+1*sp[35]*pow(sp[34],2)*
ang[1] - 8.E+0*sp[35]*pow(sp[43],2)*sp[27]*ang[0] + 1.2E+1*pow(sp[35],2)*sp[27]*sp[44]*
ang[0] - 4.E+0*pow(sp[35],2)*sp[27]*ang[1] - 6.E+0*pow(sp[35],2)*pow(sp[27],2)*
ang[0] + 1.2E+1*pow(sp[35],2)*sp[44]*ang[1] - 6.E+0*pow(sp[35],2)*pow(sp[44],2)*
ang[0] - 6.E+0*pow(sp[35],2)*ang[2] - 1.6E+1*pow(sp[34],2)*pow(sp[43],2)*ang[0]
+ 3.2E+1*pow(sp[34],3)*sp[43]*ang[0] - 1.6E+1*pow(sp[34],4)*ang[0];

kern[6][0] = (1.0/ denom)* kern[6][0];
kern[6][1] =   0;

kern[6][1] = (1.0/ denom)* kern[6][1];
kern[6][2] =   0;

kern[6][2] = (1.0/ denom)* kern[6][2];
kern[6][3] =   - 3.2E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*ang[0] - 4.8E+1*sp[35]*
sp[34]*pow(sp[43],2)*sp[44]*ang[0] + 4.8E+1*sp[35]*sp[34]*pow(sp[43],2)*ang[1] + 3.2E+1
*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*ang[0] + 8.E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[0]
- 6.4E+1*sp[35]*pow(sp[34],2)*sp[43]*ang[1] - 1.6E+1*sp[35]*pow(sp[34],3)*sp[27]*
ang[0] - 3.2E+1*sp[35]*pow(sp[34],3)*sp[44]*ang[0] + 1.6E+1*sp[35]*pow(sp[34],3)*
ang[1] + 1.6E+1*sp[35]*pow(sp[43],3)*sp[27]*ang[0] + 8.E+0*pow(sp[35],2)*sp[34]*sp[27]*
sp[44]*ang[0] + 8.E+0*pow(sp[35],2)*sp[34]*sp[27]*ang[1] + 4.E+0*pow(sp[35],2)*sp[34]*
pow(sp[27],2)*ang[0] + 2.4E+1*pow(sp[35],2)*sp[34]*sp[44]*ang[1] - 1.2E+1*pow(
sp[35],2)*sp[34]*pow(sp[44],2)*ang[0] - 1.2E+1*pow(sp[35],2)*sp[34]*ang[2] - 8.E+0*
pow(sp[35],2)*sp[43]*sp[27]*sp[44]*ang[0] - 8.E+0*pow(sp[35],2)*sp[43]*sp[27]*ang[1] -
4.E+0*pow(sp[35],2)*sp[43]*pow(sp[27],2)*ang[0] - 2.4E+1*pow(sp[35],2)*sp[43]*sp[44]*
ang[1] + 1.2E+1*pow(sp[35],2)*sp[43]*pow(sp[44],2)*ang[0] + 1.2E+1*pow(sp[35],2)*
sp[43]*ang[2] + 3.2E+1*pow(sp[34],2)*pow(sp[43],3)*ang[0] - 6.4E+1*pow(sp[34],3)*
pow(sp[43],2)*ang[0] + 3.2E+1*pow(sp[34],4)*sp[43]*ang[0];

kern[6][3] = (1.0/ denom)* kern[6][3];
kern[6][4] =   0;

kern[6][4] = (1.0/ denom)* kern[6][4];
kern[6][5] =  1.6E+1*sp[35]*sp[34]*pow(sp[43],3)*sp[27]*ang[0] + 2.4E+1*sp[35]*sp[34]*
pow(sp[43],3)*sp[44]*ang[0] - 2.4E+1*sp[35]*sp[34]*pow(sp[43],3)*ang[1] - 1.6E+1*sp[35]
*pow(sp[34],2)*pow(sp[43],2)*sp[27]*ang[0] - 4.E+1*sp[35]*pow(sp[34],2)*pow(sp[43],2)
*sp[44]*ang[0] + 3.2E+1*sp[35]*pow(sp[34],2)*pow(sp[43],2)*ang[1] + 8.E+0*sp[35]*
pow(sp[34],3)*sp[43]*sp[27]*ang[0] + 1.6E+1*sp[35]*pow(sp[34],3)*sp[43]*sp[44]*ang[0] -
8.E+0*sp[35]*pow(sp[34],3)*sp[43]*ang[1] - 8.E+0*sp[35]*pow(sp[43],4)*sp[27]*ang[0] -
4.E+0*pow(sp[35],2)*sp[34]*sp[43]*sp[27]*sp[44]*ang[0] - 4.E+0*pow(sp[35],2)*sp[34]*sp[43]*
sp[27]*ang[1] - 2.E+0*pow(sp[35],2)*sp[34]*sp[43]*pow(sp[27],2)*ang[0] - 1.2E+1*pow(
sp[35],2)*sp[34]*sp[43]*sp[44]*ang[1] + 6.E+0*pow(sp[35],2)*sp[34]*sp[43]*pow(sp[44],2)*
ang[0] + 6.E+0*pow(sp[35],2)*sp[34]*sp[43]*ang[2] + 4.E+0*pow(sp[35],2)*pow(
sp[43],2)*sp[27]*sp[44]*ang[0] + 4.E+0*pow(sp[35],2)*pow(sp[43],2)*sp[27]*ang[1] +
2.E+0*pow(sp[35],2)*pow(sp[43],2)*pow(sp[27],2)*ang[0] + 1.2E+1*pow(sp[35],2)*
pow(sp[43],2)*sp[44]*ang[1] - 6.E+0*pow(sp[35],2)*pow(sp[43],2)*pow(sp[44],2)*ang[0]
- 6.E+0*pow(sp[35],2)*pow(sp[43],2)*ang[2] - 1.6E+1*pow(sp[34],2)*pow(
sp[43],4)*ang[0] + 3.2E+1*pow(sp[34],3)*pow(sp[43],3)*ang[0] - 1.6E+1*pow(
sp[34],4)*pow(sp[43],2)*ang[0];

kern[6][5] = (1.0/ denom)* kern[6][5];
kern[6][6] =  8.E+0*sp[35]*sp[34]*sp[43]*sp[27]*sp[44]*ang[0] - 2.8E+1*sp[35]*sp[34]*sp[43]*
sp[27]*ang[1] + 4.E+0*sp[35]*sp[34]*sp[43]*pow(sp[27],2)*ang[0] - 1.2E+1*sp[35]*sp[34]*sp[43]
*sp[44]*ang[1] - 1.2E+1*sp[35]*sp[34]*sp[43]*pow(sp[44],2)*ang[0] + 2.4E+1*sp[35]*sp[34]*
sp[43]*ang[2] - 4.E+0*sp[35]*pow(sp[34],2)*sp[27]*sp[44]*ang[0] + 8.E+0*sp[35]*pow(
sp[34],2)*sp[27]*ang[1] - 4.E+0*sp[35]*pow(sp[34],2)*pow(sp[27],2)*ang[0] + 8.E+0*
sp[35]*pow(sp[34],2)*sp[44]*ang[1] + 8.E+0*sp[35]*pow(sp[34],2)*pow(sp[44],2)*ang[0] -
4.E+0*sp[35]*pow(sp[34],2)*ang[2] + 4.E+0*sp[35]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] +
8.E+0*sp[35]*pow(sp[43],2)*sp[27]*ang[1] - 4.E+0*sp[35]*pow(sp[43],2)*pow(sp[27],2)*
ang[0] + 4.E+0*pow(sp[35],2)*sp[27]*sp[44]*ang[1] - 5.E+0*pow(sp[35],2)*sp[27]*pow(
sp[44],2)*ang[0] - 1.1E+1*pow(sp[35],2)*sp[27]*ang[2] + pow(sp[35],2)*pow(sp[27],2)*
sp[44]*ang[0] + 4.E+0*pow(sp[35],2)*pow(sp[27],2)*ang[1] + pow(sp[35],2)*pow(
sp[27],3)*ang[0] - 9.E+0*pow(sp[35],2)*sp[44]*ang[2] + 3.E+0*pow(sp[35],2)*pow(
sp[44],3)*ang[0] + 6.E+0*pow(sp[35],2)*ang[3] - 8.E+0*pow(sp[34],2)*pow(sp[43],2)
*sp[27]*ang[0] + 8.E+0*pow(sp[34],2)*pow(sp[43],2)*sp[44]*ang[0] + 1.6E+1*pow(
sp[34],2)*pow(sp[43],2)*ang[1] + 8.E+0*pow(sp[34],3)*sp[43]*sp[27]*ang[0] - 8.E+0*
pow(sp[34],3)*sp[43]*sp[44]*ang[0] - 8.E+0*pow(sp[34],3)*sp[43]*ang[1];

kern[6][6] = (1.0/ denom)* kern[6][6];
kern[6][7] =   0;

kern[6][7] = (1.0/ denom)* kern[6][7];

denom =  - 6.4E+1*sp[35]*pow(sp[34],3)*sp[27] + 3.2E+1*pow(sp[35],2)*sp[34]*pow(
sp[27],2) + 3.2E+1*pow(sp[34],5);

kern[7][0] =   0;

kern[7][0] = (1.0/ denom)* kern[7][0];
kern[7][1] =  1.6E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*ang[0] - 4.8E+1*sp[35]*sp[34]*
pow(sp[43],2)*sp[44]*ang[0] + 4.8E+1*sp[35]*sp[34]*pow(sp[43],2)*ang[1] - 3.2E+1*sp[35]
*pow(sp[34],2)*sp[43]*sp[27]*ang[0] + 4.8E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[0] -
3.2E+1*sp[35]*pow(sp[34],2)*sp[43]*ang[1] + 1.6E+1*sp[35]*pow(sp[43],3)*sp[27]*ang[0]
- 2.4E+1*pow(sp[35],2)*sp[43]*sp[27]*sp[44]*ang[0] + 8.E+0*pow(sp[35],2)*sp[43]*sp[27]*
ang[1] + 1.2E+1*pow(sp[35],2)*sp[43]*pow(sp[27],2)*ang[0] - 2.4E+1*pow(sp[35],2)*
sp[43]*sp[44]*ang[1] + 1.2E+1*pow(sp[35],2)*sp[43]*pow(sp[44],2)*ang[0] + 1.2E+1*
pow(sp[35],2)*sp[43]*ang[2] + 3.2E+1*pow(sp[34],2)*pow(sp[43],3)*ang[0] - 6.4E+1*
pow(sp[34],3)*pow(sp[43],2)*ang[0] + 3.2E+1*pow(sp[34],4)*sp[43]*ang[0];

kern[7][1] = (1.0/ denom)* kern[7][1];
kern[7][2] =  8.E+0*sp[35]*sp[34]*sp[27]*sp[44]*ang[0] + 8.E+0*sp[35]*sp[34]*sp[27]*ang[1]
+ 4.E+0*sp[35]*sp[34]*pow(sp[27],2)*ang[0] + 2.4E+1*sp[35]*sp[34]*sp[44]*ang[1] -
1.2E+1*sp[35]*sp[34]*pow(sp[44],2)*ang[0] - 1.2E+1*sp[35]*sp[34]*ang[2] + 1.6E+1*sp[35]
*sp[43]*sp[27]*sp[44]*ang[0] - 1.6E+1*sp[35]*sp[43]*sp[27]*ang[1] - 1.6E+1*sp[35]*sp[43]*pow(
sp[27],2)*ang[0] - 4.8E+1*sp[34]*pow(sp[43],2)*sp[27]*ang[0] + 6.4E+1*pow(sp[34],2)*
sp[43]*sp[27]*ang[0] + 3.2E+1*pow(sp[34],2)*sp[43]*sp[44]*ang[0] - 3.2E+1*pow(sp[34],2)
*sp[43]*ang[1] - 1.6E+1*pow(sp[34],3)*sp[27]*ang[0] - 3.2E+1*pow(sp[34],3)*sp[44]*
ang[0] + 1.6E+1*pow(sp[34],3)*ang[1];

kern[7][2] = (1.0/ denom)* kern[7][2];
kern[7][3] =   0;

kern[7][3] = (1.0/ denom)* kern[7][3];
kern[7][4] =   0;

kern[7][4] = (1.0/ denom)* kern[7][4];
kern[7][5] =   0;

kern[7][5] = (1.0/ denom)* kern[7][5];
kern[7][6] =   0;

kern[7][6] = (1.0/ denom)* kern[7][6];
kern[7][7] =  1.6E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 6.4E+1*sp[35]*
sp[34]*pow(sp[43],2)*sp[27]*ang[1] + 4.E+1*sp[35]*sp[34]*pow(sp[43],2)*pow(sp[27],2)*
ang[0] - 9.6E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[44]*ang[1] + 7.2E+1*sp[35]*sp[34]*pow(
sp[43],2)*pow(sp[44],2)*ang[0] + 2.4E+1*sp[35]*sp[34]*pow(sp[43],2)*ang[2] + 1.6E+1*
sp[35]*pow(sp[34],2)*sp[43]*sp[27]*ang[1] - 8.E+0*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[27],2)
*ang[0] + 4.8E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[1] - 5.6E+1*sp[35]*pow(
sp[34],2)*sp[43]*pow(sp[44],2)*ang[0] - 8.E+0*sp[35]*pow(sp[34],2)*sp[43]*ang[2] -
4.8E+1*sp[35]*pow(sp[43],3)*sp[27]*sp[44]*ang[0] + 3.2E+1*sp[35]*pow(sp[43],3)*sp[27]*
ang[1] - 1.6E+1*sp[35]*pow(sp[43],3)*pow(sp[27],2)*ang[0] + 2.E+1*pow(sp[35],2)*
sp[43]*sp[27]*pow(sp[44],2)*ang[0] - 4.E+0*pow(sp[35],2)*sp[43]*sp[27]*ang[2] - 4.E+0*
pow(sp[35],2)*sp[43]*pow(sp[27],2)*sp[44]*ang[0] + 8.E+0*pow(sp[35],2)*sp[43]*pow(
sp[27],2)*ang[1] - 4.E+0*pow(sp[35],2)*sp[43]*pow(sp[27],3)*ang[0] - 1.2E+1*pow(
sp[35],2)*sp[43]*sp[44]*ang[2] + 2.4E+1*pow(sp[35],2)*sp[43]*pow(sp[44],2)*ang[1] -
1.2E+1*pow(sp[35],2)*sp[43]*pow(sp[44],3)*ang[0] + 9.6E+1*sp[34]*pow(sp[43],4)*sp[27]
*ang[0] - 1.28E+2*pow(sp[34],2)*pow(sp[43],3)*sp[27]*ang[0] - 9.6E+1*pow(
sp[34],2)*pow(sp[43],3)*sp[44]*ang[0] + 6.4E+1*pow(sp[34],2)*pow(sp[43],3)*ang[1] +
3.2E+1*pow(sp[34],3)*pow(sp[43],2)*sp[27]*ang[0] + 1.28E+2*pow(sp[34],3)*pow(
sp[43],2)*sp[44]*ang[0] - 3.2E+1*pow(sp[34],3)*pow(sp[43],2)*ang[1] - 3.2E+1*pow(
sp[34],4)*sp[43]*sp[44]*ang[0];

kern[7][7] = (1.0/ denom)* kern[7][7];

}


void kernelY_noopti_axial(matCdoub& kern,
const ArrayScalarProducts& sp,  const Cdoub& svp,
const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


// local variables
Cdoub denom;


denom =  - 8.E+0*sp[35]*sp[44] + 8.E+0*pow(sp[43],2);

kern[0][0] =   - 8.E+0*sp[35]*sp[44]*ssp*ssm + 8.E+0*sp[35]*sp[39]*sp[37]*svp*
svm - 8.E+0*sp[43]*sp[31]*sp[37]*svp*svm - 8.E+0*sp[43]*sp[29]*sp[39]*svp*svm
+ 8.E+0*pow(sp[43],2)*ssp*ssm + 8.E+0*sp[31]*sp[29]*sp[44]*svp*svm;

kern[0][0] = (1.0/ denom)* kern[0][0];
kern[0][1] =  8.E+0*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm + 8.E+0*sp[35]*sp[43]*sp[29]*
sp[44]*svp*ssm - 8.E+0*pow(sp[43],3)*sp[31]*ssp*svm - 8.E+0*pow(sp[43],3)*
sp[29]*svp*ssm;

kern[0][1] = (1.0/ denom)* kern[0][1];
kern[0][2] =   - 8.E+0*sp[35]*sp[44]*sp[39]*ssp*svm - 8.E+0*sp[35]*sp[44]*sp[37]*
svp*ssm + 8.E+0*pow(sp[43],2)*sp[39]*ssp*svm + 8.E+0*pow(sp[43],2)*sp[37]*
svp*ssm;

kern[0][2] = (1.0/ denom)* kern[0][2];
kern[0][3] =   - 1.6E+1*sp[35]*sp[31]*sp[44]*sp[37]*svp*svm + 1.6E+1*sp[35]*sp[29]
*sp[44]*sp[39]*svp*svm + 1.6E+1*pow(sp[43],2)*sp[31]*sp[37]*svp*svm - 1.6E+1*
pow(sp[43],2)*sp[29]*sp[39]*svp*svm;

kern[0][3] = (1.0/ denom)* kern[0][3];
kern[0][4] =   0;

kern[0][4] = (1.0/ denom)* kern[0][4];
kern[0][5] =   0;

kern[0][5] = (1.0/ denom)* kern[0][5];
kern[0][6] =   0;

kern[0][6] = (1.0/ denom)* kern[0][6];
kern[0][7] =   0;

kern[0][7] = (1.0/ denom)* kern[0][7];

denom =  - 1.6E+1*sp[35]*pow(sp[43],3) + 1.6E+1*pow(sp[35],2)*sp[43]*sp[44];

kern[1][0] =   - 1.6E+1*sp[35]*sp[43]*sp[39]*ssp*svm - 1.6E+1*sp[35]*sp[43]*sp[37]*
svp*ssm + 1.6E+1*sp[35]*sp[31]*sp[44]*ssp*svm + 1.6E+1*sp[35]*sp[29]*sp[44]*svp*
ssm;

kern[1][0] = (1.0/ denom)* kern[1][0];
kern[1][1] =   - 1.6E+1*sp[35]*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm - 1.6E+1*sp[35]*
pow(sp[43],3)*ssp*ssm + 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]*ssp*ssm + 1.6E+1*
pow(sp[35],2)*sp[43]*sp[39]*sp[37]*svp*svm;

kern[1][1] = (1.0/ denom)* kern[1][1];
kern[1][2] =   - 3.2E+1*sp[35]*sp[43]*sp[39]*sp[37]*svp*svm + 1.6E+1*sp[35]*sp[31]
*sp[44]*sp[37]*svp*svm + 1.6E+1*sp[35]*sp[29]*sp[44]*sp[39]*svp*svm;

kern[1][2] = (1.0/ denom)* kern[1][2];
kern[1][3] =   - 3.2E+1*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm + 3.2E+1*sp[35]*
pow(sp[43],2)*sp[37]*svp*ssm + 3.2E+1*pow(sp[35],2)*sp[44]*sp[39]*ssp*svm -
3.2E+1*pow(sp[35],2)*sp[44]*sp[37]*svp*ssm;

kern[1][3] = (1.0/ denom)* kern[1][3];
kern[1][4] =   0;

kern[1][4] = (1.0/ denom)* kern[1][4];
kern[1][5] =   0;

kern[1][5] = (1.0/ denom)* kern[1][5];
kern[1][6] =   0;

kern[1][6] = (1.0/ denom)* kern[1][6];
kern[1][7] =   0;

kern[1][7] = (1.0/ denom)* kern[1][7];

denom =  - 8.E+0*sp[35]*sp[44] + 8.E+0*pow(sp[43],2);

kern[2][0] =  8.E+0*sp[35]*sp[39]*ssp*svm + 8.E+0*sp[35]*sp[37]*svp*ssm -
8.E+0*sp[43]*sp[31]*ssp*svm - 8.E+0*sp[43]*sp[29]*svp*ssm;

kern[2][0] = (1.0/ denom)* kern[2][0];
kern[2][1] =   - 8.E+0*sp[35]*sp[43]*sp[31]*sp[37]*svp*svm - 8.E+0*sp[35]*sp[43]*
sp[29]*sp[39]*svp*svm + 1.6E+1*pow(sp[43],2)*sp[31]*sp[29]*svp*svm;

kern[2][1] = (1.0/ denom)* kern[2][1];
kern[2][2] =   - 8.E+0*sp[35]*sp[44]*ssp*ssm + 8.E+0*sp[35]*sp[39]*sp[37]*svp*
svm + 8.E+0*pow(sp[43],2)*ssp*ssm - 8.E+0*sp[31]*sp[29]*sp[44]*svp*svm;

kern[2][2] = (1.0/ denom)* kern[2][2];
kern[2][3] =   - 1.6E+1*sp[35]*sp[31]*sp[44]*ssp*svm + 1.6E+1*sp[35]*sp[29]*sp[44]*
svp*ssm + 1.6E+1*pow(sp[43],2)*sp[31]*ssp*svm - 1.6E+1*pow(sp[43],2)*sp[29]
*svp*ssm;

kern[2][3] = (1.0/ denom)* kern[2][3];
kern[2][4] =   0;

kern[2][4] = (1.0/ denom)* kern[2][4];
kern[2][5] =   0;

kern[2][5] = (1.0/ denom)* kern[2][5];
kern[2][6] =   0;

kern[2][6] = (1.0/ denom)* kern[2][6];
kern[2][7] =   0;

kern[2][7] = (1.0/ denom)* kern[2][7];

denom =  - 4.8E+1*sp[35]*pow(sp[43],2) + 4.8E+1*pow(sp[35],2)*sp[44];

kern[3][0] =  1.6E+1*sp[35]*sp[31]*sp[37]*svp*svm - 1.6E+1*sp[35]*sp[29]*sp[39]*
svp*svm;

kern[3][0] = (1.0/ denom)* kern[3][0];
kern[3][1] =  1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*ssp*svm - 1.6E+1*sp[35]*pow(
sp[43],2)*sp[29]*svp*ssm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[39]*ssp*svm + 1.6E+1
*pow(sp[35],2)*sp[43]*sp[37]*svp*ssm;

kern[3][1] = (1.0/ denom)* kern[3][1];
kern[3][2] =  1.6E+1*sp[35]*sp[43]*sp[39]*ssp*svm - 1.6E+1*sp[35]*sp[43]*sp[37]*svp
*ssm - 1.6E+1*sp[35]*sp[31]*sp[44]*ssp*svm + 1.6E+1*sp[35]*sp[29]*sp[44]*svp*ssm;

kern[3][2] = (1.0/ denom)* kern[3][2];
kern[3][3] =   - 3.2E+1*sp[35]*sp[43]*sp[31]*sp[37]*svp*svm - 3.2E+1*sp[35]*sp[43]*
sp[29]*sp[39]*svp*svm - 1.6E+1*sp[35]*pow(sp[43],2)*sp[7]*svp*svm - 4.8E+1*
sp[35]*pow(sp[43],2)*ssp*ssm + 1.6E+1*pow(sp[35],2)*sp[44]*sp[7]*svp*svm +
4.8E+1*pow(sp[35],2)*sp[44]*ssp*ssm + 3.2E+1*pow(sp[35],2)*sp[39]*sp[37]*svp*
svm + 3.2E+1*pow(sp[43],2)*sp[31]*sp[29]*svp*svm;

kern[3][3] = (1.0/ denom)* kern[3][3];
kern[3][4] =   0;

kern[3][4] = (1.0/ denom)* kern[3][4];
kern[3][5] =   0;

kern[3][5] = (1.0/ denom)* kern[3][5];
kern[3][6] =   0;

kern[3][6] = (1.0/ denom)* kern[3][6];
kern[3][7] =   0;

kern[3][7] = (1.0/ denom)* kern[3][7];

denom =  - 4.E+0*sp[35]*sp[43]*sp[44] + 4.E+0*pow(sp[43],3);

kern[4][0] =   - 4.E+0*sp[35]*sp[39]*ssp*svm + 4.E+0*sp[35]*sp[37]*svp*ssm +
4.E+0*sp[43]*sp[31]*ssp*svm - 4.E+0*sp[43]*sp[29]*svp*ssm;

kern[4][0] = (1.0/ denom)* kern[4][0];
kern[4][1] =   - 4.E+0*sp[35]*sp[43]*sp[31]*sp[37]*svp*svm + 4.E+0*sp[35]*sp[43]*
sp[29]*sp[39]*svp*svm;

kern[4][1] = (1.0/ denom)* kern[4][1];
kern[4][2] =  4.E+0*sp[43]*sp[31]*sp[37]*svp*svm - 4.E+0*sp[43]*sp[29]*sp[39]*svp
*svm;

kern[4][2] = (1.0/ denom)* kern[4][2];
kern[4][3] =  8.E+0*sp[35]*sp[31]*sp[44]*ssp*svm - 8.E+0*sp[35]*sp[29]*sp[44]*svp*
ssm - 8.E+0*pow(sp[43],2)*sp[31]*ssp*svm + 8.E+0*pow(sp[43],2)*sp[29]*svp*
ssm;

kern[4][3] = (1.0/ denom)* kern[4][3];
kern[4][4] =   - 4.E+0*sp[35]*sp[43]*sp[44]*sp[7]*svp*svm - 4.E+0*sp[35]*sp[43]*
sp[44]*ssp*ssm + 4.E+0*pow(sp[43],3)*sp[7]*svp*svm + 4.E+0*pow(sp[43],3)*
ssp*ssm;

kern[4][4] = (1.0/ denom)* kern[4][4];
kern[4][5] =   - 4.E+0*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm + 4.E+0*sp[35]*sp[43]*
sp[29]*sp[44]*svp*ssm + 4.E+0*pow(sp[43],3)*sp[31]*ssp*svm - 4.E+0*pow(
sp[43],3)*sp[29]*svp*ssm;

kern[4][5] = (1.0/ denom)* kern[4][5];
kern[4][6] =  4.E+0*sp[35]*sp[44]*sp[39]*ssp*svm - 4.E+0*sp[35]*sp[44]*sp[37]*svp*
ssm - 4.E+0*pow(sp[43],2)*sp[39]*ssp*svm + 4.E+0*pow(sp[43],2)*sp[37]*svp*
ssm;

kern[4][6] = (1.0/ denom)* kern[4][6];
kern[4][7] =  8.E+0*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 8.E+0*sp[35]*sp[43]*
sp[29]*sp[44]*sp[39]*svp*svm - 8.E+0*pow(sp[43],3)*sp[31]*sp[37]*svp*svm +
8.E+0*pow(sp[43],3)*sp[29]*sp[39]*svp*svm;

kern[4][7] = (1.0/ denom)* kern[4][7];

denom = 2.4E+1*sp[35]*pow(sp[43],5) - 4.8E+1*pow(sp[35],2)*pow(sp[43],3)*sp[44] +
2.4E+1*pow(sp[35],3)*sp[43]*pow(sp[44],2);

kern[5][0] =   - 7.2E+1*sp[35]*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm + 3.2E+1*sp[35]*
pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 6.4E+1*sp[35]*pow(sp[43],2)*sp[29]*sp[39]*
svp*svm - 2.4E+1*sp[35]*pow(sp[43],3)*sp[7]*svp*svm + 2.4E+1*pow(sp[35],2)
*sp[43]*sp[44]*sp[7]*svp*svm - 7.2E+1*pow(sp[35],2)*sp[43]*sp[39]*sp[37]*svp*svm
+ 4.E+1*pow(sp[35],2)*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*pow(sp[35],2)*sp[29]
*sp[44]*sp[39]*svp*svm;

kern[5][0] = (1.0/ denom)* kern[5][0];
kern[5][1] =  8.E+0*sp[35]*pow(sp[43],4)*sp[31]*ssp*svm + 4.E+1*sp[35]*pow(
sp[43],4)*sp[29]*svp*ssm - 8.E+0*pow(sp[35],2)*pow(sp[43],2)*sp[31]*sp[44]*ssp*
svm - 4.E+1*pow(sp[35],2)*pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 8.E+0*pow(
sp[35],2)*pow(sp[43],3)*sp[39]*ssp*svm - 4.E+1*pow(sp[35],2)*pow(sp[43],3)*sp[37]
*svp*ssm + 8.E+0*pow(sp[35],3)*sp[43]*sp[44]*sp[39]*ssp*svm + 4.E+1*pow(
sp[35],3)*sp[43]*sp[44]*sp[37]*svp*ssm;

kern[5][1] = (1.0/ denom)* kern[5][1];
kern[5][2] =  1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm - 1.6E+1*sp[35]*
pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 1.6E+1*sp[35]*pow(sp[43],3)*sp[39]*ssp*svm
+ 1.6E+1*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm + 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]
*sp[39]*ssp*svm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm - 1.6E+1*
pow(sp[35],2)*sp[31]*pow(sp[44],2)*ssp*svm + 1.6E+1*pow(sp[35],2)*sp[29]*pow(
sp[44],2)*svp*ssm;

kern[5][2] = (1.0/ denom)* kern[5][2];
kern[5][3] =  1.28E+2*sp[35]*pow(sp[43],2)*sp[31]*sp[29]*sp[44]*svp*svm - 6.4E+1
*sp[35]*pow(sp[43],3)*sp[31]*sp[37]*svp*svm + 3.2E+1*sp[35]*pow(sp[43],3)*sp[29]*
sp[39]*svp*svm + 6.4E+1*sp[35]*pow(sp[43],4)*sp[7]*svp*svm + 6.4E+1*pow(
sp[35],2)*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 3.2E+1*pow(sp[35],2)*sp[43]*sp[29]*sp[44]
*sp[39]*svp*svm - 1.28E+2*pow(sp[35],2)*pow(sp[43],2)*sp[44]*sp[7]*svp*svm
- 3.2E+1*pow(sp[35],2)*pow(sp[43],2)*sp[39]*sp[37]*svp*svm - 9.6E+1*pow(
sp[35],2)*sp[31]*sp[29]*pow(sp[44],2)*svp*svm + 3.2E+1*pow(sp[35],3)*sp[44]*sp[39]*
sp[37]*svp*svm + 6.4E+1*pow(sp[35],3)*pow(sp[44],2)*sp[7]*svp*svm -
3.2E+1*pow(sp[43],4)*sp[31]*sp[29]*svp*svm;

kern[5][3] = (1.0/ denom)* kern[5][3];
kern[5][4] =  2.4E+1*sp[35]*pow(sp[43],3)*sp[31]*sp[44]*ssp*svm - 2.4E+1*sp[35]*
pow(sp[43],3)*sp[29]*sp[44]*svp*ssm - 2.4E+1*sp[35]*pow(sp[43],4)*sp[39]*ssp*svm
+ 2.4E+1*sp[35]*pow(sp[43],4)*sp[37]*svp*ssm - 2.4E+1*pow(sp[35],2)*sp[43]*
sp[31]*pow(sp[44],2)*ssp*svm + 2.4E+1*pow(sp[35],2)*sp[43]*sp[29]*pow(sp[44],2)*
svp*ssm + 2.4E+1*pow(sp[35],2)*pow(sp[43],2)*sp[44]*sp[39]*ssp*svm - 2.4E+1*
pow(sp[35],2)*pow(sp[43],2)*sp[44]*sp[37]*svp*ssm;

kern[5][4] = (1.0/ denom)* kern[5][4];
kern[5][5] =   - 4.8E+1*sp[35]*pow(sp[43],3)*sp[31]*sp[29]*sp[44]*svp*svm +
2.4E+1*sp[35]*pow(sp[43],4)*sp[31]*sp[37]*svp*svm + 2.4E+1*sp[35]*pow(sp[43],4)*
sp[29]*sp[39]*svp*svm - 2.4E+1*sp[35]*pow(sp[43],5)*sp[7]*svp*svm + 2.4E+1*
sp[35]*pow(sp[43],5)*ssp*ssm + 4.8E+1*pow(sp[35],2)*sp[43]*sp[31]*sp[29]*pow(
sp[44],2)*svp*svm - 2.4E+1*pow(sp[35],2)*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*
svm - 2.4E+1*pow(sp[35],2)*pow(sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm + 4.8E+1
*pow(sp[35],2)*pow(sp[43],3)*sp[44]*sp[7]*svp*svm - 4.8E+1*pow(sp[35],2)*pow(
sp[43],3)*sp[44]*ssp*ssm - 2.4E+1*pow(sp[35],3)*sp[43]*pow(sp[44],2)*sp[7]*svp*
svm + 2.4E+1*pow(sp[35],3)*sp[43]*pow(sp[44],2)*ssp*ssm;

kern[5][5] = (1.0/ denom)* kern[5][5];
kern[5][6] =  2.4E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm + 2.4E+1*
sp[35]*pow(sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm - 4.8E+1*sp[35]*pow(sp[43],3)*sp[39]
*sp[37]*svp*svm + 4.8E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[39]*sp[37]*svp*svm -
2.4E+1*pow(sp[35],2)*sp[31]*pow(sp[44],2)*sp[37]*svp*svm - 2.4E+1*pow(
sp[35],2)*sp[29]*pow(sp[44],2)*sp[39]*svp*svm;

kern[5][6] = (1.0/ denom)* kern[5][6];
kern[5][7] =   - 4.8E+1*sp[35]*pow(sp[43],5)*sp[39]*ssp*svm - 4.8E+1*sp[35]*
pow(sp[43],5)*sp[37]*svp*ssm + 9.6E+1*pow(sp[35],2)*pow(sp[43],3)*sp[44]*sp[39]*
ssp*svm + 9.6E+1*pow(sp[35],2)*pow(sp[43],3)*sp[44]*sp[37]*svp*ssm - 4.8E+1*
pow(sp[35],3)*sp[43]*pow(sp[44],2)*sp[39]*ssp*svm - 4.8E+1*pow(sp[35],3)*sp[43]*
pow(sp[44],2)*sp[37]*svp*ssm;

kern[5][7] = (1.0/ denom)* kern[5][7];

denom =  - 1.6E+1*sp[35]*pow(sp[43],2)*sp[44] + 8.E+0*pow(sp[35],2)*pow(sp[44],2)
+ 8.E+0*pow(sp[43],4);

kern[6][0] =  2.4E+1*sp[35]*sp[43]*sp[31]*sp[37]*svp*svm + 2.4E+1*sp[35]*sp[43]*
sp[29]*sp[39]*svp*svm - 8.E+0*sp[35]*pow(sp[43],2)*sp[7]*svp*svm - 8.E+0*
sp[35]*sp[31]*sp[29]*sp[44]*svp*svm + 8.E+0*pow(sp[35],2)*sp[44]*sp[7]*svp*svm -
2.4E+1*pow(sp[35],2)*sp[39]*sp[37]*svp*svm - 1.6E+1*pow(sp[43],2)*sp[31]*sp[29]
*svp*svm;

kern[6][0] = (1.0/ denom)* kern[6][0];
kern[6][1] =   0;

kern[6][1] = (1.0/ denom)* kern[6][1];
kern[6][2] =   - 8.E+0*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm - 8.E+0*sp[35]*sp[43]*
sp[29]*sp[44]*svp*ssm - 8.E+0*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm - 8.E+0*sp[35]*
pow(sp[43],2)*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*sp[44]*sp[39]*ssp*svm +
8.E+0*pow(sp[35],2)*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(sp[43],3)*sp[31]*ssp*svm
+ 8.E+0*pow(sp[43],3)*sp[29]*svp*ssm;

kern[6][2] = (1.0/ denom)* kern[6][2];
kern[6][3] =   - 3.2E+1*sp[35]*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm - 3.2E+1*sp[35]*
pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 3.2E+1*pow(sp[35],2)*sp[31]*sp[44]*sp[37]*
svp*svm + 3.2E+1*pow(sp[43],3)*sp[31]*sp[29]*svp*svm;

kern[6][3] = (1.0/ denom)* kern[6][3];
kern[6][4] =   - 8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm + 8.E+0*sp[35]*
pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 8.E+0*sp[35]*pow(sp[43],3)*sp[39]*ssp*svm
+ 8.E+0*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*
sp[39]*ssp*svm - 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(
sp[43],4)*sp[31]*ssp*svm - 8.E+0*pow(sp[43],4)*sp[29]*svp*ssm;

kern[6][4] = (1.0/ denom)* kern[6][4];
kern[6][5] =  1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[29]*sp[44]*svp*svm + 8.E+0*
sp[35]*pow(sp[43],3)*sp[31]*sp[37]*svp*svm + 8.E+0*sp[35]*pow(sp[43],3)*sp[29]*sp[39]
*svp*svm - 8.E+0*pow(sp[35],2)*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 8.E+0*
pow(sp[35],2)*sp[43]*sp[29]*sp[44]*sp[39]*svp*svm - 1.6E+1*pow(sp[43],4)*sp[31]*
sp[29]*svp*svm;

kern[6][5] = (1.0/ denom)* kern[6][5];
kern[6][6] =   - 8.E+0*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 8.E+0*sp[35]*
sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]*sp[7]*svp*
svm - 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]*ssp*ssm - 1.6E+1*sp[35]*pow(sp[43],2)*
sp[39]*sp[37]*svp*svm + 1.6E+1*pow(sp[35],2)*sp[44]*sp[39]*sp[37]*svp*svm -
8.E+0*pow(sp[35],2)*pow(sp[44],2)*sp[7]*svp*svm + 8.E+0*pow(sp[35],2)*pow(
sp[44],2)*ssp*ssm + 8.E+0*pow(sp[43],3)*sp[31]*sp[37]*svp*svm + 8.E+0*pow(
sp[43],3)*sp[29]*sp[39]*svp*svm - 8.E+0*pow(sp[43],4)*sp[7]*svp*svm + 8.E+0
*pow(sp[43],4)*ssp*ssm;

kern[6][6] = (1.0/ denom)* kern[6][6];
kern[6][7] =  3.2E+1*sp[35]*pow(sp[43],3)*sp[31]*sp[44]*ssp*svm + 3.2E+1*sp[35]*
pow(sp[43],3)*sp[29]*sp[44]*svp*ssm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[31]*pow(
sp[44],2)*ssp*svm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[29]*pow(sp[44],2)*svp*ssm -
1.6E+1*pow(sp[43],5)*sp[31]*ssp*svm - 1.6E+1*pow(sp[43],5)*sp[29]*svp*ssm;

kern[6][7] = (1.0/ denom)* kern[6][7];

denom =  - 6.4E+1*sp[35]*pow(sp[43],3)*sp[44] + 3.2E+1*pow(sp[35],2)*sp[43]*pow(
sp[44],2) + 3.2E+1*pow(sp[43],5);

kern[7][0] =   0;

kern[7][0] = (1.0/ denom)* kern[7][0];
kern[7][1] =  1.6E+1*sp[35]*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm - 4.8E+1*sp[35]*
pow(sp[43],2)*sp[31]*sp[37]*svp*svm - 4.8E+1*sp[35]*pow(sp[43],2)*sp[29]*sp[39]*
svp*svm + 1.6E+1*sp[35]*pow(sp[43],3)*sp[7]*svp*svm - 1.6E+1*pow(sp[35],2)
*sp[43]*sp[44]*sp[7]*svp*svm + 4.8E+1*pow(sp[35],2)*sp[43]*sp[39]*sp[37]*svp*svm
+ 3.2E+1*pow(sp[43],3)*sp[31]*sp[29]*svp*svm;

kern[7][1] = (1.0/ denom)* kern[7][1];
kern[7][2] =  1.6E+1*sp[35]*sp[43]*sp[44]*sp[7]*svp*svm - 4.8E+1*sp[35]*sp[43]*
sp[39]*sp[37]*svp*svm + 1.6E+1*sp[35]*sp[31]*sp[44]*sp[37]*svp*svm + 1.6E+1*sp[35]
*sp[29]*sp[44]*sp[39]*svp*svm - 4.8E+1*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm +
3.2E+1*pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 3.2E+1*pow(sp[43],2)*sp[29]*sp[39]
*svp*svm - 1.6E+1*pow(sp[43],3)*sp[7]*svp*svm;

kern[7][2] = (1.0/ denom)* kern[7][2];
kern[7][3] =  6.4E+1*sp[35]*sp[43]*sp[29]*sp[44]*svp*ssm + 6.4E+1*sp[35]*pow(
sp[43],2)*sp[37]*svp*ssm - 6.4E+1*pow(sp[35],2)*sp[44]*sp[37]*svp*ssm - 6.4E+1
*pow(sp[43],3)*sp[29]*svp*ssm;

kern[7][3] = (1.0/ denom)* kern[7][3];
kern[7][4] =   - 1.6E+1*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm + 1.6E+1*sp[35]*
sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 1.6E+1*pow(sp[43],3)*sp[31]*sp[37]*svp*svm
- 1.6E+1*pow(sp[43],3)*sp[29]*sp[39]*svp*svm;

kern[7][4] = (1.0/ denom)* kern[7][4];
kern[7][5] =   - 1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm - 1.6E+1*
sp[35]*pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 1.6E+1*sp[35]*pow(sp[43],3)*sp[39]*ssp*
svm - 1.6E+1*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm + 1.6E+1*pow(sp[35],2)*sp[43]*
sp[44]*sp[39]*ssp*svm + 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm +
1.6E+1*pow(sp[43],4)*sp[31]*ssp*svm + 1.6E+1*pow(sp[43],4)*sp[29]*svp*ssm;

kern[7][5] = (1.0/ denom)* kern[7][5];
kern[7][6] =   - 1.6E+1*sp[35]*sp[43]*sp[44]*sp[39]*ssp*svm - 1.6E+1*sp[35]*sp[43]*
sp[44]*sp[37]*svp*ssm + 1.6E+1*sp[35]*sp[31]*pow(sp[44],2)*ssp*svm + 1.6E+1*
sp[35]*sp[29]*pow(sp[44],2)*svp*ssm - 1.6E+1*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm
- 1.6E+1*pow(sp[43],2)*sp[29]*sp[44]*svp*ssm + 1.6E+1*pow(sp[43],3)*sp[39]*
ssp*svm + 1.6E+1*pow(sp[43],3)*sp[37]*svp*ssm;

kern[7][6] = (1.0/ denom)* kern[7][6];
kern[7][7] =   - 6.4E+1*sp[35]*sp[43]*sp[31]*sp[29]*pow(sp[44],2)*svp*svm +
6.4E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm + 6.4E+1*sp[35]*pow(
sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm - 6.4E+1*sp[35]*pow(sp[43],3)*sp[44]*sp[7]*
svp*svm - 6.4E+1*sp[35]*pow(sp[43],3)*sp[44]*ssp*ssm + 6.4E+1*sp[35]*pow(
sp[43],3)*sp[39]*sp[37]*svp*svm - 6.4E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[39]*sp[37]*
svp*svm + 3.2E+1*pow(sp[35],2)*sp[43]*pow(sp[44],2)*sp[7]*svp*svm + 3.2E+1
*pow(sp[35],2)*sp[43]*pow(sp[44],2)*ssp*ssm + 6.4E+1*pow(sp[43],3)*sp[31]*sp[29]*
sp[44]*svp*svm - 6.4E+1*pow(sp[43],4)*sp[31]*sp[37]*svp*svm - 6.4E+1*pow(
sp[43],4)*sp[29]*sp[39]*svp*svm + 3.2E+1*pow(sp[43],5)*sp[7]*svp*svm +
3.2E+1*pow(sp[43],5)*ssp*ssm;

kern[7][7] = (1.0/ denom)* kern[7][7];


}


void kernelL_axial(matCdoub& kern,
const ArrayScalarProducts& sp,
const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){


// local variables
array<Cdoub, 79> w;
Cdoub denom;


w[1]=sp[35];
w[2]=sp[27];
w[3]=sp[44];
w[4]=sp[34];
w[5]=sp[43];
w[6]=w[3]*ang[0];
w[7]=w[6] + ang[1];
w[8]=pow(w[4],2);
w[9]=w[7]*w[8];
w[10]=w[6] - ang[1];
w[11]=w[2]*ang[0];
w[12]=w[10] + w[11];
w[13]= - w[4]*w[12];
w[14]=w[11]*w[5];
w[13]=w[13] + w[14];
w[13]=w[5]*w[13];
w[9]=w[9] + w[13];
w[13]=2.E+0*ang[1];
w[15]=w[13] - w[6];
w[16]=w[15]*w[3];
w[16]=w[16] - ang[2];
w[17]=3.E+0*ang[1];
w[18]= - w[17] - w[6];
w[18]=2.E+0*w[18] + w[11];
w[18]=w[2]*w[18];
w[18]=w[18] - w[16];
w[18]=w[1]*w[18];
w[9]=4.E+0*w[9] + w[18];
w[9]=2.E+0*w[9];
w[18]=2.E+0*w[11];
w[19]=w[18] + w[10];
w[20]=w[19]*w[4];
w[20]=w[20] - w[14];
w[20]=w[20]*w[5];
w[21]=2.E+0*w[6];
w[22]=w[11] - ang[1];
w[23]=w[21] + w[22];
w[24]=w[23]*w[8];
w[20]=w[20] - w[24];
w[20]=w[20]*w[5];
w[24]=pow(w[4],3);
w[25]=w[24]*w[6];
w[20]=w[20] + w[25];
w[25]=2.E+0*w[7];
w[26]=w[25] - w[11];
w[26]=w[26]*w[2];
w[26]=w[26] + w[16];
w[27]=w[5] - w[4];
w[26]= - w[26]*w[27];
w[28]= - w[1]*w[26];
w[28]=4.E+0*w[20] + w[28];
w[28]=4.E+0*w[28];
w[29]=4.E+0*w[5];
w[20]=w[20]*w[29];
w[30]=w[1]*w[5];
w[26]=w[26]*w[30];
w[20]=w[20] - w[26];
w[26]= - 2.E+0*w[20];
w[31]=w[11] - w[6];
w[32]=w[31] - w[13];
w[33]=w[5]*w[2];
w[34]=w[32]*w[33];
w[17]=w[17] - w[11];
w[17]=w[17]*w[2];
w[35]=w[7]*w[3];
w[35]= - w[35] + 2.E+0*ang[2];
w[17]=w[17] - w[35];
w[36]=w[4]*w[17];
w[36]=w[36] + w[34];
w[36]=w[5]*w[36];
w[37]=w[13] + w[6];
w[37]=w[37]*w[3];
w[38]=w[6]*w[2];
w[37]=w[37] - w[38];
w[39]= - w[37]*w[8];
w[36]=w[39] + w[36];
w[39]=4.E+0*ang[1];
w[40]=3.E+0*w[6];
w[41]=w[39] + w[40];
w[42]=w[3]*w[41];
w[41]=w[11] - w[41];
w[41]=w[2]*w[41];
w[43]=5.E+0*ang[2];
w[41]=w[41] + w[43] + w[42];
w[41]=w[2]*w[41];
w[42]=pow(w[3],2);
w[44]=w[42]*ang[0];
w[45]= - w[44] + 3.E+0*ang[2];
w[45]=w[45]*w[3];
w[45]=w[45] - 2.E+0*ang[3];
w[41]=w[41] + w[45];
w[41]=w[1]*w[41];
w[36]=4.E+0*w[36] + w[41];
w[41]= - w[8]*w[11];
w[46]=w[4]*w[18];
w[46]=w[46] - w[14];
w[46]=w[5]*w[46];
w[41]=w[41] + w[46];
w[41]=w[41]*w[29];
w[46]=2.E+0*w[10];
w[47]= - w[46] + w[11];
w[47]=w[2]*w[47];
w[47]=w[47] - w[16];
w[47]=w[47]*w[30];
w[41]=w[41] + w[47];
w[47]=4.E+0*w[1];
w[41]=w[41]*w[47];
w[48]=w[10]*w[3];
w[49]=w[22]*w[2];
w[48]=w[48] - w[49];
w[48]=w[48]*w[4];
w[49]=2.E+0*w[33];
w[50]= - w[31]*w[49];
w[50]= - w[48] + w[50];
w[50]=8.E+0*w[1]*w[50];
w[31]=w[31]*w[33];
w[31]=w[48] + w[31];
w[31]=w[5]*w[31];
w[38]= - w[44] + w[38];
w[38]=w[38]*w[8];
w[31]=w[38] + w[31];
w[31]=w[31]*w[29];
w[38]=3.E+0*ang[0];
w[38]=w[42]*w[38];
w[42]=w[13] - w[11];
w[48]= - w[40] - w[42];
w[48]=w[2]*w[48];
w[38]=w[48] + ang[2] + w[38];
w[38]=w[2]*w[38];
w[48]=w[3]*w[16];
w[38]=w[48] + w[38];
w[38]=w[38]*w[30];
w[31]=w[31] + w[38];
w[31]=w[31]*w[47];
w[38]=w[10] - w[11];
w[27]= - w[30]*w[38]*w[27];
w[47]=2.E+0*ang[0];
w[48]=w[47]*w[8];
w[51]=w[5]*ang[0];
w[52]=w[51]*w[4];
w[48]=w[48] - w[52];
w[48]=w[48]*w[5];
w[52]=w[24]*ang[0];
w[48]=w[48] - w[52];
w[52]=2.E+0*w[5];
w[53]= - w[48]*w[52];
w[27]=w[53] + w[27];
w[27]=8.E+0*w[27];
w[53]= - w[22]*w[8];
w[54]= - ang[1] + w[18];
w[54]=w[4]*w[54];
w[54]=w[54] - w[14];
w[54]=w[5]*w[54];
w[53]=w[53] + w[54];
w[54]=w[21] - w[11];
w[55]= - w[2]*w[54];
w[55]=w[55] - ang[2] + w[44];
w[55]=w[1]*w[55];
w[53]=4.E+0*w[53] + w[55];
w[53]=2.E+0*w[53];
w[20]= - 4.E+0*w[20];
w[7]=w[7] - w[11];
w[55]=w[7]*w[8];
w[56]= - w[5]*w[38];
w[57]=3.E+0*w[4];
w[58]=ang[1]*w[57];
w[56]=w[58] + w[56];
w[56]=w[5]*w[56];
w[55]=w[55] + w[56];
w[56]=5.E+0*ang[1];
w[58]= - w[56] + w[6];
w[58]=w[3]*w[58];
w[54]= - w[56] - w[54];
w[54]=w[2]*w[54];
w[54]=w[54] + 4.E+0*ang[2] + w[58];
w[54]=w[1]*w[54];
w[54]=2.E+0*w[55] + w[54];
w[54]=w[1]*w[54];
w[48]= - w[48]*w[29];
w[48]=w[48] + w[54];
w[48]=8.E+0*w[48];
w[54]=pow(w[5],2);
w[55]= - w[4]*w[54]*w[13];
w[56]=w[2] + w[3];
w[56]=w[56]*ang[1];
w[56]=w[56] - ang[2];
w[58]=w[56]*w[30];
w[55]=w[55] + w[58];
w[55]=6.E+0*w[55];
w[58]=2.E+0*w[8];
w[19]=w[58]*w[19];
w[59]=w[57]*w[14];
w[19]=w[19] - w[59];
w[19]=w[19]*w[5];
w[23]=w[23]*w[24];
w[19]=w[19] - w[23];
w[60]=w[25] + w[11];
w[60]=w[60]*w[2];
w[16]=3.E+0*w[16];
w[60]=w[60] + w[16];
w[61]=w[60]*w[4];
w[62]=4.E+0*w[33];
w[38]=w[62]*w[38];
w[38]=w[38] + w[61];
w[38]=w[38]*w[1];
w[19]=w[38] + 4.E+0*w[19];
w[38]=6.E+0*w[1];
w[63]=w[19]*w[38];
w[64]=7.E+0*w[11];
w[65]=w[40] - ang[1];
w[65]=2.E+0*w[65];
w[66]=w[65] + w[64];
w[66]=w[2]*w[66];
w[66]= - w[16] + w[66];
w[66]=w[66]*w[57];
w[67]= - w[46] - w[11];
w[33]=w[67]*w[33];
w[33]=w[66] + 8.E+0*w[33];
w[33]=w[5]*w[33];
w[66]=8.E+0*ang[1];
w[67]= - w[66] + w[6];
w[67]=w[3]*w[67];
w[68]=w[39] + w[6];
w[69]= - 2.E+0*w[68] + w[11];
w[69]=w[2]*w[69];
w[67]=w[69] + 7.E+0*ang[2] + w[67];
w[69]=2.E+0*w[2];
w[67]=w[1]*w[67]*w[69];
w[70]=1.9E+1*ang[1] + w[6];
w[70]=2.E+0*w[70] - 1.5E+1*w[11];
w[70]=w[2]*w[70];
w[71]=3.4E+1*ang[1] - 1.1E+1*w[6];
w[71]=w[3]*w[71];
w[70]=w[70] - 2.3E+1*ang[2] + w[71];
w[70]=w[70]*w[8];
w[33]=w[67] + w[70] + w[33];
w[33]=w[1]*w[33];
w[67]=w[39] - w[40];
w[70]= - 3.E+0*w[67] + 8.E+0*w[11];
w[70]=w[70]*w[24];
w[71]=4.E+0*w[11];
w[72]= - w[71] - w[10];
w[72]=w[72]*w[8];
w[73]=w[14]*w[4];
w[72]=5.E+0*w[72] + 1.1E+1*w[73];
w[72]=w[5]*w[72];
w[70]=w[70] + w[72];
w[70]=w[5]*w[70];
w[72]=4.E+0*w[6];
w[73]= - w[72] + w[22];
w[74]=pow(w[4],4);
w[73]=w[73]*w[74];
w[70]=w[73] + w[70];
w[33]=4.E+0*w[70] + w[33];
w[33]=w[1]*w[33];
w[70]=w[74]*w[47];
w[73]= - w[24]*w[51];
w[70]=w[70] + w[73];
w[70]=w[5]*w[70];
w[73]=pow(w[4],5);
w[75]= - ang[0]*w[73];
w[70]=w[75] + w[70];
w[75]=8.E+0*w[5];
w[70]=w[70]*w[75];
w[33]=w[70] + w[33];
w[33]=4.E+0*w[33];
w[66]= - w[66] + w[40];
w[66]=w[3]*w[66];
w[70]=w[39] - w[6];
w[76]=2.E+0*w[70];
w[77]=3.E+0*w[11];
w[78]= - w[76] + w[77];
w[78]=w[2]*w[78];
w[43]=w[78] + w[43] + w[66];
w[43]=w[43]*w[8];
w[64]= - 6.E+0*w[10] - w[64];
w[64]=w[2]*w[64];
w[64]=w[16] + w[64];
w[64]=w[4]*w[64];
w[12]=w[12]*w[62];
w[12]=w[64] + w[12];
w[12]=w[5]*w[12];
w[12]=w[43] + w[12];
w[12]=w[5]*w[12];
w[43]=w[1]*w[56]*w[49];
w[12]=w[12] + w[43];
w[12]=w[1]*w[12];
w[43]=5.E+0*w[11];
w[46]=w[46] + w[43];
w[46]=w[46]*w[8];
w[46]=w[46] - w[59];
w[46]=w[5]*w[46];
w[10]=3.E+0*w[10];
w[56]=w[10] + w[18];
w[64]= - w[56]*w[24];
w[46]=w[64] + w[46];
w[46]=w[5]*w[46];
w[64]=w[74]*w[6];
w[46]=w[64] + w[46];
w[46]=w[46]*w[29];
w[12]=w[46] + w[12];
w[12]=w[12]*w[38];
w[38]= - w[17]*w[58];
w[46]= - w[57]*w[34];
w[38]=w[38] + w[46];
w[38]=w[5]*w[38];
w[37]=w[37]*w[24];
w[37]=w[37] + w[38];
w[38]=w[3]*w[70];
w[46]= - w[77] + 1.2E+1*ang[1] + w[6];
w[46]=w[2]*w[46];
w[38]=w[46] - 1.5E+1*ang[2] + w[38];
w[38]=w[2]*w[38];
w[45]=3.E+0*w[45];
w[38]= - w[45] + w[38];
w[38]=w[4]*w[38];
w[17]= - w[17]*w[62];
w[17]=w[38] + w[17];
w[17]=w[1]*w[17];
w[17]=4.E+0*w[37] + w[17];
w[17]=3.E+0*w[1]*w[17];
w[10]=w[10] - w[11];
w[10]=w[10]*w[4];
w[10]=w[10] - w[14];
w[10]=w[10]*w[5];
w[13]=w[40] - w[13];
w[37]=w[13] - w[18];
w[37]=w[37]*w[8];
w[10]=w[10] - w[37];
w[37]=w[65] - w[77];
w[37]=w[37]*w[2];
w[37]=w[37] + w[16];
w[38]=w[1]*w[37];
w[38]=4.E+0*w[10] + w[38];
w[38]=w[1]*w[38];
w[40]=w[24]*w[47];
w[46]=w[51]*w[8];
w[40]=w[40] - w[46];
w[40]=w[40]*w[5];
w[46]=w[74]*ang[0];
w[40]=w[40] - w[46];
w[46]=8.E+0*w[40];
w[38]=w[46] + w[38];
w[38]=2.E+0*w[38];
w[47]=w[60]*w[5];
w[47]=w[47] - w[61];
w[51]= - w[1]*w[47];
w[56]=w[56]*w[4];
w[14]=w[56] - w[14];
w[14]=w[14]*w[5];
w[39]= - w[39] + 5.E+0*w[6];
w[18]=w[39] + w[18];
w[18]=w[18]*w[8];
w[14]=w[14] - w[18];
w[14]=w[14]*w[5];
w[14]=w[14] + w[23];
w[18]= - 4.E+0*w[14] + w[51];
w[18]=w[1]*w[18];
w[23]=w[40]*w[75];
w[18]= - w[23] + w[18];
w[18]=4.E+0*w[18];
w[40]=w[47]*w[30];
w[14]=w[14]*w[29];
w[14]=w[14] + w[40];
w[14]=w[1]*w[14];
w[40]=w[54]*w[46];
w[14]=w[40] + w[14];
w[14]=2.E+0*w[14];
w[21]=w[11] - 7.E+0*ang[1] + w[21];
w[21]=w[2]*w[21];
w[21]=3.E+0*w[35] + w[21];
w[21]=w[4]*w[21];
w[21]=w[21] - w[34];
w[21]=w[5]*w[21];
w[15]=w[15] - w[11];
w[15]=w[15]*w[2];
w[15]=w[15] - ang[2];
w[25]=w[3]*w[25];
w[25]=w[25] + w[15];
w[25]=w[25]*w[8];
w[21]=w[25] + w[21];
w[25]= - w[3]*w[39];
w[34]=w[11] + w[68];
w[34]=w[2]*w[34];
w[25]=w[34] - 1.1E+1*ang[2] + w[25];
w[25]=w[2]*w[25];
w[25]= - w[45] + w[25];
w[25]=w[1]*w[25];
w[21]=4.E+0*w[21] + w[25];
w[21]=w[1]*w[21];
w[7]= - w[7]*w[24];
w[25]= - w[5]*w[32]*w[8];
w[7]=w[7] + w[25];
w[7]=w[7]*w[75];
w[7]=w[7] + w[21];
w[10]= - w[10]*w[29];
w[21]= - w[37]*w[30];
w[10]=w[10] + w[21];
w[10]=w[1]*w[10];
w[10]= - w[23] + w[10];
w[10]=4.E+0*w[10];
w[19]=4.E+0*w[19];
w[21]= - w[3]*w[67];
w[21]=ang[2] + w[21];
w[23]= - w[76] + w[43];
w[23]=w[2]*w[23];
w[21]=3.E+0*w[21] + w[23];
w[21]=w[4]*w[21];
w[11]= - w[11] - w[13];
w[11]=w[11]*w[49];
w[11]=w[21] + w[11];
w[11]=w[5]*w[11];
w[21]=w[2]*w[42];
w[6]=6.E+0*ang[1] - 7.E+0*w[6];
w[6]=w[3]*w[6];
w[6]=w[21] - ang[2] + w[6];
w[6]=w[6]*w[8];
w[6]=w[6] + w[11];
w[6]=w[6]*w[52];
w[11]=w[3]*w[16];
w[15]=5.E+0*w[44] + w[15];
w[15]=w[2]*w[15];
w[11]=w[11] + w[15];
w[11]=w[11]*w[30];
w[6]=w[6] + w[11];
w[6]=w[1]*w[6];
w[11]= - w[71] - w[13];
w[11]=w[11]*w[8];
w[11]=w[11] + w[59];
w[11]=w[5]*w[11];
w[13]=w[72] + w[22];
w[13]=w[13]*w[24];
w[11]=w[13] + w[11];
w[11]=w[5]*w[11];
w[11]= - w[64] + w[11];
w[11]=w[11]*w[75];
w[6]=w[11] + w[6];
w[6]=4.E+0*w[6];
w[11]=w[1]*w[2];
w[13]=w[11] - w[8];
w[15]=8.E+0*w[13];
w[11]=w[11]*w[4];
w[11]=w[11] - w[24];
w[16]=1.6E+1*w[1]*w[11];
w[13]=4.8E+1*w[1]*w[13];
w[11]= - 4.E+0*w[11];
w[21]=w[69]*w[24];
w[22]=w[1]*pow(w[2],2);
w[23]=w[22]*w[4];
w[21]=w[21] - w[23];
w[21]=w[21]*w[1];
w[21]=w[21] - w[73];
w[23]= - 2.4E+1*w[1]*w[21];
w[8]= - w[8]*w[69];
w[8]=w[8] + w[22];
w[8]=w[1]*w[8];
w[8]=w[74] + w[8];
w[8]=8.E+0*w[8];
w[21]= - 3.2E+1*w[21];


denom =  - w[15];

kern[0][0] = (1.0/ denom)* w[9];

kern[0][1] = (1.0/ denom)*0.0;

kern[0][2] = (1.0/ denom)*0.0;

kern[0][3] = (1.0/ denom)* w[28];

kern[0][4] = (1.0/ denom)*0.0;

kern[0][5] = (1.0/ denom)* w[26];

kern[0][6] = (1.0/ denom)* w[36];

kern[0][7] = (1.0/ denom)*0.0;


denom = w[16];

kern[1][0] = (1.0/ denom)*0.0;

kern[1][1] = (1.0/ denom)* w[41];

kern[1][2] = (1.0/ denom)* w[50];

kern[1][3] = (1.0/ denom)*0.0;

kern[1][4] = (1.0/ denom)*0.0;

kern[1][5] = (1.0/ denom)*0.0;

kern[1][6] = (1.0/ denom)*0.0;

kern[1][7] = (1.0/ denom)* w[31];


denom =  - w[15];

kern[2][0] = (1.0/ denom)*0.0;

kern[2][1] = (1.0/ denom)* w[27];

kern[2][2] = (1.0/ denom)* w[53];

kern[2][3] = (1.0/ denom)*0.0;

kern[2][4] = (1.0/ denom)*0.0;

kern[2][5] = (1.0/ denom)*0.0;

kern[2][6] = (1.0/ denom)*0.0;

kern[2][7] = (1.0/ denom)* w[20];


denom = w[13];

kern[3][0] = (1.0/ denom)*0.0;

kern[3][1] = (1.0/ denom)*0.0;

kern[3][2] = (1.0/ denom)*0.0;

kern[3][3] = (1.0/ denom)* w[48];

kern[3][4] = (1.0/ denom)*0.0;

kern[3][5] = (1.0/ denom)*0.0;

kern[3][6] = (1.0/ denom)*0.0;

kern[3][7] = (1.0/ denom)*0.0;


denom = w[11];

kern[4][0] = (1.0/ denom)*0.0;

kern[4][1] = (1.0/ denom)*0.0;

kern[4][2] = (1.0/ denom)*0.0;

kern[4][3] = (1.0/ denom)*0.0;

kern[4][4] = (1.0/ denom)* w[55];

kern[4][5] = (1.0/ denom)*0.0;

kern[4][6] = (1.0/ denom)*0.0;

kern[4][7] = (1.0/ denom)*0.0;


denom = w[23];

kern[5][0] = (1.0/ denom)* w[63];

kern[5][1] = (1.0/ denom)*0.0;

kern[5][2] = (1.0/ denom)*0.0;

kern[5][3] = (1.0/ denom)* w[33];

kern[5][4] = (1.0/ denom)*0.0;

kern[5][5] = (1.0/ denom)* w[12];

kern[5][6] = (1.0/ denom)* w[17];

kern[5][7] = (1.0/ denom)*0.0;


denom = w[8];

kern[6][0] = (1.0/ denom)* w[38];

kern[6][1] = (1.0/ denom)*0.0;

kern[6][2] = (1.0/ denom)*0.0;

kern[6][3] = (1.0/ denom)* w[18];

kern[6][4] = (1.0/ denom)*0.0;

kern[6][5] = (1.0/ denom)* w[14];

kern[6][6] = (1.0/ denom)* w[7];

kern[6][7] = (1.0/ denom)*0.0;


denom = w[21];

kern[7][0] = (1.0/ denom)*0.0;

kern[7][1] = (1.0/ denom)* w[10];

kern[7][2] = (1.0/ denom)* w[19];

kern[7][3] = (1.0/ denom)*0.0;

kern[7][4] = (1.0/ denom)*0.0;

kern[7][5] = (1.0/ denom)*0.0;

kern[7][6] = (1.0/ denom)*0.0;

kern[7][7] = (1.0/ denom)* w[6];


}


void kernelY_axial(matCdoub& kern,
const ArrayScalarProducts& sp,  const Cdoub& svp,
const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


// local variables
array<Cdoub, 84> w;
Cdoub denom;


w[1]=sp[35];
w[2]=sp[44];
w[3]=sp[39];
w[4]=sp[37];
w[5]=sp[43];
w[6]=sp[31];
w[7]=sp[29];
w[8]=sp[7];
w[9]=ssp*ssm;
w[10]=w[9]*w[1];
w[11]=svp*svm;
w[12]=w[11]*w[6];
w[13]=w[12]*w[7];
w[14]=w[10] - w[13];
w[15]= - w[2]*w[14];
w[16]=w[11]*w[3];
w[17]=w[7]*w[16];
w[12]=w[12]*w[4];
w[18]=w[17] + w[12];
w[19]=w[5]*w[9];
w[19]=w[19] - w[18];
w[19]=w[5]*w[19];
w[16]=w[4]*w[16];
w[20]=w[16]*w[1];
w[15]=w[19] + w[20] + w[15];
w[15]=8.E+0*w[15];
w[19]=ssp*svm;
w[21]=w[6]*w[19];
w[22]=svp*ssm;
w[23]=w[7]*w[22];
w[24]=w[21] + w[23];
w[25]=pow(w[5],2);
w[26]=w[24]*w[25];
w[27]=w[2]*w[1];
w[28]=w[24]*w[27];
w[29]=w[28] - w[26];
w[30]=8.E+0*w[5];
w[29]=w[29]*w[30];
w[31]=w[25] - w[27];
w[32]=8.E+0*w[31];
w[19]=w[3]*w[19];
w[22]=w[4]*w[22];
w[33]=w[19] + w[22];
w[34]=w[33]*w[32];
w[35]=w[17] - w[12];
w[36]= - w[35]*w[31];
w[37]=1.6E+1*w[36];
w[38]=w[5]*w[1];
w[39]= - w[33]*w[38];
w[39]=w[28] + w[39];
w[39]=1.6E+1*w[39];
w[40]=pow(w[1],2);
w[41]=w[16]*w[40];
w[14]=w[14]*w[27];
w[42]=w[25]*w[1];
w[43]= - w[9]*w[42];
w[14]=w[43] + w[41] + w[14];
w[43]=1.6E+1*w[5];
w[14]=w[14]*w[43];
w[44]=2.E+0*w[16];
w[45]=w[44]*w[38];
w[46]=w[18]*w[27];
w[45]=w[45] - w[46];
w[47]= - 1.6E+1*w[45];
w[48]=w[19] - w[22];
w[49]=w[40]*w[2];
w[50]=w[48]*w[49];
w[51]= - w[48]*w[42];
w[51]=w[50] + w[51];
w[51]=3.2E+1*w[51];
w[52]=w[33]*w[1];
w[53]=w[24]*w[5];
w[52]=w[52] - w[53];
w[53]=8.E+0*w[52];
w[54]=w[18]*w[1];
w[55]=2.E+0*w[13];
w[56]=w[55]*w[5];
w[56]=w[56] - w[54];
w[57]=w[56]*w[30];
w[10]= - w[13] - w[10];
w[10]=w[2]*w[10];
w[58]=w[25]*w[9];
w[10]=w[58] + w[20] + w[10];
w[10]=8.E+0*w[10];
w[58]=w[21] - w[23];
w[59]=w[58]*w[25];
w[60]=w[58]*w[27];
w[59]=w[59] - w[60];
w[61]=1.6E+1*w[59];
w[62]= - 1.6E+1*w[1]*w[35];
w[63]= - w[48]*w[40];
w[64]=w[58]*w[38];
w[63]=w[63] + w[64];
w[63]=w[63]*w[43];
w[64]=w[38]*w[48];
w[64]=w[64] - w[60];
w[65]=1.6E+1*w[64];
w[11]=w[11]*w[8];
w[66]=w[11] + 3.E+0*w[9];
w[67]= - w[1]*w[66];
w[67]=w[55] + w[67];
w[67]=w[5]*w[67];
w[67]= - 2.E+0*w[54] + w[67];
w[67]=w[5]*w[67];
w[68]=2.E+0*w[41];
w[66]=w[66]*w[49];
w[66]=w[67] + w[68] + w[66];
w[66]=1.6E+1*w[66];
w[67]=w[58]*w[5];
w[69]=w[48]*w[1];
w[67]=w[67] - w[69];
w[69]=4.E+0*w[67];
w[70]=4.E+0*w[35]*w[38];
w[71]=4.E+0*w[5];
w[35]= - w[35]*w[71];
w[72]= - 8.E+0*w[59];
w[73]=w[31]*w[71];
w[74]=w[9] + w[11];
w[75]=w[74]*w[73];
w[59]=w[59]*w[71];
w[31]= - 4.E+0*w[48]*w[31];
w[48]= - w[36]*w[30];
w[71]=3.E+0*w[41];
w[76]=3.E+0*w[13];
w[77]=w[11]*w[1];
w[78]=w[76] - w[77];
w[79]= - w[78]*w[27];
w[79]= - w[71] + w[79];
w[80]=2.E+0*w[17] + w[12];
w[80]=w[1]*w[80];
w[81]=w[11]*w[38];
w[80]=4.E+0*w[80] - 3.E+0*w[81];
w[80]=w[5]*w[80];
w[79]=3.E+0*w[79] + w[80];
w[79]=w[5]*w[79];
w[80]=w[17] + 5.E+0*w[12];
w[80]=w[80]*w[49];
w[79]=w[80] + w[79];
w[79]=8.E+0*w[79];
w[19]=w[19] + 5.E+0*w[22];
w[40]= - w[19]*w[40];
w[21]=w[21] + 5.E+0*w[23];
w[80]=w[21]*w[38];
w[40]=w[40] + w[80];
w[40]=w[5]*w[40];
w[21]= - w[21]*w[49];
w[21]=w[21] + w[40];
w[21]=w[5]*w[21];
w[40]=pow(w[1],3);
w[19]=w[2]*w[19]*w[40];
w[19]=w[19] + w[21];
w[19]=w[19]*w[30];
w[21]=w[64]*w[5];
w[21]=w[21] - w[50];
w[21]=w[21]*w[5];
w[64]=pow(w[27],2);
w[58]=w[64]*w[58];
w[21]=w[21] + w[58];
w[58]= - 1.6E+1*w[21];
w[17]= - w[17] + 2.E+0*w[12];
w[80]= - w[1]*w[17];
w[81]=2.E+0*w[77];
w[82]= - w[13] + w[81];
w[82]=w[5]*w[82];
w[80]=w[80] + w[82];
w[80]=w[5]*w[80];
w[82]=w[77] - w[13];
w[82]=w[82]*w[27];
w[41]=w[80] - w[41] - 4.E+0*w[82];
w[41]=w[5]*w[41];
w[17]=w[17]*w[49];
w[17]=w[17] + w[41];
w[17]=w[5]*w[17];
w[16]=w[40]*w[16];
w[41]= - w[76] + w[81];
w[41]=w[41]*w[49];
w[16]=w[16] + w[41];
w[16]=w[2]*w[16];
w[16]=w[16] + w[17];
w[16]=3.2E+1*w[16];
w[17]=2.4E+1*w[5];
w[21]= - w[21]*w[17];
w[9]=w[9] - w[11];
w[38]=w[9]*w[38];
w[38]=w[54] + w[38];
w[38]=w[5]*w[38];
w[41]=w[9]*w[1];
w[76]= - w[13] - w[41];
w[80]=2.E+0*w[27];
w[76]=w[76]*w[80];
w[38]=w[76] + w[38];
w[38]=w[5]*w[38];
w[76]=w[18]*w[49];
w[38]= - w[76] + w[38];
w[38]=w[5]*w[38];
w[41]=w[55] + w[41];
w[41]=w[41]*w[64];
w[38]=w[41] + w[38];
w[38]=w[38]*w[17];
w[41]= - w[5]*w[45];
w[44]=w[49]*w[44];
w[41]=w[44] + w[41];
w[41]=w[5]*w[41];
w[44]= - w[18]*w[64];
w[41]=w[44] + w[41];
w[41]=2.4E+1*w[41];
w[44]=pow(w[2],2);
w[40]=w[44]*w[40];
w[45]= - w[33]*w[40];
w[81]=w[33]*w[49];
w[83]= - w[33]*w[42];
w[83]=2.E+0*w[81] + w[83];
w[83]=w[83]*w[25];
w[45]=w[45] + w[83];
w[45]=4.8E+1*w[5]*w[45];
w[77]=w[55] + w[77];
w[77]=w[77]*w[5];
w[54]=w[77] - 3.E+0*w[54];
w[54]=w[54]*w[5];
w[54]=w[54] - w[82] + w[71];
w[71]= - 8.E+0*w[54];
w[52]=w[52]*w[5];
w[52]=w[52] + w[28];
w[52]=w[52]*w[5];
w[52]=w[52] - w[81];
w[77]= - 8.E+0*w[52];
w[81]= - w[27]*w[13];
w[82]= - w[1]*w[12];
w[83]=w[5]*w[13];
w[82]=w[82] + w[83];
w[82]=w[5]*w[82];
w[81]=w[81] + w[82];
w[81]=w[5]*w[81];
w[12]=w[12]*w[49];
w[12]=w[12] + w[81];
w[12]=3.2E+1*w[12];
w[67]=w[5]*w[67];
w[60]= - w[60] + w[67];
w[60]=w[5]*w[60];
w[50]=w[50] + w[60];
w[50]=w[50]*w[30];
w[60]=w[27]*w[55];
w[56]= - w[5]*w[56];
w[56]=w[60] + w[56];
w[56]=w[5]*w[56];
w[56]= - w[76] + w[56];
w[30]=w[56]*w[30];
w[56]=w[9]*w[49];
w[56]=w[68] + w[56];
w[56]=w[2]*w[56];
w[60]=w[5]*w[9];
w[60]=w[60] + w[18];
w[60]=w[5]*w[60];
w[9]= - w[9]*w[27];
w[9]= - w[20] + w[9];
w[9]=2.E+0*w[9] + w[60];
w[9]=w[5]*w[9];
w[9]= - w[46] + w[9];
w[9]=w[5]*w[9];
w[9]=w[56] + w[9];
w[9]=8.E+0*w[9];
w[26]=2.E+0*w[28] - w[26];
w[26]=w[26]*w[25];
w[28]= - w[24]*w[64];
w[26]=w[28] + w[26];
w[26]=w[26]*w[43];
w[28]=w[54]*w[43];
w[54]= - w[2]*w[78];
w[18]=2.E+0*w[18];
w[11]= - w[5]*w[11];
w[11]=w[18] + w[11];
w[11]=w[5]*w[11];
w[11]=w[11] - 3.E+0*w[20] + w[54];
w[11]=w[5]*w[11];
w[11]=w[46] + w[11];
w[11]=1.6E+1*w[11];
w[54]=w[27]*w[23];
w[56]=w[1]*w[22];
w[23]= - w[5]*w[23];
w[23]=w[56] + w[23];
w[23]=w[5]*w[23];
w[23]=w[54] + w[23];
w[23]=w[5]*w[23];
w[22]= - w[22]*w[49];
w[22]=w[22] + w[23];
w[22]=6.4E+1*w[22];
w[23]=w[36]*w[43];
w[36]= - w[52]*w[43];
w[52]= - w[2]*w[24];
w[54]=w[5]*w[33];
w[52]=w[52] + w[54];
w[52]=w[5]*w[52];
w[33]= - w[33]*w[27];
w[33]=w[33] + w[52];
w[33]=w[5]*w[33];
w[24]=w[1]*w[24]*w[44];
w[24]=w[24] + w[33];
w[24]=1.6E+1*w[24];
w[33]=w[74]*w[1];
w[44]= - w[55] + w[33];
w[27]=w[44]*w[27];
w[27]= - w[68] + w[27];
w[27]=w[2]*w[27];
w[44]=w[5]*w[74];
w[18]= - w[18] + w[44];
w[18]=w[5]*w[18];
w[13]=w[13] - w[33];
w[13]=w[2]*w[13];
w[13]=w[20] + w[13];
w[13]=2.E+0*w[13] + w[18];
w[13]=w[5]*w[13];
w[13]=2.E+0*w[46] + w[13];
w[13]=w[5]*w[13];
w[13]=w[27] + w[13];
w[18]=3.2E+1*w[5];
w[13]=w[13]*w[18];
w[20]=w[42] - w[49];
w[27]= - w[20]*w[43];
w[20]= - 4.8E+1*w[20];
w[33]= - 2.E+0*w[49] + w[42];
w[33]=w[33]*w[25];
w[33]=w[40] + w[33];
w[17]=w[33]*w[17];
w[33]=w[80] - w[25];
w[25]=w[33]*w[25];
w[25]=w[25] - w[64];
w[33]= - 8.E+0*w[25];
w[18]= - w[25]*w[18];


denom = w[32];

kern[0][0] = (1.0/ denom)* w[15];

kern[0][1] = (1.0/ denom)* w[29];

kern[0][2] = (1.0/ denom)* w[34];

kern[0][3] = (1.0/ denom)* w[37];

kern[0][4] = (1.0/ denom)*0.0;

kern[0][5] = (1.0/ denom)*0.0;

kern[0][6] = (1.0/ denom)*0.0;

kern[0][7] = (1.0/ denom)*0.0;


denom = w[27];

kern[1][0] = (1.0/ denom)* w[39];

kern[1][1] = (1.0/ denom)* w[14];

kern[1][2] = (1.0/ denom)* w[47];

kern[1][3] = (1.0/ denom)* w[51];

kern[1][4] = (1.0/ denom)*0.0;

kern[1][5] = (1.0/ denom)*0.0;

kern[1][6] = (1.0/ denom)*0.0;

kern[1][7] = (1.0/ denom)*0.0;


denom = w[32];

kern[2][0] = (1.0/ denom)* w[53];

kern[2][1] = (1.0/ denom)* w[57];

kern[2][2] = (1.0/ denom)* w[10];

kern[2][3] = (1.0/ denom)* w[61];

kern[2][4] = (1.0/ denom)*0.0;

kern[2][5] = (1.0/ denom)*0.0;

kern[2][6] = (1.0/ denom)*0.0;

kern[2][7] = (1.0/ denom)*0.0;


denom = w[20];

kern[3][0] = (1.0/ denom)* w[62];

kern[3][1] = (1.0/ denom)* w[63];

kern[3][2] = (1.0/ denom)* w[65];

kern[3][3] = (1.0/ denom)* w[66];

kern[3][4] = (1.0/ denom)*0.0;

kern[3][5] = (1.0/ denom)*0.0;

kern[3][6] = (1.0/ denom)*0.0;

kern[3][7] = (1.0/ denom)*0.0;


denom = w[73];

kern[4][0] = (1.0/ denom)* w[69];

kern[4][1] = (1.0/ denom)* w[70];

kern[4][2] = (1.0/ denom)* w[35];

kern[4][3] = (1.0/ denom)* w[72];

kern[4][4] = (1.0/ denom)* w[75];

kern[4][5] = (1.0/ denom)* w[59];

kern[4][6] = (1.0/ denom)* w[31];

kern[4][7] = (1.0/ denom)* w[48];


denom = w[17];

kern[5][0] = (1.0/ denom)* w[79];

kern[5][1] = (1.0/ denom)* w[19];

kern[5][2] = (1.0/ denom)* w[58];

kern[5][3] = (1.0/ denom)* w[16];

kern[5][4] = (1.0/ denom)* w[21];

kern[5][5] = (1.0/ denom)* w[38];

kern[5][6] = (1.0/ denom)* w[41];

kern[5][7] = (1.0/ denom)* w[45];


denom = w[33];

kern[6][0] = (1.0/ denom)* w[71];

kern[6][1] = (1.0/ denom)*0.0;

kern[6][2] = (1.0/ denom)* w[77];

kern[6][3] = (1.0/ denom)* w[12];

kern[6][4] = (1.0/ denom)* w[50];

kern[6][5] = (1.0/ denom)* w[30];

kern[6][6] = (1.0/ denom)* w[9];

kern[6][7] = (1.0/ denom)* w[26];


denom = w[18];

kern[7][0] = (1.0/ denom)*0.0;

kern[7][1] = (1.0/ denom)* w[28];

kern[7][2] = (1.0/ denom)* w[11];

kern[7][3] = (1.0/ denom)* w[22];

kern[7][4] = (1.0/ denom)* w[23];

kern[7][5] = (1.0/ denom)* w[36];

kern[7][6] = (1.0/ denom)* w[24];

kern[7][7] = (1.0/ denom)* w[13];



}

