#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
//#include <math_ops.h>

void kernel_rotationY_axial(matCdoub& kern,
const ArrayScalarProducts& sp,  const Cdoub& svp,
const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


// local variables
array<Cdoub, 84> w;
Cdoub denom;

w[1]=sp[35];
w[2]=sp[27];
w[3]=sp[25];
w[4]=sp[23];
w[5]=sp[34];
w[6]=sp[32];
w[7]=sp[30];
w[8]=sp[12];
w[9]=ssp*ssm;
w[10]=w[9]*w[1];
w[11]=svp*svm;
w[12]=w[11]*w[6];
w[13]=w[12]*w[7];
w[14]=w[10] - w[13];
w[15]= - w[2]*w[14];
w[16]=w[11]*w[3];
w[17]=w[7]*w[16];
w[12]=w[12]*w[4];
w[18]=w[17] + w[12];
w[19]=w[5]*w[9];
w[19]=w[19] - w[18];
w[19]=w[5]*w[19];
w[16]=w[4]*w[16];
w[20]=w[16]*w[1];
w[15]=w[19] + w[20] + w[15];
w[15]=8.E+0*w[15];
w[19]=ssp*svm;
w[21]=w[6]*w[19];
w[22]=svp*ssm;
w[23]=w[7]*w[22];
w[24]=w[21] + w[23];
w[25]=pow(w[5],2);
w[26]=w[24]*w[25];
w[27]=w[2]*w[1];
w[28]=w[24]*w[27];
w[29]=w[28] - w[26];
w[30]=8.E+0*w[5];
w[29]=w[29]*w[30];
w[31]=w[25] - w[27];
w[32]=8.E+0*w[31];
w[19]=w[3]*w[19];
w[22]=w[4]*w[22];
w[33]=w[19] + w[22];
w[34]=w[33]*w[32];
w[35]=w[17] - w[12];
w[36]= - w[35]*w[31];
w[37]=1.6E+1*w[36];
w[38]=w[5]*w[1];
w[39]= - w[33]*w[38];
w[39]=w[28] + w[39];
w[39]=1.6E+1*w[39];
w[40]=pow(w[1],2);
w[41]=w[16]*w[40];
w[14]=w[14]*w[27];
w[42]=w[25]*w[1];
w[43]= - w[9]*w[42];
w[14]=w[43] + w[41] + w[14];
w[43]=1.6E+1*w[5];
w[14]=w[14]*w[43];
w[44]=2.E+0*w[16];
w[45]=w[44]*w[38];
w[46]=w[18]*w[27];
w[45]=w[45] - w[46];
w[47]= - 1.6E+1*w[45];
w[48]=w[19] - w[22];
w[49]=w[40]*w[2];
w[50]=w[48]*w[49];
w[51]= - w[48]*w[42];
w[51]=w[50] + w[51];
w[51]=3.2E+1*w[51];
w[52]=w[33]*w[1];
w[53]=w[24]*w[5];
w[52]=w[52] - w[53];
w[53]=8.E+0*w[52];
w[54]=w[18]*w[1];
w[55]=2.E+0*w[13];
w[56]=w[55]*w[5];
w[56]=w[56] - w[54];
w[57]=w[56]*w[30];
w[10]= - w[13] - w[10];
w[10]=w[2]*w[10];
w[58]=w[25]*w[9];
w[10]=w[58] + w[20] + w[10];
w[10]=8.E+0*w[10];
w[58]=w[21] - w[23];
w[59]=w[58]*w[25];
w[60]=w[58]*w[27];
w[59]=w[59] - w[60];
w[61]=1.6E+1*w[59];
w[62]= - 1.6E+1*w[1]*w[35];
w[63]= - w[48]*w[40];
w[64]=w[58]*w[38];
w[63]=w[63] + w[64];
w[63]=w[63]*w[43];
w[64]=w[38]*w[48];
w[64]=w[64] - w[60];
w[65]=1.6E+1*w[64];
w[11]=w[11]*w[8];
w[66]=w[11] + 3.E+0*w[9];
w[67]= - w[1]*w[66];
w[67]=w[55] + w[67];
w[67]=w[5]*w[67];
w[67]= - 2.E+0*w[54] + w[67];
w[67]=w[5]*w[67];
w[68]=2.E+0*w[41];
w[66]=w[66]*w[49];
w[66]=w[67] + w[68] + w[66];
w[66]=1.6E+1*w[66];
w[67]=w[58]*w[5];
w[69]=w[48]*w[1];
w[67]=w[67] - w[69];
w[69]=4.E+0*w[67];
w[70]=4.E+0*w[35]*w[38];
w[71]=4.E+0*w[5];
w[35]= - w[35]*w[71];
w[72]= - 8.E+0*w[59];
w[73]=w[31]*w[71];
w[74]=w[9] + w[11];
w[75]=w[74]*w[73];
w[59]=w[59]*w[71];
w[31]= - 4.E+0*w[48]*w[31];
w[48]= - w[36]*w[30];
w[71]=3.E+0*w[41];
w[76]=3.E+0*w[13];
w[77]=w[11]*w[1];
w[78]=w[76] - w[77];
w[79]= - w[78]*w[27];
w[79]= - w[71] + w[79];
w[80]=2.E+0*w[17] + w[12];
w[80]=w[1]*w[80];
w[81]=w[11]*w[38];
w[80]=4.E+0*w[80] - 3.E+0*w[81];
w[80]=w[5]*w[80];
w[79]=3.E+0*w[79] + w[80];
w[79]=w[5]*w[79];
w[80]=w[17] + 5.E+0*w[12];
w[80]=w[80]*w[49];
w[79]=w[80] + w[79];
w[79]=8.E+0*w[79];
w[19]=w[19] + 5.E+0*w[22];
w[40]= - w[19]*w[40];
w[21]=w[21] + 5.E+0*w[23];
w[80]=w[21]*w[38];
w[40]=w[40] + w[80];
w[40]=w[5]*w[40];
w[21]= - w[21]*w[49];
w[21]=w[21] + w[40];
w[21]=w[5]*w[21];
w[40]=pow(w[1],3);
w[19]=w[2]*w[19]*w[40];
w[19]=w[19] + w[21];
w[19]=w[19]*w[30];
w[21]=w[64]*w[5];
w[21]=w[21] - w[50];
w[21]=w[21]*w[5];
w[64]=pow(w[27],2);
w[58]=w[64]*w[58];
w[21]=w[21] + w[58];
w[58]= - 1.6E+1*w[21];
w[17]= - w[17] + 2.E+0*w[12];
w[80]= - w[1]*w[17];
w[81]=2.E+0*w[77];
w[82]= - w[13] + w[81];
w[82]=w[5]*w[82];
w[80]=w[80] + w[82];
w[80]=w[5]*w[80];
w[82]=w[77] - w[13];
w[82]=w[82]*w[27];
w[41]=w[80] - w[41] - 4.E+0*w[82];
w[41]=w[5]*w[41];
w[17]=w[17]*w[49];
w[17]=w[17] + w[41];
w[17]=w[5]*w[17];
w[16]=w[40]*w[16];
w[41]= - w[76] + w[81];
w[41]=w[41]*w[49];
w[16]=w[16] + w[41];
w[16]=w[2]*w[16];
w[16]=w[16] + w[17];
w[16]=3.2E+1*w[16];
w[17]=2.4E+1*w[5];
w[21]= - w[21]*w[17];
w[9]=w[9] - w[11];
w[38]=w[9]*w[38];
w[38]=w[54] + w[38];
w[38]=w[5]*w[38];
w[41]=w[9]*w[1];
w[76]= - w[13] - w[41];
w[80]=2.E+0*w[27];
w[76]=w[76]*w[80];
w[38]=w[76] + w[38];
w[38]=w[5]*w[38];
w[76]=w[18]*w[49];
w[38]= - w[76] + w[38];
w[38]=w[5]*w[38];
w[41]=w[55] + w[41];
w[41]=w[41]*w[64];
w[38]=w[41] + w[38];
w[38]=w[38]*w[17];
w[41]= - w[5]*w[45];
w[44]=w[49]*w[44];
w[41]=w[44] + w[41];
w[41]=w[5]*w[41];
w[44]= - w[18]*w[64];
w[41]=w[44] + w[41];
w[41]=2.4E+1*w[41];
w[44]=pow(w[2],2);
w[40]=w[44]*w[40];
w[45]= - w[33]*w[40];
w[81]=w[33]*w[49];
w[83]= - w[33]*w[42];
w[83]=2.E+0*w[81] + w[83];
w[83]=w[83]*w[25];
w[45]=w[45] + w[83];
w[45]=4.8E+1*w[5]*w[45];
w[77]=w[55] + w[77];
w[77]=w[77]*w[5];
w[54]=w[77] - 3.E+0*w[54];
w[54]=w[54]*w[5];
w[54]=w[54] - w[82] + w[71];
w[71]= - 8.E+0*w[54];
w[52]=w[52]*w[5];
w[52]=w[52] + w[28];
w[52]=w[52]*w[5];
w[52]=w[52] - w[81];
w[77]= - 8.E+0*w[52];
w[81]= - w[27]*w[13];
w[82]= - w[1]*w[12];
w[83]=w[5]*w[13];
w[82]=w[82] + w[83];
w[82]=w[5]*w[82];
w[81]=w[81] + w[82];
w[81]=w[5]*w[81];
w[12]=w[12]*w[49];
w[12]=w[12] + w[81];
w[12]=3.2E+1*w[12];
w[67]=w[5]*w[67];
w[60]= - w[60] + w[67];
w[60]=w[5]*w[60];
w[50]=w[50] + w[60];
w[50]=w[50]*w[30];
w[60]=w[27]*w[55];
w[56]= - w[5]*w[56];
w[56]=w[60] + w[56];
w[56]=w[5]*w[56];
w[56]= - w[76] + w[56];
w[30]=w[56]*w[30];
w[56]=w[9]*w[49];
w[56]=w[68] + w[56];
w[56]=w[2]*w[56];
w[60]=w[5]*w[9];
w[60]=w[60] + w[18];
w[60]=w[5]*w[60];
w[9]= - w[9]*w[27];
w[9]= - w[20] + w[9];
w[9]=2.E+0*w[9] + w[60];
w[9]=w[5]*w[9];
w[9]= - w[46] + w[9];
w[9]=w[5]*w[9];
w[9]=w[56] + w[9];
w[9]=8.E+0*w[9];
w[26]=2.E+0*w[28] - w[26];
w[26]=w[26]*w[25];
w[28]= - w[24]*w[64];
w[26]=w[28] + w[26];
w[26]=w[26]*w[43];
w[28]=w[54]*w[43];
w[54]= - w[2]*w[78];
w[18]=2.E+0*w[18];
w[11]= - w[5]*w[11];
w[11]=w[18] + w[11];
w[11]=w[5]*w[11];
w[11]=w[11] - 3.E+0*w[20] + w[54];
w[11]=w[5]*w[11];
w[11]=w[46] + w[11];
w[11]=1.6E+1*w[11];
w[54]=w[27]*w[23];
w[56]=w[1]*w[22];
w[23]= - w[5]*w[23];
w[23]=w[56] + w[23];
w[23]=w[5]*w[23];
w[23]=w[54] + w[23];
w[23]=w[5]*w[23];
w[22]= - w[22]*w[49];
w[22]=w[22] + w[23];
w[22]=6.4E+1*w[22];
w[23]=w[36]*w[43];
w[36]= - w[52]*w[43];
w[52]= - w[2]*w[24];
w[54]=w[5]*w[33];
w[52]=w[52] + w[54];
w[52]=w[5]*w[52];
w[33]= - w[33]*w[27];
w[33]=w[33] + w[52];
w[33]=w[5]*w[33];
w[24]=w[1]*w[24]*w[44];
w[24]=w[24] + w[33];
w[24]=1.6E+1*w[24];
w[33]=w[74]*w[1];
w[44]= - w[55] + w[33];
w[27]=w[44]*w[27];
w[27]= - w[68] + w[27];
w[27]=w[2]*w[27];
w[44]=w[5]*w[74];
w[18]= - w[18] + w[44];
w[18]=w[5]*w[18];
w[13]=w[13] - w[33];
w[13]=w[2]*w[13];
w[13]=w[20] + w[13];
w[13]=2.E+0*w[13] + w[18];
w[13]=w[5]*w[13];
w[13]=2.E+0*w[46] + w[13];
w[13]=w[5]*w[13];
w[13]=w[27] + w[13];
w[18]=3.2E+1*w[5];
w[13]=w[13]*w[18];
w[20]=w[42] - w[49];
w[27]= - w[20]*w[43];
w[20]= - 4.8E+1*w[20];
w[33]= - 2.E+0*w[49] + w[42];
w[33]=w[33]*w[25];
w[33]=w[40] + w[33];
w[17]=w[33]*w[17];
w[33]=w[80] - w[25];
w[25]=w[33]*w[25];
w[25]=w[25] - w[64];
w[33]= - 8.E+0*w[25];
w[18]= - w[25]*w[18];


denom = w[32];

kern[0][0] = (1.0/ denom)* w[15];

kern[0][1] = (1.0/ denom)* w[29];

kern[0][2] = (1.0/ denom)* w[34];

kern[0][3] = (1.0/ denom)* w[37];

kern[0][4] = (1.0/ denom)*0.0;

kern[0][5] = (1.0/ denom)*0.0;

kern[0][6] = (1.0/ denom)*0.0;

kern[0][7] = (1.0/ denom)*0.0;


denom = w[27];

kern[1][0] = (1.0/ denom)* w[39];

kern[1][1] = (1.0/ denom)* w[14];

kern[1][2] = (1.0/ denom)* w[47];

kern[1][3] = (1.0/ denom)* w[51];

kern[1][4] = (1.0/ denom)*0.0;

kern[1][5] = (1.0/ denom)*0.0;

kern[1][6] = (1.0/ denom)*0.0;

kern[1][7] = (1.0/ denom)*0.0;


denom = w[32];

kern[2][0] = (1.0/ denom)* w[53];

kern[2][1] = (1.0/ denom)* w[57];

kern[2][2] = (1.0/ denom)* w[10];

kern[2][3] = (1.0/ denom)* w[61];

kern[2][4] = (1.0/ denom)*0.0;

kern[2][5] = (1.0/ denom)*0.0;

kern[2][6] = (1.0/ denom)*0.0;

kern[2][7] = (1.0/ denom)*0.0;


denom = w[20];

kern[3][0] = (1.0/ denom)* w[62];

kern[3][1] = (1.0/ denom)* w[63];

kern[3][2] = (1.0/ denom)* w[65];

kern[3][3] = (1.0/ denom)* w[66];

kern[3][4] = (1.0/ denom)*0.0;

kern[3][5] = (1.0/ denom)*0.0;

kern[3][6] = (1.0/ denom)*0.0;

kern[3][7] = (1.0/ denom)*0.0;


denom = w[73];

kern[4][0] = (1.0/ denom)* w[69];

kern[4][1] = (1.0/ denom)* w[70];

kern[4][2] = (1.0/ denom)* w[35];

kern[4][3] = (1.0/ denom)* w[72];

kern[4][4] = (1.0/ denom)* w[75];

kern[4][5] = (1.0/ denom)* w[59];

kern[4][6] = (1.0/ denom)* w[31];

kern[4][7] = (1.0/ denom)* w[48];


denom = w[17];

kern[5][0] = (1.0/ denom)* w[79];

kern[5][1] = (1.0/ denom)* w[19];

kern[5][2] = (1.0/ denom)* w[58];

kern[5][3] = (1.0/ denom)* w[16];

kern[5][4] = (1.0/ denom)* w[21];

kern[5][5] = (1.0/ denom)* w[38];

kern[5][6] = (1.0/ denom)* w[41];

kern[5][7] = (1.0/ denom)* w[45];


denom = w[33];

kern[6][0] = (1.0/ denom)* w[71];

kern[6][1] = (1.0/ denom)*0.0;

kern[6][2] = (1.0/ denom)* w[77];

kern[6][3] = (1.0/ denom)* w[12];

kern[6][4] = (1.0/ denom)* w[50];

kern[6][5] = (1.0/ denom)* w[30];

kern[6][6] = (1.0/ denom)* w[9];

kern[6][7] = (1.0/ denom)* w[26];


denom = w[18];

kern[7][0] = (1.0/ denom)*0.0;

kern[7][1] = (1.0/ denom)* w[28];

kern[7][2] = (1.0/ denom)* w[11];

kern[7][3] = (1.0/ denom)* w[22];

kern[7][4] = (1.0/ denom)* w[23];

kern[7][5] = (1.0/ denom)* w[36];

kern[7][6] = (1.0/ denom)* w[24];

kern[7][7] = (1.0/ denom)* w[13];


}
