#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
 
void normalisation_notopti_axial(matCdoub& kern,
  Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
  Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
  const Cdoub& svp, const Cdoub& ssp, 
  const Cdoub& svm, const Cdoub& ssm ){
 
 
 
 
    kern[0][0] = 1.2E+1*ssp*ssm - 8.E+0*pow(P_P,-1)*P_qm*P_qp*svp*svm
       - 4.E+0*qm_qp*svp*svm;

    kern[0][1] =  - 1.2E+1*P_l*P_qm*ssp*svm - 1.2E+1*P_l*P_qp*svp*ssm;

    kern[0][2] = 4.E+0*pow(P_P,-1)*P_l*P_qm*ssp*svm + 4.E+0*pow(P_P,-1)
      *P_l*P_qp*svp*ssm + 8.E+0*l_qm*ssp*svm + 8.E+0*l_qp*svp*ssm;

    kern[0][3] = 1.6E+1*pow(P_P,-1)*P_l*P_qm*P_qp*svp*svm + 8.E+0*P_qm*
      l_qp*svp*svm - 2.4E+1*P_qp*l_qm*svp*svm;

    kern[0][4] = 4.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*ssp*svm - 4.E+0*pow(
      P_P,-1)*pow(P_l,2)*P_qp*svp*ssm - 4.E+0*P_l*l_qm*ssp*svm + 4.E+0*
      P_l*l_qp*svp*ssm;

    kern[0][5] =  - 8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*P_qp*svp*svm + 
      4.E+0*P_l*P_qm*l_qp*svp*svm + 4.E+0*P_l*P_qp*l_qm*svp*svm;

    kern[0][6] = 4.E+0*pow(P_P,-1)*P_l*P_qm*l_qp*svp*svm + 4.E+0*pow(
      P_P,-1)*P_l*P_qp*l_qm*svp*svm - 4.E+0*pow(P_P,-1)*pow(P_l,2)*
      qm_qp*svp*svm + 4.E+0*pow(P_P,-1)*pow(P_l,2)*ssp*ssm + 4.E+0*l_l*
      qm_qp*svp*svm - 4.E+0*l_l*ssp*ssm - 8.E+0*l_qm*l_qp*svp*svm;

    kern[0][7] =  - 8.E+0*pow(P_P,-1)*pow(P_l,3)*P_qm*ssp*svm - 8.E+0*
      pow(P_P,-1)*pow(P_l,3)*P_qp*svp*ssm + 8.E+0*P_l*P_qm*l_l*ssp*svm
       + 8.E+0*P_l*P_qp*l_l*svp*ssm;

 
    kern[1][0] = 1.2E+1*P_l*P_qm*ssp*svm + 1.2E+1*P_l*P_qp*svp*ssm;

    kern[1][1] = 4.E+0*P_P*pow(P_l,2)*qm_qp*svp*svm + 1.2E+1*P_P*pow(
      P_l,2)*ssp*ssm - 1.6E+1*pow(P_l,2)*P_qm*P_qp*svp*svm;

    kern[1][2] = 8.E+0*P_l*P_qm*l_qp*svp*svm + 8.E+0*P_l*P_qp*l_qm*svp*
      svm - 4.E+0*pow(P_l,2)*qm_qp*svp*svm - 1.2E+1*pow(P_l,2)*ssp*ssm;

    kern[1][3] = 2.4E+1*P_P*P_l*l_qm*ssp*svm - 8.E+0*P_P*P_l*l_qp*svp*
      ssm - 2.4E+1*pow(P_l,2)*P_qm*ssp*svm + 8.E+0*pow(P_l,2)*P_qp*svp*
      ssm;

    kern[1][4] = 4.E+0*pow(P_l,2)*P_qm*l_qp*svp*svm - 4.E+0*pow(P_l,2)*
      P_qp*l_qm*svp*svm;

    kern[1][5] =  - 4.E+0*P_P*pow(P_l,2)*l_qm*ssp*svm - 4.E+0*P_P*pow(
      P_l,2)*l_qp*svp*ssm + 4.E+0*pow(P_l,3)*P_qm*ssp*svm + 4.E+0*pow(
      P_l,3)*P_qp*svp*ssm;

    kern[1][6] =  - 4.E+0*P_l*P_qm*l_l*ssp*svm - 4.E+0*P_l*P_qp*l_l*svp
      *ssm + 4.E+0*pow(P_l,2)*l_qm*ssp*svm + 4.E+0*pow(P_l,2)*l_qp*svp*
      ssm;

    kern[1][7] =  - 8.E+0*P_P*pow(P_l,2)*l_l*qm_qp*svp*svm - 8.E+0*P_P*
      pow(P_l,2)*l_l*ssp*ssm + 1.6E+1*P_P*pow(P_l,2)*l_qm*l_qp*svp*svm
       + 1.6E+1*pow(P_l,2)*P_qm*P_qp*l_l*svp*svm - 1.6E+1*pow(P_l,3)*
      P_qm*l_qp*svp*svm - 1.6E+1*pow(P_l,3)*P_qp*l_qm*svp*svm + 8.E+0*
      pow(P_l,4)*qm_qp*svp*svm + 8.E+0*pow(P_l,4)*ssp*ssm;

 
    kern[2][0] =  - 4.E+0*pow(P_P,-1)*P_l*P_qm*ssp*svm - 4.E+0*pow(
      P_P,-1)*P_l*P_qp*svp*ssm - 8.E+0*l_qm*ssp*svm - 8.E+0*l_qp*svp*
      ssm;

    kern[2][1] = 8.E+0*P_l*P_qm*l_qp*svp*svm + 8.E+0*P_l*P_qp*l_qm*svp*
      svm - 4.E+0*pow(P_l,2)*qm_qp*svp*svm - 1.2E+1*pow(P_l,2)*ssp*ssm;

    kern[2][2] =  - 8.E+0*pow(P_P,-1)*P_l*P_qm*l_qp*svp*svm - 8.E+0*
      pow(P_P,-1)*P_l*P_qp*l_qm*svp*svm + 4.E+0*pow(P_P,-1)*pow(P_l,2)*
      qm_qp*svp*svm + 4.E+0*pow(P_P,-1)*pow(P_l,2)*ssp*ssm + 8.E+0*pow(
      P_P,-1)*P_qm*P_qp*l_l*svp*svm + 8.E+0*l_l*ssp*ssm - 8.E+0*l_qm*
      l_qp*svp*svm;

    kern[2][3] = 8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*ssp*svm + 8.E+0*pow(
      P_P,-1)*pow(P_l,2)*P_qp*svp*ssm - 2.4E+1*P_l*l_qm*ssp*svm + 8.E+0
      *P_l*l_qp*svp*ssm + 1.6E+1*P_qm*l_l*ssp*svm - 1.6E+1*P_qp*l_l*svp
      *ssm;

    kern[2][4] =  - 4.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*l_qp*svp*svm + 
      4.E+0*pow(P_P,-1)*pow(P_l,2)*P_qp*l_qm*svp*svm;

    kern[2][5] =  - 4.E+0*pow(P_P,-1)*pow(P_l,3)*P_qm*ssp*svm - 4.E+0*
      pow(P_P,-1)*pow(P_l,3)*P_qp*svp*ssm + 4.E+0*pow(P_l,2)*l_qm*ssp*
      svm + 4.E+0*pow(P_l,2)*l_qp*svp*ssm;

    kern[2][6] = 4.E+0*pow(P_P,-1)*P_l*P_qm*l_l*ssp*svm + 4.E+0*pow(
      P_P,-1)*P_l*P_qp*l_l*svp*ssm - 4.E+0*pow(P_P,-1)*pow(P_l,2)*l_qm*
      ssp*svm - 4.E+0*pow(P_P,-1)*pow(P_l,2)*l_qp*svp*ssm;

    kern[2][7] =  - 1.6E+1*pow(P_P,-1)*pow(P_l,2)*P_qm*P_qp*l_l*svp*svm
       + 1.6E+1*pow(P_P,-1)*pow(P_l,3)*P_qm*l_qp*svp*svm + 1.6E+1*pow(
      P_P,-1)*pow(P_l,3)*P_qp*l_qm*svp*svm - 8.E+0*pow(P_P,-1)*pow(
      P_l,4)*qm_qp*svp*svm - 8.E+0*pow(P_P,-1)*pow(P_l,4)*ssp*ssm + 
      8.E+0*pow(P_l,2)*l_l*qm_qp*svp*svm + 8.E+0*pow(P_l,2)*l_l*ssp*ssm
       - 1.6E+1*pow(P_l,2)*l_qm*l_qp*svp*svm;

 
    kern[3][0] =  - 1.6E+1*pow(P_P,-1)*P_l*P_qm*P_qp*svp*svm + 2.4E+1*
      P_qm*l_qp*svp*svm - 8.E+0*P_qp*l_qm*svp*svm;

    kern[3][1] =  - 8.E+0*P_P*P_l*l_qm*ssp*svm + 2.4E+1*P_P*P_l*l_qp*
      svp*ssm + 8.E+0*pow(P_l,2)*P_qm*ssp*svm - 2.4E+1*pow(P_l,2)*P_qp*
      svp*ssm;

    kern[3][2] = 8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*ssp*svm + 8.E+0*pow(
      P_P,-1)*pow(P_l,2)*P_qp*svp*ssm + 8.E+0*P_l*l_qm*ssp*svm - 2.4E+1
      *P_l*l_qp*svp*ssm - 1.6E+1*P_qm*l_l*ssp*svm + 1.6E+1*P_qp*l_l*svp
      *ssm;

    kern[3][3] = 6.4E+1*pow(P_P,-1)*pow(P_l,2)*P_qm*P_qp*svp*svm + 
      3.2E+1*P_P*l_l*qm_qp*svp*svm + 3.2E+1*P_P*l_l*ssp*ssm + 3.2E+1*
      P_P*l_qm*l_qp*svp*svm - 3.2E+1*P_l*P_qm*l_qp*svp*svm - 3.2E+1*P_l
      *P_qp*l_qm*svp*svm - 3.2E+1*pow(P_l,2)*qm_qp*svp*svm - 3.2E+1*
      pow(P_l,2)*ssp*ssm - 3.2E+1*P_qm*P_qp*l_l*svp*svm;

    kern[3][4] = 8.E+0*pow(P_P,-1)*pow(P_l,3)*P_qm*ssp*svm - 8.E+0*pow(
      P_P,-1)*pow(P_l,3)*P_qp*svp*ssm - 8.E+0*P_l*P_qm*l_l*ssp*svm + 
      8.E+0*P_l*P_qp*l_l*svp*ssm;

    kern[3][5] =  - 1.6E+1*pow(P_P,-1)*pow(P_l,3)*P_qm*P_qp*svp*svm - 
      8.E+0*P_P*P_l*l_l*qm_qp*svp*svm + 8.E+0*P_P*P_l*l_l*ssp*ssm + 
      1.6E+1*P_l*P_qm*P_qp*l_l*svp*svm + 8.E+0*pow(P_l,3)*qm_qp*svp*svm
       - 8.E+0*pow(P_l,3)*ssp*ssm;

    kern[3][6] = 8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*l_qp*svp*svm + 8.E+0
      *pow(P_P,-1)*pow(P_l,2)*P_qp*l_qm*svp*svm - 8.E+0*pow(P_P,-1)*
      pow(P_l,3)*qm_qp*svp*svm + 8.E+0*pow(P_P,-1)*pow(P_l,3)*ssp*ssm
       + 8.E+0*P_l*l_l*qm_qp*svp*svm - 8.E+0*P_l*l_l*ssp*ssm - 8.E+0*
      P_qm*l_l*l_qp*svp*svm - 8.E+0*P_qp*l_l*l_qm*svp*svm;

    kern[3][7] =  - 1.6E+1*pow(P_P,-1)*pow(P_l,4)*P_qm*ssp*svm - 1.6E+1
      *pow(P_P,-1)*pow(P_l,4)*P_qp*svp*ssm - 1.6E+1*P_P*P_l*l_l*l_qm*
      ssp*svm - 1.6E+1*P_P*P_l*l_l*l_qp*svp*ssm + 1.6E+1*pow(P_l,2)*
      P_qm*l_l*ssp*svm + 1.6E+1*pow(P_l,2)*P_qp*l_l*svp*ssm + 1.6E+1*
      pow(P_l,3)*l_qm*ssp*svm + 1.6E+1*pow(P_l,3)*l_qp*svp*ssm;

 
    kern[4][0] =  - 4.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*ssp*svm + 4.E+0*
      pow(P_P,-1)*pow(P_l,2)*P_qp*svp*ssm + 4.E+0*P_l*l_qm*ssp*svm - 
      4.E+0*P_l*l_qp*svp*ssm;

    kern[4][1] = 4.E+0*pow(P_l,2)*P_qm*l_qp*svp*svm - 4.E+0*pow(P_l,2)*
      P_qp*l_qm*svp*svm;

    kern[4][2] =  - 4.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*l_qp*svp*svm + 
      4.E+0*pow(P_P,-1)*pow(P_l,2)*P_qp*l_qm*svp*svm;

    kern[4][3] = 8.E+0*pow(P_P,-1)*pow(P_l,3)*P_qm*ssp*svm - 8.E+0*pow(
      P_P,-1)*pow(P_l,3)*P_qp*svp*ssm - 8.E+0*P_l*P_qm*l_l*ssp*svm + 
      8.E+0*P_l*P_qp*l_l*svp*ssm;

    kern[4][4] =  - 4.E+0*pow(P_P,-1)*pow(P_l,4)*qm_qp*svp*svm - 4.E+0*
      pow(P_P,-1)*pow(P_l,4)*ssp*ssm + 4.E+0*pow(P_l,2)*l_l*qm_qp*svp*
      svm + 4.E+0*pow(P_l,2)*l_l*ssp*ssm;

    kern[4][5] =  - 4.E+0*pow(P_P,-1)*pow(P_l,4)*P_qm*ssp*svm + 4.E+0*
      pow(P_P,-1)*pow(P_l,4)*P_qp*svp*ssm + 4.E+0*pow(P_l,2)*P_qm*l_l*
      ssp*svm - 4.E+0*pow(P_l,2)*P_qp*l_l*svp*ssm;

    kern[4][6] = 4.E+0*pow(P_P,-1)*pow(P_l,3)*l_qm*ssp*svm - 4.E+0*pow(
      P_P,-1)*pow(P_l,3)*l_qp*svp*ssm - 4.E+0*P_l*l_l*l_qm*ssp*svm + 
      4.E+0*P_l*l_l*l_qp*svp*ssm;

    kern[4][7] = 8.E+0*pow(P_P,-1)*pow(P_l,4)*P_qm*l_qp*svp*svm - 8.E+0
      *pow(P_P,-1)*pow(P_l,4)*P_qp*l_qm*svp*svm - 8.E+0*pow(P_l,2)*P_qm
      *l_l*l_qp*svp*svm + 8.E+0*pow(P_l,2)*P_qp*l_l*l_qm*svp*svm;

 
    kern[5][0] =  - 8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*P_qp*svp*svm + 
      4.E+0*P_l*P_qm*l_qp*svp*svm + 4.E+0*P_l*P_qp*l_qm*svp*svm;

    kern[5][1] = 4.E+0*P_P*pow(P_l,2)*l_qm*ssp*svm + 4.E+0*P_P*pow(
      P_l,2)*l_qp*svp*ssm - 4.E+0*pow(P_l,3)*P_qm*ssp*svm - 4.E+0*pow(
      P_l,3)*P_qp*svp*ssm;

    kern[5][2] = 4.E+0*pow(P_P,-1)*pow(P_l,3)*P_qm*ssp*svm + 4.E+0*pow(
      P_P,-1)*pow(P_l,3)*P_qp*svp*ssm - 4.E+0*pow(P_l,2)*l_qm*ssp*svm
       - 4.E+0*pow(P_l,2)*l_qp*svp*ssm;

    kern[5][3] = 1.6E+1*pow(P_P,-1)*pow(P_l,3)*P_qm*P_qp*svp*svm + 
      8.E+0*P_P*P_l*l_l*qm_qp*svp*svm - 8.E+0*P_P*P_l*l_l*ssp*ssm - 
      1.6E+1*P_l*P_qm*P_qp*l_l*svp*svm - 8.E+0*pow(P_l,3)*qm_qp*svp*svm
       + 8.E+0*pow(P_l,3)*ssp*ssm;

    kern[5][4] = 4.E+0*pow(P_P,-1)*pow(P_l,4)*P_qm*ssp*svm - 4.E+0*pow(
      P_P,-1)*pow(P_l,4)*P_qp*svp*ssm - 4.E+0*pow(P_l,2)*P_qm*l_l*ssp*
      svm + 4.E+0*pow(P_l,2)*P_qp*l_l*svp*ssm;

    kern[5][5] =  - 8.E+0*pow(P_P,-1)*pow(P_l,4)*P_qm*P_qp*svp*svm - 
      4.E+0*P_P*pow(P_l,2)*l_l*qm_qp*svp*svm + 4.E+0*P_P*pow(P_l,2)*l_l
      *ssp*ssm + 8.E+0*pow(P_l,2)*P_qm*P_qp*l_l*svp*svm + 4.E+0*pow(
      P_l,4)*qm_qp*svp*svm - 4.E+0*pow(P_l,4)*ssp*ssm;

    kern[5][6] = 4.E+0*pow(P_P,-1)*pow(P_l,3)*P_qm*l_qp*svp*svm + 4.E+0
      *pow(P_P,-1)*pow(P_l,3)*P_qp*l_qm*svp*svm - 4.E+0*pow(P_P,-1)*
      pow(P_l,4)*qm_qp*svp*svm + 4.E+0*pow(P_P,-1)*pow(P_l,4)*ssp*ssm
       - 4.E+0*P_l*P_qm*l_l*l_qp*svp*svm - 4.E+0*P_l*P_qp*l_l*l_qm*svp*
      svm + 4.E+0*pow(P_l,2)*l_l*qm_qp*svp*svm - 4.E+0*pow(P_l,2)*l_l*
      ssp*ssm;

    kern[5][7] =  - 8.E+0*pow(P_P,-1)*pow(P_l,5)*P_qm*ssp*svm - 8.E+0*
      pow(P_P,-1)*pow(P_l,5)*P_qp*svp*ssm - 8.E+0*P_P*pow(P_l,2)*l_l*
      l_qm*ssp*svm - 8.E+0*P_P*pow(P_l,2)*l_l*l_qp*svp*ssm + 8.E+0*pow(
      P_l,3)*P_qm*l_l*ssp*svm + 8.E+0*pow(P_l,3)*P_qp*l_l*svp*ssm + 
      8.E+0*pow(P_l,4)*l_qm*ssp*svm + 8.E+0*pow(P_l,4)*l_qp*svp*ssm;

 
    kern[6][0] = 4.E+0*pow(P_P,-1)*P_l*P_qm*l_qp*svp*svm + 4.E+0*pow(
      P_P,-1)*P_l*P_qp*l_qm*svp*svm - 4.E+0*pow(P_P,-1)*pow(P_l,2)*
      qm_qp*svp*svm + 4.E+0*pow(P_P,-1)*pow(P_l,2)*ssp*ssm + 4.E+0*l_l*
      qm_qp*svp*svm - 4.E+0*l_l*ssp*ssm - 8.E+0*l_qm*l_qp*svp*svm;

    kern[6][1] = 4.E+0*P_l*P_qm*l_l*ssp*svm + 4.E+0*P_l*P_qp*l_l*svp*
      ssm - 4.E+0*pow(P_l,2)*l_qm*ssp*svm - 4.E+0*pow(P_l,2)*l_qp*svp*
      ssm;

    kern[6][2] =  - 4.E+0*pow(P_P,-1)*P_l*P_qm*l_l*ssp*svm - 4.E+0*pow(
      P_P,-1)*P_l*P_qp*l_l*svp*ssm + 4.E+0*pow(P_P,-1)*pow(P_l,2)*l_qm*
      ssp*svm + 4.E+0*pow(P_P,-1)*pow(P_l,2)*l_qp*svp*ssm;

    kern[6][3] =  - 8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*l_qp*svp*svm - 
      8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qp*l_qm*svp*svm + 8.E+0*pow(
      P_P,-1)*pow(P_l,3)*qm_qp*svp*svm - 8.E+0*pow(P_P,-1)*pow(P_l,3)*
      ssp*ssm - 8.E+0*P_l*l_l*qm_qp*svp*svm + 8.E+0*P_l*l_l*ssp*ssm + 
      8.E+0*P_qm*l_l*l_qp*svp*svm + 8.E+0*P_qp*l_l*l_qm*svp*svm;

    kern[6][4] =  - 4.E+0*pow(P_P,-1)*pow(P_l,3)*l_qm*ssp*svm + 4.E+0*
      pow(P_P,-1)*pow(P_l,3)*l_qp*svp*ssm + 4.E+0*P_l*l_l*l_qm*ssp*svm
       - 4.E+0*P_l*l_l*l_qp*svp*ssm;

    kern[6][5] = 4.E+0*pow(P_P,-1)*pow(P_l,3)*P_qm*l_qp*svp*svm + 4.E+0
      *pow(P_P,-1)*pow(P_l,3)*P_qp*l_qm*svp*svm - 4.E+0*pow(P_P,-1)*
      pow(P_l,4)*qm_qp*svp*svm + 4.E+0*pow(P_P,-1)*pow(P_l,4)*ssp*ssm
       - 4.E+0*P_l*P_qm*l_l*l_qp*svp*svm - 4.E+0*P_l*P_qp*l_l*l_qm*svp*
      svm + 4.E+0*pow(P_l,2)*l_l*qm_qp*svp*svm - 4.E+0*pow(P_l,2)*l_l*
      ssp*ssm;

    kern[6][6] = 4.E+0*pow(P_P,-1)*pow(P_l,2)*l_l*qm_qp*svp*svm - 4.E+0
      *pow(P_P,-1)*pow(P_l,2)*l_l*ssp*ssm - 8.E+0*pow(P_P,-1)*pow(
      P_l,2)*l_qm*l_qp*svp*svm + 8.E+0*l_l*l_qm*l_qp*svp*svm - 4.E+0*
      pow(l_l,2)*qm_qp*svp*svm + 4.E+0*pow(l_l,2)*ssp*ssm;

    kern[6][7] = 8.E+0*pow(P_P,-1)*pow(P_l,3)*P_qm*l_l*ssp*svm + 8.E+0*
      pow(P_P,-1)*pow(P_l,3)*P_qp*l_l*svp*ssm - 8.E+0*pow(P_P,-1)*pow(
      P_l,4)*l_qm*ssp*svm - 8.E+0*pow(P_P,-1)*pow(P_l,4)*l_qp*svp*ssm
       - 8.E+0*P_l*P_qm*pow(l_l,2)*ssp*svm - 8.E+0*P_l*P_qp*pow(l_l,2)*
      svp*ssm + 8.E+0*pow(P_l,2)*l_l*l_qm*ssp*svm + 8.E+0*pow(P_l,2)*
      l_l*l_qp*svp*ssm;

 
    kern[7][0] = 8.E+0*pow(P_P,-1)*pow(P_l,3)*P_qm*ssp*svm + 8.E+0*pow(
      P_P,-1)*pow(P_l,3)*P_qp*svp*ssm - 8.E+0*P_l*P_qm*l_l*ssp*svm - 
      8.E+0*P_l*P_qp*l_l*svp*ssm;

    kern[7][1] =  - 8.E+0*P_P*pow(P_l,2)*l_l*qm_qp*svp*svm - 8.E+0*P_P*
      pow(P_l,2)*l_l*ssp*ssm + 1.6E+1*P_P*pow(P_l,2)*l_qm*l_qp*svp*svm
       + 1.6E+1*pow(P_l,2)*P_qm*P_qp*l_l*svp*svm - 1.6E+1*pow(P_l,3)*
      P_qm*l_qp*svp*svm - 1.6E+1*pow(P_l,3)*P_qp*l_qm*svp*svm + 8.E+0*
      pow(P_l,4)*qm_qp*svp*svm + 8.E+0*pow(P_l,4)*ssp*ssm;

    kern[7][2] =  - 1.6E+1*pow(P_P,-1)*pow(P_l,2)*P_qm*P_qp*l_l*svp*svm
       + 1.6E+1*pow(P_P,-1)*pow(P_l,3)*P_qm*l_qp*svp*svm + 1.6E+1*pow(
      P_P,-1)*pow(P_l,3)*P_qp*l_qm*svp*svm - 8.E+0*pow(P_P,-1)*pow(
      P_l,4)*qm_qp*svp*svm - 8.E+0*pow(P_P,-1)*pow(P_l,4)*ssp*ssm + 
      8.E+0*pow(P_l,2)*l_l*qm_qp*svp*svm + 8.E+0*pow(P_l,2)*l_l*ssp*ssm
       - 1.6E+1*pow(P_l,2)*l_qm*l_qp*svp*svm;

    kern[7][3] =  - 1.6E+1*pow(P_P,-1)*pow(P_l,4)*P_qm*ssp*svm - 1.6E+1
      *pow(P_P,-1)*pow(P_l,4)*P_qp*svp*ssm - 1.6E+1*P_P*P_l*l_l*l_qm*
      ssp*svm - 1.6E+1*P_P*P_l*l_l*l_qp*svp*ssm + 1.6E+1*pow(P_l,2)*
      P_qm*l_l*ssp*svm + 1.6E+1*pow(P_l,2)*P_qp*l_l*svp*ssm + 1.6E+1*
      pow(P_l,3)*l_qm*ssp*svm + 1.6E+1*pow(P_l,3)*l_qp*svp*ssm;

    kern[7][4] = 8.E+0*pow(P_P,-1)*pow(P_l,4)*P_qm*l_qp*svp*svm - 8.E+0
      *pow(P_P,-1)*pow(P_l,4)*P_qp*l_qm*svp*svm - 8.E+0*pow(P_l,2)*P_qm
      *l_l*l_qp*svp*svm + 8.E+0*pow(P_l,2)*P_qp*l_l*l_qm*svp*svm;

    kern[7][5] = 8.E+0*pow(P_P,-1)*pow(P_l,5)*P_qm*ssp*svm + 8.E+0*pow(
      P_P,-1)*pow(P_l,5)*P_qp*svp*ssm + 8.E+0*P_P*pow(P_l,2)*l_l*l_qm*
      ssp*svm + 8.E+0*P_P*pow(P_l,2)*l_l*l_qp*svp*ssm - 8.E+0*pow(
      P_l,3)*P_qm*l_l*ssp*svm - 8.E+0*pow(P_l,3)*P_qp*l_l*svp*ssm - 
      8.E+0*pow(P_l,4)*l_qm*ssp*svm - 8.E+0*pow(P_l,4)*l_qp*svp*ssm;

    kern[7][6] =  - 8.E+0*pow(P_P,-1)*pow(P_l,3)*P_qm*l_l*ssp*svm - 
      8.E+0*pow(P_P,-1)*pow(P_l,3)*P_qp*l_l*svp*ssm + 8.E+0*pow(P_P,-1)
      *pow(P_l,4)*l_qm*ssp*svm + 8.E+0*pow(P_P,-1)*pow(P_l,4)*l_qp*svp*
      ssm + 8.E+0*P_l*P_qm*pow(l_l,2)*ssp*svm + 8.E+0*P_l*P_qp*pow(
      l_l,2)*svp*ssm - 8.E+0*pow(P_l,2)*l_l*l_qm*ssp*svm - 8.E+0*pow(
      P_l,2)*l_l*l_qp*svp*ssm;

    kern[7][7] = 3.2E+1*pow(P_P,-1)*pow(P_l,4)*P_qm*P_qp*l_l*svp*svm - 
      3.2E+1*pow(P_P,-1)*pow(P_l,5)*P_qm*l_qp*svp*svm - 3.2E+1*pow(
      P_P,-1)*pow(P_l,5)*P_qp*l_qm*svp*svm + 1.6E+1*pow(P_P,-1)*pow(
      P_l,6)*qm_qp*svp*svm + 1.6E+1*pow(P_P,-1)*pow(P_l,6)*ssp*ssm - 
      3.2E+1*P_P*pow(P_l,2)*l_l*l_qm*l_qp*svp*svm + 1.6E+1*P_P*pow(
      P_l,2)*pow(l_l,2)*qm_qp*svp*svm + 1.6E+1*P_P*pow(P_l,2)*pow(
      l_l,2)*ssp*ssm - 3.2E+1*pow(P_l,2)*P_qm*P_qp*pow(l_l,2)*svp*svm
       + 3.2E+1*pow(P_l,3)*P_qm*l_l*l_qp*svp*svm + 3.2E+1*pow(P_l,3)*
      P_qp*l_l*l_qm*svp*svm - 3.2E+1*pow(P_l,4)*l_l*qm_qp*svp*svm - 
      3.2E+1*pow(P_l,4)*l_l*ssp*ssm + 3.2E+1*pow(P_l,4)*l_qm*l_qp*svp*
      svm;


}

void normalisation_axial(matCdoub& kern,
  Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
  Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
  const Cdoub& svp, const Cdoub& ssp, 
  const Cdoub& svm, const Cdoub& ssm ){
 
 
     // local variables
    array<Cdoub, 74> w;
 
 
    w[1]=pow(P_P,-1);
    w[2]=P_qm;
    w[3]=P_qp;
    w[4]=qm_qp;
    w[5]=P_l;
    w[6]=l_qm;
    w[7]=l_qp;
    w[8]=l_l;
    w[9]=P_P;
   w[10]=ssm*ssp;
   w[11]=3.E+0*w[10];
   w[12]=svm*svp;
   w[13]=w[12]*w[2];
   w[14]=w[13]*w[3];
   w[15]=2.E+0*w[14];
   w[16]=w[15]*w[1];
   w[17]=w[12]*w[4];
   w[18]= - w[16] - w[17] + w[11];
   w[18]=4.E+0*w[18];
   w[19]=ssp*svm;
   w[20]=w[2]*w[19];
   w[21]=ssm*svp;
   w[22]=w[3]*w[21];
   w[23]=w[20] + w[22];
   w[24]=w[23]*w[5];
   w[25]=1.2E+1*w[24];
   w[26]=w[5]*w[1];
   w[27]=w[23]*w[26];
   w[21]=w[7]*w[21];
   w[19]=w[6]*w[19];
   w[28]=w[21] + w[19];
   w[29]=w[27] + 2.E+0*w[28];
   w[29]=4.E+0*w[29];
   w[30]=w[15]*w[26];
   w[13]=w[13]*w[7];
   w[12]=w[12]*w[6];
   w[31]=w[3]*w[12];
   w[32]=w[30] - 3.E+0*w[31] + w[13];
   w[32]=8.E+0*w[32];
   w[33]=w[20] - w[22];
   w[34]=w[33]*w[26];
   w[35]=w[21] - w[19];
   w[34]=w[34] + w[35];
   w[36]=4.E+0*w[5];
   w[34]=w[34]*w[36];
   w[37]=w[13] + w[31];
   w[38]=w[30] - w[37];
   w[38]=w[38]*w[36];
   w[39]=w[17] - w[10];
   w[40]=w[39]*w[26];
   w[41]=w[37]*w[1];
   w[40]=w[40] - w[41];
   w[40]=w[40]*w[5];
   w[42]=w[39]*w[8];
   w[12]=w[7]*w[12];
   w[43]=2.E+0*w[12];
   w[44]=w[43] - w[42];
   w[45]=w[44] + w[40];
   w[45]=4.E+0*w[45];
   w[46]=pow(w[5],2);
   w[47]=w[46]*w[1];
   w[48]=w[47]*w[23];
   w[49]=w[23]*w[8];
   w[48]=w[48] - w[49];
   w[50]=8.E+0*w[5];
   w[48]=w[48]*w[50];
   w[11]=w[11] + w[17];
   w[51]=w[9]*w[11];
   w[51]= - 4.E+0*w[14] + w[51];
   w[52]=4.E+0*w[46];
   w[51]=w[51]*w[52];
   w[11]=w[11]*w[5];
   w[53]=2.E+0*w[37];
   w[11]=w[11] - w[53];
   w[11]=w[11]*w[36];
   w[54]= - w[21] + 3.E+0*w[19];
   w[55]=w[9]*w[54];
   w[56]=w[22] - 3.E+0*w[20];
   w[56]=w[5]*w[56];
   w[55]=w[55] + w[56];
   w[55]=w[55]*w[50];
   w[56]=w[13] - w[31];
   w[57]=w[56]*w[52];
   w[58]=w[28]*w[9];
   w[24]=w[24] - w[58];
   w[24]=w[24]*w[52];
   w[59]=w[28]*w[5];
   w[59]=w[59] - w[49];
   w[59]=w[59]*w[36];
   w[17]=w[17] + w[10];
   w[60]=w[17]*w[5];
   w[60]=w[60] - w[53];
   w[60]=w[60]*w[5];
   w[61]=w[17]*w[8];
   w[43]=w[61] - w[43];
   w[62]=w[43]*w[9];
   w[63]=w[15]*w[8];
   w[60]=w[60] - w[62] + w[63];
   w[64]=8.E+0*w[46];
   w[60]=w[60]*w[64];
   w[65]=w[17]*w[26];
   w[41]= - w[65] + 2.E+0*w[41];
   w[41]=w[41]*w[5];
   w[14]=w[14]*w[8];
   w[65]=w[14]*w[1];
   w[10]=w[8]*w[10];
   w[10]=w[65] - w[12] + w[10];
   w[10]=2.E+0*w[10] - w[41];
   w[10]=4.E+0*w[10];
   w[54]=w[27] - w[54];
   w[54]=w[5]*w[54];
   w[66]=w[33]*w[8];
   w[67]=2.E+0*w[66];
   w[54]=w[67] + w[54];
   w[54]=8.E+0*w[54];
   w[68]=w[56]*w[47];
   w[69]=4.E+0*w[68];
   w[70]=w[27] - w[28];
   w[71]=w[70]*w[52];
   w[26]=w[28]*w[26];
   w[72]=w[49]*w[1];
   w[26]=w[26] - w[72];
   w[72]=w[26]*w[36];
   w[73]=w[16]*w[8];
   w[43]= - w[43] + w[73] - w[41];
   w[43]=w[43]*w[64];
   w[13]= - w[30] - w[31] + 3.E+0*w[13];
   w[13]=8.E+0*w[13];
   w[19]= - w[19] + 3.E+0*w[21];
   w[21]=w[9]*w[19];
   w[20]= - 3.E+0*w[22] + w[20];
   w[20]=w[5]*w[20];
   w[20]=w[21] + w[20];
   w[20]=w[20]*w[50];
   w[19]=w[27] - w[19];
   w[19]=w[5]*w[19];
   w[19]= - w[67] + w[19];
   w[19]=8.E+0*w[19];
   w[21]=w[16] - w[17];
   w[21]=w[5]*w[21];
   w[21]=w[21] - w[37];
   w[21]=w[5]*w[21];
   w[22]=w[12] + w[61];
   w[22]=w[9]*w[22];
   w[14]=w[21] - w[14] + w[22];
   w[14]=3.2E+1*w[14];
   w[21]=w[33]*w[47];
   w[21]=w[21] - w[66];
   w[22]=w[21]*w[50];
   w[16]=w[16] - w[39];
   w[16]=w[16]*w[46];
   w[27]=w[42]*w[9];
   w[16]=w[16] + w[27] - w[63];
   w[27]=w[16]*w[50];
   w[30]=w[40] - w[42];
   w[30]=w[30]*w[5];
   w[31]=w[37]*w[8];
   w[30]=w[30] + w[31];
   w[31]=8.E+0*w[30];
   w[33]=w[70]*w[5];
   w[33]=w[33] - w[49];
   w[33]=w[33]*w[5];
   w[37]=w[58]*w[8];
   w[33]=w[33] + w[37];
   w[37]=w[33]*w[5];
   w[37]=1.6E+1*w[37];
   w[17]= - w[17]*w[47];
   w[17]=w[61] + w[17];
   w[17]=w[17]*w[52];
   w[21]=w[21]*w[52];
   w[39]=w[47] - w[8];
   w[35]=w[36]*w[39]*w[35];
   w[40]=w[56]*w[8];
   w[40]=w[40] - w[68];
   w[40]=w[40]*w[64];
   w[16]= - w[16]*w[52];
   w[30]=w[30]*w[36];
   w[33]=w[33]*w[64];
   w[36]= - 4.E+0*w[44]*w[39];
   w[26]=w[26]*w[5];
   w[28]=w[28]*w[8];
   w[26]=w[26] - w[28];
   w[26]=w[26]*w[5];
   w[28]=pow(w[8],2);
   w[23]=w[28]*w[23];
   w[23]=w[26] + w[23];
   w[23]=w[23]*w[50];
   w[12]=w[65] + w[12] - w[61];
   w[12]=2.E+0*w[12] - w[41];
   w[12]=w[5]*w[12];
   w[26]=w[8]*w[53];
   w[12]=w[26] + w[12];
   w[12]=w[5]*w[12];
   w[15]= - w[28]*w[15];
   w[26]=w[8]*w[62];
   w[12]=w[12] + w[15] + w[26];
   w[12]=1.6E+1*w[12]*w[46];

 
    kern[0][0] = w[18];

    kern[0][1] =  - w[25];

    kern[0][2] = w[29];

    kern[0][3] = w[32];

    kern[0][4] = w[34];

    kern[0][5] =  - w[38];

    kern[0][6] =  - w[45];

    kern[0][7] =  - w[48];

 
    kern[1][0] = w[25];

    kern[1][1] = w[51];

    kern[1][2] =  - w[11];

    kern[1][3] = w[55];

    kern[1][4] = w[57];

    kern[1][5] = w[24];

    kern[1][6] = w[59];

    kern[1][7] = w[60];

 
    kern[2][0] =  - w[29];

    kern[2][1] =  - w[11];

    kern[2][2] = w[10];

    kern[2][3] = w[54];

    kern[2][4] =  - w[69];

    kern[2][5] =  - w[71];

    kern[2][6] =  - w[72];

    kern[2][7] =  - w[43];

 
    kern[3][0] = w[13];

    kern[3][1] = w[20];

    kern[3][2] = w[19];

    kern[3][3] = w[14];

    kern[3][4] = w[22];

    kern[3][5] =  - w[27];

    kern[3][6] =  - w[31];

    kern[3][7] =  - w[37];

 
    kern[4][0] =  - w[34];

    kern[4][1] = w[57];

    kern[4][2] =  - w[69];

    kern[4][3] = w[22];

    kern[4][4] = w[17];

    kern[4][5] =  - w[21];

    kern[4][6] =  - w[35];

    kern[4][7] =  - w[40];

 
    kern[5][0] =  - w[38];

    kern[5][1] =  - w[24];

    kern[5][2] = w[71];

    kern[5][3] = w[27];

    kern[5][4] = w[21];

    kern[5][5] = w[16];

    kern[5][6] =  - w[30];

    kern[5][7] =  - w[33];

 
    kern[6][0] =  - w[45];

    kern[6][1] =  - w[59];

    kern[6][2] = w[72];

    kern[6][3] = w[31];

    kern[6][4] = w[35];

    kern[6][5] =  - w[30];

    kern[6][6] = w[36];

    kern[6][7] =  - w[23];

 
    kern[7][0] = w[48];

    kern[7][1] = w[60];

    kern[7][2] =  - w[43];

    kern[7][3] =  - w[37];

    kern[7][4] =  - w[40];

    kern[7][5] = w[33];

    kern[7][6] = w[23];

    kern[7][7] = w[12];


}

