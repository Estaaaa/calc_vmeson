//
// Created by Esther Weil on 31.01.18.
//

#ifndef CALC_VMESON_KERNEL_PS_H
#define CALC_VMESON_KERNEL_PS_H

#include <Meson/typedefMeson.h>
#include <routing_amount_scalars.h>

void kernelL_notopti_ps(matCdoub& kern,
                        const ArrayScalarProducts& sp,
                        const array<Cdoub, K_ORDER_OF_ANGLE>& ang );

void kernelY_noopti_ps(matCdoub& kern,
                       const ArrayScalarProducts& sp,  const Cdoub& svp,
                       const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm );

void kernelL_ps(matCdoub& kern,
                const ArrayScalarProducts& sp,
                const array<Cdoub, K_ORDER_OF_ANGLE>& ang );


void kernelY_ps(matCdoub& kern,
                const ArrayScalarProducts& sp,  const Cdoub& svp,
                const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm );

Cdoub calc_fpi_ps(
        Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
        Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
        VecCdoub Gamma, const Cdoub& svp,
        const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm );

void kernelall_ps(matCdoub& kern,
                  const ArrayScalarProducts& sp,  const Cdoub& svp,
                  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ,
                  const array<Cdoub, K_ORDER_OF_ANGLE>& ang );

void normalisation_ps(matCdoub& kern,
                      const ArrayScalarProducts& sp,  const Cdoub& svp,
                      const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm );

void normalisation_notopti_ps(matCdoub& kern,
                              Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
                              Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
                              const Cdoub& svp, const Cdoub& ssp,
                              const Cdoub& svm, const Cdoub& ssm );

void normalisation_ps(matCdoub& kern,
                      Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
                      Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
                      const Cdoub& svp, const Cdoub& ssp,
                      const Cdoub& svm, const Cdoub& ssm );

void kernel_rotationY_ps(matCdoub& kern,
                         const ArrayScalarProducts& sp,  const Cdoub& svp,
                         const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm );



#endif //CALC_VMESON_KERNEL_PS_H
