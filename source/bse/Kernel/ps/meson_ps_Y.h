#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
//#include <math_ops.h>

void kernel_rotationY_ps(matCdoub& kern,
const ArrayScalarProducts& sp,  const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){

//    cout<<svp<<tab<<ssp<<tab<<svm<<tab<<ssm<<endl;
//    cout<<ssm*ssp<<tab<<svp*svm<<endl;

// local variables
array<Cdoub, 33> w;
Cdoub denom;


w[1]=sp[12];
w[2]=sp[32];
w[3]=sp[30];
w[4]=sp[34];
w[5]=sp[25];
w[6]=sp[23];
w[7]=sp[27];
w[8]=sp[35];
w[9]=ssm*ssp;
w[10]=svp*svm;
w[11]=w[10]*w[1];
w[12]=w[9] + w[11];
w[13]=4.E+0*w[12];
w[14]=svm*ssp;
w[15]=w[2]*w[14];
w[16]=svp*ssm;
w[17]=w[3]*w[16];
w[18]=w[15] - w[17];
w[19]= - 4.E+0*w[18];
w[14]=w[5]*w[14];
w[16]=w[6]*w[16];
w[20]=w[14] - w[16];
w[21]=4.E+0*w[4];
w[22]= - w[20]*w[21];
w[23]=w[10]*w[2];
w[24]=w[23]*w[6];
w[10]=w[10]*w[5];
w[25]=w[10]*w[3];
w[26]=w[24] - w[25];
w[26]=8.E+0*w[26];
w[27]= - w[7]*w[18];
w[28]=w[4]*w[20];
w[27]=w[27] + w[28];
w[27]=4.E+0*w[27];
w[9]=w[9] - w[11];
w[11]=w[9]*w[4];
w[24]=w[24] + w[25];
w[11]=w[11] + w[24];
w[11]=w[11]*w[4];
w[25]= - w[8]*w[9];
w[23]=w[23]*w[3];
w[23]=2.E+0*w[23];
w[25]= - w[23] + w[25];
w[25]=w[7]*w[25];
w[25]=w[25] + w[11];
w[25]=4.E+0*w[25];
w[28]= - w[7]*w[24];
w[10]=w[10]*w[6];
w[10]=2.E+0*w[10];
w[29]=w[4]*w[10];
w[28]=w[28] + w[29];
w[28]=w[28]*w[21];
w[29]=pow(w[4],2);
w[30]=w[7]*w[8];
w[29]=w[29] - w[30];
w[31]=8.E+0*w[29];
w[14]=w[14] + w[16];
w[16]= - w[14]*w[31];
w[20]= - w[8]*w[20];
w[18]=w[4]*w[18];
w[18]=w[20] + w[18];
w[18]=4.E+0*w[18];
w[20]= - w[8]*w[24];
w[32]=w[4]*w[23];
w[20]=w[20] + w[32];
w[20]=4.E+0*w[20];
w[9]= - w[9]*w[30];
w[10]=w[10]*w[8];
w[9]=w[11] - w[10] + w[9];
w[9]=w[9]*w[21];
w[11]=w[15] + w[17];
w[15]=w[11]*w[31];
w[17]= - w[8]*w[14];
w[30]=w[4]*w[11];
w[17]=w[17] + w[30];
w[17]=8.E+0*w[17];
w[11]=w[7]*w[11];
w[14]= - w[4]*w[14];
w[11]=w[11] + w[14];
w[11]=8.E+0*w[4]*w[11];
w[14]=w[4]*w[12];
w[14]= - 2.E+0*w[24] + w[14];
w[14]=w[4]*w[14];
w[12]= - w[8]*w[12];
w[12]=w[23] + w[12];
w[12]=w[7]*w[12];
w[10]=w[14] + w[10] + w[12];
w[10]=1.6E+1*w[10];
w[12]=4.E+0*w[29];
w[14]=w[29]*w[21];
w[21]=1.6E+1*w[29];

//    for (int i = 1; i < 23; ++i) {
//        cout<<"["<<i<<"]:"<<w[i]<<tab;
//    }cout<<endl;

denom = 4.E+0;

kern[0][0] = (1.0/ denom)* w[13];

kern[0][1] = (1.0/ denom)* w[19];

kern[0][2] = (1.0/ denom)* w[22];

kern[0][3] = (1.0/ denom)* w[26];


denom = w[12];

kern[1][0] = (1.0/ denom)* w[27];

kern[1][1] = (1.0/ denom)* w[25];

kern[1][2] = (1.0/ denom)* w[28];

kern[1][3] = (1.0/ denom)* w[16];


denom = w[14];

kern[2][0] = (1.0/ denom)* w[18];

kern[2][1] = (1.0/ denom)* w[20];

kern[2][2] = (1.0/ denom)* w[9];

kern[2][3] = (1.0/ denom)* w[15];


denom = w[21];

kern[3][0] = (1.0/ denom)*  - w[26];

kern[3][1] = (1.0/ denom)* w[17];

kern[3][2] = (1.0/ denom)* w[11];

kern[3][3] = (1.0/ denom)* w[10];


}
