#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
 
Cdoub calc_fpi_ps( 
  Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
  Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
  VecCdoub Gamma, const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){
 
 
 Cdoub dummy, fpi=0.0;
 
    dummy =   - 4.E+0*P_qm*ssp*svm + 4.E+0*P_qp*svp*ssm;
 
    fpi += Gamma[0] * dummy; 
 
    dummy =  4.E+0*P_P*qm_qp*svp*svm - 4.E+0*P_P*ssp*ssm - 8.E+0*P_qm*
      P_qp*svp*svm;
 
    fpi += Gamma[1] * dummy; 
 
    dummy =   - 4.E+0*P_l*P_qm*l_qp*svp*svm - 4.E+0*P_l*P_qp*l_qm*svp*
      svm + 4.E+0*pow(P_l,2)*qm_qp*svp*svm - 4.E+0*pow(P_l,2)*ssp*ssm;
 
    fpi += Gamma[2] * dummy; 
 
    dummy =  8.E+0*P_P*l_qm*ssp*svm + 8.E+0*P_P*l_qp*svp*ssm - 8.E+0*
      P_l*P_qm*ssp*svm - 8.E+0*P_l*P_qp*svp*ssm;
 
    fpi += Gamma[3] * dummy; 
 

    return fpi;

}

