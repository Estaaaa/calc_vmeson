#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
//#include <math_ops.h>

void kernelL_notopti(array<array<Cdoub, K_N_PROJECTORS_DIRAC>,
                            K_N_BASE_ELEMENTS_DIRAC>& kern,
                              const ArrayScalarProducts& sp,
                const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){


    // local variables
    Cdoub denom=0.0;


    denom =  - 8.E+0*sp[35]*sp[27] + 8.E+0*pow(sp[34],2);

    kern[0][0] =  4.E+0*sp[35]*sp[27]*sp[44]*ang[0] + 1.2E+1*sp[35]*sp[27]*ang[1] - 2.E+0*
      sp[35]*pow(sp[27],2)*ang[0] + 4.E+0*sp[35]*sp[44]*ang[1] - 2.E+0*sp[35]*pow(sp[44],2)*
      ang[0] - 2.E+0*sp[35]*ang[2] + 8.E+0*sp[34]*sp[43]*sp[27]*ang[0] + 8.E+0*sp[34]*sp[43]*
      sp[44]*ang[0] - 8.E+0*sp[34]*sp[43]*ang[1] - 8.E+0*pow(sp[34],2)*sp[44]*ang[0] - 8.E+0
      *pow(sp[34],2)*ang[1] - 8.E+0*pow(sp[43],2)*sp[27]*ang[0];
 
    kern[0][0] = (1.0/ denom)* kern[0][0]; 
    kern[0][1] =   0;
 
    kern[0][1] = (1.0/ denom)* kern[0][1]; 
    kern[0][2] =   0;
 
    kern[0][2] = (1.0/ denom)* kern[0][2]; 
    kern[0][3] =   0;
 
    kern[0][3] = (1.0/ denom)* kern[0][3]; 
    kern[0][4] =   0;
 
    kern[0][4] = (1.0/ denom)* kern[0][4]; 
    kern[0][5] =   - 4.E+0*sp[35]*sp[34]*sp[43]*sp[27]*sp[44]*ang[0] - 4.E+0*sp[35]*sp[34]*sp[43]
      *sp[27]*ang[1] + 2.E+0*sp[35]*sp[34]*sp[43]*pow(sp[27],2)*ang[0] - 4.E+0*sp[35]*sp[34]*sp[43]
      *sp[44]*ang[1] + 2.E+0*sp[35]*sp[34]*sp[43]*pow(sp[44],2)*ang[0] + 2.E+0*sp[35]*sp[34]*sp[43]
      *ang[2] + 4.E+0*sp[35]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 4.E+0*sp[35]*pow(sp[43],2)*
      sp[27]*ang[1] - 2.E+0*sp[35]*pow(sp[43],2)*pow(sp[27],2)*ang[0] + 4.E+0*sp[35]*pow(
      sp[43],2)*sp[44]*ang[1] - 2.E+0*sp[35]*pow(sp[43],2)*pow(sp[44],2)*ang[0] - 2.E+0*
      sp[35]*pow(sp[43],2)*ang[2] + 1.6E+1*sp[34]*pow(sp[43],3)*sp[27]*ang[0] + 8.E+0*sp[34]*
      pow(sp[43],3)*sp[44]*ang[0] - 8.E+0*sp[34]*pow(sp[43],3)*ang[1] - 8.E+0*pow(
      sp[34],2)*pow(sp[43],2)*sp[27]*ang[0] - 1.6E+1*pow(sp[34],2)*pow(sp[43],2)*sp[44]*
      ang[0] + 8.E+0*pow(sp[34],2)*pow(sp[43],2)*ang[1] + 8.E+0*pow(sp[34],3)*sp[43]*
      sp[44]*ang[0] - 8.E+0*pow(sp[43],4)*sp[27]*ang[0];
 
    kern[0][5] = (1.0/ denom)* kern[0][5]; 
    kern[0][6] =   - 4.E+0*sp[35]*sp[27]*sp[44]*ang[1] - 3.E+0*sp[35]*sp[27]*pow(sp[44],2)*
      ang[0] - 5.E+0*sp[35]*sp[27]*ang[2] + 3.E+0*sp[35]*pow(sp[27],2)*sp[44]*ang[0] + 4.E+0
      *sp[35]*pow(sp[27],2)*ang[1] - sp[35]*pow(sp[27],3)*ang[0] - 3.E+0*sp[35]*sp[44]*ang[2]
       + sp[35]*pow(sp[44],3)*ang[0] + 2.E+0*sp[35]*ang[3] - 1.2E+1*sp[34]*sp[43]*sp[27]*ang[1]
       + 4.E+0*sp[34]*sp[43]*pow(sp[27],2)*ang[0] - 4.E+0*sp[34]*sp[43]*sp[44]*ang[1] - 4.E+0
      *sp[34]*sp[43]*pow(sp[44],2)*ang[0] + 8.E+0*sp[34]*sp[43]*ang[2] - 4.E+0*pow(sp[34],2)*
      sp[27]*sp[44]*ang[0] + 8.E+0*pow(sp[34],2)*sp[44]*ang[1] + 4.E+0*pow(sp[34],2)*pow(
      sp[44],2)*ang[0] + 4.E+0*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 8.E+0*pow(sp[43],2)*
      sp[27]*ang[1] - 4.E+0*pow(sp[43],2)*pow(sp[27],2)*ang[0];
 
    kern[0][6] = (1.0/ denom)* kern[0][6]; 
    kern[0][7] =   0;
 
    kern[0][7] = (1.0/ denom)* kern[0][7]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[34],2) + 1.6E+1*pow(sp[35],2)*sp[27];

    kern[1][0] =   0;
 
    kern[1][0] = (1.0/ denom)* kern[1][0]; 
    kern[1][1] =   - 3.2E+1*sp[35]*sp[34]*sp[43]*sp[27]*ang[0] + 1.6E+1*sp[35]*pow(
      sp[34],2)*sp[27]*ang[0] + 1.6E+1*sp[35]*pow(sp[43],2)*sp[27]*ang[0] + 8.E+0*pow(
      sp[35],2)*sp[27]*sp[44]*ang[0] - 8.E+0*pow(sp[35],2)*sp[27]*ang[1] - 4.E+0*pow(
      sp[35],2)*pow(sp[27],2)*ang[0] + 8.E+0*pow(sp[35],2)*sp[44]*ang[1] - 4.E+0*pow(
      sp[35],2)*pow(sp[44],2)*ang[0] - 4.E+0*pow(sp[35],2)*ang[2];
 
    kern[1][1] = (1.0/ denom)* kern[1][1]; 
    kern[1][2] =  8.E+0*sp[35]*sp[34]*sp[43]*sp[27]*ang[1] - 8.E+0*sp[35]*sp[34]*sp[43]*pow(
      sp[27],2)*ang[0] - 8.E+0*sp[35]*sp[34]*sp[43]*sp[44]*ang[1] + 8.E+0*sp[35]*sp[34]*sp[43]*pow(
      sp[44],2)*ang[0] - 1.6E+1*sp[35]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 1.6E+1*sp[35]*
      pow(sp[43],2)*pow(sp[27],2)*ang[0];
 
    kern[1][2] = (1.0/ denom)* kern[1][2]; 
    kern[1][3] =   0;
 
    kern[1][3] = (1.0/ denom)* kern[1][3]; 
    kern[1][4] =   0;
 
    kern[1][4] = (1.0/ denom)* kern[1][4]; 
    kern[1][5] =   0;
 
    kern[1][5] = (1.0/ denom)* kern[1][5]; 
    kern[1][6] =   0;
 
    kern[1][6] = (1.0/ denom)* kern[1][6]; 
    kern[1][7] =   - 1.6E+1*sp[35]*sp[34]*sp[43]*sp[27]*ang[1] + 1.6E+1*sp[35]*sp[34]*sp[43]*
      pow(sp[27],2)*ang[0] + 1.6E+1*sp[35]*sp[34]*sp[43]*sp[44]*ang[1] - 1.6E+1*sp[35]*sp[34]*
      sp[43]*pow(sp[44],2)*ang[0] - 1.6E+1*sp[35]*pow(sp[34],2)*sp[27]*sp[44]*ang[0] + 1.6E+1
      *sp[35]*pow(sp[34],2)*pow(sp[44],2)*ang[0] + 1.6E+1*sp[35]*pow(sp[43],2)*sp[27]*sp[44]*
      ang[0] - 1.6E+1*sp[35]*pow(sp[43],2)*pow(sp[27],2)*ang[0] - 1.2E+1*pow(sp[35],2)*
      sp[27]*pow(sp[44],2)*ang[0] - 4.E+0*pow(sp[35],2)*sp[27]*ang[2] + 1.2E+1*pow(
      sp[35],2)*pow(sp[27],2)*sp[44]*ang[0] + 8.E+0*pow(sp[35],2)*pow(sp[27],2)*ang[1] - 
      4.E+0*pow(sp[35],2)*pow(sp[27],3)*ang[0] + 4.E+0*pow(sp[35],2)*sp[44]*ang[2] - 
      8.E+0*pow(sp[35],2)*pow(sp[44],2)*ang[1] + 4.E+0*pow(sp[35],2)*pow(sp[44],3)*
      ang[0];
 
    kern[1][7] = (1.0/ denom)* kern[1][7]; 

    denom =  - 8.E+0*sp[35]*sp[34]*sp[27] + 8.E+0*pow(sp[34],3);

    kern[2][0] =   0;
 
    kern[2][0] = (1.0/ denom)* kern[2][0]; 
    kern[2][1] =  8.E+0*sp[35]*sp[34]*sp[27]*ang[0] - 8.E+0*sp[35]*sp[34]*sp[44]*ang[0] + 
      8.E+0*sp[35]*sp[34]*ang[1] - 8.E+0*sp[35]*sp[43]*sp[27]*ang[0] + 8.E+0*sp[35]*sp[43]*sp[44]*
      ang[0] - 8.E+0*sp[35]*sp[43]*ang[1] - 1.6E+1*sp[34]*pow(sp[43],2)*ang[0] + 3.2E+1*
      pow(sp[34],2)*sp[43]*ang[0] - 1.6E+1*pow(sp[34],3)*ang[0];
 
    kern[2][1] = (1.0/ denom)* kern[2][1]; 
    kern[2][2] =  4.E+0*sp[35]*sp[43]*sp[27]*sp[44]*ang[0] - 2.E+0*sp[35]*sp[43]*pow(sp[27],2)
      *ang[0] - 2.E+0*sp[35]*sp[43]*pow(sp[44],2)*ang[0] + 2.E+0*sp[35]*sp[43]*ang[2] - 
      1.6E+1*sp[34]*pow(sp[43],2)*sp[27]*ang[0] + 8.E+0*sp[34]*pow(sp[43],2)*ang[1] + 
      8.E+0*pow(sp[34],2)*sp[43]*sp[27]*ang[0] - 8.E+0*pow(sp[34],2)*sp[43]*ang[1] + 8.E+0
      *pow(sp[43],3)*sp[27]*ang[0];
 
    kern[2][2] = (1.0/ denom)* kern[2][2]; 
    kern[2][3] =   0;
 
    kern[2][3] = (1.0/ denom)* kern[2][3]; 
    kern[2][4] =   0;
 
    kern[2][4] = (1.0/ denom)* kern[2][4]; 
    kern[2][5] =   0;
 
    kern[2][5] = (1.0/ denom)* kern[2][5]; 
    kern[2][6] =   0;
 
    kern[2][6] = (1.0/ denom)* kern[2][6]; 
    kern[2][7] =   - 8.E+0*sp[35]*sp[34]*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*sp[34]*sp[27]*
      ang[1] + 4.E+0*sp[35]*sp[34]*pow(sp[27],2)*ang[0] - 8.E+0*sp[35]*sp[34]*sp[44]*ang[1] + 
      4.E+0*sp[35]*sp[34]*pow(sp[44],2)*ang[0] + 4.E+0*sp[35]*sp[34]*ang[2] + 8.E+0*sp[35]*
      sp[43]*sp[27]*sp[44]*ang[0] + 8.E+0*sp[35]*sp[43]*sp[27]*ang[1] - 4.E+0*sp[35]*sp[43]*pow(
      sp[27],2)*ang[0] + 8.E+0*sp[35]*sp[43]*sp[44]*ang[1] - 4.E+0*sp[35]*sp[43]*pow(sp[44],2)*
      ang[0] - 4.E+0*sp[35]*sp[43]*ang[2] + 3.2E+1*sp[34]*pow(sp[43],2)*sp[27]*ang[0] + 
      1.6E+1*sp[34]*pow(sp[43],2)*sp[44]*ang[0] - 1.6E+1*sp[34]*pow(sp[43],2)*ang[1] - 
      1.6E+1*pow(sp[34],2)*sp[43]*sp[27]*ang[0] - 3.2E+1*pow(sp[34],2)*sp[43]*sp[44]*ang[0]
       + 1.6E+1*pow(sp[34],2)*sp[43]*ang[1] + 1.6E+1*pow(sp[34],3)*sp[44]*ang[0] - 
      1.6E+1*pow(sp[43],3)*sp[27]*ang[0];
 
    kern[2][7] = (1.0/ denom)* kern[2][7]; 

    denom =  - 3.2E+1*sp[35]*sp[27] + 3.2E+1*pow(sp[34],2);

    kern[3][0] =   0;
 
    kern[3][0] = (1.0/ denom)* kern[3][0]; 
    kern[3][1] =   0;
 
    kern[3][1] = (1.0/ denom)* kern[3][1]; 
    kern[3][2] =   0;
 
    kern[3][2] = (1.0/ denom)* kern[3][2]; 
    kern[3][3] =   - 1.6E+1*sp[35]*sp[27]*sp[44]*ang[0] - 3.2E+1*sp[35]*sp[27]*ang[1] + 
      8.E+0*sp[35]*pow(sp[27],2)*ang[0] - 3.2E+1*sp[35]*sp[44]*ang[1] + 8.E+0*sp[35]*pow(
      sp[44],2)*ang[0] + 2.4E+1*sp[35]*ang[2] - 3.2E+1*sp[34]*sp[43]*sp[27]*ang[0] - 3.2E+1*
      sp[34]*sp[43]*sp[44]*ang[0] + 6.4E+1*sp[34]*sp[43]*ang[1] + 3.2E+1*pow(sp[34],2)*sp[44]*
      ang[0] + 3.2E+1*pow(sp[43],2)*sp[27]*ang[0];
 
    kern[3][3] = (1.0/ denom)* kern[3][3]; 
    kern[3][4] =   0;
 
    kern[3][4] = (1.0/ denom)* kern[3][4]; 
    kern[3][5] =   0;
 
    kern[3][5] = (1.0/ denom)* kern[3][5]; 
    kern[3][6] =   0;
 
    kern[3][6] = (1.0/ denom)* kern[3][6]; 
    kern[3][7] =   0;
 
    kern[3][7] = (1.0/ denom)* kern[3][7]; 

    denom = 4.E+0*sp[35]*sp[27] - 4.E+0*pow(sp[34],2);

    kern[4][0] =   0;
 
    kern[4][0] = (1.0/ denom)* kern[4][0]; 
    kern[4][1] =   0;
 
    kern[4][1] = (1.0/ denom)* kern[4][1]; 
    kern[4][2] =   0;
 
    kern[4][2] = (1.0/ denom)* kern[4][2]; 
    kern[4][3] =   0;
 
    kern[4][3] = (1.0/ denom)* kern[4][3]; 
    kern[4][4] =  6.E+0*sp[35]*sp[27]*ang[1] + 6.E+0*sp[35]*sp[44]*ang[1] - 6.E+0*sp[35]*
      ang[2] - 1.2E+1*sp[34]*sp[43]*ang[1];
 
    kern[4][4] = (1.0/ denom)* kern[4][4]; 
    kern[4][5] =   0;
 
    kern[4][5] = (1.0/ denom)* kern[4][5]; 
    kern[4][6] =   0;
 
    kern[4][6] = (1.0/ denom)* kern[4][6]; 
    kern[4][7] =   0;
 
    kern[4][7] = (1.0/ denom)* kern[4][7]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[34],3)*sp[27] + 8.E+0*pow(sp[35],2)*sp[34]*pow(
      sp[27],2) + 8.E+0*pow(sp[34],5);

    kern[5][0] =   - 4.E+0*sp[35]*sp[34]*sp[27]*sp[44]*ang[0] - 4.E+0*sp[35]*sp[34]*sp[27]*
      ang[1] - 2.E+0*sp[35]*sp[34]*pow(sp[27],2)*ang[0] - 1.2E+1*sp[35]*sp[34]*sp[44]*ang[1] + 
      6.E+0*sp[35]*sp[34]*pow(sp[44],2)*ang[0] + 6.E+0*sp[35]*sp[34]*ang[2] - 8.E+0*sp[35]*
      sp[43]*sp[27]*sp[44]*ang[0] + 8.E+0*sp[35]*sp[43]*sp[27]*ang[1] + 8.E+0*sp[35]*sp[43]*pow(
      sp[27],2)*ang[0] + 2.4E+1*sp[34]*pow(sp[43],2)*sp[27]*ang[0] - 3.2E+1*pow(sp[34],2)*
      sp[43]*sp[27]*ang[0] - 1.6E+1*pow(sp[34],2)*sp[43]*sp[44]*ang[0] + 1.6E+1*pow(sp[34],2)
      *sp[43]*ang[1] + 8.E+0*pow(sp[34],3)*sp[27]*ang[0] + 1.6E+1*pow(sp[34],3)*sp[44]*
      ang[0] - 8.E+0*pow(sp[34],3)*ang[1];
 
    kern[5][0] = (1.0/ denom)* kern[5][0]; 
    kern[5][1] =   0;
 
    kern[5][1] = (1.0/ denom)* kern[5][1]; 
    kern[5][2] =   0;
 
    kern[5][2] = (1.0/ denom)* kern[5][2]; 
    kern[5][3] =   0;
 
    kern[5][3] = (1.0/ denom)* kern[5][3]; 
    kern[5][4] =   0;
 
    kern[5][4] = (1.0/ denom)* kern[5][4]; 
    kern[5][5] =  1.2E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 1.2E+1*sp[35]*
      sp[34]*pow(sp[43],2)*sp[27]*ang[1] + 1.4E+1*sp[35]*sp[34]*pow(sp[43],2)*pow(sp[27],2)*
      ang[0] - 1.2E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[44]*ang[1] + 6.E+0*sp[35]*sp[34]*pow(
      sp[43],2)*pow(sp[44],2)*ang[0] + 6.E+0*sp[35]*sp[34]*pow(sp[43],2)*ang[2] - 4.E+0*
      sp[35]*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0] + 1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*
      ang[1] - 6.E+0*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 1.6E+1*sp[35]*pow(
      sp[34],2)*sp[43]*sp[44]*ang[1] - 6.E+0*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[44],2)*ang[0] - 
      1.E+1*sp[35]*pow(sp[34],2)*sp[43]*ang[2] - 8.E+0*sp[35]*pow(sp[43],3)*sp[27]*sp[44]*ang[0]
       + 8.E+0*sp[35]*pow(sp[43],3)*sp[27]*ang[1] - 8.E+0*sp[35]*pow(sp[43],3)*pow(
      sp[27],2)*ang[0] - 4.E+0*pow(sp[35],2)*sp[43]*sp[27]*sp[44]*ang[1] + 4.E+0*pow(
      sp[35],2)*sp[43]*sp[27]*ang[2] - 4.E+0*pow(sp[35],2)*sp[43]*pow(sp[27],2)*ang[1] + 
      2.4E+1*sp[34]*pow(sp[43],4)*sp[27]*ang[0] - 4.E+1*pow(sp[34],2)*pow(sp[43],3)*sp[27]*
      ang[0] - 1.6E+1*pow(sp[34],2)*pow(sp[43],3)*sp[44]*ang[0] + 1.6E+1*pow(sp[34],2)*
      pow(sp[43],3)*ang[1] + 1.6E+1*pow(sp[34],3)*pow(sp[43],2)*sp[27]*ang[0] + 2.4E+1*
      pow(sp[34],3)*pow(sp[43],2)*sp[44]*ang[0] - 2.4E+1*pow(sp[34],3)*pow(sp[43],2)*
      ang[1] - 8.E+0*pow(sp[34],4)*sp[43]*sp[44]*ang[0];
 
    kern[5][5] = (1.0/ denom)* kern[5][5]; 
    kern[5][6] =   - 4.E+0*sp[35]*sp[34]*sp[27]*sp[44]*ang[1] + sp[35]*sp[34]*sp[27]*pow(
      sp[44],2)*ang[0] + 1.5E+1*sp[35]*sp[34]*sp[27]*ang[2] - sp[35]*sp[34]*pow(sp[27],2)*sp[44]*
      ang[0] - 1.2E+1*sp[35]*sp[34]*pow(sp[27],2)*ang[1] + 3.E+0*sp[35]*sp[34]*pow(sp[27],3)*
      ang[0] + 9.E+0*sp[35]*sp[34]*sp[44]*ang[2] - 3.E+0*sp[35]*sp[34]*pow(sp[44],3)*ang[0] - 
      6.E+0*sp[35]*sp[34]*ang[3] + 4.E+0*sp[35]*sp[43]*sp[27]*sp[44]*ang[1] + 4.E+0*sp[35]*sp[43]*
      sp[27]*pow(sp[44],2)*ang[0] - 8.E+0*sp[35]*sp[43]*sp[27]*ang[2] + 1.2E+1*sp[35]*sp[43]*
      pow(sp[27],2)*ang[1] - 4.E+0*sp[35]*sp[43]*pow(sp[27],3)*ang[0] - 1.2E+1*sp[34]*pow(
      sp[43],2)*sp[27]*sp[44]*ang[0] - 2.4E+1*sp[34]*pow(sp[43],2)*sp[27]*ang[1] + 1.2E+1*sp[34]
      *pow(sp[43],2)*pow(sp[27],2)*ang[0] + 2.4E+1*pow(sp[34],2)*sp[43]*sp[27]*ang[1] - 
      8.E+0*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 8.E+0*pow(sp[34],2)*sp[43]*sp[44]*
      ang[1] + 8.E+0*pow(sp[34],2)*sp[43]*pow(sp[44],2)*ang[0] - 1.6E+1*pow(sp[34],2)*
      sp[43]*ang[2] + 4.E+0*pow(sp[34],3)*sp[27]*sp[44]*ang[0] - 8.E+0*pow(sp[34],3)*sp[44]*
      ang[1] - 4.E+0*pow(sp[34],3)*pow(sp[44],2)*ang[0];
 
    kern[5][6] = (1.0/ denom)* kern[5][6]; 
    kern[5][7] =   0;
 
    kern[5][7] = (1.0/ denom)* kern[5][7]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[34],2)*sp[27] + 8.E+0*pow(sp[35],2)*pow(sp[27],2)
       + 8.E+0*pow(sp[34],4);

    kern[6][0] =  8.E+0*sp[35]*sp[34]*sp[43]*sp[27]*ang[0] - 2.4E+1*sp[35]*sp[34]*sp[43]*sp[44]*
      ang[0] + 2.4E+1*sp[35]*sp[34]*sp[43]*ang[1] - 1.6E+1*sp[35]*pow(sp[34],2)*sp[27]*ang[0]
       + 2.4E+1*sp[35]*pow(sp[34],2)*sp[44]*ang[0] - 1.6E+1*sp[35]*pow(sp[34],2)*ang[1] + 
      8.E+0*sp[35]*pow(sp[43],2)*sp[27]*ang[0] - 1.2E+1*pow(sp[35],2)*sp[27]*sp[44]*ang[0] + 
      4.E+0*pow(sp[35],2)*sp[27]*ang[1] + 6.E+0*pow(sp[35],2)*pow(sp[27],2)*ang[0] - 
      1.2E+1*pow(sp[35],2)*sp[44]*ang[1] + 6.E+0*pow(sp[35],2)*pow(sp[44],2)*ang[0] + 
      6.E+0*pow(sp[35],2)*ang[2] + 1.6E+1*pow(sp[34],2)*pow(sp[43],2)*ang[0] - 
      3.2E+1*pow(sp[34],3)*sp[43]*ang[0] + 1.6E+1*pow(sp[34],4)*ang[0];
 
    kern[6][0] = (1.0/ denom)* kern[6][0]; 
    kern[6][1] =   0;
 
    kern[6][1] = (1.0/ denom)* kern[6][1]; 
    kern[6][2] =   0;
 
    kern[6][2] = (1.0/ denom)* kern[6][2]; 
    kern[6][3] =   0;
 
    kern[6][3] = (1.0/ denom)* kern[6][3]; 
    kern[6][4] =   0;
 
    kern[6][4] = (1.0/ denom)* kern[6][4]; 
    kern[6][5] =   - 1.6E+1*sp[35]*sp[34]*pow(sp[43],3)*sp[27]*ang[0] - 2.4E+1*sp[35]*
      sp[34]*pow(sp[43],3)*sp[44]*ang[0] + 2.4E+1*sp[35]*sp[34]*pow(sp[43],3)*ang[1] + 1.6E+1
      *sp[35]*pow(sp[34],2)*pow(sp[43],2)*sp[27]*ang[0] + 4.E+1*sp[35]*pow(sp[34],2)*pow(
      sp[43],2)*sp[44]*ang[0] - 3.2E+1*sp[35]*pow(sp[34],2)*pow(sp[43],2)*ang[1] - 8.E+0*
      sp[35]*pow(sp[34],3)*sp[43]*sp[27]*ang[0] - 1.6E+1*sp[35]*pow(sp[34],3)*sp[43]*sp[44]*ang[0]
       + 8.E+0*sp[35]*pow(sp[34],3)*sp[43]*ang[1] + 8.E+0*sp[35]*pow(sp[43],4)*sp[27]*ang[0]
       + 4.E+0*pow(sp[35],2)*sp[34]*sp[43]*sp[27]*sp[44]*ang[0] + 4.E+0*pow(sp[35],2)*sp[34]*
      sp[43]*sp[27]*ang[1] + 2.E+0*pow(sp[35],2)*sp[34]*sp[43]*pow(sp[27],2)*ang[0] + 1.2E+1*
      pow(sp[35],2)*sp[34]*sp[43]*sp[44]*ang[1] - 6.E+0*pow(sp[35],2)*sp[34]*sp[43]*pow(sp[44],2)
      *ang[0] - 6.E+0*pow(sp[35],2)*sp[34]*sp[43]*ang[2] - 4.E+0*pow(sp[35],2)*pow(
      sp[43],2)*sp[27]*sp[44]*ang[0] - 4.E+0*pow(sp[35],2)*pow(sp[43],2)*sp[27]*ang[1] - 
      2.E+0*pow(sp[35],2)*pow(sp[43],2)*pow(sp[27],2)*ang[0] - 1.2E+1*pow(sp[35],2)*
      pow(sp[43],2)*sp[44]*ang[1] + 6.E+0*pow(sp[35],2)*pow(sp[43],2)*pow(sp[44],2)*ang[0]
       + 6.E+0*pow(sp[35],2)*pow(sp[43],2)*ang[2] + 1.6E+1*pow(sp[34],2)*pow(
      sp[43],4)*ang[0] - 3.2E+1*pow(sp[34],3)*pow(sp[43],3)*ang[0] + 1.6E+1*pow(
      sp[34],4)*pow(sp[43],2)*ang[0];
 
    kern[6][5] = (1.0/ denom)* kern[6][5]; 
    kern[6][6] =   - 8.E+0*sp[35]*sp[34]*sp[43]*sp[27]*sp[44]*ang[0] + 2.8E+1*sp[35]*sp[34]*
      sp[43]*sp[27]*ang[1] - 4.E+0*sp[35]*sp[34]*sp[43]*pow(sp[27],2)*ang[0] + 1.2E+1*sp[35]*sp[34]
      *sp[43]*sp[44]*ang[1] + 1.2E+1*sp[35]*sp[34]*sp[43]*pow(sp[44],2)*ang[0] - 2.4E+1*sp[35]*
      sp[34]*sp[43]*ang[2] + 4.E+0*sp[35]*pow(sp[34],2)*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*pow(
      sp[34],2)*sp[27]*ang[1] + 4.E+0*sp[35]*pow(sp[34],2)*pow(sp[27],2)*ang[0] - 8.E+0*
      sp[35]*pow(sp[34],2)*sp[44]*ang[1] - 8.E+0*sp[35]*pow(sp[34],2)*pow(sp[44],2)*ang[0] + 
      4.E+0*sp[35]*pow(sp[34],2)*ang[2] - 4.E+0*sp[35]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 
      8.E+0*sp[35]*pow(sp[43],2)*sp[27]*ang[1] + 4.E+0*sp[35]*pow(sp[43],2)*pow(sp[27],2)*
      ang[0] - 4.E+0*pow(sp[35],2)*sp[27]*sp[44]*ang[1] + 5.E+0*pow(sp[35],2)*sp[27]*pow(
      sp[44],2)*ang[0] + 1.1E+1*pow(sp[35],2)*sp[27]*ang[2] - pow(sp[35],2)*pow(sp[27],2)*
      sp[44]*ang[0] - 4.E+0*pow(sp[35],2)*pow(sp[27],2)*ang[1] - pow(sp[35],2)*pow(
      sp[27],3)*ang[0] + 9.E+0*pow(sp[35],2)*sp[44]*ang[2] - 3.E+0*pow(sp[35],2)*pow(
      sp[44],3)*ang[0] - 6.E+0*pow(sp[35],2)*ang[3] + 8.E+0*pow(sp[34],2)*pow(sp[43],2)
      *sp[27]*ang[0] - 8.E+0*pow(sp[34],2)*pow(sp[43],2)*sp[44]*ang[0] - 1.6E+1*pow(
      sp[34],2)*pow(sp[43],2)*ang[1] - 8.E+0*pow(sp[34],3)*sp[43]*sp[27]*ang[0] + 8.E+0*
      pow(sp[34],3)*sp[43]*sp[44]*ang[0] + 8.E+0*pow(sp[34],3)*sp[43]*ang[1];
 
    kern[6][6] = (1.0/ denom)* kern[6][6]; 
    kern[6][7] =   0;
 
    kern[6][7] = (1.0/ denom)* kern[6][7]; 

    denom =  - 6.4E+1*sp[35]*pow(sp[34],2)*sp[27] + 3.2E+1*pow(sp[35],2)*pow(sp[27],2)
       + 3.2E+1*pow(sp[34],4);

    kern[7][0] =   0;
 
    kern[7][0] = (1.0/ denom)* kern[7][0]; 
    kern[7][1] =   - 1.6E+1*sp[35]*sp[34]*sp[43]*sp[27]*ang[0] + 4.8E+1*sp[35]*sp[34]*sp[43]*
      sp[44]*ang[0] - 4.8E+1*sp[35]*sp[34]*sp[43]*ang[1] + 3.2E+1*sp[35]*pow(sp[34],2)*sp[27]*
      ang[0] - 4.8E+1*sp[35]*pow(sp[34],2)*sp[44]*ang[0] + 3.2E+1*sp[35]*pow(sp[34],2)*
      ang[1] - 1.6E+1*sp[35]*pow(sp[43],2)*sp[27]*ang[0] + 2.4E+1*pow(sp[35],2)*sp[27]*sp[44]
      *ang[0] - 8.E+0*pow(sp[35],2)*sp[27]*ang[1] - 1.2E+1*pow(sp[35],2)*pow(sp[27],2)*
      ang[0] + 2.4E+1*pow(sp[35],2)*sp[44]*ang[1] - 1.2E+1*pow(sp[35],2)*pow(sp[44],2)*
      ang[0] - 1.2E+1*pow(sp[35],2)*ang[2] - 3.2E+1*pow(sp[34],2)*pow(sp[43],2)*ang[0]
       + 6.4E+1*pow(sp[34],3)*sp[43]*ang[0] - 3.2E+1*pow(sp[34],4)*ang[0];
 
    kern[7][1] = (1.0/ denom)* kern[7][1]; 
    kern[7][2] =   - 8.E+0*sp[35]*sp[34]*sp[43]*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*sp[34]*sp[43]
      *sp[27]*ang[1] - 4.E+0*sp[35]*sp[34]*sp[43]*pow(sp[27],2)*ang[0] - 2.4E+1*sp[35]*sp[34]*
      sp[43]*sp[44]*ang[1] + 1.2E+1*sp[35]*sp[34]*sp[43]*pow(sp[44],2)*ang[0] + 1.2E+1*sp[35]*
      sp[34]*sp[43]*ang[2] - 1.6E+1*sp[35]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 1.6E+1*sp[35]*
      pow(sp[43],2)*sp[27]*ang[1] + 1.6E+1*sp[35]*pow(sp[43],2)*pow(sp[27],2)*ang[0] + 
      4.8E+1*sp[34]*pow(sp[43],3)*sp[27]*ang[0] - 6.4E+1*pow(sp[34],2)*pow(sp[43],2)*sp[27]
      *ang[0] - 3.2E+1*pow(sp[34],2)*pow(sp[43],2)*sp[44]*ang[0] + 3.2E+1*pow(sp[34],2)
      *pow(sp[43],2)*ang[1] + 1.6E+1*pow(sp[34],3)*sp[43]*sp[27]*ang[0] + 3.2E+1*pow(
      sp[34],3)*sp[43]*sp[44]*ang[0] - 1.6E+1*pow(sp[34],3)*sp[43]*ang[1];
 
    kern[7][2] = (1.0/ denom)* kern[7][2]; 
    kern[7][3] =   0;
 
    kern[7][3] = (1.0/ denom)* kern[7][3]; 
    kern[7][4] =   0;
 
    kern[7][4] = (1.0/ denom)* kern[7][4]; 
    kern[7][5] =   0;
 
    kern[7][5] = (1.0/ denom)* kern[7][5]; 
    kern[7][6] =   0;
 
    kern[7][6] = (1.0/ denom)* kern[7][6]; 
    kern[7][7] =   - 1.6E+1*sp[35]*sp[34]*sp[43]*sp[27]*sp[44]*ang[0] + 6.4E+1*sp[35]*sp[34]*
      sp[43]*sp[27]*ang[1] - 4.E+1*sp[35]*sp[34]*sp[43]*pow(sp[27],2)*ang[0] + 9.6E+1*sp[35]*sp[34]
      *sp[43]*sp[44]*ang[1] - 7.2E+1*sp[35]*sp[34]*sp[43]*pow(sp[44],2)*ang[0] - 2.4E+1*sp[35]*
      sp[34]*sp[43]*ang[2] - 1.6E+1*sp[35]*pow(sp[34],2)*sp[27]*ang[1] + 8.E+0*sp[35]*pow(
      sp[34],2)*pow(sp[27],2)*ang[0] - 4.8E+1*sp[35]*pow(sp[34],2)*sp[44]*ang[1] + 5.6E+1*
      sp[35]*pow(sp[34],2)*pow(sp[44],2)*ang[0] + 8.E+0*sp[35]*pow(sp[34],2)*ang[2] + 
      4.8E+1*sp[35]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 3.2E+1*sp[35]*pow(sp[43],2)*sp[27]*
      ang[1] + 1.6E+1*sp[35]*pow(sp[43],2)*pow(sp[27],2)*ang[0] - 2.E+1*pow(sp[35],2)*
      sp[27]*pow(sp[44],2)*ang[0] + 4.E+0*pow(sp[35],2)*sp[27]*ang[2] + 4.E+0*pow(
      sp[35],2)*pow(sp[27],2)*sp[44]*ang[0] - 8.E+0*pow(sp[35],2)*pow(sp[27],2)*ang[1] + 
      4.E+0*pow(sp[35],2)*pow(sp[27],3)*ang[0] + 1.2E+1*pow(sp[35],2)*sp[44]*ang[2] - 
      2.4E+1*pow(sp[35],2)*pow(sp[44],2)*ang[1] + 1.2E+1*pow(sp[35],2)*pow(sp[44],3)*
      ang[0] - 9.6E+1*sp[34]*pow(sp[43],3)*sp[27]*ang[0] + 1.28E+2*pow(sp[34],2)*pow(
      sp[43],2)*sp[27]*ang[0] + 9.6E+1*pow(sp[34],2)*pow(sp[43],2)*sp[44]*ang[0] - 6.4E+1*
      pow(sp[34],2)*pow(sp[43],2)*ang[1] - 3.2E+1*pow(sp[34],3)*sp[43]*sp[27]*ang[0] - 
      1.28E+2*pow(sp[34],3)*sp[43]*sp[44]*ang[0] + 3.2E+1*pow(sp[34],3)*sp[43]*ang[1] + 
      3.2E+1*pow(sp[34],4)*sp[44]*ang[0];
 
    kern[7][7] = (1.0/ denom)* kern[7][7]; 

}


void kernelY_noopti(array<array<Cdoub, K_N_PROJECTORS_DIRAC>,
                            K_N_BASE_ELEMENTS_DIRAC>& kern,
  const ArrayScalarProducts& sp,  const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


    // local variables
    Cdoub denom;


    denom =  - 8.E+0*sp[35]*sp[44] + 8.E+0*pow(sp[43],2);

    kern[0][0] =   - 8.E+0*sp[35]*sp[44]*ssp*ssm - 8.E+0*sp[35]*sp[39]*sp[37]*svp*
      svm + 8.E+0*sp[43]*sp[31]*sp[37]*svp*svm + 8.E+0*sp[43]*sp[29]*sp[39]*svp*svm
       + 8.E+0*pow(sp[43],2)*ssp*ssm - 8.E+0*sp[31]*sp[29]*sp[44]*svp*svm;
 
    kern[0][0] = (1.0/ denom)* kern[0][0]; 
    kern[0][1] =  8.E+0*sp[35]*sp[31]*sp[44]*ssp*svm - 8.E+0*sp[35]*sp[29]*sp[44]*svp*
      ssm - 8.E+0*pow(sp[43],2)*sp[31]*ssp*svm + 8.E+0*pow(sp[43],2)*sp[29]*svp*
      ssm;
 
    kern[0][1] = (1.0/ denom)* kern[0][1]; 
    kern[0][2] =   - 8.E+0*sp[35]*sp[43]*sp[44]*sp[39]*ssp*svm + 8.E+0*sp[35]*sp[43]*sp[44]
      *sp[37]*svp*ssm + 8.E+0*pow(sp[43],3)*sp[39]*ssp*svm - 8.E+0*pow(sp[43],3)*
      sp[37]*svp*ssm;
 
    kern[0][2] = (1.0/ denom)* kern[0][2]; 
    kern[0][3] =  1.6E+1*sp[35]*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*sp[35]*sp[29]*
      sp[44]*sp[39]*svp*svm - 1.6E+1*pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 1.6E+1*
      pow(sp[43],2)*sp[29]*sp[39]*svp*svm;
 
    kern[0][3] = (1.0/ denom)* kern[0][3]; 
    kern[0][4] =   0;
 
    kern[0][4] = (1.0/ denom)* kern[0][4]; 
    kern[0][5] =   0;
 
    kern[0][5] = (1.0/ denom)* kern[0][5]; 
    kern[0][6] =   0;
 
    kern[0][6] = (1.0/ denom)* kern[0][6]; 
    kern[0][7] =   0;
 
    kern[0][7] = (1.0/ denom)* kern[0][7]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[43],2) + 1.6E+1*pow(sp[35],2)*sp[44];

    kern[1][0] =   - 1.6E+1*sp[35]*sp[43]*sp[39]*ssp*svm + 1.6E+1*sp[35]*sp[43]*sp[37]*
      svp*ssm + 1.6E+1*sp[35]*sp[31]*sp[44]*ssp*svm - 1.6E+1*sp[35]*sp[29]*sp[44]*svp*
      ssm;
 
    kern[1][0] = (1.0/ denom)* kern[1][0]; 
    kern[1][1] =   - 1.6E+1*sp[35]*pow(sp[43],2)*ssp*ssm + 1.6E+1*sp[35]*sp[31]*
      sp[29]*sp[44]*svp*svm + 1.6E+1*pow(sp[35],2)*sp[44]*ssp*ssm - 1.6E+1*pow(
      sp[35],2)*sp[39]*sp[37]*svp*svm;
 
    kern[1][1] = (1.0/ denom)* kern[1][1]; 
    kern[1][2] =   - 1.6E+1*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*sp[35]*
      sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 3.2E+1*sp[35]*pow(sp[43],2)*sp[39]*sp[37]*svp*
      svm;
 
    kern[1][2] = (1.0/ denom)* kern[1][2]; 
    kern[1][3] =   - 3.2E+1*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm - 3.2E+1*sp[35]*
      pow(sp[43],2)*sp[37]*svp*ssm + 3.2E+1*pow(sp[35],2)*sp[44]*sp[39]*ssp*svm + 
      3.2E+1*pow(sp[35],2)*sp[44]*sp[37]*svp*ssm;
 
    kern[1][3] = (1.0/ denom)* kern[1][3]; 
    kern[1][4] =   0;
 
    kern[1][4] = (1.0/ denom)* kern[1][4]; 
    kern[1][5] =   0;
 
    kern[1][5] = (1.0/ denom)* kern[1][5]; 
    kern[1][6] =   0;
 
    kern[1][6] = (1.0/ denom)* kern[1][6]; 
    kern[1][7] =   0;
 
    kern[1][7] = (1.0/ denom)* kern[1][7]; 

    denom =  - 8.E+0*sp[35]*sp[43]*sp[44] + 8.E+0*pow(sp[43],3);

    kern[2][0] =  8.E+0*sp[35]*sp[39]*ssp*svm - 8.E+0*sp[35]*sp[37]*svp*ssm - 
      8.E+0*sp[43]*sp[31]*ssp*svm + 8.E+0*sp[43]*sp[29]*svp*ssm;
 
    kern[2][0] = (1.0/ denom)* kern[2][0]; 
    kern[2][1] =  8.E+0*sp[35]*sp[31]*sp[37]*svp*svm + 8.E+0*sp[35]*sp[29]*sp[39]*svp
      *svm - 1.6E+1*sp[43]*sp[31]*sp[29]*svp*svm;
 
    kern[2][1] = (1.0/ denom)* kern[2][1]; 
    kern[2][2] =   - 8.E+0*sp[35]*sp[43]*sp[44]*ssp*ssm - 8.E+0*sp[35]*sp[43]*sp[39]*
      sp[37]*svp*svm + 8.E+0*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm + 8.E+0*pow(sp[43],3)
      *ssp*ssm;
 
    kern[2][2] = (1.0/ denom)* kern[2][2]; 
    kern[2][3] =   - 1.6E+1*sp[35]*sp[31]*sp[44]*ssp*svm - 1.6E+1*sp[35]*sp[29]*sp[44]*
      svp*ssm + 1.6E+1*pow(sp[43],2)*sp[31]*ssp*svm + 1.6E+1*pow(sp[43],2)*sp[29]
      *svp*ssm;
 
    kern[2][3] = (1.0/ denom)* kern[2][3]; 
    kern[2][4] =   0;
 
    kern[2][4] = (1.0/ denom)* kern[2][4]; 
    kern[2][5] =   0;
 
    kern[2][5] = (1.0/ denom)* kern[2][5]; 
    kern[2][6] =   0;
 
    kern[2][6] = (1.0/ denom)* kern[2][6]; 
    kern[2][7] =   0;
 
    kern[2][7] = (1.0/ denom)* kern[2][7]; 

    denom =  - 3.2E+1*sp[35]*sp[44] + 3.2E+1*pow(sp[43],2);

    kern[3][0] =  1.6E+1*sp[31]*sp[37]*svp*svm - 1.6E+1*sp[29]*sp[39]*svp*svm;
 
    kern[3][0] = (1.0/ denom)* kern[3][0]; 
    kern[3][1] =  1.6E+1*sp[35]*sp[39]*ssp*svm + 1.6E+1*sp[35]*sp[37]*svp*ssm - 
      1.6E+1*sp[43]*sp[31]*ssp*svm - 1.6E+1*sp[43]*sp[29]*svp*ssm;
 
    kern[3][1] = (1.0/ denom)* kern[3][1]; 
    kern[3][2] =  1.6E+1*sp[43]*sp[31]*sp[44]*ssp*svm + 1.6E+1*sp[43]*sp[29]*sp[44]*svp
      *ssm - 1.6E+1*pow(sp[43],2)*sp[39]*ssp*svm - 1.6E+1*pow(sp[43],2)*sp[37]*
      svp*ssm;
 
    kern[3][2] = (1.0/ denom)* kern[3][2]; 
    kern[3][3] =   - 3.2E+1*sp[35]*sp[44]*ssp*ssm + 3.2E+1*sp[35]*sp[39]*sp[37]*svp*
      svm - 3.2E+1*sp[43]*sp[31]*sp[37]*svp*svm - 3.2E+1*sp[43]*sp[29]*sp[39]*svp*svm
       + 3.2E+1*pow(sp[43],2)*ssp*ssm + 3.2E+1*sp[31]*sp[29]*sp[44]*svp*svm;
 
    kern[3][3] = (1.0/ denom)* kern[3][3]; 
    kern[3][4] =   0;
 
    kern[3][4] = (1.0/ denom)* kern[3][4]; 
    kern[3][5] =   0;
 
    kern[3][5] = (1.0/ denom)* kern[3][5]; 
    kern[3][6] =   0;
 
    kern[3][6] = (1.0/ denom)* kern[3][6]; 
    kern[3][7] =   0;
 
    kern[3][7] = (1.0/ denom)* kern[3][7]; 

    denom = 4.E+0*sp[35]*sp[44] - 4.E+0*pow(sp[43],2);

    kern[4][0] =  4.E+0*sp[35]*sp[39]*ssp*svm + 4.E+0*sp[35]*sp[37]*svp*ssm - 
      4.E+0*sp[43]*sp[31]*ssp*svm - 4.E+0*sp[43]*sp[29]*svp*ssm;
 
    kern[4][0] = (1.0/ denom)* kern[4][0]; 
    kern[4][1] =   - 4.E+0*sp[35]*sp[31]*sp[37]*svp*svm + 4.E+0*sp[35]*sp[29]*sp[39]*
      svp*svm;
 
    kern[4][1] = (1.0/ denom)* kern[4][1]; 
    kern[4][2] =  4.E+0*pow(sp[43],2)*sp[31]*sp[37]*svp*svm - 4.E+0*pow(sp[43],2)
      *sp[29]*sp[39]*svp*svm;
 
    kern[4][2] = (1.0/ denom)* kern[4][2]; 
    kern[4][3] =   0;
 
    kern[4][3] = (1.0/ denom)* kern[4][3]; 
    kern[4][4] =   - 4.E+0*sp[35]*sp[44]*sp[7]*svp*svm + 4.E+0*sp[35]*sp[44]*ssp*
      ssm + 4.E+0*pow(sp[43],2)*sp[7]*svp*svm - 4.E+0*pow(sp[43],2)*ssp*ssm;
 
    kern[4][4] = (1.0/ denom)* kern[4][4]; 
    kern[4][5] =  4.E+0*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm + 4.E+0*sp[35]*sp[43]*sp[29]*
      sp[44]*svp*ssm - 4.E+0*pow(sp[43],3)*sp[31]*ssp*svm - 4.E+0*pow(sp[43],3)*
      sp[29]*svp*ssm;
 
    kern[4][5] = (1.0/ denom)* kern[4][5]; 
    kern[4][6] =   - 4.E+0*sp[35]*sp[44]*sp[39]*ssp*svm - 4.E+0*sp[35]*sp[44]*sp[37]*
      svp*ssm + 4.E+0*pow(sp[43],2)*sp[39]*ssp*svm + 4.E+0*pow(sp[43],2)*sp[37]*
      svp*ssm;
 
    kern[4][6] = (1.0/ denom)* kern[4][6]; 
    kern[4][7] =  8.E+0*sp[35]*sp[31]*sp[44]*sp[37]*svp*svm - 8.E+0*sp[35]*sp[29]*sp[44]*
      sp[39]*svp*svm - 8.E+0*pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 8.E+0*pow(
      sp[43],2)*sp[29]*sp[39]*svp*svm;
 
    kern[4][7] = (1.0/ denom)* kern[4][7]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44] + 8.E+0*pow(sp[35],2)*sp[43]*pow(
      sp[44],2) + 8.E+0*pow(sp[43],5);

    kern[5][0] =   - 8.E+0*sp[35]*sp[43]*sp[44]*sp[7]*svp*svm + 2.4E+1*sp[35]*sp[43]*
      sp[39]*sp[37]*svp*svm - 8.E+0*sp[35]*sp[31]*sp[44]*sp[37]*svp*svm - 8.E+0*sp[35]*
      sp[29]*sp[44]*sp[39]*svp*svm + 2.4E+1*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm - 1.6E+1
      *pow(sp[43],2)*sp[31]*sp[37]*svp*svm - 1.6E+1*pow(sp[43],2)*sp[29]*sp[39]*svp*
      svm + 8.E+0*pow(sp[43],3)*sp[7]*svp*svm;
 
    kern[5][0] = (1.0/ denom)* kern[5][0]; 
    kern[5][1] =   - 8.E+0*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm + 8.E+0*sp[35]*sp[43]*
      sp[29]*sp[44]*svp*ssm - 8.E+0*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm + 8.E+0*sp[35]*
      pow(sp[43],2)*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*sp[44]*sp[39]*ssp*svm - 
      8.E+0*pow(sp[35],2)*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(sp[43],3)*sp[31]*ssp*svm
       - 8.E+0*pow(sp[43],3)*sp[29]*svp*ssm;
 
    kern[5][1] = (1.0/ denom)* kern[5][1]; 
    kern[5][2] =   0;
 
    kern[5][2] = (1.0/ denom)* kern[5][2]; 
    kern[5][3] =   - 1.6E+1*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm + 1.6E+1*sp[35]*
      sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 1.6E+1*pow(sp[43],3)*sp[31]*sp[37]*svp*svm
       - 1.6E+1*pow(sp[43],3)*sp[29]*sp[39]*svp*svm;
 
    kern[5][3] = (1.0/ denom)* kern[5][3]; 
    kern[5][4] =  8.E+0*sp[35]*sp[43]*sp[44]*sp[39]*ssp*svm + 8.E+0*sp[35]*sp[43]*sp[44]*
      sp[37]*svp*ssm - 8.E+0*sp[35]*sp[31]*pow(sp[44],2)*ssp*svm - 8.E+0*sp[35]*sp[29]
      *pow(sp[44],2)*svp*ssm + 8.E+0*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm + 8.E+0*
      pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 8.E+0*pow(sp[43],3)*sp[39]*ssp*svm - 
      8.E+0*pow(sp[43],3)*sp[37]*svp*ssm;
 
    kern[5][4] = (1.0/ denom)* kern[5][4]; 
    kern[5][5] =   - 1.6E+1*sp[35]*sp[43]*sp[31]*sp[29]*pow(sp[44],2)*svp*svm + 
      8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*sp[35]*pow(sp[43],2)
      *sp[29]*sp[44]*sp[39]*svp*svm - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44]*sp[7]*svp*svm
       - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44]*ssp*ssm + 8.E+0*pow(sp[35],2)*sp[43]*pow(
      sp[44],2)*sp[7]*svp*svm + 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[44],2)*ssp*ssm + 
      1.6E+1*pow(sp[43],3)*sp[31]*sp[29]*sp[44]*svp*svm - 8.E+0*pow(sp[43],4)*sp[31]*
      sp[37]*svp*svm - 8.E+0*pow(sp[43],4)*sp[29]*sp[39]*svp*svm + 8.E+0*pow(
      sp[43],5)*sp[7]*svp*svm + 8.E+0*pow(sp[43],5)*ssp*ssm;
 
    kern[5][5] = (1.0/ denom)* kern[5][5]; 
    kern[5][6] =   - 1.6E+1*sp[35]*sp[43]*sp[44]*sp[39]*sp[37]*svp*svm + 8.E+0*sp[35]*
      sp[31]*pow(sp[44],2)*sp[37]*svp*svm + 8.E+0*sp[35]*sp[29]*pow(sp[44],2)*sp[39]*svp
      *svm - 8.E+0*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm - 8.E+0*pow(sp[43],2)*
      sp[29]*sp[44]*sp[39]*svp*svm + 1.6E+1*pow(sp[43],3)*sp[39]*sp[37]*svp*svm;
 
    kern[5][6] = (1.0/ denom)* kern[5][6]; 
    kern[5][7] =  3.2E+1*sp[35]*pow(sp[43],2)*sp[44]*sp[39]*ssp*svm - 3.2E+1*sp[35]*
      pow(sp[43],2)*sp[44]*sp[37]*svp*ssm - 1.6E+1*pow(sp[35],2)*pow(sp[44],2)*sp[39]*
      ssp*svm + 1.6E+1*pow(sp[35],2)*pow(sp[44],2)*sp[37]*svp*ssm - 1.6E+1*pow(
      sp[43],4)*sp[39]*ssp*svm + 1.6E+1*pow(sp[43],4)*sp[37]*svp*ssm;
 
    kern[5][7] = (1.0/ denom)* kern[5][7]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[43],2)*sp[44] + 8.E+0*pow(sp[35],2)*pow(sp[44],2)
       + 8.E+0*pow(sp[43],4);

    kern[6][0] =   - 2.4E+1*sp[35]*sp[43]*sp[31]*sp[37]*svp*svm - 2.4E+1*sp[35]*sp[43]*
      sp[29]*sp[39]*svp*svm + 8.E+0*sp[35]*pow(sp[43],2)*sp[7]*svp*svm + 8.E+0*
      sp[35]*sp[31]*sp[29]*sp[44]*svp*svm - 8.E+0*pow(sp[35],2)*sp[44]*sp[7]*svp*svm + 
      2.4E+1*pow(sp[35],2)*sp[39]*sp[37]*svp*svm + 1.6E+1*pow(sp[43],2)*sp[31]*sp[29]
      *svp*svm;
 
    kern[6][0] = (1.0/ denom)* kern[6][0]; 
    kern[6][1] =   0;
 
    kern[6][1] = (1.0/ denom)* kern[6][1]; 
    kern[6][2] =   - 8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm + 8.E+0*sp[35]*
      pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 8.E+0*sp[35]*pow(sp[43],3)*sp[39]*ssp*svm
       + 8.E+0*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*
      sp[39]*ssp*svm - 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(
      sp[43],4)*sp[31]*ssp*svm - 8.E+0*pow(sp[43],4)*sp[29]*svp*ssm;
 
    kern[6][2] = (1.0/ denom)* kern[6][2]; 
    kern[6][3] =  1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[37]*svp*svm - 1.6E+1*sp[35]*
      pow(sp[43],2)*sp[29]*sp[39]*svp*svm - 1.6E+1*pow(sp[35],2)*sp[31]*sp[44]*sp[37]*
      svp*svm + 1.6E+1*pow(sp[35],2)*sp[29]*sp[44]*sp[39]*svp*svm;
 
    kern[6][3] = (1.0/ denom)* kern[6][3]; 
    kern[6][4] =   - 8.E+0*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm - 8.E+0*sp[35]*sp[43]*
      sp[29]*sp[44]*svp*ssm - 8.E+0*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm - 8.E+0*sp[35]*
      pow(sp[43],2)*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*sp[44]*sp[39]*ssp*svm + 
      8.E+0*pow(sp[35],2)*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(sp[43],3)*sp[31]*ssp*svm
       + 8.E+0*pow(sp[43],3)*sp[29]*svp*ssm;
 
    kern[6][4] = (1.0/ denom)* kern[6][4]; 
    kern[6][5] =   - 1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[29]*sp[44]*svp*svm - 
      8.E+0*sp[35]*pow(sp[43],3)*sp[31]*sp[37]*svp*svm - 8.E+0*sp[35]*pow(sp[43],3)*
      sp[29]*sp[39]*svp*svm + 8.E+0*pow(sp[35],2)*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm + 
      8.E+0*pow(sp[35],2)*sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 1.6E+1*pow(sp[43],4)*
      sp[31]*sp[29]*svp*svm;
 
    kern[6][5] = (1.0/ denom)* kern[6][5]; 
    kern[6][6] =  8.E+0*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*sp[35]*sp[43]*
      sp[29]*sp[44]*sp[39]*svp*svm - 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]*sp[7]*svp*svm
       - 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]*ssp*ssm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[39]
      *sp[37]*svp*svm - 1.6E+1*pow(sp[35],2)*sp[44]*sp[39]*sp[37]*svp*svm + 8.E+0*
      pow(sp[35],2)*pow(sp[44],2)*sp[7]*svp*svm + 8.E+0*pow(sp[35],2)*pow(sp[44],2)
      *ssp*ssm - 8.E+0*pow(sp[43],3)*sp[31]*sp[37]*svp*svm - 8.E+0*pow(sp[43],3)*
      sp[29]*sp[39]*svp*svm + 8.E+0*pow(sp[43],4)*sp[7]*svp*svm + 8.E+0*pow(
      sp[43],4)*ssp*ssm;
 
    kern[6][6] = (1.0/ denom)* kern[6][6]; 
    kern[6][7] =  3.2E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm - 3.2E+1*sp[35]*
      pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 1.6E+1*pow(sp[35],2)*sp[31]*pow(sp[44],2)*
      ssp*svm + 1.6E+1*pow(sp[35],2)*sp[29]*pow(sp[44],2)*svp*ssm - 1.6E+1*pow(
      sp[43],4)*sp[31]*ssp*svm + 1.6E+1*pow(sp[43],4)*sp[29]*svp*ssm;
 
    kern[6][7] = (1.0/ denom)* kern[6][7]; 

    denom =  - 6.4E+1*sp[35]*pow(sp[43],2)*sp[44] + 3.2E+1*pow(sp[35],2)*pow(sp[44],2)
       + 3.2E+1*pow(sp[43],4);

    kern[7][0] =   0;
 
    kern[7][0] = (1.0/ denom)* kern[7][0]; 
    kern[7][1] =  4.8E+1*sp[35]*sp[43]*sp[31]*sp[37]*svp*svm + 4.8E+1*sp[35]*sp[43]*
      sp[29]*sp[39]*svp*svm - 1.6E+1*sp[35]*pow(sp[43],2)*sp[7]*svp*svm - 1.6E+1*
      sp[35]*sp[31]*sp[29]*sp[44]*svp*svm + 1.6E+1*pow(sp[35],2)*sp[44]*sp[7]*svp*svm
       - 4.8E+1*pow(sp[35],2)*sp[39]*sp[37]*svp*svm - 3.2E+1*pow(sp[43],2)*sp[31]*
      sp[29]*svp*svm;
 
    kern[7][1] = (1.0/ denom)* kern[7][1]; 
    kern[7][2] =   - 1.6E+1*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*sp[35]*
      sp[43]*sp[29]*sp[44]*sp[39]*svp*svm - 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]*sp[7]*svp*
      svm + 4.8E+1*sp[35]*pow(sp[43],2)*sp[39]*sp[37]*svp*svm + 4.8E+1*pow(sp[43],2)
      *sp[31]*sp[29]*sp[44]*svp*svm - 3.2E+1*pow(sp[43],3)*sp[31]*sp[37]*svp*svm - 
      3.2E+1*pow(sp[43],3)*sp[29]*sp[39]*svp*svm + 1.6E+1*pow(sp[43],4)*sp[7]*svp
      *svm;
 
    kern[7][2] = (1.0/ denom)* kern[7][2]; 
    kern[7][3] =   - 3.2E+1*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm - 3.2E+1*sp[35]*sp[43]*
      sp[29]*sp[44]*svp*ssm - 3.2E+1*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm - 3.2E+1*
      sp[35]*pow(sp[43],2)*sp[37]*svp*ssm + 3.2E+1*pow(sp[35],2)*sp[44]*sp[39]*ssp*svm
       + 3.2E+1*pow(sp[35],2)*sp[44]*sp[37]*svp*ssm + 3.2E+1*pow(sp[43],3)*sp[31]*
      ssp*svm + 3.2E+1*pow(sp[43],3)*sp[29]*svp*ssm;
 
    kern[7][3] = (1.0/ denom)* kern[7][3]; 
    kern[7][4] =  1.6E+1*sp[35]*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*sp[35]*sp[29]*
      sp[44]*sp[39]*svp*svm - 1.6E+1*pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 1.6E+1*
      pow(sp[43],2)*sp[29]*sp[39]*svp*svm;
 
    kern[7][4] = (1.0/ denom)* kern[7][4]; 
    kern[7][5] =   - 1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm + 1.6E+1*
      sp[35]*pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 1.6E+1*sp[35]*pow(sp[43],3)*sp[39]*ssp*
      svm + 1.6E+1*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm + 1.6E+1*pow(sp[35],2)*sp[43]*
      sp[44]*sp[39]*ssp*svm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm + 
      1.6E+1*pow(sp[43],4)*sp[31]*ssp*svm - 1.6E+1*pow(sp[43],4)*sp[29]*svp*ssm;
 
    kern[7][5] = (1.0/ denom)* kern[7][5]; 
    kern[7][6] =   - 1.6E+1*sp[35]*sp[43]*sp[44]*sp[39]*ssp*svm + 1.6E+1*sp[35]*sp[43]*
      sp[44]*sp[37]*svp*ssm + 1.6E+1*sp[35]*sp[31]*pow(sp[44],2)*ssp*svm - 1.6E+1*
      sp[35]*sp[29]*pow(sp[44],2)*svp*ssm - 1.6E+1*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm
       + 1.6E+1*pow(sp[43],2)*sp[29]*sp[44]*svp*ssm + 1.6E+1*pow(sp[43],3)*sp[39]*
      ssp*svm - 1.6E+1*pow(sp[43],3)*sp[37]*svp*ssm;
 
    kern[7][6] = (1.0/ denom)* kern[7][6]; 
    kern[7][7] =   - 6.4E+1*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 6.4E+1*sp[35]*
      sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 6.4E+1*sp[35]*pow(sp[43],2)*sp[44]*sp[7]*svp*
      svm - 6.4E+1*sp[35]*pow(sp[43],2)*sp[44]*ssp*ssm - 6.4E+1*sp[35]*pow(sp[43],2)*
      sp[39]*sp[37]*svp*svm + 6.4E+1*sp[35]*sp[31]*sp[29]*pow(sp[44],2)*svp*svm + 
      6.4E+1*pow(sp[35],2)*sp[44]*sp[39]*sp[37]*svp*svm - 3.2E+1*pow(sp[35],2)*pow(
      sp[44],2)*sp[7]*svp*svm + 3.2E+1*pow(sp[35],2)*pow(sp[44],2)*ssp*ssm - 
      6.4E+1*pow(sp[43],2)*sp[31]*sp[29]*sp[44]*svp*svm + 6.4E+1*pow(sp[43],3)*sp[31]*
      sp[37]*svp*svm + 6.4E+1*pow(sp[43],3)*sp[29]*sp[39]*svp*svm - 3.2E+1*pow(
      sp[43],4)*sp[7]*svp*svm + 3.2E+1*pow(sp[43],4)*ssp*ssm;
 
    kern[7][7] = (1.0/ denom)* kern[7][7]; 


}


void kernelL_ve( array<array<Cdoub, K_N_PROJECTORS_DIRAC>,
                         K_N_BASE_ELEMENTS_DIRAC>& kern,
              const ArrayScalarProducts& sp,
              const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){


    // local variables
    array<Cdoub, 65> w;
    Cdoub denom;


    w[1]=sp[35];
    w[2]=sp[27];
    w[3]=sp[44];
    w[4]=sp[34];
    w[5]=sp[43];
   w[6]=w[3]*ang[0];
   w[7]=w[6] + ang[1];
   w[8]=pow(w[4],2);
   w[9]= - w[7]*w[8];
   w[10]=w[6] - ang[1];
   w[11]=w[2]*ang[0];
   w[12]=w[10] + w[11];
   w[13]=w[4]*w[12];
   w[14]=w[11]*w[5];
   w[13]=w[13] - w[14];
   w[13]=w[5]*w[13];
   w[9]=w[9] + w[13];
   w[13]=2.E+0*ang[1];
   w[15]=w[13] - w[6];
   w[16]=w[15]*w[3];
   w[16]=w[16] - ang[2];
   w[17]=3.E+0*ang[1];
   w[18]=w[17] + w[6];
   w[18]=2.E+0*w[18] - w[11];
   w[18]=w[2]*w[18];
   w[18]=w[18] + w[16];
   w[18]=w[1]*w[18];
   w[9]=4.E+0*w[9] + w[18];
   w[9]=2.E+0*w[9];
   w[18]=2.E+0*w[11];
   w[19]=w[18] + w[10];
   w[20]=w[19]*w[4];
   w[20]=w[20] - w[14];
   w[20]=w[20]*w[5];
   w[21]=2.E+0*w[6];
   w[22]=w[11] - ang[1];
   w[23]=w[21] + w[22];
   w[24]=w[23]*w[8];
   w[20]=w[20] - w[24];
   w[20]=w[20]*w[5];
   w[24]=pow(w[4],3);
   w[25]=w[24]*w[6];
   w[20]=w[20] + w[25];
   w[25]=4.E+0*w[5];
   w[26]=w[20]*w[25];
   w[27]=2.E+0*w[7];
   w[28]=w[27] - w[11];
   w[28]=w[28]*w[2];
   w[28]=w[28] + w[16];
   w[29]=w[5] - w[4];
   w[28]= - w[28]*w[29];
   w[30]=w[1]*w[5];
   w[31]= - w[28]*w[30];
   w[26]=w[26] + w[31];
   w[26]=2.E+0*w[26];
   w[31]=w[11] - w[6];
   w[32]=w[31] - w[13];
   w[33]=w[5]*w[2];
   w[34]=w[32]*w[33];
   w[17]=w[17] - w[11];
   w[17]=w[17]*w[2];
   w[35]=w[7]*w[3];
   w[35]= - w[35] + 2.E+0*ang[2];
   w[17]=w[17] - w[35];
   w[36]= - w[4]*w[17];
   w[36]=w[36] - w[34];
   w[36]=w[5]*w[36];
   w[37]=w[13] + w[6];
   w[38]=w[37]*w[3];
   w[39]=w[6]*w[2];
   w[38]=w[38] - w[39];
   w[40]=w[38]*w[8];
   w[36]=w[40] + w[36];
   w[40]=4.E+0*ang[1];
   w[41]=3.E+0*w[6];
   w[42]=w[40] + w[41];
   w[43]= - w[3]*w[42];
   w[42]= - w[11] + w[42];
   w[42]=w[2]*w[42];
   w[44]=5.E+0*ang[2];
   w[42]=w[42] - w[44] + w[43];
   w[42]=w[2]*w[42];
   w[43]=3.E+0*ang[2];
   w[45]=pow(w[3],2);
   w[46]=w[45]*ang[0];
   w[47]=w[43] - w[46];
   w[47]=w[47]*w[3];
   w[47]=w[47] - 2.E+0*ang[3];
   w[42]=w[42] - w[47];
   w[42]=w[1]*w[42];
   w[36]=4.E+0*w[36] + w[42];
   w[42]=w[8]*w[11];
   w[48]= - w[4]*w[18];
   w[48]=w[48] + w[14];
   w[48]=w[5]*w[48];
   w[42]=w[42] + w[48];
   w[48]=2.E+0*w[10];
   w[49]=w[48] - w[11];
   w[49]=w[2]*w[49];
   w[49]=w[49] + w[16];
   w[49]=w[1]*w[49];
   w[42]=4.E+0*w[42] + w[49];
   w[49]=4.E+0*w[1];
   w[42]=w[42]*w[49];
   w[50]=w[10]*w[3];
   w[51]=w[22]*w[2];
   w[50]=w[50] - w[51];
   w[50]=w[50]*w[4];
   w[51]=2.E+0*w[33];
   w[52]=w[31]*w[51];
   w[52]=w[50] + w[52];
   w[52]=8.E+0*w[52]*w[30];
   w[31]= - w[31]*w[33];
   w[31]= - w[50] + w[31];
   w[31]=w[5]*w[31];
   w[39]=w[46] - w[39];
   w[39]=w[39]*w[8];
   w[31]=w[39] + w[31];
   w[39]=3.E+0*ang[0];
   w[39]= - w[45]*w[39];
   w[45]=w[13] - w[11];
   w[50]=w[41] + w[45];
   w[50]=w[2]*w[50];
   w[39]=w[50] - ang[2] + w[39];
   w[39]=w[2]*w[39];
   w[50]= - w[3]*w[16];
   w[39]=w[50] + w[39];
   w[39]=w[1]*w[39];
   w[31]=4.E+0*w[31] + w[39];
   w[31]=w[31]*w[49];
   w[39]=2.E+0*ang[0];
   w[49]=w[8]*w[39];
   w[50]=w[5]*ang[0];
   w[53]= - w[4]*w[50];
   w[49]=w[49] + w[53];
   w[49]=w[5]*w[49];
   w[53]= - ang[0]*w[24];
   w[49]=w[53] + w[49];
   w[53]=w[10] - w[11];
   w[29]=w[1]*w[53]*w[29];
   w[29]=2.E+0*w[49] + w[29];
   w[29]=8.E+0*w[29];
   w[49]=w[22]*w[8];
   w[54]=ang[1] - w[18];
   w[54]=w[4]*w[54];
   w[54]=w[54] + w[14];
   w[54]=w[5]*w[54];
   w[49]=w[49] + w[54];
   w[49]=w[49]*w[25];
   w[54]=w[21] - w[11];
   w[54]=w[2]*w[54];
   w[54]=w[54] + ang[2] - w[46];
   w[54]=w[54]*w[30];
   w[49]=w[49] + w[54];
   w[49]=2.E+0*w[49];
   w[28]= - w[1]*w[28];
   w[20]=4.E+0*w[20] + w[28];
   w[20]=4.E+0*w[20];
   w[28]=w[8]*w[6];
   w[15]=w[15] - w[11];
   w[54]=w[4]*w[15];
   w[54]=w[54] + w[14];
   w[54]=w[5]*w[54];
   w[28]=w[28] + w[54];
   w[54]=w[40] - w[6];
   w[55]=w[54]*w[3];
   w[37]= - 2.E+0*w[37] + w[11];
   w[37]=w[2]*w[37];
   w[37]=w[37] + w[43] - w[55];
   w[37]=w[1]*w[37];
   w[28]=4.E+0*w[28] + w[37];
   w[28]=8.E+0*w[28];
   w[37]=w[2] + w[3];
   w[37]=w[37]*ang[1];
   w[37]=w[37] - ang[2];
   w[37]=w[37]*w[1];
   w[43]= - w[5]*w[4]*w[13];
   w[43]=w[43] + w[37];
   w[43]=6.E+0*w[43];
   w[56]=2.E+0*w[8];
   w[19]=w[56]*w[19];
   w[57]=3.E+0*w[4];
   w[58]=w[57]*w[14];
   w[19]=w[19] - w[58];
   w[19]=w[19]*w[5];
   w[23]=w[23]*w[24];
   w[19]=w[19] - w[23];
   w[59]=w[27] + w[11];
   w[59]=w[59]*w[2];
   w[16]=3.E+0*w[16];
   w[59]=w[59] + w[16];
   w[60]=w[59]*w[4];
   w[33]=4.E+0*w[33];
   w[53]=w[53]*w[33];
   w[53]=w[53] + w[60];
   w[61]= - w[1]*w[53];
   w[61]= - 4.E+0*w[19] + w[61];
   w[61]=2.E+0*w[61];
   w[54]=2.E+0*w[54];
   w[62]=3.E+0*w[11];
   w[63]=w[54] - w[62];
   w[63]=w[2]*w[63];
   w[64]=8.E+0*ang[1] - w[41];
   w[64]=w[3]*w[64];
   w[44]=w[63] - w[44] + w[64];
   w[44]=w[44]*w[8];
   w[63]=6.E+0*w[10] + 7.E+0*w[11];
   w[63]=w[2]*w[63];
   w[63]= - w[16] + w[63];
   w[63]=w[4]*w[63];
   w[12]= - w[12]*w[33];
   w[12]=w[63] + w[12];
   w[12]=w[5]*w[12];
   w[12]=w[44] + w[12];
   w[12]=w[5]*w[12];
   w[37]= - w[51]*w[37];
   w[12]=w[12] + w[37];
   w[12]=w[1]*w[12];
   w[37]=5.E+0*w[11];
   w[44]= - w[48] - w[37];
   w[44]=w[44]*w[8];
   w[44]=w[44] + w[58];
   w[44]=w[5]*w[44];
   w[10]=3.E+0*w[10];
   w[48]=w[10] + w[18];
   w[63]=w[48]*w[24];
   w[44]=w[63] + w[44];
   w[44]=w[5]*w[44];
   w[63]=pow(w[4],4);
   w[64]=w[63]*w[6];
   w[44]= - w[64] + w[44];
   w[44]=w[44]*w[25];
   w[12]=w[44] + w[12];
   w[12]=2.E+0*w[12];
   w[44]=w[17]*w[56];
   w[56]=w[57]*w[34];
   w[44]=w[44] + w[56];
   w[44]=w[5]*w[44];
   w[38]= - w[38]*w[24];
   w[38]=w[38] + w[44];
   w[44]=w[62] - 1.2E+1*ang[1] - w[6];
   w[44]=w[2]*w[44];
   w[44]=w[44] + 1.5E+1*ang[2] - w[55];
   w[44]=w[2]*w[44];
   w[47]=3.E+0*w[47];
   w[44]=w[47] + w[44];
   w[44]=w[4]*w[44];
   w[17]=w[17]*w[33];
   w[17]=w[44] + w[17];
   w[17]=w[1]*w[17];
   w[17]=4.E+0*w[38] + w[17];
   w[33]=w[41] - ang[1];
   w[33]= - w[62] + 2.E+0*w[33];
   w[33]=w[33]*w[2];
   w[33]=w[33] + w[16];
   w[33]=w[33]*w[1];
   w[10]=w[10] - w[11];
   w[10]=w[10]*w[4];
   w[10]=w[10] - w[14];
   w[10]=w[10]*w[5];
   w[13]=w[41] - w[13];
   w[38]=w[13] - w[18];
   w[38]=w[38]*w[8];
   w[10]=w[10] - w[38];
   w[10]=w[33] + 4.E+0*w[10];
   w[10]=w[10]*w[1];
   w[33]=w[50]*w[8];
   w[38]=w[24]*w[39];
   w[33]=w[33] - w[38];
   w[33]=w[33]*w[5];
   w[38]=w[63]*ang[0];
   w[33]=w[33] + w[38];
   w[33]=8.E+0*w[33];
   w[10]=w[10] - w[33];
   w[38]= - 2.E+0*w[10];
   w[39]= - w[5]*w[59];
   w[39]=w[60] + w[39];
   w[39]=w[39]*w[30];
   w[44]= - w[40] + 5.E+0*w[6];
   w[18]=w[18] + w[44];
   w[18]=w[18]*w[8];
   w[48]= - w[4]*w[48];
   w[14]=w[48] + w[14];
   w[14]=w[5]*w[14];
   w[14]=w[18] + w[14];
   w[14]=w[5]*w[14];
   w[14]= - w[23] + w[14];
   w[14]=w[14]*w[25];
   w[14]=w[14] + w[39];
   w[14]=w[1]*w[14];
   w[18]=pow(w[5],2)*w[33];
   w[14]=w[18] + w[14];
   w[14]=2.E+0*w[14];
   w[18]= - w[11] + 7.E+0*ang[1] - w[21];
   w[18]=w[2]*w[18];
   w[18]= - 3.E+0*w[35] + w[18];
   w[18]=w[4]*w[18];
   w[18]=w[18] + w[34];
   w[18]=w[5]*w[18];
   w[15]=w[15]*w[2];
   w[15]=w[15] - ang[2];
   w[21]= - w[3]*w[27];
   w[21]=w[21] - w[15];
   w[21]=w[21]*w[8];
   w[18]=w[21] + w[18];
   w[21]= - w[11] - w[40] - w[6];
   w[21]=w[2]*w[21];
   w[23]=w[3]*w[44];
   w[21]=w[21] + 1.1E+1*ang[2] + w[23];
   w[21]=w[2]*w[21];
   w[21]=w[47] + w[21];
   w[21]=w[1]*w[21];
   w[18]=4.E+0*w[18] + w[21];
   w[18]=w[1]*w[18];
   w[7]= - w[11] + w[7];
   w[7]=w[7]*w[24];
   w[21]=w[5]*w[32]*w[8];
   w[7]=w[7] + w[21];
   w[7]=w[5]*w[7];
   w[7]=8.E+0*w[7] + w[18];
   w[10]=4.E+0*w[10];
   w[18]= - w[19]*w[25];
   w[19]= - w[53]*w[30];
   w[18]=w[18] + w[19];
   w[18]=4.E+0*w[18];
   w[19]=w[40] - w[41];
   w[19]=w[3]*w[19];
   w[19]= - ang[2] + w[19];
   w[21]=w[54] - w[37];
   w[21]=w[2]*w[21];
   w[19]=3.E+0*w[19] + w[21];
   w[19]=w[4]*w[19];
   w[21]=w[41] - w[45];
   w[21]=w[21]*w[51];
   w[19]=w[19] + w[21];
   w[19]=w[5]*w[19];
   w[21]= - w[2]*w[45];
   w[23]= - 6.E+0*ang[1] + 7.E+0*w[6];
   w[23]=w[3]*w[23];
   w[21]=w[21] + ang[2] + w[23];
   w[21]=w[21]*w[8];
   w[19]=w[21] + w[19];
   w[16]= - w[3]*w[16];
   w[15]= - 5.E+0*w[46] - w[15];
   w[15]=w[2]*w[15];
   w[15]=w[16] + w[15];
   w[15]=w[1]*w[15];
   w[15]=2.E+0*w[19] + w[15];
   w[15]=w[1]*w[15];
   w[11]=4.E+0*w[11] + w[13];
   w[11]=w[11]*w[8];
   w[11]=w[11] - w[58];
   w[11]=w[5]*w[11];
   w[6]= - 4.E+0*w[6] - w[22];
   w[6]=w[6]*w[24];
   w[6]=w[6] + w[11];
   w[6]=w[5]*w[6];
   w[6]=w[64] + w[6];
   w[6]=8.E+0*w[6] + w[15];
   w[6]=4.E+0*w[6];
   w[11]=w[1]*w[2];
   w[13]=w[11] - w[8];
   w[15]= - 8.E+0*w[13];
   w[16]=1.6E+1*w[1]*w[13];
   w[11]= - w[4]*w[11];
   w[11]=w[24] + w[11];
   w[11]=8.E+0*w[11];
   w[19]= - 3.2E+1*w[13];
   w[13]=4.E+0*w[13];
   w[21]=2.E+0*w[2];
   w[22]= - w[24]*w[21];
   w[23]=w[1]*pow(w[2],2);
   w[24]=w[4]*w[23];
   w[22]=w[22] + w[24];
   w[22]=w[1]*w[22];
   w[24]=pow(w[4],5);
   w[22]=w[24] + w[22];
   w[22]=8.E+0*w[22];
   w[8]=w[21]*w[8];
   w[8]=w[8] - w[23];
   w[8]=w[8]*w[1];
   w[8]=w[8] - w[63];
   w[21]= - 8.E+0*w[8];
   w[8]= - 3.2E+1*w[8];


    denom = w[15];

    kern[0][0] = (1.0/ denom)* w[9];
 
    kern[0][1] = (1.0/ denom)*0.0;
 
    kern[0][2] = (1.0/ denom)*0.0;
 
    kern[0][3] = (1.0/ denom)*0.0;
 
    kern[0][4] = (1.0/ denom)*0.0;
 
    kern[0][5] = (1.0/ denom)* w[26];
 
    kern[0][6] = (1.0/ denom)* w[36];
 
    kern[0][7] = (1.0/ denom)*0.0;
 

    denom = w[16];

    kern[1][0] = (1.0/ denom)*0.0;
 
    kern[1][1] = (1.0/ denom)* w[42];
 
    kern[1][2] = (1.0/ denom)* w[52];
 
    kern[1][3] = (1.0/ denom)*0.0;
 
    kern[1][4] = (1.0/ denom)*0.0;
 
    kern[1][5] = (1.0/ denom)*0.0;
 
    kern[1][6] = (1.0/ denom)*0.0;
 
    kern[1][7] = (1.0/ denom)* w[31];
 

    denom = w[11];

    kern[2][0] = (1.0/ denom)*0.0;
 
    kern[2][1] = (1.0/ denom)* w[29];
 
    kern[2][2] = (1.0/ denom)* w[49];
 
    kern[2][3] = (1.0/ denom)*0.0;
 
    kern[2][4] = (1.0/ denom)*0.0;
 
    kern[2][5] = (1.0/ denom)*0.0;
 
    kern[2][6] = (1.0/ denom)*0.0;
 
    kern[2][7] = (1.0/ denom)* w[20];
 

    denom = w[19];

    kern[3][0] = (1.0/ denom)*0.0;
 
    kern[3][1] = (1.0/ denom)*0.0;
 
    kern[3][2] = (1.0/ denom)*0.0;
 
    kern[3][3] = (1.0/ denom)* w[28];
 
    kern[3][4] = (1.0/ denom)*0.0;
 
    kern[3][5] = (1.0/ denom)*0.0;
 
    kern[3][6] = (1.0/ denom)*0.0;
 
    kern[3][7] = (1.0/ denom)*0.0;
 

    denom = w[13];

    kern[4][0] = (1.0/ denom)*0.0;
 
    kern[4][1] = (1.0/ denom)*0.0;
 
    kern[4][2] = (1.0/ denom)*0.0;
 
    kern[4][3] = (1.0/ denom)*0.0;
 
    kern[4][4] = (1.0/ denom)* w[43];
 
    kern[4][5] = (1.0/ denom)*0.0;
 
    kern[4][6] = (1.0/ denom)*0.0;
 
    kern[4][7] = (1.0/ denom)*0.0;
 

    denom = w[22];

    kern[5][0] = (1.0/ denom)* w[61];
 
    kern[5][1] = (1.0/ denom)*0.0;
 
    kern[5][2] = (1.0/ denom)*0.0;
 
    kern[5][3] = (1.0/ denom)*0.0;
 
    kern[5][4] = (1.0/ denom)*0.0;
 
    kern[5][5] = (1.0/ denom)* w[12];
 
    kern[5][6] = (1.0/ denom)* w[17];
 
    kern[5][7] = (1.0/ denom)*0.0;
 

    denom = w[21];

    kern[6][0] = (1.0/ denom)* w[38];
 
    kern[6][1] = (1.0/ denom)*0.0;
 
    kern[6][2] = (1.0/ denom)*0.0;
 
    kern[6][3] = (1.0/ denom)*0.0;
 
    kern[6][4] = (1.0/ denom)*0.0;
 
    kern[6][5] = (1.0/ denom)* w[14];
 
    kern[6][6] = (1.0/ denom)* w[7];
 
    kern[6][7] = (1.0/ denom)*0.0;
 

    denom = w[8];

    kern[7][0] = (1.0/ denom)*0.0;
 
    kern[7][1] = (1.0/ denom)* w[10];
 
    kern[7][2] = (1.0/ denom)* w[18];
 
    kern[7][3] = (1.0/ denom)*0.0;
 
    kern[7][4] = (1.0/ denom)*0.0;
 
    kern[7][5] = (1.0/ denom)*0.0;
 
    kern[7][6] = (1.0/ denom)*0.0;
 
    kern[7][7] = (1.0/ denom)* w[6];
 

}


void kernelY_ve(array<array<Cdoub, K_N_PROJECTORS_DIRAC>,
                            K_N_BASE_ELEMENTS_DIRAC>& kern,
  const ArrayScalarProducts& sp,  const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


    // local variables
    array<Cdoub, 73> w;
    Cdoub denom;


    w[1]=sp[35];
    w[2]=sp[44];
    w[3]=sp[39];
    w[4]=sp[37];
    w[5]=sp[43];
    w[6]=sp[31];
    w[7]=sp[29];
    w[8]=sp[7];
   w[9]=ssm*ssp;
   w[10]=w[9]*w[1];
   w[11]=svp*svm;
   w[12]=w[11]*w[7];
   w[13]=w[12]*w[6];
   w[14]=w[10] + w[13];
   w[15]= - w[2]*w[14];
   w[12]=w[12]*w[3];
   w[16]=w[11]*w[4];
   w[17]=w[6]*w[16];
   w[18]=w[12] + w[17];
   w[19]=w[9]*w[5];
   w[20]=w[19] + w[18];
   w[20]=w[5]*w[20];
   w[16]=w[3]*w[16];
   w[21]=w[16]*w[1];
   w[15]=w[20] - w[21] + w[15];
   w[15]=8.E+0*w[15];
   w[20]=ssm*svp;
   w[22]=w[7]*w[20];
   w[23]=ssp*svm;
   w[24]=w[6]*w[23];
   w[25]=w[22] - w[24];
   w[26]=pow(w[5],2);
   w[27]=w[25]*w[26];
   w[28]=w[2]*w[1];
   w[29]=w[25]*w[28];
   w[27]= - w[29] + w[27];
   w[27]=8.E+0*w[27];
   w[30]=w[26] - w[28];
   w[31]=8.E+0*w[5];
   w[32]=w[30]*w[31];
   w[20]=w[4]*w[20];
   w[23]=w[3]*w[23];
   w[33]=w[20] - w[23];
   w[34]= - w[33]*w[32];
   w[12]=w[12] - w[17];
   w[17]=w[12]*w[28];
   w[35]=w[12]*w[26];
   w[17]=w[17] - w[35];
   w[36]=1.6E+1*w[17];
   w[37]=w[33]*w[1];
   w[38]=w[5]*w[37];
   w[38]= - w[29] + w[38];
   w[38]=1.6E+1*w[38];
   w[39]=pow(w[1],2);
   w[40]=w[16]*w[39];
   w[14]=w[14]*w[28];
   w[41]= - w[26]*w[10];
   w[14]=w[41] - w[40] + w[14];
   w[14]=1.6E+1*w[14];
   w[41]=w[18]*w[28];
   w[42]=2.E+0*w[5];
   w[43]=w[21]*w[42];
   w[43]= - w[41] + w[43];
   w[44]=1.6E+1*w[5];
   w[43]=w[43]*w[44];
   w[20]=w[20] + w[23];
   w[23]=w[39]*w[2];
   w[39]=w[23]*w[20];
   w[45]=w[20]*w[1];
   w[46]= - w[26]*w[45];
   w[46]=w[39] + w[46];
   w[46]=3.2E+1*w[46];
   w[47]=w[25]*w[5];
   w[37]=w[47] - w[37];
   w[47]=8.E+0*w[37];
   w[48]=w[18]*w[1];
   w[49]=2.E+0*w[13];
   w[50]=w[49]*w[5];
   w[50]=w[50] - w[48];
   w[51]= - 8.E+0*w[50];
   w[10]=w[10] - w[13];
   w[10]=w[10]*w[2];
   w[52]=w[26]*w[9];
   w[52]=w[52] - w[21] - w[10];
   w[52]=w[52]*w[31];
   w[22]=w[22] + w[24];
   w[24]=w[22]*w[26];
   w[53]=w[22]*w[28];
   w[24]=w[24] - w[53];
   w[54]=1.6E+1*w[24];
   w[55]= - 1.6E+1*w[12];
   w[56]=w[22]*w[5];
   w[45]=w[56] - w[45];
   w[56]= - 1.6E+1*w[45];
   w[57]=w[20]*w[5];
   w[58]=w[22]*w[2];
   w[57]=w[57] - w[58];
   w[58]= - w[57]*w[44];
   w[19]=w[19] - w[18];
   w[19]=w[5]*w[19];
   w[10]=w[19] + w[21] - w[10];
   w[10]=3.2E+1*w[10];
   w[19]= - 4.E+0*w[45];
   w[59]=4.E+0*w[1]*w[12];
   w[60]= - 4.E+0*w[35];
   w[61]=4.E+0*w[30];
   w[11]=w[11]*w[8];
   w[62]=w[9] - w[11];
   w[63]= - w[62]*w[61];
   w[24]= - 4.E+0*w[5]*w[24];
   w[64]=w[20]*w[61];
   w[65]= - 8.E+0*w[17];
   w[66]=w[11]*w[1];
   w[67]= - w[66] + 3.E+0*w[13];
   w[67]=w[67]*w[2];
   w[68]=2.E+0*w[18];
   w[69]=w[11]*w[5];
   w[69]=w[69] - w[68];
   w[69]=w[69]*w[5];
   w[67]=w[67] + w[69] + 3.E+0*w[21];
   w[67]=w[67]*w[5];
   w[67]=w[67] - w[41];
   w[69]=8.E+0*w[67];
   w[37]=w[37]*w[5];
   w[29]=w[37] - w[29];
   w[29]=w[29]*w[5];
   w[37]=w[23]*w[33];
   w[29]=w[29] + w[37];
   w[37]= - 8.E+0*w[29];
   w[17]=w[17]*w[44];
   w[57]= - w[5]*w[57];
   w[20]=w[20]*w[28];
   w[20]=w[20] + w[57];
   w[20]=w[5]*w[20];
   w[57]=pow(w[2],2);
   w[70]=w[57]*w[1];
   w[22]= - w[22]*w[70];
   w[20]=w[22] + w[20];
   w[20]=8.E+0*w[20];
   w[9]=w[9] + w[11];
   w[11]=w[9]*w[5];
   w[11]=w[11] - w[18];
   w[11]=w[11]*w[5];
   w[22]=w[9]*w[1];
   w[71]=w[13] - w[22];
   w[71]=w[2]*w[71];
   w[71]=2.E+0*w[71] + w[11];
   w[71]=w[5]*w[71];
   w[71]=w[41] + w[71];
   w[71]=w[5]*w[71];
   w[22]= - w[49] + w[22];
   w[22]=w[22]*w[70];
   w[22]=w[22] + w[71];
   w[22]=w[22]*w[31];
   w[71]= - w[2]*w[18];
   w[42]=w[16]*w[42];
   w[42]=w[71] + w[42];
   w[42]=w[5]*w[42];
   w[71]=2.E+0*w[28];
   w[16]= - w[16]*w[71];
   w[16]=w[16] + w[42];
   w[16]=w[5]*w[16];
   w[42]=w[57]*w[48];
   w[16]=w[42] + w[16];
   w[16]=8.E+0*w[16];
   w[42]=w[71] - w[26];
   w[42]=w[42]*w[26];
   w[57]=pow(w[28],2);
   w[42]=w[42] - w[57];
   w[57]= - 1.6E+1*w[33]*w[42];
   w[72]=w[66] + w[49];
   w[72]=w[72]*w[5];
   w[48]=w[72] - 3.E+0*w[48];
   w[48]=w[48]*w[5];
   w[66]=w[66] - w[13];
   w[66]=w[66]*w[28];
   w[48]=w[48] - w[66] + 3.E+0*w[40];
   w[66]=8.E+0*w[48];
   w[72]= - w[29]*w[31];
   w[12]=w[12]*w[23];
   w[35]= - w[1]*w[35];
   w[12]=w[12] + w[35];
   w[12]=1.6E+1*w[12];
   w[35]=w[45]*w[5];
   w[35]=w[35] - w[53];
   w[35]=w[35]*w[5];
   w[35]=w[35] + w[39];
   w[39]=8.E+0*w[35];
   w[45]= - w[13]*w[71];
   w[50]=w[5]*w[50];
   w[45]=w[45] + w[50];
   w[45]=w[5]*w[45];
   w[50]=w[18]*w[23];
   w[45]=w[50] + w[45];
   w[45]=w[45]*w[31];
   w[40]=2.E+0*w[40];
   w[50]=w[9]*w[23];
   w[50]= - w[40] + w[50];
   w[50]=w[2]*w[50];
   w[9]= - w[9]*w[28];
   w[9]=w[21] + w[9];
   w[9]=2.E+0*w[9] + w[11];
   w[9]=w[5]*w[9];
   w[9]=w[41] + w[9];
   w[9]=w[5]*w[9];
   w[9]=w[50] + w[9];
   w[9]=8.E+0*w[9];
   w[11]= - 1.6E+1*w[25]*w[42];
   w[41]= - 1.6E+1*w[48];
   w[48]=w[67]*w[44];
   w[35]=3.2E+1*w[35];
   w[29]= - w[29]*w[44];
   w[44]=w[2]*w[25];
   w[50]= - w[5]*w[33];
   w[44]=w[44] + w[50];
   w[44]=w[5]*w[44];
   w[33]=w[33]*w[28];
   w[33]=w[33] + w[44];
   w[33]=w[5]*w[33];
   w[25]= - w[25]*w[70];
   w[25]=w[25] + w[33];
   w[25]=1.6E+1*w[25];
   w[33]=w[62]*w[1];
   w[44]=w[49] + w[33];
   w[28]=w[44]*w[28];
   w[28]=w[40] + w[28];
   w[28]=w[2]*w[28];
   w[40]=w[5]*w[62];
   w[40]=w[68] + w[40];
   w[40]=w[5]*w[40];
   w[13]= - w[13] - w[33];
   w[13]=w[2]*w[13];
   w[13]= - w[21] + w[13];
   w[13]=2.E+0*w[13] + w[40];
   w[13]=w[5]*w[13];
   w[18]= - w[18]*w[71];
   w[13]=w[18] + w[13];
   w[13]=w[5]*w[13];
   w[13]=w[28] + w[13];
   w[13]=3.2E+1*w[13];
   w[18]=8.E+0*w[30];
   w[21]= - w[1]*w[26];
   w[21]=w[23] + w[21];
   w[21]=1.6E+1*w[21];
   w[23]=3.2E+1*w[30];
   w[26]= - w[42]*w[31];
   w[28]= - 8.E+0*w[42];
   w[30]= - 3.2E+1*w[42];


    denom = w[18];

    kern[0][0] = (1.0/ denom)* w[15];
 
    kern[0][1] = (1.0/ denom)* w[27];
 
    kern[0][2] = (1.0/ denom)* w[34];
 
    kern[0][3] = (1.0/ denom)*  - w[36];
 
    kern[0][4] = (1.0/ denom)*0.0;
 
    kern[0][5] = (1.0/ denom)*0.0;
 
    kern[0][6] = (1.0/ denom)*0.0;
 
    kern[0][7] = (1.0/ denom)*0.0;
 

    denom = w[21];

    kern[1][0] = (1.0/ denom)* w[38];
 
    kern[1][1] = (1.0/ denom)* w[14];
 
    kern[1][2] = (1.0/ denom)* w[43];
 
    kern[1][3] = (1.0/ denom)* w[46];
 
    kern[1][4] = (1.0/ denom)*0.0;
 
    kern[1][5] = (1.0/ denom)*0.0;
 
    kern[1][6] = (1.0/ denom)*0.0;
 
    kern[1][7] = (1.0/ denom)*0.0;
 

    denom = w[32];

    kern[2][0] = (1.0/ denom)* w[47];
 
    kern[2][1] = (1.0/ denom)* w[51];
 
    kern[2][2] = (1.0/ denom)* w[52];
 
    kern[2][3] = (1.0/ denom)* w[54];
 
    kern[2][4] = (1.0/ denom)*0.0;
 
    kern[2][5] = (1.0/ denom)*0.0;
 
    kern[2][6] = (1.0/ denom)*0.0;
 
    kern[2][7] = (1.0/ denom)*0.0;
 

    denom = w[23];

    kern[3][0] = (1.0/ denom)* w[55];
 
    kern[3][1] = (1.0/ denom)* w[56];
 
    kern[3][2] = (1.0/ denom)* w[58];
 
    kern[3][3] = (1.0/ denom)* w[10];
 
    kern[3][4] = (1.0/ denom)*0.0;
 
    kern[3][5] = (1.0/ denom)*0.0;
 
    kern[3][6] = (1.0/ denom)*0.0;
 
    kern[3][7] = (1.0/ denom)*0.0;
 

    denom =  - w[61];

    kern[4][0] = (1.0/ denom)* w[19];
 
    kern[4][1] = (1.0/ denom)* w[59];
 
    kern[4][2] = (1.0/ denom)* w[60];
 
    kern[4][3] = (1.0/ denom)*0.0;
 
    kern[4][4] = (1.0/ denom)* w[63];
 
    kern[4][5] = (1.0/ denom)* w[24];
 
    kern[4][6] = (1.0/ denom)* w[64];
 
    kern[4][7] = (1.0/ denom)* w[65];
 

    denom = w[26];

    kern[5][0] = (1.0/ denom)* w[69];
 
    kern[5][1] = (1.0/ denom)* w[37];
 
    kern[5][2] = (1.0/ denom)*0.0;
 
    kern[5][3] = (1.0/ denom)* w[17];
 
    kern[5][4] = (1.0/ denom)* w[20];
 
    kern[5][5] = (1.0/ denom)* w[22];
 
    kern[5][6] = (1.0/ denom)* w[16];
 
    kern[5][7] = (1.0/ denom)* w[57];
 

    denom = w[28];

    kern[6][0] = (1.0/ denom)* w[66];
 
    kern[6][1] = (1.0/ denom)*0.0;
 
    kern[6][2] = (1.0/ denom)* w[72];
 
    kern[6][3] = (1.0/ denom)* w[12];
 
    kern[6][4] = (1.0/ denom)* w[39];
 
    kern[6][5] = (1.0/ denom)* w[45];
 
    kern[6][6] = (1.0/ denom)* w[9];
 
    kern[6][7] = (1.0/ denom)* w[11];
 

    denom = w[30];

    kern[7][0] = (1.0/ denom)*0.0;
 
    kern[7][1] = (1.0/ denom)* w[41];
 
    kern[7][2] = (1.0/ denom)* w[48];
 
    kern[7][3] = (1.0/ denom)* w[35];
 
    kern[7][4] = (1.0/ denom)*  - w[36];
 
    kern[7][5] = (1.0/ denom)* w[29];
 
    kern[7][6] = (1.0/ denom)* w[25];
 
    kern[7][7] = (1.0/ denom)* w[13];
 


}

