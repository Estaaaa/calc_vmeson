#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
 
Cdoub calc_fpi_ve_array(
  Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
  Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
  VecCdoub Gamma, const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){
 
 
 Cdoub dummy, fpi=0.0;
 
    dummy =  1.2E+1*ssp*ssm + 8.E+0*pow(P_P,-1)*P_qm*P_qp*svp*svm + 
      4.E+0*qm_qp*svp*svm;
 
    fpi += Gamma[0] * dummy; 
 
    dummy =   - 1.2E+1*P_qm*ssp*svm + 1.2E+1*P_qp*svp*ssm;
 
    fpi += Gamma[1] * dummy; 
 
    dummy =  4.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*ssp*svm - 4.E+0*pow(
      P_P,-1)*pow(P_l,2)*P_qp*svp*ssm + 8.E+0*P_l*l_qm*ssp*svm - 8.E+0*
      P_l*l_qp*svp*ssm;
 
    fpi += Gamma[2] * dummy; 
 
    dummy =   - 1.6E+1*P_qm*l_qp*svp*svm + 1.6E+1*P_qp*l_qm*svp*svm;
 
    fpi += Gamma[3] * dummy; 
 
    dummy =  4.E+0*pow(P_P,-1)*P_l*P_qm*ssp*svm + 4.E+0*pow(P_P,-1)*P_l
      *P_qp*svp*ssm - 4.E+0*l_qm*ssp*svm - 4.E+0*l_qp*svp*ssm;
 
    fpi += Gamma[4] * dummy; 
 
    dummy =  8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*P_qp*svp*svm - 4.E+0*P_l
      *P_qm*l_qp*svp*svm - 4.E+0*P_l*P_qp*l_qm*svp*svm;
 
    fpi += Gamma[5] * dummy; 
 
    dummy =   - 4.E+0*pow(P_P,-1)*P_l*P_qm*l_qp*svp*svm - 4.E+0*pow(
      P_P,-1)*P_l*P_qp*l_qm*svp*svm + 4.E+0*pow(P_P,-1)*pow(P_l,2)*
      qm_qp*svp*svm + 4.E+0*pow(P_P,-1)*pow(P_l,2)*ssp*ssm - 4.E+0*l_l*
      qm_qp*svp*svm - 4.E+0*l_l*ssp*ssm + 8.E+0*l_qm*l_qp*svp*svm;
 
    fpi += Gamma[6] * dummy; 
 
    dummy =   - 8.E+0*pow(P_P,-1)*pow(P_l,2)*P_qm*ssp*svm + 8.E+0*pow(
      P_P,-1)*pow(P_l,2)*P_qp*svp*ssm + 8.E+0*P_qm*l_l*ssp*svm - 8.E+0*
      P_qp*l_l*svp*ssm;
 
    fpi += Gamma[7] * dummy; 
 

    return fpi;

}

