#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
//#include <math_ops.h>

void kernelL_notopti_qphv(matCdoub& kern,
                              const ArrayScalarProducts& sp,
                const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){


    // local variables
    Cdoub denom=0.0;


    denom =  - 4.E+0*sp[35]*sp[27] + 4.E+0*pow(sp[34],2);

    kern[0][0] =  4.E+0*sp[35]*sp[27]*ang[1] - 1.2E+1*sp[34]*sp[43]*sp[27]*ang[0] - 4.E+0*
      sp[34]*sp[43]*sp[44]*ang[0] + 4.E+0*sp[34]*sp[43]*ang[1] + 4.E+0*pow(sp[34],2)*sp[27]*ang[0]
       + 4.E+0*pow(sp[34],2)*sp[44]*ang[0] - 8.E+0*pow(sp[34],2)*ang[1] + 8.E+0*pow(
      sp[43],2)*sp[27]*ang[0];
 
    kern[0][0] = (1.0/ denom)* kern[0][0]; 
    kern[0][1] =   - 6.E+0*sp[34]*sp[43]*sp[27]*ang[1] + 2.E+0*sp[34]*sp[43]*pow(sp[27],2)*
      ang[0] - 2.E+0*sp[34]*sp[43]*sp[44]*ang[1] - 2.E+0*sp[34]*sp[43]*pow(sp[44],2)*ang[0] + 
      4.E+0*sp[34]*sp[43]*ang[2] + 4.E+0*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 8.E+0*pow(
      sp[43],2)*sp[27]*ang[1] - 4.E+0*pow(sp[43],2)*pow(sp[27],2)*ang[0];
 
    kern[0][1] = (1.0/ denom)* kern[0][1]; 
    kern[0][2] =   0;
 
    kern[0][2] = (1.0/ denom)* kern[0][2]; 
    kern[0][3] =   0;
 
    kern[0][3] = (1.0/ denom)* kern[0][3]; 
    kern[0][4] =   0;
 
    kern[0][4] = (1.0/ denom)* kern[0][4]; 
    kern[0][5] =   0;
 
    kern[0][5] = (1.0/ denom)* kern[0][5]; 
    kern[0][6] =   0;
 
    kern[0][6] = (1.0/ denom)* kern[0][6]; 
    kern[0][7] =   0;
 
    kern[0][7] = (1.0/ denom)* kern[0][7]; 
    kern[0][8] =   0;
 
    kern[0][8] = (1.0/ denom)* kern[0][8]; 
    kern[0][9] =   0;
 
    kern[0][9] = (1.0/ denom)* kern[0][9]; 
    kern[0][10] =   0;
 
    kern[0][10] = (1.0/ denom)* kern[0][10]; 
    kern[0][11] =   0;
 
    kern[0][11] = (1.0/ denom)* kern[0][11]; 

    denom =  - 4.E+0*sp[35]*sp[34]*sp[27] + 4.E+0*pow(sp[34],3);

    kern[1][0] =  4.E+0*sp[35]*sp[34]*sp[27]*ang[0] - 4.E+0*sp[35]*sp[34]*sp[44]*ang[0] + 
      4.E+0*sp[35]*sp[34]*ang[1] - 4.E+0*sp[35]*sp[43]*sp[27]*ang[0] + 4.E+0*sp[35]*sp[43]*sp[44]*
      ang[0] - 4.E+0*sp[35]*sp[43]*ang[1] - 8.E+0*sp[34]*pow(sp[43],2)*ang[0] + 1.6E+1*
      pow(sp[34],2)*sp[43]*ang[0] - 8.E+0*pow(sp[34],3)*ang[0];
 
    kern[1][0] = (1.0/ denom)* kern[1][0]; 
    kern[1][1] =   - 4.E+0*sp[35]*sp[43]*sp[27]*sp[44]*ang[0] + 2.E+0*sp[35]*sp[43]*sp[27]*
      ang[1] + 2.E+0*sp[35]*sp[43]*pow(sp[27],2)*ang[0] + 2.E+0*sp[35]*sp[43]*sp[44]*ang[1] + 
      2.E+0*sp[35]*sp[43]*pow(sp[44],2)*ang[0] - 4.E+0*sp[35]*sp[43]*ang[2] + 4.E+0*sp[34]*
      pow(sp[43],2)*sp[27]*ang[0] - 4.E+0*sp[34]*pow(sp[43],2)*sp[44]*ang[0] - 8.E+0*sp[34]*
      pow(sp[43],2)*ang[1] - 4.E+0*pow(sp[34],2)*sp[43]*sp[27]*ang[0] + 4.E+0*pow(
      sp[34],2)*sp[43]*sp[44]*ang[0] + 4.E+0*pow(sp[34],2)*sp[43]*ang[1];
 
    kern[1][1] = (1.0/ denom)* kern[1][1]; 
    kern[1][2] =   0;
 
    kern[1][2] = (1.0/ denom)* kern[1][2]; 
    kern[1][3] =   0;
 
    kern[1][3] = (1.0/ denom)* kern[1][3]; 
    kern[1][4] =   0;
 
    kern[1][4] = (1.0/ denom)* kern[1][4]; 
    kern[1][5] =   0;
 
    kern[1][5] = (1.0/ denom)* kern[1][5]; 
    kern[1][6] =   0;
 
    kern[1][6] = (1.0/ denom)* kern[1][6]; 
    kern[1][7] =   0;
 
    kern[1][7] = (1.0/ denom)* kern[1][7]; 
    kern[1][8] =   0;
 
    kern[1][8] = (1.0/ denom)* kern[1][8]; 
    kern[1][9] =   0;
 
    kern[1][9] = (1.0/ denom)* kern[1][9]; 
    kern[1][10] =   0;
 
    kern[1][10] = (1.0/ denom)* kern[1][10]; 
    kern[1][11] =   0;
 
    kern[1][11] = (1.0/ denom)* kern[1][11]; 

    denom = 4.E+0*sp[34];

    kern[2][0] =   0;
 
    kern[2][0] = (1.0/ denom)* kern[2][0]; 
    kern[2][1] =   0;
 
    kern[2][1] = (1.0/ denom)* kern[2][1]; 
    kern[2][2] =  1.2E+1*sp[43]*ang[1];
 
    kern[2][2] = (1.0/ denom)* kern[2][2]; 
    kern[2][3] =   0;
 
    kern[2][3] = (1.0/ denom)* kern[2][3]; 
    kern[2][4] =   0;
 
    kern[2][4] = (1.0/ denom)* kern[2][4]; 
    kern[2][5] =   0;
 
    kern[2][5] = (1.0/ denom)* kern[2][5]; 
    kern[2][6] =   0;
 
    kern[2][6] = (1.0/ denom)* kern[2][6]; 
    kern[2][7] =   0;
 
    kern[2][7] = (1.0/ denom)* kern[2][7]; 
    kern[2][8] =   0;
 
    kern[2][8] = (1.0/ denom)* kern[2][8]; 
    kern[2][9] =   0;
 
    kern[2][9] = (1.0/ denom)* kern[2][9]; 
    kern[2][10] =   0;
 
    kern[2][10] = (1.0/ denom)* kern[2][10]; 
    kern[2][11] =   0;
 
    kern[2][11] = (1.0/ denom)* kern[2][11]; 

    denom =  - 1.6E+1*sp[35]*sp[34]*sp[27] + 1.6E+1*pow(sp[34],3);

    kern[3][0] =   0;
 
    kern[3][0] = (1.0/ denom)* kern[3][0]; 
    kern[3][1] =   0;
 
    kern[3][1] = (1.0/ denom)* kern[3][1]; 
    kern[3][2] =   0;
 
    kern[3][2] = (1.0/ denom)* kern[3][2]; 
    kern[3][3] =  1.6E+1*sp[35]*sp[43]*sp[27]*sp[44]*ang[0] + 8.E+0*sp[35]*sp[43]*sp[27]*ang[1]
       - 8.E+0*sp[35]*sp[43]*pow(sp[27],2)*ang[0] + 8.E+0*sp[35]*sp[43]*sp[44]*ang[1] - 8.E+0
      *sp[35]*sp[43]*pow(sp[44],2)*ang[0] + 3.2E+1*sp[34]*pow(sp[43],2)*sp[27]*ang[0] + 
      3.2E+1*sp[34]*pow(sp[43],2)*sp[44]*ang[0] - 1.6E+1*sp[34]*pow(sp[43],2)*ang[1] - 
      3.2E+1*pow(sp[34],2)*sp[43]*sp[44]*ang[0] - 3.2E+1*pow(sp[43],3)*sp[27]*ang[0];
 
    kern[3][3] = (1.0/ denom)* kern[3][3]; 
    kern[3][4] =   0;
 
    kern[3][4] = (1.0/ denom)* kern[3][4]; 
    kern[3][5] =   0;
 
    kern[3][5] = (1.0/ denom)* kern[3][5]; 
    kern[3][6] =   0;
 
    kern[3][6] = (1.0/ denom)* kern[3][6]; 
    kern[3][7] =   0;
 
    kern[3][7] = (1.0/ denom)* kern[3][7]; 
    kern[3][8] =   0;
 
    kern[3][8] = (1.0/ denom)* kern[3][8]; 
    kern[3][9] =   0;
 
    kern[3][9] = (1.0/ denom)* kern[3][9]; 
    kern[3][10] =   0;
 
    kern[3][10] = (1.0/ denom)* kern[3][10]; 
    kern[3][11] =   0;
 
    kern[3][11] = (1.0/ denom)* kern[3][11]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[34],2)*sp[27] + 8.E+0*pow(sp[35],2)*pow(sp[27],2)
       + 8.E+0*pow(sp[34],4);

    kern[4][0] =   - 4.E+0*sp[35]*sp[27]*sp[44]*ang[1] + 2.E+0*sp[35]*sp[27]*pow(sp[44],2)*
      ang[0] + 2.E+0*sp[35]*sp[27]*ang[2] - 4.E+0*sp[35]*pow(sp[27],2)*sp[44]*ang[0] - 4.E+0
      *sp[35]*pow(sp[27],2)*ang[1] + 2.E+0*sp[35]*pow(sp[27],3)*ang[0] - 2.4E+1*sp[34]*sp[43]
      *sp[27]*sp[44]*ang[0] + 2.4E+1*sp[34]*sp[43]*sp[27]*ang[1] - 2.4E+1*sp[34]*sp[43]*pow(
      sp[27],2)*ang[0] + 1.6E+1*pow(sp[34],2)*sp[27]*sp[44]*ang[0] - 8.E+0*pow(sp[34],2)*
      sp[27]*ang[1] + 4.E+0*pow(sp[34],2)*pow(sp[27],2)*ang[0] - 8.E+0*pow(sp[34],2)*
      sp[44]*ang[1] + 4.E+0*pow(sp[34],2)*pow(sp[44],2)*ang[0] + 4.E+0*pow(sp[34],2)*
      ang[2] + 2.4E+1*pow(sp[43],2)*pow(sp[27],2)*ang[0];
 
    kern[4][0] = (1.0/ denom)* kern[4][0]; 
    kern[4][1] =   - 3.E+0*sp[35]*sp[27]*sp[44]*ang[2] + sp[35]*sp[27]*pow(sp[44],3)*ang[0]
       + 2.E+0*sp[35]*sp[27]*ang[3] - 4.E+0*sp[35]*pow(sp[27],2)*sp[44]*ang[1] - 3.E+0*sp[35]
      *pow(sp[27],2)*pow(sp[44],2)*ang[0] - 5.E+0*sp[35]*pow(sp[27],2)*ang[2] + 3.E+0*
      sp[35]*pow(sp[27],3)*sp[44]*ang[0] + 4.E+0*sp[35]*pow(sp[27],3)*ang[1] - sp[35]*pow(
      sp[27],4)*ang[0] - 1.2E+1*sp[34]*sp[43]*sp[27]*sp[44]*ang[1] - 1.2E+1*sp[34]*sp[43]*sp[27]*
      pow(sp[44],2)*ang[0] + 2.4E+1*sp[34]*sp[43]*sp[27]*ang[2] - 3.6E+1*sp[34]*sp[43]*pow(
      sp[27],2)*ang[1] + 1.2E+1*sp[34]*sp[43]*pow(sp[27],3)*ang[0] + 1.6E+1*pow(sp[34],2)*
      sp[27]*sp[44]*ang[1] + 6.E+0*pow(sp[34],2)*sp[27]*pow(sp[44],2)*ang[0] - 1.E+1*pow(
      sp[34],2)*sp[27]*ang[2] - 6.E+0*pow(sp[34],2)*pow(sp[27],2)*sp[44]*ang[0] + 8.E+0*
      pow(sp[34],2)*pow(sp[27],2)*ang[1] - 2.E+0*pow(sp[34],2)*pow(sp[27],3)*ang[0] - 
      6.E+0*pow(sp[34],2)*sp[44]*ang[2] + 2.E+0*pow(sp[34],2)*pow(sp[44],3)*ang[0] + 
      4.E+0*pow(sp[34],2)*ang[3] + 1.2E+1*pow(sp[43],2)*pow(sp[27],2)*sp[44]*ang[0] + 
      2.4E+1*pow(sp[43],2)*pow(sp[27],2)*ang[1] - 1.2E+1*pow(sp[43],2)*pow(sp[27],3)*
      ang[0];
 
    kern[4][1] = (1.0/ denom)* kern[4][1]; 
    kern[4][2] =   0;
 
    kern[4][2] = (1.0/ denom)* kern[4][2]; 
    kern[4][3] =   0;
 
    kern[4][3] = (1.0/ denom)* kern[4][3]; 
    kern[4][4] =   - 1.6E+1*sp[35]*sp[34]*sp[43]*sp[27]*sp[44]*ang[0] + 1.6E+1*sp[35]*sp[34]*
      sp[43]*sp[27]*ang[1] + 8.E+0*sp[35]*pow(sp[34],2)*sp[27]*sp[44]*ang[0] + 1.6E+1*sp[35]*
      pow(sp[34],2)*sp[27]*ang[1] - 4.E+0*sp[35]*pow(sp[34],2)*pow(sp[27],2)*ang[0] - 
      8.E+0*sp[35]*pow(sp[34],2)*sp[44]*ang[1] + 4.E+0*sp[35]*pow(sp[34],2)*pow(sp[44],2)*
      ang[0] + 4.E+0*sp[35]*pow(sp[34],2)*ang[2] + 8.E+0*sp[35]*pow(sp[43],2)*pow(
      sp[27],2)*ang[0] - 4.E+0*pow(sp[35],2)*sp[27]*sp[44]*ang[1] + 2.E+0*pow(sp[35],2)*
      sp[27]*pow(sp[44],2)*ang[0] + 2.E+0*pow(sp[35],2)*sp[27]*ang[2] - 4.E+0*pow(
      sp[35],2)*pow(sp[27],2)*sp[44]*ang[0] - 1.2E+1*pow(sp[35],2)*pow(sp[27],2)*ang[1] + 
      2.E+0*pow(sp[35],2)*pow(sp[27],3)*ang[0] + 1.6E+1*pow(sp[34],2)*pow(sp[43],2)*
      sp[27]*ang[0] - 2.4E+1*pow(sp[34],3)*sp[43]*sp[27]*ang[0] - 8.E+0*pow(sp[34],3)*sp[43]*
      sp[44]*ang[0] + 8.E+0*pow(sp[34],3)*sp[43]*ang[1] + 8.E+0*pow(sp[34],4)*sp[27]*ang[0]
       + 8.E+0*pow(sp[34],4)*sp[44]*ang[0] - 1.6E+1*pow(sp[34],4)*ang[1];
 
    kern[4][4] = (1.0/ denom)* kern[4][4]; 
    kern[4][5] =   0;
 
    kern[4][5] = (1.0/ denom)* kern[4][5]; 
    kern[4][6] =   0;
 
    kern[4][6] = (1.0/ denom)* kern[4][6]; 
    kern[4][7] =   0;
 
    kern[4][7] = (1.0/ denom)* kern[4][7]; 
    kern[4][8] =   0;
 
    kern[4][8] = (1.0/ denom)* kern[4][8]; 
    kern[4][9] =   - 8.E+0*sp[35]*sp[34]*sp[43]*sp[27]*sp[44]*ang[1] - 8.E+0*sp[35]*sp[34]*sp[43]
      *sp[27]*pow(sp[44],2)*ang[0] + 1.6E+1*sp[35]*sp[34]*sp[43]*sp[27]*ang[2] - 2.4E+1*sp[35]*
      sp[34]*sp[43]*pow(sp[27],2)*ang[1] + 8.E+0*sp[35]*sp[34]*sp[43]*pow(sp[27],3)*ang[0] + 
      1.6E+1*sp[35]*pow(sp[34],2)*sp[27]*sp[44]*ang[1] + 6.E+0*sp[35]*pow(sp[34],2)*sp[27]*
      pow(sp[44],2)*ang[0] - 1.E+1*sp[35]*pow(sp[34],2)*sp[27]*ang[2] - 6.E+0*sp[35]*pow(
      sp[34],2)*pow(sp[27],2)*sp[44]*ang[0] + 8.E+0*sp[35]*pow(sp[34],2)*pow(sp[27],2)*ang[1]
       - 2.E+0*sp[35]*pow(sp[34],2)*pow(sp[27],3)*ang[0] - 6.E+0*sp[35]*pow(sp[34],2)*
      sp[44]*ang[2] + 2.E+0*sp[35]*pow(sp[34],2)*pow(sp[44],3)*ang[0] + 4.E+0*sp[35]*pow(
      sp[34],2)*ang[3] + 4.E+0*sp[35]*pow(sp[43],2)*pow(sp[27],2)*sp[44]*ang[0] + 8.E+0*
      sp[35]*pow(sp[43],2)*pow(sp[27],2)*ang[1] - 4.E+0*sp[35]*pow(sp[43],2)*pow(sp[27],3)*
      ang[0] - 3.E+0*pow(sp[35],2)*sp[27]*sp[44]*ang[2] + pow(sp[35],2)*sp[27]*pow(sp[44],3)*
      ang[0] + 2.E+0*pow(sp[35],2)*sp[27]*ang[3] - 4.E+0*pow(sp[35],2)*pow(sp[27],2)*
      sp[44]*ang[1] - 3.E+0*pow(sp[35],2)*pow(sp[27],2)*pow(sp[44],2)*ang[0] - 5.E+0*
      pow(sp[35],2)*pow(sp[27],2)*ang[2] + 3.E+0*pow(sp[35],2)*pow(sp[27],3)*sp[44]*ang[0]
       + 4.E+0*pow(sp[35],2)*pow(sp[27],3)*ang[1] - pow(sp[35],2)*pow(sp[27],4)*ang[0]
       + 8.E+0*pow(sp[34],2)*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 1.6E+1*pow(sp[34],2)*
      pow(sp[43],2)*sp[27]*ang[1] - 8.E+0*pow(sp[34],2)*pow(sp[43],2)*pow(sp[27],2)*ang[0]
       - 1.2E+1*pow(sp[34],3)*sp[43]*sp[27]*ang[1] + 4.E+0*pow(sp[34],3)*sp[43]*pow(
      sp[27],2)*ang[0] - 4.E+0*pow(sp[34],3)*sp[43]*sp[44]*ang[1] - 4.E+0*pow(sp[34],3)*
      sp[43]*pow(sp[44],2)*ang[0] + 8.E+0*pow(sp[34],3)*sp[43]*ang[2];
 
    kern[4][9] = (1.0/ denom)* kern[4][9]; 
    kern[4][10] =  2.E+0*sp[35]*sp[34]*sp[43]*sp[27]*pow(sp[44],2)*ang[0] - 2.E+0*sp[35]*
      sp[34]*sp[43]*sp[27]*ang[2] - 4.E+0*sp[35]*sp[34]*sp[43]*pow(sp[27],2)*sp[44]*ang[0] + 2.E+0*
      sp[35]*sp[34]*sp[43]*pow(sp[27],3)*ang[0] - 8.E+0*sp[35]*pow(sp[43],2)*pow(sp[27],2)*
      ang[1] + 2.4E+1*sp[34]*pow(sp[43],3)*pow(sp[27],2)*ang[0] - 2.4E+1*pow(sp[34],2)*
      pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 3.2E+1*pow(sp[34],2)*pow(sp[43],2)*sp[27]*ang[1]
       - 2.4E+1*pow(sp[34],2)*pow(sp[43],2)*pow(sp[27],2)*ang[0] + 1.6E+1*pow(
      sp[34],3)*sp[43]*sp[27]*sp[44]*ang[0] - 1.2E+1*pow(sp[34],3)*sp[43]*sp[27]*ang[1] + 4.E+0*
      pow(sp[34],3)*sp[43]*pow(sp[27],2)*ang[0] - 1.2E+1*pow(sp[34],3)*sp[43]*sp[44]*ang[1]
       + 4.E+0*pow(sp[34],3)*sp[43]*pow(sp[44],2)*ang[0] + 8.E+0*pow(sp[34],3)*sp[43]*
      ang[2];
 
    kern[4][10] = (1.0/ denom)* kern[4][10]; 
    kern[4][11] =   0;
 
    kern[4][11] = (1.0/ denom)* kern[4][11]; 

    denom =  - 3.2E+1*sp[35]*pow(sp[34],3)*sp[27] + 1.6E+1*pow(sp[35],2)*sp[34]*pow(
      sp[27],2) + 1.6E+1*pow(sp[34],5);

    kern[5][0] =   0;
 
    kern[5][0] = (1.0/ denom)* kern[5][0]; 
    kern[5][1] =   0;
 
    kern[5][1] = (1.0/ denom)* kern[5][1]; 
    kern[5][2] =   0;
 
    kern[5][2] = (1.0/ denom)* kern[5][2]; 
    kern[5][3] =  1.6E+1*sp[35]*sp[34]*sp[27]*sp[44]*ang[1] + 1.2E+1*sp[35]*sp[34]*sp[27]*pow(
      sp[44],2)*ang[0] - 1.2E+1*sp[35]*sp[34]*sp[27]*ang[2] + 1.2E+1*sp[35]*sp[34]*pow(sp[27],2)
      *sp[44]*ang[0] + 2.4E+1*sp[35]*sp[34]*pow(sp[27],2)*ang[1] - 1.2E+1*sp[35]*sp[34]*pow(
      sp[27],3)*ang[0] - 1.2E+1*sp[35]*sp[34]*sp[44]*ang[2] + 2.4E+1*sp[35]*sp[34]*pow(sp[44],2)
      *ang[1] - 1.2E+1*sp[35]*sp[34]*pow(sp[44],3)*ang[0] - 3.2E+1*sp[35]*sp[43]*sp[27]*sp[44]*
      ang[1] + 2.4E+1*sp[35]*sp[43]*sp[27]*pow(sp[44],2)*ang[0] + 8.E+0*sp[35]*sp[43]*sp[27]*
      ang[2] - 4.8E+1*sp[35]*sp[43]*pow(sp[27],2)*sp[44]*ang[0] - 3.2E+1*sp[35]*sp[43]*pow(
      sp[27],2)*ang[1] + 2.4E+1*sp[35]*sp[43]*pow(sp[27],3)*ang[0] - 1.44E+2*sp[34]*pow(
      sp[43],2)*sp[27]*sp[44]*ang[0] + 9.6E+1*sp[34]*pow(sp[43],2)*sp[27]*ang[1] - 1.44E+2*
      sp[34]*pow(sp[43],2)*pow(sp[27],2)*ang[0] + 1.92E+2*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*
      ang[0] - 6.4E+1*pow(sp[34],2)*sp[43]*sp[27]*ang[1] + 4.8E+1*pow(sp[34],2)*sp[43]*
      pow(sp[27],2)*ang[0] - 6.4E+1*pow(sp[34],2)*sp[43]*sp[44]*ang[1] + 4.8E+1*pow(
      sp[34],2)*sp[43]*pow(sp[44],2)*ang[0] + 1.6E+1*pow(sp[34],2)*sp[43]*ang[2] - 4.8E+1*
      pow(sp[34],3)*sp[27]*sp[44]*ang[0] + 3.2E+1*pow(sp[34],3)*sp[44]*ang[1] - 4.8E+1*
      pow(sp[34],3)*pow(sp[44],2)*ang[0] + 9.6E+1*pow(sp[43],3)*pow(sp[27],2)*ang[0];
 
    kern[5][3] = (1.0/ denom)* kern[5][3]; 
    kern[5][4] =   0;
 
    kern[5][4] = (1.0/ denom)* kern[5][4]; 
    kern[5][5] =  1.6E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 3.2E+1*sp[35]*
      sp[34]*pow(sp[43],2)*sp[27]*ang[1] + 1.6E+1*sp[35]*sp[34]*pow(sp[43],2)*pow(sp[27],2)*
      ang[0] + 1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0] + 2.4E+1*sp[35]*pow(
      sp[34],2)*sp[43]*sp[27]*ang[1] - 1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0]
       + 2.4E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[1] - 1.6E+1*sp[35]*pow(sp[34],2)*sp[43]
      *pow(sp[44],2)*ang[0] - 8.E+0*sp[35]*pow(sp[34],2)*sp[43]*ang[2] - 1.6E+1*sp[35]*
      pow(sp[43],3)*pow(sp[27],2)*ang[0] + 4.E+0*pow(sp[35],2)*sp[43]*sp[27]*pow(sp[44],2)*
      ang[0] - 4.E+0*pow(sp[35],2)*sp[43]*sp[27]*ang[2] - 8.E+0*pow(sp[35],2)*sp[43]*pow(
      sp[27],2)*sp[44]*ang[0] + 4.E+0*pow(sp[35],2)*sp[43]*pow(sp[27],3)*ang[0] - 3.2E+1*
      pow(sp[34],2)*pow(sp[43],3)*sp[27]*ang[0] + 3.2E+1*pow(sp[34],3)*pow(sp[43],2)*sp[27]
      *ang[0] + 3.2E+1*pow(sp[34],3)*pow(sp[43],2)*sp[44]*ang[0] - 1.6E+1*pow(sp[34],3)
      *pow(sp[43],2)*ang[1] - 3.2E+1*pow(sp[34],4)*sp[43]*sp[44]*ang[0];
 
    kern[5][5] = (1.0/ denom)* kern[5][5]; 
    kern[5][6] =  8.E+0*sp[35]*sp[34]*sp[27]*sp[44]*ang[0] + 8.E+0*sp[35]*sp[34]*sp[27]*ang[1]
       + 4.E+0*sp[35]*sp[34]*pow(sp[27],2)*ang[0] + 2.4E+1*sp[35]*sp[34]*sp[44]*ang[1] - 
      1.2E+1*sp[35]*sp[34]*pow(sp[44],2)*ang[0] - 1.2E+1*sp[35]*sp[34]*ang[2] + 1.6E+1*sp[35]
      *sp[43]*sp[27]*sp[44]*ang[0] - 1.6E+1*sp[35]*sp[43]*sp[27]*ang[1] - 1.6E+1*sp[35]*sp[43]*pow(
      sp[27],2)*ang[0] - 4.8E+1*sp[34]*pow(sp[43],2)*sp[27]*ang[0] + 6.4E+1*pow(sp[34],2)*
      sp[43]*sp[27]*ang[0] + 3.2E+1*pow(sp[34],2)*sp[43]*sp[44]*ang[0] - 3.2E+1*pow(sp[34],2)
      *sp[43]*ang[1] - 1.6E+1*pow(sp[34],3)*sp[27]*ang[0] - 3.2E+1*pow(sp[34],3)*sp[44]*
      ang[0] + 1.6E+1*pow(sp[34],3)*ang[1];
 
    kern[5][6] = (1.0/ denom)* kern[5][6]; 
    kern[5][7] =   0;
 
    kern[5][7] = (1.0/ denom)* kern[5][7]; 
    kern[5][8] =   0;
 
    kern[5][8] = (1.0/ denom)* kern[5][8]; 
    kern[5][9] =   0;
 
    kern[5][9] = (1.0/ denom)* kern[5][9]; 
    kern[5][10] =   0;
 
    kern[5][10] = (1.0/ denom)* kern[5][10]; 
    kern[5][11] =  8.E+0*sp[35]*sp[34]*sp[27]*sp[44]*ang[1] + 6.E+0*sp[35]*sp[34]*sp[27]*pow(
      sp[44],2)*ang[0] - 6.E+0*sp[35]*sp[34]*sp[27]*ang[2] + 6.E+0*sp[35]*sp[34]*pow(sp[27],2)*
      sp[44]*ang[0] + 1.2E+1*sp[35]*sp[34]*pow(sp[27],2)*ang[1] - 6.E+0*sp[35]*sp[34]*pow(
      sp[27],3)*ang[0] - 6.E+0*sp[35]*sp[34]*sp[44]*ang[2] + 1.2E+1*sp[35]*sp[34]*pow(sp[44],2)*
      ang[1] - 6.E+0*sp[35]*sp[34]*pow(sp[44],3)*ang[0] - 8.E+0*sp[35]*sp[43]*sp[27]*sp[44]*ang[1]
       + 8.E+0*sp[35]*sp[43]*sp[27]*pow(sp[44],2)*ang[0] - 1.6E+1*sp[35]*sp[43]*pow(sp[27],2)*
      sp[44]*ang[0] - 8.E+0*sp[35]*sp[43]*pow(sp[27],2)*ang[1] + 8.E+0*sp[35]*sp[43]*pow(
      sp[27],3)*ang[0] - 2.4E+1*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 2.4E+1*sp[34]*
      pow(sp[43],2)*pow(sp[27],2)*ang[0] + 6.4E+1*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0]
       - 1.6E+1*pow(sp[34],2)*sp[43]*sp[27]*ang[1] + 1.6E+1*pow(sp[34],2)*sp[43]*pow(
      sp[27],2)*ang[0] - 1.6E+1*pow(sp[34],2)*sp[43]*sp[44]*ang[1] + 1.6E+1*pow(sp[34],2)*
      sp[43]*pow(sp[44],2)*ang[0] - 2.4E+1*pow(sp[34],3)*sp[27]*sp[44]*ang[0] + 1.6E+1*
      pow(sp[34],3)*sp[44]*ang[1] - 2.4E+1*pow(sp[34],3)*pow(sp[44],2)*ang[0];
 
    kern[5][11] = (1.0/ denom)* kern[5][11]; 

    denom =  - 1.6E+1*sp[35]*sp[27] + 1.6E+1*pow(sp[34],2);

    kern[6][0] =   0;
 
    kern[6][0] = (1.0/ denom)* kern[6][0]; 
    kern[6][1] =   0;
 
    kern[6][1] = (1.0/ denom)* kern[6][1]; 
    kern[6][2] =   0;
 
    kern[6][2] = (1.0/ denom)* kern[6][2]; 
    kern[6][3] =   - 1.2E+1*sp[35]*sp[27]*pow(sp[44],2)*ang[0] - 4.E+0*sp[35]*sp[27]*
      ang[2] + 1.2E+1*sp[35]*pow(sp[27],2)*sp[44]*ang[0] + 8.E+0*sp[35]*pow(sp[27],2)*ang[1]
       - 4.E+0*sp[35]*pow(sp[27],3)*ang[0] + 4.E+0*sp[35]*sp[44]*ang[2] - 8.E+0*sp[35]*
      pow(sp[44],2)*ang[1] + 4.E+0*sp[35]*pow(sp[44],3)*ang[0] - 1.6E+1*sp[34]*sp[43]*sp[27]*
      ang[1] + 1.6E+1*sp[34]*sp[43]*pow(sp[27],2)*ang[0] + 1.6E+1*sp[34]*sp[43]*sp[44]*ang[1]
       - 1.6E+1*sp[34]*sp[43]*pow(sp[44],2)*ang[0] - 1.6E+1*pow(sp[34],2)*sp[27]*sp[44]*
      ang[0] + 1.6E+1*pow(sp[34],2)*pow(sp[44],2)*ang[0] + 1.6E+1*pow(sp[43],2)*sp[27]*
      sp[44]*ang[0] - 1.6E+1*pow(sp[43],2)*pow(sp[27],2)*ang[0];
 
    kern[6][3] = (1.0/ denom)* kern[6][3]; 
    kern[6][4] =   0;
 
    kern[6][4] = (1.0/ denom)* kern[6][4]; 
    kern[6][5] =  8.E+0*sp[35]*sp[34]*sp[43]*sp[27]*ang[1] - 8.E+0*sp[35]*sp[34]*sp[43]*pow(
      sp[27],2)*ang[0] - 8.E+0*sp[35]*sp[34]*sp[43]*sp[44]*ang[1] + 8.E+0*sp[35]*sp[34]*sp[43]*pow(
      sp[44],2)*ang[0] - 1.6E+1*sp[35]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 1.6E+1*sp[35]*
      pow(sp[43],2)*pow(sp[27],2)*ang[0];
 
    kern[6][5] = (1.0/ denom)* kern[6][5]; 
    kern[6][6] =   - 8.E+0*sp[35]*sp[27]*sp[44]*ang[0] + 8.E+0*sp[35]*sp[27]*ang[1] + 
      4.E+0*sp[35]*pow(sp[27],2)*ang[0] - 8.E+0*sp[35]*sp[44]*ang[1] + 4.E+0*sp[35]*pow(
      sp[44],2)*ang[0] + 4.E+0*sp[35]*ang[2] + 3.2E+1*sp[34]*sp[43]*sp[27]*ang[0] - 1.6E+1*
      pow(sp[34],2)*sp[27]*ang[0] - 1.6E+1*pow(sp[43],2)*sp[27]*ang[0];
 
    kern[6][6] = (1.0/ denom)* kern[6][6]; 
    kern[6][7] =   0;
 
    kern[6][7] = (1.0/ denom)* kern[6][7]; 
    kern[6][8] =   0;
 
    kern[6][8] = (1.0/ denom)* kern[6][8]; 
    kern[6][9] =   0;
 
    kern[6][9] = (1.0/ denom)* kern[6][9]; 
    kern[6][10] =   0;
 
    kern[6][10] = (1.0/ denom)* kern[6][10]; 
    kern[6][11] =   - 6.E+0*sp[35]*sp[27]*pow(sp[44],2)*ang[0] - 2.E+0*sp[35]*sp[27]*
      ang[2] + 6.E+0*sp[35]*pow(sp[27],2)*sp[44]*ang[0] + 4.E+0*sp[35]*pow(sp[27],2)*ang[1]
       - 2.E+0*sp[35]*pow(sp[27],3)*ang[0] + 2.E+0*sp[35]*sp[44]*ang[2] - 4.E+0*sp[35]*
      pow(sp[44],2)*ang[1] + 2.E+0*sp[35]*pow(sp[44],3)*ang[0] - 8.E+0*pow(sp[34],2)*
      sp[27]*sp[44]*ang[0] + 8.E+0*pow(sp[34],2)*pow(sp[44],2)*ang[0] - 8.E+0*pow(
      sp[43],2)*sp[27]*sp[44]*ang[0] + 8.E+0*pow(sp[43],2)*pow(sp[27],2)*ang[0];
 
    kern[6][11] = (1.0/ denom)* kern[6][11]; 

    denom =  - 4.8E+1*sp[35]*sp[27] + 4.8E+1*pow(sp[34],2);

    kern[7][0] =   0;
 
    kern[7][0] = (1.0/ denom)* kern[7][0]; 
    kern[7][1] =   0;
 
    kern[7][1] = (1.0/ denom)* kern[7][1]; 
    kern[7][2] =   0;
 
    kern[7][2] = (1.0/ denom)* kern[7][2]; 
    kern[7][3] =   0;
 
    kern[7][3] = (1.0/ denom)* kern[7][3]; 
    kern[7][4] =   0;
 
    kern[7][4] = (1.0/ denom)* kern[7][4]; 
    kern[7][5] =   0;
 
    kern[7][5] = (1.0/ denom)* kern[7][5]; 
    kern[7][6] =   0;
 
    kern[7][6] = (1.0/ denom)* kern[7][6]; 
    kern[7][7] =   - 2.4E+1*sp[35]*sp[27]*sp[44]*ang[0] - 4.8E+1*sp[35]*sp[27]*ang[1] + 
      1.2E+1*sp[35]*pow(sp[27],2)*ang[0] - 4.8E+1*sp[35]*sp[44]*ang[1] + 1.2E+1*sp[35]*
      pow(sp[44],2)*ang[0] + 3.6E+1*sp[35]*ang[2] - 4.8E+1*sp[34]*sp[43]*sp[27]*ang[0] - 
      4.8E+1*sp[34]*sp[43]*sp[44]*ang[0] + 9.6E+1*sp[34]*sp[43]*ang[1] + 4.8E+1*pow(sp[34],2)
      *sp[44]*ang[0] + 4.8E+1*pow(sp[43],2)*sp[27]*ang[0];
 
    kern[7][7] = (1.0/ denom)* kern[7][7]; 
    kern[7][8] =   0;
 
    kern[7][8] = (1.0/ denom)* kern[7][8]; 
    kern[7][9] =   0;
 
    kern[7][9] = (1.0/ denom)* kern[7][9]; 
    kern[7][10] =   0;
 
    kern[7][10] = (1.0/ denom)* kern[7][10]; 
    kern[7][11] =   0;
 
    kern[7][11] = (1.0/ denom)* kern[7][11]; 

    denom =  - 4.E+0*sp[35]*sp[34]*sp[27] + 4.E+0*pow(sp[34],3);

    kern[8][0] =   0;
 
    kern[8][0] = (1.0/ denom)* kern[8][0]; 
    kern[8][1] =   0;
 
    kern[8][1] = (1.0/ denom)* kern[8][1]; 
    kern[8][2] =  6.E+0*sp[34]*sp[27]*ang[1] + 6.E+0*sp[34]*sp[44]*ang[1] - 6.E+0*sp[34]*
      ang[2] - 1.2E+1*sp[43]*sp[27]*ang[1];
 
    kern[8][2] = (1.0/ denom)* kern[8][2]; 
    kern[8][3] =   0;
 
    kern[8][3] = (1.0/ denom)* kern[8][3]; 
    kern[8][4] =   0;
 
    kern[8][4] = (1.0/ denom)* kern[8][4]; 
    kern[8][5] =   0;
 
    kern[8][5] = (1.0/ denom)* kern[8][5]; 
    kern[8][6] =   0;
 
    kern[8][6] = (1.0/ denom)* kern[8][6]; 
    kern[8][7] =   0;
 
    kern[8][7] = (1.0/ denom)* kern[8][7]; 
    kern[8][8] =   - 6.E+0*sp[35]*sp[34]*sp[27]*ang[1] - 6.E+0*sp[35]*sp[34]*sp[44]*ang[1] + 
      6.E+0*sp[35]*sp[34]*ang[2] + 1.2E+1*pow(sp[34],2)*sp[43]*ang[1];
 
    kern[8][8] = (1.0/ denom)* kern[8][8]; 
    kern[8][9] =   0;
 
    kern[8][9] = (1.0/ denom)* kern[8][9]; 
    kern[8][10] =   0;
 
    kern[8][10] = (1.0/ denom)* kern[8][10]; 
    kern[8][11] =   0;
 
    kern[8][11] = (1.0/ denom)* kern[8][11]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[34],3)*sp[27] + 8.E+0*pow(sp[35],2)*sp[34]*pow(
      sp[27],2) + 8.E+0*pow(sp[34],5);

    kern[9][0] =  4.E+0*sp[35]*sp[34]*sp[27]*sp[44]*ang[0] + 4.E+0*sp[35]*sp[34]*sp[27]*ang[1]
       + 2.E+0*sp[35]*sp[34]*pow(sp[27],2)*ang[0] + 1.2E+1*sp[35]*sp[34]*sp[44]*ang[1] - 
      6.E+0*sp[35]*sp[34]*pow(sp[44],2)*ang[0] - 6.E+0*sp[35]*sp[34]*ang[2] + 8.E+0*sp[35]*
      sp[43]*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*sp[43]*sp[27]*ang[1] - 8.E+0*sp[35]*sp[43]*pow(
      sp[27],2)*ang[0] - 2.4E+1*sp[34]*pow(sp[43],2)*sp[27]*ang[0] + 3.2E+1*pow(sp[34],2)*
      sp[43]*sp[27]*ang[0] + 1.6E+1*pow(sp[34],2)*sp[43]*sp[44]*ang[0] - 1.6E+1*pow(sp[34],2)
      *sp[43]*ang[1] - 8.E+0*pow(sp[34],3)*sp[27]*ang[0] - 1.6E+1*pow(sp[34],3)*sp[44]*
      ang[0] + 8.E+0*pow(sp[34],3)*ang[1];
 
    kern[9][0] = (1.0/ denom)* kern[9][0]; 
    kern[9][1] =   - 4.E+0*sp[35]*sp[34]*sp[27]*sp[44]*ang[1] + 5.E+0*sp[35]*sp[34]*sp[27]*
      pow(sp[44],2)*ang[0] + 1.1E+1*sp[35]*sp[34]*sp[27]*ang[2] - sp[35]*sp[34]*pow(sp[27],2)*
      sp[44]*ang[0] - 4.E+0*sp[35]*sp[34]*pow(sp[27],2)*ang[1] - sp[35]*sp[34]*pow(sp[27],3)*
      ang[0] + 9.E+0*sp[35]*sp[34]*sp[44]*ang[2] - 3.E+0*sp[35]*sp[34]*pow(sp[44],3)*ang[0] - 
      6.E+0*sp[35]*sp[34]*ang[3] + 4.E+0*sp[35]*sp[43]*sp[27]*sp[44]*ang[1] + 4.E+0*sp[35]*sp[43]*
      sp[27]*pow(sp[44],2)*ang[0] - 8.E+0*sp[35]*sp[43]*sp[27]*ang[2] - 8.E+0*sp[35]*sp[43]*pow(
      sp[27],2)*sp[44]*ang[0] + 4.E+0*sp[35]*sp[43]*pow(sp[27],2)*ang[1] + 4.E+0*sp[35]*sp[43]*
      pow(sp[27],3)*ang[0] - 1.2E+1*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 2.4E+1*sp[34]
      *pow(sp[43],2)*sp[27]*ang[1] + 1.2E+1*sp[34]*pow(sp[43],2)*pow(sp[27],2)*ang[0] + 
      8.E+0*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0] + 3.2E+1*pow(sp[34],2)*sp[43]*sp[27]*
      ang[1] - 1.6E+1*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 8.E+0*pow(sp[34],2)*
      sp[43]*sp[44]*ang[1] + 8.E+0*pow(sp[34],2)*sp[43]*pow(sp[44],2)*ang[0] - 1.6E+1*pow(
      sp[34],2)*sp[43]*ang[2] + 4.E+0*pow(sp[34],3)*sp[27]*sp[44]*ang[0] - 8.E+0*pow(
      sp[34],3)*sp[27]*ang[1] + 4.E+0*pow(sp[34],3)*pow(sp[27],2)*ang[0] - 8.E+0*pow(
      sp[34],3)*sp[44]*ang[1] - 8.E+0*pow(sp[34],3)*pow(sp[44],2)*ang[0] + 4.E+0*pow(
      sp[34],3)*ang[2];
 
    kern[9][1] = (1.0/ denom)* kern[9][1]; 
    kern[9][2] =   0;
 
    kern[9][2] = (1.0/ denom)* kern[9][2]; 
    kern[9][3] =   0;
 
    kern[9][3] = (1.0/ denom)* kern[9][3]; 
    kern[9][4] =   - 8.E+0*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*ang[0] - 8.E+0*sp[35]*pow(
      sp[34],2)*sp[43]*sp[27]*ang[0] + 2.4E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[0] - 2.4E+1
      *sp[35]*pow(sp[34],2)*sp[43]*ang[1] + 1.6E+1*sp[35]*pow(sp[34],3)*sp[27]*ang[0] - 
      2.4E+1*sp[35]*pow(sp[34],3)*sp[44]*ang[0] + 1.6E+1*sp[35]*pow(sp[34],3)*ang[1] + 
      1.2E+1*pow(sp[35],2)*sp[34]*sp[27]*sp[44]*ang[0] - 4.E+0*pow(sp[35],2)*sp[34]*sp[27]*
      ang[1] - 6.E+0*pow(sp[35],2)*sp[34]*pow(sp[27],2)*ang[0] + 1.2E+1*pow(sp[35],2)*
      sp[34]*sp[44]*ang[1] - 6.E+0*pow(sp[35],2)*sp[34]*pow(sp[44],2)*ang[0] - 6.E+0*pow(
      sp[35],2)*sp[34]*ang[2] - 1.6E+1*pow(sp[34],3)*pow(sp[43],2)*ang[0] + 3.2E+1*pow(
      sp[34],4)*sp[43]*ang[0] - 1.6E+1*pow(sp[34],5)*ang[0];
 
    kern[9][4] = (1.0/ denom)* kern[9][4]; 
    kern[9][5] =   0;
 
    kern[9][5] = (1.0/ denom)* kern[9][5]; 
    kern[9][6] =   0;
 
    kern[9][6] = (1.0/ denom)* kern[9][6]; 
    kern[9][7] =   0;
 
    kern[9][7] = (1.0/ denom)* kern[9][7]; 
    kern[9][8] =   0;
 
    kern[9][8] = (1.0/ denom)* kern[9][8]; 
    kern[9][9] =   - 4.E+0*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*
      sp[34]*pow(sp[43],2)*sp[27]*ang[1] + 4.E+0*sp[35]*sp[34]*pow(sp[43],2)*pow(sp[27],2)*
      ang[0] - 8.E+0*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0] + 2.8E+1*sp[35]*pow(
      sp[34],2)*sp[43]*sp[27]*ang[1] - 4.E+0*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 
      1.2E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[1] + 1.2E+1*sp[35]*pow(sp[34],2)*sp[43]*
      pow(sp[44],2)*ang[0] - 2.4E+1*sp[35]*pow(sp[34],2)*sp[43]*ang[2] + 4.E+0*sp[35]*pow(
      sp[34],3)*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*pow(sp[34],3)*sp[27]*ang[1] + 4.E+0*sp[35]*
      pow(sp[34],3)*pow(sp[27],2)*ang[0] - 8.E+0*sp[35]*pow(sp[34],3)*sp[44]*ang[1] - 
      8.E+0*sp[35]*pow(sp[34],3)*pow(sp[44],2)*ang[0] + 4.E+0*sp[35]*pow(sp[34],3)*ang[2]
       - 4.E+0*pow(sp[35],2)*sp[34]*sp[27]*sp[44]*ang[1] + 5.E+0*pow(sp[35],2)*sp[34]*sp[27]*
      pow(sp[44],2)*ang[0] + 1.1E+1*pow(sp[35],2)*sp[34]*sp[27]*ang[2] - pow(sp[35],2)*sp[34]
      *pow(sp[27],2)*sp[44]*ang[0] - 4.E+0*pow(sp[35],2)*sp[34]*pow(sp[27],2)*ang[1] - 
      pow(sp[35],2)*sp[34]*pow(sp[27],3)*ang[0] + 9.E+0*pow(sp[35],2)*sp[34]*sp[44]*ang[2] - 
      3.E+0*pow(sp[35],2)*sp[34]*pow(sp[44],3)*ang[0] - 6.E+0*pow(sp[35],2)*sp[34]*ang[3]
       + 8.E+0*pow(sp[34],3)*pow(sp[43],2)*sp[27]*ang[0] - 8.E+0*pow(sp[34],3)*pow(
      sp[43],2)*sp[44]*ang[0] - 1.6E+1*pow(sp[34],3)*pow(sp[43],2)*ang[1] - 8.E+0*pow(
      sp[34],4)*sp[43]*sp[27]*ang[0] + 8.E+0*pow(sp[34],4)*sp[43]*sp[44]*ang[0] + 8.E+0*pow(
      sp[34],4)*sp[43]*ang[1];
 
    kern[9][9] = (1.0/ denom)* kern[9][9]; 
    kern[9][10] =  8.E+0*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*
      sp[34]*pow(sp[43],2)*sp[27]*ang[1] - 8.E+0*sp[35]*sp[34]*pow(sp[43],2)*pow(sp[27],2)*
      ang[0] + 4.E+0*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0] + 4.E+0*sp[35]*pow(
      sp[34],2)*sp[43]*sp[27]*ang[1] + 2.E+0*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 
      1.2E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[1] - 6.E+0*sp[35]*pow(sp[34],2)*sp[43]*
      pow(sp[44],2)*ang[0] - 6.E+0*sp[35]*pow(sp[34],2)*sp[43]*ang[2] - 2.4E+1*pow(
      sp[34],2)*pow(sp[43],3)*sp[27]*ang[0] + 3.2E+1*pow(sp[34],3)*pow(sp[43],2)*sp[27]*
      ang[0] + 1.6E+1*pow(sp[34],3)*pow(sp[43],2)*sp[44]*ang[0] - 1.6E+1*pow(sp[34],3)*
      pow(sp[43],2)*ang[1] - 8.E+0*pow(sp[34],4)*sp[43]*sp[27]*ang[0] - 1.6E+1*pow(
      sp[34],4)*sp[43]*sp[44]*ang[0] + 8.E+0*pow(sp[34],4)*sp[43]*ang[1];
 
    kern[9][10] = (1.0/ denom)* kern[9][10]; 
    kern[9][11] =   0;
 
    kern[9][11] = (1.0/ denom)* kern[9][11]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[34],3)*sp[27] + 8.E+0*pow(sp[35],2)*sp[34]*pow(
      sp[27],2) + 8.E+0*pow(sp[34],5);

    kern[10][0] =  4.E+0*sp[35]*sp[34]*sp[27]*sp[44]*ang[0] + 4.E+0*sp[35]*sp[34]*sp[27]*ang[1]
       + 2.E+0*sp[35]*sp[34]*pow(sp[27],2)*ang[0] + 1.2E+1*sp[35]*sp[34]*sp[44]*ang[1] - 
      6.E+0*sp[35]*sp[34]*pow(sp[44],2)*ang[0] - 6.E+0*sp[35]*sp[34]*ang[2] + 8.E+0*sp[35]*
      sp[43]*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*sp[43]*sp[27]*ang[1] - 8.E+0*sp[35]*sp[43]*pow(
      sp[27],2)*ang[0] - 2.4E+1*sp[34]*pow(sp[43],2)*sp[27]*ang[0] + 3.2E+1*pow(sp[34],2)*
      sp[43]*sp[27]*ang[0] + 1.6E+1*pow(sp[34],2)*sp[43]*sp[44]*ang[0] - 1.6E+1*pow(sp[34],2)
      *sp[43]*ang[1] - 8.E+0*pow(sp[34],3)*sp[27]*ang[0] - 1.6E+1*pow(sp[34],3)*sp[44]*
      ang[0] + 8.E+0*pow(sp[34],3)*ang[1];
 
    kern[10][0] = (1.0/ denom)* kern[10][0]; 
    kern[10][1] =   - 4.E+0*sp[35]*sp[34]*sp[27]*sp[44]*ang[1] + sp[35]*sp[34]*sp[27]*pow(
      sp[44],2)*ang[0] + 1.5E+1*sp[35]*sp[34]*sp[27]*ang[2] - sp[35]*sp[34]*pow(sp[27],2)*sp[44]*
      ang[0] - 1.2E+1*sp[35]*sp[34]*pow(sp[27],2)*ang[1] + 3.E+0*sp[35]*sp[34]*pow(sp[27],3)*
      ang[0] + 9.E+0*sp[35]*sp[34]*sp[44]*ang[2] - 3.E+0*sp[35]*sp[34]*pow(sp[44],3)*ang[0] - 
      6.E+0*sp[35]*sp[34]*ang[3] + 4.E+0*sp[35]*sp[43]*sp[27]*sp[44]*ang[1] + 4.E+0*sp[35]*sp[43]*
      sp[27]*pow(sp[44],2)*ang[0] - 8.E+0*sp[35]*sp[43]*sp[27]*ang[2] + 1.2E+1*sp[35]*sp[43]*
      pow(sp[27],2)*ang[1] - 4.E+0*sp[35]*sp[43]*pow(sp[27],3)*ang[0] - 1.2E+1*sp[34]*pow(
      sp[43],2)*sp[27]*sp[44]*ang[0] - 2.4E+1*sp[34]*pow(sp[43],2)*sp[27]*ang[1] + 1.2E+1*sp[34]
      *pow(sp[43],2)*pow(sp[27],2)*ang[0] + 2.4E+1*pow(sp[34],2)*sp[43]*sp[27]*ang[1] - 
      8.E+0*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 8.E+0*pow(sp[34],2)*sp[43]*sp[44]*
      ang[1] + 8.E+0*pow(sp[34],2)*sp[43]*pow(sp[44],2)*ang[0] - 1.6E+1*pow(sp[34],2)*
      sp[43]*ang[2] + 4.E+0*pow(sp[34],3)*sp[27]*sp[44]*ang[0] - 8.E+0*pow(sp[34],3)*sp[44]*
      ang[1] - 4.E+0*pow(sp[34],3)*pow(sp[44],2)*ang[0];
 
    kern[10][1] = (1.0/ denom)* kern[10][1]; 
    kern[10][2] =   0;
 
    kern[10][2] = (1.0/ denom)* kern[10][2]; 
    kern[10][3] =   0;
 
    kern[10][3] = (1.0/ denom)* kern[10][3]; 
    kern[10][4] =   - 2.4E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*ang[0] + 3.2E+1*sp[35]*
      pow(sp[34],2)*sp[43]*sp[27]*ang[0] + 1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[0] - 
      1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*ang[1] - 8.E+0*sp[35]*pow(sp[34],3)*sp[27]*ang[0] - 
      1.6E+1*sp[35]*pow(sp[34],3)*sp[44]*ang[0] + 8.E+0*sp[35]*pow(sp[34],3)*ang[1] + 
      4.E+0*pow(sp[35],2)*sp[34]*sp[27]*sp[44]*ang[0] + 4.E+0*pow(sp[35],2)*sp[34]*sp[27]*ang[1]
       + 2.E+0*pow(sp[35],2)*sp[34]*pow(sp[27],2)*ang[0] + 1.2E+1*pow(sp[35],2)*sp[34]*
      sp[44]*ang[1] - 6.E+0*pow(sp[35],2)*sp[34]*pow(sp[44],2)*ang[0] - 6.E+0*pow(
      sp[35],2)*sp[34]*ang[2] + 8.E+0*pow(sp[35],2)*sp[43]*sp[27]*sp[44]*ang[0] - 8.E+0*pow(
      sp[35],2)*sp[43]*sp[27]*ang[1] - 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[27],2)*ang[0];
 
    kern[10][4] = (1.0/ denom)* kern[10][4]; 
    kern[10][5] =   0;
 
    kern[10][5] = (1.0/ denom)* kern[10][5]; 
    kern[10][6] =   0;
 
    kern[10][6] = (1.0/ denom)* kern[10][6]; 
    kern[10][7] =   0;
 
    kern[10][7] = (1.0/ denom)* kern[10][7]; 
    kern[10][8] =   0;
 
    kern[10][8] = (1.0/ denom)* kern[10][8]; 
    kern[10][9] =   - 1.2E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 2.4E+1*
      sp[35]*sp[34]*pow(sp[43],2)*sp[27]*ang[1] + 1.2E+1*sp[35]*sp[34]*pow(sp[43],2)*pow(
      sp[27],2)*ang[0] + 2.4E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*ang[1] - 8.E+0*sp[35]*pow(
      sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 8.E+0*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[1] + 
      8.E+0*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[44],2)*ang[0] - 1.6E+1*sp[35]*pow(sp[34],2)*
      sp[43]*ang[2] + 4.E+0*sp[35]*pow(sp[34],3)*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*pow(
      sp[34],3)*sp[44]*ang[1] - 4.E+0*sp[35]*pow(sp[34],3)*pow(sp[44],2)*ang[0] - 4.E+0*
      pow(sp[35],2)*sp[34]*sp[27]*sp[44]*ang[1] + pow(sp[35],2)*sp[34]*sp[27]*pow(sp[44],2)*ang[0]
       + 1.5E+1*pow(sp[35],2)*sp[34]*sp[27]*ang[2] - pow(sp[35],2)*sp[34]*pow(sp[27],2)*sp[44]
      *ang[0] - 1.2E+1*pow(sp[35],2)*sp[34]*pow(sp[27],2)*ang[1] + 3.E+0*pow(sp[35],2)*
      sp[34]*pow(sp[27],3)*ang[0] + 9.E+0*pow(sp[35],2)*sp[34]*sp[44]*ang[2] - 3.E+0*pow(
      sp[35],2)*sp[34]*pow(sp[44],3)*ang[0] - 6.E+0*pow(sp[35],2)*sp[34]*ang[3] + 4.E+0*
      pow(sp[35],2)*sp[43]*sp[27]*sp[44]*ang[1] + 4.E+0*pow(sp[35],2)*sp[43]*sp[27]*pow(sp[44],2)
      *ang[0] - 8.E+0*pow(sp[35],2)*sp[43]*sp[27]*ang[2] + 1.2E+1*pow(sp[35],2)*sp[43]*
      pow(sp[27],2)*ang[1] - 4.E+0*pow(sp[35],2)*sp[43]*pow(sp[27],3)*ang[0];
 
    kern[10][9] = (1.0/ denom)* kern[10][9]; 
    kern[10][10] =  1.6E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*
      sp[34]*pow(sp[43],2)*sp[27]*ang[1] + 1.6E+1*sp[35]*sp[34]*pow(sp[43],2)*pow(sp[27],2)*
      ang[0] - 4.E+0*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0] + 1.6E+1*sp[35]*pow(
      sp[34],2)*sp[43]*sp[27]*ang[1] - 6.E+0*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 
      1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[1] - 6.E+0*sp[35]*pow(sp[34],2)*sp[43]*
      pow(sp[44],2)*ang[0] - 1.E+1*sp[35]*pow(sp[34],2)*sp[43]*ang[2] - 1.6E+1*sp[35]*pow(
      sp[43],3)*pow(sp[27],2)*ang[0] - 4.E+0*pow(sp[35],2)*sp[43]*sp[27]*sp[44]*ang[1] + 
      4.E+0*pow(sp[35],2)*sp[43]*sp[27]*ang[2] - 4.E+0*pow(sp[35],2)*sp[43]*pow(sp[27],2)*
      ang[1] - 8.E+0*pow(sp[34],2)*pow(sp[43],3)*sp[27]*ang[0] + 8.E+0*pow(sp[34],3)*
      pow(sp[43],2)*sp[27]*ang[0] + 8.E+0*pow(sp[34],3)*pow(sp[43],2)*sp[44]*ang[0] - 
      1.6E+1*pow(sp[34],3)*pow(sp[43],2)*ang[1] - 8.E+0*pow(sp[34],4)*sp[43]*sp[44]*ang[0]
      ;
 
    kern[10][10] = (1.0/ denom)* kern[10][10]; 
    kern[10][11] =   0;
 
    kern[10][11] = (1.0/ denom)* kern[10][11]; 

    denom =  - 3.2E+1*sp[35]*pow(sp[34],3)*sp[27] + 1.6E+1*pow(sp[35],2)*sp[34]*pow(
      sp[27],2) + 1.6E+1*pow(sp[34],5);

    kern[11][0] =   0;
 
    kern[11][0] = (1.0/ denom)* kern[11][0]; 
    kern[11][1] =   0;
 
    kern[11][1] = (1.0/ denom)* kern[11][1]; 
    kern[11][2] =   0;
 
    kern[11][2] = (1.0/ denom)* kern[11][2]; 
    kern[11][3] =  1.12E+2*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 6.4E+1*sp[35]
      *sp[34]*pow(sp[43],2)*sp[27]*ang[1] + 8.E+1*sp[35]*sp[34]*pow(sp[43],2)*pow(sp[27],2)*
      ang[0] - 1.12E+2*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0] + 4.8E+1*sp[35]*pow(
      sp[34],2)*sp[43]*sp[27]*ang[1] - 2.4E+1*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0]
       + 8.E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[1] - 5.6E+1*sp[35]*pow(sp[34],2)*sp[43]*
      pow(sp[44],2)*ang[0] - 2.4E+1*sp[35]*pow(sp[34],2)*sp[43]*ang[2] - 1.6E+1*sp[35]*
      pow(sp[34],3)*sp[27]*ang[1] + 8.E+0*sp[35]*pow(sp[34],3)*pow(sp[27],2)*ang[0] - 
      4.8E+1*sp[35]*pow(sp[34],3)*sp[44]*ang[1] + 5.6E+1*sp[35]*pow(sp[34],3)*pow(sp[44],2)
      *ang[0] + 8.E+0*sp[35]*pow(sp[34],3)*ang[2] - 6.4E+1*sp[35]*pow(sp[43],3)*pow(
      sp[27],2)*ang[0] - 2.E+1*pow(sp[35],2)*sp[34]*sp[27]*pow(sp[44],2)*ang[0] + 4.E+0*
      pow(sp[35],2)*sp[34]*sp[27]*ang[2] + 4.E+0*pow(sp[35],2)*sp[34]*pow(sp[27],2)*sp[44]*
      ang[0] - 8.E+0*pow(sp[35],2)*sp[34]*pow(sp[27],2)*ang[1] + 4.E+0*pow(sp[35],2)*
      sp[34]*pow(sp[27],3)*ang[0] + 1.2E+1*pow(sp[35],2)*sp[34]*sp[44]*ang[2] - 2.4E+1*
      pow(sp[35],2)*sp[34]*pow(sp[44],2)*ang[1] + 1.2E+1*pow(sp[35],2)*sp[34]*pow(sp[44],3)
      *ang[0] + 1.6E+1*pow(sp[35],2)*sp[43]*sp[27]*sp[44]*ang[1] - 1.6E+1*pow(sp[35],2)*
      sp[43]*sp[27]*pow(sp[44],2)*ang[0] + 3.2E+1*pow(sp[35],2)*sp[43]*pow(sp[27],2)*sp[44]*
      ang[0] + 1.6E+1*pow(sp[35],2)*sp[43]*pow(sp[27],2)*ang[1] - 1.6E+1*pow(sp[35],2)*
      sp[43]*pow(sp[27],3)*ang[0] - 3.2E+1*pow(sp[34],2)*pow(sp[43],3)*sp[27]*ang[0] + 
      6.4E+1*pow(sp[34],3)*pow(sp[43],2)*sp[27]*ang[0] + 3.2E+1*pow(sp[34],3)*pow(
      sp[43],2)*sp[44]*ang[0] - 3.2E+1*pow(sp[34],3)*pow(sp[43],2)*ang[1] - 3.2E+1*pow(
      sp[34],4)*sp[43]*sp[27]*ang[0] - 6.4E+1*pow(sp[34],4)*sp[43]*sp[44]*ang[0] + 3.2E+1*
      pow(sp[34],4)*sp[43]*ang[1] + 3.2E+1*pow(sp[34],5)*sp[44]*ang[0];
 
    kern[11][3] = (1.0/ denom)* kern[11][3]; 
    kern[11][4] =   0;
 
    kern[11][4] = (1.0/ denom)* kern[11][4]; 
    kern[11][5] =  4.8E+1*sp[35]*pow(sp[34],2)*pow(sp[43],3)*sp[27]*ang[0] - 6.4E+1*
      sp[35]*pow(sp[34],3)*pow(sp[43],2)*sp[27]*ang[0] - 3.2E+1*sp[35]*pow(sp[34],3)*pow(
      sp[43],2)*sp[44]*ang[0] + 3.2E+1*sp[35]*pow(sp[34],3)*pow(sp[43],2)*ang[1] + 1.6E+1*
      sp[35]*pow(sp[34],4)*sp[43]*sp[27]*ang[0] + 3.2E+1*sp[35]*pow(sp[34],4)*sp[43]*sp[44]*ang[0]
       - 1.6E+1*sp[35]*pow(sp[34],4)*sp[43]*ang[1] - 1.6E+1*pow(sp[35],2)*sp[34]*pow(
      sp[43],2)*sp[27]*sp[44]*ang[0] + 1.6E+1*pow(sp[35],2)*sp[34]*pow(sp[43],2)*sp[27]*ang[1]
       + 1.6E+1*pow(sp[35],2)*sp[34]*pow(sp[43],2)*pow(sp[27],2)*ang[0] - 8.E+0*pow(
      sp[35],2)*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0] - 8.E+0*pow(sp[35],2)*pow(sp[34],2)*
      sp[43]*sp[27]*ang[1] - 4.E+0*pow(sp[35],2)*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] - 
      2.4E+1*pow(sp[35],2)*pow(sp[34],2)*sp[43]*sp[44]*ang[1] + 1.2E+1*pow(sp[35],2)*
      pow(sp[34],2)*sp[43]*pow(sp[44],2)*ang[0] + 1.2E+1*pow(sp[35],2)*pow(sp[34],2)*sp[43]
      *ang[2];
 
    kern[11][5] = (1.0/ denom)* kern[11][5]; 
    kern[11][6] =  1.6E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*ang[0] + 1.6E+1*sp[35]*pow(
      sp[34],2)*sp[43]*sp[27]*ang[0] - 4.8E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[0] + 4.8E+1
      *sp[35]*pow(sp[34],2)*sp[43]*ang[1] - 3.2E+1*sp[35]*pow(sp[34],3)*sp[27]*ang[0] + 
      4.8E+1*sp[35]*pow(sp[34],3)*sp[44]*ang[0] - 3.2E+1*sp[35]*pow(sp[34],3)*ang[1] - 
      2.4E+1*pow(sp[35],2)*sp[34]*sp[27]*sp[44]*ang[0] + 8.E+0*pow(sp[35],2)*sp[34]*sp[27]*
      ang[1] + 1.2E+1*pow(sp[35],2)*sp[34]*pow(sp[27],2)*ang[0] - 2.4E+1*pow(sp[35],2)*
      sp[34]*sp[44]*ang[1] + 1.2E+1*pow(sp[35],2)*sp[34]*pow(sp[44],2)*ang[0] + 1.2E+1*
      pow(sp[35],2)*sp[34]*ang[2] + 3.2E+1*pow(sp[34],3)*pow(sp[43],2)*ang[0] - 6.4E+1*
      pow(sp[34],4)*sp[43]*ang[0] + 3.2E+1*pow(sp[34],5)*ang[0];
 
    kern[11][6] = (1.0/ denom)* kern[11][6]; 
    kern[11][7] =   0;
 
    kern[11][7] = (1.0/ denom)* kern[11][7]; 
    kern[11][8] =   0;
 
    kern[11][8] = (1.0/ denom)* kern[11][8]; 
    kern[11][9] =   0;
 
    kern[11][9] = (1.0/ denom)* kern[11][9]; 
    kern[11][10] =   0;
 
    kern[11][10] = (1.0/ denom)* kern[11][10]; 
    kern[11][11] =  8.E+0*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 2.4E+1*sp[35]*
      sp[34]*pow(sp[43],2)*pow(sp[27],2)*ang[0] - 1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*
      sp[44]*ang[0] + 2.4E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*ang[1] - 2.4E+1*sp[35]*pow(
      sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 2.4E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[1]
       - 2.4E+1*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[44],2)*ang[0] - 8.E+0*sp[35]*pow(
      sp[34],3)*sp[27]*ang[1] + 4.E+0*sp[35]*pow(sp[34],3)*pow(sp[27],2)*ang[0] - 2.4E+1*
      sp[35]*pow(sp[34],3)*sp[44]*ang[1] + 2.8E+1*sp[35]*pow(sp[34],3)*pow(sp[44],2)*ang[0]
       + 4.E+0*sp[35]*pow(sp[34],3)*ang[2] - 1.E+1*pow(sp[35],2)*sp[34]*sp[27]*pow(
      sp[44],2)*ang[0] + 2.E+0*pow(sp[35],2)*sp[34]*sp[27]*ang[2] + 2.E+0*pow(sp[35],2)*
      sp[34]*pow(sp[27],2)*sp[44]*ang[0] - 4.E+0*pow(sp[35],2)*sp[34]*pow(sp[27],2)*ang[1] + 
      2.E+0*pow(sp[35],2)*sp[34]*pow(sp[27],3)*ang[0] + 6.E+0*pow(sp[35],2)*sp[34]*sp[44]*
      ang[2] - 1.2E+1*pow(sp[35],2)*sp[34]*pow(sp[44],2)*ang[1] + 6.E+0*pow(sp[35],2)*
      sp[34]*pow(sp[44],3)*ang[0] + 1.6E+1*pow(sp[34],3)*pow(sp[43],2)*sp[44]*ang[0] - 
      3.2E+1*pow(sp[34],4)*sp[43]*sp[44]*ang[0] + 1.6E+1*pow(sp[34],5)*sp[44]*ang[0];
 
    kern[11][11] = (1.0/ denom)* kern[11][11]; 

}


void kernelY_noopti_qphv(matCdoub& kern,
  const ArrayScalarProducts& sp,  const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


    // local variables
    Cdoub denom;


    denom =  - 4.E+0*sp[35]*sp[44] + 4.E+0*pow(sp[43],2);

    kern[0][0] =   - 4.E+0*sp[35]*sp[44]*sp[7]*svp*svm - 4.E+0*sp[35]*sp[44]*ssp*
      ssm - 4.E+0*sp[43]*sp[31]*sp[37]*svp*svm - 4.E+0*sp[43]*sp[29]*sp[39]*svp*svm
       + 4.E+0*pow(sp[43],2)*sp[7]*svp*svm + 4.E+0*pow(sp[43],2)*ssp*ssm + 
      8.E+0*sp[31]*sp[29]*sp[44]*svp*svm;
 
    kern[0][0] = (1.0/ denom)* kern[0][0]; 
    kern[0][1] =  4.E+0*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm + 4.E+0*sp[43]*sp[29]*sp[44]*
      sp[39]*svp*svm - 8.E+0*pow(sp[43],2)*sp[39]*sp[37]*svp*svm;
 
    kern[0][1] = (1.0/ denom)* kern[0][1]; 
    kern[0][2] =  4.E+0*sp[43]*sp[31]*sp[44]*ssp*svm + 4.E+0*sp[43]*sp[29]*sp[44]*svp*
      ssm - 4.E+0*pow(sp[43],2)*sp[39]*ssp*svm - 4.E+0*pow(sp[43],2)*sp[37]*svp*
      ssm;
 
    kern[0][2] = (1.0/ denom)* kern[0][2]; 
    kern[0][3] =  8.E+0*sp[35]*sp[43]*sp[44]*sp[39]*ssp*svm - 8.E+0*sp[35]*sp[43]*sp[44]*
      sp[37]*svp*ssm - 8.E+0*pow(sp[43],3)*sp[39]*ssp*svm + 8.E+0*pow(sp[43],3)*
      sp[37]*svp*ssm;
 
    kern[0][3] = (1.0/ denom)* kern[0][3]; 
    kern[0][4] =   0;
 
    kern[0][4] = (1.0/ denom)* kern[0][4]; 
    kern[0][5] =   0;
 
    kern[0][5] = (1.0/ denom)* kern[0][5]; 
    kern[0][6] =   0;
 
    kern[0][6] = (1.0/ denom)* kern[0][6]; 
    kern[0][7] =   0;
 
    kern[0][7] = (1.0/ denom)* kern[0][7]; 
    kern[0][8] =   0;
 
    kern[0][8] = (1.0/ denom)* kern[0][8]; 
    kern[0][9] =   0;
 
    kern[0][9] = (1.0/ denom)* kern[0][9]; 
    kern[0][10] =   0;
 
    kern[0][10] = (1.0/ denom)* kern[0][10]; 
    kern[0][11] =   0;
 
    kern[0][11] = (1.0/ denom)* kern[0][11]; 

    denom =  - 4.E+0*sp[35]*sp[43]*sp[44] + 4.E+0*pow(sp[43],3);

    kern[1][0] =  4.E+0*sp[35]*sp[31]*sp[37]*svp*svm + 4.E+0*sp[35]*sp[29]*sp[39]*svp
      *svm - 8.E+0*sp[43]*sp[31]*sp[29]*svp*svm;
 
    kern[1][0] = (1.0/ denom)* kern[1][0]; 
    kern[1][1] =   - 4.E+0*sp[35]*sp[43]*sp[44]*sp[7]*svp*svm - 4.E+0*sp[35]*sp[43]*
      sp[44]*ssp*ssm + 8.E+0*sp[35]*sp[43]*sp[39]*sp[37]*svp*svm - 4.E+0*pow(sp[43],2)*
      sp[31]*sp[37]*svp*svm - 4.E+0*pow(sp[43],2)*sp[29]*sp[39]*svp*svm + 4.E+0*
      pow(sp[43],3)*sp[7]*svp*svm + 4.E+0*pow(sp[43],3)*ssp*ssm;
 
    kern[1][1] = (1.0/ denom)* kern[1][1]; 
    kern[1][2] =  4.E+0*sp[35]*sp[43]*sp[39]*ssp*svm + 4.E+0*sp[35]*sp[43]*sp[37]*svp*
      ssm - 4.E+0*pow(sp[43],2)*sp[31]*ssp*svm - 4.E+0*pow(sp[43],2)*sp[29]*svp*
      ssm;
 
    kern[1][2] = (1.0/ denom)* kern[1][2]; 
    kern[1][3] =   - 8.E+0*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm + 8.E+0*sp[35]*sp[43]*
      sp[29]*sp[44]*svp*ssm + 8.E+0*pow(sp[43],3)*sp[31]*ssp*svm - 8.E+0*pow(
      sp[43],3)*sp[29]*svp*ssm;
 
    kern[1][3] = (1.0/ denom)* kern[1][3]; 
    kern[1][4] =   0;
 
    kern[1][4] = (1.0/ denom)* kern[1][4]; 
    kern[1][5] =   0;
 
    kern[1][5] = (1.0/ denom)* kern[1][5]; 
    kern[1][6] =   0;
 
    kern[1][6] = (1.0/ denom)* kern[1][6]; 
    kern[1][7] =   0;
 
    kern[1][7] = (1.0/ denom)* kern[1][7]; 
    kern[1][8] =   0;
 
    kern[1][8] = (1.0/ denom)* kern[1][8]; 
    kern[1][9] =   0;
 
    kern[1][9] = (1.0/ denom)* kern[1][9]; 
    kern[1][10] =   0;
 
    kern[1][10] = (1.0/ denom)* kern[1][10]; 
    kern[1][11] =   0;
 
    kern[1][11] = (1.0/ denom)* kern[1][11]; 

    denom = 4.E+0*sp[43];

    kern[2][0] =  4.E+0*sp[31]*ssp*svm + 4.E+0*sp[29]*svp*ssm;
 
    kern[2][0] = (1.0/ denom)* kern[2][0]; 
    kern[2][1] =  4.E+0*sp[43]*sp[39]*ssp*svm + 4.E+0*sp[43]*sp[37]*svp*ssm;
 
    kern[2][1] = (1.0/ denom)* kern[2][1]; 
    kern[2][2] =   - 4.E+0*sp[43]*sp[7]*svp*svm + 4.E+0*sp[43]*ssp*ssm;
 
    kern[2][2] = (1.0/ denom)* kern[2][2]; 
    kern[2][3] =  8.E+0*sp[43]*sp[31]*sp[37]*svp*svm - 8.E+0*sp[43]*sp[29]*sp[39]*svp
      *svm;
 
    kern[2][3] = (1.0/ denom)* kern[2][3]; 
    kern[2][4] =   0;
 
    kern[2][4] = (1.0/ denom)* kern[2][4]; 
    kern[2][5] =   0;
 
    kern[2][5] = (1.0/ denom)* kern[2][5]; 
    kern[2][6] =   0;
 
    kern[2][6] = (1.0/ denom)* kern[2][6]; 
    kern[2][7] =   0;
 
    kern[2][7] = (1.0/ denom)* kern[2][7]; 
    kern[2][8] =   0;
 
    kern[2][8] = (1.0/ denom)* kern[2][8]; 
    kern[2][9] =   0;
 
    kern[2][9] = (1.0/ denom)* kern[2][9]; 
    kern[2][10] =   0;
 
    kern[2][10] = (1.0/ denom)* kern[2][10]; 
    kern[2][11] =   0;
 
    kern[2][11] = (1.0/ denom)* kern[2][11]; 

    denom =  - 1.6E+1*sp[35]*sp[43]*sp[44] + 1.6E+1*pow(sp[43],3);

    kern[3][0] =   - 8.E+0*sp[35]*sp[39]*ssp*svm + 8.E+0*sp[35]*sp[37]*svp*ssm + 
      8.E+0*sp[43]*sp[31]*ssp*svm - 8.E+0*sp[43]*sp[29]*svp*ssm;
 
    kern[3][0] = (1.0/ denom)* kern[3][0]; 
    kern[3][1] =  8.E+0*sp[43]*sp[31]*sp[44]*ssp*svm - 8.E+0*sp[43]*sp[29]*sp[44]*svp*
      ssm - 8.E+0*pow(sp[43],2)*sp[39]*ssp*svm + 8.E+0*pow(sp[43],2)*sp[37]*svp*
      ssm;
 
    kern[3][1] = (1.0/ denom)* kern[3][1]; 
    kern[3][2] =   - 8.E+0*sp[43]*sp[31]*sp[37]*svp*svm + 8.E+0*sp[43]*sp[29]*sp[39]*
      svp*svm;
 
    kern[3][2] = (1.0/ denom)* kern[3][2]; 
    kern[3][3] =  1.6E+1*sp[35]*sp[43]*sp[44]*sp[7]*svp*svm - 1.6E+1*sp[35]*sp[43]*sp[44]
      *ssp*ssm - 3.2E+1*sp[35]*sp[43]*sp[39]*sp[37]*svp*svm - 3.2E+1*sp[43]*sp[31]*
      sp[29]*sp[44]*svp*svm + 3.2E+1*pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 3.2E+1*
      pow(sp[43],2)*sp[29]*sp[39]*svp*svm - 1.6E+1*pow(sp[43],3)*sp[7]*svp*svm + 
      1.6E+1*pow(sp[43],3)*ssp*ssm;
 
    kern[3][3] = (1.0/ denom)* kern[3][3]; 
    kern[3][4] =   0;
 
    kern[3][4] = (1.0/ denom)* kern[3][4]; 
    kern[3][5] =   0;
 
    kern[3][5] = (1.0/ denom)* kern[3][5]; 
    kern[3][6] =   0;
 
    kern[3][6] = (1.0/ denom)* kern[3][6]; 
    kern[3][7] =   0;
 
    kern[3][7] = (1.0/ denom)* kern[3][7]; 
    kern[3][8] =   0;
 
    kern[3][8] = (1.0/ denom)* kern[3][8]; 
    kern[3][9] =   0;
 
    kern[3][9] = (1.0/ denom)* kern[3][9]; 
    kern[3][10] =   0;
 
    kern[3][10] = (1.0/ denom)* kern[3][10]; 
    kern[3][11] =   0;
 
    kern[3][11] = (1.0/ denom)* kern[3][11]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[43],2)*sp[44] + 8.E+0*pow(sp[35],2)*pow(sp[44],2)
       + 8.E+0*pow(sp[43],4);

    kern[4][0] =  8.E+0*sp[35]*sp[44]*sp[39]*sp[37]*svp*svm - 8.E+0*sp[35]*pow(
      sp[44],2)*sp[7]*svp*svm - 2.4E+1*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 2.4E+1*
      sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 8.E+0*pow(sp[43],2)*sp[44]*sp[7]*svp*svm + 
      1.6E+1*pow(sp[43],2)*sp[39]*sp[37]*svp*svm + 2.4E+1*sp[31]*sp[29]*pow(sp[44],2)
      *svp*svm;
 
    kern[4][0] = (1.0/ denom)* kern[4][0]; 
    kern[4][1] =   0;
 
    kern[4][1] = (1.0/ denom)* kern[4][1]; 
    kern[4][2] =   0;
 
    kern[4][2] = (1.0/ denom)* kern[4][2]; 
    kern[4][3] =   0;
 
    kern[4][3] = (1.0/ denom)* kern[4][3]; 
    kern[4][4] =   - 1.6E+1*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*sp[35]*
      sp[43]*sp[29]*sp[44]*sp[39]*svp*svm - 8.E+0*sp[35]*pow(sp[43],2)*sp[44]*sp[7]*svp*
      svm - 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]*ssp*ssm + 1.6E+1*sp[35]*pow(sp[43],2)*
      sp[39]*sp[37]*svp*svm + 8.E+0*sp[35]*sp[31]*sp[29]*pow(sp[44],2)*svp*svm + 
      8.E+0*pow(sp[35],2)*sp[44]*sp[39]*sp[37]*svp*svm + 8.E+0*pow(sp[35],2)*pow(
      sp[44],2)*ssp*ssm + 1.6E+1*pow(sp[43],2)*sp[31]*sp[29]*sp[44]*svp*svm - 8.E+0*
      pow(sp[43],3)*sp[31]*sp[37]*svp*svm - 8.E+0*pow(sp[43],3)*sp[29]*sp[39]*svp*svm
       + 8.E+0*pow(sp[43],4)*sp[7]*svp*svm + 8.E+0*pow(sp[43],4)*ssp*ssm;
 
    kern[4][4] = (1.0/ denom)* kern[4][4]; 
    kern[4][5] =   - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44]*sp[39]*ssp*svm + 1.6E+1*
      sp[35]*pow(sp[43],3)*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[44],2)
      *sp[39]*ssp*svm - 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[44],2)*sp[37]*svp*ssm + 
      8.E+0*pow(sp[43],5)*sp[39]*ssp*svm - 8.E+0*pow(sp[43],5)*sp[37]*svp*ssm;
 
    kern[4][5] = (1.0/ denom)* kern[4][5]; 
    kern[4][6] =   - 8.E+0*sp[35]*sp[43]*sp[44]*sp[39]*ssp*svm + 8.E+0*sp[35]*sp[43]*sp[44]
      *sp[37]*svp*ssm + 8.E+0*sp[35]*sp[31]*pow(sp[44],2)*ssp*svm - 8.E+0*sp[35]*
      sp[29]*pow(sp[44],2)*svp*ssm - 8.E+0*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm + 
      8.E+0*pow(sp[43],2)*sp[29]*sp[44]*svp*ssm + 8.E+0*pow(sp[43],3)*sp[39]*ssp*svm
       - 8.E+0*pow(sp[43],3)*sp[37]*svp*ssm;
 
    kern[4][6] = (1.0/ denom)* kern[4][6]; 
    kern[4][7] =  8.E+0*sp[35]*sp[31]*pow(sp[44],2)*sp[37]*svp*svm - 8.E+0*sp[35]*
      sp[29]*pow(sp[44],2)*sp[39]*svp*svm - 8.E+0*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp
      *svm + 8.E+0*pow(sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm;
 
    kern[4][7] = (1.0/ denom)* kern[4][7]; 
    kern[4][8] =  8.E+0*sp[35]*sp[43]*sp[31]*pow(sp[44],2)*ssp*svm + 8.E+0*sp[35]*sp[43]
      *sp[29]*pow(sp[44],2)*svp*ssm - 8.E+0*sp[35]*pow(sp[43],2)*sp[44]*sp[39]*ssp*svm
       - 8.E+0*sp[35]*pow(sp[43],2)*sp[44]*sp[37]*svp*ssm - 8.E+0*pow(sp[43],3)*sp[31]*
      sp[44]*ssp*svm - 8.E+0*pow(sp[43],3)*sp[29]*sp[44]*svp*ssm + 8.E+0*pow(
      sp[43],4)*sp[39]*ssp*svm + 8.E+0*pow(sp[43],4)*sp[37]*svp*ssm;
 
    kern[4][8] = (1.0/ denom)* kern[4][8]; 
    kern[4][9] =   - 8.E+0*sp[35]*sp[43]*sp[31]*pow(sp[44],2)*sp[37]*svp*svm - 8.E+0
      *sp[35]*sp[43]*sp[29]*pow(sp[44],2)*sp[39]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]
      *sp[39]*sp[37]*svp*svm + 8.E+0*pow(sp[43],3)*sp[31]*sp[44]*sp[37]*svp*svm + 
      8.E+0*pow(sp[43],3)*sp[29]*sp[44]*sp[39]*svp*svm - 1.6E+1*pow(sp[43],4)*sp[39]*
      sp[37]*svp*svm;
 
    kern[4][9] = (1.0/ denom)* kern[4][9]; 
    kern[4][10] =  8.E+0*sp[35]*pow(sp[43],2)*sp[44]*sp[39]*sp[37]*svp*svm - 8.E+0*
      sp[35]*pow(sp[43],2)*pow(sp[44],2)*sp[7]*svp*svm + 2.4E+1*pow(sp[43],2)*sp[31]*
      sp[29]*pow(sp[44],2)*svp*svm - 2.4E+1*pow(sp[43],3)*sp[31]*sp[44]*sp[37]*svp*svm
       - 2.4E+1*pow(sp[43],3)*sp[29]*sp[44]*sp[39]*svp*svm + 8.E+0*pow(sp[43],4)*sp[44]
      *sp[7]*svp*svm + 1.6E+1*pow(sp[43],4)*sp[39]*sp[37]*svp*svm;
 
    kern[4][10] = (1.0/ denom)* kern[4][10]; 
    kern[4][11] =   0;
 
    kern[4][11] = (1.0/ denom)* kern[4][11]; 

    denom =  - 3.2E+1*sp[35]*pow(sp[43],3)*sp[44] + 1.6E+1*pow(sp[35],2)*sp[43]*pow(
      sp[44],2) + 1.6E+1*pow(sp[43],5);

    kern[5][0] =   0;
 
    kern[5][0] = (1.0/ denom)* kern[5][0]; 
    kern[5][1] =   0;
 
    kern[5][1] = (1.0/ denom)* kern[5][1]; 
    kern[5][2] =   0;
 
    kern[5][2] = (1.0/ denom)* kern[5][2]; 
    kern[5][3] =   0;
 
    kern[5][3] = (1.0/ denom)* kern[5][3]; 
    kern[5][4] =  1.6E+1*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm - 1.6E+1*sp[35]*sp[43]*sp[29]
      *sp[44]*svp*ssm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm - 1.6E+1*sp[35]*
      pow(sp[43],2)*sp[37]*svp*ssm - 1.6E+1*pow(sp[35],2)*sp[44]*sp[39]*ssp*svm + 
      1.6E+1*pow(sp[35],2)*sp[44]*sp[37]*svp*ssm - 1.6E+1*pow(sp[43],3)*sp[31]*ssp*
      svm + 1.6E+1*pow(sp[43],3)*sp[29]*svp*ssm;
 
    kern[5][4] = (1.0/ denom)* kern[5][4]; 
    kern[5][5] =   - 1.6E+1*sp[35]*sp[43]*sp[31]*sp[29]*pow(sp[44],2)*svp*svm + 
      1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm + 1.6E+1*sp[35]*pow(
      sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],3)*sp[44]*sp[7]*
      svp*svm - 3.2E+1*sp[35]*pow(sp[43],3)*sp[44]*ssp*ssm - 6.4E+1*sp[35]*pow(
      sp[43],3)*sp[39]*sp[37]*svp*svm + 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[39]*sp[37]*
      svp*svm + 1.6E+1*pow(sp[35],2)*sp[43]*pow(sp[44],2)*ssp*ssm - 3.2E+1*pow(
      sp[43],3)*sp[31]*sp[29]*sp[44]*svp*svm + 3.2E+1*pow(sp[43],4)*sp[31]*sp[37]*svp*
      svm + 3.2E+1*pow(sp[43],4)*sp[29]*sp[39]*svp*svm - 1.6E+1*pow(sp[43],5)*
      sp[7]*svp*svm + 1.6E+1*pow(sp[43],5)*ssp*ssm;
 
    kern[5][5] = (1.0/ denom)* kern[5][5]; 
    kern[5][6] =  1.6E+1*sp[35]*sp[43]*sp[44]*sp[7]*svp*svm - 4.8E+1*sp[35]*sp[43]*
      sp[39]*sp[37]*svp*svm + 1.6E+1*sp[35]*sp[31]*sp[44]*sp[37]*svp*svm + 1.6E+1*sp[35]
      *sp[29]*sp[44]*sp[39]*svp*svm - 4.8E+1*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm + 
      3.2E+1*pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 3.2E+1*pow(sp[43],2)*sp[29]*sp[39]
      *svp*svm - 1.6E+1*pow(sp[43],3)*sp[7]*svp*svm;
 
    kern[5][6] = (1.0/ denom)* kern[5][6]; 
    kern[5][7] =  1.6E+1*sp[35]*sp[43]*sp[44]*sp[39]*ssp*svm + 1.6E+1*sp[35]*sp[43]*sp[44]*
      sp[37]*svp*ssm - 1.6E+1*sp[35]*sp[31]*pow(sp[44],2)*ssp*svm - 1.6E+1*sp[35]*
      sp[29]*pow(sp[44],2)*svp*ssm + 1.6E+1*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm + 
      1.6E+1*pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 1.6E+1*pow(sp[43],3)*sp[39]*ssp*
      svm - 1.6E+1*pow(sp[43],3)*sp[37]*svp*ssm;
 
    kern[5][7] = (1.0/ denom)* kern[5][7]; 
    kern[5][8] =  1.6E+1*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*sp[35]*sp[43]
      *sp[29]*sp[44]*sp[39]*svp*svm - 1.6E+1*pow(sp[43],3)*sp[31]*sp[37]*svp*svm + 
      1.6E+1*pow(sp[43],3)*sp[29]*sp[39]*svp*svm;
 
    kern[5][8] = (1.0/ denom)* kern[5][8]; 
    kern[5][9] =  1.6E+1*sp[35]*sp[43]*sp[31]*pow(sp[44],2)*ssp*svm - 1.6E+1*sp[35]*
      sp[43]*sp[29]*pow(sp[44],2)*svp*ssm - 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]*sp[39]*ssp*
      svm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]*sp[37]*svp*ssm - 1.6E+1*pow(sp[43],3)*
      sp[31]*sp[44]*ssp*svm + 1.6E+1*pow(sp[43],3)*sp[29]*sp[44]*svp*ssm + 1.6E+1*
      pow(sp[43],4)*sp[39]*ssp*svm - 1.6E+1*pow(sp[43],4)*sp[37]*svp*ssm;
 
    kern[5][9] = (1.0/ denom)* kern[5][9]; 
    kern[5][10] =   0;
 
    kern[5][10] = (1.0/ denom)* kern[5][10]; 
    kern[5][11] =   - 1.6E+1*sp[35]*sp[43]*sp[44]*sp[39]*sp[37]*svp*svm + 1.6E+1*sp[35]
      *sp[43]*pow(sp[44],2)*sp[7]*svp*svm - 4.8E+1*sp[43]*sp[31]*sp[29]*pow(sp[44],2)*
      svp*svm + 4.8E+1*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm + 4.8E+1*pow(
      sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm - 1.6E+1*pow(sp[43],3)*sp[44]*sp[7]*svp*
      svm - 3.2E+1*pow(sp[43],3)*sp[39]*sp[37]*svp*svm;
 
    kern[5][11] = (1.0/ denom)* kern[5][11]; 

    denom =  - 1.6E+1*sp[35]*sp[44] + 1.6E+1*pow(sp[43],2);

    kern[6][0] =   - 1.6E+1*sp[43]*sp[39]*ssp*svm + 1.6E+1*sp[43]*sp[37]*svp*ssm
       + 1.6E+1*sp[31]*sp[44]*ssp*svm - 1.6E+1*sp[29]*sp[44]*svp*ssm;
 
    kern[6][0] = (1.0/ denom)* kern[6][0]; 
    kern[6][1] =   0;
 
    kern[6][1] = (1.0/ denom)* kern[6][1]; 
    kern[6][2] =   0;
 
    kern[6][2] = (1.0/ denom)* kern[6][2]; 
    kern[6][3] =   0;
 
    kern[6][3] = (1.0/ denom)* kern[6][3]; 
    kern[6][4] =   - 1.6E+1*sp[35]*sp[43]*sp[39]*ssp*svm + 1.6E+1*sp[35]*sp[43]*sp[37]*
      svp*ssm + 1.6E+1*sp[35]*sp[31]*sp[44]*ssp*svm - 1.6E+1*sp[35]*sp[29]*sp[44]*svp*
      ssm;
 
    kern[6][4] = (1.0/ denom)* kern[6][4]; 
    kern[6][5] =   - 1.6E+1*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*sp[35]*
      sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 3.2E+1*sp[35]*pow(sp[43],2)*sp[39]*sp[37]*svp*
      svm;
 
    kern[6][5] = (1.0/ denom)* kern[6][5]; 
    kern[6][6] =   - 1.6E+1*sp[35]*sp[44]*ssp*ssm + 1.6E+1*sp[35]*sp[39]*sp[37]*svp*
      svm + 1.6E+1*pow(sp[43],2)*ssp*ssm - 1.6E+1*sp[31]*sp[29]*sp[44]*svp*svm;
 
    kern[6][6] = (1.0/ denom)* kern[6][6]; 
    kern[6][7] =   - 1.6E+1*sp[35]*sp[44]*sp[39]*ssp*svm - 1.6E+1*sp[35]*sp[44]*sp[37]*
      svp*ssm + 1.6E+1*pow(sp[43],2)*sp[39]*ssp*svm + 1.6E+1*pow(sp[43],2)*sp[37]
      *svp*ssm;
 
    kern[6][7] = (1.0/ denom)* kern[6][7]; 
    kern[6][8] =   0;
 
    kern[6][8] = (1.0/ denom)* kern[6][8]; 
    kern[6][9] =   0;
 
    kern[6][9] = (1.0/ denom)* kern[6][9]; 
    kern[6][10] =  1.6E+1*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm - 1.6E+1*pow(
      sp[43],2)*sp[29]*sp[44]*svp*ssm - 1.6E+1*pow(sp[43],3)*sp[39]*ssp*svm + 1.6E+1
      *pow(sp[43],3)*sp[37]*svp*ssm;
 
    kern[6][10] = (1.0/ denom)* kern[6][10]; 
    kern[6][11] =   - 1.6E+1*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*sp[43]*
      sp[29]*sp[44]*sp[39]*svp*svm + 3.2E+1*pow(sp[43],2)*sp[39]*sp[37]*svp*svm;
 
    kern[6][11] = (1.0/ denom)* kern[6][11]; 

    denom =  - 4.8E+1*sp[35]*sp[44] + 4.8E+1*pow(sp[43],2);

    kern[7][0] =   - 4.8E+1*sp[31]*sp[37]*svp*svm + 4.8E+1*sp[29]*sp[39]*svp*
      svm;
 
    kern[7][0] = (1.0/ denom)* kern[7][0]; 
    kern[7][1] =   0;
 
    kern[7][1] = (1.0/ denom)* kern[7][1]; 
    kern[7][2] =   0;
 
    kern[7][2] = (1.0/ denom)* kern[7][2]; 
    kern[7][3] =   0;
 
    kern[7][3] = (1.0/ denom)* kern[7][3]; 
    kern[7][4] =   - 4.8E+1*sp[35]*sp[31]*sp[37]*svp*svm + 4.8E+1*sp[35]*sp[29]*
      sp[39]*svp*svm;
 
    kern[7][4] = (1.0/ denom)* kern[7][4]; 
    kern[7][5] =   - 4.8E+1*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm - 4.8E+1*sp[35]*sp[43]*
      sp[29]*sp[44]*svp*ssm + 4.8E+1*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm + 4.8E+1*
      sp[35]*pow(sp[43],2)*sp[37]*svp*ssm;
 
    kern[7][5] = (1.0/ denom)* kern[7][5]; 
    kern[7][6] =  4.8E+1*sp[35]*sp[39]*ssp*svm + 4.8E+1*sp[35]*sp[37]*svp*ssm - 
      4.8E+1*sp[43]*sp[31]*ssp*svm - 4.8E+1*sp[43]*sp[29]*svp*ssm;
 
    kern[7][6] = (1.0/ denom)* kern[7][6]; 
    kern[7][7] =   - 4.8E+1*sp[35]*sp[44]*ssp*ssm + 4.8E+1*sp[35]*sp[39]*sp[37]*svp*
      svm - 4.8E+1*sp[43]*sp[31]*sp[37]*svp*svm - 4.8E+1*sp[43]*sp[29]*sp[39]*svp*svm
       + 4.8E+1*pow(sp[43],2)*ssp*ssm + 4.8E+1*sp[31]*sp[29]*sp[44]*svp*svm;
 
    kern[7][7] = (1.0/ denom)* kern[7][7]; 
    kern[7][8] =   0;
 
    kern[7][8] = (1.0/ denom)* kern[7][8]; 
    kern[7][9] =   0;
 
    kern[7][9] = (1.0/ denom)* kern[7][9]; 
    kern[7][10] =   - 4.8E+1*pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 4.8E+1*pow(
      sp[43],2)*sp[29]*sp[39]*svp*svm;
 
    kern[7][10] = (1.0/ denom)* kern[7][10]; 
    kern[7][11] =   - 4.8E+1*sp[43]*sp[31]*sp[44]*ssp*svm - 4.8E+1*sp[43]*sp[29]*sp[44]
      *svp*ssm + 4.8E+1*pow(sp[43],2)*sp[39]*ssp*svm + 4.8E+1*pow(sp[43],2)*
      sp[37]*svp*ssm;
 
    kern[7][11] = (1.0/ denom)* kern[7][11]; 

    denom =  - 4.E+0*sp[35]*sp[43]*sp[44] + 4.E+0*pow(sp[43],3);

    kern[8][0] =  4.E+0*sp[43]*sp[39]*ssp*svm + 4.E+0*sp[43]*sp[37]*svp*ssm - 
      4.E+0*sp[31]*sp[44]*ssp*svm - 4.E+0*sp[29]*sp[44]*svp*ssm;
 
    kern[8][0] = (1.0/ denom)* kern[8][0]; 
    kern[8][1] =   0;
 
    kern[8][1] = (1.0/ denom)* kern[8][1]; 
    kern[8][2] =   0;
 
    kern[8][2] = (1.0/ denom)* kern[8][2]; 
    kern[8][3] =   0;
 
    kern[8][3] = (1.0/ denom)* kern[8][3]; 
    kern[8][4] =  4.E+0*sp[35]*sp[43]*sp[39]*ssp*svm + 4.E+0*sp[35]*sp[43]*sp[37]*svp*
      ssm - 4.E+0*pow(sp[43],2)*sp[31]*ssp*svm - 4.E+0*pow(sp[43],2)*sp[29]*svp*
      ssm;
 
    kern[8][4] = (1.0/ denom)* kern[8][4]; 
    kern[8][5] =  4.E+0*pow(sp[43],3)*sp[31]*sp[37]*svp*svm - 4.E+0*pow(sp[43],3)
      *sp[29]*sp[39]*svp*svm;
 
    kern[8][5] = (1.0/ denom)* kern[8][5]; 
    kern[8][6] =  4.E+0*sp[43]*sp[31]*sp[37]*svp*svm - 4.E+0*sp[43]*sp[29]*sp[39]*svp
      *svm;
 
    kern[8][6] = (1.0/ denom)* kern[8][6]; 
    kern[8][7] =   0;
 
    kern[8][7] = (1.0/ denom)* kern[8][7]; 
    kern[8][8] =  4.E+0*sp[35]*sp[43]*sp[44]*sp[7]*svp*svm - 4.E+0*sp[35]*sp[43]*sp[44]*
      ssp*ssm - 4.E+0*pow(sp[43],3)*sp[7]*svp*svm + 4.E+0*pow(sp[43],3)*ssp*
      ssm;
 
    kern[8][8] = (1.0/ denom)* kern[8][8]; 
    kern[8][9] =  4.E+0*sp[35]*sp[43]*sp[44]*sp[39]*ssp*svm + 4.E+0*sp[35]*sp[43]*sp[44]*
      sp[37]*svp*ssm - 4.E+0*pow(sp[43],3)*sp[39]*ssp*svm - 4.E+0*pow(sp[43],3)*
      sp[37]*svp*ssm;
 
    kern[8][9] = (1.0/ denom)* kern[8][9]; 
    kern[8][10] =   - 4.E+0*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm - 4.E+0*pow(
      sp[43],2)*sp[29]*sp[44]*svp*ssm + 4.E+0*pow(sp[43],3)*sp[39]*ssp*svm + 4.E+0*
      pow(sp[43],3)*sp[37]*svp*ssm;
 
    kern[8][10] = (1.0/ denom)* kern[8][10]; 
    kern[8][11] =  4.E+0*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 4.E+0*sp[43]*sp[29]*sp[44]
      *sp[39]*svp*svm;
 
    kern[8][11] = (1.0/ denom)* kern[8][11]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44] + 8.E+0*pow(sp[35],2)*sp[43]*pow(
      sp[44],2) + 8.E+0*pow(sp[43],5);

    kern[9][0] =  8.E+0*sp[35]*sp[43]*sp[44]*sp[7]*svp*svm - 2.4E+1*sp[35]*sp[43]*sp[39]
      *sp[37]*svp*svm + 8.E+0*sp[35]*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*sp[35]*sp[29]*
      sp[44]*sp[39]*svp*svm - 2.4E+1*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm + 1.6E+1*pow(
      sp[43],2)*sp[31]*sp[37]*svp*svm + 1.6E+1*pow(sp[43],2)*sp[29]*sp[39]*svp*svm - 
      8.E+0*pow(sp[43],3)*sp[7]*svp*svm;
 
    kern[9][0] = (1.0/ denom)* kern[9][0]; 
    kern[9][1] =   0;
 
    kern[9][1] = (1.0/ denom)* kern[9][1]; 
    kern[9][2] =   0;
 
    kern[9][2] = (1.0/ denom)* kern[9][2]; 
    kern[9][3] =   0;
 
    kern[9][3] = (1.0/ denom)* kern[9][3]; 
    kern[9][4] =   - 8.E+0*sp[35]*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm + 2.4E+1*sp[35]*
      pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 2.4E+1*sp[35]*pow(sp[43],2)*sp[29]*sp[39]*
      svp*svm - 8.E+0*sp[35]*pow(sp[43],3)*sp[7]*svp*svm + 8.E+0*pow(sp[35],2)*
      sp[43]*sp[44]*sp[7]*svp*svm - 2.4E+1*pow(sp[35],2)*sp[43]*sp[39]*sp[37]*svp*svm
       - 1.6E+1*pow(sp[43],3)*sp[31]*sp[29]*svp*svm;
 
    kern[9][4] = (1.0/ denom)* kern[9][4]; 
    kern[9][5] =  8.E+0*sp[35]*pow(sp[43],3)*sp[31]*sp[44]*ssp*svm - 8.E+0*sp[35]*
      pow(sp[43],3)*sp[29]*sp[44]*svp*ssm + 8.E+0*sp[35]*pow(sp[43],4)*sp[39]*ssp*svm
       - 8.E+0*sp[35]*pow(sp[43],4)*sp[37]*svp*ssm - 8.E+0*pow(sp[35],2)*pow(
      sp[43],2)*sp[44]*sp[39]*ssp*svm + 8.E+0*pow(sp[35],2)*pow(sp[43],2)*sp[44]*sp[37]*
      svp*ssm - 8.E+0*pow(sp[43],5)*sp[31]*ssp*svm + 8.E+0*pow(sp[43],5)*sp[29]*
      svp*ssm;
 
    kern[9][5] = (1.0/ denom)* kern[9][5]; 
    kern[9][6] =   0;
 
    kern[9][6] = (1.0/ denom)* kern[9][6]; 
    kern[9][7] =   - 8.E+0*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*sp[35]*
      sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 8.E+0*pow(sp[43],3)*sp[31]*sp[37]*svp*svm - 
      8.E+0*pow(sp[43],3)*sp[29]*sp[39]*svp*svm;
 
    kern[9][7] = (1.0/ denom)* kern[9][7]; 
    kern[9][8] =   - 8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm - 8.E+0*sp[35]*
      pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 8.E+0*sp[35]*pow(sp[43],3)*sp[39]*ssp*svm
       - 8.E+0*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*
      sp[39]*ssp*svm + 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(
      sp[43],4)*sp[31]*ssp*svm + 8.E+0*pow(sp[43],4)*sp[29]*svp*ssm;
 
    kern[9][8] = (1.0/ denom)* kern[9][8]; 
    kern[9][9] =  8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*
      sp[35]*pow(sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44]*
      sp[7]*svp*svm - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44]*ssp*ssm + 1.6E+1*sp[35]*
      pow(sp[43],3)*sp[39]*sp[37]*svp*svm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[39]*
      sp[37]*svp*svm + 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[44],2)*sp[7]*svp*svm + 
      8.E+0*pow(sp[35],2)*sp[43]*pow(sp[44],2)*ssp*ssm - 8.E+0*pow(sp[43],4)*sp[31]*
      sp[37]*svp*svm - 8.E+0*pow(sp[43],4)*sp[29]*sp[39]*svp*svm + 8.E+0*pow(
      sp[43],5)*sp[7]*svp*svm + 8.E+0*pow(sp[43],5)*ssp*ssm;
 
    kern[9][9] = (1.0/ denom)* kern[9][9]; 
    kern[9][10] =  8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*
      sp[35]*pow(sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm + 8.E+0*sp[35]*pow(sp[43],3)*sp[44]*
      sp[7]*svp*svm - 2.4E+1*sp[35]*pow(sp[43],3)*sp[39]*sp[37]*svp*svm - 2.4E+1*
      pow(sp[43],3)*sp[31]*sp[29]*sp[44]*svp*svm + 1.6E+1*pow(sp[43],4)*sp[31]*sp[37]*
      svp*svm + 1.6E+1*pow(sp[43],4)*sp[29]*sp[39]*svp*svm - 8.E+0*pow(sp[43],5)*
      sp[7]*svp*svm;
 
    kern[9][10] = (1.0/ denom)* kern[9][10]; 
    kern[9][11] =  8.E+0*sp[35]*sp[43]*sp[31]*pow(sp[44],2)*ssp*svm - 8.E+0*sp[35]*
      sp[43]*sp[29]*pow(sp[44],2)*svp*ssm - 8.E+0*sp[35]*pow(sp[43],2)*sp[44]*sp[39]*ssp*
      svm + 8.E+0*sp[35]*pow(sp[43],2)*sp[44]*sp[37]*svp*ssm - 8.E+0*pow(sp[43],3)*
      sp[31]*sp[44]*ssp*svm + 8.E+0*pow(sp[43],3)*sp[29]*sp[44]*svp*ssm + 8.E+0*pow(
      sp[43],4)*sp[39]*ssp*svm - 8.E+0*pow(sp[43],4)*sp[37]*svp*ssm;
 
    kern[9][11] = (1.0/ denom)* kern[9][11]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44] + 8.E+0*pow(sp[35],2)*sp[43]*pow(
      sp[44],2) + 8.E+0*pow(sp[43],5);

    kern[10][0] =  8.E+0*sp[35]*sp[43]*sp[44]*sp[7]*svp*svm - 2.4E+1*sp[35]*sp[43]*
      sp[39]*sp[37]*svp*svm + 8.E+0*sp[35]*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*sp[35]*
      sp[29]*sp[44]*sp[39]*svp*svm - 2.4E+1*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm + 1.6E+1
      *pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 1.6E+1*pow(sp[43],2)*sp[29]*sp[39]*svp*
      svm - 8.E+0*pow(sp[43],3)*sp[7]*svp*svm;
 
    kern[10][0] = (1.0/ denom)* kern[10][0]; 
    kern[10][1] =   - 1.6E+1*sp[35]*sp[43]*sp[44]*sp[39]*sp[37]*svp*svm + 8.E+0*sp[35]*
      sp[31]*pow(sp[44],2)*sp[37]*svp*svm + 8.E+0*sp[35]*sp[29]*pow(sp[44],2)*sp[39]*svp
      *svm - 8.E+0*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm - 8.E+0*pow(sp[43],2)*
      sp[29]*sp[44]*sp[39]*svp*svm + 1.6E+1*pow(sp[43],3)*sp[39]*sp[37]*svp*svm;
 
    kern[10][1] = (1.0/ denom)* kern[10][1]; 
    kern[10][2] =   - 8.E+0*sp[35]*sp[43]*sp[44]*sp[39]*ssp*svm - 8.E+0*sp[35]*sp[43]*
      sp[44]*sp[37]*svp*ssm + 8.E+0*sp[35]*sp[31]*pow(sp[44],2)*ssp*svm + 8.E+0*sp[35]*
      sp[29]*pow(sp[44],2)*svp*ssm - 8.E+0*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm - 
      8.E+0*pow(sp[43],2)*sp[29]*sp[44]*svp*ssm + 8.E+0*pow(sp[43],3)*sp[39]*ssp*svm
       + 8.E+0*pow(sp[43],3)*sp[37]*svp*ssm;
 
    kern[10][2] = (1.0/ denom)* kern[10][2]; 
    kern[10][3] =   - 3.2E+1*sp[35]*pow(sp[43],2)*sp[44]*sp[39]*ssp*svm + 3.2E+1*
      sp[35]*pow(sp[43],2)*sp[44]*sp[37]*svp*ssm + 1.6E+1*pow(sp[35],2)*pow(sp[44],2)*
      sp[39]*ssp*svm - 1.6E+1*pow(sp[35],2)*pow(sp[44],2)*sp[37]*svp*ssm + 1.6E+1
      *pow(sp[43],4)*sp[39]*ssp*svm - 1.6E+1*pow(sp[43],4)*sp[37]*svp*ssm;
 
    kern[10][3] = (1.0/ denom)* kern[10][3]; 
    kern[10][4] =   - 2.4E+1*sp[35]*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm + 1.6E+1*sp[35]
      *pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[29]*sp[39]*
      svp*svm - 8.E+0*sp[35]*pow(sp[43],3)*sp[7]*svp*svm + 8.E+0*pow(sp[35],2)*
      sp[43]*sp[44]*sp[7]*svp*svm - 2.4E+1*pow(sp[35],2)*sp[43]*sp[39]*sp[37]*svp*svm
       + 8.E+0*pow(sp[35],2)*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*pow(sp[35],2)*sp[29]
      *sp[44]*sp[39]*svp*svm;
 
    kern[10][4] = (1.0/ denom)* kern[10][4]; 
    kern[10][5] =   0;
 
    kern[10][5] = (1.0/ denom)* kern[10][5]; 
    kern[10][6] =   - 8.E+0*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm + 8.E+0*sp[35]*sp[43]*
      sp[29]*sp[44]*svp*ssm - 8.E+0*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm + 8.E+0*sp[35]*
      pow(sp[43],2)*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*sp[44]*sp[39]*ssp*svm - 
      8.E+0*pow(sp[35],2)*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(sp[43],3)*sp[31]*ssp*svm
       - 8.E+0*pow(sp[43],3)*sp[29]*svp*ssm;
 
    kern[10][6] = (1.0/ denom)* kern[10][6]; 
    kern[10][7] =   - 8.E+0*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*sp[35]*
      sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 8.E+0*pow(sp[43],3)*sp[31]*sp[37]*svp*svm - 
      8.E+0*pow(sp[43],3)*sp[29]*sp[39]*svp*svm;
 
    kern[10][7] = (1.0/ denom)* kern[10][7]; 
    kern[10][8] =  8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm + 8.E+0*sp[35]*
      pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 8.E+0*sp[35]*pow(sp[43],3)*sp[39]*ssp*svm
       - 8.E+0*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*
      sp[39]*ssp*svm + 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm - 8.E+0*pow(
      sp[35],2)*sp[31]*pow(sp[44],2)*ssp*svm - 8.E+0*pow(sp[35],2)*sp[29]*pow(sp[44],2)
      *svp*ssm;
 
    kern[10][8] = (1.0/ denom)* kern[10][8]; 
    kern[10][9] =   - 8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm - 
      8.E+0*sp[35]*pow(sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm + 1.6E+1*sp[35]*pow(
      sp[43],3)*sp[39]*sp[37]*svp*svm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[39]*sp[37]*
      svp*svm + 8.E+0*pow(sp[35],2)*sp[31]*pow(sp[44],2)*sp[37]*svp*svm + 8.E+0*
      pow(sp[35],2)*sp[29]*pow(sp[44],2)*sp[39]*svp*svm;
 
    kern[10][9] = (1.0/ denom)* kern[10][9]; 
    kern[10][10] =   - 1.6E+1*sp[35]*sp[43]*sp[31]*sp[29]*pow(sp[44],2)*svp*svm + 
      1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm + 1.6E+1*sp[35]*pow(
      sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm - 8.E+0*sp[35]*pow(sp[43],3)*sp[44]*sp[7]*svp
      *svm - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44]*ssp*ssm - 2.4E+1*sp[35]*pow(sp[43],3)*
      sp[39]*sp[37]*svp*svm + 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[44],2)*sp[7]*svp*svm
       + 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[44],2)*ssp*ssm - 8.E+0*pow(sp[43],3)*
      sp[31]*sp[29]*sp[44]*svp*svm + 8.E+0*pow(sp[43],4)*sp[31]*sp[37]*svp*svm + 
      8.E+0*pow(sp[43],4)*sp[29]*sp[39]*svp*svm + 8.E+0*pow(sp[43],5)*ssp*ssm;
 
    kern[10][10] = (1.0/ denom)* kern[10][10]; 
    kern[10][11] =   - 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]*sp[39]*ssp*svm + 1.6E+1*
      sp[35]*pow(sp[43],2)*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*pow(sp[44],2)*
      sp[39]*ssp*svm - 8.E+0*pow(sp[35],2)*pow(sp[44],2)*sp[37]*svp*ssm + 8.E+0*
      pow(sp[43],4)*sp[39]*ssp*svm - 8.E+0*pow(sp[43],4)*sp[37]*svp*ssm;
 
    kern[10][11] = (1.0/ denom)* kern[10][11]; 

    denom =  - 3.2E+1*sp[35]*pow(sp[43],3)*sp[44] + 1.6E+1*pow(sp[35],2)*sp[43]*pow(
      sp[44],2) + 1.6E+1*pow(sp[43],5);

    kern[11][0] =  1.6E+1*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm - 1.6E+1*sp[35]*sp[43]*
      sp[29]*sp[44]*svp*ssm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm - 1.6E+1*
      sp[35]*pow(sp[43],2)*sp[37]*svp*ssm - 1.6E+1*pow(sp[35],2)*sp[44]*sp[39]*ssp*svm
       + 1.6E+1*pow(sp[35],2)*sp[44]*sp[37]*svp*ssm - 1.6E+1*pow(sp[43],3)*sp[31]*
      ssp*svm + 1.6E+1*pow(sp[43],3)*sp[29]*svp*ssm;
 
    kern[11][0] = (1.0/ denom)* kern[11][0]; 
    kern[11][1] =   0;
 
    kern[11][1] = (1.0/ denom)* kern[11][1]; 
    kern[11][2] =   0;
 
    kern[11][2] = (1.0/ denom)* kern[11][2]; 
    kern[11][3] =   0;
 
    kern[11][3] = (1.0/ denom)* kern[11][3]; 
    kern[11][4] =   0;
 
    kern[11][4] = (1.0/ denom)* kern[11][4]; 
    kern[11][5] =  4.8E+1*sp[35]*pow(sp[43],3)*sp[31]*sp[29]*sp[44]*svp*svm - 3.2E+1
      *sp[35]*pow(sp[43],4)*sp[31]*sp[37]*svp*svm - 3.2E+1*sp[35]*pow(sp[43],4)*sp[29]*
      sp[39]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],5)*sp[7]*svp*svm - 1.6E+1*pow(
      sp[35],2)*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*pow(sp[35],2)*pow(
      sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm - 1.6E+1*pow(sp[35],2)*pow(sp[43],3)*sp[44]*
      sp[7]*svp*svm + 4.8E+1*pow(sp[35],2)*pow(sp[43],3)*sp[39]*sp[37]*svp*svm;
 
    kern[11][5] = (1.0/ denom)* kern[11][5]; 
    kern[11][6] =  1.6E+1*sp[35]*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm - 4.8E+1*sp[35]*
      pow(sp[43],2)*sp[31]*sp[37]*svp*svm - 4.8E+1*sp[35]*pow(sp[43],2)*sp[29]*sp[39]*
      svp*svm + 1.6E+1*sp[35]*pow(sp[43],3)*sp[7]*svp*svm - 1.6E+1*pow(sp[35],2)
      *sp[43]*sp[44]*sp[7]*svp*svm + 4.8E+1*pow(sp[35],2)*sp[43]*sp[39]*sp[37]*svp*svm
       + 3.2E+1*pow(sp[43],3)*sp[31]*sp[29]*svp*svm;
 
    kern[11][6] = (1.0/ denom)* kern[11][6]; 
    kern[11][7] =  1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm + 1.6E+1*sp[35]*
      pow(sp[43],2)*sp[29]*sp[44]*svp*ssm + 1.6E+1*sp[35]*pow(sp[43],3)*sp[39]*ssp*svm
       + 1.6E+1*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]
      *sp[39]*ssp*svm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm - 1.6E+1*
      pow(sp[43],4)*sp[31]*ssp*svm - 1.6E+1*pow(sp[43],4)*sp[29]*svp*ssm;
 
    kern[11][7] = (1.0/ denom)* kern[11][7]; 
    kern[11][8] =  1.6E+1*sp[35]*pow(sp[43],3)*sp[31]*sp[37]*svp*svm - 1.6E+1*sp[35]
      *pow(sp[43],3)*sp[29]*sp[39]*svp*svm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[31]*sp[44]*
      sp[37]*svp*svm + 1.6E+1*pow(sp[35],2)*sp[43]*sp[29]*sp[44]*sp[39]*svp*svm;
 
    kern[11][8] = (1.0/ denom)* kern[11][8]; 
    kern[11][9] =  1.6E+1*sp[35]*pow(sp[43],3)*sp[31]*sp[44]*ssp*svm - 1.6E+1*sp[35]*
      pow(sp[43],3)*sp[29]*sp[44]*svp*ssm - 1.6E+1*sp[35]*pow(sp[43],4)*sp[39]*ssp*svm
       + 1.6E+1*sp[35]*pow(sp[43],4)*sp[37]*svp*ssm - 1.6E+1*pow(sp[35],2)*sp[43]*
      sp[31]*pow(sp[44],2)*ssp*svm + 1.6E+1*pow(sp[35],2)*sp[43]*sp[29]*pow(sp[44],2)*
      svp*ssm + 1.6E+1*pow(sp[35],2)*pow(sp[43],2)*sp[44]*sp[39]*ssp*svm - 1.6E+1*
      pow(sp[35],2)*pow(sp[43],2)*sp[44]*sp[37]*svp*ssm;
 
    kern[11][9] = (1.0/ denom)* kern[11][9]; 
    kern[11][10] =  1.6E+1*sp[35]*pow(sp[43],3)*sp[31]*sp[44]*ssp*svm - 1.6E+1*sp[35]
      *pow(sp[43],3)*sp[29]*sp[44]*svp*ssm + 1.6E+1*sp[35]*pow(sp[43],4)*sp[39]*ssp*svm
       - 1.6E+1*sp[35]*pow(sp[43],4)*sp[37]*svp*ssm - 1.6E+1*pow(sp[35],2)*pow(
      sp[43],2)*sp[44]*sp[39]*ssp*svm + 1.6E+1*pow(sp[35],2)*pow(sp[43],2)*sp[44]*sp[37]*
      svp*ssm - 1.6E+1*pow(sp[43],5)*sp[31]*ssp*svm + 1.6E+1*pow(sp[43],5)*sp[29]
      *svp*ssm;
 
    kern[11][10] = (1.0/ denom)* kern[11][10]; 
    kern[11][11] =  3.2E+1*sp[35]*sp[43]*sp[31]*sp[29]*pow(sp[44],2)*svp*svm - 
      4.8E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm - 4.8E+1*sp[35]*pow(
      sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],3)*sp[44]*sp[7]*
      svp*svm - 3.2E+1*sp[35]*pow(sp[43],3)*sp[44]*ssp*ssm + 1.6E+1*sp[35]*pow(
      sp[43],3)*sp[39]*sp[37]*svp*svm + 3.2E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[39]*sp[37]*
      svp*svm - 1.6E+1*pow(sp[35],2)*sp[43]*pow(sp[44],2)*sp[7]*svp*svm + 1.6E+1
      *pow(sp[35],2)*sp[43]*pow(sp[44],2)*ssp*ssm + 1.6E+1*pow(sp[43],3)*sp[31]*sp[29]*
      sp[44]*svp*svm + 1.6E+1*pow(sp[43],5)*ssp*ssm;
 
    kern[11][11] = (1.0/ denom)* kern[11][11]; 


}


void kernelL_qphv(matCdoub& kern,
              const ArrayScalarProducts& sp,
              const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){


    // local variables
    array<Cdoub, 93> w;
    Cdoub denom;


    w[1]=sp[35];
    w[2]=sp[27];
    w[3]=sp[34];
    w[4]=sp[43];
    w[5]=sp[44];
   w[6]=w[2]*ang[0];
   w[7]=3.E+0*w[6];
   w[8]=w[5]*ang[0];
   w[9]=w[8] - ang[1];
   w[10]=w[7] + w[9];
   w[10]=w[10]*w[4];
   w[11]=2.E+0*ang[1];
   w[12]=w[11] - w[8];
   w[13]=w[12] - w[6];
   w[14]=w[13]*w[3];
   w[10]=w[10] + w[14];
   w[10]=w[10]*w[3];
   w[14]=2.E+0*w[6];
   w[15]=pow(w[4],2);
   w[16]=w[14]*w[15];
   w[10]=w[10] - w[16];
   w[16]=w[1]*w[2];
   w[17]=ang[1]*w[16];
   w[17]=w[17] - w[10];
   w[17]=4.E+0*w[17];
   w[18]=3.E+0*ang[1];
   w[19]=w[18] - w[6];
   w[20]=w[19]*w[2];
   w[21]=2.E+0*ang[2];
   w[22]=w[8] + ang[1];
   w[23]=w[22]*w[5];
   w[24]=w[21] - w[23];
   w[20]=w[20] - w[24];
   w[25]=w[3]*w[4];
   w[26]=w[20]*w[25];
   w[27]=w[11] - w[6];
   w[28]=w[27] + w[8];
   w[29]=w[28]*w[15];
   w[30]=2.E+0*w[2];
   w[31]=w[29]*w[30];
   w[26]=w[31] - w[26];
   w[31]=2.E+0*w[26];
   w[32]=w[3]*ang[0];
   w[33]=2.E+0*ang[0];
   w[34]=w[33]*w[4];
   w[32]=w[32] - w[34];
   w[32]=w[32]*w[3];
   w[34]=w[15]*ang[0];
   w[32]=w[32] + w[34];
   w[35]=2.E+0*w[3];
   w[36]= - w[32]*w[35];
   w[37]=w[9] - w[6];
   w[38]=w[4] - w[3];
   w[38]=w[1]*w[37]*w[38];
   w[36]=w[36] + w[38];
   w[36]=4.E+0*w[36];
   w[38]=w[6] - w[8];
   w[39]=w[38] - ang[1];
   w[39]=w[39]*w[25];
   w[39]=w[39] + w[29];
   w[40]= - w[39]*w[35];
   w[41]=2.E+0*w[8];
   w[42]=w[41] - ang[1];
   w[43]=w[42] - w[6];
   w[43]=w[43]*w[2];
   w[43]=w[43] + w[24];
   w[44]=w[1]*w[4];
   w[45]= - w[43]*w[44];
   w[40]=w[40] + w[45];
   w[40]=2.E+0*w[40];
   w[45]=1.2E+1*ang[1];
   w[46]=w[4]*w[45];
   w[47]=w[25]*w[41];
   w[48]=w[42] + w[14];
   w[49]=w[48]*w[15];
   w[47]=w[47] - w[49];
   w[47]=w[47]*w[3];
   w[49]=pow(w[4],3);
   w[50]=w[49]*w[14];
   w[47]=w[47] + w[50];
   w[50]=w[6] - ang[1];
   w[51]=w[50] - w[41];
   w[51]=w[51]*w[2];
   w[52]=w[9]*w[5];
   w[51]=w[51] + w[52];
   w[53]= - w[51]*w[44];
   w[53]= - 2.E+0*w[47] + w[53];
   w[53]=8.E+0*w[53];
   w[12]=w[12]*w[5];
   w[12]=w[12] - ang[2];
   w[54]=2.E+0*w[42] + w[6];
   w[54]=w[2]*w[54];
   w[54]=w[54] - w[12];
   w[54]=w[3]*w[54];
   w[55]= - w[6] - w[9];
   w[56]=w[4]*w[2];
   w[57]=6.E+0*w[56];
   w[55]=w[55]*w[57];
   w[54]=w[55] + w[54];
   w[54]=w[3]*w[54];
   w[55]=pow(w[2],2);
   w[58]=6.E+0*w[55];
   w[34]=w[34]*w[58];
   w[34]=w[34] + w[54];
   w[22]=2.E+0*w[22];
   w[54]= - w[22] + w[6];
   w[54]=w[2]*w[54];
   w[54]=w[54] - w[12];
   w[54]=w[54]*w[16];
   w[34]=2.E+0*w[34] + w[54];
   w[34]=2.E+0*w[34];
   w[54]=8.E+0*ang[1];
   w[58]=3.E+0*w[8];
   w[59]=w[54] + w[58];
   w[59]=w[59]*w[5];
   w[60]=4.E+0*ang[1];
   w[61]=w[60] - w[58];
   w[62]=w[61] - w[6];
   w[62]=w[62]*w[2];
   w[63]=5.E+0*ang[2];
   w[59]= - w[63] + w[59] + w[62];
   w[59]=w[59]*w[2];
   w[62]=3.E+0*ang[2];
   w[64]=pow(w[5],2);
   w[65]=w[64]*ang[0];
   w[66]=w[62] - w[65];
   w[66]=w[66]*w[5];
   w[66]=w[66] - 2.E+0*ang[3];
   w[59]=w[59] - w[66];
   w[59]=w[59]*w[3];
   w[57]= - w[20]*w[57];
   w[57]=w[57] + w[59];
   w[57]=w[3]*w[57];
   w[67]=w[29]*w[55];
   w[57]=6.E+0*w[67] + w[57];
   w[68]=w[60] + w[58];
   w[69]=w[68] - w[6];
   w[69]=w[69]*w[2];
   w[68]=w[68]*w[5];
   w[69]=w[69] - w[68] - w[63];
   w[69]=w[69]*w[2];
   w[69]=w[69] - w[66];
   w[69]=w[69]*w[16];
   w[57]=2.E+0*w[57] + w[69];
   w[70]=w[11] + w[8];
   w[71]= - w[6] + 2.E+0*w[70];
   w[71]=w[71]*w[2];
   w[72]=w[71] - w[12];
   w[72]=w[3]*w[72];
   w[73]=4.E+0*w[56];
   w[74]= - w[9]*w[73];
   w[72]=w[74] + w[72];
   w[72]=w[3]*w[72];
   w[74]=w[55]*w[15];
   w[75]=w[33]*w[74];
   w[72]=w[75] + w[72];
   w[75]= - w[18] - w[8];
   w[75]=2.E+0*w[75] + w[6];
   w[75]=w[2]*w[75];
   w[75]=w[75] - w[12];
   w[75]=w[75]*w[16];
   w[72]=2.E+0*w[72] + w[75];
   w[72]=w[1]*w[72];
   w[75]=pow(w[3],2);
   w[76]=4.E+0*w[75];
   w[10]= - w[10]*w[76];
   w[10]=w[10] + w[72];
   w[10]=2.E+0*w[10];
   w[72]=w[20]*w[73];
   w[59]= - w[72] + w[59];
   w[59]=w[3]*w[59];
   w[59]=2.E+0*w[67] + w[59];
   w[59]=2.E+0*w[59] + w[69];
   w[59]=w[1]*w[59];
   w[26]=w[26]*w[76];
   w[26]=w[26] + w[59];
   w[59]=w[30]*w[15];
   w[67]= - w[7] + w[61];
   w[67]=w[67]*w[59];
   w[69]= - w[18] + w[8];
   w[69]=w[5]*w[69];
   w[77]=4.E+0*w[8];
   w[19]=w[77] - w[19];
   w[19]=w[2]*w[19];
   w[19]=w[19] + w[21] + w[69];
   w[19]=w[19]*w[25];
   w[19]=w[67] + w[19];
   w[19]=w[3]*w[19];
   w[21]=w[49]*w[55];
   w[67]=w[21]*ang[0];
   w[69]=6.E+0*w[67];
   w[19]=w[69] + w[19];
   w[19]=w[19]*w[35];
   w[74]= - w[60]*w[74];
   w[78]=w[41] - w[6];
   w[78]=w[78]*w[2];
   w[78]=w[78] - w[65] + ang[2];
   w[78]=w[78]*w[56];
   w[79]= - w[3]*w[78];
   w[74]=w[74] + w[79];
   w[74]=w[1]*w[74];
   w[19]=w[19] + w[74];
   w[19]=2.E+0*w[19];
   w[74]=3.E+0*w[2];
   w[79]=w[74]*w[8];
   w[80]=w[58] - w[11];
   w[81]=w[80]*w[5];
   w[79]=w[79] + w[81];
   w[79]=w[79]*w[3];
   w[61]=w[61]*w[5];
   w[61]=w[61] - ang[2];
   w[81]=w[58] - ang[1];
   w[82]=4.E+0*w[81] + w[7];
   w[82]=w[2]*w[82];
   w[82]=w[82] - w[61];
   w[82]=w[4]*w[82];
   w[82]=w[82] - w[79];
   w[82]=w[3]*w[82];
   w[83]=w[74]*w[15];
   w[84]= - w[7] - w[80];
   w[84]=w[84]*w[83];
   w[82]=w[84] + w[82];
   w[82]=w[3]*w[82];
   w[69]=w[69] + w[82];
   w[28]=w[28]*w[74];
   w[28]=w[28] + w[68] - w[62];
   w[28]=w[28]*w[2];
   w[68]=3.E+0*w[5];
   w[82]=w[12]*w[68];
   w[28]=w[28] + w[82];
   w[28]=w[28]*w[3];
   w[84]= - w[11] - w[58];
   w[84]=2.E+0*w[84] + w[7];
   w[84]=w[2]*w[84];
   w[61]=w[84] - w[61];
   w[61]=w[4]*w[61]*w[30];
   w[61]=w[61] + w[28];
   w[61]=w[1]*w[61];
   w[61]=4.E+0*w[69] + w[61];
   w[61]=4.E+0*w[61];
   w[69]= - w[13]*w[59];
   w[84]= - w[14] + w[18] + w[41];
   w[84]=w[2]*w[84];
   w[85]=w[18] - w[41];
   w[86]=w[5]*w[85];
   w[84]=w[84] - ang[2] + w[86];
   w[84]=w[84]*w[25];
   w[69]=w[69] + w[84];
   w[69]=w[3]*w[69];
   w[21]= - w[33]*w[21];
   w[21]=w[21] + w[69];
   w[33]= - w[1]*w[78];
   w[21]=2.E+0*w[21] + w[33];
   w[21]=w[1]*w[21];
   w[33]= - w[47]*w[76];
   w[21]=w[33] + w[21];
   w[21]=4.E+0*w[21];
   w[33]=w[42] + w[6];
   w[42]=w[33]*w[3];
   w[47]=w[14] + w[9];
   w[69]=2.E+0*w[4];
   w[78]=w[47]*w[69];
   w[42]=w[42] - w[78];
   w[42]=w[42]*w[3];
   w[78]=w[7]*w[15];
   w[42]=w[42] + w[78];
   w[78]=4.E+0*w[3];
   w[42]=w[42]*w[78];
   w[22]=w[22] + w[6];
   w[22]=w[22]*w[2];
   w[84]=3.E+0*w[12];
   w[22]=w[22] + w[84];
   w[86]=w[22]*w[3];
   w[87]=w[37]*w[73];
   w[86]=w[86] + w[87];
   w[86]=w[86]*w[1];
   w[42]=w[42] - w[86];
   w[86]= - 4.E+0*w[42];
   w[87]=w[6] + w[8];
   w[83]= - w[87]*w[83];
   w[77]=w[77] + w[50];
   w[77]=w[2]*w[77];
   w[77]=w[52] + w[77];
   w[77]=w[77]*w[69];
   w[77]=w[77] - w[79];
   w[77]=w[3]*w[77];
   w[77]=w[83] + w[77];
   w[77]=w[77]*w[78];
   w[51]=w[51]*w[73];
   w[28]=w[51] + w[28];
   w[28]=w[1]*w[28];
   w[28]=w[77] + w[28];
   w[28]=2.E+0*w[28];
   w[77]=w[27] + w[58];
   w[77]=w[77]*w[2];
   w[79]=3.E+0*ang[0];
   w[64]=w[64]*w[79];
   w[64]= - w[77] + w[64] + ang[2];
   w[64]=w[64]*w[2];
   w[77]=w[12]*w[5];
   w[64]=w[64] + w[77];
   w[64]=w[64]*w[1];
   w[50]=w[50]*w[2];
   w[50]=w[50] - w[52];
   w[52]=w[4]*w[50];
   w[77]=w[8]*w[2];
   w[79]=w[77] - w[65];
   w[83]= - w[3]*w[79];
   w[52]=w[52] + w[83];
   w[52]=w[3]*w[52];
   w[83]=w[15]*w[2];
   w[87]=w[83]*w[38];
   w[52]= - w[87] + w[52];
   w[52]=4.E+0*w[52] - w[64];
   w[52]=4.E+0*w[52];
   w[38]=w[38]*w[59];
   w[50]= - w[50]*w[25];
   w[38]=w[38] + w[50];
   w[38]=8.E+0*w[1]*w[38];
   w[50]=w[15]*w[6];
   w[88]=w[4]*w[14];
   w[89]= - w[3]*w[6];
   w[88]=w[88] + w[89];
   w[88]=w[3]*w[88];
   w[88]= - w[50] + w[88];
   w[89]= - 2.E+0*w[9] + w[6];
   w[89]=w[2]*w[89];
   w[12]=w[89] - w[12];
   w[12]=w[1]*w[12];
   w[12]=4.E+0*w[88] + w[12];
   w[12]=4.E+0*w[12];
   w[79]= - w[79]*w[75];
   w[79]=w[87] + w[79];
   w[64]=4.E+0*w[79] - w[64];
   w[64]=2.E+0*w[64];
   w[79]=w[4]*w[13];
   w[87]=w[8]*w[3];
   w[79]=w[79] + w[87];
   w[79]=w[3]*w[79];
   w[79]=w[50] + w[79];
   w[88]=w[60] - w[8];
   w[89]=w[88]*w[5];
   w[71]= - w[71] + w[62] - w[89];
   w[71]=w[1]*w[71];
   w[71]=4.E+0*w[79] + w[71];
   w[71]=1.2E+1*w[71];
   w[56]= - w[11]*w[56];
   w[79]=w[5] + w[2];
   w[79]=ang[1]*w[79];
   w[79]=w[79] - ang[2];
   w[90]=w[3]*w[79];
   w[56]=w[56] + w[90];
   w[56]=6.E+0*w[56];
   w[11]=w[4]*w[75]*w[11];
   w[90]=w[1]*w[3];
   w[91]= - w[79]*w[90];
   w[11]=w[11] + w[91];
   w[11]=6.E+0*w[11];
   w[42]=2.E+0*w[42];
   w[43]= - w[43]*w[73];
   w[73]= - w[60] + 5.E+0*w[8];
   w[73]=w[73]*w[5];
   w[91]=w[60] + w[8];
   w[92]=w[91] + w[6];
   w[92]=w[92]*w[2];
   w[73]= - w[73] + w[92] - 1.1E+1*ang[2];
   w[73]=w[73]*w[2];
   w[66]=3.E+0*w[66];
   w[73]=w[73] - w[66];
   w[92]= - w[3]*w[73];
   w[43]=w[43] + w[92];
   w[43]=w[1]*w[43];
   w[91]= - w[14] + w[91];
   w[91]=w[2]*w[91];
   w[91]=w[91] - w[24];
   w[91]=w[91]*w[69];
   w[92]=w[13]*w[2];
   w[92]=w[92] - ang[2];
   w[23]=w[92] + 2.E+0*w[23];
   w[23]=w[23]*w[3];
   w[91]=w[91] - w[23];
   w[91]=w[3]*w[91];
   w[74]=w[29]*w[74];
   w[91]= - w[74] + w[91];
   w[91]=w[91]*w[78];
   w[43]=w[91] + w[43];
   w[91]= - w[6] + 3.E+0*w[9];
   w[91]=w[91]*w[4];
   w[14]=w[80] - w[14];
   w[14]=w[14]*w[3];
   w[14]=w[91] - w[14];
   w[14]=w[14]*w[3];
   w[14]=w[14] - w[50];
   w[14]=w[14]*w[78];
   w[50]= - w[7] + 2.E+0*w[81];
   w[50]=w[50]*w[2];
   w[50]=w[50] + w[84];
   w[50]=w[50]*w[90];
   w[14]=w[14] + w[50];
   w[14]=w[14]*w[1];
   w[50]=pow(w[3],3);
   w[80]=8.E+0*w[50];
   w[32]=w[32]*w[80];
   w[14]=w[14] - w[32];
   w[32]=2.E+0*w[14];
   w[81]= - w[6] + 7.E+0*ang[1] - w[41];
   w[81]=w[2]*w[81];
   w[24]= - 3.E+0*w[24] + w[81];
   w[24]=w[4]*w[24];
   w[23]=w[24] - w[23];
   w[23]=w[3]*w[23];
   w[24]= - w[2]*w[29];
   w[23]=w[24] + w[23];
   w[23]=w[23]*w[78];
   w[24]= - w[73]*w[90];
   w[23]=w[23] + w[24];
   w[23]=w[1]*w[23];
   w[24]= - w[39]*w[80];
   w[23]=w[24] + w[23];
   w[24]=w[33]*w[25];
   w[29]=w[47]*w[15];
   w[24]= - w[24] + 2.E+0*w[29];
   w[24]=w[24]*w[3];
   w[39]=w[49]*w[7];
   w[24]=w[24] - w[39];
   w[24]=w[24]*w[76];
   w[22]=w[22]*w[25];
   w[39]=4.E+0*w[83];
   w[37]=w[39]*w[37];
   w[22]=w[22] + w[37];
   w[22]=w[22]*w[90];
   w[22]=w[24] + w[22];
   w[24]=2.E+0*w[22];
   w[37]=w[8] + w[45] - w[7];
   w[37]=w[37]*w[2];
   w[37]=w[37] + w[89] - 1.5E+1*ang[2];
   w[37]=w[37]*w[2];
   w[37]=w[37] - w[66];
   w[37]=w[37]*w[3];
   w[37]=w[37] - w[72];
   w[37]=w[37]*w[1];
   w[20]=w[20]*w[69];
   w[45]=w[70]*w[5];
   w[45]=w[45] - w[77];
   w[45]=w[45]*w[3];
   w[20]=w[45] - w[20];
   w[20]=w[20]*w[3];
   w[20]=w[20] + w[74];
   w[20]=w[20]*w[78];
   w[20]=w[37] + w[20];
   w[37]= - w[1]*w[42];
   w[45]= - w[1]*w[20];
   w[47]=w[54] - w[58];
   w[47]=w[5]*w[47];
   w[54]=2.E+0*w[88] - w[7];
   w[54]=w[2]*w[54];
   w[47]=w[54] - w[63] + w[47];
   w[47]=w[47]*w[25];
   w[39]=w[48]*w[39];
   w[39]=w[39] + w[47];
   w[39]=w[3]*w[39];
   w[44]= - w[79]*w[30]*w[44];
   w[47]=8.E+0*w[67];
   w[39]=w[44] - w[47] + w[39];
   w[39]=w[1]*w[39];
   w[13]= - w[13]*w[15];
   w[25]= - w[8]*w[25];
   w[13]=w[13] + w[25];
   w[13]=w[3]*w[13];
   w[25]=w[49]*w[6];
   w[13]= - w[25] + w[13];
   w[13]=w[13]*w[76];
   w[13]=w[13] + w[39];
   w[13]=2.E+0*w[13];
   w[39]=7.E+0*w[8];
   w[6]=5.E+0*w[6] - w[60] + w[39];
   w[6]=w[6]*w[59];
   w[18]=w[18] - w[39];
   w[18]=2.E+0*w[18] - w[7];
   w[18]=w[2]*w[18];
   w[44]=1.E+1*ang[1] - w[39];
   w[44]=w[5]*w[44];
   w[18]=w[18] - w[62] + w[44];
   w[18]=w[4]*w[18];
   w[27]=w[27]*w[2];
   w[39]= - w[39] + 6.E+0*ang[1];
   w[39]=w[39]*w[5];
   w[27]=w[39] + w[27] - ang[2];
   w[27]=w[27]*w[3];
   w[18]=w[18] - w[27];
   w[18]=w[3]*w[18];
   w[6]=w[6] + w[18];
   w[6]=w[3]*w[6];
   w[6]= - w[47] + w[6];
   w[18]=w[92] + 5.E+0*w[65];
   w[18]=w[18]*w[2];
   w[18]=w[18] + w[82];
   w[39]= - w[3]*w[18];
   w[39]= - w[51] + w[39];
   w[39]=w[1]*w[39];
   w[6]=2.E+0*w[6] + w[39];
   w[6]=w[1]*w[6];
   w[33]= - w[4]*w[33];
   w[33]=w[33] + w[87];
   w[33]=w[3]*w[33];
   w[29]=w[29] + w[33];
   w[29]=w[3]*w[29];
   w[25]= - w[25] + w[29];
   w[25]=w[25]*w[75];
   w[6]=8.E+0*w[25] + w[6];
   w[6]=4.E+0*w[6];
   w[22]= - 4.E+0*w[1]*w[22];
   w[14]= - 4.E+0*w[14];
   w[25]=w[8] + w[7];
   w[25]=w[25]*w[59];
   w[9]= - w[9]*w[68];
   w[7]= - w[7] + w[85];
   w[7]=w[2]*w[7];
   w[7]=w[9] + w[7];
   w[7]=w[7]*w[69];
   w[7]=w[7] - w[27];
   w[7]=w[3]*w[7];
   w[7]=w[25] + w[7];
   w[7]=w[7]*w[35];
   w[9]= - w[18]*w[90];
   w[7]=w[7] + w[9];
   w[7]=w[1]*w[7];
   w[8]=w[15]*w[8];
   w[9]= - w[4]*w[41];
   w[9]=w[9] + w[87];
   w[9]=w[3]*w[9];
   w[8]=w[8] + w[9];
   w[8]=w[8]*w[80];
   w[7]=w[8] + w[7];
   w[7]=2.E+0*w[7];
   w[8]=w[16] - w[75];
   w[9]= - 4.E+0*w[8];
   w[15]=w[90]*w[2];
   w[15]=w[15] - w[50];
   w[16]=4.E+0*w[15];
   w[15]= - 1.6E+1*w[15];
   w[18]= - w[75]*w[30];
   w[25]=w[1]*w[55];
   w[18]=w[18] + w[25];
   w[18]=w[1]*w[18];
   w[25]=pow(w[3],4);
   w[18]=w[25] + w[18];
   w[18]=8.E+0*w[18];
   w[25]=w[90]*w[55];
   w[27]=w[50]*w[30];
   w[25]=w[25] - w[27];
   w[25]=w[25]*w[1];
   w[27]=pow(w[3],5);
   w[25]=w[25] + w[27];
   w[27]=1.6E+1*w[25];
   w[29]= - 1.6E+1*w[8];
   w[8]= - 4.8E+1*w[8];
   w[25]=8.E+0*w[25];


    denom = w[9]; //cout<< denom<<tab<<sp[35]<<tab<<sp[27]<<tab<<sp[34]<<tab<<sp[35]*sp[27]<<tab<<pow(sp[34],2.0) <<tab<<(- 4.E+0*sp[35]*sp[27] + 4.E+0*pow(sp[34],2))<<endl;

    kern[0][0] = (1.0/ denom)* w[17];
 
    kern[0][1] = (1.0/ denom)* w[31];
 
    kern[0][2] = (1.0/ denom)*0.0;
 
    kern[0][3] = (1.0/ denom)*0.0;
 
    kern[0][4] = (1.0/ denom)*0.0;
 
    kern[0][5] = (1.0/ denom)*0.0;
 
    kern[0][6] = (1.0/ denom)*0.0;
 
    kern[0][7] = (1.0/ denom)*0.0;
 
    kern[0][8] = (1.0/ denom)*0.0;
 
    kern[0][9] = (1.0/ denom)*0.0;
 
    kern[0][10] = (1.0/ denom)*0.0;
 
    kern[0][11] = (1.0/ denom)*0.0;
 

    denom =  - w[16];

    kern[1][0] = (1.0/ denom)* w[36];
 
    kern[1][1] = (1.0/ denom)* w[40];
 
    kern[1][2] = (1.0/ denom)*0.0;
 
    kern[1][3] = (1.0/ denom)*0.0;
 
    kern[1][4] = (1.0/ denom)*0.0;
 
    kern[1][5] = (1.0/ denom)*0.0;
 
    kern[1][6] = (1.0/ denom)*0.0;
 
    kern[1][7] = (1.0/ denom)*0.0;
 
    kern[1][8] = (1.0/ denom)*0.0;
 
    kern[1][9] = (1.0/ denom)*0.0;
 
    kern[1][10] = (1.0/ denom)*0.0;
 
    kern[1][11] = (1.0/ denom)*0.0;
 

    denom = w[78];

    kern[2][0] = (1.0/ denom)*0.0;
 
    kern[2][1] = (1.0/ denom)*0.0;
 
    kern[2][2] = (1.0/ denom)* w[46];
 
    kern[2][3] = (1.0/ denom)*0.0;
 
    kern[2][4] = (1.0/ denom)*0.0;
 
    kern[2][5] = (1.0/ denom)*0.0;
 
    kern[2][6] = (1.0/ denom)*0.0;
 
    kern[2][7] = (1.0/ denom)*0.0;
 
    kern[2][8] = (1.0/ denom)*0.0;
 
    kern[2][9] = (1.0/ denom)*0.0;
 
    kern[2][10] = (1.0/ denom)*0.0;
 
    kern[2][11] = (1.0/ denom)*0.0;
 

    denom = w[15];

    kern[3][0] = (1.0/ denom)*0.0;
 
    kern[3][1] = (1.0/ denom)*0.0;
 
    kern[3][2] = (1.0/ denom)*0.0;
 
    kern[3][3] = (1.0/ denom)* w[53];
 
    kern[3][4] = (1.0/ denom)*0.0;
 
    kern[3][5] = (1.0/ denom)*0.0;
 
    kern[3][6] = (1.0/ denom)*0.0;
 
    kern[3][7] = (1.0/ denom)*0.0;
 
    kern[3][8] = (1.0/ denom)*0.0;
 
    kern[3][9] = (1.0/ denom)*0.0;
 
    kern[3][10] = (1.0/ denom)*0.0;
 
    kern[3][11] = (1.0/ denom)*0.0;
 

    denom = w[18];
//    if(real(denom) < 1e-18){denom=1.0; cout<<" x ";}
//    if(real(denom) < 1e-18){cout<<denom<<tab<<sp[35]<<tab<<sp[34]<<tab<<sp[27]<<tab<<"T"<<(pow(pow(sp[34],2.0)-sp[35]*sp[27],2.0))<<endl; cin.get(); }

    kern[4][0] = (1.0/ denom)* w[34];
 
    kern[4][1] = (1.0/ denom)* w[57];
 
    kern[4][2] = (1.0/ denom)*0.0;
 
    kern[4][3] = (1.0/ denom)*0.0;
 
    kern[4][4] = (1.0/ denom)* w[10];
 
    kern[4][5] = (1.0/ denom)*0.0;
 
    kern[4][6] = (1.0/ denom)*0.0;
 
    kern[4][7] = (1.0/ denom)*0.0;
 
    kern[4][8] = (1.0/ denom)*0.0;
 
    kern[4][9] = (1.0/ denom)* w[26];
 
    kern[4][10] = (1.0/ denom)* w[19];
 
    kern[4][11] = (1.0/ denom)*0.0;
 

    denom = w[27];

    kern[5][0] = (1.0/ denom)*0.0;
 
    kern[5][1] = (1.0/ denom)*0.0;
 
    kern[5][2] = (1.0/ denom)*0.0;
 
    kern[5][3] = (1.0/ denom)* w[61];
 
    kern[5][4] = (1.0/ denom)*0.0;
 
    kern[5][5] = (1.0/ denom)* w[21];
 
    kern[5][6] = (1.0/ denom)* w[86];
 
    kern[5][7] = (1.0/ denom)*0.0;
 
    kern[5][8] = (1.0/ denom)*0.0;
 
    kern[5][9] = (1.0/ denom)*0.0;
 
    kern[5][10] = (1.0/ denom)*0.0;
 
    kern[5][11] = (1.0/ denom)* w[28];
 

    denom = w[29];

    kern[6][0] = (1.0/ denom)*0.0;
 
    kern[6][1] = (1.0/ denom)*0.0;
 
    kern[6][2] = (1.0/ denom)*0.0;
 
    kern[6][3] = (1.0/ denom)* w[52];
 
    kern[6][4] = (1.0/ denom)*0.0;
 
    kern[6][5] = (1.0/ denom)* w[38];
 
    kern[6][6] = (1.0/ denom)* w[12];
 
    kern[6][7] = (1.0/ denom)*0.0;
 
    kern[6][8] = (1.0/ denom)*0.0;
 
    kern[6][9] = (1.0/ denom)*0.0;
 
    kern[6][10] = (1.0/ denom)*0.0;
 
    kern[6][11] = (1.0/ denom)* w[64];
 

    denom = w[8];

    kern[7][0] = (1.0/ denom)*0.0;
 
    kern[7][1] = (1.0/ denom)*0.0;
 
    kern[7][2] = (1.0/ denom)*0.0;
 
    kern[7][3] = (1.0/ denom)*0.0;
 
    kern[7][4] = (1.0/ denom)*0.0;
 
    kern[7][5] = (1.0/ denom)*0.0;
 
    kern[7][6] = (1.0/ denom)*0.0;
 
    kern[7][7] = (1.0/ denom)* w[71];
 
    kern[7][8] = (1.0/ denom)*0.0;
 
    kern[7][9] = (1.0/ denom)*0.0;
 
    kern[7][10] = (1.0/ denom)*0.0;
 
    kern[7][11] = (1.0/ denom)*0.0;
 

    denom =  - w[16];

    kern[8][0] = (1.0/ denom)*0.0;
 
    kern[8][1] = (1.0/ denom)*0.0;
 
    kern[8][2] = (1.0/ denom)* w[56];
 
    kern[8][3] = (1.0/ denom)*0.0;
 
    kern[8][4] = (1.0/ denom)*0.0;
 
    kern[8][5] = (1.0/ denom)*0.0;
 
    kern[8][6] = (1.0/ denom)*0.0;
 
    kern[8][7] = (1.0/ denom)*0.0;
 
    kern[8][8] = (1.0/ denom)* w[11];
 
    kern[8][9] = (1.0/ denom)*0.0;
 
    kern[8][10] = (1.0/ denom)*0.0;
 
    kern[8][11] = (1.0/ denom)*0.0;
 

    denom = w[25];

    kern[9][0] = (1.0/ denom)*  - w[42];
 
    kern[9][1] = (1.0/ denom)* w[43];
 
    kern[9][2] = (1.0/ denom)*0.0;
 
    kern[9][3] = (1.0/ denom)*0.0;
 
    kern[9][4] = (1.0/ denom)* w[32];
 
    kern[9][5] = (1.0/ denom)*0.0;
 
    kern[9][6] = (1.0/ denom)*0.0;
 
    kern[9][7] = (1.0/ denom)*0.0;
 
    kern[9][8] = (1.0/ denom)*0.0;
 
    kern[9][9] = (1.0/ denom)* w[23];
 
    kern[9][10] = (1.0/ denom)* w[24];
 
    kern[9][11] = (1.0/ denom)*0.0;
 

    denom = w[25];

    kern[10][0] = (1.0/ denom)*  - w[42];
 
    kern[10][1] = (1.0/ denom)*  - w[20];
 
    kern[10][2] = (1.0/ denom)*0.0;
 
    kern[10][3] = (1.0/ denom)*0.0;
 
    kern[10][4] = (1.0/ denom)* w[37];
 
    kern[10][5] = (1.0/ denom)*0.0;
 
    kern[10][6] = (1.0/ denom)*0.0;
 
    kern[10][7] = (1.0/ denom)*0.0;
 
    kern[10][8] = (1.0/ denom)*0.0;
 
    kern[10][9] = (1.0/ denom)* w[45];
 
    kern[10][10] = (1.0/ denom)* w[13];
 
    kern[10][11] = (1.0/ denom)*0.0;
 

    denom = w[27];

    kern[11][0] = (1.0/ denom)*0.0;
 
    kern[11][1] = (1.0/ denom)*0.0;
 
    kern[11][2] = (1.0/ denom)*0.0;
 
    kern[11][3] = (1.0/ denom)* w[6];
 
    kern[11][4] = (1.0/ denom)*0.0;
 
    kern[11][5] = (1.0/ denom)* w[22];
 
    kern[11][6] = (1.0/ denom)* w[14];
 
    kern[11][7] = (1.0/ denom)*0.0;
 
    kern[11][8] = (1.0/ denom)*0.0;
 
    kern[11][9] = (1.0/ denom)*0.0;
 
    kern[11][10] = (1.0/ denom)*0.0;
 
    kern[11][11] = (1.0/ denom)* w[7];
 

}


void kernelY_qphv(matCdoub& kern,
  const ArrayScalarProducts& sp,  const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


    // local variables
    array<Cdoub, 114> w;
    Cdoub denom;


    w[1]=sp[35];
    w[2]=sp[44];
    w[3]=sp[7];
    w[4]=sp[43];
    w[5]=sp[31];
    w[6]=sp[37];
    w[7]=sp[29];
    w[8]=sp[39];
   w[9]=ssm*ssp;
   w[10]=svp*svm;
   w[11]=w[10]*w[3];
   w[12]=w[9] + w[11];
   w[13]=w[12]*w[4];
   w[14]=w[10]*w[6];
   w[15]=w[14]*w[5];
   w[10]=w[10]*w[7];
   w[16]=w[8]*w[10];
   w[17]=w[15] + w[16];
   w[13]=w[13] - w[17];
   w[13]=w[13]*w[4];
   w[10]=w[5]*w[10];
   w[18]=w[10]*w[2];
   w[19]=2.E+0*w[18];
   w[20]=w[13] + w[19];
   w[21]=w[1]*w[2];
   w[22]= - w[12]*w[21];
   w[22]=w[22] + w[20];
   w[22]=4.E+0*w[22];
   w[23]=w[17]*w[2];
   w[14]=w[14]*w[8];
   w[24]=2.E+0*w[14];
   w[25]=w[24]*w[4];
   w[26]=w[25] - w[23];
   w[27]=4.E+0*w[4];
   w[28]= - w[26]*w[27];
   w[29]=ssm*svp;
   w[30]=w[7]*w[29];
   w[31]=svm*ssp;
   w[32]=w[5]*w[31];
   w[33]=w[30] + w[32];
   w[34]=w[33]*w[2];
   w[29]=w[6]*w[29];
   w[31]=w[8]*w[31];
   w[35]=w[29] + w[31];
   w[36]=w[35]*w[4];
   w[34]=w[34] - w[36];
   w[37]=w[34]*w[27];
   w[29]=w[29] - w[31];
   w[31]=w[29]*w[21];
   w[38]=pow(w[4],2);
   w[39]=w[29]*w[38];
   w[40]= - w[31] + w[39];
   w[41]=8.E+0*w[4];
   w[40]=w[40]*w[41];
   w[42]=w[17]*w[1];
   w[43]=2.E+0*w[10];
   w[44]= - w[4]*w[43];
   w[44]=w[42] + w[44];
   w[44]=4.E+0*w[44];
   w[45]=w[12]*w[2];
   w[46]=w[45] - w[24];
   w[47]= - w[1]*w[46];
   w[47]=w[47] + w[13];
   w[47]=w[47]*w[27];
   w[48]=w[35]*w[1];
   w[49]=w[33]*w[4];
   w[48]=w[48] - w[49];
   w[49]=w[48]*w[27];
   w[30]=w[30] - w[32];
   w[32]=w[30]*w[21];
   w[50]= - w[30]*w[38];
   w[50]=w[32] + w[50];
   w[50]=w[50]*w[41];
   w[51]=4.E+0*w[33];
   w[52]=w[35]*w[27];
   w[53]=w[9] - w[11];
   w[54]=w[53]*w[27];
   w[15]=w[15] - w[16];
   w[16]=w[41]*w[15];
   w[55]=w[30]*w[4];
   w[56]=w[29]*w[1];
   w[55]=w[55] - w[56];
   w[56]= - 8.E+0*w[55];
   w[57]=w[30]*w[2];
   w[58]=w[29]*w[4];
   w[57]=w[57] - w[58];
   w[59]= - w[57]*w[41];
   w[60]=w[53]*w[4];
   w[61]=2.E+0*w[17];
   w[60]=w[60] + w[61];
   w[60]=w[60]*w[4];
   w[19]=w[60] - w[19];
   w[60]=w[53]*w[2];
   w[60]=w[60] + w[24];
   w[62]= - w[1]*w[60];
   w[62]=w[62] + w[19];
   w[63]=1.6E+1*w[4];
   w[62]=w[62]*w[63];
   w[64]=w[11]*w[2];
   w[65]=w[64] + w[24];
   w[65]=w[65]*w[4];
   w[23]=w[65] - 3.E+0*w[23];
   w[23]=w[23]*w[4];
   w[65]=w[64] - w[14];
   w[65]=w[65]*w[21];
   w[66]=pow(w[2],2);
   w[10]=w[66]*w[10];
   w[23]=w[23] - w[65] + 3.E+0*w[10];
   w[65]=8.E+0*w[23];
   w[67]=w[9]*w[2];
   w[68]=w[67] + w[14];
   w[68]=w[68]*w[21];
   w[69]=w[10] + w[68];
   w[69]=w[1]*w[69];
   w[70]=2.E+0*w[9];
   w[71]=w[70] + w[11];
   w[71]=w[71]*w[2];
   w[72]=w[24] - w[71];
   w[72]=w[1]*w[72];
   w[20]=w[72] + w[20];
   w[20]=w[4]*w[20];
   w[72]=w[17]*w[21];
   w[73]=2.E+0*w[72];
   w[20]= - w[73] + w[20];
   w[20]=w[4]*w[20];
   w[20]=w[69] + w[20];
   w[20]=8.E+0*w[20];
   w[69]=2.E+0*w[21];
   w[74]=w[69]*w[29];
   w[39]=w[74] - w[39];
   w[39]=w[39]*w[38];
   w[74]=pow(w[21],2);
   w[75]=w[74]*w[29];
   w[39]=w[39] - w[75];
   w[75]=w[39]*w[41];
   w[76]=w[57]*w[4];
   w[31]=w[76] + w[31];
   w[31]=w[31]*w[4];
   w[66]=w[66]*w[1];
   w[76]=w[66]*w[30];
   w[31]=w[31] - w[76];
   w[76]=8.E+0*w[31];
   w[77]=w[15]*w[66];
   w[78]=w[15]*w[38];
   w[79]= - w[2]*w[78];
   w[77]=w[77] + w[79];
   w[77]=8.E+0*w[77];
   w[79]=w[34]*w[4];
   w[80]=w[35]*w[21];
   w[81]=w[79] + w[80];
   w[81]=w[81]*w[4];
   w[82]=w[66]*w[33];
   w[81]=w[81] - w[82];
   w[82]= - w[81]*w[41];
   w[83]=w[24]*w[21];
   w[84]=w[26]*w[4];
   w[83]=w[83] - w[84];
   w[83]=w[83]*w[4];
   w[84]=w[66]*w[17];
   w[83]=w[83] - w[84];
   w[84]=w[83]*w[41];
   w[85]=8.E+0*w[38];
   w[86]=w[23]*w[85];
   w[55]=w[55]*w[4];
   w[55]=w[55] - w[32];
   w[55]=w[55]*w[4];
   w[87]=w[2]*pow(w[1],2);
   w[29]=w[87]*w[29];
   w[55]=w[55] + w[29];
   w[88]=1.6E+1*w[55];
   w[68]= - w[10] + w[68];
   w[68]=w[1]*w[68];
   w[70]=w[70] - w[11];
   w[70]=w[70]*w[2];
   w[89]= - 4.E+0*w[14] - w[70];
   w[89]=w[1]*w[89];
   w[19]=w[89] + w[19];
   w[19]=w[4]*w[19];
   w[19]=w[72] + w[19];
   w[19]=w[4]*w[19];
   w[19]=w[68] + w[19];
   w[19]=w[19]*w[63];
   w[68]=w[11]*w[4];
   w[61]=w[68] - w[61];
   w[61]=w[61]*w[4];
   w[68]=3.E+0*w[14];
   w[64]=w[68] - w[64];
   w[64]=w[64]*w[1];
   w[61]=w[61] + w[64] + 3.E+0*w[18];
   w[89]=w[61]*w[4];
   w[89]=w[89] - w[72];
   w[90]= - 1.6E+1*w[89];
   w[91]=1.6E+1*w[81];
   w[92]=w[15]*w[21];
   w[92]=w[92] - w[78];
   w[93]=w[92]*w[63];
   w[94]=w[31]*w[63];
   w[23]= - w[23]*w[63];
   w[95]= - 1.6E+1*w[57];
   w[58]=w[58]*w[1];
   w[32]=w[58] - w[32];
   w[58]=1.6E+1*w[32];
   w[25]=w[25]*w[1];
   w[25]=w[25] - w[72];
   w[96]=w[25]*w[63];
   w[67]=w[67] - w[14];
   w[67]=w[67]*w[1];
   w[97]=w[9]*w[38];
   w[98]=w[97] - w[18] - w[67];
   w[98]=1.6E+1*w[98];
   w[99]=w[35]*w[38];
   w[80]=w[99] - w[80];
   w[99]=1.6E+1*w[80];
   w[100]=1.6E+1*w[38];
   w[57]= - w[57]*w[100];
   w[26]=w[26]*w[63];
   w[101]=4.8E+1*w[15];
   w[102]= - w[1]*w[101];
   w[36]=w[36]*w[1];
   w[103]=w[33]*w[21];
   w[36]=w[36] - w[103];
   w[36]=w[36]*w[4];
   w[104]=4.8E+1*w[36];
   w[105]=4.8E+1*w[48];
   w[9]=w[9]*w[4];
   w[106]=w[9] - w[17];
   w[106]=w[4]*w[106];
   w[67]=w[106] + w[18] - w[67];
   w[67]=4.8E+1*w[67];
   w[106]= - 4.8E+1*w[78];
   w[79]= - 4.8E+1*w[79];
   w[34]=4.E+0*w[34];
   w[107]=4.E+0*w[15]*pow(w[4],3);
   w[108]=w[27]*w[15];
   w[109]=w[38] - w[21];
   w[110]=w[109]*w[27];
   w[53]=w[53]*w[110];
   w[80]= - w[80]*w[27];
   w[111]= - w[38]*w[34];
   w[112]=w[2]*w[108];
   w[113]=8.E+0*w[89];
   w[11]=w[11]*w[1];
   w[11]=w[11] + w[43];
   w[11]=w[11]*w[4];
   w[11]=w[11] - 3.E+0*w[42];
   w[11]=w[11]*w[4];
   w[42]=w[64] + w[18];
   w[42]=w[42]*w[1];
   w[11]=w[11] + w[42];
   w[42]= - w[11]*w[41];
   w[43]=w[55]*w[85];
   w[64]=w[92]*w[41];
   w[48]=w[48]*w[4];
   w[48]=w[48] + w[103];
   w[48]=w[48]*w[4];
   w[35]=w[87]*w[35];
   w[48]=w[48] - w[35];
   w[92]= - w[48]*w[41];
   w[45]=w[14] - w[45];
   w[45]=w[1]*w[45];
   w[13]=2.E+0*w[45] + w[13];
   w[13]=w[4]*w[13];
   w[13]=w[72] + w[13];
   w[13]=w[4]*w[13];
   w[45]=w[46]*w[87];
   w[13]=w[45] + w[13];
   w[13]=w[13]*w[41];
   w[45]= - w[89]*w[85];
   w[31]=w[31]*w[41];
   w[46]= - 8.E+0*w[83];
   w[81]= - 8.E+0*w[81];
   w[83]=1.6E+1*w[39];
   w[61]=w[4]*w[1]*w[61];
   w[85]=w[87]*w[17];
   w[61]=w[61] - w[85];
   w[85]= - 8.E+0*w[61];
   w[89]= - 8.E+0*w[55];
   w[35]=w[35] - w[36];
   w[35]=w[4]*w[35];
   w[33]= - w[33]*w[74];
   w[33]=w[33] + w[35];
   w[33]=8.E+0*w[33];
   w[24]= - w[24]*w[87];
   w[25]=w[4]*w[25];
   w[24]=w[24] + w[25];
   w[24]=w[4]*w[24];
   w[25]=w[17]*w[74];
   w[24]=w[25] + w[24];
   w[24]=8.E+0*w[24];
   w[10]=2.E+0*w[10];
   w[12]=w[12]*w[66];
   w[12]= - w[10] + w[12];
   w[12]=w[1]*w[12];
   w[25]= - w[68] - w[71];
   w[25]=w[1]*w[25];
   w[9]=w[9] + w[17];
   w[9]=w[4]*w[9];
   w[9]=w[9] - w[18] + w[25];
   w[9]=w[4]*w[9];
   w[9]=w[73] + w[9];
   w[9]=w[4]*w[9];
   w[9]=w[12] + w[9];
   w[9]=w[9]*w[41];
   w[12]=8.E+0*w[39];
   w[17]=w[61]*w[100];
   w[11]=w[11]*w[63];
   w[25]=w[48]*w[63];
   w[15]= - w[15]*w[87];
   w[35]=w[1]*w[78];
   w[15]=w[15] + w[35];
   w[15]=w[15]*w[63];
   w[32]=w[4]*w[32];
   w[29]= - w[29] + w[32];
   w[29]=w[4]*w[29];
   w[30]=w[30]*w[74];
   w[29]=w[30] + w[29];
   w[29]=w[29]*w[63];
   w[30]=w[55]*w[100];
   w[21]=w[60]*w[21];
   w[10]=w[10] + w[21];
   w[10]=w[1]*w[10];
   w[14]=w[14] - w[70];
   w[14]=w[1]*w[14];
   w[14]=w[97] + w[18] + w[14];
   w[14]=w[4]*w[14];
   w[14]= - 3.E+0*w[72] + w[14];
   w[14]=w[4]*w[14];
   w[10]=w[10] + w[14];
   w[10]=w[10]*w[63];
   w[14]=4.E+0*w[109];
   w[18]=w[109]*w[63];
   w[21]=w[69] - w[38];
   w[21]=w[21]*w[38];
   w[21]=w[21] - w[74];
   w[32]= - 8.E+0*w[21];
   w[35]=w[21]*w[63];
   w[36]=1.6E+1*w[109];
   w[38]=4.8E+1*w[109];
   w[21]=w[21]*w[41];


    denom = w[14];

    kern[0][0] = (1.0/ denom)* w[22];
 
    kern[0][1] = (1.0/ denom)* w[28];
 
    kern[0][2] = (1.0/ denom)* w[37];
 
    kern[0][3] = (1.0/ denom)* w[40];
 
    kern[0][4] = (1.0/ denom)*0.0;
 
    kern[0][5] = (1.0/ denom)*0.0;
 
    kern[0][6] = (1.0/ denom)*0.0;
 
    kern[0][7] = (1.0/ denom)*0.0;
 
    kern[0][8] = (1.0/ denom)*0.0;
 
    kern[0][9] = (1.0/ denom)*0.0;
 
    kern[0][10] = (1.0/ denom)*0.0;
 
    kern[0][11] = (1.0/ denom)*0.0;
 

    denom = w[110];

    kern[1][0] = (1.0/ denom)* w[44];
 
    kern[1][1] = (1.0/ denom)* w[47];
 
    kern[1][2] = (1.0/ denom)* w[49];
 
    kern[1][3] = (1.0/ denom)* w[50];
 
    kern[1][4] = (1.0/ denom)*0.0;
 
    kern[1][5] = (1.0/ denom)*0.0;
 
    kern[1][6] = (1.0/ denom)*0.0;
 
    kern[1][7] = (1.0/ denom)*0.0;
 
    kern[1][8] = (1.0/ denom)*0.0;
 
    kern[1][9] = (1.0/ denom)*0.0;
 
    kern[1][10] = (1.0/ denom)*0.0;
 
    kern[1][11] = (1.0/ denom)*0.0;
 

    denom = w[27];

    kern[2][0] = (1.0/ denom)* w[51];
 
    kern[2][1] = (1.0/ denom)* w[52];
 
    kern[2][2] = (1.0/ denom)* w[54];
 
    kern[2][3] = (1.0/ denom)* w[16];
 
    kern[2][4] = (1.0/ denom)*0.0;
 
    kern[2][5] = (1.0/ denom)*0.0;
 
    kern[2][6] = (1.0/ denom)*0.0;
 
    kern[2][7] = (1.0/ denom)*0.0;
 
    kern[2][8] = (1.0/ denom)*0.0;
 
    kern[2][9] = (1.0/ denom)*0.0;
 
    kern[2][10] = (1.0/ denom)*0.0;
 
    kern[2][11] = (1.0/ denom)*0.0;
 

    denom = w[18];

    kern[3][0] = (1.0/ denom)* w[56];
 
    kern[3][1] = (1.0/ denom)* w[59];

    kern[3][2] = (1.0/ denom)*  - w[16];
 
    kern[3][3] = (1.0/ denom)* w[62];
 
    kern[3][4] = (1.0/ denom)*0.0;
 
    kern[3][5] = (1.0/ denom)*0.0;
 
    kern[3][6] = (1.0/ denom)*0.0;
 
    kern[3][7] = (1.0/ denom)*0.0;
 
    kern[3][8] = (1.0/ denom)*0.0;
 
    kern[3][9] = (1.0/ denom)*0.0;
 
    kern[3][10] = (1.0/ denom)*0.0;
 
    kern[3][11] = (1.0/ denom)*0.0;
 

    denom = w[32];
    if(real(denom) < 1e-18){denom=1.0; cout<<" x ";}


    kern[4][0] = (1.0/ denom)* w[65];
 
    kern[4][1] = (1.0/ denom)*0.0;
 
    kern[4][2] = (1.0/ denom)*0.0;
 
    kern[4][3] = (1.0/ denom)*0.0;
 
    kern[4][4] = (1.0/ denom)* w[20];
 
    kern[4][5] = (1.0/ denom)* w[75];
 
    kern[4][6] = (1.0/ denom)* w[76];
 
    kern[4][7] = (1.0/ denom)* w[77];
 
    kern[4][8] = (1.0/ denom)* w[82];
 
    kern[4][9] = (1.0/ denom)* w[84];
 
    kern[4][10] = (1.0/ denom)* w[86];
 
    kern[4][11] = (1.0/ denom)*0.0;
 

    denom =  - w[35];

    kern[5][0] = (1.0/ denom)*0.0;
 
    kern[5][1] = (1.0/ denom)*0.0;
 
    kern[5][2] = (1.0/ denom)*0.0;
 
    kern[5][3] = (1.0/ denom)*0.0;
 
    kern[5][4] = (1.0/ denom)* w[88];
 
    kern[5][5] = (1.0/ denom)* w[19];
 
    kern[5][6] = (1.0/ denom)* w[90];
 
    kern[5][7] = (1.0/ denom)* w[91];
 
    kern[5][8] = (1.0/ denom)* w[93];
 
    kern[5][9] = (1.0/ denom)* w[94];
 
    kern[5][10] = (1.0/ denom)*0.0;
 
    kern[5][11] = (1.0/ denom)* w[23];
 

    denom = w[36];

    kern[6][0] = (1.0/ denom)* w[95];
 
    kern[6][1] = (1.0/ denom)*0.0;
 
    kern[6][2] = (1.0/ denom)*0.0;
 
    kern[6][3] = (1.0/ denom)*0.0;
 
    kern[6][4] = (1.0/ denom)* w[58];
 
    kern[6][5] = (1.0/ denom)* w[96];
 
    kern[6][6] = (1.0/ denom)* w[98];
 
    kern[6][7] = (1.0/ denom)* w[99];
 
    kern[6][8] = (1.0/ denom)*0.0;
 
    kern[6][9] = (1.0/ denom)*0.0;
 
    kern[6][10] = (1.0/ denom)* w[57];
 
    kern[6][11] = (1.0/ denom)* w[26];
 

    denom = w[38];

    kern[7][0] = (1.0/ denom)*  - w[101];
 
    kern[7][1] = (1.0/ denom)*0.0;
 
    kern[7][2] = (1.0/ denom)*0.0;
 
    kern[7][3] = (1.0/ denom)*0.0;
 
    kern[7][4] = (1.0/ denom)* w[102];
 
    kern[7][5] = (1.0/ denom)* w[104];
 
    kern[7][6] = (1.0/ denom)* w[105];
 
    kern[7][7] = (1.0/ denom)* w[67];
 
    kern[7][8] = (1.0/ denom)*0.0;
 
    kern[7][9] = (1.0/ denom)*0.0;
 
    kern[7][10] = (1.0/ denom)* w[106];
 
    kern[7][11] = (1.0/ denom)* w[79];
 

    denom = w[110];

    kern[8][0] = (1.0/ denom)*  - w[34];
 
    kern[8][1] = (1.0/ denom)*0.0;
 
    kern[8][2] = (1.0/ denom)*0.0;
 
    kern[8][3] = (1.0/ denom)*0.0;
 
    kern[8][4] = (1.0/ denom)* w[49];
 
    kern[8][5] = (1.0/ denom)* w[107];
 
    kern[8][6] = (1.0/ denom)* w[108];
 
    kern[8][7] = (1.0/ denom)*0.0;
 
    kern[8][8] = (1.0/ denom)* w[53];
 
    kern[8][9] = (1.0/ denom)* w[80];
 
    kern[8][10] = (1.0/ denom)* w[111];
 
    kern[8][11] = (1.0/ denom)* w[112];
 

    denom =  - w[21];

    kern[9][0] = (1.0/ denom)*  - w[113];
 
    kern[9][1] = (1.0/ denom)*0.0;
 
    kern[9][2] = (1.0/ denom)*0.0;
 
    kern[9][3] = (1.0/ denom)*0.0;
 
    kern[9][4] = (1.0/ denom)* w[42];
 
    kern[9][5] = (1.0/ denom)* w[43];
 
    kern[9][6] = (1.0/ denom)*0.0;
 
    kern[9][7] = (1.0/ denom)*  - w[64];
 
    kern[9][8] = (1.0/ denom)* w[92];
 
    kern[9][9] = (1.0/ denom)* w[13];
 
    kern[9][10] = (1.0/ denom)* w[45];
 
    kern[9][11] = (1.0/ denom)* w[31];
 

    denom =  - w[21];

    kern[10][0] = (1.0/ denom)*  - w[113];
 
    kern[10][1] = (1.0/ denom)* w[46];
 
    kern[10][2] = (1.0/ denom)* w[81];
 
    kern[10][3] = (1.0/ denom)* w[83];
 
    kern[10][4] = (1.0/ denom)* w[85];
 
    kern[10][5] = (1.0/ denom)*0.0;
 
    kern[10][6] = (1.0/ denom)* w[89];
 
    kern[10][7] = (1.0/ denom)*  - w[64];
 
    kern[10][8] = (1.0/ denom)* w[33];
 
    kern[10][9] = (1.0/ denom)* w[24];
 
    kern[10][10] = (1.0/ denom)* w[9];
 
    kern[10][11] = (1.0/ denom)* w[12];
 

    denom =  - w[35];

    kern[11][0] = (1.0/ denom)* w[88];
 
    kern[11][1] = (1.0/ denom)*0.0;
 
    kern[11][2] = (1.0/ denom)*0.0;
 
    kern[11][3] = (1.0/ denom)*0.0;
 
    kern[11][4] = (1.0/ denom)*0.0;
 
    kern[11][5] = (1.0/ denom)* w[17];
 
    kern[11][6] = (1.0/ denom)* w[11];
 
    kern[11][7] = (1.0/ denom)* w[25];
 
    kern[11][8] = (1.0/ denom)* w[15];
 
    kern[11][9] = (1.0/ denom)* w[29];
 
    kern[11][10] = (1.0/ denom)* w[30];
 
    kern[11][11] = (1.0/ denom)* w[10];
 


}

