#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
 
void normalisation_notopti_qphv(matCdoub& kern,
  Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
  Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
  const Cdoub& svp, const Cdoub& ssp, 
  const Cdoub& svm, const Cdoub& ssm ){
 
 
 
 
    kern[0][0] =  - 1.6E+1*ssp*ssm - 8.E+0*qm_qp*svp*svm;

    kern[0][1] =  - 4.E+0*l_l*qm_qp*svp*svm - 4.E+0*l_l*ssp*ssm + 8.E+0
      *l_qm*l_qp*svp*svm;

    kern[0][2] = 4.E+0*l_qm*ssp*svm + 4.E+0*l_qp*svp*ssm;

    kern[0][3] = 2.4E+1*l_qm*i_*ssp*svm - 2.4E+1*l_qp*i_*svp*ssm;

    kern[0][4] =  - 4.E+0*P_P*qm_qp*svp*svm - 1.2E+1*P_P*ssp*ssm - 
      8.E+0*P_qm*P_qp*svp*svm;

    kern[0][5] =  - 8.E+0*P_P*P_l*l_qm*ssp*svm + 8.E+0*P_P*P_l*l_qp*svp
      *ssm - 4.E+0*pow(P_l,2)*P_qm*ssp*svm + 4.E+0*pow(P_l,2)*P_qp*svp*
      ssm;

    kern[0][6] =  - 1.2E+1*P_qm*ssp*svm + 1.2E+1*P_qp*svp*ssm;

    kern[0][7] =  - 8.E+0*P_qm*l_qp*svp*svm + 8.E+0*P_qp*l_qm*svp*svm;

    kern[0][8] =  - 4.E+0*P_P*l_qm*ssp*svm - 4.E+0*P_P*l_qp*svp*ssm + 
      4.E+0*P_l*P_qm*ssp*svm + 4.E+0*P_l*P_qp*svp*ssm;

    kern[0][9] =  - 4.E+0*P_P*l_l*qm_qp*svp*svm - 4.E+0*P_P*l_l*ssp*ssm
       + 8.E+0*P_P*l_qm*l_qp*svp*svm - 4.E+0*P_l*P_qm*l_qp*svp*svm - 
      4.E+0*P_l*P_qp*l_qm*svp*svm + 4.E+0*pow(P_l,2)*qm_qp*svp*svm + 
      4.E+0*pow(P_l,2)*ssp*ssm;

    kern[0][10] =  - 4.E+0*P_l*P_qm*l_qp*svp*svm - 4.E+0*P_l*P_qp*l_qm*
      svp*svm - 4.E+0*pow(P_l,2)*qm_qp*svp*svm - 1.2E+1*pow(P_l,2)*ssp*
      ssm;

    kern[0][11] =  - 8.E+0*P_l*l_qm*ssp*svm + 8.E+0*P_l*l_qp*svp*ssm - 
      4.E+0*P_qm*l_l*ssp*svm + 4.E+0*P_qp*l_l*svp*ssm;

 
    kern[1][0] =  - 4.E+0*l_l*qm_qp*svp*svm - 4.E+0*l_l*ssp*ssm + 8.E+0
      *l_qm*l_qp*svp*svm;

    kern[1][1] = 8.E+0*l_l*l_qm*l_qp*svp*svm - 4.E+0*pow(l_l,2)*qm_qp*
      svp*svm - 4.E+0*pow(l_l,2)*ssp*ssm;

    kern[1][2] = 4.E+0*l_l*l_qm*ssp*svm + 4.E+0*l_l*l_qp*svp*ssm;

    kern[1][3] =  0;

    kern[1][4] =  - 4.E+0*P_P*l_l*qm_qp*svp*svm - 4.E+0*P_P*l_l*ssp*ssm
       + 8.E+0*P_P*l_qm*l_qp*svp*svm - 4.E+0*P_l*P_qm*l_qp*svp*svm - 
      4.E+0*P_l*P_qp*l_qm*svp*svm + 4.E+0*pow(P_l,2)*qm_qp*svp*svm + 
      4.E+0*pow(P_l,2)*ssp*ssm;

    kern[1][5] =  - 4.E+0*pow(P_l,2)*P_qm*l_l*ssp*svm + 4.E+0*pow(
      P_l,2)*P_qp*l_l*svp*ssm + 4.E+0*pow(P_l,3)*l_qm*ssp*svm - 4.E+0*
      pow(P_l,3)*l_qp*svp*ssm;

    kern[1][6] = 4.E+0*P_l*l_qm*ssp*svm - 4.E+0*P_l*l_qp*svp*ssm - 
      4.E+0*P_qm*l_l*ssp*svm + 4.E+0*P_qp*l_l*svp*ssm;

    kern[1][7] =  0;

    kern[1][8] =  - 4.E+0*P_P*l_l*l_qm*ssp*svm - 4.E+0*P_P*l_l*l_qp*svp
      *ssm + 4.E+0*pow(P_l,2)*l_qm*ssp*svm + 4.E+0*pow(P_l,2)*l_qp*svp*
      ssm;

    kern[1][9] = 8.E+0*P_P*l_l*l_qm*l_qp*svp*svm - 4.E+0*P_P*pow(l_l,2)
      *qm_qp*svp*svm - 4.E+0*P_P*pow(l_l,2)*ssp*ssm + 4.E+0*pow(P_l,2)*
      l_l*qm_qp*svp*svm + 4.E+0*pow(P_l,2)*l_l*ssp*ssm - 8.E+0*pow(
      P_l,2)*l_qm*l_qp*svp*svm;

    kern[1][10] =  - 4.E+0*P_l*P_qm*l_l*l_qp*svp*svm - 4.E+0*P_l*P_qp*
      l_l*l_qm*svp*svm + 8.E+0*pow(P_l,2)*l_qm*l_qp*svp*svm;

    kern[1][11] = 4.E+0*P_l*l_l*l_qm*ssp*svm - 4.E+0*P_l*l_l*l_qp*svp*
      ssm - 4.E+0*P_qm*pow(l_l,2)*ssp*svm + 4.E+0*P_qp*pow(l_l,2)*svp*
      ssm;

 
    kern[2][0] = 4.E+0*l_qm*ssp*svm + 4.E+0*l_qp*svp*ssm;

    kern[2][1] = 4.E+0*l_l*l_qm*ssp*svm + 4.E+0*l_l*l_qp*svp*ssm;

    kern[2][2] =  - 4.E+0*l_l*qm_qp*svp*svm + 4.E+0*l_l*ssp*ssm;

    kern[2][3] =  0;

    kern[2][4] = 4.E+0*P_P*l_qm*ssp*svm + 4.E+0*P_P*l_qp*svp*ssm - 
      4.E+0*P_l*P_qm*ssp*svm - 4.E+0*P_l*P_qp*svp*ssm;

    kern[2][5] = 4.E+0*pow(P_l,2)*P_qm*l_qp*svp*svm - 4.E+0*pow(P_l,2)*
      P_qp*l_qm*svp*svm;

    kern[2][6] = 4.E+0*P_qm*l_qp*svp*svm - 4.E+0*P_qp*l_qm*svp*svm;

    kern[2][7] =  0;

    kern[2][8] = 4.E+0*P_P*l_l*qm_qp*svp*svm - 4.E+0*P_P*l_l*ssp*ssm - 
      4.E+0*pow(P_l,2)*qm_qp*svp*svm + 4.E+0*pow(P_l,2)*ssp*ssm;

    kern[2][9] = 4.E+0*P_P*l_l*l_qm*ssp*svm + 4.E+0*P_P*l_l*l_qp*svp*
      ssm - 4.E+0*pow(P_l,2)*l_qm*ssp*svm - 4.E+0*pow(P_l,2)*l_qp*svp*
      ssm;

    kern[2][10] =  - 4.E+0*P_l*P_qm*l_l*ssp*svm - 4.E+0*P_l*P_qp*l_l*
      svp*ssm + 4.E+0*pow(P_l,2)*l_qm*ssp*svm + 4.E+0*pow(P_l,2)*l_qp*
      svp*ssm;

    kern[2][11] = 4.E+0*P_qm*l_l*l_qp*svp*svm - 4.E+0*P_qp*l_l*l_qm*svp
      *svm;

 
    kern[3][0] =  - 2.4E+1*l_qm*i_*ssp*svm + 2.4E+1*l_qp*i_*svp*ssm;

    kern[3][1] =  0;

    kern[3][2] =  0;

    kern[3][3] =  - 1.6E+1*l_l*qm_qp*svp*svm + 4.8E+1*l_l*ssp*ssm + 
      6.4E+1*l_qm*l_qp*svp*svm;

    kern[3][4] =  - 1.6E+1*P_P*l_qm*i_*ssp*svm + 1.6E+1*P_P*l_qp*i_*svp
      *ssm - 8.E+0*P_l*P_qm*i_*ssp*svm + 8.E+0*P_l*P_qp*i_*svp*ssm;

    kern[3][5] = 1.6E+1*P_P*P_l*l_l*i_*ssp*ssm + 1.6E+1*P_P*P_l*l_qm*
      l_qp*i_*svp*svm - 1.6E+1*P_l*P_qm*P_qp*l_l*i_*svp*svm + 1.6E+1*
      pow(P_l,2)*P_qm*l_qp*i_*svp*svm + 1.6E+1*pow(P_l,2)*P_qp*l_qm*i_*
      svp*svm - 8.E+0*pow(P_l,3)*qm_qp*i_*svp*svm + 8.E+0*pow(P_l,3)*i_
      *ssp*ssm;

    kern[3][6] =  - 8.E+0*P_l*qm_qp*i_*svp*svm + 2.4E+1*P_l*i_*ssp*ssm
       + 1.6E+1*P_qm*l_qp*i_*svp*svm + 1.6E+1*P_qp*l_qm*i_*svp*svm;

    kern[3][7] = 1.6E+1*P_l*l_qm*i_*ssp*svm + 1.6E+1*P_l*l_qp*i_*svp*
      ssm - 1.6E+1*P_qm*l_l*i_*ssp*svm - 1.6E+1*P_qp*l_l*i_*svp*ssm;

    kern[3][8] =  - 8.E+0*P_l*P_qm*l_qp*i_*svp*svm + 8.E+0*P_l*P_qp*
      l_qm*i_*svp*svm;

    kern[3][9] =  - 8.E+0*P_l*P_qm*l_l*i_*ssp*svm + 8.E+0*P_l*P_qp*l_l*
      i_*svp*ssm + 8.E+0*pow(P_l,2)*l_qm*i_*ssp*svm - 8.E+0*pow(P_l,2)*
      l_qp*i_*svp*ssm;

    kern[3][10] =  - 2.4E+1*pow(P_l,2)*l_qm*i_*ssp*svm + 2.4E+1*pow(
      P_l,2)*l_qp*i_*svp*ssm;

    kern[3][11] =  - 8.E+0*P_l*l_l*qm_qp*i_*svp*svm + 2.4E+1*P_l*l_l*i_
      *ssp*ssm + 3.2E+1*P_l*l_qm*l_qp*i_*svp*svm;

 
    kern[4][0] =  - 4.E+0*P_P*qm_qp*svp*svm - 1.2E+1*P_P*ssp*ssm - 
      8.E+0*P_qm*P_qp*svp*svm;

    kern[4][1] =  - 4.E+0*P_P*l_l*qm_qp*svp*svm - 4.E+0*P_P*l_l*ssp*ssm
       + 8.E+0*P_P*l_qm*l_qp*svp*svm - 4.E+0*P_l*P_qm*l_qp*svp*svm - 
      4.E+0*P_l*P_qp*l_qm*svp*svm + 4.E+0*pow(P_l,2)*qm_qp*svp*svm + 
      4.E+0*pow(P_l,2)*ssp*ssm;

    kern[4][2] = 4.E+0*P_P*l_qm*ssp*svm + 4.E+0*P_P*l_qp*svp*ssm - 
      4.E+0*P_l*P_qm*ssp*svm - 4.E+0*P_l*P_qp*svp*ssm;

    kern[4][3] = 1.6E+1*P_P*l_qm*i_*ssp*svm - 1.6E+1*P_P*l_qp*i_*svp*
      ssm + 8.E+0*P_l*P_qm*i_*ssp*svm - 8.E+0*P_l*P_qp*i_*svp*ssm;

    kern[4][4] =  - 8.E+0*P_P*P_qm*P_qp*svp*svm - 4.E+0*pow(P_P,2)*
      qm_qp*svp*svm - 1.2E+1*pow(P_P,2)*ssp*ssm;

    kern[4][5] =  - 4.E+0*P_P*pow(P_l,2)*P_qm*ssp*svm + 4.E+0*P_P*pow(
      P_l,2)*P_qp*svp*ssm - 8.E+0*pow(P_P,2)*P_l*l_qm*ssp*svm + 8.E+0*
      pow(P_P,2)*P_l*l_qp*svp*ssm;

    kern[4][6] =  - 1.2E+1*P_P*P_qm*ssp*svm + 1.2E+1*P_P*P_qp*svp*ssm;

    kern[4][7] =  - 8.E+0*P_P*P_qm*l_qp*svp*svm + 8.E+0*P_P*P_qp*l_qm*
      svp*svm;

    kern[4][8] = 4.E+0*P_P*P_l*P_qm*ssp*svm + 4.E+0*P_P*P_l*P_qp*svp*
      ssm - 4.E+0*pow(P_P,2)*l_qm*ssp*svm - 4.E+0*pow(P_P,2)*l_qp*svp*
      ssm;

    kern[4][9] =  - 4.E+0*P_P*P_l*P_qm*l_qp*svp*svm - 4.E+0*P_P*P_l*
      P_qp*l_qm*svp*svm + 4.E+0*P_P*pow(P_l,2)*qm_qp*svp*svm + 4.E+0*
      P_P*pow(P_l,2)*ssp*ssm - 4.E+0*pow(P_P,2)*l_l*qm_qp*svp*svm - 
      4.E+0*pow(P_P,2)*l_l*ssp*ssm + 8.E+0*pow(P_P,2)*l_qm*l_qp*svp*svm
      ;

    kern[4][10] =  - 4.E+0*P_P*P_l*P_qm*l_qp*svp*svm - 4.E+0*P_P*P_l*
      P_qp*l_qm*svp*svm - 4.E+0*P_P*pow(P_l,2)*qm_qp*svp*svm - 1.2E+1*
      P_P*pow(P_l,2)*ssp*ssm;

    kern[4][11] =  - 8.E+0*P_P*P_l*l_qm*ssp*svm + 8.E+0*P_P*P_l*l_qp*
      svp*ssm - 4.E+0*P_P*P_qm*l_l*ssp*svm + 4.E+0*P_P*P_qp*l_l*svp*ssm
      ;

 
    kern[5][0] =  - 8.E+0*P_P*P_l*l_qm*ssp*svm + 8.E+0*P_P*P_l*l_qp*svp
      *ssm - 4.E+0*pow(P_l,2)*P_qm*ssp*svm + 4.E+0*pow(P_l,2)*P_qp*svp*
      ssm;

    kern[5][1] =  - 4.E+0*pow(P_l,2)*P_qm*l_l*ssp*svm + 4.E+0*pow(
      P_l,2)*P_qp*l_l*svp*ssm + 4.E+0*pow(P_l,3)*l_qm*ssp*svm - 4.E+0*
      pow(P_l,3)*l_qp*svp*ssm;

    kern[5][2] = 4.E+0*pow(P_l,2)*P_qm*l_qp*svp*svm - 4.E+0*pow(P_l,2)*
      P_qp*l_qm*svp*svm;

    kern[5][3] =  - 1.6E+1*P_P*P_l*l_l*i_*ssp*ssm - 1.6E+1*P_P*P_l*l_qm
      *l_qp*i_*svp*svm + 1.6E+1*P_l*P_qm*P_qp*l_l*i_*svp*svm - 1.6E+1*
      pow(P_l,2)*P_qm*l_qp*i_*svp*svm - 1.6E+1*pow(P_l,2)*P_qp*l_qm*i_*
      svp*svm + 8.E+0*pow(P_l,3)*qm_qp*i_*svp*svm - 8.E+0*pow(P_l,3)*i_
      *ssp*ssm;

    kern[5][4] =  - 4.E+0*P_P*pow(P_l,2)*P_qm*ssp*svm + 4.E+0*P_P*pow(
      P_l,2)*P_qp*svp*ssm - 8.E+0*pow(P_P,2)*P_l*l_qm*ssp*svm + 8.E+0*
      pow(P_P,2)*P_l*l_qp*svp*ssm;

    kern[5][5] =  - 8.E+0*P_P*pow(P_l,2)*P_qm*P_qp*l_l*svp*svm + 8.E+0*
      P_P*pow(P_l,3)*P_qm*l_qp*svp*svm + 8.E+0*P_P*pow(P_l,3)*P_qp*l_qm
      *svp*svm - 4.E+0*P_P*pow(P_l,4)*qm_qp*svp*svm + 4.E+0*P_P*pow(
      P_l,4)*ssp*ssm + 8.E+0*pow(P_P,2)*pow(P_l,2)*l_l*ssp*ssm + 8.E+0*
      pow(P_P,2)*pow(P_l,2)*l_qm*l_qp*svp*svm;

    kern[5][6] = 8.E+0*P_P*P_l*P_qm*l_qp*svp*svm + 8.E+0*P_P*P_l*P_qp*
      l_qm*svp*svm - 4.E+0*P_P*pow(P_l,2)*qm_qp*svp*svm + 1.2E+1*P_P*
      pow(P_l,2)*ssp*ssm;

    kern[5][7] =  - 8.E+0*P_P*P_l*P_qm*l_l*ssp*svm - 8.E+0*P_P*P_l*P_qp
      *l_l*svp*ssm + 8.E+0*P_P*pow(P_l,2)*l_qm*ssp*svm + 8.E+0*P_P*pow(
      P_l,2)*l_qp*svp*ssm;

    kern[5][8] =  - 4.E+0*P_P*pow(P_l,2)*P_qm*l_qp*svp*svm + 4.E+0*P_P*
      pow(P_l,2)*P_qp*l_qm*svp*svm;

    kern[5][9] =  - 4.E+0*P_P*pow(P_l,2)*P_qm*l_l*ssp*svm + 4.E+0*P_P*
      pow(P_l,2)*P_qp*l_l*svp*ssm + 4.E+0*P_P*pow(P_l,3)*l_qm*ssp*svm
       - 4.E+0*P_P*pow(P_l,3)*l_qp*svp*ssm;

    kern[5][10] =  - 1.2E+1*P_P*pow(P_l,3)*l_qm*ssp*svm + 1.2E+1*P_P*
      pow(P_l,3)*l_qp*svp*ssm;

    kern[5][11] =  - 4.E+0*P_P*pow(P_l,2)*l_l*qm_qp*svp*svm + 1.2E+1*
      P_P*pow(P_l,2)*l_l*ssp*ssm + 1.6E+1*P_P*pow(P_l,2)*l_qm*l_qp*svp*
      svm;

 
    kern[6][0] =  - 1.2E+1*P_qm*ssp*svm + 1.2E+1*P_qp*svp*ssm;

    kern[6][1] = 4.E+0*P_l*l_qm*ssp*svm - 4.E+0*P_l*l_qp*svp*ssm - 
      4.E+0*P_qm*l_l*ssp*svm + 4.E+0*P_qp*l_l*svp*ssm;

    kern[6][2] = 4.E+0*P_qm*l_qp*svp*svm - 4.E+0*P_qp*l_qm*svp*svm;

    kern[6][3] = 8.E+0*P_l*qm_qp*i_*svp*svm - 2.4E+1*P_l*i_*ssp*ssm - 
      1.6E+1*P_qm*l_qp*i_*svp*svm - 1.6E+1*P_qp*l_qm*i_*svp*svm;

    kern[6][4] =  - 1.2E+1*P_P*P_qm*ssp*svm + 1.2E+1*P_P*P_qp*svp*ssm;

    kern[6][5] = 8.E+0*P_P*P_l*P_qm*l_qp*svp*svm + 8.E+0*P_P*P_l*P_qp*
      l_qm*svp*svm - 4.E+0*P_P*pow(P_l,2)*qm_qp*svp*svm + 1.2E+1*P_P*
      pow(P_l,2)*ssp*ssm;

    kern[6][6] =  - 4.E+0*P_P*qm_qp*svp*svm + 1.2E+1*P_P*ssp*ssm + 
      1.6E+1*P_qm*P_qp*svp*svm;

    kern[6][7] = 8.E+0*P_P*l_qm*ssp*svm + 8.E+0*P_P*l_qp*svp*ssm - 
      8.E+0*P_l*P_qm*ssp*svm - 8.E+0*P_l*P_qp*svp*ssm;

    kern[6][8] =  - 4.E+0*P_P*P_qm*l_qp*svp*svm + 4.E+0*P_P*P_qp*l_qm*
      svp*svm;

    kern[6][9] = 4.E+0*P_P*P_l*l_qm*ssp*svm - 4.E+0*P_P*P_l*l_qp*svp*
      ssm - 4.E+0*P_P*P_qm*l_l*ssp*svm + 4.E+0*P_P*P_qp*l_l*svp*ssm;

    kern[6][10] =  - 4.E+0*P_P*P_l*l_qm*ssp*svm + 4.E+0*P_P*P_l*l_qp*
      svp*ssm - 8.E+0*pow(P_l,2)*P_qm*ssp*svm + 8.E+0*pow(P_l,2)*P_qp*
      svp*ssm;

    kern[6][11] =  - 4.E+0*P_P*l_l*qm_qp*svp*svm + 4.E+0*P_P*l_l*ssp*
      ssm + 8.E+0*P_P*l_qm*l_qp*svp*svm + 8.E+0*pow(P_l,2)*ssp*ssm + 
      8.E+0*P_qm*P_qp*l_l*svp*svm;

 
    kern[7][0] =  - 8.E+0*P_qm*l_qp*svp*svm + 8.E+0*P_qp*l_qm*svp*svm;

    kern[7][1] =  0;

    kern[7][2] =  0;

    kern[7][3] =  - 1.6E+1*P_l*l_qm*i_*ssp*svm - 1.6E+1*P_l*l_qp*i_*svp
      *ssm + 1.6E+1*P_qm*l_l*i_*ssp*svm + 1.6E+1*P_qp*l_l*i_*svp*ssm;

    kern[7][4] =  - 8.E+0*P_P*P_qm*l_qp*svp*svm + 8.E+0*P_P*P_qp*l_qm*
      svp*svm;

    kern[7][5] =  - 8.E+0*P_P*P_l*P_qm*l_l*ssp*svm - 8.E+0*P_P*P_l*P_qp
      *l_l*svp*ssm + 8.E+0*P_P*pow(P_l,2)*l_qm*ssp*svm + 8.E+0*P_P*pow(
      P_l,2)*l_qp*svp*ssm;

    kern[7][6] = 8.E+0*P_P*l_qm*ssp*svm + 8.E+0*P_P*l_qp*svp*ssm - 
      8.E+0*P_l*P_qm*ssp*svm - 8.E+0*P_l*P_qp*svp*ssm;

    kern[7][7] =  - 8.E+0*P_P*l_l*ssp*ssm + 8.E+0*P_P*l_qm*l_qp*svp*svm
       - 8.E+0*P_l*P_qm*l_qp*svp*svm - 8.E+0*P_l*P_qp*l_qm*svp*svm + 
      8.E+0*pow(P_l,2)*ssp*ssm + 8.E+0*P_qm*P_qp*l_l*svp*svm;

    kern[7][8] =  0;

    kern[7][9] =  0;

    kern[7][10] =  - 8.E+0*pow(P_l,2)*P_qm*l_qp*svp*svm + 8.E+0*pow(
      P_l,2)*P_qp*l_qm*svp*svm;

    kern[7][11] =  - 8.E+0*P_l*P_qm*l_l*ssp*svm - 8.E+0*P_l*P_qp*l_l*
      svp*ssm + 8.E+0*pow(P_l,2)*l_qm*ssp*svm + 8.E+0*pow(P_l,2)*l_qp*
      svp*ssm;

 
    kern[8][0] =  - 4.E+0*P_P*l_qm*ssp*svm - 4.E+0*P_P*l_qp*svp*ssm + 
      4.E+0*P_l*P_qm*ssp*svm + 4.E+0*P_l*P_qp*svp*ssm;

    kern[8][1] =  - 4.E+0*P_P*l_l*l_qm*ssp*svm - 4.E+0*P_P*l_l*l_qp*svp
      *ssm + 4.E+0*pow(P_l,2)*l_qm*ssp*svm + 4.E+0*pow(P_l,2)*l_qp*svp*
      ssm;

    kern[8][2] = 4.E+0*P_P*l_l*qm_qp*svp*svm - 4.E+0*P_P*l_l*ssp*ssm - 
      4.E+0*pow(P_l,2)*qm_qp*svp*svm + 4.E+0*pow(P_l,2)*ssp*ssm;

    kern[8][3] = 8.E+0*P_l*P_qm*l_qp*i_*svp*svm - 8.E+0*P_l*P_qp*l_qm*
      i_*svp*svm;

    kern[8][4] = 4.E+0*P_P*P_l*P_qm*ssp*svm + 4.E+0*P_P*P_l*P_qp*svp*
      ssm - 4.E+0*pow(P_P,2)*l_qm*ssp*svm - 4.E+0*pow(P_P,2)*l_qp*svp*
      ssm;

    kern[8][5] =  - 4.E+0*P_P*pow(P_l,2)*P_qm*l_qp*svp*svm + 4.E+0*P_P*
      pow(P_l,2)*P_qp*l_qm*svp*svm;

    kern[8][6] =  - 4.E+0*P_P*P_qm*l_qp*svp*svm + 4.E+0*P_P*P_qp*l_qm*
      svp*svm;

    kern[8][7] =  0;

    kern[8][8] = 4.E+0*P_P*pow(P_l,2)*qm_qp*svp*svm - 4.E+0*P_P*pow(
      P_l,2)*ssp*ssm - 4.E+0*pow(P_P,2)*l_l*qm_qp*svp*svm + 4.E+0*pow(
      P_P,2)*l_l*ssp*ssm;

    kern[8][9] = 4.E+0*P_P*pow(P_l,2)*l_qm*ssp*svm + 4.E+0*P_P*pow(
      P_l,2)*l_qp*svp*ssm - 4.E+0*pow(P_P,2)*l_l*l_qm*ssp*svm - 4.E+0*
      pow(P_P,2)*l_l*l_qp*svp*ssm;

    kern[8][10] = 4.E+0*P_P*P_l*P_qm*l_l*ssp*svm + 4.E+0*P_P*P_l*P_qp*
      l_l*svp*ssm - 4.E+0*P_P*pow(P_l,2)*l_qm*ssp*svm - 4.E+0*P_P*pow(
      P_l,2)*l_qp*svp*ssm;

    kern[8][11] =  - 4.E+0*P_P*P_qm*l_l*l_qp*svp*svm + 4.E+0*P_P*P_qp*
      l_l*l_qm*svp*svm;

 
    kern[9][0] =  - 4.E+0*P_P*l_l*qm_qp*svp*svm - 4.E+0*P_P*l_l*ssp*ssm
       + 8.E+0*P_P*l_qm*l_qp*svp*svm - 4.E+0*P_l*P_qm*l_qp*svp*svm - 
      4.E+0*P_l*P_qp*l_qm*svp*svm + 4.E+0*pow(P_l,2)*qm_qp*svp*svm + 
      4.E+0*pow(P_l,2)*ssp*ssm;

    kern[9][1] = 8.E+0*P_P*l_l*l_qm*l_qp*svp*svm - 4.E+0*P_P*pow(l_l,2)
      *qm_qp*svp*svm - 4.E+0*P_P*pow(l_l,2)*ssp*ssm + 4.E+0*pow(P_l,2)*
      l_l*qm_qp*svp*svm + 4.E+0*pow(P_l,2)*l_l*ssp*ssm - 8.E+0*pow(
      P_l,2)*l_qm*l_qp*svp*svm;

    kern[9][2] = 4.E+0*P_P*l_l*l_qm*ssp*svm + 4.E+0*P_P*l_l*l_qp*svp*
      ssm - 4.E+0*pow(P_l,2)*l_qm*ssp*svm - 4.E+0*pow(P_l,2)*l_qp*svp*
      ssm;

    kern[9][3] = 8.E+0*P_l*P_qm*l_l*i_*ssp*svm - 8.E+0*P_l*P_qp*l_l*i_*
      svp*ssm - 8.E+0*pow(P_l,2)*l_qm*i_*ssp*svm + 8.E+0*pow(P_l,2)*
      l_qp*i_*svp*ssm;

    kern[9][4] =  - 4.E+0*P_P*P_l*P_qm*l_qp*svp*svm - 4.E+0*P_P*P_l*
      P_qp*l_qm*svp*svm + 4.E+0*P_P*pow(P_l,2)*qm_qp*svp*svm + 4.E+0*
      P_P*pow(P_l,2)*ssp*ssm - 4.E+0*pow(P_P,2)*l_l*qm_qp*svp*svm - 
      4.E+0*pow(P_P,2)*l_l*ssp*ssm + 8.E+0*pow(P_P,2)*l_qm*l_qp*svp*svm
      ;

    kern[9][5] =  - 4.E+0*P_P*pow(P_l,2)*P_qm*l_l*ssp*svm + 4.E+0*P_P*
      pow(P_l,2)*P_qp*l_l*svp*ssm + 4.E+0*P_P*pow(P_l,3)*l_qm*ssp*svm
       - 4.E+0*P_P*pow(P_l,3)*l_qp*svp*ssm;

    kern[9][6] = 4.E+0*P_P*P_l*l_qm*ssp*svm - 4.E+0*P_P*P_l*l_qp*svp*
      ssm - 4.E+0*P_P*P_qm*l_l*ssp*svm + 4.E+0*P_P*P_qp*l_l*svp*ssm;

    kern[9][7] =  0;

    kern[9][8] = 4.E+0*P_P*pow(P_l,2)*l_qm*ssp*svm + 4.E+0*P_P*pow(
      P_l,2)*l_qp*svp*ssm - 4.E+0*pow(P_P,2)*l_l*l_qm*ssp*svm - 4.E+0*
      pow(P_P,2)*l_l*l_qp*svp*ssm;

    kern[9][9] = 4.E+0*P_P*pow(P_l,2)*l_l*qm_qp*svp*svm + 4.E+0*P_P*
      pow(P_l,2)*l_l*ssp*ssm - 8.E+0*P_P*pow(P_l,2)*l_qm*l_qp*svp*svm
       + 8.E+0*pow(P_P,2)*l_l*l_qm*l_qp*svp*svm - 4.E+0*pow(P_P,2)*pow(
      l_l,2)*qm_qp*svp*svm - 4.E+0*pow(P_P,2)*pow(l_l,2)*ssp*ssm;

    kern[9][10] =  - 4.E+0*P_P*P_l*P_qm*l_l*l_qp*svp*svm - 4.E+0*P_P*
      P_l*P_qp*l_l*l_qm*svp*svm + 8.E+0*P_P*pow(P_l,2)*l_qm*l_qp*svp*
      svm;

    kern[9][11] = 4.E+0*P_P*P_l*l_l*l_qm*ssp*svm - 4.E+0*P_P*P_l*l_l*
      l_qp*svp*ssm - 4.E+0*P_P*P_qm*pow(l_l,2)*ssp*svm + 4.E+0*P_P*P_qp
      *pow(l_l,2)*svp*ssm;

 
    kern[10][0] =  - 4.E+0*P_l*P_qm*l_qp*svp*svm - 4.E+0*P_l*P_qp*l_qm*
      svp*svm - 4.E+0*pow(P_l,2)*qm_qp*svp*svm - 1.2E+1*pow(P_l,2)*ssp*
      ssm;

    kern[10][1] =  - 4.E+0*P_l*P_qm*l_l*l_qp*svp*svm - 4.E+0*P_l*P_qp*
      l_l*l_qm*svp*svm + 8.E+0*pow(P_l,2)*l_qm*l_qp*svp*svm;

    kern[10][2] =  - 4.E+0*P_l*P_qm*l_l*ssp*svm - 4.E+0*P_l*P_qp*l_l*
      svp*ssm + 4.E+0*pow(P_l,2)*l_qm*ssp*svm + 4.E+0*pow(P_l,2)*l_qp*
      svp*ssm;

    kern[10][3] = 2.4E+1*pow(P_l,2)*l_qm*i_*ssp*svm - 2.4E+1*pow(P_l,2)
      *l_qp*i_*svp*ssm;

    kern[10][4] =  - 4.E+0*P_P*P_l*P_qm*l_qp*svp*svm - 4.E+0*P_P*P_l*
      P_qp*l_qm*svp*svm - 4.E+0*P_P*pow(P_l,2)*qm_qp*svp*svm - 1.2E+1*
      P_P*pow(P_l,2)*ssp*ssm;

    kern[10][5] =  - 1.2E+1*P_P*pow(P_l,3)*l_qm*ssp*svm + 1.2E+1*P_P*
      pow(P_l,3)*l_qp*svp*ssm;

    kern[10][6] =  - 4.E+0*P_P*P_l*l_qm*ssp*svm + 4.E+0*P_P*P_l*l_qp*
      svp*ssm - 8.E+0*pow(P_l,2)*P_qm*ssp*svm + 8.E+0*pow(P_l,2)*P_qp*
      svp*ssm;

    kern[10][7] =  - 8.E+0*pow(P_l,2)*P_qm*l_qp*svp*svm + 8.E+0*pow(
      P_l,2)*P_qp*l_qm*svp*svm;

    kern[10][8] = 4.E+0*P_P*P_l*P_qm*l_l*ssp*svm + 4.E+0*P_P*P_l*P_qp*
      l_l*svp*ssm - 4.E+0*P_P*pow(P_l,2)*l_qm*ssp*svm - 4.E+0*P_P*pow(
      P_l,2)*l_qp*svp*ssm;

    kern[10][9] =  - 4.E+0*P_P*P_l*P_qm*l_l*l_qp*svp*svm - 4.E+0*P_P*
      P_l*P_qp*l_l*l_qm*svp*svm + 8.E+0*P_P*pow(P_l,2)*l_qm*l_qp*svp*
      svm;

    kern[10][10] =  - 4.E+0*P_P*pow(P_l,2)*l_l*qm_qp*svp*svm - 4.E+0*
      P_P*pow(P_l,2)*l_l*ssp*ssm + 8.E+0*pow(P_l,2)*P_qm*P_qp*l_l*svp*
      svm - 8.E+0*pow(P_l,3)*P_qm*l_qp*svp*svm - 8.E+0*pow(P_l,3)*P_qp*
      l_qm*svp*svm - 8.E+0*pow(P_l,4)*ssp*ssm;

    kern[10][11] =  - 4.E+0*P_P*P_l*l_l*l_qm*ssp*svm + 4.E+0*P_P*P_l*
      l_l*l_qp*svp*ssm - 8.E+0*pow(P_l,3)*l_qm*ssp*svm + 8.E+0*pow(
      P_l,3)*l_qp*svp*ssm;

 
    kern[11][0] =  - 8.E+0*P_l*l_qm*ssp*svm + 8.E+0*P_l*l_qp*svp*ssm - 
      4.E+0*P_qm*l_l*ssp*svm + 4.E+0*P_qp*l_l*svp*ssm;

    kern[11][1] = 4.E+0*P_l*l_l*l_qm*ssp*svm - 4.E+0*P_l*l_l*l_qp*svp*
      ssm - 4.E+0*P_qm*pow(l_l,2)*ssp*svm + 4.E+0*P_qp*pow(l_l,2)*svp*
      ssm;

    kern[11][2] = 4.E+0*P_qm*l_l*l_qp*svp*svm - 4.E+0*P_qp*l_l*l_qm*svp
      *svm;

    kern[11][3] = 8.E+0*P_l*l_l*qm_qp*i_*svp*svm - 2.4E+1*P_l*l_l*i_*
      ssp*ssm - 3.2E+1*P_l*l_qm*l_qp*i_*svp*svm;

    kern[11][4] =  - 8.E+0*P_P*P_l*l_qm*ssp*svm + 8.E+0*P_P*P_l*l_qp*
      svp*ssm - 4.E+0*P_P*P_qm*l_l*ssp*svm + 4.E+0*P_P*P_qp*l_l*svp*ssm
      ;

    kern[11][5] =  - 4.E+0*P_P*pow(P_l,2)*l_l*qm_qp*svp*svm + 1.2E+1*
      P_P*pow(P_l,2)*l_l*ssp*ssm + 1.6E+1*P_P*pow(P_l,2)*l_qm*l_qp*svp*
      svm;

    kern[11][6] =  - 4.E+0*P_P*l_l*qm_qp*svp*svm + 4.E+0*P_P*l_l*ssp*
      ssm + 8.E+0*P_P*l_qm*l_qp*svp*svm + 8.E+0*pow(P_l,2)*ssp*ssm + 
      8.E+0*P_qm*P_qp*l_l*svp*svm;

    kern[11][7] =  - 8.E+0*P_l*P_qm*l_l*ssp*svm - 8.E+0*P_l*P_qp*l_l*
      svp*ssm + 8.E+0*pow(P_l,2)*l_qm*ssp*svm + 8.E+0*pow(P_l,2)*l_qp*
      svp*ssm;

    kern[11][8] =  - 4.E+0*P_P*P_qm*l_l*l_qp*svp*svm + 4.E+0*P_P*P_qp*
      l_l*l_qm*svp*svm;

    kern[11][9] = 4.E+0*P_P*P_l*l_l*l_qm*ssp*svm - 4.E+0*P_P*P_l*l_l*
      l_qp*svp*ssm - 4.E+0*P_P*P_qm*pow(l_l,2)*ssp*svm + 4.E+0*P_P*P_qp
      *pow(l_l,2)*svp*ssm;

    kern[11][10] =  - 4.E+0*P_P*P_l*l_l*l_qm*ssp*svm + 4.E+0*P_P*P_l*
      l_l*l_qp*svp*ssm - 8.E+0*pow(P_l,3)*l_qm*ssp*svm + 8.E+0*pow(
      P_l,3)*l_qp*svp*ssm;

    kern[11][11] = 8.E+0*P_P*l_l*l_qm*l_qp*svp*svm - 4.E+0*P_P*pow(
      l_l,2)*qm_qp*svp*svm + 4.E+0*P_P*pow(l_l,2)*ssp*ssm - 8.E+0*P_l*
      P_qm*l_l*l_qp*svp*svm - 8.E+0*P_l*P_qp*l_l*l_qm*svp*svm + 8.E+0*
      pow(P_l,2)*l_l*ssp*ssm + 1.6E+1*pow(P_l,2)*l_qm*l_qp*svp*svm + 
      8.E+0*P_qm*P_qp*pow(l_l,2)*svp*svm;


}

void normalisation_qphv(matCdoub& kern,
  Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
  Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
  const Cdoub& svp, const Cdoub& ssp, 
  const Cdoub& svm, const Cdoub& ssm ){
 
 
     // local variables
    array<Cdoub, 91> w;
 
 
    w[1]=qm_qp;
    w[2]=l_l;
    w[3]=l_qm;
    w[4]=l_qp;
    w[5]=P_P;
    w[6]=P_qm;
    w[7]=P_qp;
    w[8]=P_l;
   w[9]=svm*svp;
   w[10]=w[9]*w[1];
   w[11]=ssp*ssm;
   w[12]= - 2.E+0*w[11] - w[10];
   w[12]=8.E+0*w[12];
   w[13]=w[10] + w[11];
   w[14]=w[13]*w[2];
   w[15]=w[9]*w[4];
   w[16]=w[15]*w[3];
   w[17]=2.E+0*w[16];
   w[14]=w[14] - w[17];
   w[18]=4.E+0*w[14];
   w[19]=ssm*svp;
   w[20]=w[4]*w[19];
   w[21]=svm*ssp;
   w[22]=w[3]*w[21];
   w[23]=w[20] + w[22];
   w[24]=4.E+0*w[23];
   w[20]=w[20] - w[22];
   w[22]=w[20]*i_;
   w[22]=2.4E+1*w[22];
   w[25]=3.E+0*w[11];
   w[26]=w[25] + w[10];
   w[27]=w[26]*w[5];
   w[9]=w[9]*w[7];
   w[28]=w[6]*w[9];
   w[27]=w[27] + 2.E+0*w[28];
   w[29]=4.E+0*w[27];
   w[30]=w[20]*w[8];
   w[31]=2.E+0*w[5];
   w[32]=w[31]*w[30];
   w[21]=w[6]*w[21];
   w[19]=w[7]*w[19];
   w[33]=w[21] - w[19];
   w[34]=pow(w[8],2);
   w[35]=w[33]*w[34];
   w[32]=w[32] - w[35];
   w[36]=4.E+0*w[32];
   w[37]=1.2E+1*w[33];
   w[15]=w[15]*w[6];
   w[9]=w[3]*w[9];
   w[38]=w[15] - w[9];
   w[39]=8.E+0*w[38];
   w[19]=w[21] + w[19];
   w[21]=w[19]*w[8];
   w[40]=w[23]*w[5];
   w[21]=w[21] - w[40];
   w[40]=4.E+0*w[21];
   w[41]=w[13]*w[8];
   w[9]=w[15] + w[9];
   w[15]=w[41] - w[9];
   w[15]=w[15]*w[8];
   w[41]=w[14]*w[5];
   w[15]=w[15] - w[41];
   w[41]=4.E+0*w[15];
   w[26]=w[26]*w[8];
   w[26]=w[26] + w[9];
   w[42]=4.E+0*w[8];
   w[43]=w[26]*w[42];
   w[44]=w[33]*w[2];
   w[45]= - w[44] + 2.E+0*w[30];
   w[46]=4.E+0*w[45];
   w[47]=4.E+0*w[2];
   w[48]= - w[14]*w[47];
   w[49]=w[47]*w[23];
   w[44]=w[44] + w[30];
   w[50]=w[44]*w[34];
   w[50]=4.E+0*w[50];
   w[51]=4.E+0*w[44];
   w[52]=w[5]*w[2];
   w[53]=w[52] - w[34];
   w[54]= - w[23]*w[53];
   w[55]=4.E+0*w[54];
   w[14]= - w[14]*w[53];
   w[56]=4.E+0*w[14];
   w[57]=w[9]*w[2];
   w[58]=w[17]*w[8];
   w[58]=w[58] - w[57];
   w[59]=w[58]*w[42];
   w[60]=w[30]*w[2];
   w[61]=pow(w[2],2);
   w[62]=w[61]*w[33];
   w[60]=w[60] + w[62];
   w[62]=4.E+0*w[60];
   w[63]=w[10] - w[11];
   w[47]= - w[63]*w[47];
   w[64]=4.E+0*w[38];
   w[65]=w[64]*w[34];
   w[53]= - w[63]*w[53];
   w[66]=4.E+0*w[53];
   w[23]=w[23]*w[8];
   w[19]=w[19]*w[2];
   w[19]=w[23] - w[19];
   w[23]=w[42]*w[19];
   w[42]=w[64]*w[2];
   w[10]=w[25] - w[10];
   w[25]=w[10]*w[2];
   w[25]=w[25] + 4.E+0*w[16];
   w[67]=1.6E+1*w[25];
   w[33]=w[33]*w[8];
   w[68]=w[31]*w[20];
   w[33]=w[33] - w[68];
   w[68]=8.E+0*i_;
   w[33]=w[33]*w[68];
   w[69]=w[63]*w[8];
   w[70]=2.E+0*w[9];
   w[69]=w[69] - w[70];
   w[69]=w[69]*w[8];
   w[71]=w[11]*w[2];
   w[72]=w[71] + w[16];
   w[31]=w[72]*w[31];
   w[72]=w[28]*w[2];
   w[31]= - w[31] + w[69] + 2.E+0*w[72];
   w[69]=w[68]*w[8];
   w[73]=w[31]*w[69];
   w[74]=w[10]*w[8];
   w[70]=w[74] + w[70];
   w[68]=w[70]*w[68];
   w[74]=w[19]*i_;
   w[74]=1.6E+1*w[74];
   w[75]=w[69]*w[38];
   w[76]=w[69]*w[44];
   w[77]=w[22]*w[34];
   w[69]=w[25]*w[69];
   w[78]=4.E+0*w[5];
   w[27]= - w[27]*w[78];
   w[32]=w[32]*w[78];
   w[79]=w[37]*w[5];
   w[80]=w[39]*w[5];
   w[81]=w[21]*w[78];
   w[15]=w[15]*w[78];
   w[82]=w[78]*w[8];
   w[26]=w[26]*w[82];
   w[45]=w[45]*w[78];
   w[83]=w[78]*w[34];
   w[31]= - w[31]*w[83];
   w[70]=w[70]*w[82];
   w[84]=8.E+0*w[8];
   w[84]=w[19]*w[84];
   w[85]=w[84]*w[5];
   w[38]=w[38]*w[78];
   w[86]=w[38]*w[34];
   w[44]=w[44]*w[78];
   w[87]=w[44]*w[34];
   w[20]=w[20]*pow(w[8],3);
   w[88]=1.2E+1*w[5];
   w[88]=w[88]*w[20];
   w[25]=w[25]*w[83];
   w[10]=w[5]*w[10];
   w[10]=4.E+0*w[28] + w[10];
   w[10]=4.E+0*w[10];
   w[21]=8.E+0*w[21];
   w[83]=w[30]*w[5];
   w[35]= - w[83] + 2.E+0*w[35];
   w[35]=4.E+0*w[35];
   w[83]=w[34]*w[11];
   w[83]=w[83] + w[72];
   w[63]=w[63]*w[2];
   w[63]=w[63] - w[17];
   w[89]=w[63]*w[5];
   w[83]= - w[89] + 2.E+0*w[83];
   w[83]=4.E+0*w[83];
   w[11]=w[11]*w[8];
   w[89]=w[11] - w[9];
   w[89]=w[8]*w[89];
   w[16]=w[16] - w[71];
   w[16]=w[5]*w[16];
   w[16]=w[16] + w[72] + w[89];
   w[16]=8.E+0*w[16];
   w[89]=w[39]*w[34];
   w[53]=w[53]*w[78];
   w[54]=w[54]*w[78];
   w[19]=w[82]*w[19];
   w[90]=w[38]*w[2];
   w[14]=w[14]*w[78];
   w[58]=w[58]*w[82];
   w[60]=w[60]*w[78];
   w[9]= - w[11] - w[9];
   w[9]=w[8]*w[9];
   w[9]=w[72] + w[9];
   w[11]= - w[13]*w[52];
   w[9]=2.E+0*w[9] + w[11];
   w[9]=4.E+0*w[34]*w[9];
   w[11]=w[52]*w[30];
   w[11]=w[11] + 2.E+0*w[20];
   w[11]=4.E+0*w[11];
   w[13]=w[17] + w[71];
   w[13]=w[8]*w[13];
   w[13]= - w[57] + w[13];
   w[13]=w[8]*w[13];
   w[17]=w[61]*w[28];
   w[13]=w[17] + w[13];
   w[17]= - w[63]*w[52];
   w[13]=2.E+0*w[13] + w[17];
   w[13]=4.E+0*w[13];

 
    kern[0][0] = w[12];

    kern[0][1] =  - w[18];

    kern[0][2] = w[24];

    kern[0][3] =  - w[22];

    kern[0][4] =  - w[29];

    kern[0][5] = w[36];

    kern[0][6] =  - w[37];

    kern[0][7] =  - w[39];

    kern[0][8] = w[40];

    kern[0][9] = w[41];

    kern[0][10] =  - w[43];

    kern[0][11] = w[46];

 
    kern[1][0] =  - w[18];

    kern[1][1] = w[48];

    kern[1][2] = w[49];

    kern[1][3] =  0;

    kern[1][4] = w[41];

    kern[1][5] =  - w[50];

    kern[1][6] =  - w[51];

    kern[1][7] =  0;

    kern[1][8] = w[55];

    kern[1][9] = w[56];

    kern[1][10] = w[59];

    kern[1][11] =  - w[62];

 
    kern[2][0] = w[24];

    kern[2][1] = w[49];

    kern[2][2] = w[47];

    kern[2][3] =  0;

    kern[2][4] =  - w[40];

    kern[2][5] = w[65];

    kern[2][6] = w[64];

    kern[2][7] =  0;

    kern[2][8] =  - w[66];

    kern[2][9] =  - w[55];

    kern[2][10] = w[23];

    kern[2][11] = w[42];

 
    kern[3][0] = w[22];

    kern[3][1] =  0;

    kern[3][2] =  0;

    kern[3][3] = w[67];

    kern[3][4] =  - w[33];

    kern[3][5] =  - w[73];

    kern[3][6] = w[68];

    kern[3][7] = w[74];

    kern[3][8] =  - w[75];

    kern[3][9] =  - w[76];

    kern[3][10] = w[77];

    kern[3][11] = w[69];

 
    kern[4][0] =  - w[29];

    kern[4][1] = w[41];

    kern[4][2] =  - w[40];

    kern[4][3] = w[33];

    kern[4][4] = w[27];

    kern[4][5] = w[32];

    kern[4][6] =  - w[79];

    kern[4][7] =  - w[80];

    kern[4][8] = w[81];

    kern[4][9] = w[15];

    kern[4][10] =  - w[26];

    kern[4][11] = w[45];

 
    kern[5][0] = w[36];

    kern[5][1] =  - w[50];

    kern[5][2] = w[65];

    kern[5][3] = w[73];

    kern[5][4] = w[32];

    kern[5][5] = w[31];

    kern[5][6] = w[70];

    kern[5][7] = w[85];

    kern[5][8] =  - w[86];

    kern[5][9] =  - w[87];

    kern[5][10] = w[88];

    kern[5][11] = w[25];

 
    kern[6][0] =  - w[37];

    kern[6][1] =  - w[51];

    kern[6][2] = w[64];

    kern[6][3] =  - w[68];

    kern[6][4] =  - w[79];

    kern[6][5] = w[70];

    kern[6][6] = w[10];

    kern[6][7] =  - w[21];

    kern[6][8] =  - w[38];

    kern[6][9] =  - w[44];

    kern[6][10] =  - w[35];

    kern[6][11] = w[83];

 
    kern[7][0] =  - w[39];

    kern[7][1] =  0;

    kern[7][2] =  0;

    kern[7][3] =  - w[74];

    kern[7][4] =  - w[80];

    kern[7][5] = w[85];

    kern[7][6] =  - w[21];

    kern[7][7] = w[16];

    kern[7][8] =  0;

    kern[7][9] =  0;

    kern[7][10] =  - w[89];

    kern[7][11] = w[84];

 
    kern[8][0] = w[40];

    kern[8][1] = w[55];

    kern[8][2] =  - w[66];

    kern[8][3] = w[75];

    kern[8][4] = w[81];

    kern[8][5] =  - w[86];

    kern[8][6] =  - w[38];

    kern[8][7] =  0;

    kern[8][8] = w[53];

    kern[8][9] = w[54];

    kern[8][10] =  - w[19];

    kern[8][11] =  - w[90];

 
    kern[9][0] = w[41];

    kern[9][1] = w[56];

    kern[9][2] =  - w[55];

    kern[9][3] = w[76];

    kern[9][4] = w[15];

    kern[9][5] =  - w[87];

    kern[9][6] =  - w[44];

    kern[9][7] =  0;

    kern[9][8] = w[54];

    kern[9][9] = w[14];

    kern[9][10] = w[58];

    kern[9][11] =  - w[60];

 
    kern[10][0] =  - w[43];

    kern[10][1] = w[59];

    kern[10][2] = w[23];

    kern[10][3] =  - w[77];

    kern[10][4] =  - w[26];

    kern[10][5] = w[88];

    kern[10][6] =  - w[35];

    kern[10][7] =  - w[89];

    kern[10][8] =  - w[19];

    kern[10][9] = w[58];

    kern[10][10] = w[9];

    kern[10][11] = w[11];

 
    kern[11][0] = w[46];

    kern[11][1] =  - w[62];

    kern[11][2] = w[42];

    kern[11][3] =  - w[69];

    kern[11][4] = w[45];

    kern[11][5] = w[25];

    kern[11][6] = w[83];

    kern[11][7] = w[84];

    kern[11][8] =  - w[90];

    kern[11][9] =  - w[60];

    kern[11][10] = w[11];

    kern[11][11] = w[13];


}

