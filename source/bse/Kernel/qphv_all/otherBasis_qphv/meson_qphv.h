#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
//#include <math_ops.h>

void kernelL_notopti_qphv(matCdoub& kern,
                              const ArrayScalarProducts& sp,
                const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){


    // local variables
    Cdoub denom=0.0;


    denom =  - 4.E+0*sp[35]*sp[27] + 4.E+0*pow(sp[34],2);

    kern[0][0] =  4.E+0*sp[35]*sp[27]*ang[1] - 1.2E+1*sp[34]*sp[43]*sp[27]*ang[0] - 4.E+0*
      sp[34]*sp[43]*sp[44]*ang[0] + 4.E+0*sp[34]*sp[43]*ang[1] + 4.E+0*pow(sp[34],2)*sp[27]*ang[0]
       + 4.E+0*pow(sp[34],2)*sp[44]*ang[0] - 8.E+0*pow(sp[34],2)*ang[1] + 8.E+0*pow(
      sp[43],2)*sp[27]*ang[0];
 
    kern[0][0] = (1.0/ denom)* kern[0][0]; 
    kern[0][1] =   - 6.E+0*sp[34]*sp[43]*sp[27]*ang[1] + 2.E+0*sp[34]*sp[43]*pow(sp[27],2)*
      ang[0] - 2.E+0*sp[34]*sp[43]*sp[44]*ang[1] - 2.E+0*sp[34]*sp[43]*pow(sp[44],2)*ang[0] + 
      4.E+0*sp[34]*sp[43]*ang[2] + 4.E+0*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 8.E+0*pow(
      sp[43],2)*sp[27]*ang[1] - 4.E+0*pow(sp[43],2)*pow(sp[27],2)*ang[0];
 
    kern[0][1] = (1.0/ denom)* kern[0][1]; 
    kern[0][2] =   0;
 
    kern[0][2] = (1.0/ denom)* kern[0][2]; 
    kern[0][3] =   0;
 
    kern[0][3] = (1.0/ denom)* kern[0][3]; 
    kern[0][4] =   0;
 
    kern[0][4] = (1.0/ denom)* kern[0][4]; 
    kern[0][5] =   0;
 
    kern[0][5] = (1.0/ denom)* kern[0][5]; 
    kern[0][6] =   0;
 
    kern[0][6] = (1.0/ denom)* kern[0][6]; 
    kern[0][7] =   0;
 
    kern[0][7] = (1.0/ denom)* kern[0][7]; 
    kern[0][8] =   0;
 
    kern[0][8] = (1.0/ denom)* kern[0][8]; 
    kern[0][9] =   0;
 
    kern[0][9] = (1.0/ denom)* kern[0][9]; 
    kern[0][10] =   0;
 
    kern[0][10] = (1.0/ denom)* kern[0][10]; 
    kern[0][11] =   0;
 
    kern[0][11] = (1.0/ denom)* kern[0][11]; 

    denom =  - 4.E+0*sp[35]*sp[34]*sp[27] + 4.E+0*pow(sp[34],3);

    kern[1][0] =  4.E+0*sp[35]*sp[34]*sp[27]*ang[0] - 4.E+0*sp[35]*sp[34]*sp[44]*ang[0] + 
      4.E+0*sp[35]*sp[34]*ang[1] - 4.E+0*sp[35]*sp[43]*sp[27]*ang[0] + 4.E+0*sp[35]*sp[43]*sp[44]*
      ang[0] - 4.E+0*sp[35]*sp[43]*ang[1] - 8.E+0*sp[34]*pow(sp[43],2)*ang[0] + 1.6E+1*
      pow(sp[34],2)*sp[43]*ang[0] - 8.E+0*pow(sp[34],3)*ang[0];
 
    kern[1][0] = (1.0/ denom)* kern[1][0]; 
    kern[1][1] =   - 4.E+0*sp[35]*sp[43]*sp[27]*sp[44]*ang[0] + 2.E+0*sp[35]*sp[43]*sp[27]*
      ang[1] + 2.E+0*sp[35]*sp[43]*pow(sp[27],2)*ang[0] + 2.E+0*sp[35]*sp[43]*sp[44]*ang[1] + 
      2.E+0*sp[35]*sp[43]*pow(sp[44],2)*ang[0] - 4.E+0*sp[35]*sp[43]*ang[2] + 4.E+0*sp[34]*
      pow(sp[43],2)*sp[27]*ang[0] - 4.E+0*sp[34]*pow(sp[43],2)*sp[44]*ang[0] - 8.E+0*sp[34]*
      pow(sp[43],2)*ang[1] - 4.E+0*pow(sp[34],2)*sp[43]*sp[27]*ang[0] + 4.E+0*pow(
      sp[34],2)*sp[43]*sp[44]*ang[0] + 4.E+0*pow(sp[34],2)*sp[43]*ang[1];
 
    kern[1][1] = (1.0/ denom)* kern[1][1]; 
    kern[1][2] =   0;
 
    kern[1][2] = (1.0/ denom)* kern[1][2]; 
    kern[1][3] =   0;
 
    kern[1][3] = (1.0/ denom)* kern[1][3]; 
    kern[1][4] =   0;
 
    kern[1][4] = (1.0/ denom)* kern[1][4]; 
    kern[1][5] =   0;
 
    kern[1][5] = (1.0/ denom)* kern[1][5]; 
    kern[1][6] =   0;
 
    kern[1][6] = (1.0/ denom)* kern[1][6]; 
    kern[1][7] =   0;
 
    kern[1][7] = (1.0/ denom)* kern[1][7]; 
    kern[1][8] =   0;
 
    kern[1][8] = (1.0/ denom)* kern[1][8]; 
    kern[1][9] =   0;
 
    kern[1][9] = (1.0/ denom)* kern[1][9]; 
    kern[1][10] =   0;
 
    kern[1][10] = (1.0/ denom)* kern[1][10]; 
    kern[1][11] =   0;
 
    kern[1][11] = (1.0/ denom)* kern[1][11]; 

    denom = 4.E+0*sp[34];

    kern[2][0] =   0;
 
    kern[2][0] = (1.0/ denom)* kern[2][0]; 
    kern[2][1] =   0;
 
    kern[2][1] = (1.0/ denom)* kern[2][1]; 
    kern[2][2] =  1.2E+1*sp[43]*ang[1];
 
    kern[2][2] = (1.0/ denom)* kern[2][2]; 
    kern[2][3] =   0;
 
    kern[2][3] = (1.0/ denom)* kern[2][3]; 
    kern[2][4] =   0;
 
    kern[2][4] = (1.0/ denom)* kern[2][4]; 
    kern[2][5] =   0;
 
    kern[2][5] = (1.0/ denom)* kern[2][5]; 
    kern[2][6] =   0;
 
    kern[2][6] = (1.0/ denom)* kern[2][6]; 
    kern[2][7] =   0;
 
    kern[2][7] = (1.0/ denom)* kern[2][7]; 
    kern[2][8] =   0;
 
    kern[2][8] = (1.0/ denom)* kern[2][8]; 
    kern[2][9] =   0;
 
    kern[2][9] = (1.0/ denom)* kern[2][9]; 
    kern[2][10] =   0;
 
    kern[2][10] = (1.0/ denom)* kern[2][10]; 
    kern[2][11] =   0;
 
    kern[2][11] = (1.0/ denom)* kern[2][11]; 

    denom =  - 1.6E+1*sp[35]*sp[27] + 1.6E+1*pow(sp[34],2);

    kern[3][0] =   0;
 
    kern[3][0] = (1.0/ denom)* kern[3][0]; 
    kern[3][1] =   0;
 
    kern[3][1] = (1.0/ denom)* kern[3][1]; 
    kern[3][2] =   0;
 
    kern[3][2] = (1.0/ denom)* kern[3][2]; 
    kern[3][3] =  1.6E+1*sp[35]*sp[27]*sp[44]*ang[0] + 8.E+0*sp[35]*sp[27]*ang[1] - 8.E+0*
      sp[35]*pow(sp[27],2)*ang[0] + 8.E+0*sp[35]*sp[44]*ang[1] - 8.E+0*sp[35]*pow(sp[44],2)*
      ang[0] + 3.2E+1*sp[34]*sp[43]*sp[27]*ang[0] + 3.2E+1*sp[34]*sp[43]*sp[44]*ang[0] - 1.6E+1
      *sp[34]*sp[43]*ang[1] - 3.2E+1*pow(sp[34],2)*sp[44]*ang[0] - 3.2E+1*pow(sp[43],2)*
      sp[27]*ang[0];
 
    kern[3][3] = (1.0/ denom)* kern[3][3]; 
    kern[3][4] =   0;
 
    kern[3][4] = (1.0/ denom)* kern[3][4]; 
    kern[3][5] =   0;
 
    kern[3][5] = (1.0/ denom)* kern[3][5]; 
    kern[3][6] =   0;
 
    kern[3][6] = (1.0/ denom)* kern[3][6]; 
    kern[3][7] =   0;
 
    kern[3][7] = (1.0/ denom)* kern[3][7]; 
    kern[3][8] =   0;
 
    kern[3][8] = (1.0/ denom)* kern[3][8]; 
    kern[3][9] =   0;
 
    kern[3][9] = (1.0/ denom)* kern[3][9]; 
    kern[3][10] =   0;
 
    kern[3][10] = (1.0/ denom)* kern[3][10]; 
    kern[3][11] =   0;
 
    kern[3][11] = (1.0/ denom)* kern[3][11]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[34],2)*sp[27] + 8.E+0*pow(sp[35],2)*pow(sp[27],2)
       + 8.E+0*pow(sp[34],4);

    kern[4][0] =   - 4.E+0*sp[35]*sp[27]*sp[44]*ang[1] + 2.E+0*sp[35]*sp[27]*pow(sp[44],2)*
      ang[0] + 2.E+0*sp[35]*sp[27]*ang[2] - 4.E+0*sp[35]*pow(sp[27],2)*sp[44]*ang[0] - 4.E+0
      *sp[35]*pow(sp[27],2)*ang[1] + 2.E+0*sp[35]*pow(sp[27],3)*ang[0] - 2.4E+1*sp[34]*sp[43]
      *sp[27]*sp[44]*ang[0] + 2.4E+1*sp[34]*sp[43]*sp[27]*ang[1] - 2.4E+1*sp[34]*sp[43]*pow(
      sp[27],2)*ang[0] + 1.6E+1*pow(sp[34],2)*sp[27]*sp[44]*ang[0] - 8.E+0*pow(sp[34],2)*
      sp[27]*ang[1] + 4.E+0*pow(sp[34],2)*pow(sp[27],2)*ang[0] - 8.E+0*pow(sp[34],2)*
      sp[44]*ang[1] + 4.E+0*pow(sp[34],2)*pow(sp[44],2)*ang[0] + 4.E+0*pow(sp[34],2)*
      ang[2] + 2.4E+1*pow(sp[43],2)*pow(sp[27],2)*ang[0];
 
    kern[4][0] = (1.0/ denom)* kern[4][0]; 
    kern[4][1] =   - 3.E+0*sp[35]*sp[27]*sp[44]*ang[2] + sp[35]*sp[27]*pow(sp[44],3)*ang[0]
       + 2.E+0*sp[35]*sp[27]*ang[3] - 4.E+0*sp[35]*pow(sp[27],2)*sp[44]*ang[1] - 3.E+0*sp[35]
      *pow(sp[27],2)*pow(sp[44],2)*ang[0] - 5.E+0*sp[35]*pow(sp[27],2)*ang[2] + 3.E+0*
      sp[35]*pow(sp[27],3)*sp[44]*ang[0] + 4.E+0*sp[35]*pow(sp[27],3)*ang[1] - sp[35]*pow(
      sp[27],4)*ang[0] - 1.2E+1*sp[34]*sp[43]*sp[27]*sp[44]*ang[1] - 1.2E+1*sp[34]*sp[43]*sp[27]*
      pow(sp[44],2)*ang[0] + 2.4E+1*sp[34]*sp[43]*sp[27]*ang[2] - 3.6E+1*sp[34]*sp[43]*pow(
      sp[27],2)*ang[1] + 1.2E+1*sp[34]*sp[43]*pow(sp[27],3)*ang[0] + 1.6E+1*pow(sp[34],2)*
      sp[27]*sp[44]*ang[1] + 6.E+0*pow(sp[34],2)*sp[27]*pow(sp[44],2)*ang[0] - 1.E+1*pow(
      sp[34],2)*sp[27]*ang[2] - 6.E+0*pow(sp[34],2)*pow(sp[27],2)*sp[44]*ang[0] + 8.E+0*
      pow(sp[34],2)*pow(sp[27],2)*ang[1] - 2.E+0*pow(sp[34],2)*pow(sp[27],3)*ang[0] - 
      6.E+0*pow(sp[34],2)*sp[44]*ang[2] + 2.E+0*pow(sp[34],2)*pow(sp[44],3)*ang[0] + 
      4.E+0*pow(sp[34],2)*ang[3] + 1.2E+1*pow(sp[43],2)*pow(sp[27],2)*sp[44]*ang[0] + 
      2.4E+1*pow(sp[43],2)*pow(sp[27],2)*ang[1] - 1.2E+1*pow(sp[43],2)*pow(sp[27],3)*
      ang[0];
 
    kern[4][1] = (1.0/ denom)* kern[4][1]; 
    kern[4][2] =   0;
 
    kern[4][2] = (1.0/ denom)* kern[4][2]; 
    kern[4][3] =   0;
 
    kern[4][3] = (1.0/ denom)* kern[4][3]; 
    kern[4][4] =   - 1.6E+1*sp[35]*sp[34]*sp[43]*sp[27]*sp[44]*ang[0] + 1.6E+1*sp[35]*sp[34]*
      sp[43]*sp[27]*ang[1] + 8.E+0*sp[35]*pow(sp[34],2)*sp[27]*sp[44]*ang[0] + 1.6E+1*sp[35]*
      pow(sp[34],2)*sp[27]*ang[1] - 4.E+0*sp[35]*pow(sp[34],2)*pow(sp[27],2)*ang[0] - 
      8.E+0*sp[35]*pow(sp[34],2)*sp[44]*ang[1] + 4.E+0*sp[35]*pow(sp[34],2)*pow(sp[44],2)*
      ang[0] + 4.E+0*sp[35]*pow(sp[34],2)*ang[2] + 8.E+0*sp[35]*pow(sp[43],2)*pow(
      sp[27],2)*ang[0] - 4.E+0*pow(sp[35],2)*sp[27]*sp[44]*ang[1] + 2.E+0*pow(sp[35],2)*
      sp[27]*pow(sp[44],2)*ang[0] + 2.E+0*pow(sp[35],2)*sp[27]*ang[2] - 4.E+0*pow(
      sp[35],2)*pow(sp[27],2)*sp[44]*ang[0] - 1.2E+1*pow(sp[35],2)*pow(sp[27],2)*ang[1] + 
      2.E+0*pow(sp[35],2)*pow(sp[27],3)*ang[0] + 1.6E+1*pow(sp[34],2)*pow(sp[43],2)*
      sp[27]*ang[0] - 2.4E+1*pow(sp[34],3)*sp[43]*sp[27]*ang[0] - 8.E+0*pow(sp[34],3)*sp[43]*
      sp[44]*ang[0] + 8.E+0*pow(sp[34],3)*sp[43]*ang[1] + 8.E+0*pow(sp[34],4)*sp[27]*ang[0]
       + 8.E+0*pow(sp[34],4)*sp[44]*ang[0] - 1.6E+1*pow(sp[34],4)*ang[1];
 
    kern[4][4] = (1.0/ denom)* kern[4][4]; 
    kern[4][5] =   0;
 
    kern[4][5] = (1.0/ denom)* kern[4][5]; 
    kern[4][6] =   0;
 
    kern[4][6] = (1.0/ denom)* kern[4][6]; 
    kern[4][7] =   0;
 
    kern[4][7] = (1.0/ denom)* kern[4][7]; 
    kern[4][8] =   0;
 
    kern[4][8] = (1.0/ denom)* kern[4][8]; 
    kern[4][9] =   - 8.E+0*sp[35]*sp[34]*sp[43]*sp[27]*sp[44]*ang[1] - 8.E+0*sp[35]*sp[34]*sp[43]
      *sp[27]*pow(sp[44],2)*ang[0] + 1.6E+1*sp[35]*sp[34]*sp[43]*sp[27]*ang[2] - 2.4E+1*sp[35]*
      sp[34]*sp[43]*pow(sp[27],2)*ang[1] + 8.E+0*sp[35]*sp[34]*sp[43]*pow(sp[27],3)*ang[0] + 
      1.6E+1*sp[35]*pow(sp[34],2)*sp[27]*sp[44]*ang[1] + 6.E+0*sp[35]*pow(sp[34],2)*sp[27]*
      pow(sp[44],2)*ang[0] - 1.E+1*sp[35]*pow(sp[34],2)*sp[27]*ang[2] - 6.E+0*sp[35]*pow(
      sp[34],2)*pow(sp[27],2)*sp[44]*ang[0] + 8.E+0*sp[35]*pow(sp[34],2)*pow(sp[27],2)*ang[1]
       - 2.E+0*sp[35]*pow(sp[34],2)*pow(sp[27],3)*ang[0] - 6.E+0*sp[35]*pow(sp[34],2)*
      sp[44]*ang[2] + 2.E+0*sp[35]*pow(sp[34],2)*pow(sp[44],3)*ang[0] + 4.E+0*sp[35]*pow(
      sp[34],2)*ang[3] + 4.E+0*sp[35]*pow(sp[43],2)*pow(sp[27],2)*sp[44]*ang[0] + 8.E+0*
      sp[35]*pow(sp[43],2)*pow(sp[27],2)*ang[1] - 4.E+0*sp[35]*pow(sp[43],2)*pow(sp[27],3)*
      ang[0] - 3.E+0*pow(sp[35],2)*sp[27]*sp[44]*ang[2] + pow(sp[35],2)*sp[27]*pow(sp[44],3)*
      ang[0] + 2.E+0*pow(sp[35],2)*sp[27]*ang[3] - 4.E+0*pow(sp[35],2)*pow(sp[27],2)*
      sp[44]*ang[1] - 3.E+0*pow(sp[35],2)*pow(sp[27],2)*pow(sp[44],2)*ang[0] - 5.E+0*
      pow(sp[35],2)*pow(sp[27],2)*ang[2] + 3.E+0*pow(sp[35],2)*pow(sp[27],3)*sp[44]*ang[0]
       + 4.E+0*pow(sp[35],2)*pow(sp[27],3)*ang[1] - pow(sp[35],2)*pow(sp[27],4)*ang[0]
       + 8.E+0*pow(sp[34],2)*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 1.6E+1*pow(sp[34],2)*
      pow(sp[43],2)*sp[27]*ang[1] - 8.E+0*pow(sp[34],2)*pow(sp[43],2)*pow(sp[27],2)*ang[0]
       - 1.2E+1*pow(sp[34],3)*sp[43]*sp[27]*ang[1] + 4.E+0*pow(sp[34],3)*sp[43]*pow(
      sp[27],2)*ang[0] - 4.E+0*pow(sp[34],3)*sp[43]*sp[44]*ang[1] - 4.E+0*pow(sp[34],3)*
      sp[43]*pow(sp[44],2)*ang[0] + 8.E+0*pow(sp[34],3)*sp[43]*ang[2];
 
    kern[4][9] = (1.0/ denom)* kern[4][9]; 
    kern[4][10] =  2.E+0*sp[35]*sp[34]*sp[43]*sp[27]*pow(sp[44],2)*ang[0] - 2.E+0*sp[35]*
      sp[34]*sp[43]*sp[27]*ang[2] - 4.E+0*sp[35]*sp[34]*sp[43]*pow(sp[27],2)*sp[44]*ang[0] + 2.E+0*
      sp[35]*sp[34]*sp[43]*pow(sp[27],3)*ang[0] - 8.E+0*sp[35]*pow(sp[43],2)*pow(sp[27],2)*
      ang[1] + 2.4E+1*sp[34]*pow(sp[43],3)*pow(sp[27],2)*ang[0] - 2.4E+1*pow(sp[34],2)*
      pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 3.2E+1*pow(sp[34],2)*pow(sp[43],2)*sp[27]*ang[1]
       - 2.4E+1*pow(sp[34],2)*pow(sp[43],2)*pow(sp[27],2)*ang[0] + 1.6E+1*pow(
      sp[34],3)*sp[43]*sp[27]*sp[44]*ang[0] - 1.2E+1*pow(sp[34],3)*sp[43]*sp[27]*ang[1] + 4.E+0*
      pow(sp[34],3)*sp[43]*pow(sp[27],2)*ang[0] - 1.2E+1*pow(sp[34],3)*sp[43]*sp[44]*ang[1]
       + 4.E+0*pow(sp[34],3)*sp[43]*pow(sp[44],2)*ang[0] + 8.E+0*pow(sp[34],3)*sp[43]*
      ang[2];
 
    kern[4][10] = (1.0/ denom)* kern[4][10]; 
    kern[4][11] =   0;
 
    kern[4][11] = (1.0/ denom)* kern[4][11]; 

    denom =  - 3.2E+1*sp[35]*pow(sp[34],3)*sp[27] + 1.6E+1*pow(sp[35],2)*sp[34]*pow(
      sp[27],2) + 1.6E+1*pow(sp[34],5);

    kern[5][0] =   0;
 
    kern[5][0] = (1.0/ denom)* kern[5][0]; 
    kern[5][1] =   0;
 
    kern[5][1] = (1.0/ denom)* kern[5][1]; 
    kern[5][2] =   0;
 
    kern[5][2] = (1.0/ denom)* kern[5][2]; 
    kern[5][3] =   - 1.6E+1*sp[35]*sp[27]*sp[44]*i_*ang[1] + 8.E+0*sp[35]*sp[27]*pow(
      sp[44],2)*i_*ang[0] + 8.E+0*sp[35]*sp[27]*i_*ang[2] - 1.6E+1*sp[35]*pow(sp[27],2)*
      sp[44]*i_*ang[0] - 1.6E+1*sp[35]*pow(sp[27],2)*i_*ang[1] + 8.E+0*sp[35]*pow(
      sp[27],3)*i_*ang[0] - 9.6E+1*sp[34]*sp[43]*sp[27]*sp[44]*i_*ang[0] + 9.6E+1*sp[34]*sp[43]*
      sp[27]*i_*ang[1] - 9.6E+1*sp[34]*sp[43]*pow(sp[27],2)*i_*ang[0] + 6.4E+1*pow(
      sp[34],2)*sp[27]*sp[44]*i_*ang[0] - 3.2E+1*pow(sp[34],2)*sp[27]*i_*ang[1] + 1.6E+1*
      pow(sp[34],2)*pow(sp[27],2)*i_*ang[0] - 3.2E+1*pow(sp[34],2)*sp[44]*i_*ang[1] + 
      1.6E+1*pow(sp[34],2)*pow(sp[44],2)*i_*ang[0] + 1.6E+1*pow(sp[34],2)*i_*ang[2]
       + 9.6E+1*pow(sp[43],2)*pow(sp[27],2)*i_*ang[0];
 
    kern[5][3] = (1.0/ denom)* kern[5][3]; 
    kern[5][4] =   0;
 
    kern[5][4] = (1.0/ denom)* kern[5][4]; 
    kern[5][5] =  1.6E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 3.2E+1*sp[35]*
      sp[34]*pow(sp[43],2)*sp[27]*ang[1] + 1.6E+1*sp[35]*sp[34]*pow(sp[43],2)*pow(sp[27],2)*
      ang[0] + 1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0] + 2.4E+1*sp[35]*pow(
      sp[34],2)*sp[43]*sp[27]*ang[1] - 1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0]
       + 2.4E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[1] - 1.6E+1*sp[35]*pow(sp[34],2)*sp[43]
      *pow(sp[44],2)*ang[0] - 8.E+0*sp[35]*pow(sp[34],2)*sp[43]*ang[2] - 1.6E+1*sp[35]*
      pow(sp[43],3)*pow(sp[27],2)*ang[0] + 4.E+0*pow(sp[35],2)*sp[43]*sp[27]*pow(sp[44],2)*
      ang[0] - 4.E+0*pow(sp[35],2)*sp[43]*sp[27]*ang[2] - 8.E+0*pow(sp[35],2)*sp[43]*pow(
      sp[27],2)*sp[44]*ang[0] + 4.E+0*pow(sp[35],2)*sp[43]*pow(sp[27],3)*ang[0] - 3.2E+1*
      pow(sp[34],2)*pow(sp[43],3)*sp[27]*ang[0] + 3.2E+1*pow(sp[34],3)*pow(sp[43],2)*sp[27]
      *ang[0] + 3.2E+1*pow(sp[34],3)*pow(sp[43],2)*sp[44]*ang[0] - 1.6E+1*pow(sp[34],3)
      *pow(sp[43],2)*ang[1] - 3.2E+1*pow(sp[34],4)*sp[43]*sp[44]*ang[0];
 
    kern[5][5] = (1.0/ denom)* kern[5][5]; 
    kern[5][6] =  8.E+0*sp[35]*sp[34]*sp[27]*sp[44]*ang[0] + 8.E+0*sp[35]*sp[34]*sp[27]*ang[1]
       + 4.E+0*sp[35]*sp[34]*pow(sp[27],2)*ang[0] + 2.4E+1*sp[35]*sp[34]*sp[44]*ang[1] - 
      1.2E+1*sp[35]*sp[34]*pow(sp[44],2)*ang[0] - 1.2E+1*sp[35]*sp[34]*ang[2] + 1.6E+1*sp[35]
      *sp[43]*sp[27]*sp[44]*ang[0] - 1.6E+1*sp[35]*sp[43]*sp[27]*ang[1] - 1.6E+1*sp[35]*sp[43]*pow(
      sp[27],2)*ang[0] - 4.8E+1*sp[34]*pow(sp[43],2)*sp[27]*ang[0] + 6.4E+1*pow(sp[34],2)*
      sp[43]*sp[27]*ang[0] + 3.2E+1*pow(sp[34],2)*sp[43]*sp[44]*ang[0] - 3.2E+1*pow(sp[34],2)
      *sp[43]*ang[1] - 1.6E+1*pow(sp[34],3)*sp[27]*ang[0] - 3.2E+1*pow(sp[34],3)*sp[44]*
      ang[0] + 1.6E+1*pow(sp[34],3)*ang[1];
 
    kern[5][6] = (1.0/ denom)* kern[5][6]; 
    kern[5][7] =   0;
 
    kern[5][7] = (1.0/ denom)* kern[5][7]; 
    kern[5][8] =   0;
 
    kern[5][8] = (1.0/ denom)* kern[5][8]; 
    kern[5][9] =   0;
 
    kern[5][9] = (1.0/ denom)* kern[5][9]; 
    kern[5][10] =   0;
 
    kern[5][10] = (1.0/ denom)* kern[5][10]; 
    kern[5][11] =  8.E+0*sp[35]*sp[34]*sp[27]*sp[44]*ang[1] + 6.E+0*sp[35]*sp[34]*sp[27]*pow(
      sp[44],2)*ang[0] - 6.E+0*sp[35]*sp[34]*sp[27]*ang[2] + 6.E+0*sp[35]*sp[34]*pow(sp[27],2)*
      sp[44]*ang[0] + 1.2E+1*sp[35]*sp[34]*pow(sp[27],2)*ang[1] - 6.E+0*sp[35]*sp[34]*pow(
      sp[27],3)*ang[0] - 6.E+0*sp[35]*sp[34]*sp[44]*ang[2] + 1.2E+1*sp[35]*sp[34]*pow(sp[44],2)*
      ang[1] - 6.E+0*sp[35]*sp[34]*pow(sp[44],3)*ang[0] - 8.E+0*sp[35]*sp[43]*sp[27]*sp[44]*ang[1]
       + 8.E+0*sp[35]*sp[43]*sp[27]*pow(sp[44],2)*ang[0] - 1.6E+1*sp[35]*sp[43]*pow(sp[27],2)*
      sp[44]*ang[0] - 8.E+0*sp[35]*sp[43]*pow(sp[27],2)*ang[1] + 8.E+0*sp[35]*sp[43]*pow(
      sp[27],3)*ang[0] - 2.4E+1*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 2.4E+1*sp[34]*
      pow(sp[43],2)*pow(sp[27],2)*ang[0] + 6.4E+1*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0]
       - 1.6E+1*pow(sp[34],2)*sp[43]*sp[27]*ang[1] + 1.6E+1*pow(sp[34],2)*sp[43]*pow(
      sp[27],2)*ang[0] - 1.6E+1*pow(sp[34],2)*sp[43]*sp[44]*ang[1] + 1.6E+1*pow(sp[34],2)*
      sp[43]*pow(sp[44],2)*ang[0] - 2.4E+1*pow(sp[34],3)*sp[27]*sp[44]*ang[0] + 1.6E+1*
      pow(sp[34],3)*sp[44]*ang[1] - 2.4E+1*pow(sp[34],3)*pow(sp[44],2)*ang[0];
 
    kern[5][11] = (1.0/ denom)* kern[5][11]; 

    denom =  - 1.6E+1*sp[35]*sp[27] + 1.6E+1*pow(sp[34],2);

    kern[6][0] =   0;
 
    kern[6][0] = (1.0/ denom)* kern[6][0]; 
    kern[6][1] =   0;
 
    kern[6][1] = (1.0/ denom)* kern[6][1]; 
    kern[6][2] =   0;
 
    kern[6][2] = (1.0/ denom)* kern[6][2]; 
    kern[6][3] =   - 1.6E+1*sp[34]*sp[27]*i_*ang[1] + 1.6E+1*sp[34]*pow(sp[27],2)*i_*
      ang[0] + 1.6E+1*sp[34]*sp[44]*i_*ang[1] - 1.6E+1*sp[34]*pow(sp[44],2)*i_*ang[0] + 
      3.2E+1*sp[43]*sp[27]*sp[44]*i_*ang[0] - 3.2E+1*sp[43]*pow(sp[27],2)*i_*ang[0];
 
    kern[6][3] = (1.0/ denom)* kern[6][3]; 
    kern[6][4] =   0;
 
    kern[6][4] = (1.0/ denom)* kern[6][4]; 
    kern[6][5] =  8.E+0*sp[35]*sp[34]*sp[43]*sp[27]*ang[1] - 8.E+0*sp[35]*sp[34]*sp[43]*pow(
      sp[27],2)*ang[0] - 8.E+0*sp[35]*sp[34]*sp[43]*sp[44]*ang[1] + 8.E+0*sp[35]*sp[34]*sp[43]*pow(
      sp[44],2)*ang[0] - 1.6E+1*sp[35]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 1.6E+1*sp[35]*
      pow(sp[43],2)*pow(sp[27],2)*ang[0];
 
    kern[6][5] = (1.0/ denom)* kern[6][5]; 
    kern[6][6] =   - 8.E+0*sp[35]*sp[27]*sp[44]*ang[0] + 8.E+0*sp[35]*sp[27]*ang[1] + 
      4.E+0*sp[35]*pow(sp[27],2)*ang[0] - 8.E+0*sp[35]*sp[44]*ang[1] + 4.E+0*sp[35]*pow(
      sp[44],2)*ang[0] + 4.E+0*sp[35]*ang[2] + 3.2E+1*sp[34]*sp[43]*sp[27]*ang[0] - 1.6E+1*
      pow(sp[34],2)*sp[27]*ang[0] - 1.6E+1*pow(sp[43],2)*sp[27]*ang[0];
 
    kern[6][6] = (1.0/ denom)* kern[6][6]; 
    kern[6][7] =   0;
 
    kern[6][7] = (1.0/ denom)* kern[6][7]; 
    kern[6][8] =   0;
 
    kern[6][8] = (1.0/ denom)* kern[6][8]; 
    kern[6][9] =   0;
 
    kern[6][9] = (1.0/ denom)* kern[6][9]; 
    kern[6][10] =   0;
 
    kern[6][10] = (1.0/ denom)* kern[6][10]; 
    kern[6][11] =   - 6.E+0*sp[35]*sp[27]*pow(sp[44],2)*ang[0] - 2.E+0*sp[35]*sp[27]*
      ang[2] + 6.E+0*sp[35]*pow(sp[27],2)*sp[44]*ang[0] + 4.E+0*sp[35]*pow(sp[27],2)*ang[1]
       - 2.E+0*sp[35]*pow(sp[27],3)*ang[0] + 2.E+0*sp[35]*sp[44]*ang[2] - 4.E+0*sp[35]*
      pow(sp[44],2)*ang[1] + 2.E+0*sp[35]*pow(sp[44],3)*ang[0] - 8.E+0*pow(sp[34],2)*
      sp[27]*sp[44]*ang[0] + 8.E+0*pow(sp[34],2)*pow(sp[44],2)*ang[0] - 8.E+0*pow(
      sp[43],2)*sp[27]*sp[44]*ang[0] + 8.E+0*pow(sp[43],2)*pow(sp[27],2)*ang[0];
 
    kern[6][11] = (1.0/ denom)* kern[6][11]; 

    denom =  - 4.8E+1*sp[35]*sp[27] + 4.8E+1*pow(sp[34],2);

    kern[7][0] =   0;
 
    kern[7][0] = (1.0/ denom)* kern[7][0]; 
    kern[7][1] =   0;
 
    kern[7][1] = (1.0/ denom)* kern[7][1]; 
    kern[7][2] =   0;
 
    kern[7][2] = (1.0/ denom)* kern[7][2]; 
    kern[7][3] =   0;
 
    kern[7][3] = (1.0/ denom)* kern[7][3]; 
    kern[7][4] =   0;
 
    kern[7][4] = (1.0/ denom)* kern[7][4]; 
    kern[7][5] =   0;
 
    kern[7][5] = (1.0/ denom)* kern[7][5]; 
    kern[7][6] =   0;
 
    kern[7][6] = (1.0/ denom)* kern[7][6]; 
    kern[7][7] =   - 2.4E+1*sp[35]*sp[27]*sp[44]*ang[0] - 4.8E+1*sp[35]*sp[27]*ang[1] + 
      1.2E+1*sp[35]*pow(sp[27],2)*ang[0] - 4.8E+1*sp[35]*sp[44]*ang[1] + 1.2E+1*sp[35]*
      pow(sp[44],2)*ang[0] + 3.6E+1*sp[35]*ang[2] - 4.8E+1*sp[34]*sp[43]*sp[27]*ang[0] - 
      4.8E+1*sp[34]*sp[43]*sp[44]*ang[0] + 9.6E+1*sp[34]*sp[43]*ang[1] + 4.8E+1*pow(sp[34],2)
      *sp[44]*ang[0] + 4.8E+1*pow(sp[43],2)*sp[27]*ang[0];
 
    kern[7][7] = (1.0/ denom)* kern[7][7]; 
    kern[7][8] =   0;
 
    kern[7][8] = (1.0/ denom)* kern[7][8]; 
    kern[7][9] =   0;
 
    kern[7][9] = (1.0/ denom)* kern[7][9]; 
    kern[7][10] =   0;
 
    kern[7][10] = (1.0/ denom)* kern[7][10]; 
    kern[7][11] =   0;
 
    kern[7][11] = (1.0/ denom)* kern[7][11]; 

    denom =  - 4.E+0*sp[35]*sp[34]*sp[27] + 4.E+0*pow(sp[34],3);

    kern[8][0] =   0;
 
    kern[8][0] = (1.0/ denom)* kern[8][0]; 
    kern[8][1] =   0;
 
    kern[8][1] = (1.0/ denom)* kern[8][1]; 
    kern[8][2] =  6.E+0*sp[34]*sp[27]*ang[1] + 6.E+0*sp[34]*sp[44]*ang[1] - 6.E+0*sp[34]*
      ang[2] - 1.2E+1*sp[43]*sp[27]*ang[1];
 
    kern[8][2] = (1.0/ denom)* kern[8][2]; 
    kern[8][3] =   0;
 
    kern[8][3] = (1.0/ denom)* kern[8][3]; 
    kern[8][4] =   0;
 
    kern[8][4] = (1.0/ denom)* kern[8][4]; 
    kern[8][5] =   0;
 
    kern[8][5] = (1.0/ denom)* kern[8][5]; 
    kern[8][6] =   0;
 
    kern[8][6] = (1.0/ denom)* kern[8][6]; 
    kern[8][7] =   0;
 
    kern[8][7] = (1.0/ denom)* kern[8][7]; 
    kern[8][8] =   - 6.E+0*sp[35]*sp[34]*sp[27]*ang[1] - 6.E+0*sp[35]*sp[34]*sp[44]*ang[1] + 
      6.E+0*sp[35]*sp[34]*ang[2] + 1.2E+1*pow(sp[34],2)*sp[43]*ang[1];
 
    kern[8][8] = (1.0/ denom)* kern[8][8]; 
    kern[8][9] =   0;
 
    kern[8][9] = (1.0/ denom)* kern[8][9]; 
    kern[8][10] =   0;
 
    kern[8][10] = (1.0/ denom)* kern[8][10]; 
    kern[8][11] =   0;
 
    kern[8][11] = (1.0/ denom)* kern[8][11]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[34],3)*sp[27] + 8.E+0*pow(sp[35],2)*sp[34]*pow(
      sp[27],2) + 8.E+0*pow(sp[34],5);

    kern[9][0] =  4.E+0*sp[35]*sp[34]*sp[27]*sp[44]*ang[0] + 4.E+0*sp[35]*sp[34]*sp[27]*ang[1]
       + 2.E+0*sp[35]*sp[34]*pow(sp[27],2)*ang[0] + 1.2E+1*sp[35]*sp[34]*sp[44]*ang[1] - 
      6.E+0*sp[35]*sp[34]*pow(sp[44],2)*ang[0] - 6.E+0*sp[35]*sp[34]*ang[2] + 8.E+0*sp[35]*
      sp[43]*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*sp[43]*sp[27]*ang[1] - 8.E+0*sp[35]*sp[43]*pow(
      sp[27],2)*ang[0] - 2.4E+1*sp[34]*pow(sp[43],2)*sp[27]*ang[0] + 3.2E+1*pow(sp[34],2)*
      sp[43]*sp[27]*ang[0] + 1.6E+1*pow(sp[34],2)*sp[43]*sp[44]*ang[0] - 1.6E+1*pow(sp[34],2)
      *sp[43]*ang[1] - 8.E+0*pow(sp[34],3)*sp[27]*ang[0] - 1.6E+1*pow(sp[34],3)*sp[44]*
      ang[0] + 8.E+0*pow(sp[34],3)*ang[1];
 
    kern[9][0] = (1.0/ denom)* kern[9][0]; 
    kern[9][1] =   - 4.E+0*sp[35]*sp[34]*sp[27]*sp[44]*ang[1] + 5.E+0*sp[35]*sp[34]*sp[27]*
      pow(sp[44],2)*ang[0] + 1.1E+1*sp[35]*sp[34]*sp[27]*ang[2] - sp[35]*sp[34]*pow(sp[27],2)*
      sp[44]*ang[0] - 4.E+0*sp[35]*sp[34]*pow(sp[27],2)*ang[1] - sp[35]*sp[34]*pow(sp[27],3)*
      ang[0] + 9.E+0*sp[35]*sp[34]*sp[44]*ang[2] - 3.E+0*sp[35]*sp[34]*pow(sp[44],3)*ang[0] - 
      6.E+0*sp[35]*sp[34]*ang[3] + 4.E+0*sp[35]*sp[43]*sp[27]*sp[44]*ang[1] + 4.E+0*sp[35]*sp[43]*
      sp[27]*pow(sp[44],2)*ang[0] - 8.E+0*sp[35]*sp[43]*sp[27]*ang[2] - 8.E+0*sp[35]*sp[43]*pow(
      sp[27],2)*sp[44]*ang[0] + 4.E+0*sp[35]*sp[43]*pow(sp[27],2)*ang[1] + 4.E+0*sp[35]*sp[43]*
      pow(sp[27],3)*ang[0] - 1.2E+1*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 2.4E+1*sp[34]
      *pow(sp[43],2)*sp[27]*ang[1] + 1.2E+1*sp[34]*pow(sp[43],2)*pow(sp[27],2)*ang[0] + 
      8.E+0*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0] + 3.2E+1*pow(sp[34],2)*sp[43]*sp[27]*
      ang[1] - 1.6E+1*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 8.E+0*pow(sp[34],2)*
      sp[43]*sp[44]*ang[1] + 8.E+0*pow(sp[34],2)*sp[43]*pow(sp[44],2)*ang[0] - 1.6E+1*pow(
      sp[34],2)*sp[43]*ang[2] + 4.E+0*pow(sp[34],3)*sp[27]*sp[44]*ang[0] - 8.E+0*pow(
      sp[34],3)*sp[27]*ang[1] + 4.E+0*pow(sp[34],3)*pow(sp[27],2)*ang[0] - 8.E+0*pow(
      sp[34],3)*sp[44]*ang[1] - 8.E+0*pow(sp[34],3)*pow(sp[44],2)*ang[0] + 4.E+0*pow(
      sp[34],3)*ang[2];
 
    kern[9][1] = (1.0/ denom)* kern[9][1]; 
    kern[9][2] =   0;
 
    kern[9][2] = (1.0/ denom)* kern[9][2]; 
    kern[9][3] =   0;
 
    kern[9][3] = (1.0/ denom)* kern[9][3]; 
    kern[9][4] =   - 8.E+0*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*ang[0] - 8.E+0*sp[35]*pow(
      sp[34],2)*sp[43]*sp[27]*ang[0] + 2.4E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[0] - 2.4E+1
      *sp[35]*pow(sp[34],2)*sp[43]*ang[1] + 1.6E+1*sp[35]*pow(sp[34],3)*sp[27]*ang[0] - 
      2.4E+1*sp[35]*pow(sp[34],3)*sp[44]*ang[0] + 1.6E+1*sp[35]*pow(sp[34],3)*ang[1] + 
      1.2E+1*pow(sp[35],2)*sp[34]*sp[27]*sp[44]*ang[0] - 4.E+0*pow(sp[35],2)*sp[34]*sp[27]*
      ang[1] - 6.E+0*pow(sp[35],2)*sp[34]*pow(sp[27],2)*ang[0] + 1.2E+1*pow(sp[35],2)*
      sp[34]*sp[44]*ang[1] - 6.E+0*pow(sp[35],2)*sp[34]*pow(sp[44],2)*ang[0] - 6.E+0*pow(
      sp[35],2)*sp[34]*ang[2] - 1.6E+1*pow(sp[34],3)*pow(sp[43],2)*ang[0] + 3.2E+1*pow(
      sp[34],4)*sp[43]*ang[0] - 1.6E+1*pow(sp[34],5)*ang[0];
 
    kern[9][4] = (1.0/ denom)* kern[9][4]; 
    kern[9][5] =   0;
 
    kern[9][5] = (1.0/ denom)* kern[9][5]; 
    kern[9][6] =   0;
 
    kern[9][6] = (1.0/ denom)* kern[9][6]; 
    kern[9][7] =   0;
 
    kern[9][7] = (1.0/ denom)* kern[9][7]; 
    kern[9][8] =   0;
 
    kern[9][8] = (1.0/ denom)* kern[9][8]; 
    kern[9][9] =   - 4.E+0*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*
      sp[34]*pow(sp[43],2)*sp[27]*ang[1] + 4.E+0*sp[35]*sp[34]*pow(sp[43],2)*pow(sp[27],2)*
      ang[0] - 8.E+0*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0] + 2.8E+1*sp[35]*pow(
      sp[34],2)*sp[43]*sp[27]*ang[1] - 4.E+0*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 
      1.2E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[1] + 1.2E+1*sp[35]*pow(sp[34],2)*sp[43]*
      pow(sp[44],2)*ang[0] - 2.4E+1*sp[35]*pow(sp[34],2)*sp[43]*ang[2] + 4.E+0*sp[35]*pow(
      sp[34],3)*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*pow(sp[34],3)*sp[27]*ang[1] + 4.E+0*sp[35]*
      pow(sp[34],3)*pow(sp[27],2)*ang[0] - 8.E+0*sp[35]*pow(sp[34],3)*sp[44]*ang[1] - 
      8.E+0*sp[35]*pow(sp[34],3)*pow(sp[44],2)*ang[0] + 4.E+0*sp[35]*pow(sp[34],3)*ang[2]
       - 4.E+0*pow(sp[35],2)*sp[34]*sp[27]*sp[44]*ang[1] + 5.E+0*pow(sp[35],2)*sp[34]*sp[27]*
      pow(sp[44],2)*ang[0] + 1.1E+1*pow(sp[35],2)*sp[34]*sp[27]*ang[2] - pow(sp[35],2)*sp[34]
      *pow(sp[27],2)*sp[44]*ang[0] - 4.E+0*pow(sp[35],2)*sp[34]*pow(sp[27],2)*ang[1] - 
      pow(sp[35],2)*sp[34]*pow(sp[27],3)*ang[0] + 9.E+0*pow(sp[35],2)*sp[34]*sp[44]*ang[2] - 
      3.E+0*pow(sp[35],2)*sp[34]*pow(sp[44],3)*ang[0] - 6.E+0*pow(sp[35],2)*sp[34]*ang[3]
       + 8.E+0*pow(sp[34],3)*pow(sp[43],2)*sp[27]*ang[0] - 8.E+0*pow(sp[34],3)*pow(
      sp[43],2)*sp[44]*ang[0] - 1.6E+1*pow(sp[34],3)*pow(sp[43],2)*ang[1] - 8.E+0*pow(
      sp[34],4)*sp[43]*sp[27]*ang[0] + 8.E+0*pow(sp[34],4)*sp[43]*sp[44]*ang[0] + 8.E+0*pow(
      sp[34],4)*sp[43]*ang[1];
 
    kern[9][9] = (1.0/ denom)* kern[9][9]; 
    kern[9][10] =  8.E+0*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*
      sp[34]*pow(sp[43],2)*sp[27]*ang[1] - 8.E+0*sp[35]*sp[34]*pow(sp[43],2)*pow(sp[27],2)*
      ang[0] + 4.E+0*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0] + 4.E+0*sp[35]*pow(
      sp[34],2)*sp[43]*sp[27]*ang[1] + 2.E+0*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 
      1.2E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[1] - 6.E+0*sp[35]*pow(sp[34],2)*sp[43]*
      pow(sp[44],2)*ang[0] - 6.E+0*sp[35]*pow(sp[34],2)*sp[43]*ang[2] - 2.4E+1*pow(
      sp[34],2)*pow(sp[43],3)*sp[27]*ang[0] + 3.2E+1*pow(sp[34],3)*pow(sp[43],2)*sp[27]*
      ang[0] + 1.6E+1*pow(sp[34],3)*pow(sp[43],2)*sp[44]*ang[0] - 1.6E+1*pow(sp[34],3)*
      pow(sp[43],2)*ang[1] - 8.E+0*pow(sp[34],4)*sp[43]*sp[27]*ang[0] - 1.6E+1*pow(
      sp[34],4)*sp[43]*sp[44]*ang[0] + 8.E+0*pow(sp[34],4)*sp[43]*ang[1];
 
    kern[9][10] = (1.0/ denom)* kern[9][10]; 
    kern[9][11] =   0;
 
    kern[9][11] = (1.0/ denom)* kern[9][11]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[34],3)*sp[27] + 8.E+0*pow(sp[35],2)*sp[34]*pow(
      sp[27],2) + 8.E+0*pow(sp[34],5);

    kern[10][0] =  4.E+0*sp[35]*sp[34]*sp[27]*sp[44]*ang[0] + 4.E+0*sp[35]*sp[34]*sp[27]*ang[1]
       + 2.E+0*sp[35]*sp[34]*pow(sp[27],2)*ang[0] + 1.2E+1*sp[35]*sp[34]*sp[44]*ang[1] - 
      6.E+0*sp[35]*sp[34]*pow(sp[44],2)*ang[0] - 6.E+0*sp[35]*sp[34]*ang[2] + 8.E+0*sp[35]*
      sp[43]*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*sp[43]*sp[27]*ang[1] - 8.E+0*sp[35]*sp[43]*pow(
      sp[27],2)*ang[0] - 2.4E+1*sp[34]*pow(sp[43],2)*sp[27]*ang[0] + 3.2E+1*pow(sp[34],2)*
      sp[43]*sp[27]*ang[0] + 1.6E+1*pow(sp[34],2)*sp[43]*sp[44]*ang[0] - 1.6E+1*pow(sp[34],2)
      *sp[43]*ang[1] - 8.E+0*pow(sp[34],3)*sp[27]*ang[0] - 1.6E+1*pow(sp[34],3)*sp[44]*
      ang[0] + 8.E+0*pow(sp[34],3)*ang[1];
 
    kern[10][0] = (1.0/ denom)* kern[10][0]; 
    kern[10][1] =   - 4.E+0*sp[35]*sp[34]*sp[27]*sp[44]*ang[1] + sp[35]*sp[34]*sp[27]*pow(
      sp[44],2)*ang[0] + 1.5E+1*sp[35]*sp[34]*sp[27]*ang[2] - sp[35]*sp[34]*pow(sp[27],2)*sp[44]*
      ang[0] - 1.2E+1*sp[35]*sp[34]*pow(sp[27],2)*ang[1] + 3.E+0*sp[35]*sp[34]*pow(sp[27],3)*
      ang[0] + 9.E+0*sp[35]*sp[34]*sp[44]*ang[2] - 3.E+0*sp[35]*sp[34]*pow(sp[44],3)*ang[0] - 
      6.E+0*sp[35]*sp[34]*ang[3] + 4.E+0*sp[35]*sp[43]*sp[27]*sp[44]*ang[1] + 4.E+0*sp[35]*sp[43]*
      sp[27]*pow(sp[44],2)*ang[0] - 8.E+0*sp[35]*sp[43]*sp[27]*ang[2] + 1.2E+1*sp[35]*sp[43]*
      pow(sp[27],2)*ang[1] - 4.E+0*sp[35]*sp[43]*pow(sp[27],3)*ang[0] - 1.2E+1*sp[34]*pow(
      sp[43],2)*sp[27]*sp[44]*ang[0] - 2.4E+1*sp[34]*pow(sp[43],2)*sp[27]*ang[1] + 1.2E+1*sp[34]
      *pow(sp[43],2)*pow(sp[27],2)*ang[0] + 2.4E+1*pow(sp[34],2)*sp[43]*sp[27]*ang[1] - 
      8.E+0*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 8.E+0*pow(sp[34],2)*sp[43]*sp[44]*
      ang[1] + 8.E+0*pow(sp[34],2)*sp[43]*pow(sp[44],2)*ang[0] - 1.6E+1*pow(sp[34],2)*
      sp[43]*ang[2] + 4.E+0*pow(sp[34],3)*sp[27]*sp[44]*ang[0] - 8.E+0*pow(sp[34],3)*sp[44]*
      ang[1] - 4.E+0*pow(sp[34],3)*pow(sp[44],2)*ang[0];
 
    kern[10][1] = (1.0/ denom)* kern[10][1]; 
    kern[10][2] =   0;
 
    kern[10][2] = (1.0/ denom)* kern[10][2]; 
    kern[10][3] =   0;
 
    kern[10][3] = (1.0/ denom)* kern[10][3]; 
    kern[10][4] =   - 2.4E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*ang[0] + 3.2E+1*sp[35]*
      pow(sp[34],2)*sp[43]*sp[27]*ang[0] + 1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[0] - 
      1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*ang[1] - 8.E+0*sp[35]*pow(sp[34],3)*sp[27]*ang[0] - 
      1.6E+1*sp[35]*pow(sp[34],3)*sp[44]*ang[0] + 8.E+0*sp[35]*pow(sp[34],3)*ang[1] + 
      4.E+0*pow(sp[35],2)*sp[34]*sp[27]*sp[44]*ang[0] + 4.E+0*pow(sp[35],2)*sp[34]*sp[27]*ang[1]
       + 2.E+0*pow(sp[35],2)*sp[34]*pow(sp[27],2)*ang[0] + 1.2E+1*pow(sp[35],2)*sp[34]*
      sp[44]*ang[1] - 6.E+0*pow(sp[35],2)*sp[34]*pow(sp[44],2)*ang[0] - 6.E+0*pow(
      sp[35],2)*sp[34]*ang[2] + 8.E+0*pow(sp[35],2)*sp[43]*sp[27]*sp[44]*ang[0] - 8.E+0*pow(
      sp[35],2)*sp[43]*sp[27]*ang[1] - 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[27],2)*ang[0];
 
    kern[10][4] = (1.0/ denom)* kern[10][4]; 
    kern[10][5] =   0;
 
    kern[10][5] = (1.0/ denom)* kern[10][5]; 
    kern[10][6] =   0;
 
    kern[10][6] = (1.0/ denom)* kern[10][6]; 
    kern[10][7] =   0;
 
    kern[10][7] = (1.0/ denom)* kern[10][7]; 
    kern[10][8] =   0;
 
    kern[10][8] = (1.0/ denom)* kern[10][8]; 
    kern[10][9] =   - 1.2E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 2.4E+1*
      sp[35]*sp[34]*pow(sp[43],2)*sp[27]*ang[1] + 1.2E+1*sp[35]*sp[34]*pow(sp[43],2)*pow(
      sp[27],2)*ang[0] + 2.4E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*ang[1] - 8.E+0*sp[35]*pow(
      sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 8.E+0*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[1] + 
      8.E+0*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[44],2)*ang[0] - 1.6E+1*sp[35]*pow(sp[34],2)*
      sp[43]*ang[2] + 4.E+0*sp[35]*pow(sp[34],3)*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*pow(
      sp[34],3)*sp[44]*ang[1] - 4.E+0*sp[35]*pow(sp[34],3)*pow(sp[44],2)*ang[0] - 4.E+0*
      pow(sp[35],2)*sp[34]*sp[27]*sp[44]*ang[1] + pow(sp[35],2)*sp[34]*sp[27]*pow(sp[44],2)*ang[0]
       + 1.5E+1*pow(sp[35],2)*sp[34]*sp[27]*ang[2] - pow(sp[35],2)*sp[34]*pow(sp[27],2)*sp[44]
      *ang[0] - 1.2E+1*pow(sp[35],2)*sp[34]*pow(sp[27],2)*ang[1] + 3.E+0*pow(sp[35],2)*
      sp[34]*pow(sp[27],3)*ang[0] + 9.E+0*pow(sp[35],2)*sp[34]*sp[44]*ang[2] - 3.E+0*pow(
      sp[35],2)*sp[34]*pow(sp[44],3)*ang[0] - 6.E+0*pow(sp[35],2)*sp[34]*ang[3] + 4.E+0*
      pow(sp[35],2)*sp[43]*sp[27]*sp[44]*ang[1] + 4.E+0*pow(sp[35],2)*sp[43]*sp[27]*pow(sp[44],2)
      *ang[0] - 8.E+0*pow(sp[35],2)*sp[43]*sp[27]*ang[2] + 1.2E+1*pow(sp[35],2)*sp[43]*
      pow(sp[27],2)*ang[1] - 4.E+0*pow(sp[35],2)*sp[43]*pow(sp[27],3)*ang[0];
 
    kern[10][9] = (1.0/ denom)* kern[10][9]; 
    kern[10][10] =  1.6E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*
      sp[34]*pow(sp[43],2)*sp[27]*ang[1] + 1.6E+1*sp[35]*sp[34]*pow(sp[43],2)*pow(sp[27],2)*
      ang[0] - 4.E+0*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0] + 1.6E+1*sp[35]*pow(
      sp[34],2)*sp[43]*sp[27]*ang[1] - 6.E+0*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 
      1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[1] - 6.E+0*sp[35]*pow(sp[34],2)*sp[43]*
      pow(sp[44],2)*ang[0] - 1.E+1*sp[35]*pow(sp[34],2)*sp[43]*ang[2] - 1.6E+1*sp[35]*pow(
      sp[43],3)*pow(sp[27],2)*ang[0] - 4.E+0*pow(sp[35],2)*sp[43]*sp[27]*sp[44]*ang[1] + 
      4.E+0*pow(sp[35],2)*sp[43]*sp[27]*ang[2] - 4.E+0*pow(sp[35],2)*sp[43]*pow(sp[27],2)*
      ang[1] - 8.E+0*pow(sp[34],2)*pow(sp[43],3)*sp[27]*ang[0] + 8.E+0*pow(sp[34],3)*
      pow(sp[43],2)*sp[27]*ang[0] + 8.E+0*pow(sp[34],3)*pow(sp[43],2)*sp[44]*ang[0] - 
      1.6E+1*pow(sp[34],3)*pow(sp[43],2)*ang[1] - 8.E+0*pow(sp[34],4)*sp[43]*sp[44]*ang[0]
      ;
 
    kern[10][10] = (1.0/ denom)* kern[10][10]; 
    kern[10][11] =   0;
 
    kern[10][11] = (1.0/ denom)* kern[10][11]; 

    denom =  - 3.2E+1*sp[35]*pow(sp[34],2)*sp[27] + 1.6E+1*pow(sp[35],2)*pow(sp[27],2)
       + 1.6E+1*pow(sp[34],4);

    kern[11][0] =   0;
 
    kern[11][0] = (1.0/ denom)* kern[11][0]; 
    kern[11][1] =   0;
 
    kern[11][1] = (1.0/ denom)* kern[11][1]; 
    kern[11][2] =   0;
 
    kern[11][2] = (1.0/ denom)* kern[11][2]; 
    kern[11][3] =  1.6E+1*sp[35]*sp[34]*sp[27]*sp[44]*i_*ang[0] + 1.6E+1*sp[35]*sp[34]*sp[27]*
      i_*ang[1] + 8.E+0*sp[35]*sp[34]*pow(sp[27],2)*i_*ang[0] + 4.8E+1*sp[35]*sp[34]*sp[44]*
      i_*ang[1] - 2.4E+1*sp[35]*sp[34]*pow(sp[44],2)*i_*ang[0] - 2.4E+1*sp[35]*sp[34]*i_*
      ang[2] + 3.2E+1*sp[35]*sp[43]*sp[27]*sp[44]*i_*ang[0] - 3.2E+1*sp[35]*sp[43]*sp[27]*i_*
      ang[1] - 3.2E+1*sp[35]*sp[43]*pow(sp[27],2)*i_*ang[0] - 9.6E+1*sp[34]*pow(sp[43],2)*
      sp[27]*i_*ang[0] + 1.28E+2*pow(sp[34],2)*sp[43]*sp[27]*i_*ang[0] + 6.4E+1*pow(
      sp[34],2)*sp[43]*sp[44]*i_*ang[0] - 6.4E+1*pow(sp[34],2)*sp[43]*i_*ang[1] - 3.2E+1*
      pow(sp[34],3)*sp[27]*i_*ang[0] - 6.4E+1*pow(sp[34],3)*sp[44]*i_*ang[0] + 3.2E+1*
      pow(sp[34],3)*i_*ang[1];
 
    kern[11][3] = (1.0/ denom)* kern[11][3]; 
    kern[11][4] =   0;
 
    kern[11][4] = (1.0/ denom)* kern[11][4]; 
    kern[11][5] =  4.8E+1*sp[35]*sp[34]*pow(sp[43],3)*sp[27]*ang[0] - 6.4E+1*sp[35]*pow(
      sp[34],2)*pow(sp[43],2)*sp[27]*ang[0] - 3.2E+1*sp[35]*pow(sp[34],2)*pow(sp[43],2)*sp[44]
      *ang[0] + 3.2E+1*sp[35]*pow(sp[34],2)*pow(sp[43],2)*ang[1] + 1.6E+1*sp[35]*pow(
      sp[34],3)*sp[43]*sp[27]*ang[0] + 3.2E+1*sp[35]*pow(sp[34],3)*sp[43]*sp[44]*ang[0] - 1.6E+1
      *sp[35]*pow(sp[34],3)*sp[43]*ang[1] - 8.E+0*pow(sp[35],2)*sp[34]*sp[43]*sp[27]*sp[44]*ang[0]
       - 8.E+0*pow(sp[35],2)*sp[34]*sp[43]*sp[27]*ang[1] - 4.E+0*pow(sp[35],2)*sp[34]*sp[43]*
      pow(sp[27],2)*ang[0] - 2.4E+1*pow(sp[35],2)*sp[34]*sp[43]*sp[44]*ang[1] + 1.2E+1*
      pow(sp[35],2)*sp[34]*sp[43]*pow(sp[44],2)*ang[0] + 1.2E+1*pow(sp[35],2)*sp[34]*sp[43]*
      ang[2] - 1.6E+1*pow(sp[35],2)*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 1.6E+1*pow(
      sp[35],2)*pow(sp[43],2)*sp[27]*ang[1] + 1.6E+1*pow(sp[35],2)*pow(sp[43],2)*pow(
      sp[27],2)*ang[0];
 
    kern[11][5] = (1.0/ denom)* kern[11][5]; 
    kern[11][6] =  1.6E+1*sp[35]*sp[34]*sp[43]*sp[27]*ang[0] - 4.8E+1*sp[35]*sp[34]*sp[43]*sp[44]
      *ang[0] + 4.8E+1*sp[35]*sp[34]*sp[43]*ang[1] - 3.2E+1*sp[35]*pow(sp[34],2)*sp[27]*ang[0]
       + 4.8E+1*sp[35]*pow(sp[34],2)*sp[44]*ang[0] - 3.2E+1*sp[35]*pow(sp[34],2)*ang[1] + 
      1.6E+1*sp[35]*pow(sp[43],2)*sp[27]*ang[0] - 2.4E+1*pow(sp[35],2)*sp[27]*sp[44]*ang[0]
       + 8.E+0*pow(sp[35],2)*sp[27]*ang[1] + 1.2E+1*pow(sp[35],2)*pow(sp[27],2)*ang[0]
       - 2.4E+1*pow(sp[35],2)*sp[44]*ang[1] + 1.2E+1*pow(sp[35],2)*pow(sp[44],2)*ang[0]
       + 1.2E+1*pow(sp[35],2)*ang[2] + 3.2E+1*pow(sp[34],2)*pow(sp[43],2)*ang[0] - 
      6.4E+1*pow(sp[34],3)*sp[43]*ang[0] + 3.2E+1*pow(sp[34],4)*ang[0];
 
    kern[11][6] = (1.0/ denom)* kern[11][6]; 
    kern[11][7] =   0;
 
    kern[11][7] = (1.0/ denom)* kern[11][7]; 
    kern[11][8] =   0;
 
    kern[11][8] = (1.0/ denom)* kern[11][8]; 
    kern[11][9] =   0;
 
    kern[11][9] = (1.0/ denom)* kern[11][9]; 
    kern[11][10] =   0;
 
    kern[11][10] = (1.0/ denom)* kern[11][10]; 
    kern[11][11] =   - 1.6E+1*sp[35]*sp[34]*sp[43]*sp[27]*sp[44]*ang[0] + 2.4E+1*sp[35]*sp[34]
      *sp[43]*sp[27]*ang[1] - 2.4E+1*sp[35]*sp[34]*sp[43]*pow(sp[27],2)*ang[0] + 2.4E+1*sp[35]*
      sp[34]*sp[43]*sp[44]*ang[1] - 2.4E+1*sp[35]*sp[34]*sp[43]*pow(sp[44],2)*ang[0] - 8.E+0*sp[35]
      *pow(sp[34],2)*sp[27]*ang[1] + 4.E+0*sp[35]*pow(sp[34],2)*pow(sp[27],2)*ang[0] - 
      2.4E+1*sp[35]*pow(sp[34],2)*sp[44]*ang[1] + 2.8E+1*sp[35]*pow(sp[34],2)*pow(sp[44],2)
      *ang[0] + 4.E+0*sp[35]*pow(sp[34],2)*ang[2] + 8.E+0*sp[35]*pow(sp[43],2)*sp[27]*sp[44]*
      ang[0] + 2.4E+1*sp[35]*pow(sp[43],2)*pow(sp[27],2)*ang[0] - 1.E+1*pow(sp[35],2)*
      sp[27]*pow(sp[44],2)*ang[0] + 2.E+0*pow(sp[35],2)*sp[27]*ang[2] + 2.E+0*pow(
      sp[35],2)*pow(sp[27],2)*sp[44]*ang[0] - 4.E+0*pow(sp[35],2)*pow(sp[27],2)*ang[1] + 
      2.E+0*pow(sp[35],2)*pow(sp[27],3)*ang[0] + 6.E+0*pow(sp[35],2)*sp[44]*ang[2] - 
      1.2E+1*pow(sp[35],2)*pow(sp[44],2)*ang[1] + 6.E+0*pow(sp[35],2)*pow(sp[44],3)*
      ang[0] + 1.6E+1*pow(sp[34],2)*pow(sp[43],2)*sp[44]*ang[0] - 3.2E+1*pow(sp[34],3)*
      sp[43]*sp[44]*ang[0] + 1.6E+1*pow(sp[34],4)*sp[44]*ang[0];
 
    kern[11][11] = (1.0/ denom)* kern[11][11]; 

}


void kernelY_noopti_qphv(matCdoub& kern,
  const ArrayScalarProducts& sp,  const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


    // local variables
    Cdoub denom;


    denom =  - 4.E+0*sp[35]*sp[44] + 4.E+0*pow(sp[43],2);

    kern[0][0] =   - 4.E+0*sp[35]*sp[44]*sp[7]*svp*svm - 4.E+0*sp[35]*sp[44]*ssp*
      ssm - 4.E+0*sp[43]*sp[31]*sp[37]*svp*svm - 4.E+0*sp[43]*sp[29]*sp[39]*svp*svm
       + 4.E+0*pow(sp[43],2)*sp[7]*svp*svm + 4.E+0*pow(sp[43],2)*ssp*ssm + 
      8.E+0*sp[31]*sp[29]*sp[44]*svp*svm;
 
    kern[0][0] = (1.0/ denom)* kern[0][0]; 
    kern[0][1] =  4.E+0*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm + 4.E+0*sp[43]*sp[29]*sp[44]*
      sp[39]*svp*svm - 8.E+0*pow(sp[43],2)*sp[39]*sp[37]*svp*svm;
 
    kern[0][1] = (1.0/ denom)* kern[0][1]; 
    kern[0][2] =  4.E+0*sp[43]*sp[31]*sp[44]*ssp*svm + 4.E+0*sp[43]*sp[29]*sp[44]*svp*
      ssm - 4.E+0*pow(sp[43],2)*sp[39]*ssp*svm - 4.E+0*pow(sp[43],2)*sp[37]*svp*
      ssm;
 
    kern[0][2] = (1.0/ denom)* kern[0][2]; 
    kern[0][3] =  8.E+0*sp[35]*sp[44]*sp[39]*i_*ssp*svm - 8.E+0*sp[35]*sp[44]*sp[37]*i_
      *svp*ssm - 8.E+0*pow(sp[43],2)*sp[39]*i_*ssp*svm + 8.E+0*pow(sp[43],2)*
      sp[37]*i_*svp*ssm;
 
    kern[0][3] = (1.0/ denom)* kern[0][3]; 
    kern[0][4] =   0;
 
    kern[0][4] = (1.0/ denom)* kern[0][4]; 
    kern[0][5] =   0;
 
    kern[0][5] = (1.0/ denom)* kern[0][5]; 
    kern[0][6] =   0;
 
    kern[0][6] = (1.0/ denom)* kern[0][6]; 
    kern[0][7] =   0;
 
    kern[0][7] = (1.0/ denom)* kern[0][7]; 
    kern[0][8] =   0;
 
    kern[0][8] = (1.0/ denom)* kern[0][8]; 
    kern[0][9] =   0;
 
    kern[0][9] = (1.0/ denom)* kern[0][9]; 
    kern[0][10] =   0;
 
    kern[0][10] = (1.0/ denom)* kern[0][10]; 
    kern[0][11] =   0;
 
    kern[0][11] = (1.0/ denom)* kern[0][11]; 

    denom =  - 4.E+0*sp[35]*sp[43]*sp[44] + 4.E+0*pow(sp[43],3);

    kern[1][0] =  4.E+0*sp[35]*sp[31]*sp[37]*svp*svm + 4.E+0*sp[35]*sp[29]*sp[39]*svp
      *svm - 8.E+0*sp[43]*sp[31]*sp[29]*svp*svm;
 
    kern[1][0] = (1.0/ denom)* kern[1][0]; 
    kern[1][1] =   - 4.E+0*sp[35]*sp[43]*sp[44]*sp[7]*svp*svm - 4.E+0*sp[35]*sp[43]*
      sp[44]*ssp*ssm + 8.E+0*sp[35]*sp[43]*sp[39]*sp[37]*svp*svm - 4.E+0*pow(sp[43],2)*
      sp[31]*sp[37]*svp*svm - 4.E+0*pow(sp[43],2)*sp[29]*sp[39]*svp*svm + 4.E+0*
      pow(sp[43],3)*sp[7]*svp*svm + 4.E+0*pow(sp[43],3)*ssp*ssm;
 
    kern[1][1] = (1.0/ denom)* kern[1][1]; 
    kern[1][2] =  4.E+0*sp[35]*sp[43]*sp[39]*ssp*svm + 4.E+0*sp[35]*sp[43]*sp[37]*svp*
      ssm - 4.E+0*pow(sp[43],2)*sp[31]*ssp*svm - 4.E+0*pow(sp[43],2)*sp[29]*svp*
      ssm;
 
    kern[1][2] = (1.0/ denom)* kern[1][2]; 
    kern[1][3] =   - 8.E+0*sp[35]*sp[31]*sp[44]*i_*ssp*svm + 8.E+0*sp[35]*sp[29]*sp[44]
      *i_*svp*ssm + 8.E+0*pow(sp[43],2)*sp[31]*i_*ssp*svm - 8.E+0*pow(sp[43],2)
      *sp[29]*i_*svp*ssm;
 
    kern[1][3] = (1.0/ denom)* kern[1][3]; 
    kern[1][4] =   0;
 
    kern[1][4] = (1.0/ denom)* kern[1][4]; 
    kern[1][5] =   0;
 
    kern[1][5] = (1.0/ denom)* kern[1][5]; 
    kern[1][6] =   0;
 
    kern[1][6] = (1.0/ denom)* kern[1][6]; 
    kern[1][7] =   0;
 
    kern[1][7] = (1.0/ denom)* kern[1][7]; 
    kern[1][8] =   0;
 
    kern[1][8] = (1.0/ denom)* kern[1][8]; 
    kern[1][9] =   0;
 
    kern[1][9] = (1.0/ denom)* kern[1][9]; 
    kern[1][10] =   0;
 
    kern[1][10] = (1.0/ denom)* kern[1][10]; 
    kern[1][11] =   0;
 
    kern[1][11] = (1.0/ denom)* kern[1][11]; 

    denom = 4.E+0*sp[43];

    kern[2][0] =  4.E+0*sp[31]*ssp*svm + 4.E+0*sp[29]*svp*ssm;
 
    kern[2][0] = (1.0/ denom)* kern[2][0]; 
    kern[2][1] =  4.E+0*sp[43]*sp[39]*ssp*svm + 4.E+0*sp[43]*sp[37]*svp*ssm;
 
    kern[2][1] = (1.0/ denom)* kern[2][1]; 
    kern[2][2] =   - 4.E+0*sp[43]*sp[7]*svp*svm + 4.E+0*sp[43]*ssp*ssm;
 
    kern[2][2] = (1.0/ denom)* kern[2][2]; 
    kern[2][3] =  8.E+0*sp[31]*sp[37]*i_*svp*svm - 8.E+0*sp[29]*sp[39]*i_*svp*
      svm;
 
    kern[2][3] = (1.0/ denom)* kern[2][3]; 
    kern[2][4] =   0;
 
    kern[2][4] = (1.0/ denom)* kern[2][4]; 
    kern[2][5] =   0;
 
    kern[2][5] = (1.0/ denom)* kern[2][5]; 
    kern[2][6] =   0;
 
    kern[2][6] = (1.0/ denom)* kern[2][6]; 
    kern[2][7] =   0;
 
    kern[2][7] = (1.0/ denom)* kern[2][7]; 
    kern[2][8] =   0;
 
    kern[2][8] = (1.0/ denom)* kern[2][8]; 
    kern[2][9] =   0;
 
    kern[2][9] = (1.0/ denom)* kern[2][9]; 
    kern[2][10] =   0;
 
    kern[2][10] = (1.0/ denom)* kern[2][10]; 
    kern[2][11] =   0;
 
    kern[2][11] = (1.0/ denom)* kern[2][11]; 

    denom =  - 1.6E+1*sp[35]*sp[44] + 1.6E+1*pow(sp[43],2);

    kern[3][0] =  8.E+0*sp[35]*sp[39]*i_*ssp*svm - 8.E+0*sp[35]*sp[37]*i_*svp*ssm
       - 8.E+0*sp[43]*sp[31]*i_*ssp*svm + 8.E+0*sp[43]*sp[29]*i_*svp*ssm;
 
    kern[3][0] = (1.0/ denom)* kern[3][0]; 
    kern[3][1] =   - 8.E+0*sp[43]*sp[31]*sp[44]*i_*ssp*svm + 8.E+0*sp[43]*sp[29]*sp[44]
      *i_*svp*ssm + 8.E+0*pow(sp[43],2)*sp[39]*i_*ssp*svm - 8.E+0*pow(sp[43],2)
      *sp[37]*i_*svp*ssm;
 
    kern[3][1] = (1.0/ denom)* kern[3][1]; 
    kern[3][2] =  8.E+0*sp[43]*sp[31]*sp[37]*i_*svp*svm - 8.E+0*sp[43]*sp[29]*sp[39]*
      i_*svp*svm;
 
    kern[3][2] = (1.0/ denom)* kern[3][2]; 
    kern[3][3] =  1.6E+1*sp[35]*sp[44]*sp[7]*svp*svm - 1.6E+1*sp[35]*sp[44]*ssp*ssm
       - 3.2E+1*sp[35]*sp[39]*sp[37]*svp*svm + 3.2E+1*sp[43]*sp[31]*sp[37]*svp*svm + 
      3.2E+1*sp[43]*sp[29]*sp[39]*svp*svm - 1.6E+1*pow(sp[43],2)*sp[7]*svp*svm + 
      1.6E+1*pow(sp[43],2)*ssp*ssm - 3.2E+1*sp[31]*sp[29]*sp[44]*svp*svm;
 
    kern[3][3] = (1.0/ denom)* kern[3][3]; 
    kern[3][4] =   0;
 
    kern[3][4] = (1.0/ denom)* kern[3][4]; 
    kern[3][5] =   0;
 
    kern[3][5] = (1.0/ denom)* kern[3][5]; 
    kern[3][6] =   0;
 
    kern[3][6] = (1.0/ denom)* kern[3][6]; 
    kern[3][7] =   0;
 
    kern[3][7] = (1.0/ denom)* kern[3][7]; 
    kern[3][8] =   0;
 
    kern[3][8] = (1.0/ denom)* kern[3][8]; 
    kern[3][9] =   0;
 
    kern[3][9] = (1.0/ denom)* kern[3][9]; 
    kern[3][10] =   0;
 
    kern[3][10] = (1.0/ denom)* kern[3][10]; 
    kern[3][11] =   0;
 
    kern[3][11] = (1.0/ denom)* kern[3][11]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[43],2)*sp[44] + 8.E+0*pow(sp[35],2)*pow(sp[44],2)
       + 8.E+0*pow(sp[43],4);

    kern[4][0] =  8.E+0*sp[35]*sp[44]*sp[39]*sp[37]*svp*svm - 8.E+0*sp[35]*pow(
      sp[44],2)*sp[7]*svp*svm - 2.4E+1*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 2.4E+1*
      sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 8.E+0*pow(sp[43],2)*sp[44]*sp[7]*svp*svm + 
      1.6E+1*pow(sp[43],2)*sp[39]*sp[37]*svp*svm + 2.4E+1*sp[31]*sp[29]*pow(sp[44],2)
      *svp*svm;
 
    kern[4][0] = (1.0/ denom)* kern[4][0]; 
    kern[4][1] =   0;
 
    kern[4][1] = (1.0/ denom)* kern[4][1]; 
    kern[4][2] =   0;
 
    kern[4][2] = (1.0/ denom)* kern[4][2]; 
    kern[4][3] =   0;
 
    kern[4][3] = (1.0/ denom)* kern[4][3]; 
    kern[4][4] =   - 1.6E+1*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*sp[35]*
      sp[43]*sp[29]*sp[44]*sp[39]*svp*svm - 8.E+0*sp[35]*pow(sp[43],2)*sp[44]*sp[7]*svp*
      svm - 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]*ssp*ssm + 1.6E+1*sp[35]*pow(sp[43],2)*
      sp[39]*sp[37]*svp*svm + 8.E+0*sp[35]*sp[31]*sp[29]*pow(sp[44],2)*svp*svm + 
      8.E+0*pow(sp[35],2)*sp[44]*sp[39]*sp[37]*svp*svm + 8.E+0*pow(sp[35],2)*pow(
      sp[44],2)*ssp*ssm + 1.6E+1*pow(sp[43],2)*sp[31]*sp[29]*sp[44]*svp*svm - 8.E+0*
      pow(sp[43],3)*sp[31]*sp[37]*svp*svm - 8.E+0*pow(sp[43],3)*sp[29]*sp[39]*svp*svm
       + 8.E+0*pow(sp[43],4)*sp[7]*svp*svm + 8.E+0*pow(sp[43],4)*ssp*ssm;
 
    kern[4][4] = (1.0/ denom)* kern[4][4]; 
    kern[4][5] =   - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44]*sp[39]*ssp*svm + 1.6E+1*
      sp[35]*pow(sp[43],3)*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[44],2)
      *sp[39]*ssp*svm - 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[44],2)*sp[37]*svp*ssm + 
      8.E+0*pow(sp[43],5)*sp[39]*ssp*svm - 8.E+0*pow(sp[43],5)*sp[37]*svp*ssm;
 
    kern[4][5] = (1.0/ denom)* kern[4][5]; 
    kern[4][6] =   - 8.E+0*sp[35]*sp[43]*sp[44]*sp[39]*ssp*svm + 8.E+0*sp[35]*sp[43]*sp[44]
      *sp[37]*svp*ssm + 8.E+0*sp[35]*sp[31]*pow(sp[44],2)*ssp*svm - 8.E+0*sp[35]*
      sp[29]*pow(sp[44],2)*svp*ssm - 8.E+0*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm + 
      8.E+0*pow(sp[43],2)*sp[29]*sp[44]*svp*ssm + 8.E+0*pow(sp[43],3)*sp[39]*ssp*svm
       - 8.E+0*pow(sp[43],3)*sp[37]*svp*ssm;
 
    kern[4][6] = (1.0/ denom)* kern[4][6]; 
    kern[4][7] =  8.E+0*sp[35]*sp[31]*pow(sp[44],2)*sp[37]*svp*svm - 8.E+0*sp[35]*
      sp[29]*pow(sp[44],2)*sp[39]*svp*svm - 8.E+0*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp
      *svm + 8.E+0*pow(sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm;
 
    kern[4][7] = (1.0/ denom)* kern[4][7]; 
    kern[4][8] =  8.E+0*sp[35]*sp[43]*sp[31]*pow(sp[44],2)*ssp*svm + 8.E+0*sp[35]*sp[43]
      *sp[29]*pow(sp[44],2)*svp*ssm - 8.E+0*sp[35]*pow(sp[43],2)*sp[44]*sp[39]*ssp*svm
       - 8.E+0*sp[35]*pow(sp[43],2)*sp[44]*sp[37]*svp*ssm - 8.E+0*pow(sp[43],3)*sp[31]*
      sp[44]*ssp*svm - 8.E+0*pow(sp[43],3)*sp[29]*sp[44]*svp*ssm + 8.E+0*pow(
      sp[43],4)*sp[39]*ssp*svm + 8.E+0*pow(sp[43],4)*sp[37]*svp*ssm;
 
    kern[4][8] = (1.0/ denom)* kern[4][8]; 
    kern[4][9] =   - 8.E+0*sp[35]*sp[43]*sp[31]*pow(sp[44],2)*sp[37]*svp*svm - 8.E+0
      *sp[35]*sp[43]*sp[29]*pow(sp[44],2)*sp[39]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]
      *sp[39]*sp[37]*svp*svm + 8.E+0*pow(sp[43],3)*sp[31]*sp[44]*sp[37]*svp*svm + 
      8.E+0*pow(sp[43],3)*sp[29]*sp[44]*sp[39]*svp*svm - 1.6E+1*pow(sp[43],4)*sp[39]*
      sp[37]*svp*svm;
 
    kern[4][9] = (1.0/ denom)* kern[4][9]; 
    kern[4][10] =  8.E+0*sp[35]*pow(sp[43],2)*sp[44]*sp[39]*sp[37]*svp*svm - 8.E+0*
      sp[35]*pow(sp[43],2)*pow(sp[44],2)*sp[7]*svp*svm + 2.4E+1*pow(sp[43],2)*sp[31]*
      sp[29]*pow(sp[44],2)*svp*svm - 2.4E+1*pow(sp[43],3)*sp[31]*sp[44]*sp[37]*svp*svm
       - 2.4E+1*pow(sp[43],3)*sp[29]*sp[44]*sp[39]*svp*svm + 8.E+0*pow(sp[43],4)*sp[44]
      *sp[7]*svp*svm + 1.6E+1*pow(sp[43],4)*sp[39]*sp[37]*svp*svm;
 
    kern[4][10] = (1.0/ denom)* kern[4][10]; 
    kern[4][11] =   0;
 
    kern[4][11] = (1.0/ denom)* kern[4][11]; 

    denom =  - 3.2E+1*sp[35]*pow(sp[43],3)*sp[44] + 1.6E+1*pow(sp[35],2)*sp[43]*pow(
      sp[44],2) + 1.6E+1*pow(sp[43],5);

    kern[5][0] =   0;
 
    kern[5][0] = (1.0/ denom)* kern[5][0]; 
    kern[5][1] =   0;
 
    kern[5][1] = (1.0/ denom)* kern[5][1]; 
    kern[5][2] =   0;
 
    kern[5][2] = (1.0/ denom)* kern[5][2]; 
    kern[5][3] =  3.2E+1*sp[35]*sp[44]*sp[39]*sp[37]*i_*svp*svm - 3.2E+1*sp[35]*pow(
      sp[44],2)*sp[7]*i_*svp*svm - 9.6E+1*sp[43]*sp[31]*sp[44]*sp[37]*i_*svp*svm - 
      9.6E+1*sp[43]*sp[29]*sp[44]*sp[39]*i_*svp*svm + 3.2E+1*pow(sp[43],2)*sp[44]*sp[7]
      *i_*svp*svm + 6.4E+1*pow(sp[43],2)*sp[39]*sp[37]*i_*svp*svm + 9.6E+1*
      sp[31]*sp[29]*pow(sp[44],2)*i_*svp*svm;
 
    kern[5][3] = (1.0/ denom)* kern[5][3]; 
    kern[5][4] =  1.6E+1*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm - 1.6E+1*sp[35]*sp[43]*sp[29]
      *sp[44]*svp*ssm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm - 1.6E+1*sp[35]*
      pow(sp[43],2)*sp[37]*svp*ssm - 1.6E+1*pow(sp[35],2)*sp[44]*sp[39]*ssp*svm + 
      1.6E+1*pow(sp[35],2)*sp[44]*sp[37]*svp*ssm - 1.6E+1*pow(sp[43],3)*sp[31]*ssp*
      svm + 1.6E+1*pow(sp[43],3)*sp[29]*svp*ssm;
 
    kern[5][4] = (1.0/ denom)* kern[5][4]; 
    kern[5][5] =   - 1.6E+1*sp[35]*sp[43]*sp[31]*sp[29]*pow(sp[44],2)*svp*svm + 
      1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm + 1.6E+1*sp[35]*pow(
      sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],3)*sp[44]*sp[7]*
      svp*svm - 3.2E+1*sp[35]*pow(sp[43],3)*sp[44]*ssp*ssm - 6.4E+1*sp[35]*pow(
      sp[43],3)*sp[39]*sp[37]*svp*svm + 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[39]*sp[37]*
      svp*svm + 1.6E+1*pow(sp[35],2)*sp[43]*pow(sp[44],2)*ssp*ssm - 3.2E+1*pow(
      sp[43],3)*sp[31]*sp[29]*sp[44]*svp*svm + 3.2E+1*pow(sp[43],4)*sp[31]*sp[37]*svp*
      svm + 3.2E+1*pow(sp[43],4)*sp[29]*sp[39]*svp*svm - 1.6E+1*pow(sp[43],5)*
      sp[7]*svp*svm + 1.6E+1*pow(sp[43],5)*ssp*ssm;
 
    kern[5][5] = (1.0/ denom)* kern[5][5]; 
    kern[5][6] =  1.6E+1*sp[35]*sp[43]*sp[44]*sp[7]*svp*svm - 4.8E+1*sp[35]*sp[43]*
      sp[39]*sp[37]*svp*svm + 1.6E+1*sp[35]*sp[31]*sp[44]*sp[37]*svp*svm + 1.6E+1*sp[35]
      *sp[29]*sp[44]*sp[39]*svp*svm - 4.8E+1*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm + 
      3.2E+1*pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 3.2E+1*pow(sp[43],2)*sp[29]*sp[39]
      *svp*svm - 1.6E+1*pow(sp[43],3)*sp[7]*svp*svm;
 
    kern[5][6] = (1.0/ denom)* kern[5][6]; 
    kern[5][7] =  1.6E+1*sp[35]*sp[43]*sp[44]*sp[39]*ssp*svm + 1.6E+1*sp[35]*sp[43]*sp[44]*
      sp[37]*svp*ssm - 1.6E+1*sp[35]*sp[31]*pow(sp[44],2)*ssp*svm - 1.6E+1*sp[35]*
      sp[29]*pow(sp[44],2)*svp*ssm + 1.6E+1*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm + 
      1.6E+1*pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 1.6E+1*pow(sp[43],3)*sp[39]*ssp*
      svm - 1.6E+1*pow(sp[43],3)*sp[37]*svp*ssm;
 
    kern[5][7] = (1.0/ denom)* kern[5][7]; 
    kern[5][8] =  1.6E+1*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*sp[35]*sp[43]
      *sp[29]*sp[44]*sp[39]*svp*svm - 1.6E+1*pow(sp[43],3)*sp[31]*sp[37]*svp*svm + 
      1.6E+1*pow(sp[43],3)*sp[29]*sp[39]*svp*svm;
 
    kern[5][8] = (1.0/ denom)* kern[5][8]; 
    kern[5][9] =  1.6E+1*sp[35]*sp[43]*sp[31]*pow(sp[44],2)*ssp*svm - 1.6E+1*sp[35]*
      sp[43]*sp[29]*pow(sp[44],2)*svp*ssm - 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]*sp[39]*ssp*
      svm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]*sp[37]*svp*ssm - 1.6E+1*pow(sp[43],3)*
      sp[31]*sp[44]*ssp*svm + 1.6E+1*pow(sp[43],3)*sp[29]*sp[44]*svp*ssm + 1.6E+1*
      pow(sp[43],4)*sp[39]*ssp*svm - 1.6E+1*pow(sp[43],4)*sp[37]*svp*ssm;
 
    kern[5][9] = (1.0/ denom)* kern[5][9]; 
    kern[5][10] =   0;
 
    kern[5][10] = (1.0/ denom)* kern[5][10]; 
    kern[5][11] =   - 1.6E+1*sp[35]*sp[43]*sp[44]*sp[39]*sp[37]*svp*svm + 1.6E+1*sp[35]
      *sp[43]*pow(sp[44],2)*sp[7]*svp*svm - 4.8E+1*sp[43]*sp[31]*sp[29]*pow(sp[44],2)*
      svp*svm + 4.8E+1*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm + 4.8E+1*pow(
      sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm - 1.6E+1*pow(sp[43],3)*sp[44]*sp[7]*svp*
      svm - 3.2E+1*pow(sp[43],3)*sp[39]*sp[37]*svp*svm;
 
    kern[5][11] = (1.0/ denom)* kern[5][11]; 

    denom =  - 1.6E+1*sp[35]*sp[44] + 1.6E+1*pow(sp[43],2);

    kern[6][0] =   - 1.6E+1*sp[43]*sp[39]*ssp*svm + 1.6E+1*sp[43]*sp[37]*svp*ssm
       + 1.6E+1*sp[31]*sp[44]*ssp*svm - 1.6E+1*sp[29]*sp[44]*svp*ssm;
 
    kern[6][0] = (1.0/ denom)* kern[6][0]; 
    kern[6][1] =   0;
 
    kern[6][1] = (1.0/ denom)* kern[6][1]; 
    kern[6][2] =   0;
 
    kern[6][2] = (1.0/ denom)* kern[6][2]; 
    kern[6][3] =   - 6.4E+1*sp[43]*sp[39]*sp[37]*i_*svp*svm + 3.2E+1*sp[31]*sp[44]*
      sp[37]*i_*svp*svm + 3.2E+1*sp[29]*sp[44]*sp[39]*i_*svp*svm;
 
    kern[6][3] = (1.0/ denom)* kern[6][3]; 
    kern[6][4] =   - 1.6E+1*sp[35]*sp[43]*sp[39]*ssp*svm + 1.6E+1*sp[35]*sp[43]*sp[37]*
      svp*ssm + 1.6E+1*sp[35]*sp[31]*sp[44]*ssp*svm - 1.6E+1*sp[35]*sp[29]*sp[44]*svp*
      ssm;
 
    kern[6][4] = (1.0/ denom)* kern[6][4]; 
    kern[6][5] =   - 1.6E+1*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*sp[35]*
      sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 3.2E+1*sp[35]*pow(sp[43],2)*sp[39]*sp[37]*svp*
      svm;
 
    kern[6][5] = (1.0/ denom)* kern[6][5]; 
    kern[6][6] =   - 1.6E+1*sp[35]*sp[44]*ssp*ssm + 1.6E+1*sp[35]*sp[39]*sp[37]*svp*
      svm + 1.6E+1*pow(sp[43],2)*ssp*ssm - 1.6E+1*sp[31]*sp[29]*sp[44]*svp*svm;
 
    kern[6][6] = (1.0/ denom)* kern[6][6]; 
    kern[6][7] =   - 1.6E+1*sp[35]*sp[44]*sp[39]*ssp*svm - 1.6E+1*sp[35]*sp[44]*sp[37]*
      svp*ssm + 1.6E+1*pow(sp[43],2)*sp[39]*ssp*svm + 1.6E+1*pow(sp[43],2)*sp[37]
      *svp*ssm;
 
    kern[6][7] = (1.0/ denom)* kern[6][7]; 
    kern[6][8] =   0;
 
    kern[6][8] = (1.0/ denom)* kern[6][8]; 
    kern[6][9] =   0;
 
    kern[6][9] = (1.0/ denom)* kern[6][9]; 
    kern[6][10] =  1.6E+1*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm - 1.6E+1*pow(
      sp[43],2)*sp[29]*sp[44]*svp*ssm - 1.6E+1*pow(sp[43],3)*sp[39]*ssp*svm + 1.6E+1
      *pow(sp[43],3)*sp[37]*svp*ssm;
 
    kern[6][10] = (1.0/ denom)* kern[6][10]; 
    kern[6][11] =   - 1.6E+1*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*sp[43]*
      sp[29]*sp[44]*sp[39]*svp*svm + 3.2E+1*pow(sp[43],2)*sp[39]*sp[37]*svp*svm;
 
    kern[6][11] = (1.0/ denom)* kern[6][11]; 

    denom =  - 4.8E+1*sp[35]*sp[44] + 4.8E+1*pow(sp[43],2);

    kern[7][0] =   - 4.8E+1*sp[31]*sp[37]*svp*svm + 4.8E+1*sp[29]*sp[39]*svp*
      svm;
 
    kern[7][0] = (1.0/ denom)* kern[7][0]; 
    kern[7][1] =   0;
 
    kern[7][1] = (1.0/ denom)* kern[7][1]; 
    kern[7][2] =   0;
 
    kern[7][2] = (1.0/ denom)* kern[7][2]; 
    kern[7][3] =   - 9.6E+1*sp[43]*sp[39]*i_*ssp*svm - 9.6E+1*sp[43]*sp[37]*i_*
      svp*ssm + 9.6E+1*sp[31]*sp[44]*i_*ssp*svm + 9.6E+1*sp[29]*sp[44]*i_*svp*ssm
      ;
 
    kern[7][3] = (1.0/ denom)* kern[7][3]; 
    kern[7][4] =   - 4.8E+1*sp[35]*sp[31]*sp[37]*svp*svm + 4.8E+1*sp[35]*sp[29]*
      sp[39]*svp*svm;
 
    kern[7][4] = (1.0/ denom)* kern[7][4]; 
    kern[7][5] =   - 4.8E+1*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm - 4.8E+1*sp[35]*sp[43]*
      sp[29]*sp[44]*svp*ssm + 4.8E+1*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm + 4.8E+1*
      sp[35]*pow(sp[43],2)*sp[37]*svp*ssm;
 
    kern[7][5] = (1.0/ denom)* kern[7][5]; 
    kern[7][6] =  4.8E+1*sp[35]*sp[39]*ssp*svm + 4.8E+1*sp[35]*sp[37]*svp*ssm - 
      4.8E+1*sp[43]*sp[31]*ssp*svm - 4.8E+1*sp[43]*sp[29]*svp*ssm;
 
    kern[7][6] = (1.0/ denom)* kern[7][6]; 
    kern[7][7] =   - 4.8E+1*sp[35]*sp[44]*ssp*ssm + 4.8E+1*sp[35]*sp[39]*sp[37]*svp*
      svm - 4.8E+1*sp[43]*sp[31]*sp[37]*svp*svm - 4.8E+1*sp[43]*sp[29]*sp[39]*svp*svm
       + 4.8E+1*pow(sp[43],2)*ssp*ssm + 4.8E+1*sp[31]*sp[29]*sp[44]*svp*svm;
 
    kern[7][7] = (1.0/ denom)* kern[7][7]; 
    kern[7][8] =   0;
 
    kern[7][8] = (1.0/ denom)* kern[7][8]; 
    kern[7][9] =   0;
 
    kern[7][9] = (1.0/ denom)* kern[7][9]; 
    kern[7][10] =   - 4.8E+1*pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 4.8E+1*pow(
      sp[43],2)*sp[29]*sp[39]*svp*svm;
 
    kern[7][10] = (1.0/ denom)* kern[7][10]; 
    kern[7][11] =   - 4.8E+1*sp[43]*sp[31]*sp[44]*ssp*svm - 4.8E+1*sp[43]*sp[29]*sp[44]
      *svp*ssm + 4.8E+1*pow(sp[43],2)*sp[39]*ssp*svm + 4.8E+1*pow(sp[43],2)*
      sp[37]*svp*ssm;
 
    kern[7][11] = (1.0/ denom)* kern[7][11]; 

    denom =  - 4.E+0*sp[35]*sp[43]*sp[44] + 4.E+0*pow(sp[43],3);

    kern[8][0] =  4.E+0*sp[43]*sp[39]*ssp*svm + 4.E+0*sp[43]*sp[37]*svp*ssm - 
      4.E+0*sp[31]*sp[44]*ssp*svm - 4.E+0*sp[29]*sp[44]*svp*ssm;
 
    kern[8][0] = (1.0/ denom)* kern[8][0]; 
    kern[8][1] =   0;
 
    kern[8][1] = (1.0/ denom)* kern[8][1]; 
    kern[8][2] =   0;
 
    kern[8][2] = (1.0/ denom)* kern[8][2]; 
    kern[8][3] =   - 8.E+0*sp[31]*sp[44]*sp[37]*i_*svp*svm + 8.E+0*sp[29]*sp[44]*
      sp[39]*i_*svp*svm;
 
    kern[8][3] = (1.0/ denom)* kern[8][3]; 
    kern[8][4] =  4.E+0*sp[35]*sp[43]*sp[39]*ssp*svm + 4.E+0*sp[35]*sp[43]*sp[37]*svp*
      ssm - 4.E+0*pow(sp[43],2)*sp[31]*ssp*svm - 4.E+0*pow(sp[43],2)*sp[29]*svp*
      ssm;
 
    kern[8][4] = (1.0/ denom)* kern[8][4]; 
    kern[8][5] =  4.E+0*pow(sp[43],3)*sp[31]*sp[37]*svp*svm - 4.E+0*pow(sp[43],3)
      *sp[29]*sp[39]*svp*svm;
 
    kern[8][5] = (1.0/ denom)* kern[8][5]; 
    kern[8][6] =  4.E+0*sp[43]*sp[31]*sp[37]*svp*svm - 4.E+0*sp[43]*sp[29]*sp[39]*svp
      *svm;
 
    kern[8][6] = (1.0/ denom)* kern[8][6]; 
    kern[8][7] =   0;
 
    kern[8][7] = (1.0/ denom)* kern[8][7]; 
    kern[8][8] =  4.E+0*sp[35]*sp[43]*sp[44]*sp[7]*svp*svm - 4.E+0*sp[35]*sp[43]*sp[44]*
      ssp*ssm - 4.E+0*pow(sp[43],3)*sp[7]*svp*svm + 4.E+0*pow(sp[43],3)*ssp*
      ssm;
 
    kern[8][8] = (1.0/ denom)* kern[8][8]; 
    kern[8][9] =  4.E+0*sp[35]*sp[43]*sp[44]*sp[39]*ssp*svm + 4.E+0*sp[35]*sp[43]*sp[44]*
      sp[37]*svp*ssm - 4.E+0*pow(sp[43],3)*sp[39]*ssp*svm - 4.E+0*pow(sp[43],3)*
      sp[37]*svp*ssm;
 
    kern[8][9] = (1.0/ denom)* kern[8][9]; 
    kern[8][10] =   - 4.E+0*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm - 4.E+0*pow(
      sp[43],2)*sp[29]*sp[44]*svp*ssm + 4.E+0*pow(sp[43],3)*sp[39]*ssp*svm + 4.E+0*
      pow(sp[43],3)*sp[37]*svp*ssm;
 
    kern[8][10] = (1.0/ denom)* kern[8][10]; 
    kern[8][11] =  4.E+0*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 4.E+0*sp[43]*sp[29]*sp[44]
      *sp[39]*svp*svm;
 
    kern[8][11] = (1.0/ denom)* kern[8][11]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44] + 8.E+0*pow(sp[35],2)*sp[43]*pow(
      sp[44],2) + 8.E+0*pow(sp[43],5);

    kern[9][0] =  8.E+0*sp[35]*sp[43]*sp[44]*sp[7]*svp*svm - 2.4E+1*sp[35]*sp[43]*sp[39]
      *sp[37]*svp*svm + 8.E+0*sp[35]*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*sp[35]*sp[29]*
      sp[44]*sp[39]*svp*svm - 2.4E+1*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm + 1.6E+1*pow(
      sp[43],2)*sp[31]*sp[37]*svp*svm + 1.6E+1*pow(sp[43],2)*sp[29]*sp[39]*svp*svm - 
      8.E+0*pow(sp[43],3)*sp[7]*svp*svm;
 
    kern[9][0] = (1.0/ denom)* kern[9][0]; 
    kern[9][1] =   0;
 
    kern[9][1] = (1.0/ denom)* kern[9][1]; 
    kern[9][2] =   0;
 
    kern[9][2] = (1.0/ denom)* kern[9][2]; 
    kern[9][3] =  1.6E+1*sp[35]*sp[43]*sp[44]*sp[39]*i_*ssp*svm - 1.6E+1*sp[35]*sp[43]*
      sp[44]*sp[37]*i_*svp*ssm - 1.6E+1*sp[35]*sp[31]*pow(sp[44],2)*i_*ssp*svm + 
      1.6E+1*sp[35]*sp[29]*pow(sp[44],2)*i_*svp*ssm + 1.6E+1*pow(sp[43],2)*sp[31]*
      sp[44]*i_*ssp*svm - 1.6E+1*pow(sp[43],2)*sp[29]*sp[44]*i_*svp*ssm - 1.6E+1*
      pow(sp[43],3)*sp[39]*i_*ssp*svm + 1.6E+1*pow(sp[43],3)*sp[37]*i_*svp*ssm;
 
    kern[9][3] = (1.0/ denom)* kern[9][3]; 
    kern[9][4] =   - 8.E+0*sp[35]*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm + 2.4E+1*sp[35]*
      pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 2.4E+1*sp[35]*pow(sp[43],2)*sp[29]*sp[39]*
      svp*svm - 8.E+0*sp[35]*pow(sp[43],3)*sp[7]*svp*svm + 8.E+0*pow(sp[35],2)*
      sp[43]*sp[44]*sp[7]*svp*svm - 2.4E+1*pow(sp[35],2)*sp[43]*sp[39]*sp[37]*svp*svm
       - 1.6E+1*pow(sp[43],3)*sp[31]*sp[29]*svp*svm;
 
    kern[9][4] = (1.0/ denom)* kern[9][4]; 
    kern[9][5] =  8.E+0*sp[35]*pow(sp[43],3)*sp[31]*sp[44]*ssp*svm - 8.E+0*sp[35]*
      pow(sp[43],3)*sp[29]*sp[44]*svp*ssm + 8.E+0*sp[35]*pow(sp[43],4)*sp[39]*ssp*svm
       - 8.E+0*sp[35]*pow(sp[43],4)*sp[37]*svp*ssm - 8.E+0*pow(sp[35],2)*pow(
      sp[43],2)*sp[44]*sp[39]*ssp*svm + 8.E+0*pow(sp[35],2)*pow(sp[43],2)*sp[44]*sp[37]*
      svp*ssm - 8.E+0*pow(sp[43],5)*sp[31]*ssp*svm + 8.E+0*pow(sp[43],5)*sp[29]*
      svp*ssm;
 
    kern[9][5] = (1.0/ denom)* kern[9][5]; 
    kern[9][6] =   0;
 
    kern[9][6] = (1.0/ denom)* kern[9][6]; 
    kern[9][7] =   - 8.E+0*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*sp[35]*
      sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 8.E+0*pow(sp[43],3)*sp[31]*sp[37]*svp*svm - 
      8.E+0*pow(sp[43],3)*sp[29]*sp[39]*svp*svm;
 
    kern[9][7] = (1.0/ denom)* kern[9][7]; 
    kern[9][8] =   - 8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm - 8.E+0*sp[35]*
      pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 8.E+0*sp[35]*pow(sp[43],3)*sp[39]*ssp*svm
       - 8.E+0*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*
      sp[39]*ssp*svm + 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(
      sp[43],4)*sp[31]*ssp*svm + 8.E+0*pow(sp[43],4)*sp[29]*svp*ssm;
 
    kern[9][8] = (1.0/ denom)* kern[9][8]; 
    kern[9][9] =  8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*
      sp[35]*pow(sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44]*
      sp[7]*svp*svm - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44]*ssp*ssm + 1.6E+1*sp[35]*
      pow(sp[43],3)*sp[39]*sp[37]*svp*svm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[39]*
      sp[37]*svp*svm + 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[44],2)*sp[7]*svp*svm + 
      8.E+0*pow(sp[35],2)*sp[43]*pow(sp[44],2)*ssp*ssm - 8.E+0*pow(sp[43],4)*sp[31]*
      sp[37]*svp*svm - 8.E+0*pow(sp[43],4)*sp[29]*sp[39]*svp*svm + 8.E+0*pow(
      sp[43],5)*sp[7]*svp*svm + 8.E+0*pow(sp[43],5)*ssp*ssm;
 
    kern[9][9] = (1.0/ denom)* kern[9][9]; 
    kern[9][10] =  8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*
      sp[35]*pow(sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm + 8.E+0*sp[35]*pow(sp[43],3)*sp[44]*
      sp[7]*svp*svm - 2.4E+1*sp[35]*pow(sp[43],3)*sp[39]*sp[37]*svp*svm - 2.4E+1*
      pow(sp[43],3)*sp[31]*sp[29]*sp[44]*svp*svm + 1.6E+1*pow(sp[43],4)*sp[31]*sp[37]*
      svp*svm + 1.6E+1*pow(sp[43],4)*sp[29]*sp[39]*svp*svm - 8.E+0*pow(sp[43],5)*
      sp[7]*svp*svm;
 
    kern[9][10] = (1.0/ denom)* kern[9][10]; 
    kern[9][11] =  8.E+0*sp[35]*sp[43]*sp[31]*pow(sp[44],2)*ssp*svm - 8.E+0*sp[35]*
      sp[43]*sp[29]*pow(sp[44],2)*svp*ssm - 8.E+0*sp[35]*pow(sp[43],2)*sp[44]*sp[39]*ssp*
      svm + 8.E+0*sp[35]*pow(sp[43],2)*sp[44]*sp[37]*svp*ssm - 8.E+0*pow(sp[43],3)*
      sp[31]*sp[44]*ssp*svm + 8.E+0*pow(sp[43],3)*sp[29]*sp[44]*svp*ssm + 8.E+0*pow(
      sp[43],4)*sp[39]*ssp*svm - 8.E+0*pow(sp[43],4)*sp[37]*svp*ssm;
 
    kern[9][11] = (1.0/ denom)* kern[9][11]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44] + 8.E+0*pow(sp[35],2)*sp[43]*pow(
      sp[44],2) + 8.E+0*pow(sp[43],5);

    kern[10][0] =  8.E+0*sp[35]*sp[43]*sp[44]*sp[7]*svp*svm - 2.4E+1*sp[35]*sp[43]*
      sp[39]*sp[37]*svp*svm + 8.E+0*sp[35]*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*sp[35]*
      sp[29]*sp[44]*sp[39]*svp*svm - 2.4E+1*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm + 1.6E+1
      *pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 1.6E+1*pow(sp[43],2)*sp[29]*sp[39]*svp*
      svm - 8.E+0*pow(sp[43],3)*sp[7]*svp*svm;
 
    kern[10][0] = (1.0/ denom)* kern[10][0]; 
    kern[10][1] =   - 1.6E+1*sp[35]*sp[43]*sp[44]*sp[39]*sp[37]*svp*svm + 8.E+0*sp[35]*
      sp[31]*pow(sp[44],2)*sp[37]*svp*svm + 8.E+0*sp[35]*sp[29]*pow(sp[44],2)*sp[39]*svp
      *svm - 8.E+0*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm - 8.E+0*pow(sp[43],2)*
      sp[29]*sp[44]*sp[39]*svp*svm + 1.6E+1*pow(sp[43],3)*sp[39]*sp[37]*svp*svm;
 
    kern[10][1] = (1.0/ denom)* kern[10][1]; 
    kern[10][2] =   - 8.E+0*sp[35]*sp[43]*sp[44]*sp[39]*ssp*svm - 8.E+0*sp[35]*sp[43]*
      sp[44]*sp[37]*svp*ssm + 8.E+0*sp[35]*sp[31]*pow(sp[44],2)*ssp*svm + 8.E+0*sp[35]*
      sp[29]*pow(sp[44],2)*svp*ssm - 8.E+0*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm - 
      8.E+0*pow(sp[43],2)*sp[29]*sp[44]*svp*ssm + 8.E+0*pow(sp[43],3)*sp[39]*ssp*svm
       + 8.E+0*pow(sp[43],3)*sp[37]*svp*ssm;
 
    kern[10][2] = (1.0/ denom)* kern[10][2]; 
    kern[10][3] =   0;
 
    kern[10][3] = (1.0/ denom)* kern[10][3]; 
    kern[10][4] =   - 2.4E+1*sp[35]*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm + 1.6E+1*sp[35]
      *pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[29]*sp[39]*
      svp*svm - 8.E+0*sp[35]*pow(sp[43],3)*sp[7]*svp*svm + 8.E+0*pow(sp[35],2)*
      sp[43]*sp[44]*sp[7]*svp*svm - 2.4E+1*pow(sp[35],2)*sp[43]*sp[39]*sp[37]*svp*svm
       + 8.E+0*pow(sp[35],2)*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*pow(sp[35],2)*sp[29]
      *sp[44]*sp[39]*svp*svm;
 
    kern[10][4] = (1.0/ denom)* kern[10][4]; 
    kern[10][5] =   0;
 
    kern[10][5] = (1.0/ denom)* kern[10][5]; 
    kern[10][6] =   - 8.E+0*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm + 8.E+0*sp[35]*sp[43]*
      sp[29]*sp[44]*svp*ssm - 8.E+0*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm + 8.E+0*sp[35]*
      pow(sp[43],2)*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*sp[44]*sp[39]*ssp*svm - 
      8.E+0*pow(sp[35],2)*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(sp[43],3)*sp[31]*ssp*svm
       - 8.E+0*pow(sp[43],3)*sp[29]*svp*ssm;
 
    kern[10][6] = (1.0/ denom)* kern[10][6]; 
    kern[10][7] =   - 8.E+0*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*sp[35]*
      sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 8.E+0*pow(sp[43],3)*sp[31]*sp[37]*svp*svm - 
      8.E+0*pow(sp[43],3)*sp[29]*sp[39]*svp*svm;
 
    kern[10][7] = (1.0/ denom)* kern[10][7]; 
    kern[10][8] =  8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm + 8.E+0*sp[35]*
      pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 8.E+0*sp[35]*pow(sp[43],3)*sp[39]*ssp*svm
       - 8.E+0*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*
      sp[39]*ssp*svm + 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm - 8.E+0*pow(
      sp[35],2)*sp[31]*pow(sp[44],2)*ssp*svm - 8.E+0*pow(sp[35],2)*sp[29]*pow(sp[44],2)
      *svp*ssm;
 
    kern[10][8] = (1.0/ denom)* kern[10][8]; 
    kern[10][9] =   - 8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm - 
      8.E+0*sp[35]*pow(sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm + 1.6E+1*sp[35]*pow(
      sp[43],3)*sp[39]*sp[37]*svp*svm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[39]*sp[37]*
      svp*svm + 8.E+0*pow(sp[35],2)*sp[31]*pow(sp[44],2)*sp[37]*svp*svm + 8.E+0*
      pow(sp[35],2)*sp[29]*pow(sp[44],2)*sp[39]*svp*svm;
 
    kern[10][9] = (1.0/ denom)* kern[10][9]; 
    kern[10][10] =   - 1.6E+1*sp[35]*sp[43]*sp[31]*sp[29]*pow(sp[44],2)*svp*svm + 
      1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm + 1.6E+1*sp[35]*pow(
      sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm - 8.E+0*sp[35]*pow(sp[43],3)*sp[44]*sp[7]*svp
      *svm - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44]*ssp*ssm - 2.4E+1*sp[35]*pow(sp[43],3)*
      sp[39]*sp[37]*svp*svm + 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[44],2)*sp[7]*svp*svm
       + 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[44],2)*ssp*ssm - 8.E+0*pow(sp[43],3)*
      sp[31]*sp[29]*sp[44]*svp*svm + 8.E+0*pow(sp[43],4)*sp[31]*sp[37]*svp*svm + 
      8.E+0*pow(sp[43],4)*sp[29]*sp[39]*svp*svm + 8.E+0*pow(sp[43],5)*ssp*ssm;
 
    kern[10][10] = (1.0/ denom)* kern[10][10]; 
    kern[10][11] =   - 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]*sp[39]*ssp*svm + 1.6E+1*
      sp[35]*pow(sp[43],2)*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*pow(sp[44],2)*
      sp[39]*ssp*svm - 8.E+0*pow(sp[35],2)*pow(sp[44],2)*sp[37]*svp*ssm + 8.E+0*
      pow(sp[43],4)*sp[39]*ssp*svm - 8.E+0*pow(sp[43],4)*sp[37]*svp*ssm;
 
    kern[10][11] = (1.0/ denom)* kern[10][11]; 

    denom =  - 3.2E+1*sp[35]*pow(sp[43],2)*sp[44] + 1.6E+1*pow(sp[35],2)*pow(sp[44],2)
       + 1.6E+1*pow(sp[43],4);

    kern[11][0] =   0;
 
    kern[11][0] = (1.0/ denom)* kern[11][0]; 
    kern[11][1] =  1.6E+1*sp[35]*sp[43]*sp[44]*sp[39]*ssp*svm - 1.6E+1*sp[35]*sp[43]*sp[44]
      *sp[37]*svp*ssm - 1.6E+1*sp[35]*sp[31]*pow(sp[44],2)*ssp*svm + 1.6E+1*sp[35]*
      sp[29]*pow(sp[44],2)*svp*ssm + 1.6E+1*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm - 
      1.6E+1*pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 1.6E+1*pow(sp[43],3)*sp[39]*ssp*
      svm + 1.6E+1*pow(sp[43],3)*sp[37]*svp*ssm;
 
    kern[11][1] = (1.0/ denom)* kern[11][1]; 
    kern[11][2] =  1.6E+1*sp[35]*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*sp[35]*sp[29]*
      sp[44]*sp[39]*svp*svm - 1.6E+1*pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 1.6E+1*
      pow(sp[43],2)*sp[29]*sp[39]*svp*svm;
 
    kern[11][2] = (1.0/ denom)* kern[11][2]; 
    kern[11][3] =  3.2E+1*sp[35]*sp[43]*sp[44]*sp[7]*i_*svp*svm - 9.6E+1*sp[35]*sp[43]
      *sp[39]*sp[37]*i_*svp*svm + 3.2E+1*sp[35]*sp[31]*sp[44]*sp[37]*i_*svp*svm + 
      3.2E+1*sp[35]*sp[29]*sp[44]*sp[39]*i_*svp*svm - 9.6E+1*sp[43]*sp[31]*sp[29]*sp[44]*i_
      *svp*svm + 6.4E+1*pow(sp[43],2)*sp[31]*sp[37]*i_*svp*svm + 6.4E+1*pow(
      sp[43],2)*sp[29]*sp[39]*i_*svp*svm - 3.2E+1*pow(sp[43],3)*sp[7]*i_*svp*svm;
 
    kern[11][3] = (1.0/ denom)* kern[11][3]; 
    kern[11][4] =   0;
 
    kern[11][4] = (1.0/ denom)* kern[11][4]; 
    kern[11][5] =  4.8E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[29]*sp[44]*svp*svm - 3.2E+1
      *sp[35]*pow(sp[43],3)*sp[31]*sp[37]*svp*svm - 3.2E+1*sp[35]*pow(sp[43],3)*sp[29]*
      sp[39]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],4)*sp[7]*svp*svm - 1.6E+1*pow(
      sp[35],2)*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[29]*sp[44]
      *sp[39]*svp*svm - 1.6E+1*pow(sp[35],2)*pow(sp[43],2)*sp[44]*sp[7]*svp*svm + 
      4.8E+1*pow(sp[35],2)*pow(sp[43],2)*sp[39]*sp[37]*svp*svm;
 
    kern[11][5] = (1.0/ denom)* kern[11][5]; 
    kern[11][6] =   - 4.8E+1*sp[35]*sp[43]*sp[31]*sp[37]*svp*svm - 4.8E+1*sp[35]*sp[43]
      *sp[29]*sp[39]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[7]*svp*svm + 1.6E+1
      *sp[35]*sp[31]*sp[29]*sp[44]*svp*svm - 1.6E+1*pow(sp[35],2)*sp[44]*sp[7]*svp*svm
       + 4.8E+1*pow(sp[35],2)*sp[39]*sp[37]*svp*svm + 3.2E+1*pow(sp[43],2)*sp[31]*
      sp[29]*svp*svm;
 
    kern[11][6] = (1.0/ denom)* kern[11][6]; 
    kern[11][7] =  1.6E+1*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm + 1.6E+1*sp[35]*sp[43]*
      sp[29]*sp[44]*svp*ssm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm + 1.6E+1*
      sp[35]*pow(sp[43],2)*sp[37]*svp*ssm - 1.6E+1*pow(sp[35],2)*sp[44]*sp[39]*ssp*svm
       - 1.6E+1*pow(sp[35],2)*sp[44]*sp[37]*svp*ssm - 1.6E+1*pow(sp[43],3)*sp[31]*
      ssp*svm - 1.6E+1*pow(sp[43],3)*sp[29]*svp*ssm;
 
    kern[11][7] = (1.0/ denom)* kern[11][7]; 
    kern[11][8] =  1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[37]*svp*svm - 1.6E+1*sp[35]
      *pow(sp[43],2)*sp[29]*sp[39]*svp*svm - 1.6E+1*pow(sp[35],2)*sp[31]*sp[44]*sp[37]*
      svp*svm + 1.6E+1*pow(sp[35],2)*sp[29]*sp[44]*sp[39]*svp*svm;
 
    kern[11][8] = (1.0/ denom)* kern[11][8]; 
    kern[11][9] =  1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm - 1.6E+1*sp[35]*
      pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 1.6E+1*sp[35]*pow(sp[43],3)*sp[39]*ssp*svm
       + 1.6E+1*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm + 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]
      *sp[39]*ssp*svm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm - 1.6E+1*
      pow(sp[35],2)*sp[31]*pow(sp[44],2)*ssp*svm + 1.6E+1*pow(sp[35],2)*sp[29]*pow(
      sp[44],2)*svp*ssm;
 
    kern[11][9] = (1.0/ denom)* kern[11][9]; 
    kern[11][10] =  1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm - 1.6E+1*sp[35]
      *pow(sp[43],2)*sp[29]*sp[44]*svp*ssm + 1.6E+1*sp[35]*pow(sp[43],3)*sp[39]*ssp*svm
       - 1.6E+1*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]
      *sp[39]*ssp*svm + 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm - 1.6E+1*
      pow(sp[43],4)*sp[31]*ssp*svm + 1.6E+1*pow(sp[43],4)*sp[29]*svp*ssm;
 
    kern[11][10] = (1.0/ denom)* kern[11][10]; 
    kern[11][11] =   - 4.8E+1*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 4.8E+1*
      sp[35]*sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]*sp[7]*
      svp*svm - 3.2E+1*sp[35]*pow(sp[43],2)*sp[44]*ssp*ssm + 1.6E+1*sp[35]*pow(
      sp[43],2)*sp[39]*sp[37]*svp*svm + 3.2E+1*sp[35]*sp[31]*sp[29]*pow(sp[44],2)*svp*
      svm + 3.2E+1*pow(sp[35],2)*sp[44]*sp[39]*sp[37]*svp*svm - 1.6E+1*pow(sp[35],2)
      *pow(sp[44],2)*sp[7]*svp*svm + 1.6E+1*pow(sp[35],2)*pow(sp[44],2)*ssp*ssm
       + 1.6E+1*pow(sp[43],2)*sp[31]*sp[29]*sp[44]*svp*svm + 1.6E+1*pow(sp[43],4)*
      ssp*ssm;
 
    kern[11][11] = (1.0/ denom)* kern[11][11]; 


}


void kernelL_qphv(matCdoub& kern,
              const ArrayScalarProducts& sp,
              const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){


    // local variables
    array<Cdoub, 91> w;
    Cdoub denom;


    w[1]=sp[35];
    w[2]=sp[27];
    w[3]=sp[34];
    w[4]=sp[43];
    w[5]=sp[44];
   w[6]=w[2]*ang[0];
   w[7]=3.E+0*w[6];
   w[8]=w[5]*ang[0];
   w[9]=w[8] - ang[1];
   w[10]=w[7] + w[9];
   w[10]=w[10]*w[4];
   w[11]=2.E+0*ang[1];
   w[12]=w[11] - w[8];
   w[13]=w[12] - w[6];
   w[14]=w[13]*w[3];
   w[10]=w[10] + w[14];
   w[10]=w[10]*w[3];
   w[14]=2.E+0*w[6];
   w[15]=pow(w[4],2);
   w[16]=w[14]*w[15];
   w[10]=w[10] - w[16];
   w[17]=w[1]*w[2];
   w[18]=ang[1]*w[17];
   w[18]=w[18] - w[10];
   w[18]=4.E+0*w[18];
   w[19]=3.E+0*ang[1];
   w[20]=w[19] - w[6];
   w[21]=w[20]*w[2];
   w[22]=w[8] + ang[1];
   w[23]=w[22]*w[5];
   w[24]=2.E+0*ang[2];
   w[23]=w[23] - w[24];
   w[21]=w[21] + w[23];
   w[25]=w[3]*w[4];
   w[26]=w[21]*w[25];
   w[27]=w[6] - w[8];
   w[28]=w[27] - w[11];
   w[29]=w[28]*w[15];
   w[30]=2.E+0*w[2];
   w[31]=w[29]*w[30];
   w[26]=w[31] + w[26];
   w[31]= - 2.E+0*w[26];
   w[32]=w[3]*ang[0];
   w[33]=2.E+0*ang[0];
   w[34]=w[33]*w[4];
   w[32]=w[32] - w[34];
   w[32]=w[32]*w[3];
   w[34]=w[15]*ang[0];
   w[32]=w[32] + w[34];
   w[35]=2.E+0*w[3];
   w[36]= - w[32]*w[35];
   w[37]=w[9] - w[6];
   w[38]=w[4] - w[3];
   w[38]=w[1]*w[37]*w[38];
   w[36]=w[36] + w[38];
   w[36]=4.E+0*w[36];
   w[38]=w[27] - ang[1];
   w[38]=w[38]*w[25];
   w[38]=w[38] - w[29];
   w[39]= - w[38]*w[35];
   w[40]=2.E+0*w[8];
   w[41]=w[40] - ang[1];
   w[42]=w[41] - w[6];
   w[42]=w[42]*w[2];
   w[42]=w[42] - w[23];
   w[43]= - w[42]*w[1]*w[4];
   w[39]=w[39] + w[43];
   w[39]=2.E+0*w[39];
   w[43]=1.2E+1*ang[1];
   w[44]=w[4]*w[43];
   w[45]=w[41] + w[14];
   w[46]=w[4]*w[45];
   w[47]= - w[3]*w[40];
   w[46]=w[46] + w[47];
   w[46]=w[3]*w[46];
   w[16]= - w[16] + w[46];
   w[46]=w[6] - ang[1];
   w[47]=w[46] - w[40];
   w[47]=w[47]*w[2];
   w[48]=w[9]*w[5];
   w[47]=w[47] + w[48];
   w[49]= - w[1]*w[47];
   w[16]=2.E+0*w[16] + w[49];
   w[16]=8.E+0*w[16];
   w[49]=w[6] + 2.E+0*w[41];
   w[49]=w[49]*w[2];
   w[12]=w[12]*w[5];
   w[12]=w[12] - ang[2];
   w[49]=w[49] - w[12];
   w[49]=w[49]*w[3];
   w[50]=w[9] + w[6];
   w[51]=w[4]*w[2];
   w[52]=6.E+0*w[51];
   w[50]=w[50]*w[52];
   w[49]=w[49] - w[50];
   w[49]=w[49]*w[3];
   w[50]=pow(w[2],2);
   w[53]=6.E+0*w[50];
   w[34]=w[53]*w[34];
   w[34]=w[49] + w[34];
   w[49]=2.E+0*w[22];
   w[54]=w[49] - w[6];
   w[54]=w[54]*w[2];
   w[54]=w[54] + w[12];
   w[54]=w[54]*w[17];
   w[34]= - w[54] + 2.E+0*w[34];
   w[54]=2.E+0*w[34];
   w[55]=3.E+0*w[8];
   w[56]=4.E+0*ang[1];
   w[57]=w[55] - w[56];
   w[58]=w[57] + w[6];
   w[58]=w[58]*w[2];
   w[59]=8.E+0*ang[1];
   w[60]=w[59] + w[55];
   w[60]=w[60]*w[5];
   w[61]=5.E+0*ang[2];
   w[58]=w[61] + w[58] - w[60];
   w[58]=w[58]*w[2];
   w[60]=3.E+0*ang[2];
   w[62]=pow(w[5],2);
   w[63]=w[62]*ang[0];
   w[64]=w[60] - w[63];
   w[64]=w[64]*w[5];
   w[64]=w[64] - 2.E+0*ang[3];
   w[58]=w[58] + w[64];
   w[58]=w[58]*w[3];
   w[52]= - w[21]*w[52];
   w[52]=w[52] - w[58];
   w[52]=w[3]*w[52];
   w[65]= - w[29]*w[53];
   w[52]=w[65] + w[52];
   w[65]=w[55] + w[56];
   w[66]=w[65] - w[6];
   w[66]=w[66]*w[2];
   w[65]=w[65]*w[5];
   w[66]=w[66] - w[65] - w[61];
   w[66]=w[66]*w[2];
   w[66]=w[66] - w[64];
   w[66]=w[66]*w[17];
   w[52]=2.E+0*w[52] + w[66];
   w[67]=w[11] + w[8];
   w[68]= - w[6] + 2.E+0*w[67];
   w[68]=w[68]*w[2];
   w[69]=w[68] - w[12];
   w[69]=w[3]*w[69];
   w[70]=4.E+0*w[51];
   w[71]= - w[9]*w[70];
   w[69]=w[71] + w[69];
   w[69]=w[3]*w[69];
   w[71]=w[50]*w[15];
   w[72]=w[33]*w[71];
   w[69]=w[72] + w[69];
   w[72]= - w[19] - w[8];
   w[72]=2.E+0*w[72] + w[6];
   w[72]=w[2]*w[72];
   w[72]=w[72] - w[12];
   w[72]=w[72]*w[17];
   w[69]=2.E+0*w[69] + w[72];
   w[69]=w[1]*w[69];
   w[72]=pow(w[3],2);
   w[73]=4.E+0*w[72];
   w[10]= - w[10]*w[73];
   w[10]=w[10] + w[69];
   w[10]=2.E+0*w[10];
   w[69]=w[21]*w[70];
   w[58]= - w[69] - w[58];
   w[58]=w[3]*w[58];
   w[74]=w[50]*w[29];
   w[58]= - 2.E+0*w[74] + w[58];
   w[58]=2.E+0*w[58] + w[66];
   w[58]=w[1]*w[58];
   w[26]= - w[26]*w[73];
   w[26]=w[26] + w[58];
   w[58]=w[30]*w[15];
   w[57]= - w[7] - w[57];
   w[57]=w[57]*w[58];
   w[66]= - w[19] + w[8];
   w[66]=w[5]*w[66];
   w[74]=4.E+0*w[8];
   w[20]=w[74] - w[20];
   w[20]=w[2]*w[20];
   w[20]=w[20] + w[24] + w[66];
   w[20]=w[20]*w[25];
   w[20]=w[57] + w[20];
   w[20]=w[3]*w[20];
   w[24]=pow(w[4],3);
   w[53]=w[53]*w[24]*ang[0];
   w[20]=w[53] + w[20];
   w[20]=w[20]*w[35];
   w[35]= - w[56]*w[71];
   w[53]=w[40] - w[6];
   w[53]=w[53]*w[2];
   w[53]=w[53] - w[63] + ang[2];
   w[53]=w[53]*w[51];
   w[57]= - w[3]*w[53];
   w[35]=w[35] + w[57];
   w[35]=w[1]*w[35];
   w[20]=w[20] + w[35];
   w[20]=2.E+0*w[20];
   w[35]=8.E+0*i_;
   w[34]=w[34]*w[35];
   w[57]= - w[13]*w[58];
   w[66]= - w[14] + w[19] + w[40];
   w[66]=w[2]*w[66];
   w[19]=w[19] - w[40];
   w[71]=w[5]*w[19];
   w[66]=w[66] - ang[2] + w[71];
   w[66]=w[66]*w[25];
   w[57]=w[57] + w[66];
   w[57]=w[3]*w[57];
   w[66]=w[24]*w[50];
   w[33]= - w[33]*w[66];
   w[33]=w[33] + w[57];
   w[53]= - w[1]*w[53];
   w[33]=2.E+0*w[33] + w[53];
   w[33]=w[1]*w[33];
   w[53]=w[45]*w[15];
   w[57]= - w[40]*w[25];
   w[53]=w[53] + w[57];
   w[53]=w[3]*w[53];
   w[57]= - w[24]*w[14];
   w[53]=w[57] + w[53];
   w[53]=w[53]*w[73];
   w[33]=w[53] + w[33];
   w[33]=4.E+0*w[33];
   w[41]=w[41] + w[6];
   w[53]=w[41]*w[3];
   w[57]=w[14] + w[9];
   w[71]=2.E+0*w[4];
   w[75]=w[57]*w[71];
   w[53]=w[53] - w[75];
   w[53]=w[53]*w[3];
   w[75]=w[7]*w[15];
   w[53]=w[53] + w[75];
   w[75]=4.E+0*w[3];
   w[53]=w[53]*w[75];
   w[49]=w[49] + w[6];
   w[49]=w[49]*w[2];
   w[76]=3.E+0*w[12];
   w[49]=w[49] + w[76];
   w[77]=w[49]*w[3];
   w[78]=w[37]*w[70];
   w[77]=w[77] + w[78];
   w[77]=w[77]*w[1];
   w[53]=w[53] - w[77];
   w[77]= - 4.E+0*w[53];
   w[78]=w[6] + w[8];
   w[79]=3.E+0*w[2];
   w[78]= - w[78]*w[15]*w[79];
   w[80]=w[55] - w[11];
   w[81]= - w[5]*w[80];
   w[82]= - w[8]*w[79];
   w[81]=w[81] + w[82];
   w[81]=w[3]*w[81];
   w[74]=w[74] + w[46];
   w[74]=w[2]*w[74];
   w[74]=w[48] + w[74];
   w[74]=w[74]*w[71];
   w[74]=w[74] + w[81];
   w[74]=w[3]*w[74];
   w[74]=w[78] + w[74];
   w[74]=w[74]*w[75];
   w[28]= - w[28]*w[79];
   w[28]=w[28] - w[60] + w[65];
   w[28]=w[2]*w[28];
   w[65]=w[76]*w[5];
   w[28]=w[65] + w[28];
   w[28]=w[3]*w[28];
   w[47]=w[47]*w[70];
   w[28]=w[47] + w[28];
   w[28]=w[1]*w[28];
   w[28]=w[74] + w[28];
   w[28]=2.E+0*w[28];
   w[47]=w[30]*w[4];
   w[74]= - w[27]*w[47];
   w[46]=w[46]*w[2];
   w[46]=w[46] - w[48];
   w[78]=w[3]*w[46];
   w[74]=w[74] + w[78];
   w[74]=1.6E+1*i_*w[74];
   w[78]=w[27]*w[58];
   w[46]= - w[46]*w[25];
   w[46]=w[78] + w[46];
   w[46]=8.E+0*w[1]*w[46];
   w[78]=w[15]*w[6];
   w[81]=w[4]*w[14];
   w[82]= - w[3]*w[6];
   w[81]=w[81] + w[82];
   w[81]=w[3]*w[81];
   w[81]= - w[78] + w[81];
   w[82]= - 2.E+0*w[9] + w[6];
   w[82]=w[2]*w[82];
   w[82]=w[82] - w[12];
   w[82]=w[1]*w[82];
   w[81]=4.E+0*w[81] + w[82];
   w[81]=4.E+0*w[81];
   w[82]=w[15]*w[2];
   w[27]=w[27]*w[82];
   w[83]=w[8]*w[2];
   w[84]=w[63] - w[83];
   w[84]=w[84]*w[72];
   w[27]=w[27] + w[84];
   w[84]=3.E+0*ang[0];
   w[62]= - w[62]*w[84];
   w[84]=w[11] - w[6];
   w[85]=w[55] + w[84];
   w[85]=w[2]*w[85];
   w[62]=w[85] - ang[2] + w[62];
   w[62]=w[2]*w[62];
   w[12]= - w[5]*w[12];
   w[12]=w[12] + w[62];
   w[12]=w[1]*w[12];
   w[12]=4.E+0*w[27] + w[12];
   w[12]=2.E+0*w[12];
   w[27]=w[4]*w[13];
   w[62]=w[8]*w[3];
   w[27]=w[27] + w[62];
   w[27]=w[3]*w[27];
   w[27]=w[78] + w[27];
   w[85]=w[56] - w[8];
   w[86]=w[85]*w[5];
   w[60]= - w[68] + w[60] - w[86];
   w[60]=w[1]*w[60];
   w[27]=4.E+0*w[27] + w[60];
   w[27]=1.2E+1*w[27];
   w[51]= - w[11]*w[51];
   w[60]=w[5] + w[2];
   w[60]=ang[1]*w[60];
   w[60]=w[60] - ang[2];
   w[68]=w[3]*w[60];
   w[51]=w[51] + w[68];
   w[51]=6.E+0*w[51];
   w[11]=w[4]*w[72]*w[11];
   w[68]=w[1]*w[3];
   w[87]= - w[60]*w[68];
   w[11]=w[11] + w[87];
   w[11]=6.E+0*w[11];
   w[87]=2.E+0*w[53];
   w[88]=2.E+0*w[5];
   w[22]=w[22]*w[88];
   w[88]=w[13]*w[2];
   w[88]=w[88] - ang[2];
   w[22]=w[22] + w[88];
   w[22]=w[22]*w[3];
   w[89]=w[56] + w[8];
   w[90]= - w[14] + w[89];
   w[90]=w[2]*w[90];
   w[90]=w[90] + w[23];
   w[90]=w[90]*w[71];
   w[90]=w[90] - w[22];
   w[90]=w[3]*w[90];
   w[79]=w[29]*w[79];
   w[90]=w[79] + w[90];
   w[90]=w[90]*w[75];
   w[42]= - w[42]*w[70];
   w[56]= - w[56] + 5.E+0*w[8];
   w[56]=w[56]*w[5];
   w[70]=w[89] + w[6];
   w[70]=w[70]*w[2];
   w[56]= - w[56] + w[70] - 1.1E+1*ang[2];
   w[56]=w[56]*w[2];
   w[64]=3.E+0*w[64];
   w[56]=w[56] - w[64];
   w[70]= - w[3]*w[56];
   w[42]=w[42] + w[70];
   w[42]=w[1]*w[42];
   w[42]=w[90] + w[42];
   w[9]= - w[6] + 3.E+0*w[9];
   w[9]=w[9]*w[4];
   w[14]=w[80] - w[14];
   w[14]=w[14]*w[3];
   w[9]=w[9] - w[14];
   w[9]=w[9]*w[3];
   w[9]=w[9] - w[78];
   w[14]=w[9]*w[75];
   w[70]=w[55] - ang[1];
   w[70]= - w[7] + 2.E+0*w[70];
   w[70]=w[70]*w[2];
   w[70]=w[70] + w[76];
   w[76]=w[70]*w[68];
   w[14]=w[14] + w[76];
   w[14]=w[1]*w[14];
   w[32]=8.E+0*w[32];
   w[76]=pow(w[3],3);
   w[78]= - w[76]*w[32];
   w[14]=w[78] + w[14];
   w[14]=2.E+0*w[14];
   w[78]= - w[6] + 7.E+0*ang[1] - w[40];
   w[78]=w[2]*w[78];
   w[23]=3.E+0*w[23] + w[78];
   w[23]=w[4]*w[23];
   w[22]=w[23] - w[22];
   w[22]=w[3]*w[22];
   w[23]=w[2]*w[29];
   w[22]=w[23] + w[22];
   w[22]=w[22]*w[75];
   w[23]= - w[56]*w[68];
   w[22]=w[22] + w[23];
   w[22]=w[1]*w[22];
   w[23]=8.E+0*w[76];
   w[23]= - w[38]*w[23];
   w[22]=w[23] + w[22];
   w[23]=w[41]*w[25];
   w[29]=w[57]*w[15];
   w[23]= - w[23] + 2.E+0*w[29];
   w[23]=w[23]*w[3];
   w[29]=w[24]*w[7];
   w[23]=w[23] - w[29];
   w[29]=w[23]*w[73];
   w[38]=w[49]*w[25];
   w[41]=4.E+0*w[82];
   w[37]=w[41]*w[37];
   w[37]=w[38] + w[37];
   w[38]=w[37]*w[68];
   w[29]=w[29] + w[38];
   w[29]=2.E+0*w[29];
   w[38]=w[8] + w[43] - w[7];
   w[38]=w[38]*w[2];
   w[38]=w[38] + w[86] - 1.5E+1*ang[2];
   w[38]=w[38]*w[2];
   w[38]=w[38] - w[64];
   w[38]=w[38]*w[3];
   w[38]=w[38] - w[69];
   w[38]=w[38]*w[1];
   w[21]=w[21]*w[71];
   w[43]=w[67]*w[5];
   w[43]=w[43] - w[83];
   w[43]=w[43]*w[3];
   w[21]=w[43] - w[21];
   w[21]=w[21]*w[3];
   w[21]=w[21] - w[79];
   w[21]=w[21]*w[75];
   w[21]=w[38] + w[21];
   w[38]= - w[1]*w[87];
   w[43]= - w[1]*w[21];
   w[49]=w[59] - w[55];
   w[49]=w[5]*w[49];
   w[55]=2.E+0*w[85] - w[7];
   w[55]=w[2]*w[55];
   w[49]=w[55] - w[61] + w[49];
   w[49]=w[49]*w[25];
   w[41]=w[45]*w[41];
   w[41]=w[41] + w[49];
   w[41]=w[3]*w[41];
   w[45]=ang[0]*w[66];
   w[47]= - w[1]*w[60]*w[47];
   w[41]=w[47] - 8.E+0*w[45] + w[41];
   w[41]=w[1]*w[41];
   w[13]= - w[13]*w[15];
   w[25]= - w[8]*w[25];
   w[13]=w[13] + w[25];
   w[13]=w[3]*w[13];
   w[6]= - w[24]*w[6];
   w[6]=w[6] + w[13];
   w[6]=w[6]*w[73];
   w[6]=w[6] + w[41];
   w[6]=2.E+0*w[6];
   w[13]= - w[53]*w[35];
   w[23]= - w[23]*w[75];
   w[24]= - w[1]*w[37];
   w[23]=w[23] + w[24];
   w[23]=4.E+0*w[1]*w[23];
   w[24]= - w[1]*w[70];
   w[9]= - 4.E+0*w[9] + w[24];
   w[9]=w[1]*w[9];
   w[24]=w[72]*w[32];
   w[9]=w[24] + w[9];
   w[9]=4.E+0*w[9];
   w[24]=w[8] + w[7];
   w[24]=w[24]*w[58];
   w[7]= - w[7] + w[19];
   w[7]=w[2]*w[7];
   w[7]= - 3.E+0*w[48] + w[7];
   w[7]=w[7]*w[71];
   w[19]= - w[2]*w[84];
   w[25]= - 6.E+0*ang[1] + 7.E+0*w[8];
   w[25]=w[5]*w[25];
   w[19]=w[19] + ang[2] + w[25];
   w[19]=w[3]*w[19];
   w[7]=w[7] + w[19];
   w[7]=w[3]*w[7];
   w[7]=w[24] + w[7];
   w[19]= - 5.E+0*w[63] - w[88];
   w[19]=w[2]*w[19];
   w[19]= - w[65] + w[19];
   w[19]=w[1]*w[19];
   w[7]=2.E+0*w[7] + w[19];
   w[7]=w[1]*w[7];
   w[8]=w[15]*w[8];
   w[15]= - w[4]*w[40];
   w[15]=w[15] + w[62];
   w[15]=w[3]*w[15];
   w[8]=w[8] + w[15];
   w[15]=8.E+0*w[72];
   w[8]=w[8]*w[15];
   w[7]=w[8] + w[7];
   w[7]=2.E+0*w[7];
   w[8]=w[17] - w[72];
   w[15]= - 4.E+0*w[8];
   w[17]=w[17]*w[3];
   w[17]=w[17] - w[76];
   w[17]=4.E+0*w[17];
   w[19]=1.6E+1*w[8];
   w[24]=w[72]*w[30];
   w[25]=w[50]*w[1];
   w[24]=w[24] - w[25];
   w[24]=w[24]*w[1];
   w[25]=pow(w[3],4);
   w[24]=w[24] - w[25];
   w[25]= - 8.E+0*w[24];
   w[32]=w[68]*w[50];
   w[30]=w[76]*w[30];
   w[30]=w[32] - w[30];
   w[30]=w[30]*w[1];
   w[32]=pow(w[3],5);
   w[30]=w[30] + w[32];
   w[32]=1.6E+1*w[30];
   w[8]= - 4.8E+1*w[8];
   w[30]=8.E+0*w[30];
   w[24]= - 1.6E+1*w[24];


    denom = w[15];

    kern[0][0] = (1.0/ denom)* w[18];
 
    kern[0][1] = (1.0/ denom)* w[31];
 
    kern[0][2] = (1.0/ denom)*0.0;
 
    kern[0][3] = (1.0/ denom)*0.0;
 
    kern[0][4] = (1.0/ denom)*0.0;
 
    kern[0][5] = (1.0/ denom)*0.0;
 
    kern[0][6] = (1.0/ denom)*0.0;
 
    kern[0][7] = (1.0/ denom)*0.0;
 
    kern[0][8] = (1.0/ denom)*0.0;
 
    kern[0][9] = (1.0/ denom)*0.0;
 
    kern[0][10] = (1.0/ denom)*0.0;
 
    kern[0][11] = (1.0/ denom)*0.0;
 

    denom =  - w[17];

    kern[1][0] = (1.0/ denom)* w[36];
 
    kern[1][1] = (1.0/ denom)* w[39];
 
    kern[1][2] = (1.0/ denom)*0.0;
 
    kern[1][3] = (1.0/ denom)*0.0;
 
    kern[1][4] = (1.0/ denom)*0.0;
 
    kern[1][5] = (1.0/ denom)*0.0;
 
    kern[1][6] = (1.0/ denom)*0.0;
 
    kern[1][7] = (1.0/ denom)*0.0;
 
    kern[1][8] = (1.0/ denom)*0.0;
 
    kern[1][9] = (1.0/ denom)*0.0;
 
    kern[1][10] = (1.0/ denom)*0.0;
 
    kern[1][11] = (1.0/ denom)*0.0;
 

    denom = w[75];

    kern[2][0] = (1.0/ denom)*0.0;
 
    kern[2][1] = (1.0/ denom)*0.0;
 
    kern[2][2] = (1.0/ denom)* w[44];
 
    kern[2][3] = (1.0/ denom)*0.0;
 
    kern[2][4] = (1.0/ denom)*0.0;
 
    kern[2][5] = (1.0/ denom)*0.0;
 
    kern[2][6] = (1.0/ denom)*0.0;
 
    kern[2][7] = (1.0/ denom)*0.0;
 
    kern[2][8] = (1.0/ denom)*0.0;
 
    kern[2][9] = (1.0/ denom)*0.0;
 
    kern[2][10] = (1.0/ denom)*0.0;
 
    kern[2][11] = (1.0/ denom)*0.0;
 

    denom =  - w[19];

    kern[3][0] = (1.0/ denom)*0.0;
 
    kern[3][1] = (1.0/ denom)*0.0;
 
    kern[3][2] = (1.0/ denom)*0.0;
 
    kern[3][3] = (1.0/ denom)* w[16];
 
    kern[3][4] = (1.0/ denom)*0.0;
 
    kern[3][5] = (1.0/ denom)*0.0;
 
    kern[3][6] = (1.0/ denom)*0.0;
 
    kern[3][7] = (1.0/ denom)*0.0;
 
    kern[3][8] = (1.0/ denom)*0.0;
 
    kern[3][9] = (1.0/ denom)*0.0;
 
    kern[3][10] = (1.0/ denom)*0.0;
 
    kern[3][11] = (1.0/ denom)*0.0;
 

    denom = w[25];

    kern[4][0] = (1.0/ denom)* w[54];
 
    kern[4][1] = (1.0/ denom)* w[52];
 
    kern[4][2] = (1.0/ denom)*0.0;
 
    kern[4][3] = (1.0/ denom)*0.0;
 
    kern[4][4] = (1.0/ denom)* w[10];
 
    kern[4][5] = (1.0/ denom)*0.0;
 
    kern[4][6] = (1.0/ denom)*0.0;
 
    kern[4][7] = (1.0/ denom)*0.0;
 
    kern[4][8] = (1.0/ denom)*0.0;
 
    kern[4][9] = (1.0/ denom)* w[26];
 
    kern[4][10] = (1.0/ denom)* w[20];
 
    kern[4][11] = (1.0/ denom)*0.0;
 

    denom = w[32];

    kern[5][0] = (1.0/ denom)*0.0;
 
    kern[5][1] = (1.0/ denom)*0.0;
 
    kern[5][2] = (1.0/ denom)*0.0;
 
    kern[5][3] = (1.0/ denom)* w[34];
 
    kern[5][4] = (1.0/ denom)*0.0;
 
    kern[5][5] = (1.0/ denom)* w[33];
 
    kern[5][6] = (1.0/ denom)* w[77];
 
    kern[5][7] = (1.0/ denom)*0.0;
 
    kern[5][8] = (1.0/ denom)*0.0;
 
    kern[5][9] = (1.0/ denom)*0.0;
 
    kern[5][10] = (1.0/ denom)*0.0;
 
    kern[5][11] = (1.0/ denom)* w[28];
 

    denom =  - w[19];

    kern[6][0] = (1.0/ denom)*0.0;
 
    kern[6][1] = (1.0/ denom)*0.0;
 
    kern[6][2] = (1.0/ denom)*0.0;
 
    kern[6][3] = (1.0/ denom)* w[74];
 
    kern[6][4] = (1.0/ denom)*0.0;
 
    kern[6][5] = (1.0/ denom)* w[46];
 
    kern[6][6] = (1.0/ denom)* w[81];
 
    kern[6][7] = (1.0/ denom)*0.0;
 
    kern[6][8] = (1.0/ denom)*0.0;
 
    kern[6][9] = (1.0/ denom)*0.0;
 
    kern[6][10] = (1.0/ denom)*0.0;
 
    kern[6][11] = (1.0/ denom)* w[12];
 

    denom = w[8];

    kern[7][0] = (1.0/ denom)*0.0;
 
    kern[7][1] = (1.0/ denom)*0.0;
 
    kern[7][2] = (1.0/ denom)*0.0;
 
    kern[7][3] = (1.0/ denom)*0.0;
 
    kern[7][4] = (1.0/ denom)*0.0;
 
    kern[7][5] = (1.0/ denom)*0.0;
 
    kern[7][6] = (1.0/ denom)*0.0;
 
    kern[7][7] = (1.0/ denom)* w[27];
 
    kern[7][8] = (1.0/ denom)*0.0;
 
    kern[7][9] = (1.0/ denom)*0.0;
 
    kern[7][10] = (1.0/ denom)*0.0;
 
    kern[7][11] = (1.0/ denom)*0.0;
 

    denom =  - w[17];

    kern[8][0] = (1.0/ denom)*0.0;
 
    kern[8][1] = (1.0/ denom)*0.0;
 
    kern[8][2] = (1.0/ denom)* w[51];
 
    kern[8][3] = (1.0/ denom)*0.0;
 
    kern[8][4] = (1.0/ denom)*0.0;
 
    kern[8][5] = (1.0/ denom)*0.0;
 
    kern[8][6] = (1.0/ denom)*0.0;
 
    kern[8][7] = (1.0/ denom)*0.0;
 
    kern[8][8] = (1.0/ denom)* w[11];
 
    kern[8][9] = (1.0/ denom)*0.0;
 
    kern[8][10] = (1.0/ denom)*0.0;
 
    kern[8][11] = (1.0/ denom)*0.0;
 

    denom = w[30];

    kern[9][0] = (1.0/ denom)*  - w[87];
 
    kern[9][1] = (1.0/ denom)* w[42];
 
    kern[9][2] = (1.0/ denom)*0.0;
 
    kern[9][3] = (1.0/ denom)*0.0;
 
    kern[9][4] = (1.0/ denom)* w[14];
 
    kern[9][5] = (1.0/ denom)*0.0;
 
    kern[9][6] = (1.0/ denom)*0.0;
 
    kern[9][7] = (1.0/ denom)*0.0;
 
    kern[9][8] = (1.0/ denom)*0.0;
 
    kern[9][9] = (1.0/ denom)* w[22];
 
    kern[9][10] = (1.0/ denom)* w[29];
 
    kern[9][11] = (1.0/ denom)*0.0;
 

    denom = w[30];

    kern[10][0] = (1.0/ denom)*  - w[87];
 
    kern[10][1] = (1.0/ denom)*  - w[21];
 
    kern[10][2] = (1.0/ denom)*0.0;
 
    kern[10][3] = (1.0/ denom)*0.0;
 
    kern[10][4] = (1.0/ denom)* w[38];
 
    kern[10][5] = (1.0/ denom)*0.0;
 
    kern[10][6] = (1.0/ denom)*0.0;
 
    kern[10][7] = (1.0/ denom)*0.0;
 
    kern[10][8] = (1.0/ denom)*0.0;
 
    kern[10][9] = (1.0/ denom)* w[43];
 
    kern[10][10] = (1.0/ denom)* w[6];
 
    kern[10][11] = (1.0/ denom)*0.0;
 

    denom = w[24];

    kern[11][0] = (1.0/ denom)*0.0;
 
    kern[11][1] = (1.0/ denom)*0.0;
 
    kern[11][2] = (1.0/ denom)*0.0;
 
    kern[11][3] = (1.0/ denom)* w[13];
 
    kern[11][4] = (1.0/ denom)*0.0;
 
    kern[11][5] = (1.0/ denom)* w[23];
 
    kern[11][6] = (1.0/ denom)* w[9];
 
    kern[11][7] = (1.0/ denom)*0.0;
 
    kern[11][8] = (1.0/ denom)*0.0;
 
    kern[11][9] = (1.0/ denom)*0.0;
 
    kern[11][10] = (1.0/ denom)*0.0;
 
    kern[11][11] = (1.0/ denom)* w[7];
 

}


void kernelY_qphv(matCdoub& kern,
  const ArrayScalarProducts& sp,  const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


    // local variables
    array<Cdoub, 123> w;
    Cdoub denom;


    w[1]=sp[35];
    w[2]=sp[44];
    w[3]=sp[7];
    w[4]=sp[43];
    w[5]=sp[31];
    w[6]=sp[37];
    w[7]=sp[29];
    w[8]=sp[39];
   w[9]=ssp*ssm;
   w[10]=svp*svm;
   w[11]=w[10]*w[3];
   w[12]=w[9] + w[11];
   w[13]=w[12]*w[4];
   w[14]=w[10]*w[6];
   w[15]=w[14]*w[5];
   w[10]=w[10]*w[7];
   w[16]=w[8]*w[10];
   w[17]=w[15] + w[16];
   w[13]=w[13] - w[17];
   w[13]=w[13]*w[4];
   w[10]=w[5]*w[10];
   w[18]=w[10]*w[2];
   w[19]=2.E+0*w[18];
   w[20]=w[13] + w[19];
   w[21]=w[1]*w[2];
   w[22]= - w[12]*w[21];
   w[22]=w[22] + w[20];
   w[22]=4.E+0*w[22];
   w[23]=w[17]*w[2];
   w[14]=w[14]*w[8];
   w[24]=2.E+0*w[14];
   w[25]=w[24]*w[4];
   w[26]=w[25] - w[23];
   w[27]=4.E+0*w[4];
   w[28]= - w[26]*w[27];
   w[29]=svp*ssm;
   w[30]=w[7]*w[29];
   w[31]=ssp*svm;
   w[32]=w[5]*w[31];
   w[33]=w[30] + w[32];
   w[34]=w[33]*w[2];
   w[29]=w[6]*w[29];
   w[31]=w[8]*w[31];
   w[35]=w[29] + w[31];
   w[36]=w[35]*w[4];
   w[34]=w[34] - w[36];
   w[37]=w[34]*w[27];
   w[29]=w[29] - w[31];
   w[31]=pow(w[4],2);
   w[38]=w[29]*w[31];
   w[39]=w[29]*w[21];
   w[40]= - w[39] + w[38];
   w[41]=8.E+0*i_;
   w[40]=w[40]*w[41];
   w[42]=w[17]*w[1];
   w[43]=2.E+0*w[10];
   w[44]= - w[4]*w[43];
   w[44]=w[42] + w[44];
   w[44]=4.E+0*w[44];
   w[45]=w[12]*w[2];
   w[46]=w[45] - w[24];
   w[47]= - w[1]*w[46];
   w[47]=w[47] + w[13];
   w[47]=w[47]*w[27];
   w[48]=w[35]*w[1];
   w[49]=w[33]*w[4];
   w[48]=w[48] - w[49];
   w[49]=w[48]*w[27];
   w[30]=w[30] - w[32];
   w[32]=w[30]*w[21];
   w[50]= - w[30]*w[31];
   w[50]=w[32] + w[50];
   w[50]=w[50]*w[41];
   w[51]=4.E+0*w[33];
   w[52]=w[35]*w[27];
   w[53]=w[9] - w[11];
   w[54]=w[53]*w[27];
   w[15]=w[15] - w[16];
   w[16]=w[41]*w[15];
   w[55]=w[29]*w[1];
   w[56]=w[30]*w[4];
   w[55]=w[55] - w[56];
   w[41]= - w[55]*w[41];
   w[56]=w[30]*w[2];
   w[57]=w[29]*w[4];
   w[56]=w[56] - w[57];
   w[58]=8.E+0*w[4];
   w[59]=w[58]*i_;
   w[60]=w[56]*w[59];
   w[59]=w[15]*w[59];
   w[61]=w[53]*w[4];
   w[62]=2.E+0*w[17];
   w[61]=w[61] + w[62];
   w[61]=w[61]*w[4];
   w[19]=w[61] - w[19];
   w[61]=w[53]*w[2];
   w[61]=w[61] + w[24];
   w[63]= - w[1]*w[61];
   w[63]=w[63] + w[19];
   w[63]=1.6E+1*w[63];
   w[64]=w[11]*w[2];
   w[65]=w[64] + w[24];
   w[65]=w[65]*w[4];
   w[65]=w[65] - 3.E+0*w[23];
   w[65]=w[65]*w[4];
   w[66]=w[64] - w[14];
   w[66]=w[66]*w[21];
   w[67]=pow(w[2],2);
   w[10]=w[67]*w[10];
   w[65]=w[65] - w[66] + 3.E+0*w[10];
   w[66]=8.E+0*w[65];
   w[68]=w[9]*w[2];
   w[69]=w[68] + w[14];
   w[69]=w[69]*w[21];
   w[70]=w[10] + w[69];
   w[70]=w[1]*w[70];
   w[71]=2.E+0*w[9];
   w[72]=w[71] + w[11];
   w[72]=w[72]*w[2];
   w[73]=w[24] - w[72];
   w[73]=w[1]*w[73];
   w[20]=w[73] + w[20];
   w[20]=w[4]*w[20];
   w[73]=w[17]*w[21];
   w[74]=2.E+0*w[73];
   w[20]= - w[74] + w[20];
   w[20]=w[4]*w[20];
   w[20]=w[70] + w[20];
   w[20]=8.E+0*w[20];
   w[70]=2.E+0*w[21];
   w[75]=w[70]*w[29];
   w[38]=w[75] - w[38];
   w[38]=w[38]*w[31];
   w[75]=pow(w[21],2);
   w[76]=w[75]*w[29];
   w[38]=w[38] - w[76];
   w[76]=w[38]*w[58];
   w[77]=w[56]*w[4];
   w[39]=w[77] + w[39];
   w[39]=w[39]*w[4];
   w[67]=w[67]*w[1];
   w[77]=w[67]*w[30];
   w[39]=w[39] - w[77];
   w[77]=8.E+0*w[39];
   w[78]=w[15]*w[67];
   w[79]=w[15]*w[31];
   w[80]= - w[2]*w[79];
   w[78]=w[78] + w[80];
   w[78]=8.E+0*w[78];
   w[80]=w[34]*w[4];
   w[81]=w[35]*w[21];
   w[82]=w[80] + w[81];
   w[82]=w[82]*w[4];
   w[83]=w[67]*w[33];
   w[82]=w[82] - w[83];
   w[83]= - w[82]*w[58];
   w[84]=w[24]*w[21];
   w[85]=w[26]*w[4];
   w[84]=w[84] - w[85];
   w[84]=w[84]*w[4];
   w[85]=w[67]*w[17];
   w[84]=w[84] - w[85];
   w[85]=w[84]*w[58];
   w[86]=8.E+0*w[31];
   w[87]=w[65]*w[86];
   w[88]=3.2E+1*i_;
   w[89]=w[65]*w[88];
   w[55]=w[55]*w[4];
   w[55]=w[55] + w[32];
   w[55]=w[55]*w[4];
   w[90]=pow(w[1],2);
   w[91]=w[90]*w[2];
   w[29]=w[91]*w[29];
   w[55]=w[55] - w[29];
   w[92]= - 1.6E+1*w[55];
   w[69]= - w[10] + w[69];
   w[69]=w[1]*w[69];
   w[71]=w[71] - w[11];
   w[71]=w[71]*w[2];
   w[93]= - 4.E+0*w[14] - w[71];
   w[93]=w[1]*w[93];
   w[19]=w[93] + w[19];
   w[19]=w[4]*w[19];
   w[19]=w[73] + w[19];
   w[19]=w[4]*w[19];
   w[19]=w[69] + w[19];
   w[69]=1.6E+1*w[4];
   w[19]=w[19]*w[69];
   w[93]=w[11]*w[4];
   w[62]=w[93] - w[62];
   w[62]=w[62]*w[4];
   w[93]=3.E+0*w[14];
   w[64]=w[93] - w[64];
   w[64]=w[64]*w[1];
   w[62]=w[62] + w[64] + 3.E+0*w[18];
   w[94]=w[62]*w[4];
   w[94]=w[94] - w[73];
   w[95]= - 1.6E+1*w[94];
   w[96]=1.6E+1*w[82];
   w[97]=w[15]*w[21];
   w[97]=w[97] - w[79];
   w[98]=w[97]*w[69];
   w[99]=w[39]*w[69];
   w[65]= - w[65]*w[69];
   w[56]=1.6E+1*w[56];
   w[100]= - w[26]*w[88];
   w[57]=w[57]*w[1];
   w[32]=w[57] - w[32];
   w[57]=1.6E+1*w[32];
   w[25]=w[25]*w[1];
   w[25]=w[25] - w[73];
   w[101]=w[25]*w[69];
   w[68]=w[68] - w[14];
   w[68]=w[68]*w[1];
   w[102]=w[9]*w[31];
   w[103]=w[102] - w[18] - w[68];
   w[103]=1.6E+1*w[103];
   w[104]=w[35]*w[31];
   w[81]=w[104] - w[81];
   w[104]=1.6E+1*w[81];
   w[105]= - w[31]*w[56];
   w[26]=w[26]*w[69];
   w[106]=4.8E+1*w[15];
   w[107]=9.6E+1*i_*w[34];
   w[108]= - w[1]*w[106];
   w[36]=w[36]*w[1];
   w[109]=w[33]*w[21];
   w[36]=w[36] - w[109];
   w[36]=w[36]*w[4];
   w[110]=4.8E+1*w[36];
   w[111]=4.8E+1*w[48];
   w[9]=w[9]*w[4];
   w[112]=w[9] - w[17];
   w[112]=w[4]*w[112];
   w[68]=w[112] + w[18] - w[68];
   w[68]=4.8E+1*w[68];
   w[112]= - 4.8E+1*w[79];
   w[80]= - 4.8E+1*w[80];
   w[34]=4.E+0*w[34];
   w[113]= - w[2]*w[16];
   w[114]=4.E+0*w[15]*pow(w[4],3);
   w[115]=w[27]*w[15];
   w[116]=w[31] - w[21];
   w[117]=w[116]*w[27];
   w[53]=w[53]*w[117];
   w[81]= - w[81]*w[27];
   w[118]= - w[31]*w[34];
   w[119]=w[2]*w[115];
   w[120]=8.E+0*w[94];
   w[121]=1.6E+1*w[39];
   w[122]= - i_*w[121];
   w[11]=w[11]*w[1];
   w[11]=w[11] + w[43];
   w[11]=w[11]*w[4];
   w[11]=w[11] - 3.E+0*w[42];
   w[11]=w[11]*w[4];
   w[42]=w[64] + w[18];
   w[42]=w[42]*w[1];
   w[11]=w[11] + w[42];
   w[42]= - w[11]*w[58];
   w[43]= - w[55]*w[86];
   w[64]=w[97]*w[58];
   w[48]=w[48]*w[4];
   w[48]=w[48] + w[109];
   w[48]=w[48]*w[4];
   w[35]=w[91]*w[35];
   w[48]=w[48] - w[35];
   w[109]= - w[48]*w[58];
   w[45]=w[14] - w[45];
   w[45]=w[1]*w[45];
   w[13]=2.E+0*w[45] + w[13];
   w[13]=w[4]*w[13];
   w[13]=w[73] + w[13];
   w[13]=w[4]*w[13];
   w[45]=w[46]*w[91];
   w[13]=w[45] + w[13];
   w[13]=w[13]*w[58];
   w[45]= - w[94]*w[86];
   w[39]=w[39]*w[58];
   w[46]= - 8.E+0*w[84];
   w[82]= - 8.E+0*w[82];
   w[62]=w[4]*w[1]*w[62];
   w[23]=w[23]*w[90];
   w[23]=w[62] - w[23];
   w[62]= - 8.E+0*w[23];
   w[84]=8.E+0*w[55];
   w[35]=w[35] - w[36];
   w[35]=w[4]*w[35];
   w[33]= - w[33]*w[75];
   w[33]=w[33] + w[35];
   w[33]=8.E+0*w[33];
   w[24]= - w[24]*w[91];
   w[25]=w[4]*w[25];
   w[24]=w[24] + w[25];
   w[24]=w[4]*w[24];
   w[25]=w[17]*w[75];
   w[24]=w[25] + w[24];
   w[24]=8.E+0*w[24];
   w[10]=2.E+0*w[10];
   w[12]=w[12]*w[67];
   w[12]= - w[10] + w[12];
   w[12]=w[1]*w[12];
   w[25]= - w[93] - w[72];
   w[25]=w[1]*w[25];
   w[9]=w[9] + w[17];
   w[9]=w[4]*w[9];
   w[9]=w[9] - w[18] + w[25];
   w[9]=w[4]*w[9];
   w[9]=w[74] + w[9];
   w[9]=w[4]*w[9];
   w[9]=w[12] + w[9];
   w[9]=w[9]*w[58];
   w[12]=8.E+0*w[38];
   w[17]=1.6E+1*w[97];
   w[25]= - w[94]*w[88];
   w[23]=w[23]*w[69];
   w[11]=1.6E+1*w[11];
   w[35]=1.6E+1*w[48];
   w[15]= - w[15]*w[91];
   w[36]=w[1]*w[79];
   w[15]=w[15] + w[36];
   w[15]=1.6E+1*w[15];
   w[32]=w[4]*w[32];
   w[29]= - w[29] + w[32];
   w[29]=w[4]*w[29];
   w[30]=w[30]*w[75];
   w[29]=w[30] + w[29];
   w[29]=1.6E+1*w[29];
   w[30]= - w[55]*w[69];
   w[21]=w[61]*w[21];
   w[10]=w[10] + w[21];
   w[10]=w[1]*w[10];
   w[14]=w[14] - w[71];
   w[14]=w[1]*w[14];
   w[14]=w[102] + w[18] + w[14];
   w[14]=w[4]*w[14];
   w[14]= - 3.E+0*w[73] + w[14];
   w[14]=w[4]*w[14];
   w[10]=w[10] + w[14];
   w[10]=1.6E+1*w[10];
   w[14]=4.E+0*w[116];
   w[18]=1.6E+1*w[116];
   w[21]=w[70] - w[31];
   w[21]=w[21]*w[31];
   w[21]=w[21] - w[75];
   w[31]= - 8.E+0*w[21];
   w[32]= - w[21]*w[69];
   w[36]=4.8E+1*w[116];
   w[38]=w[21]*w[58];
   w[21]= - 1.6E+1*w[21];


    denom = w[14];

    kern[0][0] = (1.0/ denom)* w[22];
 
    kern[0][1] = (1.0/ denom)* w[28];
 
    kern[0][2] = (1.0/ denom)* w[37];
 
    kern[0][3] = (1.0/ denom)* w[40];
 
    kern[0][4] = (1.0/ denom)*0.0;
 
    kern[0][5] = (1.0/ denom)*0.0;
 
    kern[0][6] = (1.0/ denom)*0.0;
 
    kern[0][7] = (1.0/ denom)*0.0;
 
    kern[0][8] = (1.0/ denom)*0.0;
 
    kern[0][9] = (1.0/ denom)*0.0;
 
    kern[0][10] = (1.0/ denom)*0.0;
 
    kern[0][11] = (1.0/ denom)*0.0;
 

    denom = w[117];

    kern[1][0] = (1.0/ denom)* w[44];
 
    kern[1][1] = (1.0/ denom)* w[47];
 
    kern[1][2] = (1.0/ denom)* w[49];
 
    kern[1][3] = (1.0/ denom)* w[50];
 
    kern[1][4] = (1.0/ denom)*0.0;
 
    kern[1][5] = (1.0/ denom)*0.0;
 
    kern[1][6] = (1.0/ denom)*0.0;
 
    kern[1][7] = (1.0/ denom)*0.0;
 
    kern[1][8] = (1.0/ denom)*0.0;
 
    kern[1][9] = (1.0/ denom)*0.0;
 
    kern[1][10] = (1.0/ denom)*0.0;
 
    kern[1][11] = (1.0/ denom)*0.0;
 

    denom = w[27];

    kern[2][0] = (1.0/ denom)* w[51];
 
    kern[2][1] = (1.0/ denom)* w[52];
 
    kern[2][2] = (1.0/ denom)* w[54];
 
    kern[2][3] = (1.0/ denom)* w[16];
 
    kern[2][4] = (1.0/ denom)*0.0;
 
    kern[2][5] = (1.0/ denom)*0.0;
 
    kern[2][6] = (1.0/ denom)*0.0;
 
    kern[2][7] = (1.0/ denom)*0.0;
 
    kern[2][8] = (1.0/ denom)*0.0;
 
    kern[2][9] = (1.0/ denom)*0.0;
 
    kern[2][10] = (1.0/ denom)*0.0;
 
    kern[2][11] = (1.0/ denom)*0.0;
 

    denom = w[18];

    kern[3][0] = (1.0/ denom)* w[41];
 
    kern[3][1] = (1.0/ denom)* w[60];
 
    kern[3][2] = (1.0/ denom)* w[59];
 
    kern[3][3] = (1.0/ denom)* w[63];
 
    kern[3][4] = (1.0/ denom)*0.0;
 
    kern[3][5] = (1.0/ denom)*0.0;
 
    kern[3][6] = (1.0/ denom)*0.0;
 
    kern[3][7] = (1.0/ denom)*0.0;
 
    kern[3][8] = (1.0/ denom)*0.0;
 
    kern[3][9] = (1.0/ denom)*0.0;
 
    kern[3][10] = (1.0/ denom)*0.0;
 
    kern[3][11] = (1.0/ denom)*0.0;
 

    denom = w[31];

    kern[4][0] = (1.0/ denom)* w[66];
 
    kern[4][1] = (1.0/ denom)*0.0;
 
    kern[4][2] = (1.0/ denom)*0.0;
 
    kern[4][3] = (1.0/ denom)*0.0;
 
    kern[4][4] = (1.0/ denom)* w[20];
 
    kern[4][5] = (1.0/ denom)* w[76];
 
    kern[4][6] = (1.0/ denom)* w[77];
 
    kern[4][7] = (1.0/ denom)* w[78];
 
    kern[4][8] = (1.0/ denom)* w[83];
 
    kern[4][9] = (1.0/ denom)* w[85];
 
    kern[4][10] = (1.0/ denom)* w[87];
 
    kern[4][11] = (1.0/ denom)*0.0;
 

    denom = w[32];

    kern[5][0] = (1.0/ denom)*0.0;
 
    kern[5][1] = (1.0/ denom)*0.0;
 
    kern[5][2] = (1.0/ denom)*0.0;
 
    kern[5][3] = (1.0/ denom)* w[89];
 
    kern[5][4] = (1.0/ denom)* w[92];
 
    kern[5][5] = (1.0/ denom)* w[19];
 
    kern[5][6] = (1.0/ denom)* w[95];
 
    kern[5][7] = (1.0/ denom)* w[96];
 
    kern[5][8] = (1.0/ denom)* w[98];
 
    kern[5][9] = (1.0/ denom)* w[99];
 
    kern[5][10] = (1.0/ denom)*0.0;
 
    kern[5][11] = (1.0/ denom)* w[65];
 

    denom = w[18];

    kern[6][0] = (1.0/ denom)*  - w[56];
 
    kern[6][1] = (1.0/ denom)*0.0;
 
    kern[6][2] = (1.0/ denom)*0.0;
 
    kern[6][3] = (1.0/ denom)* w[100];
 
    kern[6][4] = (1.0/ denom)* w[57];
 
    kern[6][5] = (1.0/ denom)* w[101];
 
    kern[6][6] = (1.0/ denom)* w[103];
 
    kern[6][7] = (1.0/ denom)* w[104];
 
    kern[6][8] = (1.0/ denom)*0.0;
 
    kern[6][9] = (1.0/ denom)*0.0;
 
    kern[6][10] = (1.0/ denom)* w[105];
 
    kern[6][11] = (1.0/ denom)* w[26];
 

    denom = w[36];

    kern[7][0] = (1.0/ denom)*  - w[106];
 
    kern[7][1] = (1.0/ denom)*0.0;
 
    kern[7][2] = (1.0/ denom)*0.0;
 
    kern[7][3] = (1.0/ denom)* w[107];
 
    kern[7][4] = (1.0/ denom)* w[108];
 
    kern[7][5] = (1.0/ denom)* w[110];
 
    kern[7][6] = (1.0/ denom)* w[111];
 
    kern[7][7] = (1.0/ denom)* w[68];
 
    kern[7][8] = (1.0/ denom)*0.0;
 
    kern[7][9] = (1.0/ denom)*0.0;
 
    kern[7][10] = (1.0/ denom)* w[112];
 
    kern[7][11] = (1.0/ denom)* w[80];
 

    denom = w[117];

    kern[8][0] = (1.0/ denom)*  - w[34];
 
    kern[8][1] = (1.0/ denom)*0.0;
 
    kern[8][2] = (1.0/ denom)*0.0;
 
    kern[8][3] = (1.0/ denom)* w[113];
 
    kern[8][4] = (1.0/ denom)* w[49];
 
    kern[8][5] = (1.0/ denom)* w[114];
 
    kern[8][6] = (1.0/ denom)* w[115];
 
    kern[8][7] = (1.0/ denom)*0.0;
 
    kern[8][8] = (1.0/ denom)* w[53];
 
    kern[8][9] = (1.0/ denom)* w[81];
 
    kern[8][10] = (1.0/ denom)* w[118];
 
    kern[8][11] = (1.0/ denom)* w[119];
 

    denom =  - w[38];

    kern[9][0] = (1.0/ denom)*  - w[120];
 
    kern[9][1] = (1.0/ denom)*0.0;
 
    kern[9][2] = (1.0/ denom)*0.0;
 
    kern[9][3] = (1.0/ denom)* w[122];
 
    kern[9][4] = (1.0/ denom)* w[42];
 
    kern[9][5] = (1.0/ denom)* w[43];
 
    kern[9][6] = (1.0/ denom)*0.0;
 
    kern[9][7] = (1.0/ denom)*  - w[64];
 
    kern[9][8] = (1.0/ denom)* w[109];
 
    kern[9][9] = (1.0/ denom)* w[13];
 
    kern[9][10] = (1.0/ denom)* w[45];
 
    kern[9][11] = (1.0/ denom)* w[39];
 

    denom =  - w[38];

    kern[10][0] = (1.0/ denom)*  - w[120];
 
    kern[10][1] = (1.0/ denom)* w[46];
 
    kern[10][2] = (1.0/ denom)* w[82];
 
    kern[10][3] = (1.0/ denom)*0.0;
 
    kern[10][4] = (1.0/ denom)* w[62];
 
    kern[10][5] = (1.0/ denom)*0.0;
 
    kern[10][6] = (1.0/ denom)* w[84];
 
    kern[10][7] = (1.0/ denom)*  - w[64];
 
    kern[10][8] = (1.0/ denom)* w[33];
 
    kern[10][9] = (1.0/ denom)* w[24];
 
    kern[10][10] = (1.0/ denom)* w[9];
 
    kern[10][11] = (1.0/ denom)* w[12];
 

    denom = w[21];

    kern[11][0] = (1.0/ denom)*0.0;
 
    kern[11][1] = (1.0/ denom)*  - w[121];
 
    kern[11][2] = (1.0/ denom)* w[17];
 
    kern[11][3] = (1.0/ denom)* w[25];
 
    kern[11][4] = (1.0/ denom)*0.0;
 
    kern[11][5] = (1.0/ denom)* w[23];
 
    kern[11][6] = (1.0/ denom)* w[11];
 
    kern[11][7] = (1.0/ denom)* w[35];
 
    kern[11][8] = (1.0/ denom)* w[15];
 
    kern[11][9] = (1.0/ denom)* w[29];
 
    kern[11][10] = (1.0/ denom)* w[30];
 
    kern[11][11] = (1.0/ denom)* w[10];
 


}

