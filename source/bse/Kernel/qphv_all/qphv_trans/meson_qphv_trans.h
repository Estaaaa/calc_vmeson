#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
//#include <math_ops.h>

void kernelL_notopti_qphv_trans(matCdoub& kern,
                              const ArrayScalarProducts& sp,
                const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){


    // local variables
    Cdoub denom=0.0;


    denom = 8.E+0*sp[35]*pow(sp[34],4) - 1.6E+1*pow(sp[35],2)*pow(sp[34],2)*sp[27] + 
      8.E+0*pow(sp[35],3)*pow(sp[27],2);

    kern[0][0] =  1.6E+1*sp[35]*pow(sp[34],2)*pow(sp[43],2)*sp[27]*ang[0] - 2.4E+1*
      sp[35]*pow(sp[34],3)*sp[43]*sp[27]*ang[0] - 8.E+0*sp[35]*pow(sp[34],3)*sp[43]*sp[44]*ang[0]
       + 8.E+0*sp[35]*pow(sp[34],3)*sp[43]*ang[1] + 8.E+0*sp[35]*pow(sp[34],4)*sp[27]*ang[0]
       + 8.E+0*sp[35]*pow(sp[34],4)*sp[44]*ang[0] - 1.6E+1*sp[35]*pow(sp[34],4)*ang[1] - 
      1.6E+1*pow(sp[35],2)*sp[34]*sp[43]*sp[27]*sp[44]*ang[0] + 1.6E+1*pow(sp[35],2)*sp[34]*
      sp[43]*sp[27]*ang[1] + 8.E+0*pow(sp[35],2)*pow(sp[34],2)*sp[27]*sp[44]*ang[0] + 1.6E+1*
      pow(sp[35],2)*pow(sp[34],2)*sp[27]*ang[1] - 4.E+0*pow(sp[35],2)*pow(sp[34],2)*pow(
      sp[27],2)*ang[0] - 8.E+0*pow(sp[35],2)*pow(sp[34],2)*sp[44]*ang[1] + 4.E+0*pow(
      sp[35],2)*pow(sp[34],2)*pow(sp[44],2)*ang[0] + 4.E+0*pow(sp[35],2)*pow(sp[34],2)*
      ang[2] + 8.E+0*pow(sp[35],2)*pow(sp[43],2)*pow(sp[27],2)*ang[0] - 4.E+0*pow(
      sp[35],3)*sp[27]*sp[44]*ang[1] + 2.E+0*pow(sp[35],3)*sp[27]*pow(sp[44],2)*ang[0] + 
      2.E+0*pow(sp[35],3)*sp[27]*ang[2] - 4.E+0*pow(sp[35],3)*pow(sp[27],2)*sp[44]*ang[0]
       - 1.2E+1*pow(sp[35],3)*pow(sp[27],2)*ang[1] + 2.E+0*pow(sp[35],3)*pow(
      sp[27],3)*ang[0];
 
    kern[0][0] = (1.0/ denom)* kern[0][0]; 
    kern[0][1] =   0;
 
    kern[0][1] = (1.0/ denom)* kern[0][1]; 
    kern[0][2] =   0;
 
    kern[0][2] = (1.0/ denom)* kern[0][2]; 
    kern[0][3] =   0;
 
    kern[0][3] = (1.0/ denom)* kern[0][3]; 
    kern[0][4] =   0;
 
    kern[0][4] = (1.0/ denom)* kern[0][4]; 
    kern[0][5] =  8.E+0*sp[35]*pow(sp[34],2)*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 1.6E+1
      *sp[35]*pow(sp[34],2)*pow(sp[43],2)*sp[27]*ang[1] - 8.E+0*sp[35]*pow(sp[34],2)*pow(
      sp[43],2)*pow(sp[27],2)*ang[0] - 1.2E+1*sp[35]*pow(sp[34],3)*sp[43]*sp[27]*ang[1] + 
      4.E+0*sp[35]*pow(sp[34],3)*sp[43]*pow(sp[27],2)*ang[0] - 4.E+0*sp[35]*pow(sp[34],3)*
      sp[43]*sp[44]*ang[1] - 4.E+0*sp[35]*pow(sp[34],3)*sp[43]*pow(sp[44],2)*ang[0] + 8.E+0*
      sp[35]*pow(sp[34],3)*sp[43]*ang[2] - 8.E+0*pow(sp[35],2)*sp[34]*sp[43]*sp[27]*sp[44]*ang[1]
       - 8.E+0*pow(sp[35],2)*sp[34]*sp[43]*sp[27]*pow(sp[44],2)*ang[0] + 1.6E+1*pow(
      sp[35],2)*sp[34]*sp[43]*sp[27]*ang[2] - 2.4E+1*pow(sp[35],2)*sp[34]*sp[43]*pow(sp[27],2)*
      ang[1] + 8.E+0*pow(sp[35],2)*sp[34]*sp[43]*pow(sp[27],3)*ang[0] + 1.6E+1*pow(
      sp[35],2)*pow(sp[34],2)*sp[27]*sp[44]*ang[1] + 6.E+0*pow(sp[35],2)*pow(sp[34],2)*sp[27]*
      pow(sp[44],2)*ang[0] - 1.E+1*pow(sp[35],2)*pow(sp[34],2)*sp[27]*ang[2] - 6.E+0*
      pow(sp[35],2)*pow(sp[34],2)*pow(sp[27],2)*sp[44]*ang[0] + 8.E+0*pow(sp[35],2)*pow(
      sp[34],2)*pow(sp[27],2)*ang[1] - 2.E+0*pow(sp[35],2)*pow(sp[34],2)*pow(sp[27],3)*
      ang[0] - 6.E+0*pow(sp[35],2)*pow(sp[34],2)*sp[44]*ang[2] + 2.E+0*pow(sp[35],2)*
      pow(sp[34],2)*pow(sp[44],3)*ang[0] + 4.E+0*pow(sp[35],2)*pow(sp[34],2)*ang[3] + 
      4.E+0*pow(sp[35],2)*pow(sp[43],2)*pow(sp[27],2)*sp[44]*ang[0] + 8.E+0*pow(
      sp[35],2)*pow(sp[43],2)*pow(sp[27],2)*ang[1] - 4.E+0*pow(sp[35],2)*pow(sp[43],2)*
      pow(sp[27],3)*ang[0] - 3.E+0*pow(sp[35],3)*sp[27]*sp[44]*ang[2] + pow(sp[35],3)*sp[27]*
      pow(sp[44],3)*ang[0] + 2.E+0*pow(sp[35],3)*sp[27]*ang[3] - 4.E+0*pow(sp[35],3)*
      pow(sp[27],2)*sp[44]*ang[1] - 3.E+0*pow(sp[35],3)*pow(sp[27],2)*pow(sp[44],2)*ang[0]
       - 5.E+0*pow(sp[35],3)*pow(sp[27],2)*ang[2] + 3.E+0*pow(sp[35],3)*pow(sp[27],3)
      *sp[44]*ang[0] + 4.E+0*pow(sp[35],3)*pow(sp[27],3)*ang[1] - pow(sp[35],3)*pow(
      sp[27],4)*ang[0];
 
    kern[0][5] = (1.0/ denom)* kern[0][5]; 
    kern[0][6] =  2.4E+1*sp[35]*sp[34]*pow(sp[43],3)*pow(sp[27],2)*ang[0] - 2.4E+1*
      sp[35]*pow(sp[34],2)*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 3.2E+1*sp[35]*pow(sp[34],2)*
      pow(sp[43],2)*sp[27]*ang[1] - 2.4E+1*sp[35]*pow(sp[34],2)*pow(sp[43],2)*pow(sp[27],2)
      *ang[0] + 1.6E+1*sp[35]*pow(sp[34],3)*sp[43]*sp[27]*sp[44]*ang[0] - 1.2E+1*sp[35]*pow(
      sp[34],3)*sp[43]*sp[27]*ang[1] + 4.E+0*sp[35]*pow(sp[34],3)*sp[43]*pow(sp[27],2)*ang[0] - 
      1.2E+1*sp[35]*pow(sp[34],3)*sp[43]*sp[44]*ang[1] + 4.E+0*sp[35]*pow(sp[34],3)*sp[43]*
      pow(sp[44],2)*ang[0] + 8.E+0*sp[35]*pow(sp[34],3)*sp[43]*ang[2] + 2.E+0*pow(
      sp[35],2)*sp[34]*sp[43]*sp[27]*pow(sp[44],2)*ang[0] - 2.E+0*pow(sp[35],2)*sp[34]*sp[43]*sp[27]
      *ang[2] - 4.E+0*pow(sp[35],2)*sp[34]*sp[43]*pow(sp[27],2)*sp[44]*ang[0] + 2.E+0*pow(
      sp[35],2)*sp[34]*sp[43]*pow(sp[27],3)*ang[0] - 8.E+0*pow(sp[35],2)*pow(sp[43],2)*pow(
      sp[27],2)*ang[1];
 
    kern[0][6] = (1.0/ denom)* kern[0][6]; 
    kern[0][7] =   0;
 
    kern[0][7] = (1.0/ denom)* kern[0][7]; 

    denom = 1.6E+1*sp[35]*pow(sp[34],5) - 3.2E+1*pow(sp[35],2)*pow(sp[34],3)*sp[27] + 
      1.6E+1*pow(sp[35],3)*sp[34]*pow(sp[27],2);

    kern[1][0] =   0;
 
    kern[1][0] = (1.0/ denom)* kern[1][0]; 
    kern[1][1] =   - 3.2E+1*sp[35]*pow(sp[34],2)*pow(sp[43],3)*sp[27]*ang[0] + 3.2E+1
      *sp[35]*pow(sp[34],3)*pow(sp[43],2)*sp[27]*ang[0] + 3.2E+1*sp[35]*pow(sp[34],3)*pow(
      sp[43],2)*sp[44]*ang[0] - 1.6E+1*sp[35]*pow(sp[34],3)*pow(sp[43],2)*ang[1] - 3.2E+1*
      sp[35]*pow(sp[34],4)*sp[43]*sp[44]*ang[0] + 1.6E+1*pow(sp[35],2)*sp[34]*pow(sp[43],2)*
      sp[27]*sp[44]*ang[0] - 3.2E+1*pow(sp[35],2)*sp[34]*pow(sp[43],2)*sp[27]*ang[1] + 1.6E+1
      *pow(sp[35],2)*sp[34]*pow(sp[43],2)*pow(sp[27],2)*ang[0] + 1.6E+1*pow(sp[35],2)*
      pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0] + 2.4E+1*pow(sp[35],2)*pow(sp[34],2)*sp[43]*
      sp[27]*ang[1] - 1.6E+1*pow(sp[35],2)*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 
      2.4E+1*pow(sp[35],2)*pow(sp[34],2)*sp[43]*sp[44]*ang[1] - 1.6E+1*pow(sp[35],2)*
      pow(sp[34],2)*sp[43]*pow(sp[44],2)*ang[0] - 8.E+0*pow(sp[35],2)*pow(sp[34],2)*sp[43]*
      ang[2] - 1.6E+1*pow(sp[35],2)*pow(sp[43],3)*pow(sp[27],2)*ang[0] + 4.E+0*pow(
      sp[35],3)*sp[43]*sp[27]*pow(sp[44],2)*ang[0] - 4.E+0*pow(sp[35],3)*sp[43]*sp[27]*ang[2] - 
      8.E+0*pow(sp[35],3)*sp[43]*pow(sp[27],2)*sp[44]*ang[0] + 4.E+0*pow(sp[35],3)*sp[43]*
      pow(sp[27],3)*ang[0];
 
    kern[1][1] = (1.0/ denom)* kern[1][1]; 
    kern[1][2] =   - 4.8E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*ang[0] + 6.4E+1*sp[35]*
      pow(sp[34],2)*sp[43]*sp[27]*ang[0] + 3.2E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[0] - 
      3.2E+1*sp[35]*pow(sp[34],2)*sp[43]*ang[1] - 1.6E+1*sp[35]*pow(sp[34],3)*sp[27]*ang[0]
       - 3.2E+1*sp[35]*pow(sp[34],3)*sp[44]*ang[0] + 1.6E+1*sp[35]*pow(sp[34],3)*ang[1] + 
      8.E+0*pow(sp[35],2)*sp[34]*sp[27]*sp[44]*ang[0] + 8.E+0*pow(sp[35],2)*sp[34]*sp[27]*ang[1]
       + 4.E+0*pow(sp[35],2)*sp[34]*pow(sp[27],2)*ang[0] + 2.4E+1*pow(sp[35],2)*sp[34]*
      sp[44]*ang[1] - 1.2E+1*pow(sp[35],2)*sp[34]*pow(sp[44],2)*ang[0] - 1.2E+1*pow(
      sp[35],2)*sp[34]*ang[2] + 1.6E+1*pow(sp[35],2)*sp[43]*sp[27]*sp[44]*ang[0] - 1.6E+1*
      pow(sp[35],2)*sp[43]*sp[27]*ang[1] - 1.6E+1*pow(sp[35],2)*sp[43]*pow(sp[27],2)*ang[0];
 
    kern[1][2] = (1.0/ denom)* kern[1][2]; 
    kern[1][3] =   0;
 
    kern[1][3] = (1.0/ denom)* kern[1][3]; 
    kern[1][4] =   0;
 
    kern[1][4] = (1.0/ denom)* kern[1][4]; 
    kern[1][5] =   0;
 
    kern[1][5] = (1.0/ denom)* kern[1][5]; 
    kern[1][6] =   0;
 
    kern[1][6] = (1.0/ denom)* kern[1][6]; 
    kern[1][7] =   - 2.4E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 2.4E+1*
      sp[35]*sp[34]*pow(sp[43],2)*pow(sp[27],2)*ang[0] + 6.4E+1*sp[35]*pow(sp[34],2)*sp[43]*
      sp[27]*sp[44]*ang[0] - 1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*ang[1] + 1.6E+1*sp[35]*
      pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] - 1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*
      ang[1] + 1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[44],2)*ang[0] - 2.4E+1*sp[35]*
      pow(sp[34],3)*sp[27]*sp[44]*ang[0] + 1.6E+1*sp[35]*pow(sp[34],3)*sp[44]*ang[1] - 2.4E+1
      *sp[35]*pow(sp[34],3)*pow(sp[44],2)*ang[0] + 8.E+0*pow(sp[35],2)*sp[34]*sp[27]*sp[44]*
      ang[1] + 6.E+0*pow(sp[35],2)*sp[34]*sp[27]*pow(sp[44],2)*ang[0] - 6.E+0*pow(
      sp[35],2)*sp[34]*sp[27]*ang[2] + 6.E+0*pow(sp[35],2)*sp[34]*pow(sp[27],2)*sp[44]*ang[0] + 
      1.2E+1*pow(sp[35],2)*sp[34]*pow(sp[27],2)*ang[1] - 6.E+0*pow(sp[35],2)*sp[34]*pow(
      sp[27],3)*ang[0] - 6.E+0*pow(sp[35],2)*sp[34]*sp[44]*ang[2] + 1.2E+1*pow(sp[35],2)*
      sp[34]*pow(sp[44],2)*ang[1] - 6.E+0*pow(sp[35],2)*sp[34]*pow(sp[44],3)*ang[0] - 
      8.E+0*pow(sp[35],2)*sp[43]*sp[27]*sp[44]*ang[1] + 8.E+0*pow(sp[35],2)*sp[43]*sp[27]*pow(
      sp[44],2)*ang[0] - 1.6E+1*pow(sp[35],2)*sp[43]*pow(sp[27],2)*sp[44]*ang[0] - 8.E+0*
      pow(sp[35],2)*sp[43]*pow(sp[27],2)*ang[1] + 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[27],3)*
      ang[0];
 
    kern[1][7] = (1.0/ denom)* kern[1][7]; 

    denom =  - 1.6E+1*sp[35]*sp[27] + 1.6E+1*pow(sp[34],2);

    kern[2][0] =   0;
 
    kern[2][0] = (1.0/ denom)* kern[2][0]; 
    kern[2][1] =  8.E+0*sp[35]*sp[34]*sp[43]*sp[27]*ang[1] - 8.E+0*sp[35]*sp[34]*sp[43]*pow(
      sp[27],2)*ang[0] - 8.E+0*sp[35]*sp[34]*sp[43]*sp[44]*ang[1] + 8.E+0*sp[35]*sp[34]*sp[43]*pow(
      sp[44],2)*ang[0] - 1.6E+1*sp[35]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 1.6E+1*sp[35]*
      pow(sp[43],2)*pow(sp[27],2)*ang[0];
 
    kern[2][1] = (1.0/ denom)* kern[2][1]; 
    kern[2][2] =   - 8.E+0*sp[35]*sp[27]*sp[44]*ang[0] + 8.E+0*sp[35]*sp[27]*ang[1] + 
      4.E+0*sp[35]*pow(sp[27],2)*ang[0] - 8.E+0*sp[35]*sp[44]*ang[1] + 4.E+0*sp[35]*pow(
      sp[44],2)*ang[0] + 4.E+0*sp[35]*ang[2] + 3.2E+1*sp[34]*sp[43]*sp[27]*ang[0] - 1.6E+1*
      pow(sp[34],2)*sp[27]*ang[0] - 1.6E+1*pow(sp[43],2)*sp[27]*ang[0];
 
    kern[2][2] = (1.0/ denom)* kern[2][2]; 
    kern[2][3] =   0;
 
    kern[2][3] = (1.0/ denom)* kern[2][3]; 
    kern[2][4] =   0;
 
    kern[2][4] = (1.0/ denom)* kern[2][4]; 
    kern[2][5] =   0;
 
    kern[2][5] = (1.0/ denom)* kern[2][5]; 
    kern[2][6] =   0;
 
    kern[2][6] = (1.0/ denom)* kern[2][6]; 
    kern[2][7] =   - 6.E+0*sp[35]*sp[27]*pow(sp[44],2)*ang[0] - 2.E+0*sp[35]*sp[27]*ang[2]
       + 6.E+0*sp[35]*pow(sp[27],2)*sp[44]*ang[0] + 4.E+0*sp[35]*pow(sp[27],2)*ang[1] - 
      2.E+0*sp[35]*pow(sp[27],3)*ang[0] + 2.E+0*sp[35]*sp[44]*ang[2] - 4.E+0*sp[35]*pow(
      sp[44],2)*ang[1] + 2.E+0*sp[35]*pow(sp[44],3)*ang[0] - 8.E+0*pow(sp[34],2)*sp[27]*
      sp[44]*ang[0] + 8.E+0*pow(sp[34],2)*pow(sp[44],2)*ang[0] - 8.E+0*pow(sp[43],2)*
      sp[27]*sp[44]*ang[0] + 8.E+0*pow(sp[43],2)*pow(sp[27],2)*ang[0];
 
    kern[2][7] = (1.0/ denom)* kern[2][7]; 

    denom =  - 4.8E+1*sp[35]*sp[27] + 4.8E+1*pow(sp[34],2);

    kern[3][0] =   0;
 
    kern[3][0] = (1.0/ denom)* kern[3][0]; 
    kern[3][1] =   0;
 
    kern[3][1] = (1.0/ denom)* kern[3][1]; 
    kern[3][2] =   0;
 
    kern[3][2] = (1.0/ denom)* kern[3][2]; 
    kern[3][3] =   - 2.4E+1*sp[35]*sp[27]*sp[44]*ang[0] - 4.8E+1*sp[35]*sp[27]*ang[1] + 
      1.2E+1*sp[35]*pow(sp[27],2)*ang[0] - 4.8E+1*sp[35]*sp[44]*ang[1] + 1.2E+1*sp[35]*
      pow(sp[44],2)*ang[0] + 3.6E+1*sp[35]*ang[2] - 4.8E+1*sp[34]*sp[43]*sp[27]*ang[0] - 
      4.8E+1*sp[34]*sp[43]*sp[44]*ang[0] + 9.6E+1*sp[34]*sp[43]*ang[1] + 4.8E+1*pow(sp[34],2)
      *sp[44]*ang[0] + 4.8E+1*pow(sp[43],2)*sp[27]*ang[0];
 
    kern[3][3] = (1.0/ denom)* kern[3][3]; 
    kern[3][4] =   0;
 
    kern[3][4] = (1.0/ denom)* kern[3][4]; 
    kern[3][5] =   0;
 
    kern[3][5] = (1.0/ denom)* kern[3][5]; 
    kern[3][6] =   0;
 
    kern[3][6] = (1.0/ denom)* kern[3][6]; 
    kern[3][7] =   0;
 
    kern[3][7] = (1.0/ denom)* kern[3][7]; 

    denom = 4.E+0*sp[35]*pow(sp[34],2) - 4.E+0*pow(sp[35],2)*sp[27];

    kern[4][0] =   0;
 
    kern[4][0] = (1.0/ denom)* kern[4][0]; 
    kern[4][1] =   0;
 
    kern[4][1] = (1.0/ denom)* kern[4][1]; 
    kern[4][2] =   0;
 
    kern[4][2] = (1.0/ denom)* kern[4][2]; 
    kern[4][3] =   0;
 
    kern[4][3] = (1.0/ denom)* kern[4][3]; 
    kern[4][4] =  1.2E+1*sp[35]*sp[34]*sp[43]*ang[1] - 6.E+0*pow(sp[35],2)*sp[27]*ang[1]
       - 6.E+0*pow(sp[35],2)*sp[44]*ang[1] + 6.E+0*pow(sp[35],2)*ang[2];
 
    kern[4][4] = (1.0/ denom)* kern[4][4]; 
    kern[4][5] =   0;
 
    kern[4][5] = (1.0/ denom)* kern[4][5]; 
    kern[4][6] =   0;
 
    kern[4][6] = (1.0/ denom)* kern[4][6]; 
    kern[4][7] =   0;
 
    kern[4][7] = (1.0/ denom)* kern[4][7]; 

    denom = 8.E+0*sp[35]*pow(sp[34],4) - 1.6E+1*pow(sp[35],2)*pow(sp[34],2)*sp[27] + 
      8.E+0*pow(sp[35],3)*pow(sp[27],2);

    kern[5][0] =   - 1.6E+1*sp[35]*pow(sp[34],2)*pow(sp[43],2)*ang[0] + 3.2E+1*sp[35]
      *pow(sp[34],3)*sp[43]*ang[0] - 1.6E+1*sp[35]*pow(sp[34],4)*ang[0] - 8.E+0*pow(
      sp[35],2)*sp[34]*sp[43]*sp[27]*ang[0] + 2.4E+1*pow(sp[35],2)*sp[34]*sp[43]*sp[44]*ang[0] - 
      2.4E+1*pow(sp[35],2)*sp[34]*sp[43]*ang[1] + 1.6E+1*pow(sp[35],2)*pow(sp[34],2)*sp[27]
      *ang[0] - 2.4E+1*pow(sp[35],2)*pow(sp[34],2)*sp[44]*ang[0] + 1.6E+1*pow(sp[35],2)
      *pow(sp[34],2)*ang[1] - 8.E+0*pow(sp[35],2)*pow(sp[43],2)*sp[27]*ang[0] + 1.2E+1*
      pow(sp[35],3)*sp[27]*sp[44]*ang[0] - 4.E+0*pow(sp[35],3)*sp[27]*ang[1] - 6.E+0*pow(
      sp[35],3)*pow(sp[27],2)*ang[0] + 1.2E+1*pow(sp[35],3)*sp[44]*ang[1] - 6.E+0*pow(
      sp[35],3)*pow(sp[44],2)*ang[0] - 6.E+0*pow(sp[35],3)*ang[2];
 
    kern[5][0] = (1.0/ denom)* kern[5][0]; 
    kern[5][1] =   0;
 
    kern[5][1] = (1.0/ denom)* kern[5][1]; 
    kern[5][2] =   0;
 
    kern[5][2] = (1.0/ denom)* kern[5][2]; 
    kern[5][3] =   0;
 
    kern[5][3] = (1.0/ denom)* kern[5][3]; 
    kern[5][4] =   0;
 
    kern[5][4] = (1.0/ denom)* kern[5][4]; 
    kern[5][5] =  8.E+0*sp[35]*pow(sp[34],2)*pow(sp[43],2)*sp[27]*ang[0] - 8.E+0*sp[35]*
      pow(sp[34],2)*pow(sp[43],2)*sp[44]*ang[0] - 1.6E+1*sp[35]*pow(sp[34],2)*pow(sp[43],2)
      *ang[1] - 8.E+0*sp[35]*pow(sp[34],3)*sp[43]*sp[27]*ang[0] + 8.E+0*sp[35]*pow(sp[34],3)*
      sp[43]*sp[44]*ang[0] + 8.E+0*sp[35]*pow(sp[34],3)*sp[43]*ang[1] - 8.E+0*pow(sp[35],2)*
      sp[34]*sp[43]*sp[27]*sp[44]*ang[0] + 2.8E+1*pow(sp[35],2)*sp[34]*sp[43]*sp[27]*ang[1] - 4.E+0
      *pow(sp[35],2)*sp[34]*sp[43]*pow(sp[27],2)*ang[0] + 1.2E+1*pow(sp[35],2)*sp[34]*sp[43]*
      sp[44]*ang[1] + 1.2E+1*pow(sp[35],2)*sp[34]*sp[43]*pow(sp[44],2)*ang[0] - 2.4E+1*
      pow(sp[35],2)*sp[34]*sp[43]*ang[2] + 4.E+0*pow(sp[35],2)*pow(sp[34],2)*sp[27]*sp[44]*
      ang[0] - 8.E+0*pow(sp[35],2)*pow(sp[34],2)*sp[27]*ang[1] + 4.E+0*pow(sp[35],2)*
      pow(sp[34],2)*pow(sp[27],2)*ang[0] - 8.E+0*pow(sp[35],2)*pow(sp[34],2)*sp[44]*ang[1]
       - 8.E+0*pow(sp[35],2)*pow(sp[34],2)*pow(sp[44],2)*ang[0] + 4.E+0*pow(sp[35],2)
      *pow(sp[34],2)*ang[2] - 4.E+0*pow(sp[35],2)*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 
      8.E+0*pow(sp[35],2)*pow(sp[43],2)*sp[27]*ang[1] + 4.E+0*pow(sp[35],2)*pow(
      sp[43],2)*pow(sp[27],2)*ang[0] - 4.E+0*pow(sp[35],3)*sp[27]*sp[44]*ang[1] + 5.E+0*
      pow(sp[35],3)*sp[27]*pow(sp[44],2)*ang[0] + 1.1E+1*pow(sp[35],3)*sp[27]*ang[2] - 
      pow(sp[35],3)*pow(sp[27],2)*sp[44]*ang[0] - 4.E+0*pow(sp[35],3)*pow(sp[27],2)*ang[1]
       - pow(sp[35],3)*pow(sp[27],3)*ang[0] + 9.E+0*pow(sp[35],3)*sp[44]*ang[2] - 3.E+0
      *pow(sp[35],3)*pow(sp[44],3)*ang[0] - 6.E+0*pow(sp[35],3)*ang[3];
 
    kern[5][5] = (1.0/ denom)* kern[5][5]; 
    kern[5][6] =   - 2.4E+1*sp[35]*sp[34]*pow(sp[43],3)*sp[27]*ang[0] + 3.2E+1*sp[35]*
      pow(sp[34],2)*pow(sp[43],2)*sp[27]*ang[0] + 1.6E+1*sp[35]*pow(sp[34],2)*pow(sp[43],2)
      *sp[44]*ang[0] - 1.6E+1*sp[35]*pow(sp[34],2)*pow(sp[43],2)*ang[1] - 8.E+0*sp[35]*
      pow(sp[34],3)*sp[43]*sp[27]*ang[0] - 1.6E+1*sp[35]*pow(sp[34],3)*sp[43]*sp[44]*ang[0] + 
      8.E+0*sp[35]*pow(sp[34],3)*sp[43]*ang[1] + 4.E+0*pow(sp[35],2)*sp[34]*sp[43]*sp[27]*sp[44]*
      ang[0] + 4.E+0*pow(sp[35],2)*sp[34]*sp[43]*sp[27]*ang[1] + 2.E+0*pow(sp[35],2)*sp[34]*
      sp[43]*pow(sp[27],2)*ang[0] + 1.2E+1*pow(sp[35],2)*sp[34]*sp[43]*sp[44]*ang[1] - 6.E+0*
      pow(sp[35],2)*sp[34]*sp[43]*pow(sp[44],2)*ang[0] - 6.E+0*pow(sp[35],2)*sp[34]*sp[43]*
      ang[2] + 8.E+0*pow(sp[35],2)*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 8.E+0*pow(
      sp[35],2)*pow(sp[43],2)*sp[27]*ang[1] - 8.E+0*pow(sp[35],2)*pow(sp[43],2)*pow(
      sp[27],2)*ang[0];
 
    kern[5][6] = (1.0/ denom)* kern[5][6]; 
    kern[5][7] =   0;
 
    kern[5][7] = (1.0/ denom)* kern[5][7]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[34],3)*sp[27] + 8.E+0*pow(sp[35],2)*sp[34]*pow(
      sp[27],2) + 8.E+0*pow(sp[34],5);

    kern[6][0] =   - 2.4E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*ang[0] + 3.2E+1*sp[35]*
      pow(sp[34],2)*sp[43]*sp[27]*ang[0] + 1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[0] - 
      1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*ang[1] - 8.E+0*sp[35]*pow(sp[34],3)*sp[27]*ang[0] - 
      1.6E+1*sp[35]*pow(sp[34],3)*sp[44]*ang[0] + 8.E+0*sp[35]*pow(sp[34],3)*ang[1] + 
      4.E+0*pow(sp[35],2)*sp[34]*sp[27]*sp[44]*ang[0] + 4.E+0*pow(sp[35],2)*sp[34]*sp[27]*ang[1]
       + 2.E+0*pow(sp[35],2)*sp[34]*pow(sp[27],2)*ang[0] + 1.2E+1*pow(sp[35],2)*sp[34]*
      sp[44]*ang[1] - 6.E+0*pow(sp[35],2)*sp[34]*pow(sp[44],2)*ang[0] - 6.E+0*pow(
      sp[35],2)*sp[34]*ang[2] + 8.E+0*pow(sp[35],2)*sp[43]*sp[27]*sp[44]*ang[0] - 8.E+0*pow(
      sp[35],2)*sp[43]*sp[27]*ang[1] - 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[27],2)*ang[0];
 
    kern[6][0] = (1.0/ denom)* kern[6][0]; 
    kern[6][1] =   0;
 
    kern[6][1] = (1.0/ denom)* kern[6][1]; 
    kern[6][2] =   0;
 
    kern[6][2] = (1.0/ denom)* kern[6][2]; 
    kern[6][3] =   0;
 
    kern[6][3] = (1.0/ denom)* kern[6][3]; 
    kern[6][4] =   0;
 
    kern[6][4] = (1.0/ denom)* kern[6][4]; 
    kern[6][5] =   - 1.2E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 2.4E+1*
      sp[35]*sp[34]*pow(sp[43],2)*sp[27]*ang[1] + 1.2E+1*sp[35]*sp[34]*pow(sp[43],2)*pow(
      sp[27],2)*ang[0] + 2.4E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*ang[1] - 8.E+0*sp[35]*pow(
      sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 8.E+0*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[1] + 
      8.E+0*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[44],2)*ang[0] - 1.6E+1*sp[35]*pow(sp[34],2)*
      sp[43]*ang[2] + 4.E+0*sp[35]*pow(sp[34],3)*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*pow(
      sp[34],3)*sp[44]*ang[1] - 4.E+0*sp[35]*pow(sp[34],3)*pow(sp[44],2)*ang[0] - 4.E+0*
      pow(sp[35],2)*sp[34]*sp[27]*sp[44]*ang[1] + pow(sp[35],2)*sp[34]*sp[27]*pow(sp[44],2)*ang[0]
       + 1.5E+1*pow(sp[35],2)*sp[34]*sp[27]*ang[2] - pow(sp[35],2)*sp[34]*pow(sp[27],2)*sp[44]
      *ang[0] - 1.2E+1*pow(sp[35],2)*sp[34]*pow(sp[27],2)*ang[1] + 3.E+0*pow(sp[35],2)*
      sp[34]*pow(sp[27],3)*ang[0] + 9.E+0*pow(sp[35],2)*sp[34]*sp[44]*ang[2] - 3.E+0*pow(
      sp[35],2)*sp[34]*pow(sp[44],3)*ang[0] - 6.E+0*pow(sp[35],2)*sp[34]*ang[3] + 4.E+0*
      pow(sp[35],2)*sp[43]*sp[27]*sp[44]*ang[1] + 4.E+0*pow(sp[35],2)*sp[43]*sp[27]*pow(sp[44],2)
      *ang[0] - 8.E+0*pow(sp[35],2)*sp[43]*sp[27]*ang[2] + 1.2E+1*pow(sp[35],2)*sp[43]*
      pow(sp[27],2)*ang[1] - 4.E+0*pow(sp[35],2)*sp[43]*pow(sp[27],3)*ang[0];
 
    kern[6][5] = (1.0/ denom)* kern[6][5]; 
    kern[6][6] =  1.6E+1*sp[35]*sp[34]*pow(sp[43],2)*sp[27]*sp[44]*ang[0] - 8.E+0*sp[35]*
      sp[34]*pow(sp[43],2)*sp[27]*ang[1] + 1.6E+1*sp[35]*sp[34]*pow(sp[43],2)*pow(sp[27],2)*
      ang[0] - 4.E+0*sp[35]*pow(sp[34],2)*sp[43]*sp[27]*sp[44]*ang[0] + 1.6E+1*sp[35]*pow(
      sp[34],2)*sp[43]*sp[27]*ang[1] - 6.E+0*sp[35]*pow(sp[34],2)*sp[43]*pow(sp[27],2)*ang[0] + 
      1.6E+1*sp[35]*pow(sp[34],2)*sp[43]*sp[44]*ang[1] - 6.E+0*sp[35]*pow(sp[34],2)*sp[43]*
      pow(sp[44],2)*ang[0] - 1.E+1*sp[35]*pow(sp[34],2)*sp[43]*ang[2] - 1.6E+1*sp[35]*pow(
      sp[43],3)*pow(sp[27],2)*ang[0] - 4.E+0*pow(sp[35],2)*sp[43]*sp[27]*sp[44]*ang[1] + 
      4.E+0*pow(sp[35],2)*sp[43]*sp[27]*ang[2] - 4.E+0*pow(sp[35],2)*sp[43]*pow(sp[27],2)*
      ang[1] - 8.E+0*pow(sp[34],2)*pow(sp[43],3)*sp[27]*ang[0] + 8.E+0*pow(sp[34],3)*
      pow(sp[43],2)*sp[27]*ang[0] + 8.E+0*pow(sp[34],3)*pow(sp[43],2)*sp[44]*ang[0] - 
      1.6E+1*pow(sp[34],3)*pow(sp[43],2)*ang[1] - 8.E+0*pow(sp[34],4)*sp[43]*sp[44]*ang[0]
      ;
 
    kern[6][6] = (1.0/ denom)* kern[6][6]; 
    kern[6][7] =   0;
 
    kern[6][7] = (1.0/ denom)* kern[6][7]; 

    denom =  - 3.2E+1*sp[35]*pow(sp[34],2)*sp[27] + 1.6E+1*pow(sp[35],2)*pow(sp[27],2)
       + 1.6E+1*pow(sp[34],4);

    kern[7][0] =   0;
 
    kern[7][0] = (1.0/ denom)* kern[7][0]; 
    kern[7][1] =  4.8E+1*sp[35]*sp[34]*pow(sp[43],3)*sp[27]*ang[0] - 6.4E+1*sp[35]*pow(
      sp[34],2)*pow(sp[43],2)*sp[27]*ang[0] - 3.2E+1*sp[35]*pow(sp[34],2)*pow(sp[43],2)*sp[44]
      *ang[0] + 3.2E+1*sp[35]*pow(sp[34],2)*pow(sp[43],2)*ang[1] + 1.6E+1*sp[35]*pow(
      sp[34],3)*sp[43]*sp[27]*ang[0] + 3.2E+1*sp[35]*pow(sp[34],3)*sp[43]*sp[44]*ang[0] - 1.6E+1
      *sp[35]*pow(sp[34],3)*sp[43]*ang[1] - 8.E+0*pow(sp[35],2)*sp[34]*sp[43]*sp[27]*sp[44]*ang[0]
       - 8.E+0*pow(sp[35],2)*sp[34]*sp[43]*sp[27]*ang[1] - 4.E+0*pow(sp[35],2)*sp[34]*sp[43]*
      pow(sp[27],2)*ang[0] - 2.4E+1*pow(sp[35],2)*sp[34]*sp[43]*sp[44]*ang[1] + 1.2E+1*
      pow(sp[35],2)*sp[34]*sp[43]*pow(sp[44],2)*ang[0] + 1.2E+1*pow(sp[35],2)*sp[34]*sp[43]*
      ang[2] - 1.6E+1*pow(sp[35],2)*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 1.6E+1*pow(
      sp[35],2)*pow(sp[43],2)*sp[27]*ang[1] + 1.6E+1*pow(sp[35],2)*pow(sp[43],2)*pow(
      sp[27],2)*ang[0];
 
    kern[7][1] = (1.0/ denom)* kern[7][1]; 
    kern[7][2] =  1.6E+1*sp[35]*sp[34]*sp[43]*sp[27]*ang[0] - 4.8E+1*sp[35]*sp[34]*sp[43]*sp[44]*
      ang[0] + 4.8E+1*sp[35]*sp[34]*sp[43]*ang[1] - 3.2E+1*sp[35]*pow(sp[34],2)*sp[27]*ang[0]
       + 4.8E+1*sp[35]*pow(sp[34],2)*sp[44]*ang[0] - 3.2E+1*sp[35]*pow(sp[34],2)*ang[1] + 
      1.6E+1*sp[35]*pow(sp[43],2)*sp[27]*ang[0] - 2.4E+1*pow(sp[35],2)*sp[27]*sp[44]*ang[0]
       + 8.E+0*pow(sp[35],2)*sp[27]*ang[1] + 1.2E+1*pow(sp[35],2)*pow(sp[27],2)*ang[0]
       - 2.4E+1*pow(sp[35],2)*sp[44]*ang[1] + 1.2E+1*pow(sp[35],2)*pow(sp[44],2)*ang[0]
       + 1.2E+1*pow(sp[35],2)*ang[2] + 3.2E+1*pow(sp[34],2)*pow(sp[43],2)*ang[0] - 
      6.4E+1*pow(sp[34],3)*sp[43]*ang[0] + 3.2E+1*pow(sp[34],4)*ang[0];
 
    kern[7][2] = (1.0/ denom)* kern[7][2]; 
    kern[7][3] =   0;
 
    kern[7][3] = (1.0/ denom)* kern[7][3]; 
    kern[7][4] =   0;
 
    kern[7][4] = (1.0/ denom)* kern[7][4]; 
    kern[7][5] =   0;
 
    kern[7][5] = (1.0/ denom)* kern[7][5]; 
    kern[7][6] =   0;
 
    kern[7][6] = (1.0/ denom)* kern[7][6]; 
    kern[7][7] =   - 1.6E+1*sp[35]*sp[34]*sp[43]*sp[27]*sp[44]*ang[0] + 2.4E+1*sp[35]*sp[34]*
      sp[43]*sp[27]*ang[1] - 2.4E+1*sp[35]*sp[34]*sp[43]*pow(sp[27],2)*ang[0] + 2.4E+1*sp[35]*
      sp[34]*sp[43]*sp[44]*ang[1] - 2.4E+1*sp[35]*sp[34]*sp[43]*pow(sp[44],2)*ang[0] - 8.E+0*sp[35]
      *pow(sp[34],2)*sp[27]*ang[1] + 4.E+0*sp[35]*pow(sp[34],2)*pow(sp[27],2)*ang[0] - 
      2.4E+1*sp[35]*pow(sp[34],2)*sp[44]*ang[1] + 2.8E+1*sp[35]*pow(sp[34],2)*pow(sp[44],2)
      *ang[0] + 4.E+0*sp[35]*pow(sp[34],2)*ang[2] + 8.E+0*sp[35]*pow(sp[43],2)*sp[27]*sp[44]*
      ang[0] + 2.4E+1*sp[35]*pow(sp[43],2)*pow(sp[27],2)*ang[0] - 1.E+1*pow(sp[35],2)*
      sp[27]*pow(sp[44],2)*ang[0] + 2.E+0*pow(sp[35],2)*sp[27]*ang[2] + 2.E+0*pow(
      sp[35],2)*pow(sp[27],2)*sp[44]*ang[0] - 4.E+0*pow(sp[35],2)*pow(sp[27],2)*ang[1] + 
      2.E+0*pow(sp[35],2)*pow(sp[27],3)*ang[0] + 6.E+0*pow(sp[35],2)*sp[44]*ang[2] - 
      1.2E+1*pow(sp[35],2)*pow(sp[44],2)*ang[1] + 6.E+0*pow(sp[35],2)*pow(sp[44],3)*
      ang[0] + 1.6E+1*pow(sp[34],2)*pow(sp[43],2)*sp[44]*ang[0] - 3.2E+1*pow(sp[34],3)*
      sp[43]*sp[44]*ang[0] + 1.6E+1*pow(sp[34],4)*sp[44]*ang[0];
 
    kern[7][7] = (1.0/ denom)* kern[7][7]; 

}


void kernelY_noopti_qphv_trans(matCdoub& kern,
  const ArrayScalarProducts& sp,  const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


    // local variables
    Cdoub denom;


    denom = 8.E+0*sp[35]*pow(sp[43],4) - 1.6E+1*pow(sp[35],2)*pow(sp[43],2)*sp[44] + 
      8.E+0*pow(sp[35],3)*pow(sp[44],2);

    kern[0][0] =  1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[29]*sp[44]*svp*svm - 8.E+0*
      sp[35]*pow(sp[43],3)*sp[31]*sp[37]*svp*svm - 8.E+0*sp[35]*pow(sp[43],3)*sp[29]*sp[39]
      *svp*svm + 8.E+0*sp[35]*pow(sp[43],4)*sp[7]*svp*svm + 8.E+0*sp[35]*pow(
      sp[43],4)*ssp*ssm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 
      1.6E+1*pow(sp[35],2)*sp[43]*sp[29]*sp[44]*sp[39]*svp*svm - 8.E+0*pow(sp[35],2)*
      pow(sp[43],2)*sp[44]*sp[7]*svp*svm - 1.6E+1*pow(sp[35],2)*pow(sp[43],2)*sp[44]*
      ssp*ssm + 1.6E+1*pow(sp[35],2)*pow(sp[43],2)*sp[39]*sp[37]*svp*svm + 8.E+0*
      pow(sp[35],2)*sp[31]*sp[29]*pow(sp[44],2)*svp*svm + 8.E+0*pow(sp[35],3)*sp[44]*
      sp[39]*sp[37]*svp*svm + 8.E+0*pow(sp[35],3)*pow(sp[44],2)*ssp*ssm;
 
    kern[0][0] = (1.0/ denom)* kern[0][0]; 
    kern[0][1] =  8.E+0*sp[35]*pow(sp[43],5)*sp[39]*ssp*svm - 8.E+0*sp[35]*pow(
      sp[43],5)*sp[37]*svp*ssm - 1.6E+1*pow(sp[35],2)*pow(sp[43],3)*sp[44]*sp[39]*ssp*
      svm + 1.6E+1*pow(sp[35],2)*pow(sp[43],3)*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(
      sp[35],3)*sp[43]*pow(sp[44],2)*sp[39]*ssp*svm - 8.E+0*pow(sp[35],3)*sp[43]*pow(
      sp[44],2)*sp[37]*svp*ssm;
 
    kern[0][1] = (1.0/ denom)* kern[0][1]; 
    kern[0][2] =   - 8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm + 8.E+0*sp[35]*
      pow(sp[43],2)*sp[29]*sp[44]*svp*ssm + 8.E+0*sp[35]*pow(sp[43],3)*sp[39]*ssp*svm
       - 8.E+0*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm - 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*
      sp[39]*ssp*svm + 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(
      sp[35],2)*sp[31]*pow(sp[44],2)*ssp*svm - 8.E+0*pow(sp[35],2)*sp[29]*pow(sp[44],2)
      *svp*ssm;
 
    kern[0][2] = (1.0/ denom)* kern[0][2]; 
    kern[0][3] =   - 8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0
      *sp[35]*pow(sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm + 8.E+0*pow(sp[35],2)*sp[31]*
      pow(sp[44],2)*sp[37]*svp*svm - 8.E+0*pow(sp[35],2)*sp[29]*pow(sp[44],2)*sp[39]*
      svp*svm;
 
    kern[0][3] = (1.0/ denom)* kern[0][3]; 
    kern[0][4] =   - 8.E+0*sp[35]*pow(sp[43],3)*sp[31]*sp[44]*ssp*svm - 8.E+0*sp[35]*
      pow(sp[43],3)*sp[29]*sp[44]*svp*ssm + 8.E+0*sp[35]*pow(sp[43],4)*sp[39]*ssp*svm
       + 8.E+0*sp[35]*pow(sp[43],4)*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*sp[43]*sp[31]*
      pow(sp[44],2)*ssp*svm + 8.E+0*pow(sp[35],2)*sp[43]*sp[29]*pow(sp[44],2)*svp*ssm
       - 8.E+0*pow(sp[35],2)*pow(sp[43],2)*sp[44]*sp[39]*ssp*svm - 8.E+0*pow(
      sp[35],2)*pow(sp[43],2)*sp[44]*sp[37]*svp*ssm;
 
    kern[0][4] = (1.0/ denom)* kern[0][4]; 
    kern[0][5] =  8.E+0*sp[35]*pow(sp[43],3)*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*
      sp[35]*pow(sp[43],3)*sp[29]*sp[44]*sp[39]*svp*svm - 1.6E+1*sp[35]*pow(sp[43],4)*sp[39]
      *sp[37]*svp*svm - 8.E+0*pow(sp[35],2)*sp[43]*sp[31]*pow(sp[44],2)*sp[37]*svp*svm
       - 8.E+0*pow(sp[35],2)*sp[43]*sp[29]*pow(sp[44],2)*sp[39]*svp*svm + 1.6E+1*
      pow(sp[35],2)*pow(sp[43],2)*sp[44]*sp[39]*sp[37]*svp*svm;
 
    kern[0][5] = (1.0/ denom)* kern[0][5]; 
    kern[0][6] =  2.4E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[29]*pow(sp[44],2)*svp*svm - 
      2.4E+1*sp[35]*pow(sp[43],3)*sp[31]*sp[44]*sp[37]*svp*svm - 2.4E+1*sp[35]*pow(
      sp[43],3)*sp[29]*sp[44]*sp[39]*svp*svm + 8.E+0*sp[35]*pow(sp[43],4)*sp[44]*sp[7]*svp
      *svm + 1.6E+1*sp[35]*pow(sp[43],4)*sp[39]*sp[37]*svp*svm + 8.E+0*pow(sp[35],2)
      *pow(sp[43],2)*sp[44]*sp[39]*sp[37]*svp*svm - 8.E+0*pow(sp[35],2)*pow(sp[43],2)*
      pow(sp[44],2)*sp[7]*svp*svm;
 
    kern[0][6] = (1.0/ denom)* kern[0][6]; 
    kern[0][7] =   0;
 
    kern[0][7] = (1.0/ denom)* kern[0][7]; 

    denom = 1.6E+1*sp[35]*pow(sp[43],5) - 3.2E+1*pow(sp[35],2)*pow(sp[43],3)*sp[44] + 
      1.6E+1*pow(sp[35],3)*sp[43]*pow(sp[44],2);

    kern[1][0] =   - 1.6E+1*sp[35]*pow(sp[43],3)*sp[31]*ssp*svm + 1.6E+1*sp[35]*
      pow(sp[43],3)*sp[29]*svp*ssm + 1.6E+1*pow(sp[35],2)*sp[43]*sp[31]*sp[44]*ssp*svm
       - 1.6E+1*pow(sp[35],2)*sp[43]*sp[29]*sp[44]*svp*ssm + 1.6E+1*pow(sp[35],2)*
      pow(sp[43],2)*sp[39]*ssp*svm - 1.6E+1*pow(sp[35],2)*pow(sp[43],2)*sp[37]*svp*
      ssm - 1.6E+1*pow(sp[35],3)*sp[44]*sp[39]*ssp*svm + 1.6E+1*pow(sp[35],3)*sp[44]*
      sp[37]*svp*ssm;
 
    kern[1][0] = (1.0/ denom)* kern[1][0]; 
    kern[1][1] =   - 3.2E+1*sp[35]*pow(sp[43],3)*sp[31]*sp[29]*sp[44]*svp*svm + 
      3.2E+1*sp[35]*pow(sp[43],4)*sp[31]*sp[37]*svp*svm + 3.2E+1*sp[35]*pow(sp[43],4)*
      sp[29]*sp[39]*svp*svm - 1.6E+1*sp[35]*pow(sp[43],5)*sp[7]*svp*svm + 1.6E+1*
      sp[35]*pow(sp[43],5)*ssp*ssm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[31]*sp[29]*pow(
      sp[44],2)*svp*svm + 1.6E+1*pow(sp[35],2)*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*
      svm + 1.6E+1*pow(sp[35],2)*pow(sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm + 1.6E+1
      *pow(sp[35],2)*pow(sp[43],3)*sp[44]*sp[7]*svp*svm - 3.2E+1*pow(sp[35],2)*pow(
      sp[43],3)*sp[44]*ssp*ssm - 6.4E+1*pow(sp[35],2)*pow(sp[43],3)*sp[39]*sp[37]*svp*
      svm + 1.6E+1*pow(sp[35],3)*sp[43]*sp[44]*sp[39]*sp[37]*svp*svm + 1.6E+1*pow(
      sp[35],3)*sp[43]*pow(sp[44],2)*ssp*ssm;
 
    kern[1][1] = (1.0/ denom)* kern[1][1]; 
    kern[1][2] =   - 4.8E+1*sp[35]*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm + 3.2E+1*sp[35]*
      pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 3.2E+1*sp[35]*pow(sp[43],2)*sp[29]*sp[39]*
      svp*svm - 1.6E+1*sp[35]*pow(sp[43],3)*sp[7]*svp*svm + 1.6E+1*pow(sp[35],2)
      *sp[43]*sp[44]*sp[7]*svp*svm - 4.8E+1*pow(sp[35],2)*sp[43]*sp[39]*sp[37]*svp*svm
       + 1.6E+1*pow(sp[35],2)*sp[31]*sp[44]*sp[37]*svp*svm + 1.6E+1*pow(sp[35],2)*
      sp[29]*sp[44]*sp[39]*svp*svm;
 
    kern[1][2] = (1.0/ denom)* kern[1][2]; 
    kern[1][3] =  1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm + 1.6E+1*sp[35]*
      pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 1.6E+1*sp[35]*pow(sp[43],3)*sp[39]*ssp*svm
       - 1.6E+1*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm + 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]
      *sp[39]*ssp*svm + 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm - 1.6E+1*
      pow(sp[35],2)*sp[31]*pow(sp[44],2)*ssp*svm - 1.6E+1*pow(sp[35],2)*sp[29]*pow(
      sp[44],2)*svp*ssm;
 
    kern[1][3] = (1.0/ denom)* kern[1][3]; 
    kern[1][4] =   - 1.6E+1*sp[35]*pow(sp[43],3)*sp[31]*sp[37]*svp*svm + 1.6E+1*
      sp[35]*pow(sp[43],3)*sp[29]*sp[39]*svp*svm + 1.6E+1*pow(sp[35],2)*sp[43]*sp[31]*sp[44]
      *sp[37]*svp*svm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[29]*sp[44]*sp[39]*svp*svm;
 
    kern[1][4] = (1.0/ denom)* kern[1][4]; 
    kern[1][5] =   - 1.6E+1*sp[35]*pow(sp[43],3)*sp[31]*sp[44]*ssp*svm + 1.6E+1*
      sp[35]*pow(sp[43],3)*sp[29]*sp[44]*svp*ssm + 1.6E+1*sp[35]*pow(sp[43],4)*sp[39]*ssp*
      svm - 1.6E+1*sp[35]*pow(sp[43],4)*sp[37]*svp*ssm + 1.6E+1*pow(sp[35],2)*sp[43]*
      sp[31]*pow(sp[44],2)*ssp*svm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[29]*pow(sp[44],2)*
      svp*ssm - 1.6E+1*pow(sp[35],2)*pow(sp[43],2)*sp[44]*sp[39]*ssp*svm + 1.6E+1*
      pow(sp[35],2)*pow(sp[43],2)*sp[44]*sp[37]*svp*ssm;
 
    kern[1][5] = (1.0/ denom)* kern[1][5]; 
    kern[1][6] =   0;
 
    kern[1][6] = (1.0/ denom)* kern[1][6]; 
    kern[1][7] =   - 4.8E+1*sp[35]*sp[43]*sp[31]*sp[29]*pow(sp[44],2)*svp*svm + 
      4.8E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm + 4.8E+1*sp[35]*pow(
      sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44]*sp[7]*
      svp*svm - 3.2E+1*sp[35]*pow(sp[43],3)*sp[39]*sp[37]*svp*svm - 1.6E+1*pow(
      sp[35],2)*sp[43]*sp[44]*sp[39]*sp[37]*svp*svm + 1.6E+1*pow(sp[35],2)*sp[43]*pow(
      sp[44],2)*sp[7]*svp*svm;
 
    kern[1][7] = (1.0/ denom)* kern[1][7]; 

    denom =  - 1.6E+1*sp[35]*sp[44] + 1.6E+1*pow(sp[43],2);

    kern[2][0] =   - 1.6E+1*sp[35]*sp[43]*sp[39]*ssp*svm + 1.6E+1*sp[35]*sp[43]*sp[37]*
      svp*ssm + 1.6E+1*sp[35]*sp[31]*sp[44]*ssp*svm - 1.6E+1*sp[35]*sp[29]*sp[44]*svp*
      ssm;
 
    kern[2][0] = (1.0/ denom)* kern[2][0]; 
    kern[2][1] =   - 1.6E+1*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*sp[35]*
      sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 3.2E+1*sp[35]*pow(sp[43],2)*sp[39]*sp[37]*svp*
      svm;
 
    kern[2][1] = (1.0/ denom)* kern[2][1]; 
    kern[2][2] =   - 1.6E+1*sp[35]*sp[44]*ssp*ssm + 1.6E+1*sp[35]*sp[39]*sp[37]*svp*
      svm + 1.6E+1*pow(sp[43],2)*ssp*ssm - 1.6E+1*sp[31]*sp[29]*sp[44]*svp*svm;
 
    kern[2][2] = (1.0/ denom)* kern[2][2]; 
    kern[2][3] =   - 1.6E+1*sp[35]*sp[44]*sp[39]*ssp*svm - 1.6E+1*sp[35]*sp[44]*sp[37]*
      svp*ssm + 1.6E+1*pow(sp[43],2)*sp[39]*ssp*svm + 1.6E+1*pow(sp[43],2)*sp[37]
      *svp*ssm;
 
    kern[2][3] = (1.0/ denom)* kern[2][3]; 
    kern[2][4] =   0;
 
    kern[2][4] = (1.0/ denom)* kern[2][4]; 
    kern[2][5] =   0;
 
    kern[2][5] = (1.0/ denom)* kern[2][5]; 
    kern[2][6] =  1.6E+1*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm - 1.6E+1*pow(
      sp[43],2)*sp[29]*sp[44]*svp*ssm - 1.6E+1*pow(sp[43],3)*sp[39]*ssp*svm + 1.6E+1
      *pow(sp[43],3)*sp[37]*svp*ssm;
 
    kern[2][6] = (1.0/ denom)* kern[2][6]; 
    kern[2][7] =   - 1.6E+1*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*sp[43]*sp[29]
      *sp[44]*sp[39]*svp*svm + 3.2E+1*pow(sp[43],2)*sp[39]*sp[37]*svp*svm;
 
    kern[2][7] = (1.0/ denom)* kern[2][7]; 

    denom =  - 4.8E+1*sp[35]*sp[44] + 4.8E+1*pow(sp[43],2);

    kern[3][0] =   - 4.8E+1*sp[35]*sp[31]*sp[37]*svp*svm + 4.8E+1*sp[35]*sp[29]*
      sp[39]*svp*svm;
 
    kern[3][0] = (1.0/ denom)* kern[3][0]; 
    kern[3][1] =   - 4.8E+1*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm - 4.8E+1*sp[35]*sp[43]*
      sp[29]*sp[44]*svp*ssm + 4.8E+1*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm + 4.8E+1*
      sp[35]*pow(sp[43],2)*sp[37]*svp*ssm;
 
    kern[3][1] = (1.0/ denom)* kern[3][1]; 
    kern[3][2] =  4.8E+1*sp[35]*sp[39]*ssp*svm + 4.8E+1*sp[35]*sp[37]*svp*ssm - 
      4.8E+1*sp[43]*sp[31]*ssp*svm - 4.8E+1*sp[43]*sp[29]*svp*ssm;
 
    kern[3][2] = (1.0/ denom)* kern[3][2]; 
    kern[3][3] =   - 4.8E+1*sp[35]*sp[44]*ssp*ssm + 4.8E+1*sp[35]*sp[39]*sp[37]*svp*
      svm - 4.8E+1*sp[43]*sp[31]*sp[37]*svp*svm - 4.8E+1*sp[43]*sp[29]*sp[39]*svp*svm
       + 4.8E+1*pow(sp[43],2)*ssp*ssm + 4.8E+1*sp[31]*sp[29]*sp[44]*svp*svm;
 
    kern[3][3] = (1.0/ denom)* kern[3][3]; 
    kern[3][4] =   0;
 
    kern[3][4] = (1.0/ denom)* kern[3][4]; 
    kern[3][5] =   0;
 
    kern[3][5] = (1.0/ denom)* kern[3][5]; 
    kern[3][6] =   - 4.8E+1*pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 4.8E+1*pow(
      sp[43],2)*sp[29]*sp[39]*svp*svm;
 
    kern[3][6] = (1.0/ denom)* kern[3][6]; 
    kern[3][7] =   - 4.8E+1*sp[43]*sp[31]*sp[44]*ssp*svm - 4.8E+1*sp[43]*sp[29]*sp[44]*
      svp*ssm + 4.8E+1*pow(sp[43],2)*sp[39]*ssp*svm + 4.8E+1*pow(sp[43],2)*sp[37]
      *svp*ssm;
 
    kern[3][7] = (1.0/ denom)* kern[3][7]; 

    denom = 4.E+0*sp[35]*pow(sp[43],2) - 4.E+0*pow(sp[35],2)*sp[44];

    kern[4][0] =   - 4.E+0*sp[35]*sp[43]*sp[31]*ssp*svm - 4.E+0*sp[35]*sp[43]*sp[29]*
      svp*ssm + 4.E+0*pow(sp[35],2)*sp[39]*ssp*svm + 4.E+0*pow(sp[35],2)*sp[37]*
      svp*ssm;
 
    kern[4][0] = (1.0/ denom)* kern[4][0]; 
    kern[4][1] =  4.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[37]*svp*svm - 4.E+0*sp[35]*
      pow(sp[43],2)*sp[29]*sp[39]*svp*svm;
 
    kern[4][1] = (1.0/ denom)* kern[4][1]; 
    kern[4][2] =  4.E+0*sp[35]*sp[31]*sp[37]*svp*svm - 4.E+0*sp[35]*sp[29]*sp[39]*svp
      *svm;
 
    kern[4][2] = (1.0/ denom)* kern[4][2]; 
    kern[4][3] =   0;
 
    kern[4][3] = (1.0/ denom)* kern[4][3]; 
    kern[4][4] =   - 4.E+0*sp[35]*pow(sp[43],2)*sp[7]*svp*svm + 4.E+0*sp[35]*
      pow(sp[43],2)*ssp*ssm + 4.E+0*pow(sp[35],2)*sp[44]*sp[7]*svp*svm - 4.E+0*
      pow(sp[35],2)*sp[44]*ssp*ssm;
 
    kern[4][4] = (1.0/ denom)* kern[4][4]; 
    kern[4][5] =   - 4.E+0*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm - 4.E+0*sp[35]*pow(
      sp[43],2)*sp[37]*svp*ssm + 4.E+0*pow(sp[35],2)*sp[44]*sp[39]*ssp*svm + 4.E+0*
      pow(sp[35],2)*sp[44]*sp[37]*svp*ssm;
 
    kern[4][5] = (1.0/ denom)* kern[4][5]; 
    kern[4][6] =   - 4.E+0*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm - 4.E+0*sp[35]*sp[43]*
      sp[29]*sp[44]*svp*ssm + 4.E+0*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm + 4.E+0*sp[35]*
      pow(sp[43],2)*sp[37]*svp*ssm;
 
    kern[4][6] = (1.0/ denom)* kern[4][6]; 
    kern[4][7] =  4.E+0*sp[35]*sp[31]*sp[44]*sp[37]*svp*svm - 4.E+0*sp[35]*sp[29]*sp[44]*
      sp[39]*svp*svm;
 
    kern[4][7] = (1.0/ denom)* kern[4][7]; 

    denom = 8.E+0*sp[35]*pow(sp[43],4) - 1.6E+1*pow(sp[35],2)*pow(sp[43],2)*sp[44] + 
      8.E+0*pow(sp[35],3)*pow(sp[44],2);

    kern[5][0] =   - 1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[29]*svp*svm + 2.4E+1*
      pow(sp[35],2)*sp[43]*sp[31]*sp[37]*svp*svm + 2.4E+1*pow(sp[35],2)*sp[43]*sp[29]*
      sp[39]*svp*svm - 8.E+0*pow(sp[35],2)*pow(sp[43],2)*sp[7]*svp*svm - 8.E+0*
      pow(sp[35],2)*sp[31]*sp[29]*sp[44]*svp*svm + 8.E+0*pow(sp[35],3)*sp[44]*sp[7]*svp
      *svm - 2.4E+1*pow(sp[35],3)*sp[39]*sp[37]*svp*svm;
 
    kern[5][0] = (1.0/ denom)* kern[5][0]; 
    kern[5][1] =   - 8.E+0*sp[35]*pow(sp[43],4)*sp[31]*ssp*svm + 8.E+0*sp[35]*pow(
      sp[43],4)*sp[29]*svp*ssm + 8.E+0*pow(sp[35],2)*pow(sp[43],2)*sp[31]*sp[44]*ssp*
      svm - 8.E+0*pow(sp[35],2)*pow(sp[43],2)*sp[29]*sp[44]*svp*ssm + 8.E+0*pow(
      sp[35],2)*pow(sp[43],3)*sp[39]*ssp*svm - 8.E+0*pow(sp[35],2)*pow(sp[43],3)*sp[37]
      *svp*ssm - 8.E+0*pow(sp[35],3)*sp[43]*sp[44]*sp[39]*ssp*svm + 8.E+0*pow(
      sp[35],3)*sp[43]*sp[44]*sp[37]*svp*ssm;
 
    kern[5][1] = (1.0/ denom)* kern[5][1]; 
    kern[5][2] =   0;
 
    kern[5][2] = (1.0/ denom)* kern[5][2]; 
    kern[5][3] =  8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[37]*svp*svm - 8.E+0*sp[35]*
      pow(sp[43],2)*sp[29]*sp[39]*svp*svm - 8.E+0*pow(sp[35],2)*sp[31]*sp[44]*sp[37]*svp
      *svm + 8.E+0*pow(sp[35],2)*sp[29]*sp[44]*sp[39]*svp*svm;
 
    kern[5][3] = (1.0/ denom)* kern[5][3]; 
    kern[5][4] =  8.E+0*sp[35]*pow(sp[43],3)*sp[31]*ssp*svm + 8.E+0*sp[35]*pow(
      sp[43],3)*sp[29]*svp*ssm - 8.E+0*pow(sp[35],2)*sp[43]*sp[31]*sp[44]*ssp*svm - 
      8.E+0*pow(sp[35],2)*sp[43]*sp[29]*sp[44]*svp*ssm - 8.E+0*pow(sp[35],2)*pow(
      sp[43],2)*sp[39]*ssp*svm - 8.E+0*pow(sp[35],2)*pow(sp[43],2)*sp[37]*svp*ssm + 
      8.E+0*pow(sp[35],3)*sp[44]*sp[39]*ssp*svm + 8.E+0*pow(sp[35],3)*sp[44]*sp[37]*svp
      *ssm;
 
    kern[5][4] = (1.0/ denom)* kern[5][4]; 
    kern[5][5] =   - 8.E+0*sp[35]*pow(sp[43],3)*sp[31]*sp[37]*svp*svm - 8.E+0*sp[35]
      *pow(sp[43],3)*sp[29]*sp[39]*svp*svm + 8.E+0*sp[35]*pow(sp[43],4)*sp[7]*svp*
      svm + 8.E+0*sp[35]*pow(sp[43],4)*ssp*ssm + 8.E+0*pow(sp[35],2)*sp[43]*sp[31]*
      sp[44]*sp[37]*svp*svm + 8.E+0*pow(sp[35],2)*sp[43]*sp[29]*sp[44]*sp[39]*svp*svm - 
      1.6E+1*pow(sp[35],2)*pow(sp[43],2)*sp[44]*sp[7]*svp*svm - 1.6E+1*pow(
      sp[35],2)*pow(sp[43],2)*sp[44]*ssp*ssm + 1.6E+1*pow(sp[35],2)*pow(sp[43],2)*sp[39]
      *sp[37]*svp*svm - 1.6E+1*pow(sp[35],3)*sp[44]*sp[39]*sp[37]*svp*svm + 8.E+0*
      pow(sp[35],3)*pow(sp[44],2)*sp[7]*svp*svm + 8.E+0*pow(sp[35],3)*pow(sp[44],2)
      *ssp*ssm;
 
    kern[5][5] = (1.0/ denom)* kern[5][5]; 
    kern[5][6] =   - 2.4E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[29]*sp[44]*svp*svm + 
      1.6E+1*sp[35]*pow(sp[43],3)*sp[31]*sp[37]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],3)*
      sp[29]*sp[39]*svp*svm - 8.E+0*sp[35]*pow(sp[43],4)*sp[7]*svp*svm + 8.E+0*
      pow(sp[35],2)*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*pow(sp[35],2)*sp[43]*sp[29]*
      sp[44]*sp[39]*svp*svm + 8.E+0*pow(sp[35],2)*pow(sp[43],2)*sp[44]*sp[7]*svp*svm
       - 2.4E+1*pow(sp[35],2)*pow(sp[43],2)*sp[39]*sp[37]*svp*svm;
 
    kern[5][6] = (1.0/ denom)* kern[5][6]; 
    kern[5][7] =   - 8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm + 8.E+0*sp[35]*
      pow(sp[43],2)*sp[29]*sp[44]*svp*ssm + 8.E+0*sp[35]*pow(sp[43],3)*sp[39]*ssp*svm
       - 8.E+0*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm - 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*
      sp[39]*ssp*svm + 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(
      sp[35],2)*sp[31]*pow(sp[44],2)*ssp*svm - 8.E+0*pow(sp[35],2)*sp[29]*pow(sp[44],2)
      *svp*ssm;
 
    kern[5][7] = (1.0/ denom)* kern[5][7]; 

    denom =  - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44] + 8.E+0*pow(sp[35],2)*sp[43]*pow(
      sp[44],2) + 8.E+0*pow(sp[43],5);

    kern[6][0] =   - 2.4E+1*sp[35]*sp[43]*sp[31]*sp[29]*sp[44]*svp*svm + 1.6E+1*sp[35]*
      pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[29]*sp[39]*
      svp*svm - 8.E+0*sp[35]*pow(sp[43],3)*sp[7]*svp*svm + 8.E+0*pow(sp[35],2)*
      sp[43]*sp[44]*sp[7]*svp*svm - 2.4E+1*pow(sp[35],2)*sp[43]*sp[39]*sp[37]*svp*svm
       + 8.E+0*pow(sp[35],2)*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*pow(sp[35],2)*sp[29]
      *sp[44]*sp[39]*svp*svm;
 
    kern[6][0] = (1.0/ denom)* kern[6][0]; 
    kern[6][1] =   0;
 
    kern[6][1] = (1.0/ denom)* kern[6][1]; 
    kern[6][2] =   - 8.E+0*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm + 8.E+0*sp[35]*sp[43]*
      sp[29]*sp[44]*svp*ssm - 8.E+0*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm + 8.E+0*sp[35]*
      pow(sp[43],2)*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*sp[44]*sp[39]*ssp*svm - 
      8.E+0*pow(sp[35],2)*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(sp[43],3)*sp[31]*ssp*svm
       - 8.E+0*pow(sp[43],3)*sp[29]*svp*ssm;
 
    kern[6][2] = (1.0/ denom)* kern[6][2]; 
    kern[6][3] =   - 8.E+0*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm + 8.E+0*sp[35]*
      sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 8.E+0*pow(sp[43],3)*sp[31]*sp[37]*svp*svm - 
      8.E+0*pow(sp[43],3)*sp[29]*sp[39]*svp*svm;
 
    kern[6][3] = (1.0/ denom)* kern[6][3]; 
    kern[6][4] =  8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm + 8.E+0*sp[35]*
      pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 8.E+0*sp[35]*pow(sp[43],3)*sp[39]*ssp*svm
       - 8.E+0*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*
      sp[39]*ssp*svm + 8.E+0*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm - 8.E+0*pow(
      sp[35],2)*sp[31]*pow(sp[44],2)*ssp*svm - 8.E+0*pow(sp[35],2)*sp[29]*pow(sp[44],2)
      *svp*ssm;
 
    kern[6][4] = (1.0/ denom)* kern[6][4]; 
    kern[6][5] =   - 8.E+0*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm - 8.E+0
      *sp[35]*pow(sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],3)*
      sp[39]*sp[37]*svp*svm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[39]*sp[37]*svp*svm
       + 8.E+0*pow(sp[35],2)*sp[31]*pow(sp[44],2)*sp[37]*svp*svm + 8.E+0*pow(
      sp[35],2)*sp[29]*pow(sp[44],2)*sp[39]*svp*svm;
 
    kern[6][5] = (1.0/ denom)* kern[6][5]; 
    kern[6][6] =   - 1.6E+1*sp[35]*sp[43]*sp[31]*sp[29]*pow(sp[44],2)*svp*svm + 
      1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*sp[37]*svp*svm + 1.6E+1*sp[35]*pow(
      sp[43],2)*sp[29]*sp[44]*sp[39]*svp*svm - 8.E+0*sp[35]*pow(sp[43],3)*sp[44]*sp[7]*svp
      *svm - 1.6E+1*sp[35]*pow(sp[43],3)*sp[44]*ssp*ssm - 2.4E+1*sp[35]*pow(sp[43],3)*
      sp[39]*sp[37]*svp*svm + 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[44],2)*sp[7]*svp*svm
       + 8.E+0*pow(sp[35],2)*sp[43]*pow(sp[44],2)*ssp*ssm - 8.E+0*pow(sp[43],3)*
      sp[31]*sp[29]*sp[44]*svp*svm + 8.E+0*pow(sp[43],4)*sp[31]*sp[37]*svp*svm + 
      8.E+0*pow(sp[43],4)*sp[29]*sp[39]*svp*svm + 8.E+0*pow(sp[43],5)*ssp*ssm;
 
    kern[6][6] = (1.0/ denom)* kern[6][6]; 
    kern[6][7] =   - 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]*sp[39]*ssp*svm + 1.6E+1*
      sp[35]*pow(sp[43],2)*sp[44]*sp[37]*svp*ssm + 8.E+0*pow(sp[35],2)*pow(sp[44],2)*
      sp[39]*ssp*svm - 8.E+0*pow(sp[35],2)*pow(sp[44],2)*sp[37]*svp*ssm + 8.E+0*
      pow(sp[43],4)*sp[39]*ssp*svm - 8.E+0*pow(sp[43],4)*sp[37]*svp*ssm;
 
    kern[6][7] = (1.0/ denom)* kern[6][7]; 

    denom =  - 3.2E+1*sp[35]*pow(sp[43],2)*sp[44] + 1.6E+1*pow(sp[35],2)*pow(sp[44],2)
       + 1.6E+1*pow(sp[43],4);

    kern[7][0] =   0;
 
    kern[7][0] = (1.0/ denom)* kern[7][0]; 
    kern[7][1] =  4.8E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[29]*sp[44]*svp*svm - 3.2E+1*
      sp[35]*pow(sp[43],3)*sp[31]*sp[37]*svp*svm - 3.2E+1*sp[35]*pow(sp[43],3)*sp[29]*
      sp[39]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],4)*sp[7]*svp*svm - 1.6E+1*pow(
      sp[35],2)*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[29]*sp[44]
      *sp[39]*svp*svm - 1.6E+1*pow(sp[35],2)*pow(sp[43],2)*sp[44]*sp[7]*svp*svm + 
      4.8E+1*pow(sp[35],2)*pow(sp[43],2)*sp[39]*sp[37]*svp*svm;
 
    kern[7][1] = (1.0/ denom)* kern[7][1]; 
    kern[7][2] =   - 4.8E+1*sp[35]*sp[43]*sp[31]*sp[37]*svp*svm - 4.8E+1*sp[35]*sp[43]*
      sp[29]*sp[39]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[7]*svp*svm + 1.6E+1*
      sp[35]*sp[31]*sp[29]*sp[44]*svp*svm - 1.6E+1*pow(sp[35],2)*sp[44]*sp[7]*svp*svm
       + 4.8E+1*pow(sp[35],2)*sp[39]*sp[37]*svp*svm + 3.2E+1*pow(sp[43],2)*sp[31]*
      sp[29]*svp*svm;
 
    kern[7][2] = (1.0/ denom)* kern[7][2]; 
    kern[7][3] =  1.6E+1*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm + 1.6E+1*sp[35]*sp[43]*sp[29]
      *sp[44]*svp*ssm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[39]*ssp*svm + 1.6E+1*sp[35]*
      pow(sp[43],2)*sp[37]*svp*ssm - 1.6E+1*pow(sp[35],2)*sp[44]*sp[39]*ssp*svm - 
      1.6E+1*pow(sp[35],2)*sp[44]*sp[37]*svp*ssm - 1.6E+1*pow(sp[43],3)*sp[31]*ssp*
      svm - 1.6E+1*pow(sp[43],3)*sp[29]*svp*ssm;
 
    kern[7][3] = (1.0/ denom)* kern[7][3]; 
    kern[7][4] =  1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[37]*svp*svm - 1.6E+1*sp[35]*
      pow(sp[43],2)*sp[29]*sp[39]*svp*svm - 1.6E+1*pow(sp[35],2)*sp[31]*sp[44]*sp[37]*
      svp*svm + 1.6E+1*pow(sp[35],2)*sp[29]*sp[44]*sp[39]*svp*svm;
 
    kern[7][4] = (1.0/ denom)* kern[7][4]; 
    kern[7][5] =  1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm - 1.6E+1*sp[35]*
      pow(sp[43],2)*sp[29]*sp[44]*svp*ssm - 1.6E+1*sp[35]*pow(sp[43],3)*sp[39]*ssp*svm
       + 1.6E+1*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm + 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]
      *sp[39]*ssp*svm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm - 1.6E+1*
      pow(sp[35],2)*sp[31]*pow(sp[44],2)*ssp*svm + 1.6E+1*pow(sp[35],2)*sp[29]*pow(
      sp[44],2)*svp*ssm;
 
    kern[7][5] = (1.0/ denom)* kern[7][5]; 
    kern[7][6] =  1.6E+1*sp[35]*pow(sp[43],2)*sp[31]*sp[44]*ssp*svm - 1.6E+1*sp[35]*
      pow(sp[43],2)*sp[29]*sp[44]*svp*ssm + 1.6E+1*sp[35]*pow(sp[43],3)*sp[39]*ssp*svm
       - 1.6E+1*sp[35]*pow(sp[43],3)*sp[37]*svp*ssm - 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]
      *sp[39]*ssp*svm + 1.6E+1*pow(sp[35],2)*sp[43]*sp[44]*sp[37]*svp*ssm - 1.6E+1*
      pow(sp[43],4)*sp[31]*ssp*svm + 1.6E+1*pow(sp[43],4)*sp[29]*svp*ssm;
 
    kern[7][6] = (1.0/ denom)* kern[7][6]; 
    kern[7][7] =   - 4.8E+1*sp[35]*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm - 4.8E+1*sp[35]*
      sp[43]*sp[29]*sp[44]*sp[39]*svp*svm + 1.6E+1*sp[35]*pow(sp[43],2)*sp[44]*sp[7]*svp*
      svm - 3.2E+1*sp[35]*pow(sp[43],2)*sp[44]*ssp*ssm + 1.6E+1*sp[35]*pow(sp[43],2)*
      sp[39]*sp[37]*svp*svm + 3.2E+1*sp[35]*sp[31]*sp[29]*pow(sp[44],2)*svp*svm + 
      3.2E+1*pow(sp[35],2)*sp[44]*sp[39]*sp[37]*svp*svm - 1.6E+1*pow(sp[35],2)*pow(
      sp[44],2)*sp[7]*svp*svm + 1.6E+1*pow(sp[35],2)*pow(sp[44],2)*ssp*ssm + 
      1.6E+1*pow(sp[43],2)*sp[31]*sp[29]*sp[44]*svp*svm + 1.6E+1*pow(sp[43],4)*ssp*
      ssm;
 
    kern[7][7] = (1.0/ denom)* kern[7][7]; 


}


void kernelL_qphv_trans(matCdoub& kern,
              const ArrayScalarProducts& sp,
              const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){


    // local variables
    array<Cdoub, 76> w;
    Cdoub denom;


    w[1]=sp[35];
    w[2]=sp[34];
    w[3]=sp[43];
    w[4]=sp[27];
    w[5]=sp[44];
   w[6]=2.E+0*ang[1];
   w[7]=w[5]*ang[0];
   w[8]=w[6] + w[7];
   w[9]=w[4]*ang[0];
   w[10]= - w[9] + 2.E+0*w[8];
   w[10]=w[10]*w[4];
   w[11]=w[6] - w[7];
   w[12]=w[11]*w[5];
   w[12]=w[12] - ang[2];
   w[13]=w[10] - w[12];
   w[14]=pow(w[2],2);
   w[13]=w[13]*w[14];
   w[15]=pow(w[4],2);
   w[16]=w[3]*ang[0];
   w[17]=w[15]*w[16];
   w[18]=2.E+0*w[4];
   w[19]=w[18]*w[2];
   w[20]=w[7] - ang[1];
   w[21]= - w[20]*w[19];
   w[21]=w[21] + w[17];
   w[22]=2.E+0*w[3];
   w[21]=w[21]*w[22];
   w[13]=w[13] + w[21];
   w[21]=3.E+0*ang[1];
   w[23]= - w[21] - w[7];
   w[23]=2.E+0*w[23] + w[9];
   w[23]=w[4]*w[23];
   w[23]=w[23] - w[12];
   w[24]=w[1]*w[4];
   w[23]=w[23]*w[24];
   w[13]=2.E+0*w[13] + w[23];
   w[13]=w[1]*w[13];
   w[23]=w[14]*w[3];
   w[25]=2.E+0*w[9];
   w[26]=w[23]*w[25];
   w[27]=3.E+0*w[9];
   w[28]= - w[27] - w[20];
   w[29]=pow(w[2],3);
   w[28]=w[28]*w[29];
   w[28]=w[28] + w[26];
   w[28]=w[3]*w[28];
   w[11]=w[11] - w[9];
   w[30]=pow(w[2],4);
   w[31]= - w[11]*w[30];
   w[28]=w[31] + w[28];
   w[13]=4.E+0*w[28] + w[13];
   w[28]=2.E+0*w[1];
   w[13]=w[13]*w[28];
   w[31]=8.E+0*ang[1];
   w[32]=3.E+0*w[7];
   w[33]=w[31] + w[32];
   w[33]=w[5]*w[33];
   w[34]=4.E+0*ang[1];
   w[35]=w[34] - w[32];
   w[36]= - w[9] + w[35];
   w[36]=w[4]*w[36];
   w[37]=5.E+0*ang[2];
   w[33]=w[36] - w[37] + w[33];
   w[33]=w[4]*w[33];
   w[36]=3.E+0*ang[2];
   w[38]=pow(w[5],2);
   w[39]=w[38]*ang[0];
   w[40]=w[36] - w[39];
   w[40]=w[40]*w[5];
   w[40]=w[40] - 2.E+0*ang[3];
   w[33]=w[33] - w[40];
   w[33]=w[33]*w[14];
   w[41]=w[21] - w[9];
   w[42]=w[41]*w[4];
   w[43]=2.E+0*ang[2];
   w[44]=w[7] + ang[1];
   w[45]=w[44]*w[5];
   w[46]=w[43] - w[45];
   w[42]=w[42] - w[46];
   w[19]= - w[42]*w[19];
   w[47]=w[6] - w[9];
   w[48]=w[47] + w[7];
   w[49]=w[15]*w[3];
   w[50]=w[48]*w[49];
   w[19]=w[19] + w[50];
   w[19]=w[19]*w[22];
   w[19]=w[33] + w[19];
   w[33]=w[34] + w[32];
   w[50]=w[33]*w[5];
   w[33]= - w[9] + w[33];
   w[33]=w[4]*w[33];
   w[33]=w[33] - w[37] - w[50];
   w[33]=w[4]*w[33];
   w[33]=w[33] - w[40];
   w[33]=w[33]*w[24];
   w[19]=2.E+0*w[19] + w[33];
   w[19]=w[1]*w[19];
   w[33]= - w[42]*w[29];
   w[51]=w[3]*w[4];
   w[52]=w[48]*w[51];
   w[53]=2.E+0*w[14];
   w[54]=w[53]*w[52];
   w[33]=w[33] + w[54];
   w[54]=4.E+0*w[3];
   w[33]=w[33]*w[54];
   w[19]=w[33] + w[19];
   w[19]=w[1]*w[19];
   w[33]= - w[21] + w[7];
   w[33]=w[5]*w[33];
   w[55]=4.E+0*w[7];
   w[41]=w[55] - w[41];
   w[41]=w[4]*w[41];
   w[33]=w[41] + w[43] + w[33];
   w[33]=w[33]*w[29];
   w[41]=w[3]*w[2];
   w[43]=3.E+0*ang[0];
   w[56]=w[15]*w[43]*w[41];
   w[35]= - w[27] + w[35];
   w[35]=w[4]*w[35]*w[14];
   w[35]=w[35] + w[56];
   w[35]=w[35]*w[22];
   w[33]=w[33] + w[35];
   w[33]=w[33]*w[22];
   w[35]=2.E+0*w[7];
   w[56]=w[35] - w[9];
   w[56]=w[56]*w[4];
   w[56]=w[56] - w[39] + ang[2];
   w[57]=w[2]*w[4];
   w[58]= - w[56]*w[57];
   w[59]= - w[34]*w[49];
   w[58]=w[58] + w[59];
   w[59]=w[1]*w[3];
   w[58]=w[58]*w[59];
   w[33]=w[33] + w[58];
   w[33]=w[33]*w[28];
   w[58]= - w[25] + w[21] + w[35];
   w[58]=w[4]*w[58];
   w[21]=w[21] - w[35];
   w[60]=w[5]*w[21];
   w[58]=w[58] - ang[2] + w[60];
   w[58]=w[58]*w[14];
   w[60]=w[11]*w[4];
   w[61]= - w[2]*w[60];
   w[17]=w[61] - w[17];
   w[17]=w[17]*w[22];
   w[17]=w[58] + w[17];
   w[17]=w[17]*w[22];
   w[56]= - w[1]*w[56]*w[51];
   w[17]=w[17] + w[56];
   w[17]=w[1]*w[17];
   w[56]=w[25] + w[35] - ang[1];
   w[58]=w[56]*w[29];
   w[26]=w[58] - w[26];
   w[26]=w[3]*w[26];
   w[58]= - w[30]*w[35];
   w[26]=w[58] + w[26];
   w[26]=w[26]*w[54];
   w[17]=w[26] + w[17];
   w[26]=4.E+0*w[1];
   w[17]=w[17]*w[26];
   w[44]=w[9] + 2.E+0*w[44];
   w[44]=w[44]*w[4];
   w[58]=3.E+0*w[12];
   w[44]=w[44] + w[58];
   w[44]=w[44]*w[2];
   w[61]=w[20] - w[9];
   w[62]=4.E+0*w[51];
   w[61]=w[61]*w[62];
   w[44]=w[44] + w[61];
   w[61]=w[44]*w[1];
   w[63]=w[25] + w[20];
   w[63]=w[63]*w[53];
   w[64]=w[41]*w[27];
   w[63]=w[63] - w[64];
   w[63]=w[63]*w[3];
   w[64]=w[9] - ang[1];
   w[65]=w[64] + w[35];
   w[65]=w[65]*w[29];
   w[63]=w[63] - w[65];
   w[61]=w[61] + 4.E+0*w[63];
   w[65]=w[61]*w[26];
   w[66]=w[9] + w[7];
   w[67]=3.E+0*w[2];
   w[68]= - w[66]*w[51]*w[67];
   w[55]=w[55] + w[64];
   w[55]=w[4]*w[55];
   w[69]=w[20]*w[5];
   w[55]=w[69] + w[55];
   w[55]=w[55]*w[53];
   w[55]=w[55] + w[68];
   w[55]=w[3]*w[55];
   w[68]=w[32] - w[6];
   w[70]= - w[5]*w[68];
   w[71]= - w[4]*w[32];
   w[70]=w[70] + w[71];
   w[70]=w[70]*w[29];
   w[55]=w[70] + w[55];
   w[70]=w[4]*w[48];
   w[50]=3.E+0*w[70] - w[36] + w[50];
   w[50]=w[4]*w[50];
   w[70]=w[58]*w[5];
   w[50]=w[70] + w[50];
   w[50]=w[2]*w[50];
   w[71]= - w[35] + w[64];
   w[71]=w[4]*w[71];
   w[71]=w[69] + w[71];
   w[71]=w[71]*w[62];
   w[50]=w[50] + w[71];
   w[50]=w[1]*w[50];
   w[50]=4.E+0*w[55] + w[50];
   w[50]=w[50]*w[28];
   w[55]= - w[4]*w[64];
   w[55]=w[69] + w[55];
   w[55]=w[2]*w[55];
   w[64]=w[9] - w[7];
   w[71]=w[64]*w[51];
   w[55]=w[55] + 2.E+0*w[71];
   w[55]=8.E+0*w[55]*w[59];
   w[71]= - w[14]*w[9];
   w[72]=w[9]*w[3];
   w[73]=w[2]*w[25];
   w[73]=w[73] - w[72];
   w[73]=w[3]*w[73];
   w[71]=w[71] + w[73];
   w[73]= - 2.E+0*w[20] + w[9];
   w[73]=w[4]*w[73];
   w[73]=w[73] - w[12];
   w[73]=w[1]*w[73];
   w[71]=4.E+0*w[71] + w[73];
   w[71]=4.E+0*w[71];
   w[73]=w[7]*w[4];
   w[74]=w[39] - w[73];
   w[74]=w[74]*w[14];
   w[75]=w[4]*w[64]*pow(w[3],2);
   w[74]=w[74] + w[75];
   w[38]= - w[38]*w[43];
   w[43]=w[32] + w[47];
   w[43]=w[4]*w[43];
   w[38]=w[43] - ang[2] + w[38];
   w[38]=w[4]*w[38];
   w[12]= - w[5]*w[12];
   w[12]=w[12] + w[38];
   w[12]=w[1]*w[12];
   w[12]=4.E+0*w[74] + w[12];
   w[12]=2.E+0*w[12];
   w[38]=w[14]*w[7];
   w[43]=w[2]*w[11];
   w[43]=w[43] + w[72];
   w[43]=w[3]*w[43];
   w[38]=w[38] + w[43];
   w[43]=w[34] - w[7];
   w[74]=w[43]*w[5];
   w[10]= - w[10] + w[36] - w[74];
   w[10]=w[1]*w[10];
   w[10]=4.E+0*w[38] + w[10];
   w[10]=1.2E+1*w[10];
   w[6]=w[6]*w[41];
   w[36]=w[4] + w[5];
   w[36]=w[36]*ang[1];
   w[36]=w[36] - ang[2];
   w[38]= - w[1]*w[36];
   w[6]=w[6] + w[38];
   w[6]=6.E+0*w[1]*w[6];
   w[38]=w[32] - ang[1];
   w[38]= - w[27] + 2.E+0*w[38];
   w[38]=w[38]*w[4];
   w[38]=w[38] + w[58];
   w[38]=w[38]*w[1];
   w[20]= - w[9] + 3.E+0*w[20];
   w[20]=w[20]*w[2];
   w[20]=w[20] - w[72];
   w[20]=w[20]*w[3];
   w[25]=w[68] - w[25];
   w[25]=w[25]*w[14];
   w[20]=w[20] - w[25];
   w[20]=w[38] + 4.E+0*w[20];
   w[20]=w[20]*w[1];
   w[16]=w[16]*w[14];
   w[25]=2.E+0*ang[0];
   w[38]=w[29]*w[25];
   w[16]=w[16] - w[38];
   w[16]=w[16]*w[3];
   w[38]=w[30]*ang[0];
   w[16]=w[16] + w[38];
   w[16]=w[20] - 8.E+0*w[16];
   w[20]=w[16]*w[28];
   w[9]= - w[9] + 7.E+0*ang[1] - w[35];
   w[9]=w[4]*w[9];
   w[9]= - 3.E+0*w[46] + w[9];
   w[9]=w[2]*w[9];
   w[9]=w[9] - w[52];
   w[9]=w[3]*w[9];
   w[38]=w[60] - ang[2];
   w[41]= - 2.E+0*w[45] - w[38];
   w[41]=w[41]*w[14];
   w[9]=w[41] + w[9];
   w[41]= - w[34] + 5.E+0*w[7];
   w[41]=w[5]*w[41];
   w[34]= - w[34] - w[66];
   w[34]=w[4]*w[34];
   w[34]=w[34] + 1.1E+1*ang[2] + w[41];
   w[34]=w[4]*w[34];
   w[40]=3.E+0*w[40];
   w[34]=w[40] + w[34];
   w[34]=w[1]*w[34];
   w[9]=4.E+0*w[9] + w[34];
   w[9]=w[1]*w[9];
   w[34]=ang[1] - w[64];
   w[34]=w[34]*w[29];
   w[41]= - w[48]*w[23];
   w[34]=w[34] + w[41];
   w[34]=w[3]*w[34];
   w[9]=8.E+0*w[34] + w[9];
   w[9]=w[1]*w[9];
   w[34]=w[44]*w[59];
   w[41]=w[63]*w[54];
   w[34]=w[34] + w[41];
   w[41]=w[34]*w[28];
   w[44]=w[61]*w[28];
   w[45]=w[27] - 1.2E+1*ang[1] - w[7];
   w[45]=w[4]*w[45];
   w[45]=w[45] + 1.5E+1*ang[2] - w[74];
   w[45]=w[4]*w[45];
   w[40]=w[40] + w[45];
   w[40]=w[2]*w[40];
   w[45]=w[42]*w[62];
   w[40]=w[40] + w[45];
   w[40]=w[1]*w[40];
   w[42]=w[42]*w[53];
   w[45]= - w[52]*w[67];
   w[42]=w[42] + w[45];
   w[42]=w[3]*w[42];
   w[8]= - w[5]*w[8];
   w[8]=w[8] + w[73];
   w[8]=w[8]*w[29];
   w[8]=w[8] + w[42];
   w[8]=4.E+0*w[8] + w[40];
   w[8]=w[1]*w[8];
   w[31]=w[31] - w[32];
   w[31]=w[5]*w[31];
   w[32]=2.E+0*w[43] - w[27];
   w[32]=w[4]*w[32];
   w[31]=w[32] - w[37] + w[31];
   w[31]=w[31]*w[14];
   w[32]=w[56]*w[57];
   w[25]= - w[25]*w[49];
   w[25]=w[32] + w[25];
   w[25]=w[25]*w[54];
   w[25]=w[31] + w[25];
   w[25]=w[3]*w[25];
   w[28]= - w[36]*w[51]*w[28];
   w[25]=w[25] + w[28];
   w[25]=w[1]*w[25];
   w[11]= - w[11]*w[29];
   w[28]= - w[14]*w[72];
   w[11]=w[11] + w[28];
   w[11]=w[3]*w[11];
   w[28]=w[30]*w[7];
   w[11]= - w[28] + w[11];
   w[11]=w[11]*w[54];
   w[11]=w[11] + w[25];
   w[11]=2.E+0*w[11];
   w[25]= - w[34]*w[26];
   w[16]= - 4.E+0*w[16];
   w[21]= - w[27] + w[21];
   w[21]=w[4]*w[21];
   w[21]= - 3.E+0*w[69] + w[21];
   w[21]=w[2]*w[21];
   w[27]=w[7] + w[27];
   w[27]=w[27]*w[51];
   w[21]=w[21] + w[27];
   w[21]=w[21]*w[22];
   w[22]= - w[4]*w[47];
   w[27]= - 6.E+0*ang[1] + 7.E+0*w[7];
   w[27]=w[5]*w[27];
   w[22]=w[22] + ang[2] + w[27];
   w[22]=w[22]*w[14];
   w[21]=w[22] + w[21];
   w[22]= - 5.E+0*w[39] - w[38];
   w[22]=w[4]*w[22];
   w[22]= - w[70] + w[22];
   w[22]=w[1]*w[22];
   w[21]=2.E+0*w[21] + w[22];
   w[21]=w[1]*w[21];
   w[22]= - w[29]*w[35];
   w[7]=w[7]*w[23];
   w[7]=w[22] + w[7];
   w[7]=w[3]*w[7];
   w[7]=w[28] + w[7];
   w[7]=8.E+0*w[7] + w[21];
   w[7]=2.E+0*w[7];
   w[21]=w[18]*w[14];
   w[15]=w[15]*w[1];
   w[21]=w[21] - w[15];
   w[21]=w[21]*w[1];
   w[21]=w[21] - w[30];
   w[22]=w[21]*w[1];
   w[22]=8.E+0*w[22];
   w[18]=w[29]*w[18];
   w[15]=w[15]*w[2];
   w[15]=w[18] - w[15];
   w[15]=w[15]*w[1];
   w[18]=pow(w[2],5);
   w[15]=w[15] - w[18];
   w[18]= - 1.6E+1*w[1]*w[15];
   w[14]=w[24] - w[14];
   w[23]= - 1.6E+1*w[14];
   w[24]= - 4.8E+1*w[14];
   w[14]= - w[14]*w[26];
   w[15]= - 8.E+0*w[15];
   w[21]= - 1.6E+1*w[21];


    denom =  - w[22];

    kern[0][0] = (1.0/ denom)* w[13];
 
    kern[0][1] = (1.0/ denom)*0.0;
 
    kern[0][2] = (1.0/ denom)*0.0;
 
    kern[0][3] = (1.0/ denom)*0.0;
 
    kern[0][4] = (1.0/ denom)*0.0;
 
    kern[0][5] = (1.0/ denom)* w[19];
 
    kern[0][6] = (1.0/ denom)* w[33];
 
    kern[0][7] = (1.0/ denom)*0.0;
 

    denom = w[18];

    kern[1][0] = (1.0/ denom)*0.0;
 
    kern[1][1] = (1.0/ denom)* w[17];
 
    kern[1][2] = (1.0/ denom)* w[65];
 
    kern[1][3] = (1.0/ denom)*0.0;
 
    kern[1][4] = (1.0/ denom)*0.0;
 
    kern[1][5] = (1.0/ denom)*0.0;
 
    kern[1][6] = (1.0/ denom)*0.0;
 
    kern[1][7] = (1.0/ denom)* w[50];
 

    denom = w[23];

    kern[2][0] = (1.0/ denom)*0.0;
 
    kern[2][1] = (1.0/ denom)* w[55];
 
    kern[2][2] = (1.0/ denom)* w[71];
 
    kern[2][3] = (1.0/ denom)*0.0;
 
    kern[2][4] = (1.0/ denom)*0.0;
 
    kern[2][5] = (1.0/ denom)*0.0;
 
    kern[2][6] = (1.0/ denom)*0.0;
 
    kern[2][7] = (1.0/ denom)* w[12];
 

    denom = w[24];

    kern[3][0] = (1.0/ denom)*0.0;
 
    kern[3][1] = (1.0/ denom)*0.0;
 
    kern[3][2] = (1.0/ denom)*0.0;
 
    kern[3][3] = (1.0/ denom)* w[10];
 
    kern[3][4] = (1.0/ denom)*0.0;
 
    kern[3][5] = (1.0/ denom)*0.0;
 
    kern[3][6] = (1.0/ denom)*0.0;
 
    kern[3][7] = (1.0/ denom)*0.0;
 

    denom = w[14];

    kern[4][0] = (1.0/ denom)*0.0;
 
    kern[4][1] = (1.0/ denom)*0.0;
 
    kern[4][2] = (1.0/ denom)*0.0;
 
    kern[4][3] = (1.0/ denom)*0.0;
 
    kern[4][4] = (1.0/ denom)* w[6];
 
    kern[4][5] = (1.0/ denom)*0.0;
 
    kern[4][6] = (1.0/ denom)*0.0;
 
    kern[4][7] = (1.0/ denom)*0.0;
 

    denom =  - w[22];

    kern[5][0] = (1.0/ denom)* w[20];
 
    kern[5][1] = (1.0/ denom)*0.0;
 
    kern[5][2] = (1.0/ denom)*0.0;
 
    kern[5][3] = (1.0/ denom)*0.0;
 
    kern[5][4] = (1.0/ denom)*0.0;
 
    kern[5][5] = (1.0/ denom)* w[9];
 
    kern[5][6] = (1.0/ denom)* w[41];
 
    kern[5][7] = (1.0/ denom)*0.0;
 

    denom = w[15];

    kern[6][0] = (1.0/ denom)* w[44];
 
    kern[6][1] = (1.0/ denom)*0.0;
 
    kern[6][2] = (1.0/ denom)*0.0;
 
    kern[6][3] = (1.0/ denom)*0.0;
 
    kern[6][4] = (1.0/ denom)*0.0;
 
    kern[6][5] = (1.0/ denom)* w[8];
 
    kern[6][6] = (1.0/ denom)* w[11];
 
    kern[6][7] = (1.0/ denom)*0.0;
 

    denom = w[21];

    kern[7][0] = (1.0/ denom)*0.0;
 
    kern[7][1] = (1.0/ denom)* w[25];
 
    kern[7][2] = (1.0/ denom)* w[16];
 
    kern[7][3] = (1.0/ denom)*0.0;
 
    kern[7][4] = (1.0/ denom)*0.0;
 
    kern[7][5] = (1.0/ denom)*0.0;
 
    kern[7][6] = (1.0/ denom)*0.0;
 
    kern[7][7] = (1.0/ denom)* w[7];
 

}


void kernelY_qphv_trans(matCdoub& kern,
  const ArrayScalarProducts& sp,  const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


    // local variables
    array<Cdoub, 98> w;
    Cdoub denom;


    w[1]=sp[35];
    w[2]=sp[43];
    w[3]=sp[31];
    w[4]=sp[29];
    w[5]=sp[44];
    w[6]=sp[37];
    w[7]=sp[39];
    w[8]=sp[7];
   w[9]=ssm*ssp;
   w[10]=svm*svp;
   w[11]=w[10]*w[8];
   w[12]=w[9] + w[11];
   w[13]=w[2]*w[1];
   w[14]=w[12]*w[13];
   w[15]=w[10]*w[7];
   w[16]=w[15]*w[4];
   w[10]=w[10]*w[3];
   w[17]=w[6]*w[10];
   w[18]=w[16] + w[17];
   w[19]=w[18]*w[1];
   w[14]=w[14] - w[19];
   w[14]=w[14]*w[2];
   w[15]=w[15]*w[6];
   w[20]=pow(w[1],2);
   w[21]=w[15]*w[20];
   w[22]=2.E+0*w[21];
   w[23]=2.E+0*w[9];
   w[24]=w[23] + w[11];
   w[24]=w[24]*w[1];
   w[10]=w[4]*w[10];
   w[25]=2.E+0*w[10];
   w[26]=w[25] - w[24];
   w[27]=w[5]*w[1];
   w[26]=w[26]*w[27];
   w[26]=w[14] + w[22] + w[26];
   w[26]=w[2]*w[26];
   w[28]=w[20]*w[5];
   w[29]=w[18]*w[28];
   w[26]= - 2.E+0*w[29] + w[26];
   w[26]=w[2]*w[26];
   w[30]=pow(w[1],3);
   w[31]=w[30]*w[15];
   w[32]=w[9]*w[1];
   w[33]=w[32] + w[10];
   w[34]=w[33]*w[28];
   w[34]=w[31] + w[34];
   w[34]=w[5]*w[34];
   w[26]=w[34] + w[26];
   w[26]=8.E+0*w[26];
   w[34]=pow(w[5],2);
   w[35]=w[34]*w[30];
   w[36]=svm*ssp;
   w[37]=w[7]*w[36];
   w[38]=ssm*svp;
   w[39]=w[6]*w[38];
   w[40]=w[37] - w[39];
   w[41]=w[40]*w[35];
   w[42]=w[40]*w[28];
   w[43]=pow(w[2],2);
   w[44]=w[43]*w[1];
   w[45]=w[40]*w[44];
   w[45]= - 2.E+0*w[42] + w[45];
   w[45]=w[45]*w[43];
   w[41]=w[41] + w[45];
   w[45]=8.E+0*w[2];
   w[41]=w[41]*w[45];
   w[46]=w[40]*w[13];
   w[36]=w[3]*w[36];
   w[38]=w[4]*w[38];
   w[47]=w[36] - w[38];
   w[48]=w[47]*w[27];
   w[46]=w[46] - w[48];
   w[49]=w[46]*w[2];
   w[49]=w[49] - w[42];
   w[49]=w[49]*w[2];
   w[50]=pow(w[27],2);
   w[51]=w[50]*w[47];
   w[49]=w[49] + w[51];
   w[51]=8.E+0*w[49];
   w[16]=w[16] - w[17];
   w[17]= - w[16]*w[50];
   w[52]=w[16]*w[43];
   w[53]=w[27]*w[52];
   w[17]=w[17] + w[53];
   w[17]=8.E+0*w[17];
   w[37]=w[37] + w[39];
   w[39]=w[37]*w[13];
   w[36]=w[36] + w[38];
   w[38]=w[36]*w[27];
   w[39]=w[39] - w[38];
   w[39]=w[39]*w[2];
   w[53]=w[37]*w[28];
   w[54]=w[39] - w[53];
   w[54]=w[54]*w[2];
   w[55]=w[50]*w[36];
   w[54]=w[54] + w[55];
   w[55]=w[54]*w[45];
   w[56]=2.E+0*w[15];
   w[57]=w[56]*w[13];
   w[58]=w[18]*w[27];
   w[57]=w[57] - w[58];
   w[59]=w[57]*w[2];
   w[60]=w[56]*w[28];
   w[59]=w[60] - w[59];
   w[59]=w[59]*w[2];
   w[60]=w[50]*w[18];
   w[59]=w[59] - w[60];
   w[60]=w[59]*w[45];
   w[61]=w[11]*w[27];
   w[62]=w[56]*w[1];
   w[61]=w[61] + w[62];
   w[61]=w[61]*w[2];
   w[62]=3.E+0*w[58];
   w[61]=w[61] - w[62];
   w[61]=w[61]*w[2];
   w[63]=w[11]*w[1];
   w[64]= - w[63] + 3.E+0*w[10];
   w[64]=w[64]*w[27];
   w[65]=w[64] + w[21];
   w[65]=w[65]*w[5];
   w[61]=w[61] + w[65];
   w[65]=8.E+0*w[61]*w[43];
   w[66]=w[40]*w[20];
   w[67]=w[47]*w[13];
   w[66]=w[66] - w[67];
   w[66]=w[66]*w[2];
   w[67]=w[47]*w[28];
   w[66]=w[66] + w[67];
   w[66]=w[66]*w[2];
   w[67]=w[30]*w[5];
   w[68]=w[67]*w[40];
   w[66]=w[66] - w[68];
   w[68]=1.6E+1*w[66];
   w[69]=2.E+0*w[19];
   w[70]=w[9] - w[11];
   w[71]=w[70]*w[13];
   w[71]=w[69] + w[71];
   w[71]=w[2]*w[71];
   w[23]=w[23] - w[11];
   w[23]=w[23]*w[1];
   w[72]= - w[25] - w[23];
   w[72]=w[72]*w[27];
   w[71]=w[71] - 4.E+0*w[21] + w[72];
   w[71]=w[2]*w[71];
   w[71]=w[29] + w[71];
   w[71]=w[2]*w[71];
   w[32]=w[32] - w[10];
   w[72]=w[32]*w[28];
   w[72]=w[31] + w[72];
   w[72]=w[5]*w[72];
   w[71]=w[72] + w[71];
   w[72]=1.6E+1*w[2];
   w[71]=w[71]*w[72];
   w[11]=w[13]*w[11];
   w[11]=w[11] - w[69];
   w[11]=w[11]*w[2];
   w[69]=3.E+0*w[21];
   w[11]=w[11] + w[64] + w[69];
   w[11]=w[11]*w[2];
   w[11]=w[11] - w[29];
   w[64]= - 1.6E+1*w[11];
   w[73]= - 1.6E+1*w[54];
   w[74]=w[16]*w[28];
   w[75]=w[44]*w[16];
   w[74]=w[74] - w[75];
   w[76]= - w[74]*w[72];
   w[77]=w[49]*w[72];
   w[61]= - w[61]*w[72];
   w[46]= - 1.6E+1*w[46];
   w[57]=w[57]*w[72];
   w[78]=w[9]*w[43];
   w[15]=w[15]*w[1];
   w[78]=w[78] + w[15];
   w[33]= - w[5]*w[33];
   w[33]=w[33] + w[78];
   w[33]=1.6E+1*w[33];
   w[79]=w[43] - w[27];
   w[80]=1.6E+1*w[79];
   w[81]=w[37]*w[80];
   w[82]=w[5]*w[47];
   w[83]= - w[2]*w[40];
   w[82]=w[82] + w[83];
   w[82]=1.6E+1*w[82]*w[43];
   w[83]= - w[5]*w[18];
   w[84]=w[2]*w[56];
   w[83]=w[83] + w[84];
   w[83]=w[83]*w[72];
   w[84]=w[16]*w[1];
   w[85]=4.8E+1*w[84];
   w[86]=4.8E+1*w[39];
   w[87]=w[37]*w[1];
   w[88]=w[36]*w[2];
   w[87]=w[87] - w[88];
   w[88]=4.8E+1*w[87];
   w[32]= - w[5]*w[32];
   w[9]=w[9]*w[2];
   w[89]=w[9] - w[18];
   w[89]=w[2]*w[89];
   w[32]=w[89] + w[15] + w[32];
   w[32]=4.8E+1*w[32];
   w[89]=4.8E+1*w[52];
   w[90]= - w[5]*w[36];
   w[91]=w[2]*w[37];
   w[90]=w[90] + w[91];
   w[90]=4.8E+1*w[2]*w[90];
   w[91]=w[37]*w[20];
   w[92]=w[36]*w[13];
   w[91]=w[91] - w[92];
   w[92]=4.E+0*w[91];
   w[75]= - 4.E+0*w[75];
   w[84]= - 4.E+0*w[84];
   w[93]=w[44] - w[28];
   w[93]=4.E+0*w[93];
   w[94]=w[70]*w[93];
   w[95]= - w[37]*w[44];
   w[95]=w[53] + w[95];
   w[95]=4.E+0*w[95];
   w[39]=4.E+0*w[39];
   w[16]=w[16]*w[27];
   w[96]= - 4.E+0*w[16];
   w[20]=w[18]*w[20];
   w[97]=w[25] + w[63];
   w[13]= - w[97]*w[13];
   w[13]=3.E+0*w[20] + w[13];
   w[13]=w[2]*w[13];
   w[20]=w[63] - w[10];
   w[63]=w[20]*w[28];
   w[13]=w[13] - 3.E+0*w[31] + w[63];
   w[13]=8.E+0*w[13];
   w[31]=w[66]*w[45];
   w[63]=8.E+0*w[74];
   w[66]= - w[2]*w[91];
   w[36]= - w[36]*w[28];
   w[36]=w[36] + w[66];
   w[36]=w[2]*w[36];
   w[37]=w[37]*w[67];
   w[36]=w[37] + w[36];
   w[36]=8.E+0*w[36];
   w[37]= - w[12]*w[28];
   w[21]=w[21] + w[37];
   w[14]=2.E+0*w[21] + w[14];
   w[14]=w[2]*w[14];
   w[14]=w[29] + w[14];
   w[14]=w[2]*w[14];
   w[21]= - w[30]*w[56];
   w[29]=w[12]*w[67];
   w[21]=w[21] + w[29];
   w[21]=w[5]*w[21];
   w[14]=w[21] + w[14];
   w[14]=8.E+0*w[14];
   w[21]= - w[11]*w[45];
   w[29]= - 8.E+0*w[11];
   w[30]=w[40]*w[1];
   w[37]=w[47]*w[2];
   w[30]=w[30] - w[37];
   w[30]=w[30]*w[2];
   w[30]=w[30] + w[48];
   w[30]=w[30]*w[2];
   w[30]=w[30] - w[42];
   w[37]= - 8.E+0*w[30];
   w[16]=w[16] - w[52];
   w[16]=w[16]*w[45];
   w[42]= - 8.E+0*w[54];
   w[47]= - 8.E+0*w[59];
   w[24]= - w[10] - w[24];
   w[24]=w[5]*w[24];
   w[9]=w[9] + w[18];
   w[9]=w[2]*w[9];
   w[9]=w[9] - 3.E+0*w[15] + w[24];
   w[9]=w[2]*w[9];
   w[9]=2.E+0*w[58] + w[9];
   w[9]=w[2]*w[9];
   w[12]=w[1]*w[12];
   w[12]= - w[25] + w[12];
   w[12]=w[1]*w[12]*w[34];
   w[9]=w[12] + w[9];
   w[9]=w[9]*w[45];
   w[12]= - w[43] + 2.E+0*w[27];
   w[12]=w[12]*w[43];
   w[12]=w[12] - w[50];
   w[15]= - 8.E+0*w[40]*w[12];
   w[11]=w[11]*w[72];
   w[18]=w[2]*w[97];
   w[18]= - 3.E+0*w[19] + w[18];
   w[18]=w[2]*w[18];
   w[19]= - w[20]*w[27];
   w[18]=w[18] + w[69] + w[19];
   w[18]=1.6E+1*w[18];
   w[19]=w[2]*w[87];
   w[19]=w[38] + w[19];
   w[19]=w[2]*w[19];
   w[19]= - w[53] + w[19];
   w[19]=1.6E+1*w[19];
   w[20]=1.6E+1*w[74];
   w[24]= - 1.6E+1*w[49];
   w[30]=w[30]*w[72];
   w[34]=w[1]*w[70];
   w[25]=w[25] + w[34];
   w[25]=w[25]*w[27];
   w[22]=w[22] + w[25];
   w[22]=w[5]*w[22];
   w[10]=w[10] - w[23];
   w[10]=w[5]*w[10];
   w[10]=w[10] + w[78];
   w[10]=w[2]*w[10];
   w[10]= - w[62] + w[10];
   w[10]=w[2]*w[10];
   w[10]=w[22] + w[10];
   w[10]=1.6E+1*w[10];
   w[22]= - w[44] + 2.E+0*w[28];
   w[22]=w[22]*w[43];
   w[22]=w[22] - w[35];
   w[23]=8.E+0*w[22];
   w[22]= - w[22]*w[72];
   w[25]=4.8E+1*w[79];
   w[27]= - w[12]*w[45];
   w[12]= - 1.6E+1*w[12];


    denom =  - w[23];

    kern[0][0] = (1.0/ denom)* w[26];
 
    kern[0][1] = (1.0/ denom)* w[41];
 
    kern[0][2] = (1.0/ denom)* w[51];
 
    kern[0][3] = (1.0/ denom)* w[17];
 
    kern[0][4] = (1.0/ denom)* w[55];
 
    kern[0][5] = (1.0/ denom)* w[60];
 
    kern[0][6] = (1.0/ denom)* w[65];
 
    kern[0][7] = (1.0/ denom)*0.0;
 

    denom = w[22];

    kern[1][0] = (1.0/ denom)* w[68];
 
    kern[1][1] = (1.0/ denom)* w[71];
 
    kern[1][2] = (1.0/ denom)* w[64];
 
    kern[1][3] = (1.0/ denom)* w[73];
 
    kern[1][4] = (1.0/ denom)* w[76];
 
    kern[1][5] = (1.0/ denom)* w[77];
 
    kern[1][6] = (1.0/ denom)*0.0;
 
    kern[1][7] = (1.0/ denom)* w[61];
 

    denom = w[80];

    kern[2][0] = (1.0/ denom)* w[46];
 
    kern[2][1] = (1.0/ denom)* w[57];
 
    kern[2][2] = (1.0/ denom)* w[33];
 
    kern[2][3] = (1.0/ denom)* w[81];
 
    kern[2][4] = (1.0/ denom)*0.0;
 
    kern[2][5] = (1.0/ denom)*0.0;
 
    kern[2][6] = (1.0/ denom)* w[82];
 
    kern[2][7] = (1.0/ denom)* w[83];
 

    denom = w[25];

    kern[3][0] = (1.0/ denom)* w[85];
 
    kern[3][1] = (1.0/ denom)* w[86];
 
    kern[3][2] = (1.0/ denom)* w[88];
 
    kern[3][3] = (1.0/ denom)* w[32];
 
    kern[3][4] = (1.0/ denom)*0.0;
 
    kern[3][5] = (1.0/ denom)*0.0;
 
    kern[3][6] = (1.0/ denom)* w[89];
 
    kern[3][7] = (1.0/ denom)* w[90];
 

    denom = w[93];

    kern[4][0] = (1.0/ denom)* w[92];
 
    kern[4][1] = (1.0/ denom)* w[75];
 
    kern[4][2] = (1.0/ denom)* w[84];
 
    kern[4][3] = (1.0/ denom)*0.0;
 
    kern[4][4] = (1.0/ denom)* w[94];
 
    kern[4][5] = (1.0/ denom)* w[95];
 
    kern[4][6] = (1.0/ denom)* w[39];
 
    kern[4][7] = (1.0/ denom)* w[96];
 

    denom =  - w[23];

    kern[5][0] = (1.0/ denom)* w[13];
 
    kern[5][1] = (1.0/ denom)* w[31];
 
    kern[5][2] = (1.0/ denom)*0.0;
 
    kern[5][3] = (1.0/ denom)* w[63];
 
    kern[5][4] = (1.0/ denom)* w[36];
 
    kern[5][5] = (1.0/ denom)* w[14];
 
    kern[5][6] = (1.0/ denom)* w[21];
 
    kern[5][7] = (1.0/ denom)* w[51];
 

    denom = w[27];

    kern[6][0] = (1.0/ denom)* w[29];
 
    kern[6][1] = (1.0/ denom)*0.0;
 
    kern[6][2] = (1.0/ denom)* w[37];
 
    kern[6][3] = (1.0/ denom)* w[16];
 
    kern[6][4] = (1.0/ denom)* w[42];
 
    kern[6][5] = (1.0/ denom)* w[47];
 
    kern[6][6] = (1.0/ denom)* w[9];
 
    kern[6][7] = (1.0/ denom)* w[15];
 

    denom = w[12];

    kern[7][0] = (1.0/ denom)*0.0;
 
    kern[7][1] = (1.0/ denom)* w[11];
 
    kern[7][2] = (1.0/ denom)* w[18];
 
    kern[7][3] = (1.0/ denom)* w[19];
 
    kern[7][4] = (1.0/ denom)* w[20];
 
    kern[7][5] = (1.0/ denom)* w[24];
 
    kern[7][6] = (1.0/ denom)* w[30];
 
    kern[7][7] = (1.0/ denom)* w[10];
 


}

