#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
 
Cdoub calc_fpi_qphv_trans( 
  Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
  Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
  VecCdoub Gamma, const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){
 
 
 Cdoub dummy, fpi=0.0;
 
    dummy =   - 4.E+0*P_P*qm_qp*i_*svp*svm - 1.2E+1*P_P*i_*ssp*ssm - 
      8.E+0*P_qm*P_qp*i_*svp*svm;
 
    fpi += Gamma[0] * dummy; 
 
    dummy =   - 8.E+0*P_P*P_l*l_qm*i_*ssp*svm + 8.E+0*P_P*P_l*l_qp*i_*
      svp*ssm - 4.E+0*pow(P_l,2)*P_qm*i_*ssp*svm + 4.E+0*pow(P_l,2)*
      P_qp*i_*svp*ssm;
 
    fpi += Gamma[1] * dummy; 
 
    dummy =   - 1.2E+1*P_qm*i_*ssp*svm + 1.2E+1*P_qp*i_*svp*ssm;
 
    fpi += Gamma[2] * dummy; 
 
    dummy =   - 8.E+0*P_qm*l_qp*i_*svp*svm + 8.E+0*P_qp*l_qm*i_*svp*svm
      ;
 
    fpi += Gamma[3] * dummy; 
 
    dummy =   - 4.E+0*P_P*l_qm*i_*ssp*svm - 4.E+0*P_P*l_qp*i_*svp*ssm
       + 4.E+0*P_l*P_qm*i_*ssp*svm + 4.E+0*P_l*P_qp*i_*svp*ssm;
 
    fpi += Gamma[4] * dummy; 
 
    dummy =   - 4.E+0*P_P*l_l*qm_qp*i_*svp*svm - 4.E+0*P_P*l_l*i_*ssp*
      ssm + 8.E+0*P_P*l_qm*l_qp*i_*svp*svm - 4.E+0*P_l*P_qm*l_qp*i_*svp
      *svm - 4.E+0*P_l*P_qp*l_qm*i_*svp*svm + 4.E+0*pow(P_l,2)*qm_qp*i_
      *svp*svm + 4.E+0*pow(P_l,2)*i_*ssp*ssm;
 
    fpi += Gamma[5] * dummy; 
 
    dummy =   - 4.E+0*P_l*P_qm*l_qp*i_*svp*svm - 4.E+0*P_l*P_qp*l_qm*i_
      *svp*svm - 4.E+0*pow(P_l,2)*qm_qp*i_*svp*svm - 1.2E+1*pow(P_l,2)*
      i_*ssp*ssm;
 
    fpi += Gamma[6] * dummy; 
 
    dummy =   - 8.E+0*P_l*l_qm*i_*ssp*svm + 8.E+0*P_l*l_qp*i_*svp*ssm
       - 4.E+0*P_qm*l_l*i_*ssp*svm + 4.E+0*P_qp*l_l*i_*svp*ssm;
 
    fpi += Gamma[7] * dummy; 
 

    return fpi;

}

