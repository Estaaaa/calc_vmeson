#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
 
void normalisation_notopti_qphv_trans(matCdoub& kern,
  Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
  Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
  const Cdoub& svp, const Cdoub& ssp, 
  const Cdoub& svm, const Cdoub& ssm ){
 
 
 
 
    kern[0][0] = 8.E+0*P_P*P_qm*P_qp*svp*svm + 4.E+0*pow(P_P,2)*qm_qp*
      svp*svm + 1.2E+1*pow(P_P,2)*ssp*ssm;

    kern[0][1] = 4.E+0*P_P*pow(P_l,2)*P_qm*ssp*svm - 4.E+0*P_P*pow(
      P_l,2)*P_qp*svp*ssm + 8.E+0*pow(P_P,2)*P_l*l_qm*ssp*svm - 8.E+0*
      pow(P_P,2)*P_l*l_qp*svp*ssm;

    kern[0][2] = 1.2E+1*P_P*P_qm*ssp*svm - 1.2E+1*P_P*P_qp*svp*ssm;

    kern[0][3] = 8.E+0*P_P*P_qm*l_qp*svp*svm - 8.E+0*P_P*P_qp*l_qm*svp*
      svm;

    kern[0][4] =  - 4.E+0*P_P*P_l*P_qm*ssp*svm - 4.E+0*P_P*P_l*P_qp*svp
      *ssm + 4.E+0*pow(P_P,2)*l_qm*ssp*svm + 4.E+0*pow(P_P,2)*l_qp*svp*
      ssm;

    kern[0][5] = 4.E+0*P_P*P_l*P_qm*l_qp*svp*svm + 4.E+0*P_P*P_l*P_qp*
      l_qm*svp*svm - 4.E+0*P_P*pow(P_l,2)*qm_qp*svp*svm - 4.E+0*P_P*
      pow(P_l,2)*ssp*ssm + 4.E+0*pow(P_P,2)*l_l*qm_qp*svp*svm + 4.E+0*
      pow(P_P,2)*l_l*ssp*ssm - 8.E+0*pow(P_P,2)*l_qm*l_qp*svp*svm;

    kern[0][6] = 4.E+0*P_P*P_l*P_qm*l_qp*svp*svm + 4.E+0*P_P*P_l*P_qp*
      l_qm*svp*svm + 4.E+0*P_P*pow(P_l,2)*qm_qp*svp*svm + 1.2E+1*P_P*
      pow(P_l,2)*ssp*ssm;

    kern[0][7] = 8.E+0*P_P*P_l*l_qm*ssp*svm - 8.E+0*P_P*P_l*l_qp*svp*
      ssm + 4.E+0*P_P*P_qm*l_l*ssp*svm - 4.E+0*P_P*P_qp*l_l*svp*ssm;

 
    kern[1][0] = 4.E+0*P_P*pow(P_l,2)*P_qm*ssp*svm - 4.E+0*P_P*pow(
      P_l,2)*P_qp*svp*ssm + 8.E+0*pow(P_P,2)*P_l*l_qm*ssp*svm - 8.E+0*
      pow(P_P,2)*P_l*l_qp*svp*ssm;

    kern[1][1] = 8.E+0*P_P*pow(P_l,2)*P_qm*P_qp*l_l*svp*svm - 8.E+0*P_P
      *pow(P_l,3)*P_qm*l_qp*svp*svm - 8.E+0*P_P*pow(P_l,3)*P_qp*l_qm*
      svp*svm + 4.E+0*P_P*pow(P_l,4)*qm_qp*svp*svm - 4.E+0*P_P*pow(
      P_l,4)*ssp*ssm - 8.E+0*pow(P_P,2)*pow(P_l,2)*l_l*ssp*ssm - 8.E+0*
      pow(P_P,2)*pow(P_l,2)*l_qm*l_qp*svp*svm;

    kern[1][2] =  - 8.E+0*P_P*P_l*P_qm*l_qp*svp*svm - 8.E+0*P_P*P_l*
      P_qp*l_qm*svp*svm + 4.E+0*P_P*pow(P_l,2)*qm_qp*svp*svm - 1.2E+1*
      P_P*pow(P_l,2)*ssp*ssm;

    kern[1][3] = 8.E+0*P_P*P_l*P_qm*l_l*ssp*svm + 8.E+0*P_P*P_l*P_qp*
      l_l*svp*ssm - 8.E+0*P_P*pow(P_l,2)*l_qm*ssp*svm - 8.E+0*P_P*pow(
      P_l,2)*l_qp*svp*ssm;

    kern[1][4] = 4.E+0*P_P*pow(P_l,2)*P_qm*l_qp*svp*svm - 4.E+0*P_P*
      pow(P_l,2)*P_qp*l_qm*svp*svm;

    kern[1][5] = 4.E+0*P_P*pow(P_l,2)*P_qm*l_l*ssp*svm - 4.E+0*P_P*pow(
      P_l,2)*P_qp*l_l*svp*ssm - 4.E+0*P_P*pow(P_l,3)*l_qm*ssp*svm + 
      4.E+0*P_P*pow(P_l,3)*l_qp*svp*ssm;

    kern[1][6] = 1.2E+1*P_P*pow(P_l,3)*l_qm*ssp*svm - 1.2E+1*P_P*pow(
      P_l,3)*l_qp*svp*ssm;

    kern[1][7] = 4.E+0*P_P*pow(P_l,2)*l_l*qm_qp*svp*svm - 1.2E+1*P_P*
      pow(P_l,2)*l_l*ssp*ssm - 1.6E+1*P_P*pow(P_l,2)*l_qm*l_qp*svp*svm;

 
    kern[2][0] = 1.2E+1*P_P*P_qm*ssp*svm - 1.2E+1*P_P*P_qp*svp*ssm;

    kern[2][1] =  - 8.E+0*P_P*P_l*P_qm*l_qp*svp*svm - 8.E+0*P_P*P_l*
      P_qp*l_qm*svp*svm + 4.E+0*P_P*pow(P_l,2)*qm_qp*svp*svm - 1.2E+1*
      P_P*pow(P_l,2)*ssp*ssm;

    kern[2][2] = 4.E+0*P_P*qm_qp*svp*svm - 1.2E+1*P_P*ssp*ssm - 1.6E+1*
      P_qm*P_qp*svp*svm;

    kern[2][3] =  - 8.E+0*P_P*l_qm*ssp*svm - 8.E+0*P_P*l_qp*svp*ssm + 
      8.E+0*P_l*P_qm*ssp*svm + 8.E+0*P_l*P_qp*svp*ssm;

    kern[2][4] = 4.E+0*P_P*P_qm*l_qp*svp*svm - 4.E+0*P_P*P_qp*l_qm*svp*
      svm;

    kern[2][5] =  - 4.E+0*P_P*P_l*l_qm*ssp*svm + 4.E+0*P_P*P_l*l_qp*svp
      *ssm + 4.E+0*P_P*P_qm*l_l*ssp*svm - 4.E+0*P_P*P_qp*l_l*svp*ssm;

    kern[2][6] = 4.E+0*P_P*P_l*l_qm*ssp*svm - 4.E+0*P_P*P_l*l_qp*svp*
      ssm + 8.E+0*pow(P_l,2)*P_qm*ssp*svm - 8.E+0*pow(P_l,2)*P_qp*svp*
      ssm;

    kern[2][7] = 4.E+0*P_P*l_l*qm_qp*svp*svm - 4.E+0*P_P*l_l*ssp*ssm - 
      8.E+0*P_P*l_qm*l_qp*svp*svm - 8.E+0*pow(P_l,2)*ssp*ssm - 8.E+0*
      P_qm*P_qp*l_l*svp*svm;

 
    kern[3][0] = 8.E+0*P_P*P_qm*l_qp*svp*svm - 8.E+0*P_P*P_qp*l_qm*svp*
      svm;

    kern[3][1] = 8.E+0*P_P*P_l*P_qm*l_l*ssp*svm + 8.E+0*P_P*P_l*P_qp*
      l_l*svp*ssm - 8.E+0*P_P*pow(P_l,2)*l_qm*ssp*svm - 8.E+0*P_P*pow(
      P_l,2)*l_qp*svp*ssm;

    kern[3][2] =  - 8.E+0*P_P*l_qm*ssp*svm - 8.E+0*P_P*l_qp*svp*ssm + 
      8.E+0*P_l*P_qm*ssp*svm + 8.E+0*P_l*P_qp*svp*ssm;

    kern[3][3] = 8.E+0*P_P*l_l*ssp*ssm - 8.E+0*P_P*l_qm*l_qp*svp*svm + 
      8.E+0*P_l*P_qm*l_qp*svp*svm + 8.E+0*P_l*P_qp*l_qm*svp*svm - 8.E+0
      *pow(P_l,2)*ssp*ssm - 8.E+0*P_qm*P_qp*l_l*svp*svm;

    kern[3][4] =  0;

    kern[3][5] =  0;

    kern[3][6] = 8.E+0*pow(P_l,2)*P_qm*l_qp*svp*svm - 8.E+0*pow(P_l,2)*
      P_qp*l_qm*svp*svm;

    kern[3][7] = 8.E+0*P_l*P_qm*l_l*ssp*svm + 8.E+0*P_l*P_qp*l_l*svp*
      ssm - 8.E+0*pow(P_l,2)*l_qm*ssp*svm - 8.E+0*pow(P_l,2)*l_qp*svp*
      ssm;

 
    kern[4][0] =  - 4.E+0*P_P*P_l*P_qm*ssp*svm - 4.E+0*P_P*P_l*P_qp*svp
      *ssm + 4.E+0*pow(P_P,2)*l_qm*ssp*svm + 4.E+0*pow(P_P,2)*l_qp*svp*
      ssm;

    kern[4][1] = 4.E+0*P_P*pow(P_l,2)*P_qm*l_qp*svp*svm - 4.E+0*P_P*
      pow(P_l,2)*P_qp*l_qm*svp*svm;

    kern[4][2] = 4.E+0*P_P*P_qm*l_qp*svp*svm - 4.E+0*P_P*P_qp*l_qm*svp*
      svm;

    kern[4][3] =  0;

    kern[4][4] =  - 4.E+0*P_P*pow(P_l,2)*qm_qp*svp*svm + 4.E+0*P_P*pow(
      P_l,2)*ssp*ssm + 4.E+0*pow(P_P,2)*l_l*qm_qp*svp*svm - 4.E+0*pow(
      P_P,2)*l_l*ssp*ssm;

    kern[4][5] =  - 4.E+0*P_P*pow(P_l,2)*l_qm*ssp*svm - 4.E+0*P_P*pow(
      P_l,2)*l_qp*svp*ssm + 4.E+0*pow(P_P,2)*l_l*l_qm*ssp*svm + 4.E+0*
      pow(P_P,2)*l_l*l_qp*svp*ssm;

    kern[4][6] =  - 4.E+0*P_P*P_l*P_qm*l_l*ssp*svm - 4.E+0*P_P*P_l*P_qp
      *l_l*svp*ssm + 4.E+0*P_P*pow(P_l,2)*l_qm*ssp*svm + 4.E+0*P_P*pow(
      P_l,2)*l_qp*svp*ssm;

    kern[4][7] = 4.E+0*P_P*P_qm*l_l*l_qp*svp*svm - 4.E+0*P_P*P_qp*l_l*
      l_qm*svp*svm;

 
    kern[5][0] = 4.E+0*P_P*P_l*P_qm*l_qp*svp*svm + 4.E+0*P_P*P_l*P_qp*
      l_qm*svp*svm - 4.E+0*P_P*pow(P_l,2)*qm_qp*svp*svm - 4.E+0*P_P*
      pow(P_l,2)*ssp*ssm + 4.E+0*pow(P_P,2)*l_l*qm_qp*svp*svm + 4.E+0*
      pow(P_P,2)*l_l*ssp*ssm - 8.E+0*pow(P_P,2)*l_qm*l_qp*svp*svm;

    kern[5][1] = 4.E+0*P_P*pow(P_l,2)*P_qm*l_l*ssp*svm - 4.E+0*P_P*pow(
      P_l,2)*P_qp*l_l*svp*ssm - 4.E+0*P_P*pow(P_l,3)*l_qm*ssp*svm + 
      4.E+0*P_P*pow(P_l,3)*l_qp*svp*ssm;

    kern[5][2] =  - 4.E+0*P_P*P_l*l_qm*ssp*svm + 4.E+0*P_P*P_l*l_qp*svp
      *ssm + 4.E+0*P_P*P_qm*l_l*ssp*svm - 4.E+0*P_P*P_qp*l_l*svp*ssm;

    kern[5][3] =  0;

    kern[5][4] =  - 4.E+0*P_P*pow(P_l,2)*l_qm*ssp*svm - 4.E+0*P_P*pow(
      P_l,2)*l_qp*svp*ssm + 4.E+0*pow(P_P,2)*l_l*l_qm*ssp*svm + 4.E+0*
      pow(P_P,2)*l_l*l_qp*svp*ssm;

    kern[5][5] =  - 4.E+0*P_P*pow(P_l,2)*l_l*qm_qp*svp*svm - 4.E+0*P_P*
      pow(P_l,2)*l_l*ssp*ssm + 8.E+0*P_P*pow(P_l,2)*l_qm*l_qp*svp*svm
       - 8.E+0*pow(P_P,2)*l_l*l_qm*l_qp*svp*svm + 4.E+0*pow(P_P,2)*pow(
      l_l,2)*qm_qp*svp*svm + 4.E+0*pow(P_P,2)*pow(l_l,2)*ssp*ssm;

    kern[5][6] = 4.E+0*P_P*P_l*P_qm*l_l*l_qp*svp*svm + 4.E+0*P_P*P_l*
      P_qp*l_l*l_qm*svp*svm - 8.E+0*P_P*pow(P_l,2)*l_qm*l_qp*svp*svm;

    kern[5][7] =  - 4.E+0*P_P*P_l*l_l*l_qm*ssp*svm + 4.E+0*P_P*P_l*l_l*
      l_qp*svp*ssm + 4.E+0*P_P*P_qm*pow(l_l,2)*ssp*svm - 4.E+0*P_P*P_qp
      *pow(l_l,2)*svp*ssm;

 
    kern[6][0] = 4.E+0*P_P*P_l*P_qm*l_qp*svp*svm + 4.E+0*P_P*P_l*P_qp*
      l_qm*svp*svm + 4.E+0*P_P*pow(P_l,2)*qm_qp*svp*svm + 1.2E+1*P_P*
      pow(P_l,2)*ssp*ssm;

    kern[6][1] = 1.2E+1*P_P*pow(P_l,3)*l_qm*ssp*svm - 1.2E+1*P_P*pow(
      P_l,3)*l_qp*svp*ssm;

    kern[6][2] = 4.E+0*P_P*P_l*l_qm*ssp*svm - 4.E+0*P_P*P_l*l_qp*svp*
      ssm + 8.E+0*pow(P_l,2)*P_qm*ssp*svm - 8.E+0*pow(P_l,2)*P_qp*svp*
      ssm;

    kern[6][3] = 8.E+0*pow(P_l,2)*P_qm*l_qp*svp*svm - 8.E+0*pow(P_l,2)*
      P_qp*l_qm*svp*svm;

    kern[6][4] =  - 4.E+0*P_P*P_l*P_qm*l_l*ssp*svm - 4.E+0*P_P*P_l*P_qp
      *l_l*svp*ssm + 4.E+0*P_P*pow(P_l,2)*l_qm*ssp*svm + 4.E+0*P_P*pow(
      P_l,2)*l_qp*svp*ssm;

    kern[6][5] = 4.E+0*P_P*P_l*P_qm*l_l*l_qp*svp*svm + 4.E+0*P_P*P_l*
      P_qp*l_l*l_qm*svp*svm - 8.E+0*P_P*pow(P_l,2)*l_qm*l_qp*svp*svm;

    kern[6][6] = 4.E+0*P_P*pow(P_l,2)*l_l*qm_qp*svp*svm + 4.E+0*P_P*
      pow(P_l,2)*l_l*ssp*ssm - 8.E+0*pow(P_l,2)*P_qm*P_qp*l_l*svp*svm
       + 8.E+0*pow(P_l,3)*P_qm*l_qp*svp*svm + 8.E+0*pow(P_l,3)*P_qp*
      l_qm*svp*svm + 8.E+0*pow(P_l,4)*ssp*ssm;

    kern[6][7] = 4.E+0*P_P*P_l*l_l*l_qm*ssp*svm - 4.E+0*P_P*P_l*l_l*
      l_qp*svp*ssm + 8.E+0*pow(P_l,3)*l_qm*ssp*svm - 8.E+0*pow(P_l,3)*
      l_qp*svp*ssm;

 
    kern[7][0] = 8.E+0*P_P*P_l*l_qm*ssp*svm - 8.E+0*P_P*P_l*l_qp*svp*
      ssm + 4.E+0*P_P*P_qm*l_l*ssp*svm - 4.E+0*P_P*P_qp*l_l*svp*ssm;

    kern[7][1] = 4.E+0*P_P*pow(P_l,2)*l_l*qm_qp*svp*svm - 1.2E+1*P_P*
      pow(P_l,2)*l_l*ssp*ssm - 1.6E+1*P_P*pow(P_l,2)*l_qm*l_qp*svp*svm;

    kern[7][2] = 4.E+0*P_P*l_l*qm_qp*svp*svm - 4.E+0*P_P*l_l*ssp*ssm - 
      8.E+0*P_P*l_qm*l_qp*svp*svm - 8.E+0*pow(P_l,2)*ssp*ssm - 8.E+0*
      P_qm*P_qp*l_l*svp*svm;

    kern[7][3] = 8.E+0*P_l*P_qm*l_l*ssp*svm + 8.E+0*P_l*P_qp*l_l*svp*
      ssm - 8.E+0*pow(P_l,2)*l_qm*ssp*svm - 8.E+0*pow(P_l,2)*l_qp*svp*
      ssm;

    kern[7][4] = 4.E+0*P_P*P_qm*l_l*l_qp*svp*svm - 4.E+0*P_P*P_qp*l_l*
      l_qm*svp*svm;

    kern[7][5] =  - 4.E+0*P_P*P_l*l_l*l_qm*ssp*svm + 4.E+0*P_P*P_l*l_l*
      l_qp*svp*ssm + 4.E+0*P_P*P_qm*pow(l_l,2)*ssp*svm - 4.E+0*P_P*P_qp
      *pow(l_l,2)*svp*ssm;

    kern[7][6] = 4.E+0*P_P*P_l*l_l*l_qm*ssp*svm - 4.E+0*P_P*P_l*l_l*
      l_qp*svp*ssm + 8.E+0*pow(P_l,3)*l_qm*ssp*svm - 8.E+0*pow(P_l,3)*
      l_qp*svp*ssm;

    kern[7][7] =  - 8.E+0*P_P*l_l*l_qm*l_qp*svp*svm + 4.E+0*P_P*pow(
      l_l,2)*qm_qp*svp*svm - 4.E+0*P_P*pow(l_l,2)*ssp*ssm + 8.E+0*P_l*
      P_qm*l_l*l_qp*svp*svm + 8.E+0*P_l*P_qp*l_l*l_qm*svp*svm - 8.E+0*
      pow(P_l,2)*l_l*ssp*ssm - 1.6E+1*pow(P_l,2)*l_qm*l_qp*svp*svm - 
      8.E+0*P_qm*P_qp*pow(l_l,2)*svp*svm;


}

void normalisation_qphv_trans(matCdoub& kern,
  Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
  Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
  const Cdoub& svp, const Cdoub& ssp, 
  const Cdoub& svm, const Cdoub& ssm ){
 
 
     // local variables
    array<Cdoub, 56> w;
 
 
    w[1]=P_P;
    w[2]=P_qm;
    w[3]=P_qp;
    w[4]=qm_qp;
    w[5]=P_l;
    w[6]=l_qm;
    w[7]=l_qp;
    w[8]=l_l;
   w[9]=ssm*ssp;
   w[10]=3.E+0*w[9];
   w[11]=svm*svp;
   w[12]=w[11]*w[4];
   w[13]=w[10] + w[12];
   w[14]=w[1]*w[13];
   w[15]=w[11]*w[3];
   w[16]=w[2]*w[15];
   w[17]=2.E+0*w[16];
   w[14]=w[17] + w[14];
   w[14]=4.E+0*w[1]*w[14];
   w[18]=ssm*svp;
   w[19]=w[3]*w[18];
   w[20]=svm*ssp;
   w[21]=w[2]*w[20];
   w[22]=w[19] - w[21];
   w[23]=w[5]*w[1];
   w[24]=w[22]*w[23];
   w[18]=w[7]*w[18];
   w[20]=w[6]*w[20];
   w[25]=w[18] - w[20];
   w[26]=pow(w[1],2);
   w[27]=2.E+0*w[26];
   w[27]=w[27]*w[25];
   w[24]=w[24] + w[27];
   w[27]=4.E+0*w[5];
   w[24]=w[24]*w[27];
   w[28]=w[22]*w[1];
   w[28]=1.2E+1*w[28];
   w[11]=w[11]*w[7];
   w[29]=w[11]*w[2];
   w[15]=w[6]*w[15];
   w[30]=w[29] - w[15];
   w[31]=8.E+0*w[30];
   w[32]=w[31]*w[1];
   w[18]=w[18] + w[20];
   w[20]=w[18]*w[26];
   w[19]=w[19] + w[21];
   w[21]=w[19]*w[23];
   w[20]=w[20] - w[21];
   w[20]=4.E+0*w[20];
   w[21]=w[12] + w[9];
   w[33]=w[21]*w[23];
   w[15]=w[29] + w[15];
   w[29]=w[15]*w[1];
   w[33]=w[33] - w[29];
   w[33]=w[33]*w[5];
   w[34]=w[26]*w[8];
   w[35]=w[34]*w[21];
   w[11]=w[11]*w[6];
   w[36]=2.E+0*w[11];
   w[26]=w[36]*w[26];
   w[35]=w[35] - w[26];
   w[33]=w[33] - w[35];
   w[33]=4.E+0*w[33];
   w[13]=w[13]*w[23];
   w[13]=w[13] + w[29];
   w[13]=w[13]*w[27];
   w[37]=w[8]*w[1];
   w[38]=w[22]*w[37];
   w[39]=w[25]*w[23];
   w[40]=w[38] + 2.E+0*w[39];
   w[40]=4.E+0*w[40];
   w[29]=2.E+0*w[29];
   w[41]=w[12] - w[9];
   w[42]=w[41]*w[23];
   w[42]= - w[29] + w[42];
   w[42]=w[5]*w[42];
   w[26]= - w[26] + w[42];
   w[42]=pow(w[5],2);
   w[26]=w[26]*w[42];
   w[43]=w[9]*w[1];
   w[43]=w[43] - w[16];
   w[44]=w[42]*w[1];
   w[45]=w[44]*w[8];
   w[46]=w[43]*w[45];
   w[26]=w[26] - 2.E+0*w[46];
   w[26]=4.E+0*w[26];
   w[10]=w[10] - w[12];
   w[12]=w[10]*w[23];
   w[12]=w[12] + w[29];
   w[12]=w[12]*w[27];
   w[23]=w[23]*w[8];
   w[29]=w[23]*w[19];
   w[46]=w[18]*w[44];
   w[29]=w[29] - w[46];
   w[47]=8.E+0*w[29];
   w[30]=4.E+0*w[30];
   w[48]=w[30]*w[44];
   w[49]=w[45]*w[22];
   w[50]=pow(w[5],3);
   w[51]=w[50]*w[25];
   w[52]=w[51]*w[1];
   w[49]=w[49] - w[52];
   w[49]=4.E+0*w[49];
   w[52]=1.2E+1*w[52];
   w[45]=w[10]*w[45];
   w[53]=4.E+0*w[11];
   w[54]=w[53]*w[44];
   w[45]=w[45] + w[54];
   w[45]=4.E+0*w[45];
   w[10]= - w[1]*w[10];
   w[10]= - 4.E+0*w[16] + w[10];
   w[10]=4.E+0*w[10];
   w[16]=w[18]*w[1];
   w[19]=w[19]*w[5];
   w[16]=w[16] - w[19];
   w[16]=8.E+0*w[16];
   w[54]=w[30]*w[1];
   w[38]=w[38] - w[39];
   w[38]=4.E+0*w[38];
   w[22]=w[22]*w[5];
   w[25]=w[25]*w[1];
   w[22]=w[25] + 2.E+0*w[22];
   w[22]=w[22]*w[27];
   w[25]=w[9]*w[42];
   w[11]=w[11]*w[1];
   w[25]=w[25] + w[11];
   w[27]=w[41]*w[1];
   w[27]=w[27] - w[17];
   w[27]=w[27]*w[8];
   w[25]= - w[27] + 2.E+0*w[25];
   w[25]=4.E+0*w[25];
   w[9]=w[9]*w[5];
   w[55]=w[9] - w[15];
   w[55]=w[55]*w[5];
   w[11]=w[55] + w[11];
   w[43]=w[8]*w[43];
   w[43]=w[43] - w[11];
   w[43]=8.E+0*w[43];
   w[31]=w[31]*w[42];
   w[55]=w[18]*w[42];
   w[19]=w[19]*w[8];
   w[19]=w[55] - w[19];
   w[19]=8.E+0*w[19];
   w[55]= - w[44] + w[34];
   w[41]=4.E+0*w[41]*w[55];
   w[18]=w[34]*w[18];
   w[18]=w[18] - w[46];
   w[18]=4.E+0*w[18];
   w[29]=4.E+0*w[29];
   w[30]=w[30]*w[37];
   w[34]= - w[21]*w[44];
   w[34]=w[34] + w[35];
   w[34]=w[8]*w[34];
   w[35]=w[36]*w[44];
   w[34]=w[35] + w[34];
   w[34]=4.E+0*w[34];
   w[23]=w[23]*w[15];
   w[23]=w[23] - w[35];
   w[23]=4.E+0*w[23];
   w[35]=w[38]*w[8];
   w[9]=w[9] + w[15];
   w[9]=w[9]*w[50];
   w[15]=w[1]*w[21];
   w[15]= - w[17] + w[15];
   w[15]=w[8]*w[15]*w[42];
   w[9]=2.E+0*w[9] + w[15];
   w[9]=4.E+0*w[9];
   w[15]=w[39]*w[8];
   w[15]=w[15] + 2.E+0*w[51];
   w[15]=4.E+0*w[15];
   w[11]= - 2.E+0*w[11] + w[27];
   w[11]=w[8]*w[11];
   w[17]= - w[42]*w[53];
   w[11]=w[17] + w[11];
   w[11]=4.E+0*w[11];

 
    kern[0][0] = w[14];

    kern[0][1] =  - w[24];

    kern[0][2] =  - w[28];

    kern[0][3] = w[32];

    kern[0][4] = w[20];

    kern[0][5] =  - w[33];

    kern[0][6] = w[13];

    kern[0][7] =  - w[40];

 
    kern[1][0] =  - w[24];

    kern[1][1] = w[26];

    kern[1][2] =  - w[12];

    kern[1][3] = w[47];

    kern[1][4] = w[48];

    kern[1][5] =  - w[49];

    kern[1][6] =  - w[52];

    kern[1][7] =  - w[45];

 
    kern[2][0] =  - w[28];

    kern[2][1] =  - w[12];

    kern[2][2] = w[10];

    kern[2][3] =  - w[16];

    kern[2][4] = w[54];

    kern[2][5] =  - w[38];

    kern[2][6] =  - w[22];

    kern[2][7] =  - w[25];

 
    kern[3][0] = w[32];

    kern[3][1] = w[47];

    kern[3][2] =  - w[16];

    kern[3][3] = w[43];

    kern[3][4] =  0;

    kern[3][5] =  0;

    kern[3][6] = w[31];

    kern[3][7] =  - w[19];

 
    kern[4][0] = w[20];

    kern[4][1] = w[48];

    kern[4][2] = w[54];

    kern[4][3] =  0;

    kern[4][4] = w[41];

    kern[4][5] = w[18];

    kern[4][6] =  - w[29];

    kern[4][7] = w[30];

 
    kern[5][0] =  - w[33];

    kern[5][1] =  - w[49];

    kern[5][2] =  - w[38];

    kern[5][3] =  0;

    kern[5][4] = w[18];

    kern[5][5] = w[34];

    kern[5][6] = w[23];

    kern[5][7] =  - w[35];

 
    kern[6][0] = w[13];

    kern[6][1] =  - w[52];

    kern[6][2] =  - w[22];

    kern[6][3] = w[31];

    kern[6][4] =  - w[29];

    kern[6][5] = w[23];

    kern[6][6] = w[9];

    kern[6][7] =  - w[15];

 
    kern[7][0] =  - w[40];

    kern[7][1] =  - w[45];

    kern[7][2] =  - w[25];

    kern[7][3] =  - w[19];

    kern[7][4] = w[30];

    kern[7][5] =  - w[35];

    kern[7][6] =  - w[15];

    kern[7][7] = w[11];


}

