#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>

void kernelall_qphv_longi(matCdoub& kern,
  const ArrayScalarProducts& sp,  const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ,
                const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){


    // local variables
    array<Cdoub, 61> w;
    Cdoub denom;


    w[1]=sp[35];
    w[2]=sp[27];
    w[3]=sp[7];
    w[4]=sp[34];
    w[5]=sp[43];
    w[6]=sp[44];
    w[7]=sp[31];
    w[8]=sp[22];
    w[9]=sp[37];
    w[10]=sp[29];
    w[11]=sp[24];
    w[12]=sp[39];
   w[13]=ang[0]*w[6];
   w[14]=w[13] - ang[1];
   w[15]=w[2]*ang[0];
   w[16]=w[14] + w[15];
   w[16]=w[16]*w[4];
   w[17]=2.E+0*w[5];
   w[18]=w[17]*w[15];
   w[16]=w[16] - w[18];
   w[19]=w[16]*w[12];
   w[20]=w[13] + w[15];
   w[21]=2.E+0*ang[1];
   w[22]=w[20] - w[21];
   w[23]=w[22]*w[4];
   w[23]=w[23] - w[18];
   w[24]= - w[11]*w[23];
   w[24]=w[19] + w[24];
   w[24]=w[10]*w[24];
   w[25]=w[16]*w[9];
   w[26]= - w[8]*w[23];
   w[27]=w[10]*ang[1];
   w[28]=2.E+0*w[2];
   w[28]= - w[28]*w[27];
   w[26]=w[25] + w[26] + w[28];
   w[26]=w[7]*w[26];
   w[28]=3.E+0*w[15];
   w[29]=w[28] + w[14];
   w[29]=w[29]*w[4];
   w[29]=w[29] - w[18];
   w[29]=w[29]*w[5];
   w[30]=pow(w[4],2);
   w[22]=w[22]*w[30];
   w[31]=w[1]*w[2];
   w[32]=w[31]*ang[1];
   w[22]=w[29] - w[22] - w[32];
   w[29]= - w[3]*w[22];
   w[24]=w[29] + w[26] + w[24];
   w[26]=svm*svp;
   w[24]=w[24]*w[26];
   w[29]=ssm*ssp;
   w[32]= - w[22]*w[29];
   w[24]=w[24] + w[32];
   w[24]=4.E+0*w[24];
   w[32]=w[8]*w[5];
   w[33]=w[23]*w[32];
   w[34]=w[5]*w[2];
   w[35]=w[27]*w[34];
   w[33]=w[35] + w[33];
   w[35]=w[17]*w[9];
   w[16]=w[16]*w[35];
   w[36]=w[16] - w[33];
   w[36]=w[12]*w[36];
   w[37]=w[11]*w[5];
   w[23]=w[37]*w[23];
   w[38]=w[7]*ang[1];
   w[34]=w[38]*w[34];
   w[23]=w[34] + w[23];
   w[34]= - w[9]*w[23];
   w[34]=w[34] + w[36];
   w[36]=ang[0]*pow(w[6],2);
   w[39]=ang[1]*w[6];
   w[40]= - w[39] - w[36] + 2.E+0*ang[2];
   w[41]= - w[15] + 3.E+0*ang[1];
   w[41]=w[41]*w[2];
   w[41]=w[41] - w[40];
   w[41]=w[41]*w[4];
   w[42]=w[13] - w[15];
   w[43]=w[21] + w[42];
   w[43]=w[43]*w[17];
   w[44]=w[43]*w[2];
   w[41]=w[41] - w[44];
   w[44]=w[3]*w[5];
   w[45]= - w[41]*w[44];
   w[34]=2.E+0*w[34] + w[45];
   w[34]=w[34]*w[26];
   w[45]=w[29]*w[5];
   w[46]= - w[41]*w[45];
   w[34]=w[34] + w[46];
   w[34]=2.E+0*w[34];
   w[46]=w[5]*w[19];
   w[23]=w[46] - w[23];
   w[46]=svm*ssp;
   w[23]=w[23]*w[46];
   w[25]=w[5]*w[25];
   w[25]=w[25] - w[33];
   w[33]=ssm*svp;
   w[25]=w[25]*w[33];
   w[23]=w[23] + w[25];
   w[23]=4.E+0*w[23];
   w[25]=w[7]*w[5];
   w[47]= - w[41]*w[25];
   w[48]=w[17]*w[12];
   w[49]=w[22]*w[48];
   w[47]=w[47] + w[49];
   w[47]=w[47]*w[46];
   w[49]=w[10]*w[5];
   w[41]=w[41]*w[49];
   w[22]= - w[22]*w[35];
   w[22]=w[41] + w[22];
   w[22]=w[22]*w[33];
   w[22]=w[47] + w[22];
   w[22]=4.E+0*w[22];
   w[41]=w[5]*w[4];
   w[47]=2.E+0*ang[0];
   w[50]=w[41]*w[47];
   w[47]=w[30]*w[47];
   w[51]=w[50] - w[47];
   w[52]=w[42] - ang[1];
   w[53]=w[52]*w[1];
   w[54]=w[51] - w[53];
   w[55]=w[54]*w[12];
   w[21]=w[21] - w[42];
   w[21]=w[21]*w[1];
   w[21]=w[21] + w[51];
   w[51]= - w[11]*w[21];
   w[51]=w[55] + w[51];
   w[51]=w[10]*w[51];
   w[56]=w[54]*w[9];
   w[57]= - w[8]*w[21];
   w[58]=2.E+0*w[4];
   w[59]=w[58]*w[27];
   w[57]=w[56] + w[57] + w[59];
   w[57]=w[7]*w[57];
   w[59]=4.E+0*w[30];
   w[60]=w[59]*ang[0];
   w[50]= - w[50] + w[60] + w[53];
   w[50]=w[50]*w[5];
   w[47]=w[53] + w[47];
   w[47]=w[47]*w[4];
   w[47]=w[50] - w[47];
   w[50]=w[3]*w[47];
   w[50]=w[50] + w[57] + w[51];
   w[50]=w[50]*w[26];
   w[29]=w[47]*w[29];
   w[29]=w[50] + w[29];
   w[29]=4.E+0*w[29];
   w[50]=w[21]*w[32];
   w[51]=w[27]*w[41];
   w[50]=w[51] - w[50];
   w[51]=w[35]*w[54];
   w[53]=w[51] + w[50];
   w[53]=w[12]*w[53];
   w[21]=w[21]*w[37];
   w[41]=w[38]*w[41];
   w[21]=w[41] - w[21];
   w[41]=w[9]*w[21];
   w[41]=w[41] + w[53];
   w[53]=2.E+0*w[13];
   w[54]=w[53] - w[15];
   w[57]= - ang[1] + w[54];
   w[57]=w[57]*w[2];
   w[40]=w[57] + w[40];
   w[40]=w[40]*w[1];
   w[57]=w[42] + ang[1];
   w[60]=2.E+0*w[30];
   w[57]=w[57]*w[60];
   w[43]=w[43]*w[4];
   w[40]=w[40] - w[57] + w[43];
   w[43]= - w[40]*w[44];
   w[41]=2.E+0*w[41] + w[43];
   w[41]=w[41]*w[26];
   w[43]= - w[40]*w[45];
   w[41]=w[41] + w[43];
   w[41]=2.E+0*w[41];
   w[43]=w[5]*w[55];
   w[21]=w[43] + w[21];
   w[21]=w[21]*w[46];
   w[43]=w[5]*w[56];
   w[43]=w[43] + w[50];
   w[43]=w[43]*w[33];
   w[21]=w[21] + w[43];
   w[21]=4.E+0*w[21];
   w[43]= - w[40]*w[25];
   w[48]= - w[47]*w[48];
   w[43]=w[43] + w[48];
   w[43]=w[43]*w[46];
   w[40]=w[40]*w[49];
   w[35]=w[47]*w[35];
   w[35]=w[40] + w[35];
   w[35]=w[35]*w[33];
   w[35]=w[43] + w[35];
   w[35]=4.E+0*w[35];
   w[38]=w[46]*w[38];
   w[27]=w[33]*w[27];
   w[27]=w[38] + w[27];
   w[27]=1.2E+1*w[27];
   w[38]=w[12]*w[46];
   w[40]=w[9]*w[33];
   w[38]=w[38] + w[40];
   w[40]=1.2E+1*ang[1];
   w[38]=w[5]*w[38]*w[40];
   w[43]= - w[26]*w[44];
   w[43]=w[43] + w[45];
   w[40]=w[43]*w[40];
   w[43]=w[9]*w[25];
   w[47]= - w[12]*w[49];
   w[43]=w[43] + w[47];
   w[43]=2.4E+1*w[26]*ang[1]*w[43];
   w[47]=w[42]*w[1];
   w[48]=w[4] - w[5];
   w[48]=w[17]*ang[0]*w[48];
   w[47]=w[47] + w[48];
   w[50]=w[11]*w[47];
   w[57]=w[42]*w[4];
   w[60]=w[52]*w[5];
   w[57]=w[57] - w[60];
   w[60]= - w[7]*w[57];
   w[50]=w[55] + w[50] + w[60];
   w[50]=w[50]*w[46];
   w[55]= - w[8]*w[47];
   w[57]=w[10]*w[57];
   w[55]= - w[56] + w[55] + w[57];
   w[55]=w[55]*w[33];
   w[50]=w[50] + w[55];
   w[50]=8.E+0*w[50];
   w[55]=w[58]*w[13];
   w[20]=w[20]*w[5];
   w[20]=w[55] - w[20];
   w[55]=w[20]*w[17];
   w[56]=w[11]*w[55];
   w[54]=ang[1] + w[54];
   w[54]=w[54]*w[2];
   w[36]=w[54] - w[36] + w[39];
   w[39]= - w[36]*w[25];
   w[19]= - w[17]*w[19];
   w[19]=w[19] + w[56] + w[39];
   w[19]=w[19]*w[46];
   w[39]=w[36]*w[49];
   w[46]= - w[8]*w[55];
   w[16]=w[16] + w[46] + w[39];
   w[16]=w[16]*w[33];
   w[16]=w[19] + w[16];
   w[16]=4.E+0*w[16];
   w[19]=w[49]*w[11];
   w[33]=w[8]*w[25];
   w[33]= - w[19] + w[33];
   w[33]=w[42]*w[33];
   w[42]= - w[11]*w[48];
   w[46]= - w[52]*w[25];
   w[42]=w[42] + w[46];
   w[42]=w[9]*w[42];
   w[46]=w[8]*w[48];
   w[48]=w[52]*w[49];
   w[46]=w[46] + w[48];
   w[46]=w[12]*w[46];
   w[33]=w[46] + w[42] + w[33];
   w[33]=8.E+0*w[33]*w[26];
   w[42]= - w[20]*w[32];
   w[39]=w[42] + w[39];
   w[39]=w[7]*w[39];
   w[32]=w[47]*w[32];
   w[14]=w[28] - w[14];
   w[14]=w[14]*w[5];
   w[15]= - ang[1] + 2.E+0*w[15];
   w[28]=w[15]*w[4];
   w[14]=w[14] - w[28];
   w[28]= - w[14]*w[49];
   w[28]=w[51] + w[32] + w[28];
   w[28]=w[12]*w[28];
   w[19]= - w[20]*w[19];
   w[20]=w[47]*w[37];
   w[14]= - w[14]*w[25];
   w[14]=w[20] + w[14];
   w[14]=w[9]*w[14];
   w[14]=w[28] + w[14] + w[19] + w[39];
   w[15]=w[15] + w[53];
   w[15]=w[15]*w[4];
   w[15]=w[15] - w[18];
   w[15]=w[15]*w[17];
   w[17]=w[36]*w[1];
   w[13]=w[59]*w[13];
   w[13]=w[15] - w[13] + w[17];
   w[15]= - w[13]*w[44];
   w[14]=2.E+0*w[14] + w[15];
   w[14]=w[14]*w[26];
   w[13]=w[13]*w[45];
   w[13]=w[14] + w[13];
   w[13]=8.E+0*w[13];
   w[14]=w[31] - w[30];
   w[15]= - 4.E+0*w[14];
   w[14]=w[14]*w[4];
   w[17]= - 4.E+0*w[14];
   w[18]=4.E+0*w[4];
   w[14]= - 1.6E+1*w[14];


    denom = w[15];

    kern[0][0] = (1.0/ denom)* w[24];
 
    kern[0][1] = (1.0/ denom)* w[34];
 
    kern[0][2] = (1.0/ denom)* w[23];
 
    kern[0][3] = (1.0/ denom)* w[22];
 

    denom = w[17];

    kern[1][0] = (1.0/ denom)* w[29];
 
    kern[1][1] = (1.0/ denom)* w[41];
 
    kern[1][2] = (1.0/ denom)* w[21];
 
    kern[1][3] = (1.0/ denom)* w[35];
 

    denom = w[18];

    kern[2][0] = (1.0/ denom)* w[27];
 
    kern[2][1] = (1.0/ denom)* w[38];
 
    kern[2][2] = (1.0/ denom)* w[40];
 
    kern[2][3] = (1.0/ denom)* w[43];
 

    denom = w[14];

    kern[3][0] = (1.0/ denom)* w[50];
 
    kern[3][1] = (1.0/ denom)* w[16];
 
    kern[3][2] = (1.0/ denom)* w[33];
 
    kern[3][3] = (1.0/ denom)* w[13];
 

}
