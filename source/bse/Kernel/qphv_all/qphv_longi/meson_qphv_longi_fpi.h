#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
 
Cdoub calc_fpi_qphv_longi( 
  Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
  Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
  VecCdoub Gamma, const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){
 
 
 Cdoub dummy, fpi=0.0;
 
    dummy =   - 4.E+0*P(mu)*ssp*ssm - 4.E+0*P(mu)*qm_qp*svp*svm + 4.E+0
      *qm(mu)*P_qp*svp*svm + 4.E+0*qp(mu)*P_qm*svp*svm;
 
    fpi += Gamma[0] * dummy; 
 
    dummy =   - 4.E+0*l(mu)*P_l*qm_qp*svp*svm - 4.E+0*l(mu)*P_l*ssp*ssm
       + 4.E+0*qm(mu)*P_l*l_qp*svp*svm + 4.E+0*qp(mu)*P_l*l_qm*svp*svm;
 
    fpi += Gamma[1] * dummy; 
 
    dummy =  4.E+0*qm(mu)*P_l*ssp*svm + 4.E+0*qp(mu)*P_l*svp*ssm;
 
    fpi += Gamma[2] * dummy; 
 
    dummy =  8.E+0*P(mu)*P_l*l_qm*ssp*svm - 8.E+0*P(mu)*P_l*l_qp*svp*
      ssm - 8.E+0*l(mu)*P_l*P_qm*ssp*svm + 8.E+0*l(mu)*P_l*P_qp*svp*ssm
      ;
 
    fpi += Gamma[3] * dummy; 
 

    return fpi;

}

