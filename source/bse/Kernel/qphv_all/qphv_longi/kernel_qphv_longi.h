//
// Created by Esther Weil on 31.01.18.
//

#ifndef CALC_VMESON_KERNEL_QPHV_LONGI_H
#define CALC_VMESON_KERNEL_QPHV_LONGI_H

#include <Meson/typedefMeson.h>
#include <routing_amount_scalars.h>

void kernelL_notopti_qphv_longi(matCdoub& kern,
                        const ArrayScalarProducts& sp,
                        const array<Cdoub, K_ORDER_OF_ANGLE>& ang );

void kernelY_noopti_qphv_longi(matCdoub& kern,
                       const ArrayScalarProducts& sp,  const Cdoub& svp,
                       const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm );

void kernelL_qphv_longi(matCdoub& kern,
                const ArrayScalarProducts& sp,
                const array<Cdoub, K_ORDER_OF_ANGLE>& ang );


void kernelY_qphv_longi(matCdoub& kern,
                const ArrayScalarProducts& sp,  const Cdoub& svp,
                const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm );

Cdoub calc_fpi_qphv_longi(
        Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
        Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
        VecCdoub Gamma, const Cdoub& svp,
        const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm );

void kernelall_qphv_longi(matCdoub& kern,
                  const ArrayScalarProducts& sp,  const Cdoub& svp,
                  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ,
                  const array<Cdoub, K_ORDER_OF_ANGLE>& ang );

void normalisation_qphv_longi(matCdoub& kern,
                      const ArrayScalarProducts& sp,  const Cdoub& svp,
                      const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm );

void normalisation_notopti_qphv_longi(matCdoub& kern,
                              Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
                              Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
                              const Cdoub& svp, const Cdoub& ssp,
                              const Cdoub& svm, const Cdoub& ssm );

void normalisation_qphv_longi(matCdoub& kern,
                      Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,
                      Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,
                      const Cdoub& svp, const Cdoub& ssp,
                      const Cdoub& svm, const Cdoub& ssm );

void kernelInhomo_qphv_longi(VecCdoub& kern,
                       const ArrayScalarProducts& sp );

void kernelall_qphv_longi(matCdoub& kern,
                          const ArrayScalarProducts& sp,  const Cdoub& svp,
                          const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ,
                          const array<Cdoub, K_ORDER_OF_ANGLE>& ang );


#endif //CALC_VMESON_KERNEL_QPHV_LONGI_H
