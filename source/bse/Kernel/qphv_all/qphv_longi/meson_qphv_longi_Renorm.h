#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
 
void normalisation_qphv_longi(matCdoub& kern,
  const ArrayScalarProducts& sp,  const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){
 
 
     // local variables
    array<Cdoub, 27> w;
 
 
    w[1]=sp[35];
    w[2]=sp[12];
    w[3]=sp[32];
    w[4]=sp[30];
    w[5]=sp[33];
    w[6]=sp[17];
    w[7]=sp[19];
    w[8]=sp[20];
   w[9]=ssp*ssm;
   w[10]=svp*svm;
   w[11]=w[10]*w[2];
   w[12]=w[9] + w[11];
   w[13]=w[1]*w[12];
   w[14]=w[10]*w[3];
   w[15]=w[14]*w[4];
   w[15]=2.E+0*w[15];
   w[13]= - w[15] + w[13];
   w[13]=4.E+0*w[13];
   w[16]=w[12]*w[5];
   w[14]=w[14]*w[6];
   w[10]=w[10]*w[7];
   w[17]=w[10]*w[4];
   w[18]=w[14] + w[17];
   w[16]=w[16] - w[18];
   w[19]=4.E+0*w[5];
   w[16]=w[16]*w[19];
   w[20]=ssp*svm;
   w[21]=w[3]*w[20];
   w[22]=ssm*svp;
   w[23]=w[4]*w[22];
   w[24]=w[21] + w[23];
   w[19]=w[24]*w[19];
   w[21]=w[21] - w[23];
   w[23]=w[21]*w[5];
   w[20]=w[7]*w[20];
   w[22]=w[6]*w[22];
   w[24]=w[20] - w[22];
   w[25]=w[24]*w[1];
   w[23]=w[23] - w[25];
   w[25]=8.E+0*w[5];
   w[23]=w[23]*w[25];
   w[12]=w[8]*w[12];
   w[10]=w[10]*w[6];
   w[10]=2.E+0*w[10];
   w[12]= - w[10] + w[12];
   w[25]=pow(w[5],2);
   w[26]=4.E+0*w[25];
   w[12]=w[12]*w[26];
   w[20]=w[20] + w[22];
   w[20]=w[20]*w[26];
   w[21]=w[21]*w[8];
   w[22]=w[24]*w[5];
   w[21]=w[21] - w[22];
   w[22]=8.E+0*w[25];
   w[21]=w[21]*w[22];
   w[9]=w[9] - w[11];
   w[11]= - w[9]*w[26];
   w[14]=w[14] - w[17];
   w[14]=w[14]*w[22];
   w[17]=w[5]*w[9];
   w[17]=2.E+0*w[18] + w[17];
   w[17]=w[5]*w[17];
   w[9]= - w[1]*w[9];
   w[9]= - w[15] + w[9];
   w[9]=w[8]*w[9];
   w[10]= - w[1]*w[10];
   w[9]=w[17] + w[10] + w[9];
   w[9]=1.6E+1*w[9]*w[25];

 
    kern[0][0] = w[13];

    kern[0][1] = w[16];

    kern[0][2] =  - w[19];

    kern[0][3] = w[23];

 
    kern[1][0] = w[16];

    kern[1][1] = w[12];

    kern[1][2] =  - w[20];

    kern[1][3] = w[21];

 
    kern[2][0] =  - w[19];

    kern[2][1] =  - w[20];

    kern[2][2] = w[11];

    kern[2][3] =  - w[14];

 
    kern[3][0] = w[23];

    kern[3][1] = w[21];

    kern[3][2] =  - w[14];

    kern[3][3] = w[9];


}
