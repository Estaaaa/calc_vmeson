#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
//#include <math_ops.h>

void kernelL_notopti_qphv_longi(matCdoub& kern,
                              const ArrayScalarProducts& sp,
                const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){


    // local variables
    Cdoub denom=0.0;


    denom =  - 4.E+0*sp[35]*sp[27] + 4.E+0*pow(sp[34],2);

    kern[0][0] =  4.E+0*sp[35]*sp[27]*ang[1] - 1.2E+1*sp[34]*sp[43]*sp[27]*ang[0] - 4.E+0*
      sp[34]*sp[43]*sp[44]*ang[0] + 4.E+0*sp[34]*sp[43]*ang[1] + 4.E+0*pow(sp[34],2)*sp[27]*ang[0]
       + 4.E+0*pow(sp[34],2)*sp[44]*ang[0] - 8.E+0*pow(sp[34],2)*ang[1] + 8.E+0*pow(
      sp[43],2)*sp[27]*ang[0];
 
    kern[0][0] = (1.0/ denom)* kern[0][0]; 
    kern[0][1] =   - 6.E+0*sp[34]*sp[43]*sp[27]*ang[1] + 2.E+0*sp[34]*sp[43]*pow(sp[27],2)*
      ang[0] - 2.E+0*sp[34]*sp[43]*sp[44]*ang[1] - 2.E+0*sp[34]*sp[43]*pow(sp[44],2)*ang[0] + 
      4.E+0*sp[34]*sp[43]*ang[2] + 4.E+0*pow(sp[43],2)*sp[27]*sp[44]*ang[0] + 8.E+0*pow(
      sp[43],2)*sp[27]*ang[1] - 4.E+0*pow(sp[43],2)*pow(sp[27],2)*ang[0];
 
    kern[0][1] = (1.0/ denom)* kern[0][1]; 
    kern[0][2] =   0;
 
    kern[0][2] = (1.0/ denom)* kern[0][2]; 
    kern[0][3] =   0;
 
    kern[0][3] = (1.0/ denom)* kern[0][3]; 

    denom =  - 4.E+0*sp[35]*sp[34]*sp[27] + 4.E+0*pow(sp[34],3);

    kern[1][0] =  4.E+0*sp[35]*sp[34]*sp[27]*ang[0] - 4.E+0*sp[35]*sp[34]*sp[44]*ang[0] + 
      4.E+0*sp[35]*sp[34]*ang[1] - 4.E+0*sp[35]*sp[43]*sp[27]*ang[0] + 4.E+0*sp[35]*sp[43]*sp[44]*
      ang[0] - 4.E+0*sp[35]*sp[43]*ang[1] - 8.E+0*sp[34]*pow(sp[43],2)*ang[0] + 1.6E+1*
      pow(sp[34],2)*sp[43]*ang[0] - 8.E+0*pow(sp[34],3)*ang[0];
 
    kern[1][0] = (1.0/ denom)* kern[1][0]; 
    kern[1][1] =   - 4.E+0*sp[35]*sp[43]*sp[27]*sp[44]*ang[0] + 2.E+0*sp[35]*sp[43]*sp[27]*
      ang[1] + 2.E+0*sp[35]*sp[43]*pow(sp[27],2)*ang[0] + 2.E+0*sp[35]*sp[43]*sp[44]*ang[1] + 
      2.E+0*sp[35]*sp[43]*pow(sp[44],2)*ang[0] - 4.E+0*sp[35]*sp[43]*ang[2] + 4.E+0*sp[34]*
      pow(sp[43],2)*sp[27]*ang[0] - 4.E+0*sp[34]*pow(sp[43],2)*sp[44]*ang[0] - 8.E+0*sp[34]*
      pow(sp[43],2)*ang[1] - 4.E+0*pow(sp[34],2)*sp[43]*sp[27]*ang[0] + 4.E+0*pow(
      sp[34],2)*sp[43]*sp[44]*ang[0] + 4.E+0*pow(sp[34],2)*sp[43]*ang[1];
 
    kern[1][1] = (1.0/ denom)* kern[1][1]; 
    kern[1][2] =   0;
 
    kern[1][2] = (1.0/ denom)* kern[1][2]; 
    kern[1][3] =   0;
 
    kern[1][3] = (1.0/ denom)* kern[1][3]; 

    denom = 4.E+0*sp[34];

    kern[2][0] =   0;
 
    kern[2][0] = (1.0/ denom)* kern[2][0]; 
    kern[2][1] =   0;
 
    kern[2][1] = (1.0/ denom)* kern[2][1]; 
    kern[2][2] =  1.2E+1*sp[43]*ang[1];
 
    kern[2][2] = (1.0/ denom)* kern[2][2]; 
    kern[2][3] =   0;
 
    kern[2][3] = (1.0/ denom)* kern[2][3]; 

    denom =  - 1.6E+1*sp[35]*sp[34]*sp[27] + 1.6E+1*pow(sp[34],3);

    kern[3][0] =   0;
 
    kern[3][0] = (1.0/ denom)* kern[3][0]; 
    kern[3][1] =   0;
 
    kern[3][1] = (1.0/ denom)* kern[3][1]; 
    kern[3][2] =   0;
 
    kern[3][2] = (1.0/ denom)* kern[3][2]; 
    kern[3][3] =  1.6E+1*sp[35]*sp[43]*sp[27]*sp[44]*ang[0] + 8.E+0*sp[35]*sp[43]*sp[27]*ang[1]
       - 8.E+0*sp[35]*sp[43]*pow(sp[27],2)*ang[0] + 8.E+0*sp[35]*sp[43]*sp[44]*ang[1] - 8.E+0
      *sp[35]*sp[43]*pow(sp[44],2)*ang[0] + 3.2E+1*sp[34]*pow(sp[43],2)*sp[27]*ang[0] + 
      3.2E+1*sp[34]*pow(sp[43],2)*sp[44]*ang[0] - 1.6E+1*sp[34]*pow(sp[43],2)*ang[1] - 
      3.2E+1*pow(sp[34],2)*sp[43]*sp[44]*ang[0] - 3.2E+1*pow(sp[43],3)*sp[27]*ang[0];
 
    kern[3][3] = (1.0/ denom)* kern[3][3]; 

}


void kernelY_noopti_qphv_longi(matCdoub& kern,
  const ArrayScalarProducts& sp,  const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


    // local variables
    Cdoub denom;


    denom =  - 4.E+0*sp[35]*sp[44] + 4.E+0*pow(sp[43],2);

    kern[0][0] =   - 4.E+0*sp[35]*sp[44]*sp[7]*svp*svm - 4.E+0*sp[35]*sp[44]*ssp*
      ssm - 4.E+0*sp[43]*sp[31]*sp[37]*svp*svm - 4.E+0*sp[43]*sp[29]*sp[39]*svp*svm
       + 4.E+0*pow(sp[43],2)*sp[7]*svp*svm + 4.E+0*pow(sp[43],2)*ssp*ssm + 
      8.E+0*sp[31]*sp[29]*sp[44]*svp*svm;
 
    kern[0][0] = (1.0/ denom)* kern[0][0]; 
    kern[0][1] =  4.E+0*sp[43]*sp[31]*sp[44]*sp[37]*svp*svm + 4.E+0*sp[43]*sp[29]*sp[44]*
      sp[39]*svp*svm - 8.E+0*pow(sp[43],2)*sp[39]*sp[37]*svp*svm;
 
    kern[0][1] = (1.0/ denom)* kern[0][1]; 
    kern[0][2] =  4.E+0*sp[43]*sp[31]*sp[44]*ssp*svm + 4.E+0*sp[43]*sp[29]*sp[44]*svp*
      ssm - 4.E+0*pow(sp[43],2)*sp[39]*ssp*svm - 4.E+0*pow(sp[43],2)*sp[37]*svp*
      ssm;
 
    kern[0][2] = (1.0/ denom)* kern[0][2]; 
    kern[0][3] =  8.E+0*sp[35]*sp[43]*sp[44]*sp[39]*ssp*svm - 8.E+0*sp[35]*sp[43]*sp[44]*
      sp[37]*svp*ssm - 8.E+0*pow(sp[43],3)*sp[39]*ssp*svm + 8.E+0*pow(sp[43],3)*
      sp[37]*svp*ssm;
 
    kern[0][3] = (1.0/ denom)* kern[0][3]; 

    denom =  - 4.E+0*sp[35]*sp[43]*sp[44] + 4.E+0*pow(sp[43],3);

    kern[1][0] =  4.E+0*sp[35]*sp[31]*sp[37]*svp*svm + 4.E+0*sp[35]*sp[29]*sp[39]*svp
      *svm - 8.E+0*sp[43]*sp[31]*sp[29]*svp*svm;
 
    kern[1][0] = (1.0/ denom)* kern[1][0]; 
    kern[1][1] =   - 4.E+0*sp[35]*sp[43]*sp[44]*sp[7]*svp*svm - 4.E+0*sp[35]*sp[43]*
      sp[44]*ssp*ssm + 8.E+0*sp[35]*sp[43]*sp[39]*sp[37]*svp*svm - 4.E+0*pow(sp[43],2)*
      sp[31]*sp[37]*svp*svm - 4.E+0*pow(sp[43],2)*sp[29]*sp[39]*svp*svm + 4.E+0*
      pow(sp[43],3)*sp[7]*svp*svm + 4.E+0*pow(sp[43],3)*ssp*ssm;
 
    kern[1][1] = (1.0/ denom)* kern[1][1]; 
    kern[1][2] =  4.E+0*sp[35]*sp[43]*sp[39]*ssp*svm + 4.E+0*sp[35]*sp[43]*sp[37]*svp*
      ssm - 4.E+0*pow(sp[43],2)*sp[31]*ssp*svm - 4.E+0*pow(sp[43],2)*sp[29]*svp*
      ssm;
 
    kern[1][2] = (1.0/ denom)* kern[1][2]; 
    kern[1][3] =   - 8.E+0*sp[35]*sp[43]*sp[31]*sp[44]*ssp*svm + 8.E+0*sp[35]*sp[43]*
      sp[29]*sp[44]*svp*ssm + 8.E+0*pow(sp[43],3)*sp[31]*ssp*svm - 8.E+0*pow(
      sp[43],3)*sp[29]*svp*ssm;
 
    kern[1][3] = (1.0/ denom)* kern[1][3]; 

    denom = 4.E+0*sp[43];

    kern[2][0] =  4.E+0*sp[31]*ssp*svm + 4.E+0*sp[29]*svp*ssm;
 
    kern[2][0] = (1.0/ denom)* kern[2][0]; 
    kern[2][1] =  4.E+0*sp[43]*sp[39]*ssp*svm + 4.E+0*sp[43]*sp[37]*svp*ssm;
 
    kern[2][1] = (1.0/ denom)* kern[2][1]; 
    kern[2][2] =   - 4.E+0*sp[43]*sp[7]*svp*svm + 4.E+0*sp[43]*ssp*ssm;
 
    kern[2][2] = (1.0/ denom)* kern[2][2]; 
    kern[2][3] =  8.E+0*sp[43]*sp[31]*sp[37]*svp*svm - 8.E+0*sp[43]*sp[29]*sp[39]*svp
      *svm;
 
    kern[2][3] = (1.0/ denom)* kern[2][3]; 

    denom =  - 1.6E+1*sp[35]*sp[43]*sp[44] + 1.6E+1*pow(sp[43],3);

    kern[3][0] =   - 8.E+0*sp[35]*sp[39]*ssp*svm + 8.E+0*sp[35]*sp[37]*svp*ssm + 
      8.E+0*sp[43]*sp[31]*ssp*svm - 8.E+0*sp[43]*sp[29]*svp*ssm;
 
    kern[3][0] = (1.0/ denom)* kern[3][0]; 
    kern[3][1] =  8.E+0*sp[43]*sp[31]*sp[44]*ssp*svm - 8.E+0*sp[43]*sp[29]*sp[44]*svp*
      ssm - 8.E+0*pow(sp[43],2)*sp[39]*ssp*svm + 8.E+0*pow(sp[43],2)*sp[37]*svp*
      ssm;
 
    kern[3][1] = (1.0/ denom)* kern[3][1]; 
    kern[3][2] =   - 8.E+0*sp[43]*sp[31]*sp[37]*svp*svm + 8.E+0*sp[43]*sp[29]*sp[39]*
      svp*svm;
 
    kern[3][2] = (1.0/ denom)* kern[3][2]; 
    kern[3][3] =  1.6E+1*sp[35]*sp[43]*sp[44]*sp[7]*svp*svm - 1.6E+1*sp[35]*sp[43]*sp[44]
      *ssp*ssm - 3.2E+1*sp[35]*sp[43]*sp[39]*sp[37]*svp*svm - 3.2E+1*sp[43]*sp[31]*
      sp[29]*sp[44]*svp*svm + 3.2E+1*pow(sp[43],2)*sp[31]*sp[37]*svp*svm + 3.2E+1*
      pow(sp[43],2)*sp[29]*sp[39]*svp*svm - 1.6E+1*pow(sp[43],3)*sp[7]*svp*svm + 
      1.6E+1*pow(sp[43],3)*ssp*ssm;
 
    kern[3][3] = (1.0/ denom)* kern[3][3]; 


}


void kernelL_qphv_longi(matCdoub& kern,
              const ArrayScalarProducts& sp,
              const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){


    // local variables
    array<Cdoub, 24> w;
    Cdoub denom;


    w[1]=sp[35];
    w[2]=sp[27];
    w[3]=sp[34];
    w[4]=sp[43];
    w[5]=sp[44];
   w[6]=2.E+0*ang[1];
   w[7]=w[2]*ang[0];
   w[8]=w[5]*ang[0];
   w[9]=w[7] - w[6] + w[8];
   w[10]=pow(w[3],2);
   w[9]=w[9]*w[10];
   w[11]=2.E+0*w[4];
   w[12]=w[11]*w[7];
   w[13]=w[8] - ang[1];
   w[14]= - 3.E+0*w[7] - w[13];
   w[14]=w[3]*w[14];
   w[14]=w[14] + w[12];
   w[14]=w[4]*w[14];
   w[15]=w[1]*w[2];
   w[16]=ang[1]*w[15];
   w[9]=w[14] + w[16] + w[9];
   w[9]=4.E+0*w[9];
   w[14]=w[8] + ang[1];
   w[14]=w[14]*w[5];
   w[14]= - w[14] + 2.E+0*ang[2];
   w[16]= - 3.E+0*ang[1] + w[7];
   w[16]=w[2]*w[16];
   w[16]=w[16] + w[14];
   w[16]=w[3]*w[16];
   w[17]=w[7] - w[8];
   w[6]=w[6] - w[17];
   w[18]=w[2]*w[6]*w[11];
   w[16]=w[16] + w[18];
   w[16]=w[16]*w[11];
   w[18]=w[17] + ang[1];
   w[18]=w[18]*w[1];
   w[19]=2.E+0*ang[0];
   w[19]= - w[10]*w[19];
   w[19]=w[18] + w[19];
   w[19]=w[3]*w[19];
   w[20]=4.E+0*w[10];
   w[21]=w[11]*w[3];
   w[22]= - w[21] + w[20];
   w[22]=ang[0]*w[22];
   w[18]= - w[18] + w[22];
   w[18]=w[4]*w[18];
   w[18]=w[19] + w[18];
   w[18]=4.E+0*w[18];
   w[19]=2.E+0*w[8];
   w[22]=w[19] - w[7];
   w[23]=ang[1] - w[22];
   w[23]=w[2]*w[23];
   w[14]=w[23] - w[14];
   w[14]=w[1]*w[14];
   w[17]=ang[1] - w[17];
   w[17]=w[17]*w[10];
   w[6]= - w[6]*w[21];
   w[6]=w[6] + w[14] + 2.E+0*w[17];
   w[6]=w[6]*w[11];
   w[14]=1.2E+1*w[4]*ang[1];
   w[7]=2.E+0*w[7] + w[19] - ang[1];
   w[7]=w[3]*w[7];
   w[7]=w[7] - w[12];
   w[7]=w[7]*w[11];
   w[11]=ang[1] + w[22];
   w[11]=w[2]*w[11];
   w[12]= - w[5]*w[13];
   w[11]=w[12] + w[11];
   w[11]=w[1]*w[11];
   w[8]= - w[8]*w[20];
   w[7]=w[7] + w[11] + w[8];
   w[7]=8.E+0*w[4]*w[7];
   w[8]=w[15] - w[10];
   w[10]=4.E+0*w[8];
   w[8]=w[8]*w[3];
   w[11]= - 4.E+0*w[8];
   w[12]=4.E+0*w[3];
   w[8]= - 1.6E+1*w[8];


    denom =  - w[10];

    kern[0][0] = (1.0/ denom)* w[9];
 
    kern[0][1] = (1.0/ denom)* w[16];
 
    kern[0][2] = (1.0/ denom)*0.0;
 
    kern[0][3] = (1.0/ denom)*0.0;
 

    denom = w[11];

    kern[1][0] = (1.0/ denom)* w[18];
 
    kern[1][1] = (1.0/ denom)* w[6];
 
    kern[1][2] = (1.0/ denom)*0.0;
 
    kern[1][3] = (1.0/ denom)*0.0;
 

    denom = w[12];

    kern[2][0] = (1.0/ denom)*0.0;
 
    kern[2][1] = (1.0/ denom)*0.0;
 
    kern[2][2] = (1.0/ denom)* w[14];
 
    kern[2][3] = (1.0/ denom)*0.0;
 

    denom = w[8];

    kern[3][0] = (1.0/ denom)*0.0;
 
    kern[3][1] = (1.0/ denom)*0.0;
 
    kern[3][2] = (1.0/ denom)*0.0;
 
    kern[3][3] = (1.0/ denom)* w[7];
 

}


void kernelY_qphv_longi(matCdoub& kern,
  const ArrayScalarProducts& sp,  const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


    // local variables
    array<Cdoub, 33> w;
    Cdoub denom;


    w[1]=sp[35];
    w[2]=sp[44];
    w[3]=sp[7];
    w[4]=sp[43];
    w[5]=sp[31];
    w[6]=sp[37];
    w[7]=sp[29];
    w[8]=sp[39];
   w[9]=ssp*ssm;
   w[10]=svm*svp;
   w[11]=w[10]*w[3];
   w[12]=w[9] + w[11];
   w[13]=w[12]*w[4];
   w[14]=w[10]*w[7];
   w[15]=w[14]*w[8];
   w[10]=w[10]*w[6];
   w[16]=w[10]*w[5];
   w[17]=w[15] + w[16];
   w[13]=w[13] - w[17];
   w[13]=w[13]*w[4];
   w[18]=w[1]*w[2];
   w[19]= - w[12]*w[18];
   w[14]=w[14]*w[5];
   w[14]=2.E+0*w[14];
   w[20]=w[14]*w[2];
   w[19]=w[13] + w[20] + w[19];
   w[19]=4.E+0*w[19];
   w[21]=w[2]*w[17];
   w[10]=w[10]*w[8];
   w[10]=2.E+0*w[10];
   w[22]= - w[4]*w[10];
   w[21]=w[21] + w[22];
   w[22]=4.E+0*w[4];
   w[21]=w[21]*w[22];
   w[23]=svp*ssm;
   w[24]=w[7]*w[23];
   w[25]=svm*ssp;
   w[26]=w[5]*w[25];
   w[27]=w[24] + w[26];
   w[28]=w[2]*w[27];
   w[23]=w[6]*w[23];
   w[25]=w[8]*w[25];
   w[29]=w[23] + w[25];
   w[30]= - w[4]*w[29];
   w[28]=w[28] + w[30];
   w[28]=w[28]*w[22];
   w[30]=pow(w[4],2);
   w[18]=w[30] - w[18];
   w[30]=8.E+0*w[4];
   w[31]=w[18]*w[30];
   w[23]=w[23] - w[25];
   w[25]=w[23]*w[31];
   w[32]=w[1]*w[17];
   w[14]= - w[4]*w[14];
   w[14]=w[32] + w[14];
   w[14]=4.E+0*w[14];
   w[12]= - w[2]*w[12];
   w[12]=w[10] + w[12];
   w[12]=w[1]*w[12];
   w[12]=w[12] + w[13];
   w[12]=w[12]*w[22];
   w[13]=w[1]*w[29];
   w[32]= - w[4]*w[27];
   w[13]=w[13] + w[32];
   w[13]=w[13]*w[22];
   w[24]=w[24] - w[26];
   w[26]= - w[24]*w[31];
   w[27]=4.E+0*w[27];
   w[29]=w[29]*w[22];
   w[9]=w[9] - w[11];
   w[11]=w[9]*w[22];
   w[15]=w[15] - w[16];
   w[15]=w[15]*w[30];
   w[16]=w[1]*w[23];
   w[31]= - w[4]*w[24];
   w[16]=w[16] + w[31];
   w[16]=8.E+0*w[16];
   w[24]= - w[2]*w[24];
   w[23]=w[4]*w[23];
   w[23]=w[24] + w[23];
   w[23]=w[23]*w[30];
   w[24]=w[4]*w[9];
   w[17]=2.E+0*w[17] + w[24];
   w[17]=w[4]*w[17];
   w[9]= - w[2]*w[9];
   w[9]= - w[10] + w[9];
   w[9]=w[1]*w[9];
   w[9]=w[17] - w[20] + w[9];
   w[10]=1.6E+1*w[4];
   w[9]=w[9]*w[10];
   w[17]=4.E+0*w[18];
   w[20]=w[18]*w[22];
   w[10]=w[18]*w[10];


    denom = w[17];

    kern[0][0] = (1.0/ denom)* w[19];
 
    kern[0][1] = (1.0/ denom)* w[21];
 
    kern[0][2] = (1.0/ denom)* w[28];
 
    kern[0][3] = (1.0/ denom)* w[25];
 

    denom = w[20];

    kern[1][0] = (1.0/ denom)* w[14];
 
    kern[1][1] = (1.0/ denom)* w[12];
 
    kern[1][2] = (1.0/ denom)* w[13];
 
    kern[1][3] = (1.0/ denom)* w[26];
 

    denom = w[22];

    kern[2][0] = (1.0/ denom)* w[27];
 
    kern[2][1] = (1.0/ denom)* w[29];
 
    kern[2][2] = (1.0/ denom)* w[11];
 
    kern[2][3] = (1.0/ denom)*  - w[15];
 

    denom = w[10];

    kern[3][0] = (1.0/ denom)* w[16];
 
    kern[3][1] = (1.0/ denom)* w[23];
 
    kern[3][2] = (1.0/ denom)* w[15];
 
    kern[3][3] = (1.0/ denom)* w[9];
 


}

