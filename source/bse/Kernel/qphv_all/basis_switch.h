//
// Created by esther on 26.02.18.
//

#ifndef CALC_VMESON_BASIS_SWITCH_H
#define CALC_VMESON_BASIS_SWITCH_H


#include <Routing/dependence_of_all_kernels.h>
#include <Meson/MesonBse.h>

void translate_qphv_transverse_1(vector_MesonBse& vector, ps_MesonBse& scalar ){

    auto dependece = amplitude::depends_on_sphericals;
    Bse_ps ps = vector.getPs();
    int n_pp_vector = ps.AllGrid[dependece[0]]->getNumber_of_grid_points();
    int n_zp_vector = ps.AllGrid[dependece[1]]->getNumber_of_grid_points();
    ps_amplitude<3> new_vector({8,n_pp_vector,n_zp_vector});

    for (int i = 0; i < n_pp_vector ; ++i) {

        for (int j = 0; j < n_zp_vector; ++j) {


            new_vector({0,i,j}) = vector.meson({0,i,j}) + i_*scalar.meson({0,i,j});
            new_vector({1,i,j}) = vector.meson({1,i,j}) + 2.0/i_*scalar.meson({3,i,j});
            new_vector({2,i,j}) = vector.meson({2,i,j});
            new_vector({3,i,j}) = vector.meson({3,i,j});
            new_vector({4,i,j}) = vector.meson({4,i,j}) - i_*scalar.meson({2,i,j});
            new_vector({5,i,j}) = vector.meson({5,i,j}) + i_*scalar.meson({1,i,j});
            new_vector({6,i,j}) = vector.meson({6,i,j});
            new_vector({7,i,j}) = vector.meson({7,i,j});

        }
    }

    new_vector.setMass(vector.meson.getMass());
    vector.meson = new_vector;

    cout<<"Basis switch:: rewrote the qphv transverse basis = vector part of qphv. "<<endl;
}

//In case of Chebys: The sum over the angle is explicily performed to project out the angular dependence on z.
//For a calculation with normal angular depenence it shouldn't matter which of the translate-functions is used.
//Here: (1) projecting out the angle of l_i and h_i and then (2) translating it with angular dependence into f_i
void translate_qphv_transverse_2(vector_MesonBse& vector_v, ps_MesonBse& scalar_v ){

    auto dependece = amplitude::depends_on_sphericals;
    bool cheb_bool=false;
    if( vector_v.getWhich_solver() == "vector_cheb"){
        cheb_bool=true; vector_v.setWhich_solver("changed_here_for_write_out");
    }

    Bse_ps ps = vector_v.getPs();
    int n_pp_vector = ps.AllGrid[dependece[0]]->getNumber_of_grid_points();
    int n_zp_vector = ps.AllGrid[dependece[1]]->getNumber_of_grid_points();


    ps_amplitude<3> new_vector({8,n_pp_vector,n_zp_vector});
    ps_amplitude<3> vector({8,n_pp_vector,n_zp_vector});
    ps_amplitude<3> scalar({4,n_pp_vector,n_zp_vector});

    ArrayScalarProducts scalars;

    get_all_scalar_prod<1,0>({},{},{35}, ps, scalars);


    for (int i = 0; i < n_pp_vector ; ++i) {

        for (int j = 0; j < n_zp_vector; ++j) {

            if(cheb_bool){
                cheb Cheby(ps.chebs.getNumber_of_grid_points());

                for (int k = 0; k < 4; ++k) {
                    Cdoub value_s = 0.0;
                    for (int alpha = 0; alpha < ps.chebs.getNumber_of_grid_points(); ++alpha) {
                    value_s +=  scalar_v.meson.operator()({k,i,alpha})* Cheby.get(alpha,ps.zp.getGrid()[j]) * pow(vector_v.getCheby_factor(), alpha);// *sqrt(2.0);
//                    if(alpha == 0 ){value_s*sqrt(2.0);}
                    }
                    scalar({k,i,j}) = value_s;
                }

                for (int k = 0; k < 8; ++k) {
                    Cdoub value_v = 0.0;
                    for (int alpha = 0; alpha < ps.chebs.getNumber_of_grid_points(); ++alpha) {
                    value_v +=  vector_v.meson.operator()({k,i,alpha})* Cheby.get(alpha,ps.zp.getGrid()[j]) * pow(vector_v.getCheby_factor(), alpha); // *sqrt(2.0); }
//                    if(alpha == 0 ){value_v*sqrt(2.0);}
                    }
                    vector({k,i,j}) = value_v;
                }

            }else{
                vector = vector_v.meson;
                scalar = scalar_v.meson;
            }

            get_all_scalar_prod<1,2>({i,j},{dependece[0],dependece[1]},{34}, ps, scalars);


            //INSERT MATHEMATICA HERE:
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////

            new_vector({0,i,j}) = (-scalar({0,i,j}) + vector({0,i,j}) + vector({5,i,j})*pow(scalars[34],2))/scalars[35] ;

            new_vector({1,i,j}) = (2.0*scalar({3,i,j}) + vector({2,i,j}) - 2.0*vector({7,i,j}))/scalars[35] ;

            new_vector({2,i,j}) = -vector({1,i,j}) ;

            new_vector({3,i,j}) = -2.0*vector({3,i,j}) ;

            new_vector({4,i,j}) = (scalar({2,i,j}) - vector({4,i,j}))/scalars[35] ;

            new_vector({5,i,j}) = -((scalar({1,i,j}) + vector({6,i,j}))/scalars[35]) ;

            new_vector({6,i,j}) = -vector({5,i,j}) ;

            new_vector({7,i,j}) = -2.0*scalar({3,i,j}) + 2.0*vector({7,i,j}) ;
/*            new_vector({0,i,j}) = (-i_*(scalar({0,i,j}) - vector({0,i,j}) - vector({5,i,j})*pow(scalars[34],2)))/scalars[35] ;

            new_vector({1,i,j}) = (i_*(2.0*scalar({3,i,j}) + vector({2,i,j}) - 2.0*vector({7,i,j})))/scalars[35] ;

            new_vector({2,i,j}) = -i_*vector({1,i,j}) ;

            new_vector({3,i,j}) = -2.0*i_*vector({3,i,j}) ;

            new_vector({4,i,j}) = (i_*(scalar({2,i,j}) - vector({4,i,j})))/scalars[35] ;

            new_vector({5,i,j}) = (-i_*(scalar({1,i,j}) + vector({6,i,j})))/scalars[35] ;

            new_vector({6,i,j}) = -i_*vector({5,i,j}) ;

            new_vector({7,i,j}) = -2.0*i_*(scalar({3,i,j}) - vector({7,i,j})) ;*/


            ////////////////////////////////////////////////////////////////////////////////////////////////////////////


        }
    }

    new_vector.setMass(vector.getMass());
    vector_v.meson = new_vector;

    cout<<"Basis switch:: rewrote the qphv transverse basis = vector part of qphv. "<<endl;
}

//Here the Chebychev dependence is not projected out to the angle z again, but kept and transformed within.
void translate_qphv_transverse_3(vector_MesonBse& vector, ps_MesonBse& scalar ){

    auto dependece = amplitude::depends_on_sphericals;
    bool cheb_bool=false;
    if( vector.getWhich_solver() == "vector_cheb"){
//        dependece = amplitude::depends_on_sphericals_cheb; cheb_bool=true;
        dependece[1]=5;
        cheb_bool=true;
    }

    Bse_ps ps = vector.getPs();
    int n_pp_vector = ps.AllGrid[dependece[0]]->getNumber_of_grid_points();
    int n_zp_vector = ps.AllGrid[dependece[1]]->getNumber_of_grid_points();


    ps_amplitude<3> new_vector({8,n_pp_vector,n_zp_vector});

    ArrayScalarProducts scalars;

    get_all_scalar_prod<1,0>({},{},{35}, ps, scalars);


    for (int i = 0; i < n_pp_vector ; ++i) {

        for (int j = 0; j < n_zp_vector; ++j) {

//            if(cheb_bool){
//                get_all_scalar_prod_cheb<1,2>({i,j},{dependece[0],dependece[1]},{34}, ps, scalars);
//
//            }else{
                get_all_scalar_prod<1,2>({i,j},{dependece[0],dependece[1]},{34}, ps, scalars);
//            }

//            cout<<scalars[34]<<tab<<scalars[35]<<tab<<scalar.meson({0,i,j})<<tab<<vector.meson({6,i,j})<<endl;


            //INSERT MATHEMATICA HERE:
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////

            new_vector({0,i,j}) = (-scalar.meson({0,i,j}) + vector.meson({0,i,j}) + vector.meson({5,i,j})*pow(scalars[34],2))/scalars[35] ;

            new_vector({1,i,j}) = (2.0*scalar.meson({3,i,j}) + vector.meson({2,i,j}) - 2.0*vector.meson({7,i,j}))/scalars[35] ;

            new_vector({2,i,j}) = -vector.meson({1,i,j}) ;

            new_vector({3,i,j}) = -2.0*vector.meson({3,i,j}) ;

            new_vector({4,i,j}) = (scalar.meson({2,i,j}) - vector.meson({4,i,j}))/scalars[35] ;

            new_vector({5,i,j}) = -((scalar.meson({1,i,j}) + vector.meson({6,i,j}))/scalars[35]) ;

            new_vector({6,i,j}) = -vector.meson({5,i,j}) ;

            new_vector({7,i,j}) = -2.0*scalar.meson({3,i,j}) + 2.0*vector.meson({7,i,j}) ;



            ////////////////////////////////////////////////////////////////////////////////////////////////////////////


        }
    }

    new_vector.setMass(vector.meson.getMass());
    vector.meson = new_vector;

    cout<<"Basis switch:: rewrote the qphv transverse basis = vector part of qphv. "<<endl;
}

//Another try of this projecting out buisness, this time after the translatiion is performed.
//Here: (1) translating it into f_i and then (2) projecting out the angle of f_i.
void translate_qphv_transverse_4(vector_MesonBse& vector, ps_MesonBse& scalar ){

    auto dependece = amplitude::depends_on_sphericals;
    bool cheb_bool=false;
    if( vector.getWhich_solver() == "vector_cheb"){
        dependece = amplitude::depends_on_sphericals_cheb;
        cheb_bool=true; //vector.setWhich_solver("changed_here_for_write_out");
    }

    Bse_ps ps = vector.getPs();
    int n_pp_vector = ps.AllGrid[dependece[0]]->getNumber_of_grid_points();
    int n_ang_cheb = ps.AllGrid[dependece[1]]->getNumber_of_grid_points();


    ps_amplitude<3> new_vector({8,n_pp_vector,n_ang_cheb});
    ps_amplitude<3> new_new_vector({8,n_pp_vector,n_ang_cheb});

    ArrayScalarProducts scalars;

    get_all_scalar_prod<1,0>({},{},{35}, ps, scalars);


    for (int i = 0; i < n_pp_vector ; ++i) {

        for (int j = 0; j < n_ang_cheb; ++j) {


            if(cheb_bool){get_all_scalar_prod_cheb<1,2>({i,j},{dependece[0],dependece[1]},{34}, ps, scalars);}
            else{get_all_scalar_prod<1,2>({i,j},{dependece[0],dependece[1]},{34}, ps, scalars);}


            //INSERT MATHEMATICA HERE:
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////

            new_vector({0,i,j}) = (-scalar.meson({0,i,j}) + vector.meson({0,i,j}) + vector.meson({5,i,j})*pow(scalars[34],2))/scalars[35] ;

            new_vector({1,i,j}) = (2.0*scalar.meson({3,i,j}) + vector.meson({2,i,j}) - 2.0*vector.meson({7,i,j}))/scalars[35] ;

            new_vector({2,i,j}) = -vector.meson({1,i,j}) ;

            new_vector({3,i,j}) = -2.0*vector.meson({3,i,j}) ;

            new_vector({4,i,j}) = (scalar.meson({2,i,j}) - vector.meson({4,i,j}))/scalars[35] ;

            new_vector({5,i,j}) = -((scalar.meson({1,i,j}) + vector.meson({6,i,j}))/scalars[35]) ;

            new_vector({6,i,j}) = -vector.meson({5,i,j}) ;

            new_vector({7,i,j}) = -2.0*scalar.meson({3,i,j}) + 2.0*vector.meson({7,i,j}) ;


            ////////////////////////////////////////////////////////////////////////////////////////////////////////////


        }
    }


    if(cheb_bool) {
        cheb Cheby(n_ang_cheb);

        for (int i = 0; i < n_pp_vector; ++i) {
            for (int j = 0; j < n_ang_cheb; ++j) {
                for (int k = 0; k < 8; ++k) {

                    Cdoub value = 0.0;
                    for (int alpha = 0; alpha < ps.chebs.getNumber_of_grid_points(); ++alpha) {
//                        value += new_vector({k, i, alpha}) * Cheby.get(alpha, ps.zp.getGrid()[j]) *
//                                 pow(vector.getCheby_factor(), alpha);

//                        value += new_vector({k, i, alpha}) * Cheby.get(alpha, j) *
//                                 pow(vector.getCheby_factor(), -alpha);

                        value += new_vector({k, i, alpha}) * Cheby.get(alpha, ps.AllGrid[dependece[1]]->getGrid()[j]) *
                                 pow(vector.getCheby_factor(), -alpha);


//                    if(alpha == 0 ){value_s*sqrt(2.0);}
                    }
                    new_new_vector({k, i, j}) = value;  }}
        }
    }else{
        new_new_vector = new_vector;
    }



    //comment: is this still needed anyway? a Mass in case of oa qphv seems not sufficient.?
        new_new_vector.setMass(vector.meson.getMass());
        vector.meson = new_new_vector;

    cout<<"Basis switch:: rewrote the qphv transverse basis = vector part of qphv. "<<endl;
}





















//-------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------
/*
void readInn_form_Qphv_datafile(vector_MesonBse& vector,string filename){
    //Note: The ReadInn and write routine are tailored to each other, so watch out when changing one!

//    ps.setMass(Mass); /// updates the mass in phase space.cpp
//    meson.setMass(Mass);
    cout<<filename<<endl;

//    double Rgrid,Igrid;
//    double R1, R2, R3, R4, R5, R6, R7, R8;
//    double I1, I2, I3, I4, I5, I6, I7, I8;
    VecCdoub safe;
    VecCdoub cosx;
    VecCdoub rad;

    ifstream read;
    read.open(filename, ios::in);



    if(read.is_open())
    {
        string daten;
        while(getline(read,daten))
        {

            VecDoub dummy; string s = ""; int pos;

            if(daten=="") {continue;}
            if(daten[0] == '#') {continue;}


            //Reading in the amplitude:
            for (int j = 0; j <daten.size() ; ++j) {
                s += daten[j];
                if(daten[j] == '\t')
                {
                    dummy.push_back(stod(s));
                    s = "";
                }
//                if( c <= 'a'  ){}//Check osif all of the read in data are numbers.
            }
//            cout<<dummy.size()<<endl;

            for (int k = 3; k < (dummy.size()) ; ++k) {
                safe.push_back(dummy[k]);
            }
            cosx.push_back(dummy[2]);
            rad.push_back(dummy[1]);

            if(daten=="") {continue;}
            if(daten[0] == '#') {continue;}



        }
    }else{cout<<"MesonBSE::readIn: No data file found!"<<endl; assert( false);}
    read.close();

    vector.meson.setAmplitude(safe);


    cout<<"A Meson has been read inn form Ricards file: "<<filename<<"  with: n_total="<<safe.size()<<endl;




}

void translate_qphv_transverse_switch_back_compa_Ricard(vector_MesonBse& vector_v, ps_MesonBse& scalar_v ){

    auto dependece = amplitude::depends_on_sphericals;
    bool cheb_bool=false;
    if( vector_v.getWhich_solver() == "vector_cheb"){
//        dependece = amplitude::depends_on_sphericals_cheb; cheb_bool=true;
//        dependece[1]=5;
        cheb_bool=true;
    }

    Bse_ps ps = vector_v.getPs();
    int n_pp_vector = ps.AllGrid[dependece[0]]->getNumber_of_grid_points();
    int n_zp_vector = ps.AllGrid[dependece[1]]->getNumber_of_grid_points();


    ps_amplitude<3> new_vector({8,n_pp_vector,n_zp_vector});
    ps_amplitude<3> vector({8,n_pp_vector,n_zp_vector});
    ps_amplitude<3> scalar({4,n_pp_vector,n_zp_vector});

    ArrayScalarProducts scalars;

    get_all_scalar_prod<1,0>({},{},{35}, ps, scalars);


    for (int i = 0; i < n_pp_vector ; ++i) {

        for (int j = 0; j < n_zp_vector; ++j) {

            if(cheb_bool){
                cheb Cheby(ps.chebs.getNumber_of_grid_points());

                Cdoub value_s = 0.0;
                for (int k = 0; k < 4; ++k) {
                    for (int alpha = 0; alpha < ps.chebs.getNumber_of_grid_points(); ++alpha) {
                        value_s +=  scalar_v.meson.operator()({k,i,alpha})* Cheby.get(alpha,ps.zp.getGrid()[j]) * pow(vector_v.getCheby_factor(), alpha) *sqrt(2.0); }
                    scalar({k,i,j}) = value_s;
                }

                Cdoub value_v = 0.0;
                for (int k = 0; k < 8; ++k) {
                    for (int alpha = 0; alpha < ps.chebs.getNumber_of_grid_points(); ++alpha) {
                        value_v +=  vector_v.meson.operator()({k,i,alpha})* Cheby.get(alpha,ps.zp.getGrid()[j]) * pow(vector_v.getCheby_factor(), alpha) *sqrt(2.0); }
                    vector({k,i,j}) = value_v;
                }

            }else{
                vector = vector_v.meson;
                scalar = scalar_v.meson;
            }

            get_all_scalar_prod<1,2>({i,j},{dependece[0],dependece[1]},{34}, ps, scalars);


            //INSERT MATHEMATICA HERE:
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////

            new_vector({0,i,j}) = -scalar({0,i,j}) - vector({0,i,j})*scalars[35] - vector({6,i,j})*pow(scalars[34],2) ;

            new_vector({1,i,j}) = vector({2,i,j}) ;

            new_vector({2,i,j}) = -vector({7,i,j}) - vector({1,i,j})*scalars[35] ;

            new_vector({3,i,j}) = vector({3,i,j})/2. ;

            new_vector({4,i,j}) = -scalar({2,i,j}) + vector({4,i,j})*scalars[35] ;

            new_vector({5,i,j}) = vector({6,i,j}) ;

            new_vector({6,i,j}) = scalar({1,i,j}) + vector({5,i,j})*scalars[35] ;

            new_vector({7,i,j}) = -scalar({3,i,j}) - vector({7,i,j})/2. ;
*//*            new_vector({0,i,j}) = (-i_*(scalar({0,i,j}) - vector({0,i,j}) - vector({5,i,j})*pow(scalars[34],2)))/scalars[35] ;

            new_vector({1,i,j}) = (i_*(2.0*scalar({3,i,j}) + vector({2,i,j}) - 2.0*vector({7,i,j})))/scalars[35] ;

            new_vector({2,i,j}) = -i_*vector({1,i,j}) ;

            new_vector({3,i,j}) = -2.0*i_*vector({3,i,j}) ;

            new_vector({4,i,j}) = (i_*(scalar({2,i,j}) - vector({4,i,j})))/scalars[35] ;

            new_vector({5,i,j}) = (-i_*(scalar({1,i,j}) + vector({6,i,j})))/scalars[35] ;

            new_vector({6,i,j}) = -i_*vector({5,i,j}) ;

            new_vector({7,i,j}) = -2.0*i_*(scalar({3,i,j}) - vector({7,i,j})) ;*//*


            ////////////////////////////////////////////////////////////////////////////////////////////////////////////


        }
    }

    new_vector.setMass(vector.getMass());
    vector_v.meson = new_vector;

    cout<<"Basis switch:: Now from Ricards basis to my vector basis "<<endl;
}*/
#endif //CALC_VMESON_BASIS_SWITCH_H
