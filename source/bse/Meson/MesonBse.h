//
// Created by esther on 23.10.17.
//

#ifndef CALC_VMESON_MESONBSE_H
#define CALC_VMESON_MESONBSE_H


#include <Parameter.h>
#include <ps_amplitude.h>
#include <SolverWrapper.h>
#include <QuarkDse.h>
#include <MultiONew.h>
#include <Phasespace.h>
#include <Routing/scalarP.h>
#include <Routing/dependence_of_all_kernels.h>
#include <mytime.h>
#include <BaseInterpolPE.h>
#include "typedefMeson.h"

//To do: Get rid of useless stuff in the class!! And order it better: eg. what_to-calculate_flag ?

class MesonBse {


//Constructor & Destructor:
public:
    MesonBse(ps_amplitude<3> &meson, QuarkDse *dse1, QuarkDse *dse2, string which_solver, string parameter_file,
             string output_folder, bool interpolation_nessercary, array<bool,5> flag, string which_meson) :
            meson(meson), dse1(dse1), dse2(dse2), parameter(parameter_file), ps(parameter),
            folder(output_folder), which_solver(which_solver), interpolation_nessercary(interpolation_nessercary),
            what_to_calc_flag(flag), which_meson(which_meson) {

            Z2_Z2 = dse1->getQuark().Z2 * dse2->getQuark().Z2;

        //Here I have to pick if I want a method that uses the getMatrix or the update function.
        const int size_pion = (const int) meson.getSize();
        //Important Steps: Which Solver is used?
        //Different option for the solver choice:
        //(1) Using the "Old Method": Here the whole matrix is passed to the power method and then the iteration is performed.
        //Options: a chebychev(matrix_cheb) version and a angular version(matrix)
        //(2) "power method": Solve the equation using an update vector function which performs M*V product, or interpolates and then performs the product.
        //Options: Cheby, direkt angle with seperated Y L kernel calcuation, and direct angle full matrix calcualtion  (vector_fullkernel)
        //Here the difference comes in when getting the matrix, even here the kernel matrix is precalcauted and stored and updateVector only performs the matrix vector product.
        //(3) The solver for the inhomogneous equation, add the inhomogenous term in the iteration. This get's choosen when the which_meson ==  qphv.
        //There is no option for full matrix version when using the qphv, just because it seemed not nesserary and the implementation was old anyway.
        if(which_meson != "qphv" ){
            if(which_solver=="matrix")
            {
                solver = new SolverWrapper<MesonBse>("Old_method", *this, &MesonBse::get_Matrix );

            }else if(which_solver == "matrix_cheb"){
                solver = new SolverWrapper<MesonBse>("Old_method", *this, &MesonBse::get_Matrix_Cheb );

            }

            else if(which_solver == "vector" || which_solver == "vector_fullkernel" || which_solver == "vector_cheb"){
                solver = new SolverWrapper<MesonBse>("power_methode", *this, &MesonBse::updateVector, size_pion );

            }else{cout<<"MesonBse.h :: No solver set! - Working with qphv? "<<endl; } //assert(false);}


        }else{

            if(which_solver== "full_kernel_KG")
            { solver = new SolverWrapper<MesonBse>("power_methode_qphv", *this, &MesonBse::get_Matrix_times_vector, size_pion );
                cout<<"Instant calc of -Choose qphv mode."<<endl;
            }else{
                solver = new SolverWrapper<MesonBse>("power_methode_qphv", *this, &MesonBse::updateVector, size_pion );
                cout<<"Choose qphv mode. - watch out!"<<endl; }

        }




        //Important Steps: Setting up the gluon for the calcuations
        //Here the parameters of the Gluon are set, in this case as the same parameters as in the quarks.
        GluonWrapper             gl_wrapper({dse1->getParameter().d_mt, dse1->getParameter().omega_mt, dse1->getParameter().pv_cutoff},
                                            dse1->getParameter().which_gluon);
        gluon =  new  MultiONew<gluon::n_sphericals,  Cdoub, GluonWrapper, false>( gl_wrapper);


    }


    virtual ~MesonBse(){
//      ~MesonBse(){

        delete gluon;
        delete solver;

        //As quarks are deleted when using eg the algorithm for multiple mass, they can't be deleted here. - thus introduced if.
//        delete quark1; delete quark2;
        if(!already_deleted_quarks){
            delet_multi_quarks();
        }


        //Cannot delete a function...?
        //todo: Watch out the dse were just passed to the BSE class, so they are destroyed outside not here!
        //        delete dse1; delete dse2;
//        delete normalisation; delete calc_fpi; delete kernelL; delete kernelall;
//        delete kernelY; delete kernelInhomo;
        cout<<"deleting MesonBse"<<endl;

    }

//typedef for the type of the multiO definition of the quarks:
    typedef MultiONew<quark_p1::n_sphericals, array<Cdoub, 2>, QuarkDse, true> Q1;
    typedef MultiONew<quark_p2::n_sphericals, array<Cdoub, 2>, QuarkDse, true> Q2; //scalarP<quark_p2:: n_sperhicals, 2>

//Now all functions and memnbers that belong to the MesonBse class.
//---------------------------------------------------------------------------------------------------------------------------------------------
//--------------------- FUNCTIONS --------------------
//public functions:

    //// this should be called from outside the bse class to calculate the amplitude and eigenvalue for given Mass
    void solve_and_update_pion(Cdoub Psquared){

        if(Psquared == 0.0){cout<<"The mass of the boundstate is zero! on purpose? P²=0"<<endl; assert(false);}

        //Setting the mass of the meson in serval place,
        // 1 in the phase_space documents cause it is important for the routing
        // 2 in the meson, as it has a member mass.
//        Mass = Mass;  //What was this suppose to do?
//        double Mass = - real(sqrt(Psquared)*i_);
//        ps.setPsquared(Mass); /// updates the mass in phase space.cpp
        //important: cheap fix taking the real part here! Watch out when using real complex values for P²
        meson.setPsquared(real(Psquared));
        ps.setPsquared(Psquared); //important: Had to switch sqrt()-> no root...!?
//        if(real(Psquared) < 0 && which_solver == "vector_cheb"){cheby_factor = i_;}else{cheby_factor= 1.0;}
        if(real(Psquared) < 0){cheby_factor = i_;}else{cheby_factor= 1.0;}
        cout<<endl<<"______________________________________________________________"<<endl;
        cout<<"_____________________solve and update meson___________________"<<endl;
//        if(Mass !=0 ){cout<<tab<<tab<<tab<<"Calcuation for M_BS = "<<Mass<<endl;}
        cout<<tab<<tab<<tab<<"Calcuation for P = (0,0,0,"<<sqrt(Psquared)<<") "<<"and Chebyfactor="<<cheby_factor<<endl;


        //Next we update all Mutliobjects, aka set the MultiO's
        // This is basically just pre calculating the quarks. Now that the mass is set
        // we know at which points we need to evaluate S(q²).
        update_multiobjects();



        //Starting the iterative calculation.
        time_t starting, dasende;
        time(&starting);


        //Depending on the flag in constructor of BSE we choose which method is used to obtain the kernel matrix. Specification See above.
        if(which_solver == "vector" ){
            kernel = get_Matrix(); }
        else if(which_solver == "vector_fullkernel" ){
            kernel = get_Matrix_all_in_one_go();  cout<<"-----Using the direct full Kernel calculation----"<<endl; }
        else if( which_solver=="vector_cheb" ){
            kernel = get_Matrix_Cheb(); }

        // pre_calculate the inhomogenous part if nessercary
        if(which_meson =="qphv"){
            inhomogenous_part = precalc_inhomogenous_part();}

//        write_out_kernel(folder+ "kernel", 0);
//        write_out_kernel(folder+ "kernel", 1);
//        write_out_kernel(folder+ "kernel", 2);
//        write_out_kernel(folder+ "kernel", 3);
//        write_out_kernel(folder+ "kernel", 4);
//        write_out_kernel(folder+ "kernel", 5);



        cout<<endl<<"----------------------------------------------"<<endl;
        cout<<"-------------Calculating Boundstate------------"<<endl;
        solver->basesolver->solve(1.e-14, 100); /// calls the power method or which ever method is solving the equation.

        time(&dasende); give_time(starting,dasende,"Calculated the bound-sate total.");cout<<endl;


        /// update the pion:
        meson.setEigenvalue(solver->basesolver ->getBiggest_eigenvalue());
        meson.setIs_calculated(true);

        //todo: okay, so I enable this delete here. But I need to set it somewhere else. - now it gets done in the destructor and when interpolating for a mass.
//        delet_multi_quarks();
    }

    void norm_the_meson(){

        time_t starting, dasende;
        time(&starting);

        calc_boundstate_norm(1e-7);

        time(&dasende); give_time(starting,dasende,"Calculated the norm.");

//        write_out_amplitude(folder+"meson_amplitude_normed.txt");

        cout<<"----------------------------------------------"<<endl;
        cout<<"----------------------------------------------"<<endl<<endl;

    }

    //Public write and read functions.
    Cdoub read_amplitude(string filename);
    void write_out_amplitude(string filename);
    void write_out_amplitude(string filename, bool project_out_chebys);
    void write_out_amplitude(string filename, const VecCdoub &amplitude, array<int,2> variable_depends_on );
//    void read_bs(string filename);

    void calculate_chi(ps_amplitude<3> &chi, vector<int>& amp_flag);
    void calculate_two_quarks(Cdoub Psquare);


    double find_the_bs_mass(double meson_mass, double range, double eps);

    VecCdoub get_value_iteration(Cdoub pp, Cdoub zp);
    void project_back_out_the_angle_inside();

    //init the bse via file
    void init_via_file(string bse_amplitude, Cdoub Psquared, int which_vertex_quark);

    //reset memory and functions:
    void free_memory();
    virtual void free_more_memory()=0;
    void reset_inhomogenous();

//---------------------------------------------------------------------------
//Setter & Getter

    //Setter
    //Setters need to be introduced for the inherited qphv class.
    void setSolver(const SolverWrapper<MesonBse> *solver);
    void setWhich_solver(const string &which_solver);
    void setPhaseSpacePsquared(Cdoub &P4);
    void setFolder(const string &folder);
    void setQuark_safer(const string &quark_safer);
    void setWhat_to_calc_flag(const array<bool, 5> &what_to_calc_flag);
    void setAlready_deleted_quarks(bool already_deleted_quarks);


    //Getter
    //Public function related to the qphv aka multiple P² values calculations.
    const VecCdoub &getInhomogenous_part() const;
    void calc_for_different_Psq(double Ps_start, double Ps_end, int Ps_n);
    void calc_for_different_Psq(VecDoub Ps, string filename);

    //Getter
    double getZ2_Z2() const;
    const Bse_ps &getPs() const;
    const string &getWhich_solver() const;
    const Cdoub &getCheby_factor() const;
    //Function from me created to get the information of the quarks before class is destroyed. Does that work?
    void get_me_content_of_quarks(vector<array<Cdoub,2>>& quark1, vector<array<Cdoub,2>>& quark2);
    double get_decay_constant();
    Cdoub getPhaseSpacePsquared() const;
    int getNBasis() const;


//public Objects:
    ps_amplitude<3> &meson;

// -------------------- PRIVATE ------------------------------------------------------------
//private functions and objects

protected:

//important the 2 functions below have to be used together for most of the times.

//private Functions defined in here in the header:
    void delet_multi_quarks(){

        delete quark1;
        delete quark2;
        already_deleted_quarks=true;
    }

    //important: THERE IS A NEW() HERE - so make sure to have a delete as well. The delete function is above
    void update_multiobjects() {


        ///Basically just calcuated the quarks at this point! And writes them into a file.

        //Router for pre_calc objects
        scalarP<quark_p1::n_sphericals, quark_p1::depends_on_scalar_products.size()> rq1
                (quark_p1::depends_on_scalar_products, quark_p1::depends_on_sphericals, ps);
        scalarP<quark_p2::n_sphericals, quark_p2::depends_on_scalar_products.size()> rq2
                (quark_p2::depends_on_scalar_products, quark_p2::depends_on_sphericals, ps);

        //Super Indices for quarks
        SuperIndex <quark_p1::n_sphericals> siq1_cont({parameter.n_ll, parameter.n_lz});
        SuperIndex <quark_p2::n_sphericals> siq2_cont( {parameter.n_ll, parameter.n_lz});



        /// Initialize and precalculate the quarks
        time_t starting, dasende;
        time(&starting);

        quark1 = new Q1(  siq1_cont);
        quark2 = new Q2(  siq2_cont);
        already_deleted_quarks=false;

        if(what_to_calc_flag[0] == false){
            quark1->read(folder+quark_safer+"Cquark1.txt");
        }else{
            quark1->calc( *dse1  , rq1);
            quark1->write(folder+quark_safer+"Cquark1.txt",rq1 ); }


        time(&dasende); give_time(starting,dasende,"Cquark 1");
        if(what_to_calc_flag[1] == false){
            quark2->read(folder+quark_safer+"Cquark2.txt");
        }else{
            quark2->calc( *dse2  , rq2);
            quark2->write(folder+quark_safer+"Cquark2.txt",rq2 ); }

        time(&starting); give_time(dasende, starting,"Cquark 2");


    }



//------------------Objects - Private : ----------------------------------------------------------------------------
//private Objects

    //Note: The order of parameter and ps is important here! Cause parameter needs to be initalized first in order to fill ps!!!!!
    const SolverWrapper<MesonBse>* solver = nullptr;
    BseParameter parameter;
    Bse_ps ps;
    matCdoub kernel;


    //Objects
    QuarkDse *dse1 = nullptr;
    QuarkDse *dse2 = nullptr;
    Q1 *quark1;
    Q2 *quark2;
    MultiONew<gluon::n_sphericals, Cdoub, GluonWrapper, false> *gluon;

    //in case of the qphv or any other inhomogenous BSE.
    VecCdoub inhomogenous_part;

    //For a BSE with multiple values fpr P²
//    Line Psquare;
    VecDoub Psquare;


    //--------------------------------------------------------------------------------------------------------------
    //Flags and strings for which solver, mesons.... and constants.

    array<bool, 5> what_to_calc_flag;
    string folder;
    string which_solver;
    string which_meson;
    bool interpolation_nessercary;
    double Z2_Z2 = NAN;
    Cdoub cheby_factor;
    bool already_deleted_quarks=false;
    string quark_safer="";

    //Pointer for kernel functions
    FunPtrtoNorm normalisation;
    FunPtrtoDecay calc_fpi;
    FunPtrtoKernelL kernelL;
    FunPtrtoKernelAll kernelall;
    FunPtrtoKernelY kernelY;
    FunPtrInhomogenous kernelInhomo;
    FunPtrtoKernelY kernelRotationY;

//-----------------------------------Functions: private:  -----------------------------------
//More private Functions:  -all called as part of the full BSE caculation in some sort of way

    //pre calculating and safing the inhomogenous part
    virtual VecCdoub precalc_inhomogenous_part();

    //Calcuting the kernel matrix K or L, Y. With chebys or without.
    matCdoub get_Matrix();
    matCdoub get_Matrix_all_in_one_go();
    void get_Matrix_times_vector(const VecCdoub &amplitude, VecCdoub &amplitude_updated);
    matCdoub get_Matrix_Cheb();
    void updateVector(const VecCdoub &, VecCdoub &);
    void updateVector_without_kernelMat(const VecCdoub &, VecCdoub &);

    matCdoub precalc_angles();
    matCdoub precalc_angles_Cheb();

    matCdoub precalc_Lmat();
    matCdoub precalc_Lmat_Cheb();

    matCdoub precalc_rotation_Y();
    matCdoub calculate_rotationmatrix_for_extrernal_p();


    //Norm and decay constant functions.
    void calc_boundstate_norm(double epsilon);
    Cdoub boundstate_norm(double& currentmass, double& epsmass);
    Cdoub decay_constant();


    //interpolation related stuff
    bool interpolation_check();
    matDoub xvalues_interpol;
    virtual void interpolation_start();
    virtual void interpolation_update(const VecCdoub &amplitude,  VecCdoub& local_amplitude) =0;
//    void interpolation_on_outside_grid(ps_amplitude& new_meson);
    void set_kernel_functions();


    //Functions for writing out the meson amplitude:
    void write_multiple_P(string filename, int Psq_value);
    void write_out_other_amplitude(string filename, ps_amplitude<3>& chi);
    void write_out_kernel(string filename, int i);

    //For thesting the interpolation - can be deleted at some point-...
    VecCdoub testy();


    //Old:????  do I still need this:
/*
    void RhsAmplitude(const VecCdoub &amplitude, VecCdoub &amplitude_updated);
    void RhsWavefunction(const VecCdoub &amplitude, VecCdoub &amplitude_updated);

    void integrate_over_full_kernel();
    void integrate_over_gmat_kstart();*/

};


//Expicit implementations of the pseudo scalar meson and the vector meson
// Both inherit form the main_BSE bse class
// The main_BSE reason for this was the implementation of the interpolation structuire that requires some templates parameters including n_basis = vec_dim.

#define K_N_BASIS_PS 4

class ps_MesonBse : public MesonBse {

public:
    ps_MesonBse(ps_amplitude<3> &meson, QuarkDse *dse1, QuarkDse *dse2, string which_solver, string parameter_file,
                    string output_folder, bool interpolation_nessercary, array<bool,5> flag, string which_meson) :
            MesonBse(meson, dse1, dse2, which_solver, parameter_file, output_folder, interpolation_nessercary, flag, which_meson){

//        cout<<"||    ----------------------------------------------     ||"<<endl<<"      ---------pseudo scalar meson----------------       "<<endl;

        //setting the basis elements in the parameterfile
        parameter.n_basis = parameter.n_projector = K_N_BASIS_PS;

        //setting the kernel functions to calculate the kernel, norm...
        set_kernel_functions();

        //Here we check if an interpolation is nessercary and if we accumulate the x-values, as they are already known.
        interpolation_start();
    }


    virtual ~ps_MesonBse(){}

    void free_more_memory(){
        Interpol=SplineInterpolator<2, K_N_BASIS_PS, double, Cdoub>();
    }

private:

    //todo: also set it up such that the interpoaltionc choice is more abitrary?
////    BaseInterpol<2, 4, double, Cdoub>* Interpol = nullptr;
    SplineInterpolator<2, K_N_BASIS_PS, double, Cdoub> Interpol;

    void interpolation_update(const VecCdoub &amplitude,  VecCdoub& local_amplitude) {

        //The local amplitude here should be the amplitude inside, as this one is getting interpolated.

        //This function interpolate the values of amplittude on the wanted grid points x' safed in xvalues_interpol.
        //The corresponding x-values for f(x) are already set in interpolation_start() above.
        Interpol.Update( xvalues_interpol, amplitude);

        array<Cdoub, K_N_BASIS_PS> result_y;
        SuperIndex<3> Si_I({parameter.n_projector,parameter.n_ll, parameter.n_lz});
        SuperIndex<2> Si_J({parameter.n_ll, parameter.n_lz});
//        SuperIndex<3> Si_I({parameter.n_projector,parameter.n_pp, parameter.n_pz}); ///?????????? wrong

//#pragma omp parallel for
        for (int i = 0; i < Si_J.getNsuper_index() ; ++i) {

            auto indices = Si_J.are(i);
            //Here should be choosen according to the when filling the vectorin interpolation_start(): -> Check beforehand.
            // (Now set to log, as this is interpolation with equal distances.)//important:
            array<double,2> new_x_values ={log(ps.AllGrid[2]->getGrid()[indices[0]]),ps.AllGrid[3]->getGrid()[indices[1]]};
//            array<double,2> new_x_values ={ps.AllGrid[2]->getGrid()[indices[1]],ps.AllGrid[3]->getGrid()[indices[2]]};

            //performing the interpolation only one time for all amplitudes t[i]. E,F,G,H.
            Interpol.getValue(result_y,new_x_values );

            for (int j = 0; j < K_N_BASIS_PS; ++j) {
//                auto index = Si_I.are(i+j);
                local_amplitude[Si_I.is({j, indices[0], indices[1]})] = result_y[j];
//                cout<<"l["<<i+j<<"]= "<<local_amplitude[i+j]<<endl;
            }

//            local_amplitude[i] = result_y[indices[0]];

        }


    }

};


#define K_N_BASIS_VE 8

class vector_MesonBse : public MesonBse {

public:
    vector_MesonBse(ps_amplitude<3> &meson, QuarkDse *dse1, QuarkDse *dse2, string which_solver, string parameter_file,
    string output_folder, bool interpolation_nessercary, array<bool,5> flag, string which_meson) :
            MesonBse(meson, dse1, dse2, which_solver, parameter_file, output_folder, interpolation_nessercary, flag, which_meson){

//        cout<<"||    ----------------------------------------------     ||"<<endl<<"      ------------vector meson-------------------       "<<endl;

        //setting the basis elements in the parameterfile
        parameter.n_basis = parameter.n_projector = K_N_BASIS_VE;

        //setting the kernel functions to calculate the kernel, norm...
        set_kernel_functions();

        //Here we check if an interpolation is nessercary and if we accumulate the x-values, as they are already known.
        interpolation_start();
    }


    virtual ~vector_MesonBse(){}

    void free_more_memory(){
        Interpol=SplineInterpolator<2, K_N_BASIS_VE, double, Cdoub>();
    }

private:

    //todo: also set it up such that the interpoaltionc choice is more abitrary?
    SplineInterpolator<2, K_N_BASIS_VE, double, Cdoub> Interpol;

    void interpolation_update(const VecCdoub &amplitude,  VecCdoub& local_amplitude) {

        //The local amplitude here should be the amplitude inside, as this one is getting interpolated.

        //This function interpolate the values of amplittude on the wanted grid points x' safed in xvalues_interpol.
        //The corresponding x-values for f(x) are already set in interpolation_start() above.
        Interpol.Update( xvalues_interpol, amplitude);

        array<Cdoub, K_N_BASIS_VE> result_y;
        SuperIndex<3> Si_I({parameter.n_projector,parameter.n_ll, parameter.n_lz});
        SuperIndex<2> Si_J({parameter.n_ll, parameter.n_lz});
//        SuperIndex<3> Si_I({parameter.n_projector,parameter.n_pp, parameter.n_pz}); ///?????????? wrong

//#pragma omp parallel for
        for (int i = 0; i < Si_J.getNsuper_index() ; ++i) {

            auto indices = Si_J.are(i);
            //Here should be choosen according to the when filling the vectorin interpolation_start(): -> Check beforehand.
            // (Now set to log, as this is interpolation with equal distances.)
            array<double,2> new_x_values ={log(ps.AllGrid[2]->getGrid()[indices[0]]),ps.AllGrid[3]->getGrid()[indices[1]]};
//            array<double,2> new_x_values ={ps.AllGrid[2]->getGrid()[indices[1]],ps.AllGrid[3]->getGrid()[indices[2]]};

            //performing the interpolation only one time for all amplitudes t[i]. E,F,G,H.
            Interpol.getValue(result_y,new_x_values );

            for (int j = 0; j < K_N_BASIS_VE; ++j) {
//                auto index = Si_I.are(i+j);
                local_amplitude[Si_I.is({j, indices[0], indices[1]})] = result_y[j];
//                cout<<"l["<<i+j<<"]= "<<local_amplitude[i+j]<<endl;
            }

//            local_amplitude[i] = result_y[indices[0]];

        }



    }

};


#define K_N_BASIS_QPHV 12

class qphv_MesonBse : public MesonBse {

public:
    qphv_MesonBse(ps_amplitude<3> &meson, QuarkDse *dse1, QuarkDse *dse2, string which_solver, string parameter_file,
                    string output_folder, bool interpolation_nessercary, array<bool,5> flag) :
            MesonBse(meson, dse1, dse2, which_solver, parameter_file, output_folder, interpolation_nessercary, flag, "qphv"){

        cout<<"||    ----------------------------------------------     ||"<<endl<<"      ------------qphv vertex------------------       "<<endl;

        //important: I need to change this here in order to only use one structure: Thus is why now we check for the index in the amplitude.
        //setting the basis elements in the parameterfile
        parameter.n_basis = parameter.n_projector = K_N_BASIS_QPHV;
//        parameter.n_basis = parameter.n_projector = meson.getSi().getMaxima_of_partial_indices()[0];
//        Psquare =   Line(Ps_start,Ps_end, Ps_n,  "linear", "equal", true, false);

        //setting the kernel functions to calculate the kernel, norm...
        set_kernel_functions();

        //Here we check if an interpolation is nessercary and if we accumulate the x-values, as they are already known.
        interpolation_start();

        //And last but not least setting the function for calculating the inhomogenous part
//        setCalc_imhomogneouspart(precalc_inhomogenous_part);

    }

    virtual ~qphv_MesonBse(){}

    void free_more_memory(){
        Interpol=SplineInterpolator<2, K_N_BASIS_QPHV, double, Cdoub>();
    }

private:


    //Interpolation stuff ---------
    //todo: also set it up such that the interpoaltionc choice is more abitrary?
////    BaseInterpol<2, 4, double, Cdoub>* Interpol = nullptr;
    SplineInterpolator<2, K_N_BASIS_QPHV, double, Cdoub> Interpol;


    void interpolation_update(const VecCdoub &amplitude,  VecCdoub& local_amplitude) {

        //The local amplitude here should be the amplitude inside, as this one is getting interpolated.

        //This function interpolate the values of amplittude on the wanted grid points x' safed in xvalues_interpol.
        //The corresponding x-values for f(x) are already set in interpolation_start() above.
        Interpol.Update( xvalues_interpol, amplitude);

        array<Cdoub, K_N_BASIS_QPHV> result_y;
        SuperIndex<3> Si_I({parameter.n_projector,parameter.n_ll, parameter.n_lz});
        SuperIndex<2> Si_J({parameter.n_ll, parameter.n_lz});
//        SuperIndex<3> Si_I({parameter.n_projector,parameter.n_pp, parameter.n_pz}); ///?????????? wrong

//#pragma omp parallel for
        for (int i = 0; i < Si_J.getNsuper_index() ; ++i) {

            auto indices = Si_J.are(i);
            //Here should be choosen according to the when filling the vectorin interpolation_start(): -> Check beforehand.
            // (Now set to log, as this is interpolation with equal distances.)
            array<double,2> new_x_values ={log(ps.AllGrid[2]->getGrid()[indices[0]]),ps.AllGrid[3]->getGrid()[indices[1]]};
//            array<double,2> new_x_values ={ps.AllGrid[2]->getGrid()[indices[1]],ps.AllGrid[3]->getGrid()[indices[2]]};

            //performing the interpolation only one time for all amplitudes t[i]. E,F,G,H.
            Interpol.getValue(result_y,new_x_values );

            for (int j = 0; j < K_N_BASIS_QPHV; ++j) {
//                auto index = Si_I.are(i+j);
                local_amplitude[Si_I.is({j, indices[0], indices[1]})] = result_y[j];
//                cout<<"l["<<i+j<<"]= "<<local_amplitude[i+j]<<endl;
            }

//            local_amplitude[i] = result_y[indices[0]];

        }

    }
};



#endif //CALC_VMESON_MESONBSE_H
