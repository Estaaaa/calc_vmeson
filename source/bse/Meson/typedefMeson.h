//
// Created by esther on 25.01.18.
//

#ifndef CALC_VMESON_TYPEDEFMESON_H
#define CALC_VMESON_TYPEDEFMESON_H

#include <typedefs.h>
#include <Routing/routing_amount_scalars.h>
#include <extra_def.h>

//Function pointer for meson related functions.
typedef void (*FunPtrtoKernelY)( matCdoub&, const ArrayScalarProducts&, const Cdoub& ,
                                const Cdoub& , const Cdoub& , const Cdoub& );

typedef void (*FunPtrtoKernelL)( matCdoub&, const ArrayScalarProducts& , const array<Cdoub, K_ORDER_OF_ANGLE>& ang);

typedef void (*FunPtrtoNorm) (matCdoub&, Cdoub &,Cdoub &, Cdoub &, Cdoub &, Cdoub &, Cdoub &, Cdoub &, Cdoub &,
                              const Cdoub& , const Cdoub& , const Cdoub& , const Cdoub&  );

typedef Cdoub (*FunPtrtoDecay) (Cdoub &,Cdoub &, Cdoub &, Cdoub &, Cdoub &, Cdoub &, Cdoub &, Cdoub &,
                               VecCdoub , const Cdoub& , const Cdoub& , const Cdoub& , const Cdoub& );

typedef void (*FunPtrtoKernelAll) (matCdoub& , const ArrayScalarProducts& ,  const Cdoub& ,
                                   const Cdoub& , const Cdoub& , const Cdoub& ,
                                   const array<Cdoub, K_ORDER_OF_ANGLE>& );

typedef void (*FunPtrInhomogenous) (VecCdoub& , const ArrayScalarProducts& );

//typedef void (*FunPtrRotation)(matCdoub& kern,const ArrayScalarProducts& sp);

#endif //CALC_VMESON_TYPEDEFMESON_H
