//
// Created by esther on 23.10.17.
//


#include <extra_def.h>

#include "Kernel/ps/kernel_ps.h"
#include "Kernel/ve/kernel_ve.h"
#include "Kernel/qphv_all/qphv/kernel_qphv.h"
#include "Kernel/qphv_all/qphv_longi/kernel_qphv_longi.h"
#include "Kernel/qphv_all/qphv_trans/kernel_qphv_trans.h"
#include "Kernel/scalar/kernel_scalar.h"
#include "Kernel/axial/kernel_axial.h"




#include <fv.h>
#include <cheb.h>
//#include <gnuplot/gnuplot_i.h>
#include <Routing/dependence_of_all_kernels.h>
#include "../../utilities/write/write.h"
#include "MesonBse.h"

//#define K_PLOTTING_INTERPOLATION

//Getting the Kernelmatrix all in one function, no seperation. One bif kernel code.
//comment: hier fehlt noch die cheby version.
matCdoub MesonBse::get_Matrix_all_in_one_go() {

    double color_factor = 4./3.;

    //The Prefactor of the Integration includes: the color factor, the trivial integration of 2 Pi,
    // the integral factor 1/(2Pi)⁴, the Z2² and the factor of 1/2 from the radial integration(or symmetry?).
    double prefac_integration = - color_factor * 0.5 /( pow( 2. * M_PI, 4)) * 2. * M_PI * Z2_Z2;


    //Saving thing in kernel_all, initalizing this qunatitiy and the superindices.
    SuperIndex<3> Si_I({parameter.n_projector,parameter.n_pp, parameter.n_pz});
    SuperIndex<3> Si_J({parameter.n_basis, parameter.n_ll, parameter.n_lz});
    //Initalizing everything with 0's first.
    matCdoub kernel_all(Si_I.getNsuper_index(), VecCdoub(Si_J.getNsuper_index(),0.0));



    //Precalcultion of the y-angle and the rotation matrix Y = Gmat (quark propagators).
    // With time meassurment.
    time_t start, end2;
    time(&start);

    //all the scalar products that will be calculated and used within this loop.
    ArrayScalarProducts scalar_gluon;
    ArrayScalarProducts scalars;
    ArrayScalarProducts scalars_denom;

    matCdoub kernel_safe(parameter.n_projector, VecCdoub(parameter.n_basis, 0.0));




#pragma omp parallel for firstprivate(kernel_safe, scalar_gluon, scalars, scalars_denom )
for (int ll = 0; ll < parameter.n_ll; ++ll) {
    for (int zl = 0; zl < parameter.n_lz; ++zl) {


        //All the quarks need to be called
        Cdoub Q1A = quark1->operator()({ll,zl})[0];
        Cdoub Q1B = quark1->operator()({ll,zl})[1];
        Cdoub Q2A = quark2->operator()({ll,zl})[0];
        Cdoub Q2B = quark2->operator()({ll,zl})[1];

//        get_all_scalar_prod<10,2>({ll,zl},{2, 3},rotation_matrix::depends_on_scalar_products, ps, scalars_denom);
        get_all_scalar_prod<2, 2>({ll, zl}, {2,3}, {2,9}, ps, scalars_denom);

        //Comment: Got forgotten in frist set-up!
        Cdoub denorminator = 1.0/( (scalars_denom[2] * Q1A*Q1A +Q1B*Q1B)*(scalars_denom[9] *Q2A*Q2A + Q2B*Q2B) );


        for (int zp = 0; zp < parameter.n_pz; ++zp) {
            for (int pp = 0; pp < parameter.n_pp; ++pp) {


//Comment: Rearegent the loop ordering. Incredible what efficient ordering can do for ya. :) More loops isn't nessercarly bad.
//                for (int i = 0; i < parameter.n_projector ; ++i) {
//                    for (int k = 0; k < parameter.n_basis; ++k) {


                    //internal y-integration
                    for (int yl = 0; yl < parameter.n_ly; ++yl) {


                        //get the scalar product for the gluon ->momentum kk & the gluon
                        get_all_scalar_prod<1, 5>({pp, zp, ll, zl, yl}, gluon::depends_on_sphericals,
                                                  gluon::depends_on_scalar_products, ps, scalar_gluon);
                        Cdoub kk = scalar_gluon[gluon::depends_on_scalar_products[0]];
                        Cdoub current_gluon = gluon->operator()<1>({kk});

                        //calculating all the other nessercay scalar products.
                        get_all_scalar_prod<12, 5>({pp, zp, ll, zl, yl}, kernel_all::depends_on_sphericals,
                                                   kernel_all::depends_on_scalar_products, ps, scalars);

                        //Safing the idividual angular integral values. Demanded by current program structure.
                        array<Cdoub, K_ORDER_OF_ANGLE> ang_precalc;


                        //safing the angles in a special variable cause the form code is set up like this.
                        ang_precalc[0] = pow(kk, -1.0);
                        ang_precalc[1] = pow(kk, 0.0);
                        ang_precalc[2] = pow(kk, 1.0);
                        ang_precalc[3] = pow(kk, 2.0);


                        //The integration factor for y
                       double integration_factor_y = ps.yl.getWeights_integration()[yl];   //y weight



                        //calculating the whole kernel.
                        kernelall(kernel_safe, scalars, Q1A, Q1B, Q2A, Q2B, ang_precalc);

                        for (int i = 0; i < parameter.n_projector ; ++i) {
                            for (int k = 0; k < parameter.n_basis; ++k) {

//                                if((i==0 && k==5) || (i==5 && k==0) || (i==0 && k==0) || (i==5 && k==5))
//                                {
                                    kernel_all[Si_I.is({i, pp, zp})][Si_J.is({k, ll, zl})] += kernel_safe[i][k]
                                                                                              * current_gluon
                                                                                              * integration_factor_y;

//                                }else{kernel_all[Si_I.is({i, pp, zp})][Si_J.is({k, ll, zl})]=0.0;}

//                        dummy += kernel_safe[i][k] * current_gluon * ps.yl.getWeights_integration()[yl] *denorminator;
                            }
                        }


                    }


                //The integration factor includes: all the weights, for w_ll and w_zl. and the Jacobian sqrt(1-zl²)*ll.
                double integration_factor =
                        ps.ll.getWeights_integration()[ll] *                    // l weights
                        ps.zl.getWeights_integration()[zl] *                    // z weights
                        sqrt(1. - ps.zl.getGrid()[zl] * ps.zl.getGrid()[zl]) * //(1-z²) Jacobian
                        ps.ll.getGrid()[ll];                                 //l²
                //                    cout << integration_factor << " ////////////" << endl;

                for (int i = 0; i < parameter.n_projector ; ++i) {
                    for (int k = 0; k < parameter.n_basis; ++k) {

                //The full kernel multilpying the integrated K over y and more integration factors and the quark denorminator.
                kernel_all[Si_I.is({i, pp, zp})][Si_J.is({k, ll, zl})] = kernel_all[Si_I.is({i, pp, zp})][Si_J.is({k, ll, zl})]
                                                                          * integration_factor
                                                                          * prefac_integration
                                                                          * denorminator;

//                 if(i == 0 && k ==0 ){cout<< "----------"<<tab<<pp<<endl;}
//                 cout<<"kenerl["<<i<<"]["<<k<<"]: "<< dummy<<tab<<Q1A<<tab<<ps.pp.getGrid()[pp]<< tab<<denorminator<<
//                        tab<<tab<< kernel_all[Si_I.is({i, pp, zp})][Si_J.is({k, ll, zl})]<<endl;


                        }

                    }


                }
            }
        }
    }

    time(&end2); give_time(start,end2,"pion kernel calculated!");


    return kernel_all;




}
void MesonBse::get_Matrix_times_vector(const VecCdoub &amplitude, VecCdoub &amplitude_updated) {

    double color_factor = 4./3.;

    //The Prefactor of the Integration includes: the color factor, the trivial integration of 2 Pi,
    // the integral factor 1/(2Pi)⁴, the Z2² and the factor of 1/2 from the radial integration(or symmetry?).
    double prefac_integration = - color_factor * 0.5 /( pow( 2. * M_PI, 4)) * 2. * M_PI * Z2_Z2;


    //Saving thing in kernel_all, initalizing this qunatitiy and the superindices.
    SuperIndex<3> Si_I({parameter.n_projector,parameter.n_pp, parameter.n_pz});
    SuperIndex<3> Si_J({parameter.n_basis, parameter.n_ll, parameter.n_lz});
    //Initalizing everything with 0's first.
//    matCdoub kernel_all(Si_I.getNsuper_index(), VecCdoub(Si_J.getNsuper_index(),0.0));



    //Precalcultion of the y-angle and the rotation matrix Y = Gmat (quark propagators).
    // With time meassurment.
    time_t start, end2;
    time(&start);

    //all the scalar products that will be calculated and used within this loop.
    ArrayScalarProducts scalar_gluon;
    ArrayScalarProducts scalars;
    ArrayScalarProducts scalars_denom;

    matCdoub kernel_safe(parameter.n_projector, VecCdoub(parameter.n_basis, 0.0));




#pragma omp parallel for firstprivate(kernel_safe, scalar_gluon, scalars, scalars_denom )
    for (int ll = 0; ll < parameter.n_ll; ++ll) {
        for (int zl = 0; zl < parameter.n_lz; ++zl) {


            //All the quarks need to be called
            Cdoub Q1A = quark1->operator()({ll,zl})[0];
            Cdoub Q1B = quark1->operator()({ll,zl})[1];
            Cdoub Q2A = quark2->operator()({ll,zl})[0];
            Cdoub Q2B = quark2->operator()({ll,zl})[1];

//        get_all_scalar_prod<10,2>({ll,zl},{2, 3},rotation_matrix::depends_on_scalar_products, ps, scalars_denom);
            get_all_scalar_prod<2, 2>({ll, zl}, {2,3}, {2,9}, ps, scalars_denom);

            //Comment: Got forgotten in frist set-up!
            Cdoub denorminator = 1.0/( (scalars_denom[2] * Q1A*Q1A +Q1B*Q1B)*(scalars_denom[9] *Q2A*Q2A + Q2B*Q2B) );


            for (int zp = 0; zp < parameter.n_pz; ++zp) {
                for (int pp = 0; pp < parameter.n_pp; ++pp) {


//Comment: Rearegent the loop ordering. Incredible what efficient ordering can do for ya. :) More loops isn't nessercarly bad.
//                for (int i = 0; i < parameter.n_projector ; ++i) {
//                    for (int k = 0; k < parameter.n_basis; ++k) {

                    //The integration factor includes: all the weights, for w_ll and w_zl. and the Jacobian sqrt(1-zl²)*ll.
                    double integration_factor =
                            ps.ll.getWeights_integration()[ll] *                    // l weights
                            ps.zl.getWeights_integration()[zl] *                    // z weights
                            sqrt(1. - ps.zl.getGrid()[zl] * ps.zl.getGrid()[zl]) * //(1-z²) Jacobian
                            ps.ll.getGrid()[ll];                                 //l²
                    //                    cout << integration_factor << " ////////////" << endl;


                    //internal y-integration
                    for (int yl = 0; yl < parameter.n_ly; ++yl) {


                        //get the scalar product for the gluon ->momentum kk & the gluon
                        get_all_scalar_prod<1, 5>({pp, zp, ll, zl, yl}, gluon::depends_on_sphericals,
                                                  gluon::depends_on_scalar_products, ps, scalar_gluon);
                        Cdoub kk = scalar_gluon[gluon::depends_on_scalar_products[0]];
                        Cdoub current_gluon = gluon->operator()<1>({kk});

                        //calculating all the other nessercay scalar products.
                        get_all_scalar_prod<12, 5>({pp, zp, ll, zl, yl}, kernel_all::depends_on_sphericals,
                                                   kernel_all::depends_on_scalar_products, ps, scalars);

                        //Safing the idividual angular integral values. Demanded by current program structure.
                        array<Cdoub, K_ORDER_OF_ANGLE> ang_precalc;


                        //safing the angles in a special variable cause the form code is set up like this.
                        ang_precalc[0] = pow(kk, -1.0);
                        ang_precalc[1] = pow(kk, 0.0);
                        ang_precalc[2] = pow(kk, 1.0);
                        ang_precalc[3] = pow(kk, 2.0);


                        //The integration factor for y
                        double integration_factor_y = ps.yl.getWeights_integration()[yl];   //y weight



                        //calculating the whole kernel.
                        kernelall(kernel_safe, scalars, Q1A, Q1B, Q2A, Q2B, ang_precalc);

                        for (int i = 0; i < parameter.n_projector ; ++i) {
                            Cdoub sum=0.0;
                            for (int k = 0; k < parameter.n_basis; ++k) {

//                                if((i==0 && k==5) || (i==5 && k==0) || (i==0 && k==0) || (i==5 && k==5))
//                                {
//                                kernel_all[Si_I.is({i, pp, zp})][Si_J.is({k, ll, zl})]
                                  sum          += kernel_safe[i][k]
                                                  * current_gluon
                                                  * integration_factor_y
                                                  * integration_factor
                                                  * prefac_integration
                                                  * denorminator

                                                  * amplitude[Si_J.is({k,ll,zl})];

//                                }else{kernel_all[Si_I.is({i, pp, zp})][Si_J.is({k, ll, zl})]=0.0;}

//                        dummy += kernel_safe[i][k] * current_gluon * ps.yl.getWeights_integration()[yl] *denorminator;
                            }

                            amplitude_updated[Si_I.is({i,pp,zp})]=sum;
                        }


                    }


//old deelet..?
/*
                    for (int i = 0; i < parameter.n_projector ; ++i) {
                        for (int k = 0; k < parameter.n_basis; ++k) {

                            //The full kernel multilpying the integrated K over y and more integration factors and the quark denorminator.
//                            kernel_all[Si_I.is({i, pp, zp})][Si_J.is({k, ll, zl})]
                                    sum                                             += kernel_all[Si_I.is({i, pp, zp})][Si_J.is({k, ll, zl})]
                                                                                     * integration_factor
                                                                                     * prefac_integration
                                                                                     * denorminator

                                                                                     * amplitude[Si_J.is({k,ll,zl})];

//                 if(i == 0 && k ==0 ){cout<< "----------"<<tab<<pp<<endl;}
//                 cout<<"kenerl["<<i<<"]["<<k<<"]: "<< dummy<<tab<<Q1A<<tab<<ps.pp.getGrid()[pp]<< tab<<denorminator<<
//                        tab<<tab<< kernel_all[Si_I.is({i, pp, zp})][Si_J.is({k, ll, zl})]<<endl;


                        }

                    }*/


                }
            }
        }
    }

    time(&end2); give_time(start,end2,"KG calculated!");






}

//Getting the Kernelmatrix with decomposition of Y &L:
matCdoub MesonBse::get_Matrix() {

    double color_factor = 4./3.;

    //The Prefactor of the Integration includes: the color factor, the trivial integration of 2 Pi,
    // the integral factor 1/(2Pi)⁴, the Z2² and the factor of 1/2 from the radial integration(or symmetry?).
    double prefac_integration = - color_factor * 0.5 /( pow( 2. * M_PI, 4)) * 2. * M_PI * Z2_Z2;


    //Initalizing this qunatitiy and the superindices.
    SuperIndex<3> Si_I({parameter.n_projector,parameter.n_pp, parameter.n_pz});
    SuperIndex<3> Si_J({parameter.n_basis, parameter.n_ll, parameter.n_lz});
    //Safing thing in kernel_all & Initalizing everything with 0's first.
    matCdoub kernel_all(Si_I.getNsuper_index(), VecCdoub(Si_J.getNsuper_index(),0.0));

    //Comparing wiht Paul
//    matCdoub test(Si_I.getNsuper_index(), VecCdoub(Si_J.getNsuper_index(),0.0));

    //defining Si belonging to Y matrix
    SuperIndex<2> Si_Y_dirac({parameter.n_projector,parameter.n_basis});
    SuperIndex<2> Si_Y_discrete({ parameter.n_ll, parameter.n_lz});



    //Superindex for precalcuated angles and new variable to safe angles.
    SuperIndex<4> Si_angle({parameter.n_pp, parameter.n_pz, parameter.n_ll, parameter.n_lz});


    //Precalcultion of the y-angle and the rotation matrix Y = Gmat (quark propagators).
    // With time meassurment.
    time_t start, start1, start2, start3, end1, end2, end3;
    time(&start);

    //Precalculating only the angles and saving them.
//#ifdef L_CALC_ANGLE
    time(&start1);
    matCdoub preangles= precalc_angles();
    time(&end1); give_time(start1,end1,"precalc angles");
//#endif

//Precalculating the whole Lmatrix.
//#ifdef L_CALC_ALL
//
//    time(&start3);
//    SuperIndex<4> Si_Lmat_discrete({parameter.n_pp, parameter.n_pz, parameter.n_ll, parameter.n_lz});
//    matCdoub L_mat_kernel = precalc_Lmat();
//    time(&end3); give_time(start3,end3,"precalc Lmat");

//#endif

    //Calculating the rotation matrix
    time(&start2);
    matCdoub Y_kernel = precalc_rotation_Y();
    time(&end2); give_time(start2,end2,"precalc Gmat");

    //Defining loop objects
    ArrayScalarProducts scalars;
    array<Cdoub, K_ORDER_OF_ANGLE> ang_safe;

    matCdoub L_kernel(parameter.n_projector, VecCdoub(parameter.n_basis, 0.0));


#pragma omp parallel for firstprivate( L_kernel, scalars, ang_safe)
//#pragma omp parallel for default(shared)
    for (int pp = 0; pp < parameter.n_pp; ++pp) {

        for (int zp = 0; zp < parameter.n_pz; ++zp) {

            for (int ll = 0; ll < parameter.n_ll; ++ll) {

                for (int zl = 0; zl < parameter.n_lz; ++zl) {


                    //The integration factor includes: all the weights, for w_ll and w_zl. and the Jacobian sqrt(1-zl²)*ll.
                    double integration_factor =
                            ps.ll.getWeights_integration()[ll] *                    //weights
                            ps.zl.getWeights_integration()[zl] *
                            sqrt(1. - ps.zl.getGrid()[zl] * ps.zl.getGrid()[zl]) * //(1-z²)
                            ps.ll.getGrid()[ ll];                                  // l²
//                    cout << integration_factor << " ////////////" << endl;


                    //accessing precalculated data
                    ang_safe[0] = preangles[0][Si_angle.is({pp,zp,ll,zl})];
                    ang_safe[1] = preangles[1][Si_angle.is({pp,zp,ll,zl})];
                    ang_safe[2] = preangles[2][Si_angle.is({pp,zp,ll,zl})];
                    ang_safe[3] = preangles[3][Si_angle.is({pp,zp,ll,zl})];


                    //calculating the nessercay scalar products.
                    get_all_scalar_prod<5,4>({pp,zp,ll,zl},Lmat::depends_on_sphericals,Lmat::depends_on_scalar_products, ps, scalars);

                    //calculating Lmat
                    kernelL(L_kernel, scalars,ang_safe);

                    for (int i = 0; i < parameter.n_projector ; ++i) {
                        for (int k = 0; k < parameter.n_basis; ++k) {

                            for (int j = 0; j < parameter.n_basis; ++j) {

                                kernel_all[Si_I.is({i,pp,zp})][Si_J.is({k,ll,zl})] +=  L_kernel[i][j]
                                                                                       * Y_kernel[Si_Y_dirac.is({j,k})][Si_Y_discrete.is({ll,zl})]
                                                                                       * integration_factor * prefac_integration ;

//                                kernel_all[Si_I.is({i,pp,zp})][Si_J.is({k,ll,zl})] +=   L_mat_kernel[Si_Y_dirac.is({i,j})][Si_Lmat_discrete.is({pp,zp,ll,zl})]
//                                                                                       * Y_kernel[Si_Y_dirac.is({j,k})][Si_Y_discrete.is({ll,zl})]
//                                                                                       * integration_factor * prefac_integration ;

//                                if(i==0 && k==0 && j==0 ){test[Si_I.is({i,pp,zp})][Si_J.is({k,ll,zl})] +=   L_mat_kernel[Si_Y_dirac.is({i,j})][Si_Lmat_discrete.is({pp,zp,ll,zl})]
//                                                                                        * Y_kernel[Si_Y_dirac.is({j,k})][Si_Y_discrete.is({ll,zl})]
//                                                                                          * integration_factor * prefac_integration* (1.0/Z2_Z2) ;}



                            }

//                            cout<<"["<<i<<"]["<<k<<"]: "<< L_mat_kernel[Si_Y_dirac.is({i,k})][Si_Y_discrete.is({ll,zl})]<<tab<<" , ";
//                            cout<<"["<<i<<"]["<<k<<"]: "<< Y_kernel[Si_Y_dirac.is({i,k})][Si_Y_discrete.is({ll,zl})]<<tab<<" , ";
//                            cout<<"["<<i<<"]["<<k<<"]: "<< test[Si_I.is({i,pp,zp})][Si_J.is({k,ll,zl})]<<tab<<" , ";
//                            if(i==0 && k==0){cout<<"["<<i<<"]["<<k<<"]: "<< test[Si_I.is({i,pp,zp})][Si_J.is({k,ll,zl})]<<tab<<" , ";}
//                            cout<<"kenerl["<<i<<"]["<<k<<"]: "<< kernel_all[Si_I.is({i,pp,zp})][Si_J.is({k,ll,zl})]<<tab<<" , ";
//                            cout<<"kenerl["<<i<<"]["<<k<<"]: "<< kernel_all[Si_I.is({i,pp,zp})][Si_J.is({k,ll,zl})]<<tab
//                                <<dummy<<tab<<endl;

                        }
//                        cout<<endl;

                    }
//                    cout<<"-------------"<<endl;


                }
            }
        }
    }

    time(&end2); give_time(start,end2,"pion kernel calculated!");



//    array<Cdoub, K_ORDER_OF_ANGLE> ang_safe;
//    array< array <Cdoub,parameter.n_projector>,parameter.n_basis> L_kernel;
//    ArrayScalarProducts scalars;
//    for (int zp = 0; zp < parameter.n_pz; ++zp) {
//    for (int pp = 0; pp < parameter.n_pp; ++pp) {
//    for (int i = 0; i < parameter.n_projector ; ++i) {
//    for (int zl = 0; zl < parameter.n_lz; ++zl) {
//    for (int ll = 0; ll < parameter.n_ll; ++ll) {
//    for (int k = 0; k < parameter.n_basis; ++k) {
//
//        ang_safe[0] = preangles[0][Si_angle.is({pp,zp,ll,zl})];
//        ang_safe[1] = preangles[1][Si_angle.is({pp,zp,ll,zl})];
//        ang_safe[2] = preangles[2][Si_angle.is({pp,zp,ll,zl})];
//        ang_safe[3] = preangles[3][Si_angle.is({pp,zp,ll,zl})];
//
//        get_all_scalar_prod<5,4>({pp,zp,ll,zl},Lmat::depends_on_sphericals,Lmat::depends_on_scalar_products, ps, scalars);
//
//        kernelL(L_kernel, scalars,ang_safe);
//
//
////        cout<<"P:("<<i<<","<<k<<"):  "<<kernel_all[Si_I.is({i,pp,zp})][Si_J.is({k,ll,zl})]<<tab<<Y_kernel[Si_Y_dirac.is({i,k})][Si_Y_discrete.is({ll,zl})]
////            << tab << L_kernel[i][k] << tab << L_mat_kernel[Si_Y_dirac.is({i,k})][Si_Lmat_discrete.is({pp, zp, ll, zl})] <<endl;
//
////        cout<<kernel_all[Si_I.is({i,pp,zp})][Si_J.is({k,ll,zl})]<<tab <<tab <<Y_kernel[Si_Y_dirac.is({i,k})][Si_Y_discrete.is({ll,zl})]
////            <<  tab << L_mat_kernel[Si_Y_dirac.is({i,k})][Si_Lmat_discrete.is({pp, zp, ll, zl})] <<endl;
//    }}}}}}




//    return test;
    return kernel_all;




}
matCdoub MesonBse::precalc_angles() {

    SuperIndex<4> Si_angle({parameter.n_pp, parameter.n_pz, parameter.n_ll, parameter.n_lz});

    matCdoub ang(K_ORDER_OF_ANGLE, VecCdoub(Si_angle.getNsuper_index(),0.0));

    //Initilaizing the Matrix that safe the angular values with all zero's such that it can be summed onto.
    // order_of_angle_integral is the number that tell you how high of an order is safed. In the kernel it should be higher than 4.
    // (See Kernel Form code)
    //Needs to be private for the paralellisation.
    Cdoub vec[K_ORDER_OF_ANGLE]= {0.0};
    ArrayScalarProducts scalars;


//#pragma omp parallel for default(shared)
#pragma omp parallel for firstprivate(vec, scalars)
    for (int pp = 0; pp < parameter.n_pp; ++pp) {

        //Initilaizing the Matrix that safe the angular values with all zero's such that it can be summed onto.
        // order_of_angle_integral is the number that tell you how high of an order is safed. In the kernel it should be higher than 4.
        // (See Kernel Form code)
//        Cdoub vec[K_ORDER_OF_ANGLE]= {0.0};

        for (int zp = 0; zp < parameter.n_pz; ++zp) {

            for (int ll = 0; ll < parameter.n_ll; ++ll) {

                for (int zl = 0; zl < parameter.n_lz; ++zl) {

                    for (int i = 0; i < K_ORDER_OF_ANGLE; ++i) {
                        vec[i] = 0.0; }
//                    Cdoub vec[K_ORDER_OF_ANGLE]= {0.0};

                    for (int yl = 0; yl < parameter.n_ly; ++yl) {

//                        ArrayScalarProducts scalars;


                        get_all_scalar_prod<1,5>({pp,zp,ll,zl,yl},gluon::depends_on_sphericals,gluon::depends_on_scalar_products, ps, scalars);
                        Cdoub kk=scalars[gluon::depends_on_scalar_products[0]];
                        Cdoub current_gluon =  gluon->operator()<1>({kk});

                        double integration_factor =  ps.yl.getWeights_integration()[yl];

                        vec[0] +=  integration_factor * current_gluon *pow (kk,-1.0);
                        vec[1] +=  integration_factor * current_gluon *1.0;
                        vec[2] +=  integration_factor * current_gluon *pow (kk,1.0);
                        vec[3] +=  integration_factor * current_gluon *pow (kk,2.0);
//                        vec[4][Si_angle.is({pp,zp,ll,zl})] += current_gluon *esther_pow::pow(ps.yl.getGrid()[yl],4);

//                        cout<< "test: "<<gluon->operator()<1>({scalars[0]})<<endl;
//                        array<Cdoub,6> scalarp  = get_scalar_prod<6,5>({pp,zp,ll,zl,yl},{0,1,2,3,4},{1,2,3,6,7,8}, ps);
//                        cout<< "scalar: "<<scalars[0]<<tab << vec[0][Si_angle.is({pp,zp,ll,zl})]<<tab <<vec[1][Si_angle.is({pp,zp,ll,zl})]<<endl;

                    }

                    ang[0][Si_angle.is({pp,zp,ll,zl})] +=  vec[0];
                    ang[1][Si_angle.is({pp,zp,ll,zl})] +=  vec[1];
                    ang[2][Si_angle.is({pp,zp,ll,zl})] +=  vec[2];
                    ang[3][Si_angle.is({pp,zp,ll,zl})] +=  vec[3];
                }
            }
        }
    }
//    cout<<result[0][0]<<tab<<result[3][10]<<endl;
//    delete2D(order_of_angle_integral, Si_angle.getNsuper_index(), result);

    return ang;

//    for (int j = 0; j < Si_angle.getNsuper_index(); ++j) {
//        cout<<result[0][j]<<tab<<result[2][j]<<endl;
//    }


}
matCdoub MesonBse::precalc_rotation_Y() {

    SuperIndex<2> Si_dirac({parameter.n_projector,parameter.n_basis});
    SuperIndex<2> Si_discrete({ parameter.n_ll, parameter.n_lz});


    matCdoub vec(Si_dirac.getNsuper_index(), VecCdoub(Si_discrete.getNsuper_index(),0.0));

    //Defining objects used in the loop - rember privatizing for paralleisation.
    ArrayScalarProducts scalars;

    matCdoub kernel(parameter.n_projector, VecCdoub(parameter.n_basis, 0.0));
//    array<array<Cdoub,K_N_PROJECTORS_DIRAC>,K_N_BASE_ELEMENTS_DIRAC> kernel;

#pragma omp parallel for firstprivate(kernel, scalars)
    for (int ll = 0; ll < parameter.n_ll; ++ll) {

        for (int zl = 0; zl < parameter.n_lz; ++zl) {

//            ArrayScalarProducts scalars;

//            array<array<Cdoub,K_N_PROJECTORS_DIRAC>,K_N_BASE_ELEMENTS_DIRAC> kernel;
//            matCdoub kernel;
//            initMat(kernel,parameter.n_projector, parameter.n_basis);

            Cdoub Q1A = quark1->operator()({ll,zl})[0];
            Cdoub Q1B = quark1->operator()({ll,zl})[1];
            Cdoub Q2A = quark2->operator()({ll,zl})[0];
            Cdoub Q2B = quark2->operator()({ll,zl})[1];

//            Cdoub Q1A = 1.0;
//            Cdoub Q1B = 1.0;
//            Cdoub Q2A = 1.0;
//            Cdoub Q2B = 1.0;

            get_all_scalar_prod<10,2>({ll,zl},{2, 3},rotation_matrix::depends_on_scalar_products, ps, scalars);
            kernelY(kernel, scalars, Q1A, Q1B, Q2A, Q2B);

            Cdoub denorminator = 1.0/( (scalars[2] * Q1A*Q1A +Q1B*Q1B)*(scalars[9] *Q2A*Q2A + Q2B*Q2B) );

            for (int i = 0; i < parameter.n_projector ; ++i) {

                for (int j = 0; j < parameter.n_basis; ++j) {


                    vec[Si_dirac.is({i,j})][Si_discrete.is({ll,zl})] = kernel[i][j] * denorminator;


//                    if(i==0 && j==0 ){cout<<i<<tab<<j<<tab<<vec[Si_dirac.is({i,j})][Si_discrete.is({ll,zl})]<<" , ";}
//                    cout<<i<<tab<<j<<tab<<vec[Si_dirac.is({i,j})][Si_discrete.is({ll,zl})]<<" , ";
//                    cout<<i<<tab<<j<<tab<<kernel[i][j]* denorminator<<tab<<Q1A<<tab<<ps.ll.getGrid()[ll]
//                        <<tab<<scalars[2]<<tab<<scalars[7]<<tab<<scalars[31]<<endl<<tab
//                                                  <<Q2A<<tab<<Q1B<<tab<<(Q1B*Q2B+Q1A*Q2A*scalars[7])<<tab<<
//                    (Q1A*Q2B*scalars[29] - scalars[31]* Q2A*Q1B) * denorminator <<endl<<endl;


                }
//                cout<<endl;

            }
//            cout<<"---------------"<<endl;




//                        cout<< "test: "<<gluon->operator()<1>({scalars[0]})<<endl;
//                        array<Cdoub,6> scalarp  = get_scalar_prod<6,5>({pp,zp,ll,zl,yl},{0,1,2,3,4},{1,2,3,6,7,8}, ps);
//                        cout<< "scalar: "<<scalarp[0]<<tab << scalarp[3]<<tab <<scalarp[5]<<endl;

        }
    }


    return vec;
}
matCdoub MesonBse::precalc_Lmat() {

    SuperIndex<2> Si_dirac({parameter.n_projector,parameter.n_basis});
    SuperIndex<4> Si_discrete({parameter.n_pp, parameter.n_pz, parameter.n_ll, parameter.n_lz});

    ArrayScalarProducts scalarsLmat;
    array<Cdoub, K_ORDER_OF_ANGLE> ang;
    matCdoub vec(Si_dirac.getNsuper_index(), VecCdoub( Si_discrete.getNsuper_index(),0.0));


    matCdoub L_kernel(parameter.n_projector, VecCdoub(parameter.n_basis, 0.0));

#pragma omp parallel for firstprivate(L_kernel, scalarsLmat, ang)
//#pragma omp parallel for default(shared)
    for (int pp = 0; pp < parameter.n_pp; ++pp) {

        for (int zp = 0; zp < parameter.n_pz; ++zp) {

            for (int ll = 0; ll < parameter.n_ll; ++ll) {

                for (int zl = 0; zl < parameter.n_lz; ++zl) {

//                    ArrayScalarProducts scalarsLmat;

                    get_all_scalar_prod<5,4>({pp,zp,ll,zl},Lmat::depends_on_sphericals,Lmat::depends_on_scalar_products, ps, scalarsLmat);

                    for (int i = 0; i < parameter.n_projector ; ++i) {
                        for (int j = 0; j < parameter.n_basis; ++j) {


                            Cdoub sum=0.0;
                            for (int yl = 0; yl < parameter.n_ly; ++yl) {

                                ArrayScalarProducts scalarsgluon;

                                double integration_factor = ps.yl.getWeights_integration()[yl];

                                get_all_scalar_prod<1, 5>({pp, zp, ll, zl, yl}, gluon::depends_on_sphericals,
                                                          gluon::depends_on_scalar_products, ps, scalarsgluon);
                                Cdoub kk = scalarsgluon[gluon::depends_on_scalar_products[0]];
                                Cdoub current_gluon = gluon->operator()<1>({kk});

                                ang[0]= pow(kk,-1.0); ang[1] =  pow(kk, 0.0); ang[2] =  pow(kk, 1.0); ang[3] = pow(kk,2.0);
//                                ang[0]= current_gluon; ang[1] =  pow(current_gluon, 2.0);

                                kernelL(L_kernel, scalarsLmat, ang);


                                sum += L_kernel[i][j] * integration_factor * current_gluon;


//                                cout<< sum <<tab<< kernel[i][j]/ ang[1] <<tab << current_gluon <<endl;
                            }
                            vec[Si_dirac.is({i, j})][Si_discrete.is({pp, zp, ll, zl})] = sum;

//                            if(i==0 && j==0){cout<<i<<","<<j<<tab<<sum<<" , ";}
//                            cout<<"outside: "<<i<<tab<<j<<sum<<endl;
                        }
//                        cout<<endl;
                    }
//                    cout<<"---------"<<endl;




                }
            }
        }
    }

    return vec;




}
matCdoub MesonBse::calculate_rotationmatrix_for_extrernal_p() {


    bool inside_outside=false;
    if(parameter.n_pp ==  parameter.n_ll &&
    parameter.n_pz == parameter.n_lz){inside_outside=true;}; //cout<<inside_outside<<tab<<meson.isIs_calculated()<<endl;



        //Superindices for the Rotation matrix.
    SuperIndex<2> Si_dirac({parameter.n_projector,parameter.n_basis});
    SuperIndex<2> Si_discrete({ parameter.n_ll, parameter.n_lz});


    matCdoub vec(Si_dirac.getNsuper_index(), VecCdoub(Si_discrete.getNsuper_index(),0.0));

    //Defining objects used in the loop - rember privatizing for paralleisation.
    ArrayScalarProducts scalars;
    ArrayScalarProducts quark_scalars;

    matCdoub kernel(parameter.n_projector, VecCdoub(parameter.n_basis, 0.0));
//    array<array<Cdoub,K_N_PROJECTORS_DIRAC>,K_N_BASE_ELEMENTS_DIRAC> kernel;

#pragma omp parallel for firstprivate(kernel, scalars, quark_scalars)
    for (int pp = 0; pp < parameter.n_pp; ++pp) {

        for (int zp = 0; zp < parameter.n_lz; ++zp) {

//            ArrayScalarProducts scalars;

//            array<array<Cdoub,K_N_PROJECTORS_DIRAC>,K_N_BASE_ELEMENTS_DIRAC> kernel;
//            matCdoub kernel;
//            initMat(kernel,parameter.n_projector, parameter.n_basis);

            get_all_scalar_prod<2,2>({pp,zp},quark_p2_norm::depends_on_sphericals,
                                     {quark_p1_norm::depends_on_scalar_products[0],quark_p2_norm::depends_on_scalar_products[0]},
                                     ps,quark_scalars);


            Cdoub Q1A, Q2A, Q1B, Q2B;

            if(inside_outside && meson.isIs_calculated()){

                //All the quarks need to be called
                Q1A = quark1->operator()({pp,zp})[0];
                Q1B = quark1->operator()({pp,zp})[1];
                Q2A = quark2->operator()({pp,zp})[0];
                Q2B = quark2->operator()({pp,zp})[1];
            }else{

                //done: is this part working? sems like it.
                array<Cdoub,2> Q1 = dse1->getValue({quark_scalars[quark_p1_norm::depends_on_scalar_products[0]]});
                array<Cdoub,2> Q2 = dse2->getValue({quark_scalars[quark_p2_norm::depends_on_scalar_products[0]]});

                Q1A = Q1[0];
                Q1B = Q1[1];
                Q2A = Q2[0];
                Q2B = Q2[1];
            }

//            cout<<Q1A<<tab<<Q1B<<tab<<Q2A<<tab<<Q2B<<endl;

            get_all_scalar_prod<8,2>({pp,zp},{0,1},rotation_matrix_outside::depends_on_scalar_products, ps, scalars);
            kernelRotationY(kernel, scalars, Q1A, Q1B, Q2A, Q2B);

            //Comment: The denorminator is not calculated anymore to match it with my old form factor program
            //Thus is has to be included into the dernominator in the fromfactor loop.
//            Cdoub denorminator = 1.0/( (quark_scalars[quark_p1_norm::depends_on_scalar_products[0]] * Q1A*Q1A +Q1B*Q1B)
//                                *(quark_scalars[quark_p2_norm::depends_on_scalar_products[0]] *Q2A*Q2A + Q2B*Q2B));

            for (int i = 0; i < parameter.n_projector ; ++i) {

                for (int j = 0; j < parameter.n_basis; ++j) {


                    vec[Si_dirac.is({i,j})][Si_discrete.is({pp,zp})] = kernel[i][j]; // * denorminator;


//                    if(i==0 && j==0 ){cout<<i<<tab<<j<<tab<<vec[Si_dirac.is({i,j})][Si_discrete.is({ll,zl})]<<" , ";}
//                    cout<<i<<tab<<j<<tab<<vec[Si_dirac.is({i,j})][Si_discrete.is({ll,zl})]<<" , ";
//                    cout<<i<<tab<<j<<tab<<kernel[i][j]* denorminator<<tab<<Q1A<<tab<<ps.ll.getGrid()[ll]
//                        <<tab<<scalars[2]<<tab<<scalars[7]<<tab<<scalars[31]<<endl<<tab
//                                                  <<Q2A<<tab<<Q1B<<tab<<(Q1B*Q2B+Q1A*Q2A*scalars[7])<<tab<<
//                    (Q1A*Q2B*scalars[29] - scalars[31]* Q2A*Q1B) * denorminator <<endl<<endl;


                }
//                cout<<endl;

            }
//            cout<<"---------------"<<endl;




//                        cout<< "test: "<<gluon->operator()<1>({scalars[0]})<<endl;
//                        array<Cdoub,6> scalarp  = get_scalar_prod<6,5>({pp,zp,ll,zl,yl},{0,1,2,3,4},{1,2,3,6,7,8}, ps);
//                        cout<< "scalar: "<<scalarp[0]<<tab << scalarp[3]<<tab <<scalarp[5]<<endl;

        }
    }


    return vec;
}



//The Cheby Version of functions above - Getting the Kernelmatrix:
matCdoub MesonBse::get_Matrix_Cheb() {

    double color_factor = 4./3.;

    //The Prefactor of the Integration includes: the color factor, the trivial integration of 2 Pi,
    // the integral factor 1/(2Pi)⁴, the Z2² and the factor of 1/2 from the radial integration(or symmetry?).
    double prefac_integration = - color_factor * 0.5 /( pow( 2. * M_PI, 4)) * 2. * M_PI * Z2_Z2;
/// prefac stimmt


    //Saving thing in kernel_all, initalizing this qunatitiy and the superindices.
    SuperIndex<3> Si_I({parameter.n_projector,parameter.n_pp, parameter.n_cheb});
    SuperIndex<3> Si_J({parameter.n_basis, parameter.n_ll, parameter.n_cheb});
    //Initalizing everything with 0's first.
    matCdoub kernel_all(Si_I.getNsuper_index(), VecCdoub(Si_J.getNsuper_index(),0.0));

    //defining Si belonging to Y matrix
    SuperIndex<2> Si_Y_dirac({parameter.n_projector,parameter.n_basis});
    SuperIndex<2> Si_Y_discrete({ parameter.n_ll, parameter.n_lz});



    //Superindex for Lmat: (1) precalcuated angles and new variable to safe angles. (below) for direct.
    SuperIndex<4> Si_angle({parameter.n_pp, parameter.n_cheb, parameter.n_ll, parameter.n_lz});


    //Precalcultion of the y-angle and the rotation matrix Y = Gmat (quark propagators).
    // With time meassurment.
    time_t start, start1, start2, start3, end1, end2, end3;
    time(&start);

///----Choose either precalc of angluar or precalc of Lmat:

    //Precalculating only the angles and safing them.
//#ifdef L_CALC_ANGLE

    time(&start1);
    matCdoub preangles= precalc_angles_Cheb();
    time(&end1); give_time(start1,end1,"precalc angles");

//#endif

//Precalculating the whole Lmatrix.
//#ifdef L_CALC_ALL

//    time(&start3);
//    SuperIndex<4> Si_Lmat_discrete({parameter.n_pp, parameter.n_cheb, parameter.n_ll, parameter.n_lz});
//    matCdoub L_mat_kernel = precalc_Lmat_Cheb();
//    time(&end3); give_time(start3,end3,"precalc Lmat");

//#endif
//Using pre_calc angl is about 50% faster (?!)

//-------------------------------------------------------------

    //Calculating the rotation matrix
    time(&start2);
    matCdoub Y_kernel = precalc_rotation_Y();
    time(&end2); give_time(start2,end2,"precalc Gmat");


    //----------------------------------------------------------
    //These Objects only need to be defined when using precalc ang

    //Defining Objects needed in the loop
    ArrayScalarProducts scalars;
    //Saving the angular integral. Form the precalc angle version.
    array<Cdoub, K_ORDER_OF_ANGLE> ang_safe;
    //Needed to calcuate the L_kernel here.
    matCdoub L_kernel(parameter.n_projector, VecCdoub(parameter.n_basis, 0.0));

    //----------------------------------------------------------------------

//Also change pargma setting here - when switching to other kind of calculation:
//#pragma omp parallel for default(shared)
#pragma omp parallel for firstprivate(ang_safe, L_kernel, scalars)
//#pragma omp parallel for firstprivate(L_kernel, scalars, ang_safe)
//#pragma omp parallel for default(shared)
    for (int cheb_alpha = 0; cheb_alpha < parameter.n_cheb; ++cheb_alpha) {

        for (int pp = 0; pp < parameter.n_pp; ++pp) {

            for (int ll = 0; ll < parameter.n_ll; ++ll) {

                for (int cheb_beta = 0; cheb_beta < parameter.n_cheb; ++cheb_beta) {

                    //INTEGRAL!!!!!!
                    //summed up- integral
                    for (int zl = 0; zl < parameter.n_lz; ++zl) {

                        //The integration factor includes: all the weights, for w_ll and w_zl. and the Jacobian sqrt(1-zl²)*ll.
                        //Here the sqrt(1-z²) belongs to the other continious Projector of z_l : P^beta(z_l)
                        // And the factor form the Tcheby discret Projector = (2/n_c): (should to out of consistency probably appear in the L calc?)
                        double integration_factor =
                                ps.ll.getWeights_integration()[ll] *                    //weights
                                ps.zl.getWeights_integration()[zl] *
                                sqrt(1. - ps.zl.getGrid()[zl] * ps.zl.getGrid()[zl]) * //(1-z²)
                                ps.ll.getGrid()[ll] * (2.0 / parameter.n_cheb);                                  // l²

                        cheb Cheby;
                        Cdoub T_beta = Cheby.get(cheb_beta, ps.zl.getGrid()[zl]) * pow(cheby_factor, cheb_beta);

                        //----------------------------------------------------------------------------
                        //----------------------------------------------------------------------------
                        //THIS PART: is only needed when we are using precalc ang not L_mat
                        //Cacluating Lmat with the precalculated ang_safe[].

                        matCdoub L_kernel_sumed(parameter.n_projector, VecCdoub(parameter.n_basis, 0.0));
                        for (int cheb_m = 0; cheb_m < parameter.n_cheb; ++cheb_m) {

                            //T_alpha(z_cheb_m)
                            cheb Cheby(parameter.n_cheb);
                            Cdoub T_alpha_m = Cheby.get(cheb_alpha, cheb_m) * pow(cheby_factor,-cheb_alpha);

                            get_all_scalar_prod_cheb<5,4>({pp,cheb_m,ll,zl},Lmat::depends_on_sphericals,Lmat::depends_on_scalar_products,
                                                          ps, scalars);



                            //accessing precalculated data
                            ang_safe[0] = preangles[0][Si_angle.is({pp, cheb_m, ll, zl})];
                            ang_safe[1] = preangles[1][Si_angle.is({pp, cheb_m, ll, zl})];
                            ang_safe[2] = preangles[2][Si_angle.is({pp, cheb_m, ll, zl})];
                            ang_safe[3] = preangles[3][Si_angle.is({pp, cheb_m, ll, zl})];


                            //calculating Lmat
                            kernelL(L_kernel, scalars, ang_safe);
                            for (int i = 0; i < parameter.n_projector; ++i) {
                            for (int k = 0; k < parameter.n_basis; ++k) {
                                L_kernel_sumed[i][k] += L_kernel[i][k] *T_alpha_m;
                            }}
                        }

                        //---------------------------------------------------------------------------------
                        //---------------------------------------------------------------------------------


                        for (int i = 0; i < parameter.n_projector; ++i) {
                            for (int k = 0; k < parameter.n_basis; ++k) {

                                for (int j = 0; j < parameter.n_basis; ++j) {

//                                    kernel_all[Si_I.is({i,pp,cheb_alpha})][Si_J.is({k,ll,cheb_beta})]
//                                            +=  L_mat_kernel[Si_Y_dirac.is({i,j})][Si_Lmat_discrete.is({pp, cheb_alpha, ll, zl})]
//                                                * Y_kernel[Si_Y_dirac.is({j,k})][Si_Y_discrete.is({ll,zl})]
//                                                * integration_factor
//                                                * prefac_integration* T_beta;

                                        kernel_all[Si_I.is({i, pp, cheb_alpha})][Si_J.is({k, ll, cheb_beta})]
                                                += L_kernel_sumed[i][j]
                                                   * Y_kernel[Si_Y_dirac.is({j, k})][Si_Y_discrete.is({ll, zl})]
                                                   * integration_factor
                                                   * prefac_integration * T_beta ;


//                                cout<<L_mat_kernel[Si_Y_dirac.is({i,j})][Si_Lmat_discrete.is({pp, cheb_alpha, ll, zl})]<<tab<<
//                                            Y_kernel[Si_Y_dirac.is({j,k})][Si_Y_discrete.is({ll,zl})]<<tab<<  T_beta<<endl;




                                }

                            }
//                            }


                        }
                    }
                }
            }
        }
    }

    time(&end2); give_time(start,end2,"pion kernel calculated!");



/*    array<Cdoub, K_ORDER_OF_ANGLE> ang_safe;
    array< array <Cdoub,parameter.n_projector>,parameter.n_basis> L_kernel;
    ArrayScalarProducts scalars;
    for (int alpha = 0; alpha < parameter.n_cheb; ++alpha){
    for (int pp = 0; pp < parameter.n_pp; ++pp) {
    for (int i = 0; i < parameter.n_projector ; ++i) {
    for (int beta = 0; beta <parameter.n_cheb ; ++beta){
    for (int ll = 0; ll < parameter.n_ll; ++ll) {
    for (int k = 0; k < parameter.n_basis; ++k) {
    for (int j = 0; j <parameter.n_basis; ++j) {
    for (int zl = 0; zl < parameter.n_lz; ++zl) {


//        ang_safe[0] = preangles[0][Si_angle.is({pp,zp,ll,zl})];
//        ang_safe[1] = preangles[1][Si_angle.is({pp,zp,ll,zl})];
//        ang_safe[2] = preangles[2][Si_angle.is({pp,zp,ll,zl})];
//        ang_safe[3] = preangles[3][Si_angle.is({pp,zp,ll,zl})];

//        get_all_scalar_prod<5,4>({pp,zp,ll,zl},Lmat::depends_on_sphericals,Lmat::depends_on_scalar_products, ps, scalars);

//        kernelL(L_kernel, scalars,ang_safe);


        cout<<i<<tab<<j<<tab<<k <<tab<<Y_kernel[Si_Y_dirac.is({j,k})][Si_Y_discrete.is({ll,zl})]
            << tab << L_mat_kernel[Si_Y_dirac.is({i,j})][Si_Lmat_discrete.is({pp, alpha, ll, zl})] <<endl;

//        cout<<kernel_all[Si_I.is({i,pp,zp})][Si_J.is({k,ll,zl})]<<tab <<tab <<Y_kernel[Si_Y_dirac.is({i,k})][Si_Y_discrete.is({ll,zl})]
//            <<  tab << L_mat_kernel[Si_Y_dirac.is({i,k})][Si_Lmat_discrete.is({pp, zp, ll, zl})] <<endl;
    }}
       cout<< "kernel("<<i<<","<<k<<") = " <<kernel_all[Si_I.is({i,pp,alpha})][Si_J.is({k,ll,beta})]<<endl;
    }}}}}}*/




    return kernel_all;




}
matCdoub MesonBse::precalc_angles_Cheb() {

    SuperIndex<4> Si_angle({parameter.n_pp, parameter.n_cheb, parameter.n_ll, parameter.n_lz});

    matCdoub ang(K_ORDER_OF_ANGLE, VecCdoub(Si_angle.getNsuper_index(),0.0));

    //Initilaizing the Matrix that safe the angular values with all zero's such that it can be summed onto.
    // order_of_angle_integral is the number that tell you how high of an order is safed. In the kernel it should be higher than 4.
    // (See Kernel Form code)
    //Needs to be private for the paralellisation.
    Cdoub vec[K_ORDER_OF_ANGLE]= {0.0};
    ArrayScalarProducts scalars;


//#pragma omp parallel for default(shared)
#pragma omp parallel for firstprivate(vec, scalars)
    for (int zl = 0; zl < parameter.n_lz; ++zl) {
        for (int ll = 0; ll < parameter.n_ll; ++ll) {
            for (int pp = 0; pp < parameter.n_pp; ++pp) {
                for (int cheb_m = 0; cheb_m < parameter.n_cheb; ++cheb_m) {


                    //Resetting the array values to zeros for summing them up.
                    for (int i = 0; i < K_ORDER_OF_ANGLE; ++i) {
                        vec[i] = 0.0; }


                    //This part of the dependce is integrated out and safed in vec[0] to be written into ang[0][pp,zp,ll,zl]:
                    //---INTEGRAL----
                    for (int yl = 0; yl < parameter.n_ly; ++yl) {


                        get_all_scalar_prod_cheb<1, 5>({pp, cheb_m, ll, zl, yl}, gluon::depends_on_sphericals,
                                                       gluon::depends_on_scalar_products, ps, scalars);
                        Cdoub kk = scalars[gluon::depends_on_scalar_products[0]];
                        Cdoub current_gluon = gluon->operator()<1>({kk});

                        double integration_factor = ps.yl.getWeights_integration()[yl];

                        vec[0] +=  integration_factor * current_gluon * pow(kk, -1.0);
                        vec[1] +=  integration_factor * current_gluon * 1.0;
                        vec[2] +=  integration_factor * current_gluon * pow(kk, 1.0);
                        vec[3] +=  integration_factor * current_gluon * pow(kk, 2.0);
                        //                        vec[4][Si_angle.is({pp,zp,ll,zl})] += current_gluon *esther_pow::pow(ps.yl.getGrid()[yl],4);

                        //                        cout<< "test: "<<gluon->operator()<1>({scalars[0]})<<endl;
                        //                        array<Cdoub,6> scalarp  = get_scalar_prod<6,5>({pp,zp,ll,zl,yl},{0,1,2,3,4},{1,2,3,6,7,8}, ps);
                        //                        cout<< "scalar: "<<scalars[0]<<tab << vec[0][Si_angle.is({pp,zp,ll,zl})]<<tab <<vec[1][Si_angle.is({pp,zp,ll,zl})]<<endl;

                    }

                    ang[0][Si_angle.is({pp,cheb_m,ll,zl})] +=  vec[0];
                    ang[1][Si_angle.is({pp,cheb_m,ll,zl})] +=  vec[1];
                    ang[2][Si_angle.is({pp,cheb_m,ll,zl})] +=  vec[2];
                    ang[3][Si_angle.is({pp,cheb_m,ll,zl})] +=  vec[3];

                }
            }
        }
    }
//    cout<<result[0][0]<<tab<<result[3][10]<<endl;
//    delete2D(order_of_angle_integral, Si_angle.getNsuper_index(), result);

    return ang;

//    for (int j = 0; j < Si_angle.getNsuper_index(); ++j) {
//        cout<<result[0][j]<<tab<<result[2][j]<<endl;
//    }

}
matCdoub MesonBse::precalc_Lmat_Cheb() {

    SuperIndex<2> Si_dirac({parameter.n_projector,parameter.n_basis});
    SuperIndex<4> Si_discrete({parameter.n_pp, parameter.n_cheb, parameter.n_ll, parameter.n_lz});


    matCdoub vec(Si_dirac.getNsuper_index(), VecCdoub(Si_discrete.getNsuper_index(),0.0));
//    fv<Cdoub> P;
//    P.content[3]=ps.getPhaseSpacePsquared()*i_;

    //Defining objects used in the loops
    ArrayScalarProducts scalarsLmat;
    ArrayScalarProducts scalarsgluon;
    array<Cdoub, K_ORDER_OF_ANGLE> ang;

    matCdoub kernel(parameter.n_projector, VecCdoub(parameter.n_basis, 0.0));

//#pragma omp parallel for default(shared)
//#pragma omp parallel for schedule(dynamic,1) collapse(2)
#pragma omp parallel for schedule(dynamic,1) collapse(2) firstprivate(kernel, ang, scalarsLmat,scalarsgluon )
    for (int zl = 0; zl < parameter.n_lz; ++zl) {

        for (int ll = 0; ll < parameter.n_ll; ++ll) {

            for (int pp = 0; pp < parameter.n_pp; ++pp) {

                for (int cheb_alpha = 0; cheb_alpha < parameter.n_cheb; ++cheb_alpha) {






                    //This part uderneath is summed up!!!!
//                            Cdoub sum=0.0;
                    matCdoub sum(parameter.n_projector, VecCdoub(parameter.n_basis, 0.0));
                    for (int cheb_m = 0; cheb_m < parameter.n_cheb; ++cheb_m) {

//                                double zq=cos((cheb_m-0.5+1.0)*M_PI/parameter.n_cheb);
//                                double zq= ps.AllGrid[5]->getGrid()[cheb_m];

                        //T_alpha(z_cheb_m)
                        cheb Cheby(parameter.n_cheb);
                        Cdoub T_alpha_m = Cheby.get(cheb_alpha, cheb_m) * pow(cheby_factor,-cheb_alpha);

                        get_all_scalar_prod_cheb<5,4>({pp,cheb_m,ll,zl},Lmat::depends_on_sphericals,Lmat::depends_on_scalar_products,
                                                      ps, scalarsLmat);

                        for (int yl = 0; yl < parameter.n_ly; ++yl) {


                            double integration_factor = ps.yl.getWeights_integration()[yl];

                            get_all_scalar_prod_cheb<1, 5>({pp, cheb_m, ll, zl, yl}, gluon::depends_on_sphericals,
                                                           gluon::depends_on_scalar_products, ps,
                                                           scalarsgluon);

                            Cdoub kk = scalarsgluon[gluon::depends_on_scalar_products[0]];
                            Cdoub current_gluon = gluon->operator()<1>({kk});

                            ang[0]= pow(kk,-1.0); ang[1] =  pow(kk, 0.0); ang[2] =  pow(kk, 1.0); ang[3] = pow(kk,2.0);

//                                    fv<Cdoub> k, l, q;
//
//                                    q.set_spherical(ps.pp.getGrid()[pp], 0.0, 0.0,zq );
//                                    k.set_spherical(ps.ll.getGrid()[ll],0.0, ps.yl.getGrid()[yl], ps.zl.getGrid()[zl] );



//                                    l=k-q;
//                                    l.content[0]=0.0;
//                                    l.content[1] = -sqrt(ps.ll.getGrid()[ll])*sqrt(1.0-pow(ps.yl.getGrid()[yl],2.0)) * sqrt(1.0-pow(ps.zl.getGrid()[zl],2.0));
//                                    l.content[2] = sqrt(ps.pp.getGrid()[pp])*sqrt(1.0-pow(zq,2.0)) - sqrt(ps.ll.getGrid()[ll])*ps.yl.getGrid()[yl]* sqrt(1.0-pow(ps.zl.getGrid()[zl],2.0));
//                                    l.content[3] =  sqrt(ps.pp.getGrid()[pp])*zq-sqrt(ps.ll.getGrid()[ll])*ps.zl.getGrid()[zl];

//                                    cout<<"Let see l : "<<l.content[0]<<tab<<l.content[1]<<tab<<l.content[2]<<endl;


//                                    kernelL_notopti(kernel, scalarsLmat, ang);

//                                    scalarsLmat[35] = P*P;
//                                    scalarsLmat[34] = q*P;
//                                    scalarsLmat[27] = q*q;
//                                    scalarsLmat[43] = P*k;
//                                    scalarsLmat[44] = k*k;
//                                    Cdoub current_gluon = gluon->operator()<1>({l*l});

//                                    ang[0]= pow(l*l,-1.0); ang[1] =  pow(l*l, 0.0); ang[2] =  pow(l*l, 1.0); ang[3] = pow(l*l,2.0);



                            kernelL(kernel, scalarsLmat, ang);


                            for (int i = 0; i < parameter.n_projector ; ++i) {
                                for (int j = 0; j < parameter.n_basis; ++j) {


                                    sum[i][j] += kernel[i][j] * integration_factor * current_gluon * T_alpha_m;

                                }
                            }


//                                    cout<< "doubles outside:    "<< ps.pp.getGrid()[pp] <<tab<< zq <<tab<< ps.ll.getGrid()[ll]<<
//                                        tab<< ps.zl.getGrid()[zl]<<tab << ps.yl.getGrid()[yl]<<endl;
//                                    cout<< sum <<tab<< kernel[i][j] <<tab << kk <<tab<< l*l <<endl;

//                                    cout<< scalarsgluon[0] << tab <<l*l<< endl;
//                                    l=q-k;  cout<<l*l<<endl;
//                                    cout<<"Let see l : "<<l.content[0]<<tab<<l.content[1]<<tab<<l.content[2]<<endl;
//                                    cout<< scalarsLmat[27] <<tab << q*q<<endl;
//                                    cout<< scalarsLmat[35] <<tab << P*P<<endl;
//                                    cout<< scalarsLmat[34] <<tab << q*P<<endl;
//                                    cout<< scalarsLmat[43] <<tab << P*k<<endl;
//                                    cout<< scalarsLmat[44] <<tab << k*k<<endl<<endl;



                        }

                    }

                    for (int i = 0; i < parameter.n_projector ; ++i) {
                        for (int j = 0; j < parameter.n_basis; ++j) {

                            vec[Si_dirac.is({i, j})][Si_discrete.is({pp, cheb_alpha, ll, zl})] = sum[i][j];

                            //                            cout<<"outside: "<<i<<tab<<j<<sum<<endl;

                        }
                    }



                }
            }
        }
    }

    return vec;


}




//Function for the norm + decay constant:
void MesonBse::calc_boundstate_norm(double epsilon) {


    Cdoub result1, result2;

//    double c_mass=imag(ps.getPsquared());
    double c_mass=sqrt(abs(ps.getPsquared()));
    double bs_mass= sqrt(c_mass*c_mass+ epsilon);

    result1 = boundstate_norm(c_mass, bs_mass);

    bs_mass= sqrt(c_mass*c_mass- epsilon);
    result2 = boundstate_norm(c_mass, bs_mass);

    Cdoub result= (result1-result2)/(2.0*epsilon);

//    double t_mass=0.0;
//    cout<<"-----------> Only the norm: "<<boundstate_norm(c_mass, t_mass)<<endl;


    //Resultnorm=8.5; for the standart MT parameters for a pseudo scalar. !!!  //important: Here finmalresult= "multiplication factor"= 1/sqrt(N)
    Cdoub finalresult = (1.0/sqrt(result) );
    if(which_solver == "matrix_cheb" || which_solver == "vector_cheb"){
        finalresult = finalresult/sqrt(2.0);
    }

    for (int i = 0; i <meson.getSize() ; ++i) {
        Cdoub value = meson.operator()(i);
        meson.set(i, (value * finalresult) );

    }

    cout<<"The Normalization: N= "<<finalresult<<endl;
//    printf("%.10f\n",finalresult.real());
    cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;

    //and now the decay constant:

    Cdoub fpi = decay_constant();

    cout<<"The Decay constant: fpi= "<<fpi<<endl;
    cout<<"|.....................................................|"<<endl<<"|.....................................................|"<<endl;







}

Cdoub MesonBse::boundstate_norm(double& currentmass, double& epsmass) {

//This is the function to calculate the Norm of the boundstate.
//This includes a integration over a loop momentum, whereas the function only depends on pp, zp.
//The outside momenta are used cause this is where the function is calculated in and in case of a nessercary interpoaltion we would need to interpolate here again,
// which costs additional time.

    //Through comparison with Christian I discovered that I was missing a factor of Z^(-2) here.I think it is due to the fact that the normed BSE has to go with G ~ Z_2^2, and since the iteration leave no trace of this one has to use a norm factor to get G_N ~ G/sqrt(N) where N~1/Z_2².

    Cdoub norm;
    double sumR=0.0;
    double sumI=0.0;

    Cdoub Vorfac=0.0;
    if(which_meson == "vector" || which_meson == "axial"){
         Vorfac =  (1.0/(M_PI*M_PI*M_PI*M_PI* 16.0))*3.0 *2.0 * (2.0 * M_PI)  *2.0 * (-1.0/3.0);
        //Factors = integral factor * Color factor * flavor factor * frist angular integral * second trivial angular integral * 1/3 norm factor + minus sign due to charge conjugation

    }else if(which_meson == "ps"){
         Vorfac = (1.0/(M_PI*M_PI*M_PI*M_PI* 16.0))*3.0 *2.0 * (2.0 * M_PI)  *2.0;// * (1.0/Z2_Z2);
        //Factors = integral factor * Color factor * flavor factor * frist angular integral * second trivial angular integral
    }else if(which_meson == "scalar"){
        Vorfac =  - (1.0/(M_PI*M_PI*M_PI*M_PI* 16.0))*3.0 *2.0 * (2.0 * M_PI)  *2.0 ;
        //important: Where is the minus coming from?
        cout<<"Cheap fix for the Norm Integral in case of the scalar!!!"<<endl;
        //Factors = integral factor * Color factor * flavor factor * frist angular integral * second trivial angular integral * additional (-1) factor (why? - also charge conjugation) * Z^(-2)
    } else{assert(false);}


    ArrayScalarProducts scalarsQuarks;
    matCdoub kernel(parameter.n_projector, VecCdoub(parameter.n_basis, 0.0));
//    fv<Cdoub> P,Q,l,qp,qm;
//    array<array<Cdoub,K_N_PROJECTORS_DIRAC>,K_N_BASE_ELEMENTS_DIRAC> kernel;


//#pragma omp parallel for default(none) collapse(2) shared(epsmass,currentmass)
//#pragma omp parallel for firstprivate(kernel) collapse(2) reduction(+:sumR,sumI)
//#pragma omp parallel for default(shared) collapse(2) reduction(+:sumR,sumI)
//#pragma omp parallel for collapse(2) reduction(+:sumR,sumI) firstprivate(kernel, scalarsQuarks, P, Q, l, qp, qm)
#pragma omp parallel for collapse(2) reduction(+:sumR,sumI) firstprivate(kernel, scalarsQuarks)
    for (int pp = 0; pp < parameter.n_pp; ++pp) {

        for (int zp = 0; zp < parameter.n_pz; ++zp) {

//            ArrayScalarProducts scalarsQuarks;

//            array<array<Cdoub,K_N_PROJECTORS_DIRAC>,K_N_BASE_ELEMENTS_DIRAC> kernel;
//            matCdoub kernel;
//            initMat(kernel, parameter.n_projector, parameter.n_basis);

            get_all_scalar_prod<2,2>({pp,zp},quark_p1_norm::depends_on_sphericals,{quark_p1_norm::depends_on_scalar_products[0],
                                                     quark_p2_norm::depends_on_scalar_products[0]},
                                     ps, scalarsQuarks, epsmass);

            Cdoub Q1A = dse1->getRhsA(scalarsQuarks[quark_p1_norm::depends_on_scalar_products[0]]);
            Cdoub Q1B = dse1->getRhsB(scalarsQuarks[quark_p1_norm::depends_on_scalar_products[0]]);
            Cdoub Q2A = dse2->getRhsA(scalarsQuarks[quark_p2_norm::depends_on_scalar_products[0]]);
            Cdoub Q2B = dse2->getRhsB(scalarsQuarks[quark_p2_norm::depends_on_scalar_products[0]]);

            double pp_doub= ps.pp.getGrid()[pp];  double zp_doub= ps.zp.getGrid()[zp];
            fv<Cdoub> P,Q,l,qp,qm;
            P.content[3] = i_*epsmass;
            Q.content[3] = i_* currentmass;
            l.set_spherical(pp_doub, 0.0, 1.0, zp_doub );
            qp = l + (0.5)*P;
            qm = l - (0.5)*P; //Is the minus operation working??? Wasn't earlier...!!!
//            cout<<P[3]<<tab<<P[2]<<tab<<Q[3]<<tab<<Q[2]<<tab<<l[3]<<tab<<l[2]<<tab<<Q1A<<tab<<Q2A<<tab<<Q2B<<endl;

            Cdoub qp_qm = qp*qm;
            Cdoub Q_qm = Q*qm;
            Cdoub Q_qp = qp*Q;
            Cdoub Q_l = Q*l;
            Cdoub l_qm = l*qm;
            Cdoub l_qp = qp*l;
            Cdoub Q_Q = Q*Q;
            Cdoub l_l = pp_doub;


            normalisation (kernel,
                           qp_qm, Q_qm, Q_qp, Q_l, l_qm, l_qp, Q_Q, l_l,
                           Q1A, Q1B, Q2A, Q2B);

            //The integration factor includes: all the weights, for w_pp and w_zp. and the Jacobian sqrt(1-zp²)*pp.
            Cdoub integrationfactor = pp_doub*sqrt(1.0-zp_doub*zp_doub) * 0.5*
                                      ps.pp.getWeights_integration()[pp] * ps.zp.getWeights_integration()[zp];

            Cdoub denorminator = 1.0/( (scalarsQuarks[quark_p1_norm::depends_on_scalar_products[0]] * Q1A*Q1A +Q1B*Q1B)
                                       *(scalarsQuarks[quark_p2_norm::depends_on_scalar_products[0]] *Q2A*Q2A + Q2B*Q2B) );

            Cdoub temp;


            for (int i = 0; i < parameter.n_projector ; ++i) {

                for (int j = 0; j < parameter.n_basis; ++j) {

                    if(which_solver == "matrix_cheb" || which_solver == "vector_cheb"){

                        VecCdoub amp1(parameter.n_basis,0.0);
                        VecCdoub amp2(parameter.n_basis,0.0);

                        cheb Cheby(parameter.n_cheb);

                        for (int i_cheb = 0; i_cheb < parameter.n_cheb; ++i_cheb){
                            Cdoub T_cheby = Cheby.get(i_cheb, ps.zp.getGrid()[zp]);
                            amp1[j] += meson({j,pp,i_cheb}) * T_cheby * pow(i_, i_cheb);
                            amp2[i] += meson({i,pp,i_cheb}) * T_cheby * pow(i_, i_cheb);
                        }

                        temp = kernel[i][j] * denorminator *integrationfactor * amp1[j] * amp2[i];
                        sumR += real(temp); sumI += imag(temp);
//                        sum +=  kernel[i][j] * denorminator *integrationfactor * amp1[j] * amp2[i] ;

                    }else{

                        temp = kernel[i][j] * denorminator *integrationfactor * meson({j,pp,zp}) *meson({i,pp,zp});
                        sumR += real(temp); sumI +=imag(temp);
//                        sum +=  kernel[i][j] * denorminator *integrationfactor * meson({j,pp,zp}) *meson({i,pp,zp}) ;

                    }


//                    cout<<i<<tab<<j<<tab<<kernel[i][j]* denorminator<<tab<<Q1A<<tab<<ps.pp.getGrid()[pp]<<tab<<temp<<endl;
//                    cout<<i<<tab<<j<<tab<<kernel[i][j]* denorminator<<tab<<Q1A<<tab<<ps.pp.getGrid()[pp]
//                        <<tab<<scalarsQuarks[2]<<tab<<scalarsQuarks[7]<<tab<<scalarsQuarks[31]<<endl<<tab
//                                                  <<Q2A<<tab<<Q1B<<tab<<(Q1B*Q2B+Q1A*Q2A*scalarsQuarks[7])<<tab<<
//                    (Q1A*Q2B*scalarsQuarks[29] - scalarsQuarks[31]* Q2A*Q1B) * denorminator <<endl<<endl;


                }

            }




//                        cout<< "test: "<<gluon->operator()<1>({scalars[0]})<<endl;
//                        array<Cdoub,6> scalarp  = get_scalar_prod<6,5>({pp,zp,ll,zl,yl},{0,1,2,3,4},{1,2,3,6,7,8}, ps);
//                        cout<< "scalar: "<<scalarp[0]<<tab << scalarp[3]<<tab <<scalarp[5]<<endl;

        }
    }

    norm= (sumR+ i_*sumI)*Vorfac;
//    cout<<"Norm: "<<norm<<endl; cout<<Vorfac<<endl;
//    norm= sum*Vorfac;

//    double dummy=0.0;
//    for (int k = 0; k < ps.yl.getNumber_of_grid_points(); ++k) {
//        dummy += 1.0*ps.yl.getWeights_integration()[k];
//    }
//cout<<"Look here: "<<dummy<<endl;


    return norm;
}
Cdoub MesonBse::decay_constant() {

    Cdoub fpi;
//    Cdoub sum=0.0;
    double sumR=0.0; double sumI=0.0;

    double Vorfac=0.0;
    if(which_meson == "vector" ){
        Vorfac =  (1.0/(M_PI*M_PI*M_PI*M_PI* 16.0))* sqrt(Z2_Z2)   *3.0 * (2.0 * M_PI)*  2.0 * (1.0/3.0) * (1.0/ sqrt(abs(meson.getPsquared())));
        //Factors = loop integration factor * Z_2 * Color factor * frist angle integral * second angle integral * 1/3 difference in Norm * 1/m_v

    }else if(which_meson == "ps"){
        Vorfac =  (1.0/(M_PI*M_PI*M_PI*M_PI* 16.0))* (sqrt(Z2_Z2) / ( real(abs(ps.getPsquared()))) )  *3.0 * (2.0 * M_PI)*  2.0;
        //Factors = loop integration factor * Z_2 * 1/P² * Color factor * frist angle integral * second angle integral
    }else{cout<< "In case of a " <<which_meson<<" meson we do not calculate a decay constant"<<endl; return 0.0+i_*0.0;}


    ArrayScalarProducts scalarsQuarks;
    fv<Cdoub> P,l, qp, qm;

    matCdoub kernel(parameter.n_projector, VecCdoub(parameter.n_basis, 0.0));
//    array<array<Cdoub,K_N_PROJECTORS_DIRAC>,K_N_BASE_ELEMENTS_DIRAC> kernel;

//#pragma omp parallel for default(shared) collapse(2) reduction(+:sumR,sumI)
//#pragma omp parallel for firstprivate(kernel) collapse(2) reduction(+:sumR,sumI)
#pragma omp parallel for collapse(2) reduction(+:sumR,sumI) firstprivate(kernel,scalarsQuarks,P ,l ,qp ,qm)
//for (int zp = 0; zp < parameter.n_pz; ++zp) {
    for (int pp = 0; pp < parameter.n_pp; ++pp) {

        for (int zp = 0; zp < parameter.n_pz; ++zp) {



//            ArrayScalarProducts scalarsQuarks;

//            array<array<Cdoub,K_N_PROJECTORS_DIRAC>,K_N_BASE_ELEMENTS_DIRAC> kernel;
//            matCdoub kernel;
//            initMat(kernel, parameter.n_basis, parameter.n_projector);

            get_all_scalar_prod<2,2>({pp,zp},quark_p1_norm::depends_on_sphericals,{quark_p1_norm::depends_on_scalar_products[0],
                                                     quark_p2_norm::depends_on_scalar_products[0]},
                                     ps, scalarsQuarks);

            Cdoub Q1A = dse1->getRhsA(scalarsQuarks[quark_p1_norm::depends_on_scalar_products[0]]);
            Cdoub Q1B = dse1->getRhsB(scalarsQuarks[quark_p1_norm::depends_on_scalar_products[0]]);
            Cdoub Q2A = dse2->getRhsA(scalarsQuarks[quark_p2_norm::depends_on_scalar_products[0]]);
            Cdoub Q2B = dse2->getRhsB(scalarsQuarks[quark_p2_norm::depends_on_scalar_products[0]]);

            double pp_doub= ps.pp.getGrid()[pp];  double zp_doub= ps.zp.getGrid()[zp];
//            P.content[3] = i_*ps.getPhaseSpacePsquared();
            P.content[3] = sqrt(ps.getPsquared());
            l.set_spherical(pp_doub, 0.0, 1.0, zp_doub );
            qp = l + 0.5*P;
            qm = l - 0.5*P; //Is the minus operation working??? Wasn't earlier...!!!
//            cout<<P[3]<<tab<<P[2]<<tab<<Q[3]<<tab<<Q[2]<<tab<<l[3]<<tab<<l[2]<<tab<<Q1A<<tab<<Q2A<<tab<<Q2B<<endl;

            Cdoub qp_qm = qp*qm;
            Cdoub P_qm = P*qm;
            Cdoub P_qp = qp*P;
            Cdoub P_l = P*l;
            Cdoub l_qm = l*qm;
            Cdoub l_qp = qp*l;
            Cdoub P_P = P*P;
            Cdoub l_l = pp_doub;

//            array<Cdoub, parameter.n_projector> Gamma;
            VecCdoub Gamma(parameter.n_projector);



            //The integration factor includes: all the weights, for w_pp and w_zp. and the Jacobian sqrt(1-zp²)*pp.
            Cdoub integrationfactor = pp_doub*sqrt(1.0-zp_doub*zp_doub) * 0.5*
                                      ps.pp.getWeights_integration()[pp] * ps.zp.getWeights_integration()[zp];

            Cdoub denorminator = 1.0/( (scalarsQuarks[quark_p1_norm::depends_on_scalar_products[0]] * Q1A*Q1A +Q1B*Q1B)
                                       *(scalarsQuarks[quark_p2_norm::depends_on_scalar_products[0]] *Q2A*Q2A + Q2B*Q2B) );



                for (int j = 0; j < parameter.n_basis; ++j) {

                    if(which_solver == "matrix_cheb" || which_solver == "vector_cheb"){

                        VecCdoub amp1(parameter.n_basis,0.0);

                        cheb Cheby(parameter.n_cheb);

                        for (int i_cheb = 0; i_cheb < parameter.n_cheb; ++i_cheb){
                            Cdoub T_cheby = Cheby.get(i_cheb, ps.zp.getGrid()[zp]);
                            amp1[j] += meson({j,pp,i_cheb}) * T_cheby * pow(i_, i_cheb) * (sqrt(2.0));
                            //Comment: Need to put in a factor of sqrt(2.0) because I'm forcing my Powermethod to norm to 1.0
                        }

                        Gamma[j] = amp1[j];

                    }else{

                        Gamma[j] = meson({j,pp,zp});

                    }

                }


            Cdoub fpi_int = calc_fpi(qp_qm, P_qm, P_qp, P_l, l_qm, l_qp, P_P, l_l,
                                     Gamma,
                                     Q1A, Q1B, Q2A, Q2B);

//            cout<<"This is fpi ("<<zp<<","<<pp<<") : "<<fpi_int<<tab<<l_l<<tab<<Gamma[0]<<tab<<P_l<<tab<<denorminator<<tab<<integrationfactor<<endl;

                Cdoub temp = fpi_int * denorminator *integrationfactor ;

                sumR +=  real(temp) ; sumI +=  imag(temp) ;


//                    cout<<i<<tab<<j<<tab<<kernel[i][j]* denorminator<<tab<<Q1A<<tab<<ps.pp.getGrid()[ll]
//                        <<tab<<scalars[2]<<tab<<scalars[7]<<tab<<scalars[31]<<endl<<tab
//                                                  <<Q2A<<tab<<Q1B<<tab<<(Q1B*Q2B+Q1A*Q2A*scalars[7])<<tab<<
//                    (Q1A*Q2B*scalars[29] - scalars[31]* Q2A*Q1B) * denorminator <<endl<<endl;




//                        cout<< "test: "<<gluon->operator()<1>({scalars[0]})<<endl;
//                        array<Cdoub,6> scalarp  = get_scalar_prod<6,5>({pp,zp,ll,zl,yl},{0,1,2,3,4},{1,2,3,6,7,8}, ps);
//                        cout<< "scalar: "<<scalarp[0]<<tab << scalarp[3]<<tab <<scalarp[5]<<endl;

        }
    }

    fpi= (sumR+i_*sumI)*Vorfac;
//    fpi= sum*Vorfac;

//    double dummy=0.0;
//    for (int k = 0; k < ps.yl.getNumber_of_grid_points(); ++k) {
//        dummy += 1.0*ps.yl.getWeights_integration()[k];
//    }
//cout<<"Look here: "<<dummy<<endl;


    return fpi;

}

//This function was only written to check the interpolation at some point. Possibly delete soon.
VecCdoub MesonBse::testy(){

    VecCdoub result;
//    for (int i = 0; i < parameter.n_basis; ++i) {
//        for (int rad = 0; rad < parameter.n_pp; ++rad) {
//            for (int ang = 0; ang < parameter.n_pz; ++ang) {
////                result.push_back( (cos (xvalues_interpol[1][ang] ) *cos (xvalues_interpol[1][ang] ) )
////                result.push_back( 1.0
////                                  /(1.0 + xvalues_interpol[0][rad]) );
//
//                result.push_back( pow(exp(xvalues_interpol[0][rad]),2.0) * exp(- exp(xvalues_interpol[0][rad])) );
//            }
//         }
//    }

    return result;

}

//Calcuting for several P^2 values and others: //important UPDATE VECTOR
void MesonBse::updateVector(const VecCdoub &amplitude, VecCdoub &amplitude_updated) {

    int kern_size_inside = kernel[0].size();
    VecCdoub local_amplitude; local_amplitude.resize(kern_size_inside);
    int kern_outside = kernel.size();

    //output for checking the interpolation:
//    write_out_amplitude(folder+"amp_before", amplitude, {0,1});

//cout<<"Check: "<<kern_outside<<tab<<amplitude.size()<<endl;
//    cout<<kern_size_inside<<tab<<kernel.size()<<tab<<amplitude.size()<<endl;



    if(interpolation_nessercary == true){

//This section of the code is just used to plot the interpolated value vs the original to check the interpolation.
#ifdef K_PLOTTING_INTERPOLATION


        VecCdoub test =  testy();

//        if(test.size() == amplitude.size()){cout<<"yess"<<endl;}

        matCdoub data_before;
        for(int i=0; i<3; i++){
            VecCdoub v;
            for(int j=0; j<amplitude.size(); j++){v.push_back(0.0);}
            data_before.push_back(v);
        }

//        VecDoub x_grid(amplitude.size());
//        VecDoub real_amp(amplitude.size());
//        VecDoub z_grid(amplitude.size());
        for (int k = 0; k < parameter.n_basis; ++k) {
            for (int i = 0; i <parameter.n_pp ; ++i) {
                for (int j = 0; j < parameter.n_pz; ++j) {

//                    x_grid[j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ] = ps.pp.getGrid()[i];
//                    z_grid[j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ] = ps.zp.getGrid()[j];
//                    real_amp[j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ]
//                            = real(test[j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ]);

                    data_before[0][j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ] = log(ps.pp.getGrid()[i]);
                    data_before[1][j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ] = ps.zp.getGrid()[j];
                    data_before[2][j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ]
                            = test[j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ];
//                            = amplitude[j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ];

                }
            }
        }


        //tryed to implment a direct plotting.. still is not working.
//
//        Gnuplot g2;
//        g2.reset_all();
//        g2.set_multiplot();
//        g2.plot_xyz( x_grid, z_grid, real_amp);
        string folder_here =folder+"/interpolation_before.txt";
        write(data_before, folder_here);

#endif
        //Update all vectors of the interpolaion and get the new values for amplitude local.
        interpolation_update(amplitude, local_amplitude);
//        interpolation_update(test, local_amplitude);


/*        //old version- not in extra function
//        Interpol.Update( xvalues_interpol, test);
//        Interpol.Update( xvalues_interpol, amplitude);
//
//        array<Cdoub, 4> result_y;
//        SuperIndex<3> Si_I({parameter.n_projector,parameter.n_ll, parameter.n_lz});
////        SuperIndex<3> Si_I({parameter.n_projector,parameter.n_pp, parameter.n_pz}); ///??????????
//        for (int i = 0; i < kern_size_inside ; ++i) {
//            auto indices = Si_I.are(i);
//            array<double,2> new_x_values ={log(ps.AllGrid[2]->getGrid()[indices[1]]),ps.AllGrid[3]->getGrid()[indices[2]]};
//
//            //performing the interpolation only one time for all amplitudes t[i]. E,F,G,H.
////            if(indices[0]== 0){
//                Interpol.getValue(result_y,new_x_values );
////            }
//
//
//            local_amplitude[i] = result_y[indices[0]];
//
//
//        }*/


//This section of the code is just used to plot the interpolated value vs the original to check the interpolation.
#ifdef K_PLOTTING_INTERPOLATION

        matCdoub data_after, data_after_function;

        for(int i=0; i<3; i++){
            VecCdoub v;
            for(int j=0; j<local_amplitude.size(); j++){v.push_back(0.0);}
            data_after.push_back(v);
            data_after_function.push_back(v);
        }


//        VecDoub x_grid_new(local_amplitude.size());
//        VecDoub z_grid_new(local_amplitude.size());
//        VecDoub real_amp_new(local_amplitude.size());
        for (int k = 0; k < parameter.n_basis; ++k) {
            for (int i = 0; i <parameter.n_ll ; ++i) {
                for (int j = 0; j < parameter.n_lz; ++j) {
//                    x_grid_new[j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ] = ps.ll.getGrid()[i];
//                    z_grid_new[j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ] = ps.zl.getGrid()[j];
//                    real_amp[j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ]
//                            = real(local_amplitude[j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ]);

                    data_after[0][j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ] =log( ps.ll.getGrid()[i] );
                    data_after[1][j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ] = ps.zl.getGrid()[j];
                    data_after[2][j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ]
                            = local_amplitude[j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ];

//                    data_after_function[0][j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ] = log( ps.ll.getGrid()[i] );
//                    data_after_function[1][j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ] = ps.zl.getGrid()[j];
//                    data_after_function[2][j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ]
////                            = ((cos ( ps.zl.getGrid()[j] ) *cos ( ps.zl.getGrid()[j] ) )
//                           = (1.0
//                               /(1.0 + log(ps.ll.getGrid()[i]) ));

                }
            }
        }

        folder_here = folder+"/interpolation_after.txt";
        write(data_after, folder_here);

//        folder_here = folder+"/interpolation_after_function.txt";
//        write(data_after_function, folder_here);


//        Gnuplot g3;
//        g3.plot_xyz( x_grid_new, z_grid_new, real_amp_new);



//        cin.get();

#endif


    }else{

        //just passing the updated amplitude.
        for (int j = 0; j < amplitude.size(); ++j) {
                local_amplitude[j] = amplitude[j];
        }
    }

    //output for checking the interpolation:
//    write_out_amplitude(folder+"amp_after", local_amplitude, {2,3});


    //Cheking if the kernel and the meson have the same size.
//    if(meson.getSize() !=kernel.size())
//    {cout<<"Update Vector: Wait something is wrong: "<<meson.getSize()<<tab<<kernel.size()<<endl; cin.get();}
//
    //now calculating the updated vector by performing the matrix vector product.
#pragma omp parallel for  //firstprivate(L_kernel, scalarsLmat, ang)
    for (int i = 0; i < amplitude.size(); ++i) {

        Cdoub sum=0.0;
        for (int j = 0; j < kern_size_inside; ++j) {

//             sum += kernel[i][j]*amplitude[j];
            sum += kernel[i][j]*local_amplitude[j];
        }
        amplitude_updated[i]=sum;
    }




}
void MesonBse::updateVector_without_kernelMat(const VecCdoub &amplitude, VecCdoub &amplitude_updated) {

    int kern_size_inside = kernel[0].size();
    VecCdoub local_amplitude; local_amplitude.resize(kern_size_inside);
    int kern_outside = kernel.size();


//------------INTERPOLATION PART START: ---------------------------------------------------------------------------------

//cout<<"Check: "<<kern_outside<<tab<<amplitude.size()<<endl;
//    cout<<kern_size_inside<<tab<<kernel.size()<<tab<<amplitude.size()<<endl;

    if(interpolation_nessercary == true){

//This section of the code is just used to plot the interpolated value vs the original to check the interpolation.
#ifdef K_PLOTTING_INTERPOLATION


        VecCdoub test =  testy();

//        if(test.size() == amplitude.size()){cout<<"yess"<<endl;}

        matCdoub data_before;
        for(int i=0; i<3; i++){
            VecCdoub v;
            for(int j=0; j<amplitude.size(); j++){v.push_back(0.0);}
            data_before.push_back(v);
        }

//        VecDoub x_grid(amplitude.size());
//        VecDoub real_amp(amplitude.size());
//        VecDoub z_grid(amplitude.size());
        for (int k = 0; k < parameter.n_basis; ++k) {
            for (int i = 0; i <parameter.n_pp ; ++i) {
                for (int j = 0; j < parameter.n_pz; ++j) {

//                    x_grid[j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ] = ps.pp.getGrid()[i];
//                    z_grid[j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ] = ps.zp.getGrid()[j];
//                    real_amp[j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ]
//                            = real(test[j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ]);

                    data_before[0][j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ] = log(ps.pp.getGrid()[i]);
                    data_before[1][j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ] = ps.zp.getGrid()[j];
                    data_before[2][j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ]
                            = test[j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ];
//                            = amplitude[j+ parameter.n_pz* i + parameter.n_pp*parameter.n_pz*k  ];

                }
            }
        }


        //tryed to implment a direct plotting.. still is not working.
//
//        Gnuplot g2;
//        g2.reset_all();
//        g2.set_multiplot();
//        g2.plot_xyz( x_grid, z_grid, real_amp);
        string folder_here =folder+"/interpolation_before.txt";
        write(data_before, folder_here);

#endif
        //Update all vectors of the interpolaion and get the new values for amplitude local.
        interpolation_update(amplitude, local_amplitude);
//        interpolation_update(test, local_amplitude);


/*        //old version- not in extra function
//        Interpol.Update( xvalues_interpol, test);
//        Interpol.Update( xvalues_interpol, amplitude);
//
//        array<Cdoub, 4> result_y;
//        SuperIndex<3> Si_I({parameter.n_projector,parameter.n_ll, parameter.n_lz});
////        SuperIndex<3> Si_I({parameter.n_projector,parameter.n_pp, parameter.n_pz}); ///??????????
//        for (int i = 0; i < kern_size_inside ; ++i) {
//            auto indices = Si_I.are(i);
//            array<double,2> new_x_values ={log(ps.AllGrid[2]->getGrid()[indices[1]]),ps.AllGrid[3]->getGrid()[indices[2]]};
//
//            //performing the interpolation only one time for all amplitudes t[i]. E,F,G,H.
////            if(indices[0]== 0){
//                Interpol.getValue(result_y,new_x_values );
////            }
//
//
//            local_amplitude[i] = result_y[indices[0]];
//
//
//        }*/


//This section of the code is just used to plot the interpolated value vs the original to check the interpolation.
#ifdef K_PLOTTING_INTERPOLATION

        matCdoub data_after, data_after_function;

        for(int i=0; i<3; i++){
            VecCdoub v;
            for(int j=0; j<local_amplitude.size(); j++){v.push_back(0.0);}
            data_after.push_back(v);
            data_after_function.push_back(v);
        }


//        VecDoub x_grid_new(local_amplitude.size());
//        VecDoub z_grid_new(local_amplitude.size());
//        VecDoub real_amp_new(local_amplitude.size());
        for (int k = 0; k < parameter.n_basis; ++k) {
            for (int i = 0; i <parameter.n_ll ; ++i) {
                for (int j = 0; j < parameter.n_lz; ++j) {
//                    x_grid_new[j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ] = ps.ll.getGrid()[i];
//                    z_grid_new[j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ] = ps.zl.getGrid()[j];
//                    real_amp[j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ]
//                            = real(local_amplitude[j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ]);

                    data_after[0][j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ] =log( ps.ll.getGrid()[i] );
                    data_after[1][j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ] = ps.zl.getGrid()[j];
                    data_after[2][j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ]
                            = local_amplitude[j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ];

//                    data_after_function[0][j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ] = log( ps.ll.getGrid()[i] );
//                    data_after_function[1][j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ] = ps.zl.getGrid()[j];
//                    data_after_function[2][j+ parameter.n_lz* i + parameter.n_ll*parameter.n_lz*k  ]
////                            = ((cos ( ps.zl.getGrid()[j] ) *cos ( ps.zl.getGrid()[j] ) )
//                           = (1.0
//                               /(1.0 + log(ps.ll.getGrid()[i]) ));

                }
            }
        }

        folder_here = folder+"/interpolation_after.txt";
        write(data_after, folder_here);

//        folder_here = folder+"/interpolation_after_function.txt";
//        write(data_after_function, folder_here);


//        Gnuplot g3;
//        g3.plot_xyz( x_grid_new, z_grid_new, real_amp_new);



//        cin.get();

#endif


    }else{

        //just passing the updated amplitude.
        for (int j = 0; j < amplitude.size(); ++j) {
            local_amplitude[j] = amplitude[j];
        }
    }



////--------------INTERPOLATION PART END--------------------------------------------------------------------------------


    //Cheking if the kernel and the meson have the same size.
//    if(meson.getSize() !=kernel.size())
//    {cout<<"Update Vector: Wait something is wrong: "<<meson.getSize()<<tab<<kernel.size()<<endl; cin.get();}
//
    //now calculating the updated vector by performing the matrix vector product.
#pragma omp parallel for  //firstprivate(L_kernel, scalarsLmat, ang)
    for (int i = 0; i < amplitude.size(); ++i) {

        Cdoub sum=0.0;
        for (int j = 0; j < kern_size_inside; ++j) {

//             sum += kernel[i][j]*amplitude[j];
            sum += kernel[i][j]*local_amplitude[j];
        }
        amplitude_updated[i]=sum;
    }


}

double MesonBse::find_the_bs_mass(double meson_mass, double range, double eps) {

    double Mstart = meson_mass - range;
    double Mend = meson_mass + range;
    double Mresult=0.0;
    double M_current=Mstart;

    array<Cdoub,3> saveeign;
    array<double,3> saveM;

    cout<<endl<<"__________|Starting interpolation procedure for pion mass.........|________"<<endl;
    cout<<"________________________________________________________"<<endl;
    cout<<"________________________________________________________"<<endl;

    cout<<"Mstart="<<Mstart<<" , Mend="<<Mend<<endl;

    int flaggy=1; //flaggy the criteria, when the whole while iteration stops.
    int flaggy2=0; //flaggy2 represent if the eiegenvalue of the result was bigger/smaller as the previous.

    double mcounter=0.0;
    while(flaggy>=1)
    {
        for(int qs=0; qs<3; qs++)
        {

            if(qs<2){M_current=Mstart+qs*(Mend-Mstart);}
            else{M_current=Mresult;}
            saveM[qs]=M_current;

            //We must delete the quarks form the previous calcuation here
            //It has to be here, as everytime we continue
//            if( (qs==1 && flaggy2 != 1) || (qs==1 && flaggy2 != 2) ){delet_multi_quarks();}


            if(flaggy2==1 && qs==0 || flaggy2==2 && qs==1)
            {saveeign[qs]=saveeign[2]; cout<<"ALREADY CALC: EV="<<saveeign[qs]<<"......M="<<M_current<<endl;}
            else if(flaggy2==1 && qs==1 || flaggy2==2 && qs==0)
            {saveeign[qs]=saveeign[qs]; cout<<"ALREADY CALC: EV="<<saveeign[qs]<<"......M="<<M_current<<endl;}
            else
            {
                //Calculation of eigenvalue

//                solve_and_update_pion(M_current);
                solve_and_update_pion(- M_current*M_current);

                saveeign[qs]=meson.getEigenvalue();

                //It must carefully be chosen where the quarks are deleted.
                // When deleting here they won't survive for the last run with the results.
                //Thus they must be deleted in the other if case...
                //important: As I am calling solve_and_update_pion(), which included update_multiquark() I need to call delete_multiquarks() as well.
                if(qs<2){delet_multi_quarks();}


            }

            //Calcuating the interpolation between the start and end value of M:
            if(qs==1)
            {   double slope=(real(saveeign[1])-real(saveeign[0]))/(Mend-Mstart);
                double bg=real(saveeign[0])-slope*Mstart;
                Mresult=(1.0-bg)/slope;
                cout<<"INTERPOLATION: "<<slope<<"*M+"<<bg<<" between Ms: "<<Mstart<<" & Me: "<<Mend<<" ,   MRESULT: "<<Mresult<<"    : "<<mcounter<<endl;
                //if(flaggy3=0){flaggy3=1; savslope=(real(saveeign[1])-real(saveeign[0]))/(Mend*Mend-Mstart*Mstart);}
                if(Mresult<0){cout<<"The value of the Mass we would need to calculate is below 0! ABORTED"<<endl; return 0;}
            }

//            double eps4=eps;

            if(qs==2)
            {
                double val=real(saveeign[2])-1.0;
                if(val>=eps){Mend=Mresult;flaggy2=2; delet_multi_quarks();}
                else if(val<=-eps){Mstart=Mresult;flaggy2=1; delet_multi_quarks(); }
                else{flaggy=0;}
            }

            if(qs==2 && Mresult<0){cout<<"You're borders make no sense! Mstart has to be smaller or not enough GP. ABORTED"<<endl; return 0;}

        }
        mcounter++;
    }

    cout<<"________________________________________________________"<<endl;

    cout<<"____________________________           FOUND THE BS MASS                   ____________________________"<<endl;
    cout<<"________________________________________________________"<<endl;
    cout<<"________________________________________________________"<<endl;
    cout<<"Calcuations Done!!!! Steps:	"<<mcounter<<"  , -----------------> Results: Ev "<<saveeign[2]<<" ,M "<<saveM[2]<<endl<<endl;


    return saveM[2];
}

void MesonBse::calculate_chi(ps_amplitude<3> &chi, vector<int>& amp_flag) {

    //uncomment when calcuating only certain structures and loop below.
//    vector<int> amp_flag={0,1,2,3};
    if(amp_flag.size()==0){
        for (int j = 0; j < parameter.n_projector; ++j) {
            amp_flag.push_back(j); }}

    cout<<"Chi calculation: ---------------------------> we are currently using these dressing funcs: ";
    printVector(amp_flag); cout<<endl;

    time_t start2, end2;
    time_t start, end; time(&start);
    VecCdoub chi_amp;

    //defining Si belonging to Y matrix
    SuperIndex<2> Si_Y_dirac({parameter.n_projector,parameter.n_basis});
    SuperIndex<2> Si_Y_discrete({ parameter.n_pp, parameter.n_pz});


    //Calculating the rotation matrix
    time(&start2);
    matCdoub Y_kernel = calculate_rotationmatrix_for_extrernal_p();
//    matCdoub Y_kernel = precalc_rotation_Y();
    time(&end2); give_time(start2,end2,"calcuated external Gmat");

//    chi.resize_amplitude(meson.getSize());
    chi_amp.resize(meson.getSize(),0.0);

#pragma omp parallel for default(shared)
    for (int i_pp = 0; i_pp < parameter.n_pp; ++i_pp) {
        for (int i_zp = 0; i_zp < parameter.n_pz; ++i_zp) {


            for (int i = 0; i < parameter.n_basis; ++i) {

                //summed up indices
                //Here we can switch the code if we only want to include certain structures at a given point:
//                for (int j = 0; j < parameter.n_projector; ++j) {
                for (int j : amp_flag) {

                        chi_amp[meson.getSi().is({i, i_pp, i_zp})] +=  Y_kernel[Si_Y_dirac.is({i,j})][Si_Y_discrete.is({i_pp,i_zp})]
                                                                       * meson.operator()({j,i_pp, i_zp});

                }


            }


        }
    }

    chi.setAmplitude(chi_amp);

    write_out_other_amplitude(folder+"wavefunction_chi.txt",chi);

    time(&end); give_time(start,end,"calcuated the wavefunction Chi.");




}


//interpolation
bool MesonBse::interpolation_check() {
    bool needed =false;

    if(parameter.n_ll != parameter.n_pp ){
        needed =true;
    }

    return needed;
}
void MesonBse::interpolation_start() {
//This function is called when the bse is created to check if a interpolation will be nessercary, depending on the grid point choice.
// First the functions checks this with interpolation_check();
// Then sets up the value for the spline interpolation for the x-values. f(x) on grid x to-> f(y)

    interpolation_nessercary = interpolation_check();
//        interpolation_nessercary = true;

    std::array<int,2> variable_depends_on;

    if(which_solver == "vector" ||  which_solver == "vector_fullkernel"){
        variable_depends_on=amplitude::depends_on_sphericals; }
//        variable_depends_on=amplitude::depends_on_sphericals_inside; } //???????????? Should be outside right!!? Because I want to interpolate the inside, so outside is my given.
    else if( which_solver=="vector_cheb" ){
         variable_depends_on=amplitude::depends_on_sphericals_cheb;}


    //If the interpolation is nesercary the start values for x are set in this following loop.
    if(interpolation_nessercary){

        cout<<"We need to interpolate"<<endl;


//            Interpol = new SplineInterpolator< 2, 4, double, Cdoub>;


        //Filling the matrix of x-values (aka gridpoints) for interpolation later.
        //These are the points form the outside, since the inside is interpolated and the outside calculated.
        //The outside points are the pp and zp the first two entries of AllGrids.
        //!!!!!!!!!!!!!!!!!!!!!important HERE I EXPLICITLY ASUMED THE LOG GRID DISTRIBUTION-----------
        for(int i=0; i<amplitude::n_sphericals; i++){
//            cout<<"Interpolation!         --- This is assuming that we have a fixed Q^2 value!!"<<endl; //cin.get();

            VecDoub v;
            for(int j=0; j<ps.AllGrid[variable_depends_on[i]]->getNumber_of_grid_points(); j++)
            {

                double dummy= ps.AllGrid[i]->getGrid()[j];
                if(i == 0 ){dummy=log(dummy);} ///?????????????????? uncomment?
                v.push_back( dummy );
            }
            xvalues_interpol.push_back(v);
        }



        if(which_solver == "vector_cheb" )
        {

            cout<<"STOP!         --- Not implemented jet!!"<<endl; cin.get();

        }

    }//else - if no interpolation nessercary nothing happens.

}
//void MesonBse::interpolation_on_outside_grid(ps_amplitude& new_meson){
//
//}
/*
void MesonBse::interpolation_update(const VecCdoub &amplitude,  VecCdoub& local_amplitude) {

    Interpol.Update( xvalues_interpol, amplitude);

    array<Cdoub, 4> result_y;
    SuperIndex<3> Si_I({parameter.n_projector,parameter.n_ll, parameter.n_lz});
//        SuperIndex<3> Si_I({parameter.n_projector,parameter.n_pp, parameter.n_pz}); ///??????????
    for (int i = 0; i < local_amplitude.size() ; ++i) {
        auto indices = Si_I.are(i);
        array<double,2> new_x_values ={log(ps.AllGrid[2]->getGrid()[indices[1]]),ps.AllGrid[3]->getGrid()[indices[2]]};

        //performing the interpolation only one time for all amplitudes t[i]. E,F,G,H.
//            if(indices[0]== 0){
        Interpol.getValue(result_y,new_x_values );
//            }


        local_amplitude[i] = result_y[indices[0]];


    }

}
*/


// --- Write and Read ----  :
//todo: Change write file, change read accordingly!
//todo: delete this routine? why is it needed, confusing to have so many write routines.
void MesonBse::write_out_amplitude(string filename) {

    ofstream write(filename);
    assert( write.is_open());

    std::array<int,2> variable_depends_on;
    if(which_solver=="vector_cheb" || which_solver=="matrix_cheb"){variable_depends_on  = amplitude::depends_on_sphericals_cheb;}
    else{variable_depends_on =  amplitude::depends_on_sphericals;}

    write<<"# safed as: p2   ti (real/imag) i="<<parameter.n_basis<<endl;
    write<<"# #gridpoints: rad= "<<ps.AllGrid[variable_depends_on[0]]->getNumber_of_grid_points()
         <<", ang= "<< ps.AllGrid[variable_depends_on[1]]->getNumber_of_grid_points() <<endl;
    write<<"## mass²="<<tab<< meson.getPsquared()<<endl;

    ArrayScalarProducts scalars;

    for (int i = 0; i < ps.AllGrid[variable_depends_on[1]]->getNumber_of_grid_points(); ++i) {


        for (int j = 0; j < ps.pp.getNumber_of_grid_points(); ++j) {

            get_all_scalar_prod<1,2>({j,i},quark_p1_norm::depends_on_sphericals,{34},
                                     ps, scalars);

            write<<scientific<<setprecision(8)<<real(ps.pp.getGrid()[j])<<tab<<imag(ps.pp.getGrid()[j])
                 << tab ;

            for (int k = 0; k < parameter.n_basis; ++k) {

                Cdoub value= meson.operator()({k,j,i});
                write <<scientific<<setprecision(10)<< real( value ) << tab << imag (value) <<tab;

            }

            write<<scientific<<setprecision(5)<< real(meson.getPsquared()) << tab;
            write<<scientific<<setprecision(7)<< ps.AllGrid[variable_depends_on[1]]->getGrid()[i] << tab << endl;
//                 real(scalars[34]) <<tab << imag(scalars[34])<< endl;

//            write<<endl;
//                write<<ps.zp.getGrid()[i]<<endl;

        }

        write<<endl<<endl;
    }

    write<<endl<<endl<<endl;
//        write<<"#Z2"<<tab<<"Zm"<<tab<<"IRcutsq"<<tab<<"UVcutsq"<<tab<<"m"<<tab<<"mass"<<tab<<"renormp"<<endl;
//        write <<  Z2<<tab << Zm <<tab << IRcutsq <<tab << UVcutsq <<tab  << nrad <<tab << mass <<tab << renorm_p<< tab << nangl <<endl;
    write.close();

}
void MesonBse::write_out_amplitude(string filename, bool cheb_project) {

    ofstream write(filename);
    assert( write.is_open());
    bool cheb_use= false;

    std::array<int,2> variable_depends_on;
    if(which_solver=="vector_cheb" || which_solver=="matrix_cheb"){variable_depends_on  = amplitude::depends_on_sphericals_cheb; cheb_use=true;}
    else{variable_depends_on =  amplitude::depends_on_sphericals;}

    write<<"# safed as: p2   ti (real/imag) i="<<parameter.n_basis<<endl;
    write<<"# #gridpoints: rad= "<<ps.AllGrid[variable_depends_on[0]]->getNumber_of_grid_points()
         <<", ang= "<< ps.AllGrid[variable_depends_on[1]]->getNumber_of_grid_points() <<endl;
    write<<"## mass²="<<tab<< meson.getPsquared()<<endl;

    ArrayScalarProducts scalars;

    for (int i = 0; i < ps.AllGrid[variable_depends_on[1]]->getNumber_of_grid_points(); ++i) {


        for (int j = 0; j < ps.AllGrid[variable_depends_on[0]]->getNumber_of_grid_points(); ++j) {

            get_all_scalar_prod<1,2>({j,i},quark_p1_norm::depends_on_sphericals,{34},
                                     ps, scalars);

            write<<scientific<<setprecision(8)<<real(ps.pp.getGrid()[j])<<tab<<imag(ps.pp.getGrid()[j])
                 << tab ;

            for (int k = 0; k < parameter.n_basis; ++k) {


                Cdoub value= 0.0;
                //In case of the Cheby's we are proecting out the angular dependence.
                if(cheb_use && cheb_project){
                    cheb Cheby(parameter.n_cheb);

                    for (int alpha = 0; alpha < parameter.n_cheb; ++alpha) {
//                        value +=  meson.operator()({k,j,alpha})* Cheby.get(alpha,ps.AllGrid[1]->getGrid()[i]) * pow(cheby_factor, -alpha); // *sqrt(2.0);
                        value +=  meson.operator()({k,j,alpha})* Cheby.get(alpha,ps.AllGrid[5]->getGrid()[i]) * pow(cheby_factor, -alpha); // *sqrt(2.0);

                    }
                }else{value =  meson.operator()({k,j,i});}


//                Cdoub value= meson.operator()({k,j,i});
                write <<scientific<<setprecision(10)<< real( value ) << tab << imag (value) <<tab;

            }

            write<<scientific<<setprecision(5)<< real(meson.getPsquared()) << tab;
            write<<scientific<<setprecision(7)<< ps.AllGrid[variable_depends_on[1]]->getGrid()[i] << tab << endl;
//                 real(scalars[34]) <<tab << imag(scalars[34])<< endl;

//            write<<endl;
//                write<<ps.zp.getGrid()[i]<<endl;

        }

        write<<endl<<endl;
    }

    write<<endl<<endl<<endl;
//        write<<"#Z2"<<tab<<"Zm"<<tab<<"IRcutsq"<<tab<<"UVcutsq"<<tab<<"m"<<tab<<"mass"<<tab<<"renormp"<<endl;
//        write <<  Z2<<tab << Zm <<tab << IRcutsq <<tab << UVcutsq <<tab  << nrad <<tab << mass <<tab << renorm_p<< tab << nangl <<endl;
    write.close();

}
Cdoub MesonBse::read_amplitude(string filename) {


    meson.read_amplitude(filename,1, 2);
    ps.setPsquared(meson.getPsquared());
    return meson.getPsquared();


   /* //Note: The ReadInn and write routine are tailored to each other, so watch out when changing one!

//    ps.setMass(Mass); /// updates the mass in phase space.cpp
//    meson.setMass(Mass);
    cout<<filename<<endl;

//    double Rgrid,Igrid;
//    double R1, R2, R3, R4, R5, R6, R7, R8;
//    double I1, I2, I3, I4, I5, I6, I7, I8;
    VecCdoub safe;

    ifstream read;
    read.open(filename, ios::in);



    if(read.is_open())
    {
        string daten;
        while(getline(read,daten))
        {

            VecDoub dummy; string s = ""; int pos;
            //Here reading in the mass from the output file.
            if(daten[0] == '#') {
                if(daten[1] == '#')
                {
                    pos= daten.find("\t");
                    for (int j = pos; j < daten.size(); ++j) {
                        s +=daten[j]; }
                    ps.setMass(stod(s)); /// updates the mass in phase space.cpp
                    meson.setPsquared(stod(s));
                }else{continue;}
            }

            if(daten=="") {continue;}

        //Reading in the amplitude: here I am subtituting 2 because I adddes Psquared and the z at the end of the file.
            for (int j = 0; j <daten.size() -2 ; ++j) {
                s += daten[j];
                if(daten[j] == '\t')
                {
                    dummy.push_back(stod(s));
                    s = "";
                }
//                if( c <= 'a'  ){}//Check if all of the read in data are numbers.
            }
//            cout<<dummy.size()<<endl;

            for (int k = 1; k < (dummy.size()/2) ; ++k) {
                safe.push_back(dummy[2*k]+i_*dummy[(2*k+1)]);
            }

            if(daten=="") {continue;}
            if(daten[0] == '#') {continue;}


//            if(parameter.n_basis == 4){
//                stringstream(daten) >> Rgrid >> Igrid >> R1 >> I1 >> R2 >> I2 >> R3 >> I3 >> R4 >> I4;
//            }else if(parameter.n_basis == 8){
//                stringstream(daten) >> Rgrid >> Igrid >> R1 >> I1 >> R2 >> I2 >> R3 >> I3 >> R4 >> I4
//                                    >> R5 >> I5 >> R6 >> I6 >> R7 >> I7 >> R8 >> I8 ;
//            }else{assert(false);}



//                safe.push_back(R1 +i_* I1);
//                safe.push_back(R2 +i_* I2);
//                safe.push_back(R3 +i_* I3);
//                safe.push_back(R4 +i_* I4);
//
//            if(parameter.n_basis > 4){
//                safe.push_back(R5 +i_* I5);
//                safe.push_back(R6 +i_* I6);
//                safe.push_back(R7 +i_* I7);
//                safe.push_back(R8 +i_* I8);
//            }else if (parameter.n_basis !=4 || parameter.n_basis != 8){assert(false);}



        }
    }else{cout<<"MesonBSE::readIn: No data file found!"<<endl; assert( false);}
    read.close();
    cout<<meson.getMass()<<endl;
//    cout<<safe.size()<<tab<<meson.getSize()<<endl;

    meson.setAmplitude(safe);


    cout<<"A Meson has been read inn:"<<filename<<"  with: n_total="<<safe.size()<<"  and m="<<meson.getPhaseSpacePsquared()<<endl;*/



}
void MesonBse::write_out_other_amplitude(string filename, ps_amplitude<3>& chi) {

    ofstream write(filename);
    assert( write.is_open());

    std::array<int,2> variable_depends_on;
    if(which_solver=="vector_cheb" || which_solver=="matrix_cheb"){variable_depends_on  = amplitude::depends_on_sphericals_cheb;}
    else{variable_depends_on =  amplitude::depends_on_sphericals;}

    write<<"# safed as: p2   ti (real/imag) i="<<parameter.n_basis<<endl;
    write<<"## mass="<<tab<< meson.getPsquared()<<endl;

    for (int i = 0; i < ps.AllGrid[variable_depends_on[1]]->getNumber_of_grid_points(); ++i) {


        for (int j = 0; j < ps.pp.getNumber_of_grid_points(); ++j) {


            write<<scientific<<setprecision(7)<<real(ps.ll.getGrid()[j])<<tab<<imag(ps.ll.getGrid()[j])
                 << tab ;

            for (int k = 0; k < parameter.n_basis; ++k) {

                Cdoub value= chi.operator()({k,j,i});
                write <<scientific<<setprecision(10)<< real( value ) << tab << imag (value) <<tab;

            }

            write<<scientific<<setprecision(5)<< real(ps.getPsquared()) << tab;
            write<<scientific<<setprecision(7)<< ps.AllGrid[variable_depends_on[1]]->getGrid()[i] << endl;

//            write<<endl;
//                write<<ps.zp.getGrid()[i]<<endl;

        }

        write<<endl<<endl;
    }

    write<<endl<<endl<<endl;
//        write<<"#Z2"<<tab<<"Zm"<<tab<<"IRcutsq"<<tab<<"UVcutsq"<<tab<<"m"<<tab<<"mass"<<tab<<"renormp"<<endl;
//        write <<  Z2<<tab << Zm <<tab << IRcutsq <<tab << UVcutsq <<tab  << nrad <<tab << mass <<tab << renorm_p<< tab << nangl <<endl;
    write.close();

}
void MesonBse::write_out_amplitude(string filename, const VecCdoub &amplitude, array<int,2> variable_depends_on ) {

    SuperIndex<3> Si= SuperIndex<3>({parameter.n_basis, ps.AllGrid[variable_depends_on[0]]->getNumber_of_grid_points()
                                            ,ps.AllGrid[variable_depends_on[1]]->getNumber_of_grid_points()});
    ofstream write(filename);
    assert( write.is_open());

    write<<"# safed as: p2   ti (real/imag) i="<<parameter.n_basis<<endl;
    write<<"## mass="<<tab<< meson.getPsquared()<<endl;

    assert(Si.getNsuper_index() == amplitude.size());

    for (int i = 0; i < ps.AllGrid[variable_depends_on[1]]->getNumber_of_grid_points(); ++i) {


        for (int j = 0; j < ps.AllGrid[variable_depends_on[0]]->getNumber_of_grid_points(); ++j) {


            write<<scientific<<setprecision(7)<<real(ps.AllGrid[variable_depends_on[0]]->getGrid()[j])
                 <<tab<<imag(ps.AllGrid[variable_depends_on[0]]->getGrid()[j]) << tab ;

            for (int k = 0; k < parameter.n_basis; ++k) {

                Cdoub value= amplitude[Si.is({k,j,i})];
                write <<scientific<<setprecision(10)<< real( value ) << tab << imag (value) <<tab;

            }

            write<<scientific<<setprecision(5)<< real(ps.getPsquared()) << tab;
            write<<scientific<<setprecision(7)<< ps.AllGrid[variable_depends_on[1]]->getGrid()[i] << endl;

//            write<<endl;
//                write<<ps.zp.getGrid()[i]<<endl;

        }

        write<<endl<<endl;
    }

    write<<endl<<endl<<endl;
//        write<<"#Z2"<<tab<<"Zm"<<tab<<"IRcutsq"<<tab<<"UVcutsq"<<tab<<"m"<<tab<<"mass"<<tab<<"renormp"<<endl;
//        write <<  Z2<<tab << Zm <<tab << IRcutsq <<tab << UVcutsq <<tab  << nrad <<tab << mass <<tab << renorm_p<< tab << nangl <<endl;
    write.close();

}
void MesonBse::write_out_kernel(string filename, int i){

    filename = filename + "_" + to_string(i)+ ".txt";
    ofstream write(filename);
    assert( write.is_open());

    SuperIndex<3> Si_I({parameter.n_projector,parameter.n_pp, parameter.n_pz});
    SuperIndex<3> Si_J({parameter.n_basis, parameter.n_ll, parameter.n_lz});

    std::array<int,2> variable_depends_on;
    if(which_solver=="vector_cheb" || which_solver=="matrix_cheb"){variable_depends_on  = amplitude::depends_on_sphericals_cheb;}
    else{variable_depends_on =  amplitude::depends_on_sphericals;}

    //#ll(r/i)   P4     zl      kernel(r/i)i


    for (int pp = 0; pp < ps.pp.getNumber_of_grid_points() ; ++pp) {


    for (int zp = 0; zp < ps.AllGrid[variable_depends_on[1]]->getNumber_of_grid_points(); ++zp) {



        for (int ll = 0; ll < ps.ll.getNumber_of_grid_points(); ++ll) {


            for (int zl = 0; zl < ps.AllGrid[variable_depends_on[1]]->getNumber_of_grid_points(); ++zl) {


            write<<scientific<<setprecision(7)<<real(ps.ll.getGrid()[ll])<<tab<<imag(ps.ll.getGrid()[ll]) << tab ;
            write<<scientific<<setprecision(5)<< real(ps.getPsquared()) << tab;
            write<<scientific<<setprecision(7)<< ps.AllGrid[variable_depends_on[1]]->getGrid()[zl] << tab;

            for (int k = 0; k < parameter.n_basis; ++k) {

                Cdoub value=kernel[Si_I.is({i,pp,zp})][Si_J.is({k,ll,zl})];
                write <<scientific<<setprecision(10)<< real( value ) << tab << imag (value) <<tab;

            }

            write<<endl;


            }

            write<<endl<<endl;
        }

        write<<endl<<endl;
    }
        write<<endl<<endl;
    }

    write<<endl<<endl<<endl;
//        write<<"#Z2"<<tab<<"Zm"<<tab<<"IRcutsq"<<tab<<"UVcutsq"<<tab<<"m"<<tab<<"mass"<<tab<<"renormp"<<endl;
//        write <<  Z2<<tab << Zm <<tab << IRcutsq <<tab << UVcutsq <<tab  << nrad <<tab << mass <<tab << renorm_p<< tab << nangl <<endl;
    write.close();

}


//Choices: Which basis is used? How are we calculating? Which solver are we using?
void MesonBse::set_kernel_functions() {

    //Choosing the corresponding functions for vector, pseudoscalar
    if(which_meson == "vector"){
        normalisation = normalisation_ve;
        calc_fpi = calc_fpi_ve;
        kernelL = kernelL_ve;
        kernelall= kernelall_ve;
        kernelY= kernelY_ve;

//        kernelL = kernelL_qphv_trans;
//        kernelY= kernelY_qphv_trans;

    }else if (which_meson == "ps"){
        normalisation = normalisation_ps;
        calc_fpi = calc_fpi_ps;
        kernelL = kernelL_ps;
        kernelall= kernelall_ps;
        kernelY= kernelY_ps;
        kernelRotationY = kernel_rotationY_ps;

    }else if (which_meson == "qphv" && parameter.n_basis == 12){
        normalisation = normalisation_qphv;
//        calc_fpi = calc_fpi_qphv;
        kernelL = kernelL_qphv;
        kernelall= kernelall_qphv;
        kernelY= kernelY_qphv;
        kernelInhomo =kernelInhomo_qphv;

    }else if (which_meson == "qphv" && parameter.n_basis == 4){
        normalisation = normalisation_qphv_longi;
//        calc_fpi = calc_fpi_qphv_longi;

        kernelall= kernelall_qphv_longi;

        kernelL = kernelL_qphv_longi;
        kernelY= kernelY_qphv_longi;
        kernelInhomo =kernelInhomo_qphv_longi;

    }else if (which_meson == "qphv" && parameter.n_basis == 8){
//        normalisation = normalisation_qphv_trans;
//        calc_fpi = calc_fpi_qphv_trans;
//        kernelL = kernelL_qphv_trans;
//        kernelall= kernelall_qphv_trans;
//        kernelY= kernelY_qphv_trans;
//        cout<<"------------>  We entered the expected ste_kernel!!"<<endl;

        kernelInhomo =kernelInhomo_qphv_trans;

        kernelL = kernelL_ve;
        kernelall= kernelall_ve;
        kernelY= kernelY_ve;

    }else if (which_meson == "qphv"){
        normalisation = normalisation_qphv;
//        calc_fpi = calc_fpi_qphv;
        kernelL = kernelL_qphv;
        kernelall= kernelall_qphv;
        kernelY= kernelY_qphv;
        kernelInhomo =kernelInhomo_qphv;

    }else if(which_meson == "scalar"){


//        kernelall= kernelall_qphv_longi;
//        kernelL = kernelL_qphv_longi;
//        kernelY= kernelY_qphv_longi;

//        normalisation = normalisation_ps;

        normalisation = normalisation_scalar;
        kernelL = kernelL_scalar;
        kernelall= kernelall_scalar;
        kernelY= kernelY_scalar;
        kernelRotationY =  kernel_rotationY_scalar;


    }else if(which_meson == "axial"){

        normalisation = normalisation_axial;
        kernelL = kernelL_axial;
        kernelall= kernelall_axial;
        kernelY= kernelY_axial;
        kernelRotationY =  kernel_rotationY_axial;

    }
    else{
        assert(false);
        //This case is not implemented!!
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////

//Functions that got added due to the inhomogenous BSE
VecCdoub MesonBse::precalc_inhomogenous_part() {

    time_t start, end;
    time(&start);
    VecCdoub result;

    if(which_solver == "vector" || which_solver == "vector_fullkernel" || which_solver == "matrix" || which_solver == "full_kernel_KG"){

        SuperIndex<3> Si_inhomo({parameter.n_basis, parameter.n_pp, parameter.n_pz});

        VecCdoub vec(Si_inhomo.getNsuper_index(),0.0);

        //Defining objects used in the loop - rember privatizing for paralleisation.
        ArrayScalarProducts scalars;

        VecCdoub kernel(parameter.n_projector,0.0);

        get_all_scalar_prod<1,0>({},{},{35}, ps, scalars);

        kernelInhomo(kernel, scalars);




//#pragma omp parallel for firstprivate(kernel, scalars)
        for (int pp = 0; pp < parameter.n_pp; ++pp) {

            for (int pz = 0; pz < parameter.n_pz; ++pz) {

//            get_all_scalar_prod<2,2>({pp,pz},inhomogenous::depends_on_sphericals,inhomogenous::depends_on_scalar_products, ps, scalars);
//            kernelInhomo_qphv(kernel, scalars);


                for (int j = 0; j < parameter.n_basis; ++j) {


                    vec[Si_inhomo.is({j,pp,pz})] = kernel[j];
//                    cout<<"inhomo: "<<kernel[j]<<tab<<scalars[35]<<tab<<scalars[0]<<tab<<scalars[27]<<endl;

                }
            }
        }
        result=vec;
    }else if(which_solver == "vector_cheb"){

        SuperIndex<3> Si_inhomo({parameter.n_basis, parameter.n_pp, parameter.n_cheb});

        VecCdoub vec(Si_inhomo.getNsuper_index(),0.0);

        //Defining objects used in the loop - rember privatizing for paralleisation.
        ArrayScalarProducts scalars;

        VecCdoub kernel(parameter.n_projector,0.0);

        kernelInhomo(kernel, scalars);

#pragma omp parallel for firstprivate(kernel, scalars)
        for (int pp = 0; pp < parameter.n_pp; ++pp) {

            for (int alpha = 0; alpha < parameter.n_cheb; ++alpha) {

//            get_all_scalar_prod<2,2>({pp,alpha},inhomogenous::depends_on_sphericals,inhomogenous::depends_on_scalar_products, ps, scalars);
//            kernelInhomo_qphv(kernel, scalars);


                for (int j = 0; j < parameter.n_basis; ++j) {


                    if(alpha == 0 ){
                        vec[Si_inhomo.is({j,pp,alpha})] = kernel[j] * sqrt(2.0); //*T_0(z)= 1;
                        // Here I should have in the Cheby Polynominal explicitly as *T_i(z) * inhomogneous_i( in Chebys ).
                    }else{
                        vec[Si_inhomo.is({j,pp,alpha})] = 0.0;

                    }

//                    cout<<"inhomo: "<<kernel[j]<<tab<<scalars[35]<<tab<<scalars[0]<<tab<<scalars[27]<<endl;

                }
            }
        }
        result =vec;
    }else{assert(false);}



    time(&end); give_time(start,end,"precalc inhomogenous part");


    return result;
//    return kernel;



}

void MesonBse::calc_for_different_Psq(double Ps_start, double Ps_end, int Ps_n){

    //first I need to set the Values for P² on which I want to calcuate the meson.
    Line Psquare_line(Ps_start,Ps_end, Ps_n,  "linear", "equal", true, false);
    Psquare = Psquare_line.getGrid();

    //I need to first remove the old file since the write out routine is appending.
    string file_path = folder+"Qphv.txt";
    if( remove( file_path.c_str()) != 0 )
    {  perror( "MesonBSE::calc_for_different_Psq::Error deleting file" ); cout<<file_path<<endl;}
    else
        puts( "MesonBSE::calc_for_different_Psq::File successfully deleted" );

    //Adding a frist Line to the qphv document to signal what is safed in which order.
    ofstream write;
    write.open(file_path, fstream::app);
    write<<"# Q²    Re(S_0) Im(S_0) Re(p²)  Im(p²)  Re/Im(Amp[1..12])"<<endl;


//    for (int p_square = 0; p_square < Psquare.getNumber_of_grid_points(); ++p_square) {
    for (int p_square = Psquare_line.getNumber_of_grid_points()-1; p_square >= 0; --p_square) {

//        cout<<"------------------------> Test here: "<<p_square<<tab<<Psquare.getGrid()[p_square]<<endl;
        solve_and_update_pion(Psquare[p_square]);
        meson.setIs_calculated(true);

        write_multiple_P(file_path, p_square);
//        write_out_amplitude(folder+"qphv_"+ to_string(p_square)+ ".txt");

        delet_multi_quarks();

    }
}

void MesonBse::calc_for_different_Psq(VecDoub Ps, string filename){

    //first I need to set the Values for P² on which I want to calcuate the meson.
    Psquare =  Ps;
    bool delete_quarks= true; if(Ps.size()==1){delete_quarks=false;}

    //I need to first remove the old file since the write out routine is appending.
    string file_path = folder+filename+".txt";
    if( remove( file_path.c_str()) != 0 )
    {  perror( "MesonBSE::calc_for_different_Psq::Error deleting file" ); cout<<file_path<<endl;}
    else
        puts( "MesonBSE::calc_for_different_Psq::File successfully deleted" );

    //Adding a frist Line to the qphv document to signal what is safed in which order.
    ofstream write; meson.setIs_calculated(false);
    write.open(file_path, fstream::app);
    write<<"## dims: "<<tab<<Ps.size()<<tab<<ps.pp.getNumber_of_grid_points() <<tab<<ps.chebs.getNumber_of_grid_points()<<endl;
    write<<"# Q²    Re(S_0) Im(S_0) Re(p²)  Im(p²)  Re/Im(Amp[1..12])"<<endl;


//    for (int p_square = 0; p_square < Psquasre.getNumber_of_grid_points(); ++p_square) {
    for (int p_square = Ps.size()-1; p_square >= 0; --p_square) {

//        cout<<"------------------------> Test here: "<<p_square<<tab<<Psquare.getGrid()[p_square]<<endl;
        solve_and_update_pion(Psquare[p_square]);
//        meson.setIs_calculated(true);

        write_multiple_P(file_path, p_square);
//        write_out_amplitude(folder+"qphv_"+ to_string(p_square)+ ".txt");

        if(delete_quarks){delet_multi_quarks();}

    }

    write.close();
}
//WRITE for multiple values of P^2. Important for vertex.
void MesonBse::write_multiple_P(string filename, int Psq_value){

    ofstream write;
//    assert( write.is_open());
    write.open(filename, fstream::app);

    array<int,2> variable_depends_on = amplitude::depends_on_sphericals;

    bool cheb_use=false;
    bool cheb_project=true;
//    bool cheb_project=false;
    if(which_solver=="vector_cheb" || which_solver=="matrix_cheb"){cheb_use=true; variable_depends_on = amplitude::depends_on_sphericals_cheb;}

//#ifdef K_WRITEOUT_ANGPROJC

    //angular variable/dependence:
    for (int i = 0; i < ps.AllGrid[variable_depends_on[1]]->getNumber_of_grid_points(); ++i) {

        //radial component
        for (int j = 0; j < ps.pp.getNumber_of_grid_points(); ++j) {


            //P², Re(z), Im(z), Re(p²), Im(p²).

            write<<scientific<<setprecision(7)<< Psquare[Psq_value] << tab                      //PP
//                 << real(Psquare[Psq_value]/4.0 + ps.ll.getGrid()[j]/1.0) << tab << imag(Psquare[Psq_value]/4.0 + ps.ll.getGrid()[j]/1.0)
              << ps.AllGrid[variable_depends_on[1]]->getGrid()[i]<< tab << 0.0                //angle
//              << ang_grid[i]<< tab << 0.0                //angle
//              << ps.zp.getGrid()[i]<< tab << 0.0                //angle
                 << tab << real(ps.pp.getGrid()[j])<<tab<<imag(ps.pp.getGrid()[j]) << tab ;  //pp

            for (int k = 0; k < parameter.n_basis; ++k) {

                Cdoub value= 0.0;
                //In case of the Cheby's we are proecting out the angular dependence.
                if(cheb_use && cheb_project){
                    cheb Cheby(parameter.n_cheb);

                    for (int alpha = 0; alpha < parameter.n_cheb; ++alpha) {
                        value +=  meson.operator()({k,j,alpha})* Cheby.get(alpha,ps.AllGrid[5]->getGrid()[i]) * pow(cheby_factor, alpha); // *sqrt(2.0);
//                        value +=  meson.operator()({k,j,alpha})* Cheby.get(alpha,ps.AllGrid[variable_depends_on[5]]->getGrid()[i]) * pow(cheby_factor, alpha); // *sqrt(2.0);
//                        value +=  meson.operator()({k,j,alpha})* Cheby.get(alpha,ps.AllGrid[1]->getGrid()[i]) * pow(cheby_factor, -alpha); // *sqrt(2.0);


//                        value +=  meson.operator()({k,j,alpha})* Cheby.get(alpha,ang_grid[i]) * pow(cheby_factor, alpha); // *sqrt(2.0);
//                        if(alpha == 0 ){value =value *sqrt(2.0);} //I Think the Cheby factor need to be included only for T_0
                    }
                }else{value =  meson.operator()({k,j,i});}




                write <<scientific<<setprecision(10)<< real( value ) << tab << imag (value) <<tab;

            }

            write<<endl;
//                write<<ps.zp.getGrid()[i]<<endl;

        }

        write<<endl<<endl;
//        write<<endl;
    }

    write<<endl<<endl;
//        write<<"#Z2"<<tab<<"Zm"<<tab<<"IRcutsq"<<tab<<"UVcutsq"<<tab<<"m"<<tab<<"mass"<<tab<<"renormp"<<endl;
//        write <<  Z2<<tab << Zm <<tab << IRcutsq <<tab << UVcutsq <<tab  << nrad <<tab << mass <<tab << renorm_p<< tab << nangl <<endl;
    write.close();

//#endif


//#ifdef K_WRITEOUT_NOPROJ
/*
    //angular variable/dependence:
    for (int i = 0; i < ps.AllGrid[variable_depends_on[1]]->getNumber_of_grid_points(); ++i) {

        //radial component
        for (int j = 0; j < ps.pp.getNumber_of_grid_points(); ++j) {


            write<<scientific<<setprecision(7)<< Psquare[Psq_value] << tab                      //PP          -1
              << ps.AllGrid[variable_depends_on[1]]->getGrid()[i]<< tab << 0.0                //angle or Cheb  -2,3
                 << tab << real(ps.pp.getGrid()[j])<<tab<<imag(ps.pp.getGrid()[j]) << tab ;  //pp              -3,4

            for (int k = 0; k < parameter.n_basis; ++k) {


                Cdoub value;


                //In case of the Cheby's we are proecting out the angular dependence.
                if(cheb_use && cheb_project){
                    cheb Cheby(parameter.n_cheb);

                    for (int alpha = 0; alpha < parameter.n_cheb; ++alpha) {
                        value +=  meson.operator()({k,j,alpha})* Cheby.get(alpha,ps.zp.getGrid()[i]) * pow(cheby_factor, alpha); // *sqrt(2.0);
//                        if(alpha == 0 ){value =value *sqrt(2.0);} //I Think the Cheby factor need to be included only for T_0
                    }
                }else{value =  meson.operator()({k,j,i});}


//                value =  meson.operator()({k,j,i});

                write <<scientific<<setprecision(10)<< real( value ) << tab << imag (value) <<tab;

            }

            write<<endl;
//                write<<ps.zp.getGrid()[i]<<endl;

        }

        write<<endl<<endl;
//        write<<endl;
    }

    write<<endl<<endl;
//        write<<"#Z2"<<tab<<"Zm"<<tab<<"IRcutsq"<<tab<<"UVcutsq"<<tab<<"m"<<tab<<"mass"<<tab<<"renormp"<<endl;
//        write <<  Z2<<tab << Zm <<tab << IRcutsq <<tab << UVcutsq <<tab  << nrad <<tab << mass <<tab << renorm_p<< tab << nangl <<endl;
    write.close();*/

//#endif

}



//More getter & setter:
void MesonBse::setSolver(const SolverWrapper<MesonBse> *solver) {
    MesonBse::solver = solver;
}
void MesonBse::setWhich_solver(const string &which_solver) {
    MesonBse::which_solver = which_solver;
}
const VecCdoub &MesonBse::getInhomogenous_part() const {
    return inhomogenous_part;
}
double MesonBse::getZ2_Z2() const {
    return Z2_Z2;
}
const Bse_ps &MesonBse::getPs() const {
    return ps;
}
const string &MesonBse::getWhich_solver() const {
    return which_solver;
}
const Cdoub &MesonBse::getCheby_factor() const {
    return cheby_factor;
}

void MesonBse::get_me_content_of_quarks(vector<array<Cdoub, 2>> &quark_o_1, vector<array<Cdoub, 2>> &quark_o_2) {

    quark_o_1 =  quark1->getContent();
    quark_o_2 =  quark2->getContent();
}

double MesonBse::get_decay_constant() {

    Cdoub fpi = decay_constant();
    return real(fpi);
}

void MesonBse::calculate_two_quarks(Cdoub Psquared) {

    meson.setPsquared(real(Psquared));
    ps.setPsquared(Psquared);
//        if(real(Psquared) < 0 && which_solver == "vector_cheb"){cheby_factor = i_;}else{cheby_factor= 1.0;}
    if(real(Psquared) < 0){cheby_factor = i_;}else{cheby_factor= 1.0;}
    cout<<endl<<"______________________________________________________________"<<endl;
    cout<<tab<<tab<<tab<<"Calcuation for P = (0,0,0,"<<sqrt(Psquared)<<") and Chebyfactor="<<cheby_factor<<endl;

    //Next we update all Mutliobjects, aka set the MultiO's
    // This is basically just pre calculating the quarks. Now that the mass is set
    // we know at which points we need to evaluate S(q²).
    update_multiobjects();
}

void MesonBse::setPhaseSpacePsquared(Cdoub &Psquared) {
    ps.setPsquared(Psquared);
}

Cdoub MesonBse::getPhaseSpacePsquared() const {
    return ps.getPsquared();
}

void MesonBse::setFolder(const string &folder) {
    MesonBse::folder = folder;
}

void MesonBse::setQuark_safer(const string &quark_safer) {
    MesonBse::quark_safer = quark_safer;
}


void MesonBse::free_memory() {

    //comment: could there more that I could free here????
    // Solver, interpolation? Think about memeber variables.

//    cout<<inhomogenous_part.capacity()<<" -> ";
    kernel =  matCdoub();
    inhomogenous_part =  VecCdoub();

    cout<<inhomogenous_part.capacity()<<kernel.capacity()<<" - Freed memory of BSE: inhomogenous G0, kernel K."<<endl;

}

//-------------New for the additional iteration of the bse------------------------------------
VecCdoub MesonBse::get_value_iteration(Cdoub pp, Cdoub zp) {

    //frist I have to replace the value for the wanted angel and momentum (pp,zp) in the phasespace and parameter variable
    parameter.n_pp=parameter.n_pz=1.0;

    VecCdoub pp_value; pp_value.push_back(pp);
    Line pp_line = Line(pp_value);
    ps.replace_ps_grid(pp_line, 0);

    //Should z be real or also imaginary? In principle is must be allowed to be imaginary, especially with P=(0,0,0,M), spacelike.
//    VecDoub zp_value; zp_value.push_back(real(zp));
    VecCdoub zp_value; zp_value.push_back(zp);
    Line zp_line = Line(zp_value);
    ps.replace_ps_grid(zp_line, 1);


    //then I can call the usual fill_kernel function
    if(which_solver == "vector" ){
        kernel = get_Matrix(); }
    else if(which_solver == "vector_fullkernel" ){
        kernel = get_Matrix_all_in_one_go();  cout<<"-----Using the direct full Kernel calculation----"<<endl; }
    else if( which_solver=="vector_cheb" ){
        kernel = get_Matrix_Cheb(); }

    //todo: Here I could create a function called calc_inhomo... in order to acess this outside the meson class, because I am doing this np times,
    // and as this is momenta independet I could pout it outside this loop.
    if(which_meson=="qphv"){inhomogenous_part=precalc_inhomogenous_part(); }


    //And finally calculate the integral kernel times amplitude K.G
    VecCdoub result(parameter.n_basis, 0.0);

    //note: here thing were messed up because of the parallelization, this still does not work. And is it even worth it here?
//#pragma omp parallel private(k)
    for ( int k = 0; k < parameter.n_basis; ++k) {
        for ( int i = 0; i < meson.getSize(); ++i) {
//        #pragma omp critical
//        {
            //Note: kernel does already include the prefactor of Z2 infront of the integral.
            result[k] +=  kernel[k][i]*meson.getAmp()[i];
//        }

        }

        if(which_meson=="qphv"){result[k] += inhomogenous_part[k]*sqrt(Z2_Z2); }
    }


    return result;


}

void MesonBse::project_back_out_the_angle_inside() {

    if(which_solver=="vector_cheb"){

        cheb Cheby(parameter.n_cheb);
        SuperIndex<3> Si_new = SuperIndex<3>({parameter.n_basis, parameter.n_ll, parameter.n_lz});
//        SuperIndex<3> Si_new = SuperIndex<3>({parameter.n_basis, parameter.n_ll, parameter.n_cheb});
        VecCdoub result; result.resize(Si_new.getNsuper_index());

//        cout<<"Here - :"<<parameter.n_basis<<endl;

        for (int i = 0; i < Si_new.getNsuper_index(); ++i) {

            Cdoub value=0.0;
            auto indices= Si_new.are(i);
            for (int alpha = 0; alpha < parameter.n_cheb; ++alpha) {
//                        value +=  meson.operator()({k,j,alpha})* Cheby.get(alpha,ps.AllGrid[1]->getGrid()[i]) * pow(cheby_factor, -alpha); // *sqrt(2.0);
                //important: Currently I am projecting out onto z_l in order to use this for the integration.
                value +=  meson.operator()({indices[0],indices[1],alpha})* Cheby.get(alpha,ps.AllGrid[3]->getGrid()[indices[2]]) * pow(cheby_factor, -alpha); // *sqrt(2.0);
//                value +=  meson.operator()({indices[0],indices[1],alpha})* Cheby.get(alpha,ps.AllGrid[5]->getGrid()[indices[2]]) * pow(cheby_factor, -alpha); // *sqrt(2.0);

            }
            result[i]=value;
        }

        meson.setSi(Si_new);
        meson.setAmplitude(result);

    }else{cout<<"Nothing to project out, the amplitude already depends on the angle."<<endl;}



}

void MesonBse::init_via_file(string bse_amplitude_folder, Cdoub Psquared, int which_vertex_quark) {
//Goal of this function is the init the bse form precalcuated data in a file, such that the futher interpolation/iteration can be used.

    //settign the value for P² und Cheby_factor
    meson.setPsquared(real(Psquared));
    ps.setPsquared(Psquared);
    if(real(Psquared) < 0){cheby_factor = i_;}else{cheby_factor= 1.0;}

    //init the multiquarks from file too.
    //Or if non exsisting they need to be calcualted - change here - hard coded.
    folder = bse_amplitude_folder;
    what_to_calc_flag[0]=what_to_calc_flag[1]=false;
    //Or I guess with a new Mesonbse, I wont need this the delete here afterall.
//    if(!already_deleted_quarks){
//        delet_multi_quarks(); }
    quark_safer="qphv"+to_string(which_vertex_quark)+"_";
    update_multiobjects(); already_deleted_quarks=false; //as there is a new in the update multiobjects


    //then read Inn meson from the file.
    read_amplitude(bse_amplitude_folder+"Qphv"+to_string(which_vertex_quark)+".txt");
}

int MesonBse::getNBasis() const{
    int result = parameter.n_basis;
    return (result);
}

void MesonBse::setWhat_to_calc_flag(const array<bool, 5> &what_to_calc_flag) {
    MesonBse::what_to_calc_flag = what_to_calc_flag;
}

void MesonBse::reset_inhomogenous() {
    inhomogenous_part =  VecCdoub();
}

void MesonBse::setAlready_deleted_quarks(bool already_deleted_quarks) {
    MesonBse::already_deleted_quarks = already_deleted_quarks;
}




