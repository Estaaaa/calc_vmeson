//
// Created by esther on 04.07.17.
//


// toddo: das externe grid soll vom Quark mitgebracht werden
#ifndef CALC_BSE_QUARKDSE_H
#define CALC_BSE_QUARKDSE_H

//#include <vector>

using namespace std;

#include <Parameter.h>
#include "typedefs.h"
#include "Quark.h"
#include "Gluon.h"
#include "Grids.h"
//#include "const_dse.h"

//using namespace const_dse;

class QuarkDse{

public:

    //Todo: Old Constructor with const_dse file. Delete at some point
//    QuarkDse();

    QuarkDse(string parameter_file);

    QuarkDse(const DseParameter parameter);

    void Fill_QuarkDse(string quark_file);

//    QuarkDse(RQuark &quark);

    virtual ~QuarkDse();

    // todo: Sollten die private sein?
    double getRhsA(const double pp) const;
    double getRhsB(const double pp) const;
    Cdoub getRhsB(const Cdoub pp)   const;
    Cdoub getRhsA(const Cdoub pp)   const;
    array<double,2> getRhsQuark(const double pp) const;
    array<Cdoub,2> getRhsQuark(const Cdoub pp)   const;
    array<Cdoub,2> getValue(array< Cdoub, 1> pp) const;

    void calc_quark();

    const Line &getIntegration_grid_rad() const;

    const Quark<double> &getQuark() const;
    void setQuark(const RQuark &quark);

    const GluonWrapper &getGluon() const;

private:

    //Notice: The order is important here!
    DseParameter parameter;
public:
    const DseParameter &getParameter() const;

private:

    Line integration_grid_rad;
    Line integration_grid_ang;


    GluonWrapper gluon;
    RQuark quark;


    double SelfEnergyA(const double pp) const;
    double SelfEnergyB(const double pp) const;
    Cdoub SelfEnergyA(const Cdoub pp)   const;
    Cdoub SelfEnergyB(const Cdoub pp)   const;


    double calc_tolerance(const VecDoub &Aold, const VecDoub &Bold, const VecDoub &Anew, const VecDoub &Bnew);



};


#endif //CALC_BSE_QUARKDSE_H


