//
// Created by esther on 04.07.17.
//

#include <Vector_resizer.h>
#include "QuarkDse.h"


void QuarkDse::calc_quark() {

    double current_tolerance=100.0;
    int number_of_iterations=0;

    VecDoub A_current(parameter.n_rad);
    VecDoub B_current(parameter.n_rad);

    double A_at_renormalisation_pt=0.0;
    double B_at_renormalisation_pt=0.0;

    cout<<"Calculating the Quark DSE."<<endl;
    quark.mass = parameter.mass_at_renorm_pt;


    while (current_tolerance >= parameter.solver_tolerance)
    {
        number_of_iterations++;

        for (int j=0; j<parameter.n_rad; j++)
        {

            A_current[j] = getRhsA(quark.getGrid()[j]);

            B_current[j] = getRhsB(quark.getGrid()[j]);

        }

        //Calculates the biggest difference in the whole Vector of B/A and save it as our new tolerance;
        current_tolerance = calc_tolerance(quark.getA(), quark.getB(), A_current, B_current);
//        cout<<current_tolerance<<endl;


        //calculating the renormlaization constants
        A_at_renormalisation_pt =  quark.Z2 * SelfEnergyA(parameter.renomalisation_point*parameter.renomalisation_point);
        B_at_renormalisation_pt =  quark.Z2 * SelfEnergyB(parameter.renomalisation_point*parameter.renomalisation_point);

        quark.Z2= 1.0/(1.0+A_at_renormalisation_pt);

        if(parameter.mass_at_renorm_pt < 1e-5 )
        {
            quark.Zm = 0.0;
        }
        else{
            quark.Zm= parameter.mass_at_renorm_pt/quark.Z2 - B_at_renormalisation_pt;
        }


        quark.setA(A_current);
        quark.setB(B_current);

    }


    ofstream write("interaction_file.txt");
    for (int j=0; j<parameter.n_rad; j++)
    {

        write << quark.getGrid()[j]<<tab<< gluon.getValue(quark.getGrid()[j])<<endl;

    }
    write.close();



    cout<<"Calculated the real quark prop, HERE:.....Z2:"<<quark.Z2<<"  Zm:"<<quark.Zm<<"   mass:"<<parameter.mass_at_renorm_pt<<endl;
    cout<<"---------ENDE:real quark prop------------"<<"Number of iterations: "<<number_of_iterations<<"  ,Stuetzstellen:  m:"<<parameter.n_rad<<"  ,n :"<<parameter.n_angle<<endl<<endl;

}

//The real functions of Self Energies etc.
double QuarkDse::getRhsA(const double pp) const{

    return (quark.Z2 + quark.Z2 * quark.Z2 * SelfEnergyA(pp));
}

double QuarkDse::getRhsB(const double pp) const{

    return  (quark.Zm * quark.Z2+ quark.Z2 * quark.Z2 * SelfEnergyB(pp));
}

array<double,2> QuarkDse::getRhsQuark(const double pp) const{

    return {getRhsA(pp), getRhsB(pp)};
}

double QuarkDse::SelfEnergyA(const double pp) const{

    double result=0.0;

    double prefactors_color_integration=(1.0)/(8.0*M_PI*M_PI*M_PI)*((4.0)/(3.0)) ;


    //Radial Integration:
    for (int i_rad = 0; i_rad < quark.nrad; ++i_rad) {


        double sum1=0.0;
        double qq =  integration_grid_rad.getGrid()[i_rad];

        for (int i_ang = 0; i_ang < quark.nangl ; ++i_ang) {

            double ang = integration_grid_ang.getGrid()[i_ang];
            double pdotq = sqrt(pp*qq)*ang;
            double kk = pp+qq-2.0*pdotq;  //Gluon momentum


            sum1 += (sqrt(1.0-ang*ang))* gluon.getValue(kk) * ((1.0/(kk))*(-2.0*qq+3.0*(1.0+(qq/pp))*pdotq-4.0*qq*ang*ang)) *
                    integration_grid_ang.getWeights_integration()[i_ang];
        }

        result +=  prefactors_color_integration *
                   ((quark.getA()[i_rad])/(qq*quark.getA()[i_rad]*quark.getA()[i_rad]+quark.getB()[i_rad]*quark.getB()[i_rad]))* qq
                   * integration_grid_rad.getWeights_integration()[i_rad] * sum1;
    }

    return result;
}


double QuarkDse::SelfEnergyB(const double pp) const {

    double result =0.0;

    double prefactors_color_integration=(1.0)/(8.0*M_PI*M_PI*M_PI)*((4.0)/(3.0)) ;


    //Radial Integration:
    for (int i_rad = 0; i_rad < quark.nrad; ++i_rad) {


        double sum1=0.0;
        double qq =  integration_grid_rad.getGrid()[i_rad];

        for (int i_ang = 0; i_ang < quark.nangl ; ++i_ang) {

            double ang = integration_grid_ang.getGrid()[i_ang];
            double kk = pp+qq-2.0*sqrt(pp*qq)*ang;  //Gluon momentum


            sum1 += (sqrt(1.0-ang*ang))* gluon.getValue(kk) *
                    integration_grid_ang.getWeights_integration()[i_ang];
        }

        result +=  prefactors_color_integration *
                ((3.0 * quark.getB()[i_rad])/(qq*quark.getA()[i_rad]*quark.getA()[i_rad]+quark.getB()[i_rad]*quark.getB()[i_rad]))* qq
                   * integration_grid_rad.getWeights_integration()[i_rad] * sum1;
    }

    return result;
}

//now the complex versions of the Self Energies etc.
Cdoub QuarkDse::getRhsA(const Cdoub pp) const{

    return (quark.Z2 + quark.Z2 * quark.Z2 * SelfEnergyA(pp));
}

Cdoub QuarkDse::getRhsB(const Cdoub pp) const{

    return  (quark.Zm * quark.Z2+ quark.Z2 * quark.Z2 * SelfEnergyB(pp));
}

array<Cdoub,2> QuarkDse::getRhsQuark(const Cdoub pp) const{
    return {getRhsA(pp), getRhsB(pp)};
}

Cdoub QuarkDse::SelfEnergyA(const Cdoub pp) const{

    Cdoub result=0.0;

    double prefactors_color_integration=(1.0)/(8.0*M_PI*M_PI*M_PI)*((4.0)/(3.0)) ;


    //Radial Integration:
    for (int i_rad = 0; i_rad < quark.nrad; ++i_rad) {


        Cdoub sum1=0.0;
        double qq =  integration_grid_rad.getGrid()[i_rad];

        for (int i_ang = 0; i_ang < quark.nangl ; ++i_ang) {

            double ang = integration_grid_ang.getGrid()[i_ang];
            Cdoub pdotq = sqrt(pp*qq)*ang;
            Cdoub kk = pp+qq-2.0*pdotq;  //Gluon momentum

            sum1 += (sqrt(1.0-ang*ang))* gluon.getValue(kk) * ((1.0/(kk))*(-2.0*qq+3.0*(1.0+(qq/pp))*pdotq-4.0*qq*ang*ang)) *
                    integration_grid_ang.getWeights_integration()[i_ang];

//            sum1 += (sqrt(1.0-ang*ang))* gluon.getValue(kk) * ((1.0/(kk*kk))*(-2.0*qq+3.0*(1.0+(qq/pp))*pdotq-4.0*qq*ang*ang)) *
//                    integration_grid_ang.getWeights_integration()[i_ang];
        }

        result +=  prefactors_color_integration *
                   (quark.getA()[i_rad]/(qq*quark.getA()[i_rad]*quark.getA()[i_rad]+quark.getB()[i_rad]*quark.getB()[i_rad]))* qq
                   * integration_grid_rad.getWeights_integration()[i_rad] * sum1;
    }

    return result;
}


Cdoub QuarkDse::SelfEnergyB(const Cdoub pp) const{

    Cdoub result =0.0;

    double prefactors_color_integration=(1.0)/(8.0*M_PI*M_PI*M_PI)*((4.0)/(3.0)) ;


    //Radial Integration:
    for (int i_rad = 0; i_rad < quark.nrad; ++i_rad) {


        Cdoub sum1=0.0;
        Cdoub qq =  integration_grid_rad.getGrid()[i_rad];

        for (int i_ang = 0; i_ang < quark.nangl ; ++i_ang) {

            double ang = integration_grid_ang.getGrid()[i_ang];
            Cdoub kk = pp+qq-2.0*sqrt(pp*qq)*ang;  //Gluon momentum

//
//            sum1 += (sqrt(1.0-ang*ang))* gluon.getValue(kk) * ((1.0/kk))*
//                    integration_grid_ang.getWeights_integration()[i_ang];

            sum1 += (sqrt(1.0-ang*ang))* gluon.getValue(kk) *
                    integration_grid_ang.getWeights_integration()[i_ang];
        }

        result +=  prefactors_color_integration *
                   (3.0 * quark.getB()[i_rad]/(qq*quark.getA()[i_rad]*quark.getA()[i_rad]+quark.getB()[i_rad]*quark.getB()[i_rad]))* qq
                   * integration_grid_rad.getWeights_integration()[i_rad] * sum1;
    }

    return result;
}

//getter
const Quark<double> &QuarkDse::getQuark() const {
    return quark;
}

double QuarkDse::calc_tolerance(const VecDoub &Aold, const VecDoub &Bold, const VecDoub &Anew, const VecDoub &Bnew) {

    VecDoub diff_ab; diff_ab.resize(Aold.size());

    for (int i = 0; i < parameter.n_rad; ++i) {
        diff_ab[i]=fabs(((Bold[i]/Aold[i]) -(Bnew[i]/Anew[i]))/((Bold[i]/Aold[i]) +(Bnew[i]/Anew[i])));
    }



    return *max_element(begin(diff_ab), end(diff_ab));


}

//QuarkDse::QuarkDse() : integration_grid_rad(  Line( K_IRCUT_SQUARED,K_UVCUT_SQUARED, parameter.n_rad, K_DSE_FUNC_MEASSURE,K_DSE_POINT_DISTR , true, false)),
//                       integration_grid_ang( Line(-1.0,1.0,K_INT_POINTS_ANG, K_DSE_FUNC_MEASSURE_ANG, K_DSE_POINT_DISTR_ANG, true, false) ),
//                       gluon( GluonWrapper({K_D_MT, K_OMEGA_MT}, K_WHICH_GLUON)),
//                       quark(integration_grid_rad.getGrid(), K_QUARK_MASS_AT_RENORMALISATION_PT, K_RENORMPOINT, parameter.n_rad, K_INT_POINTS_ANG, K_IRCUT_SQUARED, K_UVCUT_SQUARED){
//
//}


//Constructors
//todo: it should be pointed out that the value quadrature needs to be put to true in case if you want weights in your Line?
QuarkDse::QuarkDse(const DseParameter parameter): parameter(parameter),
                                                  integration_grid_rad(  Line( parameter.IRcutsq, parameter.UVcutsq, parameter.n_rad,
                                                                                parameter.func_meassure_rad, parameter.point_distri_rad , true, false)),
                                                   integration_grid_ang( Line(-1.0,1.0,parameter.n_angle, parameter.func_meassure_ang,
                                                                              parameter.point_distri_ang, true, false) ),
                                                   gluon( GluonWrapper({parameter.d_mt,parameter.omega_mt, parameter.pv_cutoff}, parameter.which_gluon)),
                                                   quark(integration_grid_rad.getGrid(), parameter.mass_at_renorm_pt,
                                                         parameter.renomalisation_point, parameter.n_rad, parameter.n_angle, parameter.IRcutsq, parameter.UVcutsq){

//    cout<<"main_BSE: "<<this->getIntegration_grid_rad().getGrid()[0]<<tab<<this->getIntegration_grid_rad().getGrid()[1]<<endl;
}

QuarkDse::QuarkDse(string parameter_file): parameter(parameter_file),
                                           integration_grid_rad(  Line( parameter.IRcutsq, parameter.UVcutsq, parameter.n_rad,
                                                                        parameter.func_meassure_rad, parameter.point_distri_rad , true, false)),
                                           integration_grid_ang( Line(-1.0,1.0,parameter.n_angle, parameter.func_meassure_ang,
                                                                      parameter.point_distri_ang, true, false) ),
                                           gluon( GluonWrapper({parameter.d_mt,parameter.omega_mt, parameter.pv_cutoff}, parameter.which_gluon)),
                                           quark(integration_grid_rad.getGrid(), parameter.mass_at_renorm_pt,
                                                 parameter.renomalisation_point, parameter.n_rad, parameter.n_angle, parameter.IRcutsq, parameter.UVcutsq){

}




void QuarkDse::Fill_QuarkDse(string quark_file){

    RQuark quarkn(quark_file);
    assert(quarkn.nrad == parameter.n_rad);
    setQuark(quarkn);
}

QuarkDse::~QuarkDse() {

    cout << "destroying dse";

}

const GluonWrapper &QuarkDse::getGluon() const {
    return gluon;
}

array<Cdoub, 2> QuarkDse::getValue(array<Cdoub, 1> pp) const{

    return getRhsQuark(pp[0]);
}

const Line &QuarkDse::getIntegration_grid_rad() const {
    return integration_grid_rad;
}

void QuarkDse::setQuark(const RQuark &quark) {
    QuarkDse::quark = quark;
}

const DseParameter &QuarkDse::getParameter() const {
    return parameter;
}








