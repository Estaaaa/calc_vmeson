//
// Created by esther on 05.07.17.
//

#ifndef CALC_BSE_CONST_DSE_H
#define CALC_BSE_CONST_DSE_H

namespace const_dse {

//for normal Mari-Tandy choose:
#define K_D_MT          0.93312
#define K_OMEGA_MT      0.16            // hier hatte esther 0.16, = 0.4^2 = omega_paul^2 korrekt

//When Pauli-Villar MT choose:
//#define K_D_MT          1.0
//#define K_OMEGA_MT      0.16
#define K_PV_CUTOFF_SQUARED 4.0+4

//Note that you also have to change the input parameter in the DSE constructor.
//todo: maybe we can find a work around that?
#define  K_WHICH_GLUON "MT"   //Can be MT or MTPV


//Cut-off for sqrt(p²)
#define K_IRCUT_SQUARED 1.0e-6
#define K_UVCUT_SQUARED 1.0e+6
#define K_QUARK_MASS_AT_RENORMALISATION_PT 0.0037
#define K_RENORMPOINT 19.0

//radial integration grid
#define K_INT_POINTS_RAD 150
#define K_DSE_POINT_DISTR "leg"
#define K_DSE_FUNC_MEASSURE "log"

//angular integration grid
#define K_INT_POINTS_ANG 32
#define K_DSE_POINT_DISTR_ANG "leg"
#define K_DSE_FUNC_MEASSURE_ANG "linear"

#define K_SOLVER_TOLERANCE 1.0e-8


}
#endif //CALC_BSE_CONST_DSE_H
