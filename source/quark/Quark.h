//
// Created by paul on 03.07.17.
//

#ifndef CALC_BSE_QUARK_H
#define CALC_BSE_QUARK_H

#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <assert.h>
#include <limits.h>
#include <unistd.h>
#include <libgen.h>
#include "../types/typedefs.h"



using namespace std;

//extern std::string getexepath();
//extern std::string gethomdir();



template <typename T> class Quark {

public:

    Quark(){
//        read_quark(quark_file);
    }

    Quark(const vector<double> &grid, double mass, double renorm_p, int nrad, int nangl, double IRcutsq,double UVcutsq):  mass(mass),renorm_p(renorm_p),IRcutsq(IRcutsq), UVcutsq(UVcutsq),
                                                                                                              nrad(nrad), nangl(nangl), grid(grid){


        vector<T> initial(nrad,1.0);
        A=initial; B=initial;
        Zm=Z2=1.0;

    };


public:

    double  Z2, Zm, mass, renorm_p,IRcutsq, UVcutsq;
    int nrad, nangl;


    void write_quark(string filename) const {
//        system("pwd");
//        cout<<getexepath()<<endl;

//        stringstream q_number; q_number << number_q;
//        string path =gethomdir()+"data/output/quark_"+q_number.str()+".txt";
        ofstream write(filename);
        assert( write.is_open());

//        cout << write.is_open() << endl;
        write<<"#p2   A		B"<<endl;
        for(int i=0; i<nrad; i++)
        {
            Cdoub x = grid[i]; Cdoub Afun = A[i]; Cdoub Bfun=B[i];

            write<<scientific<<setprecision(10)<<x.real()<<"\t"<<x.imag()<<"\t"<<Afun.real()<<"\t"<<Afun.imag()<<"\t"
                 <<Bfun.real()<<"\t"<<Bfun.imag()<<endl;

//            write<<scientific<<setprecision(10)<<real(grid[i])<<"\t"<<imag(grid[i])<<"\t"<<real(A[i])<<"\t"<<imag(A[i])<<"\t"
//               <<real(B[i])<<"\t"<<imag(B[i])<<endl;

//            write<<scientific<<setprecision(10)<<grid[i].real()<<"\t"<<grid[i].imag()<<"\t"<<A[i].real()<<"\t"<<A[i].imag()<<"\t"
//                 <<B[i].real()<<"\t"<<B[i].imag()<<endl;
        }
        write<<endl<<endl<<endl;
        write<<"#Z2"<<tab<<"Zm"<<tab<<"IRcutsq"<<tab<<"UVcutsq"<<tab<<"m"<<tab<<"mass"<<tab<<"renormp"<<endl;
        write <<"##" <<  Z2<<tab << Zm <<tab << IRcutsq <<tab << UVcutsq <<tab  << nrad <<tab << mass <<tab << renorm_p<< tab << nangl <<endl;
        write.close();

        //todo: Plot commando for quark in gnuplot: p 'quark_0.txt' u 1:3 i 0 ,''u 1:5 i 0


    }

    void read_quark(string quark_file){};

    const vector <T> &getGrid() const{
        return grid;
    }

    void setGrid(const vector <T> &grid) {
        Quark::grid = grid;
    }

    const vector <T> &getA() const {
        return A;
    }

    void setA(const vector <T> &A) {
        Quark::A = A;
    }

    const vector <T> &getB() const {
        return B;
    }

    void setB(const vector <T> &B) {
        Quark::B = B;
    }


protected:

    vector<T> grid;
    vector<T> A;
    vector<T> B;


};


class RQuark : public Quark<double>{

public:

    RQuark(string quark_file):Quark(){
        read_quark(quark_file);
    }

    RQuark(const vector<double> &grid, double mass, double renorm_p, int nrad, int nangl, double IRcutsq,double UVcutsq): Quark(grid, mass, renorm_p, nrad, nangl, IRcutsq, UVcutsq){};


    void read_quark(string quark_file)
    {
        double Rgrid,Igrid,  RAfunc, IAfunc,  RBfunc, IBfunc;
        ifstream read;
        read.open(quark_file, ios::in);           //DSEreal
        if(read.is_open())
        {
            string daten;
            while(getline(read,daten))
            {
                //the following line trims white space from the beginning of the string
                //            daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

                if(daten=="") {continue;}
                stringstream(daten) >> Rgrid >> Igrid >> RAfunc >> IAfunc >> RBfunc >> IBfunc;



                //Here reading in the mass and other parameters from the output file.
                if(daten[0] == '#') {
                    if(daten[1] == '#')
                    {
                        daten.erase(0,2);
                        stringstream(daten)  >> Z2 >> Zm >> IRcutsq>> UVcutsq >> nrad >> mass >> renorm_p >>nangl;
                    }else{continue;}
                }

                if(daten=="") {continue;}

                grid.push_back(Rgrid);
                A.push_back(RAfunc);
                B.push_back(RBfunc);


            }
        }else{cout<<"Quark::readquark: No data file with real quark found!"<<endl; assert( false);}
        read.close();

        grid.resize(nrad); A.resize(nrad); B.resize(nrad);

        cout<<"A quark propagator has been read inn with: nrad="<<nrad<<" ,mass="<<mass<<" ,Z2="<<Z2<<" ,Zm="<<Zm<<endl;

    }



};

class CQuark : public Quark<Cdoub>{

public:

    CQuark(string quark_file):Quark(){
        read_quark(quark_file);
    }


    void read_quark(string quark_file)
    {
        double Rgrid,Igrid,  RAfunc, IAfunc,  RBfunc, IBfunc;
        ifstream read;
        read.open(quark_file, ios::in);           //DSEreal
        if(read.is_open())
        {
            string daten;
            while(getline(read,daten))
            {
                //the following line trims white space from the beginning of the string
                //            daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

                if(daten[0] == '#') {continue;}
                if(daten=="") {continue;}
                stringstream(daten) >> Rgrid >> Igrid >> RAfunc >> IAfunc >> RBfunc >> IBfunc;

                if(daten=="") {continue;}
                if(daten=="#") { continue;}
                stringstream(daten) >> Z2 >> Zm >> IRcutsq>> UVcutsq >> nrad >> mass >> renorm_p >>nangl;
                if(daten=="") {continue;}

                grid.push_back(Rgrid+i_*Igrid);
                A.push_back(RAfunc+i_*IAfunc);
                B.push_back(RBfunc+i_* IBfunc);


            }
        }else{cout<<"Quark::readquark: No data file with real quark found!"<<endl; assert( false);}
        read.close();

        grid.resize(nrad); A.resize(nrad); B.resize(nrad);

        cout<<"A quark propagator has been read inn with: nrad="<<nrad<<" ,mass="<<mass<<" ,Z2="<<Z2<<" ,Zm="<<Zm<<endl;

    }

};

//todo: maybe later I will create an object Complexquark which will be different form this class and build out of a MultiObject, two in particular one for A and one for B.



#endif //CALC_BSE_QUARK_H
