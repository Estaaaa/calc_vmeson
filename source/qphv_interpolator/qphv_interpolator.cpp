//
// Created by esther on 13.04.18.
//

#include "qphv_interpolator.h"
#include <SuperIndex.h>
#include <fstream>


//Comment: This is hard coded for 12 tensor elements!!!! :(

void qphv_interpolator::read_inn_data_qphv_file(string filename) {

    cout<<"ReadInn qphv @:  "<<filename<<endl;

    VecCdoub safe;
    VecDoub Q_squared, p_squared_re, p_squared_im;
    VecDoub helper,p_squared_re_helper;
    vector<int> n_grid_points;

    ifstream read;
    read.open(filename, ios::in);



    if(read.is_open())
    {
        string daten;
        while(getline(read,daten))
        {

            VecDoub dummy; string s = ""; int pos;
            //Here reading in the mass from the output file.
            if(daten[0] == '#') {
                if(daten[1] == '#')
                {
                    pos= daten.find("\t");
                    for (int j = pos; j < daten.size(); ++j) {
                        s +=daten[j];
                        if(daten[j] == '\t' && j !=pos)
                        {
                            n_grid_points.push_back(stoi(s));
                            s = ""; }
                    }

                    n_Qs=n_grid_points[0]; n_ps=n_grid_points[1]; n_ps_im = n_grid_points[2];  n_ang=n_grid_points[3];

                }else{continue;}
            }else {


                if (daten == "") { continue; }

//                cout << daten << endl;
                //Reading in the amplitude:
                for (int j = 0; j < daten.size(); ++j) {
                    s += daten[j];
                    if (daten[j] == '\t') {
                        dummy.push_back(stod(s));
//                        cout << stod(s) << endl;
                        s = "";
                    }
//                if( c <= 'a'  ){}//Check if all of the read in data are numbers.
                }

//            cout<<dummy.size()<<endl;

            for (int k = 3; k < (((dummy.size()-5)/2) + 3) ; ++k) {
                safe.push_back(dummy[2*k-1]+i_*dummy[(2*k)]);
            }

            helper.push_back(dummy[0]);
            p_squared_re_helper.push_back(dummy[3]);
            p_squared_im.push_back(dummy[4]);
//            p_squared.push_back(dummy[3]+i_*dummy[4]);

            if(daten=="") {continue;}
            if(daten[0] == '#') {continue;}



            }


        }
    }else{cout<<"InterpolationQPHV::readIn: No data file found!"<<endl; assert( false);}
    read.close();
    cout<<"With gridpoints: "<<n_Qs<<tab<<n_ps<<tab<<n_ps_im<<tab<<n_ang<<endl;

//-----------------------------------------------------------------------------------------------


    //Reorder the ReadInn data such that it can be used in the interpolation
    p_squared_im.resize(n_ps_im);
//    SuperIndex<3> SI_3({n_ps,n_ang,n_Qs});
    SuperIndex<4> SI_4({n_ps_im,n_ps,n_ang,n_Qs});
//    SuperIndex<4> SI({12,n_ps,n_ang,n_Qs});
    SuperIndex<5> SI({12,n_ps_im, n_ps, n_ang,n_Qs});

//    SuperIndex<4> SI_diff_order({12,n_ang,n_Qs,n_ps});
    SuperIndex<5> SI_diff_order({12,n_ang,n_Qs,n_ps, n_ps_im});




    for (int i = 0; i < n_Qs; ++i) {
//        Q_squared.push_back(helper[SI_3.is({0,0,i})]);
        Q_squared.push_back(helper[SI_4.is({0,0,0,i})]);
    }
    VecDoub().swap(helper);

    for (int i = 0; i < n_ps; ++i) {

        p_squared_re.push_back(p_squared_re_helper[SI_4.is({0,i,0,0})]);
//        cout<<p_squared_re[i]<<endl;
    }
    VecDoub().swap(p_squared_re_helper);


    //In case of the amplitude, we need to change the order of the 2 and 3 indices.
    //Since written out in the data file the order is (Q^2, ang, p^2, --------amps)
    //but now we need (Q^2,p^2, (ang, amps))
    for (int l = 0; l < safe.size(); ++l) {

//        array<int,4> exp_i = SI_diff_order.are(l);
        array<int,5> exp_i = SI_diff_order.are(l);

//        vertex_data.push_back(safe[SI.is({exp_i[0],exp_i[3],exp_i[1],exp_i[2]})]);
        vertex_data.push_back(safe[SI.is({exp_i[0],exp_i[4],exp_i[3],exp_i[1],exp_i[2]})]);

//        vertex_data.push_back(0.5 * ps.getGrid()[exp_i[3]]*ps.getGrid()[exp_i[3]]);
    }
    VecCdoub().swap(safe); //cout<<vertex_data.size()<<endl;

    //Fill up the grid matrix
    grid.resize(0);
    grid.push_back(Q_squared);
    grid.push_back(p_squared_re);
    grid.push_back(p_squared_im);



    cout<<"InterpolationQPHV:: read inn:"<<filename<<"  with: n_total="<<vertex_data.size()<<endl;




}




void create_fake_data(string file_name){


    int n_Qs=6;
    int n_ps=20;
    int n_ps_im=4;
    int n_cheb=4;

//    Line Qs( 1.0e-3, 1.0e+3, n_Qs , "linear", "leg", false, false);
//    Line ps( 1.0e-3, 1.0e+3, n_ps , "linear", "leg", false, false);
    Line Qs( -1.0, 1.0, n_Qs , "linear", "leg", false, false);
    Line ps( -1.0, 1.0, n_ps , "linear", "leg", false, false);
    Line ps_imag( -1.0, 1.0, n_ps_im , "linear", "leg", false, false);
    Line Cheb( -1.0,1.0, n_cheb , "linear","tscheb", false, false);

//    string file_name= file_path+"fake_data.txt";


    //I need to first remove the old file since the write out routine is appending.
    if( remove( file_name.c_str()) != 0 )
    {  perror( "MesonBSE::calc_for_different_Psq::Error deleting file" ); cout<<file_name<<endl;}
    else
        puts( "MesonBSE::calc_for_different_Psq::File successfully deleted" );

    //Adding a frist Line to the qphv document to signal what is safed in which order.
    ofstream write;
    write.open(file_name, fstream::app);
    write<<"## dims: "<<tab<<n_Qs<<tab<<n_ps <<tab<<n_ps_im <<tab<<n_cheb<<tab<<"ende"<<endl;
    write<<"# Q²    Re(S_0) Im(S_0) Re(p²)  Im(p²)  Re/Im(Amp[1..12])"<<endl;


//    for (int p_square = 0; p_square < Psquasre.getNumber_of_grid_points(); ++p_square) {
    for (int q_square = 0; q_square < n_Qs; ++q_square) {

        //angular variable/dependence:
        for (int i = 0; i < n_cheb; ++i) {

            //radial component - real part
            for (int j = 0; j < n_ps; ++j) {

                //radial - imag part
                for (int l = 0; l < n_ps_im; ++l) {

                    write<<scientific<<setprecision(7)<< Qs.getGrid()[q_square] << tab                      //PP
                         << Cheb.getGrid()[i]<< tab << 0.0                //angle
                         << tab << ps.getGrid()[j]<<tab<< ps_imag.getGrid()[l] << tab ;  //pp

                    for (int k = 0; k < 12; ++k) {

                    Cdoub value= exp(-0.01*ps.getGrid()[j])* 1.0/(ps.getGrid()[j]+4.0)*Qs.getGrid()[q_square]
                                 +Cheb.getGrid()[i]*0.001*i_+k*0.5*sin(Cheb.getGrid()[i]);

//                    Cdoub value=  0.7/(ps.getGrid()[j]+0.7);
//                    Cdoub value=  sin(ps.getGrid()[j]);
//                    Cdoub value=  0.5;
//                        Cdoub value=  0.5 * ps.getGrid()[j]*ps.getGrid()[j] + 0.5 * Qs.getGrid()[q_square]*Qs.getGrid()[q_square];

                        write <<scientific<<setprecision(10)<< real( value ) << tab << imag (value) <<tab;

                    }

                    write<<endl;
                }

            }

            write<<endl<<endl;
        }

        write<<endl<<endl;

    }

    write.close();




}


