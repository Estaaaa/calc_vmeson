//
// Created by esther on 02.11.18.
//

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "qphv_naive_method.h"


void write_qphv(string filename, int n_basis,
                VecCdoub& ksquared, VecCdoub& kP, VecDoub& Psquared, matCdoub &result) {

    //Superindex Si {NBasis, N p², N Q.p,  N Q²}
    int n_kP=ksquared.size();  int n_Ps = Psquared.size();
    SuperIndex<2> Si= SuperIndex<2>({n_basis,n_kP});
//    SuperIndex<3> Si_res = SuperIndex<3>({n_basis, n_kP, n_Ps});


    //WATCHOUT: Here we are appending. Need to delete file before hand.
    ofstream write(filename);
    assert( write.is_open());

//Order is motivated by current order of Gernots interpolation file. First all Q^2 values
// then the dependece on the loop variable k(l) through k.P (ll, lz, ly).

    for (int i_kP = 0; i_kP < n_kP ; ++i_kP) {

        for (int i_Ps = 0; i_Ps < n_Ps; ++i_Ps) {


                write<<scientific<<setprecision(8)<<real(ksquared[i_kP])<<tab<<imag(ksquared[i_kP])<<tab<<real(kP[i_kP])
                 <<tab<<imag(kP[i_kP])<<tab<<Psquared[i_Ps]<<tab;

            //important: Here I am starting from 4 on, because I only want to store the transverse part in the output file!!!
            //Because in the ongoing calcutions I only use the transverse part and construct the gauge part by the Ball-Cui identity.
            for (int j = 4; j < n_basis; ++j) {

                int i2 = Si.is({j, i_kP});
                write <<scientific<<setprecision(8)<<real(result[i_Ps][i2])<<tab<<imag(result[i_Ps][i2])<<tab;
            }
            write<<endl;

        }

    }


    write.close();

}


void precalc_bses_for_qphv(vector<double>& Psquared, qphv_MesonBse& Bse, string qphv_folder, int which_vertex) {
//Calculate the Qphv vertex at Q² and Q'², as P²(Psquared) in the Bse. (-> Precalc iterated bse)

//Comment: (#Compromises #Hardcoded)
// - We are assuming tha Q² and Q'² are both real!
// - Currently I am working with the write and read format assiociated to amplitude. This could be switched as we are right now a file is written that is not used.
//Important: (##possiblechanges #differentsetups)
// - If a vector of Pd values is passed then we could get ride of the loop here and it also would be safed in one big file.
// -  didnt do it becasue in this case I have to rewrite the read function().

//    cout.setstate(ios_base::failbit);

    for (int i = 0; i < Psquared.size(); ++i) {

        //Create folder for data if not jet exsisting
        string current_folder= qphv_folder+"Amp"+to_string(i)+"/";

        //Check if the the folder is already exsisting.
        string helper="cd "+current_folder+" cd ../../../";
        int error1 = system(helper.c_str());
//        cout<<error1<<endl;
/*//        char* chr = strdup(current_folder.c_str());
//        char *cstr = &current_folder[0u];
//        char *myDir = dirname(cstr);
//        cout<<cstr<<tab<<tab<<myDir<<tab<<tab<<chr<<endl;
//        struct stat myStat;
//        if ((stat(myDir, &myStat) == 0) && (((myStat.st_mode) & S_IFMT) == S_IFDIR)) {*/
        //If not, it will be created in the next step.
        if (error1 ==0 ) {
            // myDir exists and is a directory. - Do nothing
//            cout<<"The folder does exsist, and I am here."<<endl;
        }else{
            //else - Creat the fodler.
            current_folder= qphv_folder+"Amp"+to_string(i)+"/";
                const int dir_err = mkdir(current_folder.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
                if (-1 == dir_err) {
//            printf("Error creating directory!n");
                    exit(1); }
        }

        current_folder= qphv_folder+"Amp"+to_string(i)+"/";



        //Change folder in  the current BSE
        Bse.setFolder(current_folder);
        Bse.setQuark_safer("qphv"+to_string(which_vertex)+"_");

        cout<<"Vertex - "<<which_vertex<<" /"<<to_string(i)<<":  Psquared= "<<Psquared[i]<<endl;

        //Solve the Bse for a certain values of Q² and safe each result individually.
        Bse.calc_for_different_Psq({Psquared[i]},"old_Qphv"+to_string(which_vertex));

        //removing not used file (change that in the future)
        string file_path = current_folder+"old_Qphv"+to_string(which_vertex)+".txt";
        remove( file_path.c_str());



//        if( remove( file_path.c_str()) != 0 )
//        {  perror( "MesonBSE::calc_for_different_Psq::Error deleting file" ); cout<<file_path<<endl;}
//        else
//            puts( "MesonBSE::calc_for_different_Psq::File successfully deleted" );

//        string pass_message = "rm "+current_folder+"old_Qphv.txt";
//        int error = system(pass_message.c_str());


        //Writing the data to the wanted file
        Bse.write_out_amplitude(current_folder+"Qphv"+to_string(which_vertex)+".txt", false);
        Bse.write_out_amplitude(current_folder+"Projected_Qphv"+to_string(which_vertex)+".txt", true);

    }

    cout.clear();
    cout<<"Precalculated the qphv bse - "<<which_vertex<<endl;



}


void where_is_vertex_needed_read_inn(string filename, int n_pp, VecCdoub& ksquared, VecCdoub& kP, VecDoub& Psquared) {

//    cout<<filename<<endl;
//    VecCdoub ksquared;
//    VecCdoub kP;
//    VecDoub Psquared;
    //Setting the vector to zero in order to push back properly.
    Psquared= VecDoub();
    ksquared = kP= VecCdoub();

    double k2r, k2i, kQr, kQi, Qs;
    ifstream read;
    read.open(filename, ios::in);           //DSEreal
    if(read.is_open())
    {
        string daten;
        while(getline(read,daten))
        {
            //the following line trims white space from the beginning of the string
            //            daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

            if(daten=="") {continue;}
            stringstream(daten) >> k2r >> k2i >> kQr >> kQi >> Qs;


            if(daten=="") {continue;}

            ksquared.push_back(k2r+i_ * k2i);
            kP.push_back(kQr+  i_ * kQi);
            Psquared.push_back(Qs);


        }
    }else{cout<<"Quark::readquark: No data file with real quark found!"<<endl; assert( false);}
    read.close();

    ksquared.erase(ksquared.begin()+ 0);ksquared.erase(ksquared.begin()+ 0);
    kP.erase(kP.begin()+ 0);kP.erase(kP.begin()+ 0);
    Psquared.erase(Psquared.begin()+ 0);Psquared.erase(Psquared.begin()+ 0);

    //this could also be done outside.. not sure if it is working.
    Psquared.resize(n_pp);

//    for (int i = 0; i < ksquared.size(); ++i) {
//        cout<<ksquared[i]<<tab<<kP[i]<<tab<<Psquared[i]<<endl; cin.get();
//    }



}



//previous testing functions.
void testing_iteration_scalar(ps_MesonBse &bse_scalar, string output_folder, string which_method) {

    bse_scalar.project_back_out_the_angle_inside();

    bse_scalar.setWhich_solver(which_method);

    VecCdoub value= bse_scalar.get_value_iteration(1.0+i_*0.0,0.5);
    cout<<"Result for the further iteration: "<<value[0]<<tab<<value[1]<<tab<<value[2]<<tab<<value[3]<<endl;

    cout.setstate(ios_base::failbit);

    ofstream write(output_folder+"scalar_iteration_test.txt");
    assert( write.is_open());
    for (int i = 0; i < bse_scalar.getPs().ll.getNumber_of_grid_points(); ++i) {

        value = bse_scalar.get_value_iteration(bse_scalar.getPs().AllGrid[2]->getGrid()[i], 0.5);

        write<< bse_scalar.getPs().AllGrid[2]->getGrid()[i]<<tab<<0.0<<tab<< real(value[0])<<tab<<imag(value[0])<<tab<<real(value[1])<<tab<<imag(value[1])
             <<tab<<real(value[2])<<tab<<imag(value[2])<<tab<<real(value[3])<<tab<<imag(value[3])<<endl;
    }

    cout.clear();

}

void testing_iteration_qphv(qphv_MesonBse &bse, string output_folder, string which_method) {

//    bse.write_out_amplitude(output_folder+"Test_Qphv_2.txt", true);

    bse.project_back_out_the_angle_inside();

//    bse.write_out_amplitude(output_folder+"Test_Qphv.txt");


    bse.setWhich_solver(which_method);
    VecCdoub value; //the vector will be n_basis=12 elements long.

//    VecCdoub value= bse.get_value_iteration(1.0+i_*0.0,0.5);
//    cout<<"Result for the further iteration: "<<value[0]<<tab<<value[1]<<tab<<value[2]<<tab<<value[3]<<endl;

    cout.setstate(ios_base::failbit);

    ofstream write(output_folder+"vertex_iteration_test.txt");
    assert( write.is_open());

    cout<<"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"<<endl;


    for (int i = 0; i < bse.getPs().ll.getNumber_of_grid_points(); ++i) {

//        value = bse.get_value_iteration(1.0 , 0.5);
//        write<< 1.0 <<tab<<0.0<<tab<< real(value[0])<<tab<<imag(value[0])

        value = bse.get_value_iteration(bse.getPs().AllGrid[2]->getGrid()[i], 0.5);

        write<< bse.getPs().AllGrid[2]->getGrid()[i]<<tab<<0.0<<tab<< real(value[0])<<tab<<imag(value[0])
             <<tab<<real(value[1])<<tab<<imag(value[1])
             <<tab<<real(value[2])<<tab<<imag(value[2])<<tab<<real(value[3])<<tab<<imag(value[3])
             <<tab<<real(value[4])<<tab<<imag(value[4])<<tab<<real(value[5])<<tab<<imag(value[5])
             <<tab<<real(value[6])<<tab<<imag(value[6])<<tab<<real(value[7])<<tab<<imag(value[7])
             <<tab<<real(value[8])<<tab<<imag(value[8])<<tab<<real(value[9])<<tab<<imag(value[9])
             <<tab<<real(value[10])<<tab<<imag(value[10])<<tab<<real(value[11])<<tab<<imag(value[11])<<endl;
    }

    write.close();

    cout.clear();

}





//Calculating the vertex at a certain vaule for all k.Q and one Q^2: when it is already initalized.
VecCdoub calculate_the_qphv_at(VecCdoub &ksquared, VecCdoub &kP, qphv_MesonBse &bse, string which_method_for_iteration) {

    // -> 1 Q^2 value - all k.Q, k^2 values.

//important: !!! Watch out so far it is only implemented that k.P can be real, no complex value.
// Although the way I set it up before form the file it could indeed be complex
// - Okay I need to fix this, as the file with the momenta for where we need the vertex, does include complexity in the angle.

    bse.project_back_out_the_angle_inside();


    bse.setWhich_solver(which_method_for_iteration);
    VecCdoub result;
    SuperIndex<2> Si_r = SuperIndex<2>({12,ksquared.size()});
    result.resize(Si_r.getNsuper_index());

    cout.setstate(ios_base::failbit);

//#pragma omp parallel
    for (int i = 0; i < ksquared.size(); ++i) {

        Cdoub z =  get_angle_from_kP(kP[i], ksquared[i], bse.getPhaseSpacePsquared());
//        cout<<"Got value for: "<<ksquared[i]<<tab<<z<<tab;
        VecCdoub value = bse.get_value_iteration(ksquared[i], z);

        for (int j = 0; j < value.size(); ++j) {
            result[Si_r.is({j,i})]=value[j]; }

        cout<<"Got value for: "<<ksquared[i]<<tab<<z<<tab;
        cout<<result[Si_r.is({0,i})]<<tab<<result[Si_r.is({1,i})]<<endl; //cin.get();


    }

    cout.clear();
    return result;

}

Cdoub get_angle_from_kP(Cdoub kP, Cdoub ks, Cdoub Ps) {
    return  (kP/(sqrt(Ps*ks))) ;
}


//Calculate and init all qphv values
void calculate_all_qphv( qphv_MesonBse &bse, string folder, VecDoub& Psquared, VecCdoub &ksquared, VecCdoub &kP,  string methode_for_iteration, int which_vertex) {
    // -> for ALL Q^2 value and ALL k.Q, k^2 values.

    time_t starting6, dasende6;
    time(&starting6);

    cout<<"---------------------------------------------------------------------------------"<<endl;
    matCdoub safer;



    //loop for all p-values
    for (int i = 0; i < Psquared.size(); ++i) {

        //init via file.  - Choose Q^2 = i
        bse.init_via_file(folder+"Amp"+to_string(i)+"/",Psquared[i],which_vertex);

        //calc -  for all values Q^2=i, kp, ksquared.
        VecCdoub result= calculate_the_qphv_at(ksquared, kP, bse, methode_for_iteration);
        safer.push_back(result);


    }


    cout<<"calculated the quark-photon vertex: "<<which_vertex<<endl;
    time(&dasende6);give_time(starting6,dasende6,"qphv"+to_string(which_vertex));

    //write....need to append
//        string file_path = folder+"quarkphoton_grid_out_"+to_string(i)+".txt";
    write_qphv(folder+"quarkphoton_grid_out_"+to_string(which_vertex)+".txt", 12, ksquared, kP, Psquared, safer );
    normal_write(folder+to_string(which_vertex), ksquared, Psquared, safer);
    cout<<"---------------------------------------------------------------------------------"<<endl;


}



void normal_write(string folder, VecCdoub& ksquared, VecDoub& Psquared, matCdoub& safer){


    //Here I wanted to check the print out. Because the format for the vertex seems confusing.
    ofstream write(folder+"_output_for_check_qphv.txt");
    assert( write.is_open());
    SuperIndex<2> Si =  SuperIndex<2>({12,ksquared.size()});


    //loop for all p-values
    for (int i = 0; i < Psquared.size(); ++i) {

        //call results form safer:
        VecCdoub result= safer[i];


        for (int j = 0; j < ksquared.size(); ++j) {
            write<<scientific<<setprecision(8)<<real(ksquared[j])<<tab<<0.0<<tab<<real(result[Si.is({0,j})])<<tab<<imag(result[Si.is({0,j})])<<tab<<
             real(result[Si.is({1,j})])<<tab<<imag(result[Si.is({1,j})])<<tab<<
             real(result[Si.is({2,j})])<<tab<<imag(result[Si.is({2,j})])<<tab<<
             real(result[Si.is({3,j})])<<tab<<imag(result[Si.is({3,j})])<<tab<<
             real(result[Si.is({4,j})])<<tab<<imag(result[Si.is({4,j})])<<tab<<
             real(result[Si.is({5,j})])<<tab<<imag(result[Si.is({5,j})])<<tab<<
             real(result[Si.is({6,j})])<<tab<<imag(result[Si.is({6,j})])<<tab<<
             real(result[Si.is({7,j})])<<tab<<imag(result[Si.is({7,j})])<<tab<<
             real(result[Si.is({8,j})])<<tab<<imag(result[Si.is({8,j})])<<tab<<
             real(result[Si.is({9,j})])<<tab<<imag(result[Si.is({9,j})])<<tab<<
             real(result[Si.is({10,j})])<<tab<<imag(result[Si.is({10,j})])<<tab<<
             real(result[Si.is({11,j})])<<tab<<imag(result[Si.is({11,j})])<<endl;



        }

    }

    write.close();

}
