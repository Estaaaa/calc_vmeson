//
// Created by esther on 02.11.18.
//

#ifndef CALC_VMESON_QPHV_NAIVE_METHOD_H
#define CALC_VMESON_QPHV_NAIVE_METHOD_H


/*class qphv_naive_method {

};*/


#include <Meson/MesonBse.h>

//Init of the BSE
//-> In Bse class.



//Grid points: Where do i need the vertex?
//ReadInn grid points form file like in case of Gernots vertex. (ok)
void where_is_vertex_needed_read_inn(string filename, int n_pp, VecCdoub& ksquared, VecCdoub& kP, VecDoub& Psquared);


//CALC
//Calc the BSE for all needed values and safe it to file
//Program flow will be: (1) init, (2) calc_for_all - this is what will be called in the form factor main.
void calculate_all_qphv(qphv_MesonBse &bse, string folder, VecDoub& Psquared, VecCdoub &ksquared, VecCdoub &kP,
                        string methode_for_iteration,  int which_vertex);


//For one particular Q^2 value, with passing the corresponding qphv bse.
VecCdoub calculate_the_qphv_at(VecCdoub& ksquared, VecCdoub& kP, qphv_MesonBse& bse, string which_method_for_iteration);

//Helping function:
//Caclculate the angle form the four vector product p.K -> z
Cdoub get_angle_from_kP(Cdoub kP, Cdoub ks, Cdoub Ps);





//WRITE
//Save into file - the result for the form factor - in order to Read it back in for the formfactor_calc() function.
void write_qphv(string filename, int n_basis,
                VecCdoub& ksquared, VecCdoub& kP, VecDoub& Psquared, matCdoub &result);

void normal_write(string folder, VecCdoub& ksquared, VecDoub& Psquared, matCdoub& safer);


//Init (ok).
//Init for vertex interpolation: By calculating all the BSE for different Q², whcih will be needed later and safing them to file.
//Precalculate the qphv bse for all Q² values and safe it to file
void precalc_bses_for_qphv( vector<double>& Psquared, qphv_MesonBse& Bse, string qphv_folder, int which_vertex);




//Tests----------
//First test: extra iteration for real value of the bse
void testing_iteration_scalar(ps_MesonBse& bse, string folder, string which_method);
void testing_iteration_qphv(qphv_MesonBse& bse, string folder, string which_method);

#endif //CALC_VMESON_QPHV_NAIVE_METHOD_H
