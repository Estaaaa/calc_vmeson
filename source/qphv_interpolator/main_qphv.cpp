//
// Copied by Esther 13.04.18
//

//This main_BSE interpolates a set of qphv data to different grid point.
// To start you need a qphv data file and a file with you choice of new grid points you want to interpolate onto
//
// I started by implementing a 2-d spline interpolation for the total and the relative momentum
// vertex depends on: Q², Q.p, p². Q.p is translated into a Tscheby Polynom dependece,
// The interpolation is working on  leverl of the Polynominals in case of the angle.




#include <typedefs.h>
#include <array>
#include <BaseInterpolPE.h>
#include <SuperIndex.h>
#include <qphv_interpolator.h>
#include <homdir.h>

#define K_N_DRESSING_F  (12*4)

int main(int argc, char* argv[]) {


    //Folder structure
    string folder= "10";
    string output_folder =gethomdir()+"data/data"+folder+"/output_0/";


    //As a test: Create a set of fake vertex data
    string filename = output_folder+"fake_data.txt";
    create_fake_data(filename);


    //Which are the values you want interpolated?
    matDoub all_new_x_values;
    VecCdoub result;

    VecDoub new_Qs_v= {0.5,0.2, 0.8};
    VecDoub new_ps_re_v= {-0.5,0.2, -0.9};
    VecDoub new_ps_im_v= {0.0,0.1,0.1};
    all_new_x_values.push_back(new_Qs_v);
    all_new_x_values.push_back(new_ps_re_v);
    all_new_x_values.push_back(new_ps_im_v);

    cout<<"Test: How many value do I want to interpoalte: "<<all_new_x_values.size()<<tab<<all_new_x_values[0].size()<<endl;
//    cout<<"The first values: "<<all_new_x_values[0][0]<<tab<<all_new_x_values[1][0]<<endl;


//2-d interpolation of the quark-photon vertex.

    //First check if a interpolation is nessercary, maybe?
    // is_interpolation_nessercary();



    //Next: Read in the data from a data file - Could all be in a Read code. Using a class for this
    //In this we are filling up all the current gird points of the data : grids
    //Also the vector with the f(x,y) - vertex data.
    qphv_interpolator data(filename);


    //Now we nee a interpolator
    SplineInterpolator<3, K_N_DRESSING_F, double, Cdoub> Interpol;


    //Feeding the interpolation with the vertex and grid data.
//    Interpol.Update(xvalues_interpol, vertex_data);
    Interpol.Update(data.grid, data.vertex_data);
    cout<<"Interpolation Set-up..."<<endl;

    //Now erveything is set-up and ready to interpolate.

    //Here we safe the results.
    array<Cdoub, K_N_DRESSING_F> result_y;

    int number_new_x=all_new_x_values[0].size();
    SuperIndex<2> SI_all_new_values({K_N_DRESSING_F,number_new_x});
    result.resize(SI_all_new_values.getNsuper_index()); //cout<<"Size of result: "<<result.size()<<tab<<number_new_x<<endl;

    for (int i = 0; i < number_new_x; ++i) {

//        array<double, 2> new_x_values = {all_new_x_values[0][i], all_new_x_values[1][i]};
        array<double, 3> new_x_values = {all_new_x_values[0][i], all_new_x_values[1][i], all_new_x_values[2][i]};
//        cout<<all_new_x_values[0][i]<<tab<<all_new_x_values[1][i]<<tab<<all_new_x_values[2][i]<<endl;

        Interpol.getValue(result_y, new_x_values); //cout<<"after: "<<result_y.size()<<endl;

        for (int j = 0; j < K_N_DRESSING_F ; ++j) {
            result[SI_all_new_values.is({j,i})] = result_y[j];
        }

//        cout<<"Result["<<i<<"] : "<<result[0]<<tab<<result[1]<<tab<<result[2]<<tab<<result[3]<<tab
//            <<result[4]<<tab<<result[6]<<tab<<result[7]<<tab<<result[8]<<tab<<result[9]<<tab<<result[10]
//            <<tab<<result[11]<<endl;


    }
    cout<<"Interpolation Done."<<endl;




    //Write out the result
    string safe_output =  output_folder + "fake_results.txt";
    ofstream writeF(safe_output);
    assert( writeF.is_open());

    for (int i = 0; i < number_new_x; ++i) {


            for (int j = 0; j < 4 ; ++j) {

                writeF<<scientific<<setprecision(7)<<all_new_x_values[0][i]<<tab<< 0.0 << tab << 0.0 << tab <<
                      all_new_x_values[1][i] << tab << all_new_x_values[2][i] << tab;

                for (int k = 0; k < 12 ; ++k) {
                    writeF <<scientific<<setprecision(10)<< real( result[SI_all_new_values.is({(k+12*j),i})] )
                          << tab << imag (result[SI_all_new_values.is({(k+12*j),i})]) <<tab;
                }

                writeF<<endl;
            }


        writeF<<endl<<endl;

        }
        writeF.close();



        return 0;
}




