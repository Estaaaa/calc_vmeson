//
// Created by esther on 13.04.18.
//

#ifndef CALC_VMESON_QPHV_INTERPOLATOR_H
#define CALC_VMESON_QPHV_INTERPOLATOR_H

#include <typedefs.h>
#include <fstream>
#include <Grids.h>
#include <iomanip>


extern void create_fake_data(string );


class qphv_interpolator {

public:

    qphv_interpolator(string qphv_file){

        read_inn_data_qphv_file(qphv_file);
    };



private:


    void read_inn_data_qphv_file(string filepath);


public:

    VecCdoub vertex_data;
    matDoub grid;
    int n_Qs, n_ps, n_ang, n_ps_im;


};






#endif //CALC_VMESON_QPHV_INTERPOLATOR_H
