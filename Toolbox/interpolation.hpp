#ifndef INCLUDED_INTERPOLATION_HPP
#define INCLUDED_INTERPOLATION_HPP
#define _USE_MATH_DEFINES
#include <iostream>
#include <fstream>
#include <cmath>
#include <string>
#include <complex>
#include <vector>
#include <math.h>
#include <cstdlib>


#ifndef PI
#define PI M_PI
#endif


#ifndef tab
  #define tab "\t"
  #endif

using namespace std;

typedef vector<double> vecd;



class Interpol
{
  double a,b;
  double initialval;
  int N; //order of interpolation
  
  vecd storeabsc;
  vecd storeval, storey2a;
  vecd abs,absresize,absang,abslog;
  vecd coeff;
   
  void spline(vecd&, vecd&, vecd&);
  void splint(vecd&, vecd&, vecd&, double&, double&, int&, int&);
  void splintdiff(vecd&, vecd&, vecd&, double&, double&, int&, int&);
  void bisec(double, int &, int &);
  
  public:  
  
  double lowlimit, uplimit;
  void spline_update_x(vecd &);
  void spline_update_y(vecd &);
  void spline_update_all(vecd &, vecd &);
  void spline_readvalues(vecd &);
  double spline_interpol(double);
  void spline_interpol(vecd &, vecd &);
  void spline_interpol(vecd&, vecd&, vecd&, vecd&);  //output y automatically resized
  void spline_interpol_dydx(vecd &, vecd &);
  void spline_interpol_dydx(vecd&, vecd&, vecd&, vecd&);  //output y automatically resized
  
};

#endif