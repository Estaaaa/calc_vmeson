#ifndef AIDS_H
#define AIDS_H

void give_time(time_t a, time_t b, string name)
{
    cout<<"Time of "<<name<<"  :"<<round(difftime(b,a)/(3600))<<"h  , "<<round((difftime(b,a)/(3600)-trunc(difftime(b,a)/3600))*60)<<"m  , "<<
          ((difftime(b,a)/(3600)-trunc(difftime(b,a)/3600))*60-trunc((difftime(b,a)/(3600)-trunc(difftime(b,a)/3600))*60))*60<<"s"<<endl;
}


void Input_reader(int argc, char * const argv[], int &mDec, int &nzDec, int &nyDec, int &nyBSA, string &rundef, int &BSEcalctag, int &Chiraltag,
                  int &PauliVillartag, int &round, int &mQuark, int &nQuark, int &calc_quark3, int &calc_quark3_steps, int &formfactortag, int &decaytag,
                  int &n_s, double &quarkmass, double &Mresult2, double &Mdecay)
{
    if(argc<6){cout<<"Using set-up specified in main_BSE: "<<endl;rundef=rundef+"_0/";}
    else if(argc==6){cout<<"Reading Inn grid-points & later BSE is calculated: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; }
    else if(argc==7){cout<<"Reading Inn grid-points, BSE can be choosen to be calculated(1) or readInn(0) or masscalc(2): "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);}
    else if(argc==8){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);}
    else if(argc==9){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag, Pauli-Villar tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);PauliVillartag=atoi(argv[8]);}
    else if(argc==10){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag, Pauli-Villar tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);
        PauliVillartag=atoi(argv[8]); round=atoi(argv[9]);}
    else if(argc==12){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag, Pauli-Villar tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);
        PauliVillartag=atoi(argv[8]); round =atoi(argv[9]); mQuark=atoi(argv[10]); nQuark=atoi(argv[11]);}
    else if(argc==11 || argc==13){cout<<"Something is wrong here? Need angular and momentum number for real quark propagator & or the nuber of the complex quark k3 which is readInn"<<endl<<"Standard settings are running!"<<endl;}
    else if(argc==14){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag, Pauli-Villar tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);
        PauliVillartag=atoi(argv[8]); round =atoi(argv[9]); mQuark=atoi(argv[10]); nQuark=atoi(argv[11]); calc_quark3=atoi(argv[12]);calc_quark3_steps=atoi(argv[13]);}

    else if(argc==15){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag, Pauli-Villar tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);
        PauliVillartag=atoi(argv[8]); round =atoi(argv[9]); mQuark=atoi(argv[10]); nQuark=atoi(argv[11]); calc_quark3=atoi(argv[12]);calc_quark3_steps=atoi(argv[13]);formfactortag=atoi(argv[14]);}
    else if(argc==16){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag, Pauli-Villar tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);
        PauliVillartag=atoi(argv[8]); round =atoi(argv[9]); mQuark=atoi(argv[10]); nQuark=atoi(argv[11]); calc_quark3=atoi(argv[12]);calc_quark3_steps=atoi(argv[13]);formfactortag=atoi(argv[14]); decaytag=atoi(argv[15]);}
    else if(argc==17){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag, Pauli-Villar tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);
        PauliVillartag=atoi(argv[8]); round =atoi(argv[9]); mQuark=atoi(argv[10]); nQuark=atoi(argv[11]); calc_quark3=atoi(argv[12]);calc_quark3_steps=atoi(argv[13]);
        formfactortag=atoi(argv[14]); decaytag=atoi(argv[15]); n_s=atoi(argv[16]);}
    else if(argc==18){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag, Pauli-Villar tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);
        PauliVillartag=atoi(argv[8]); round =atoi(argv[9]); mQuark=atoi(argv[10]); nQuark=atoi(argv[11]); calc_quark3=atoi(argv[12]);calc_quark3_steps=atoi(argv[13]);
        formfactortag=atoi(argv[14]); decaytag=atoi(argv[15]); n_s=atoi(argv[16]);quarkmass=atof(argv[17]);}
    else if(argc==19){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag, Pauli-Villar tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);
        PauliVillartag=atoi(argv[8]); round =atoi(argv[9]); mQuark=atoi(argv[10]); nQuark=atoi(argv[11]); calc_quark3=atoi(argv[12]);calc_quark3_steps=atoi(argv[13]);
        formfactortag=atoi(argv[14]); decaytag=atoi(argv[15]); n_s=atoi(argv[16]);quarkmass=atof(argv[17]);Mresult2=atof(argv[18]);}
    else if(argc==20){cout<<"Reading Inn grid-points, BSE choice, Chirallimit tag, Pauli-Villar tag: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); nyBSA=atoi(argv[4]);rundef=rundef+"_"+argv[5]+"/"; BSEcalctag=atoi(argv[6]);Chiraltag=atoi(argv[7]);
        PauliVillartag=atoi(argv[8]); round =atoi(argv[9]); mQuark=atoi(argv[10]); nQuark=atoi(argv[11]); calc_quark3=atoi(argv[12]);calc_quark3_steps=atoi(argv[13]);
        formfactortag=atoi(argv[14]); decaytag=atoi(argv[15]); n_s=atoi(argv[16]);quarkmass=atof(argv[17]);Mresult2=atof(argv[18]); Mdecay=atof(argv[19]);}
    else if(argc>20){cout<<"Something is wrong here? Standart set-up runs. "<<endl;}


    //outout file with information

    string buffer = rundef+"run_info.txt";
    ofstream f(buffer.c_str());
    f<<"#m   nz ny  nyBSA   BSA Chiral  Pauli   DSEround    mQ  nQ  calc3   save3   fft decay   n_s mq  Mpi Mdecay"<<endl;
    f<<mDec<<tab<<nzDec<<tab<<nyDec<<tab<<nyBSA<<tab<<BSEcalctag<<tab<<Chiraltag<<tab<<PauliVillartag<<tab<<round<<tab<<mQuark<<tab<<nQuark<<tab<<calc_quark3<<tab
       <<calc_quark3_steps<<tab<<formfactortag<<tab<<decaytag<<tab<<n_s<<tab<<quarkmass<<tab<<Mresult2<<tab<<Mdecay<<endl;
    f.close();

}

void Input_reader_new(int argc, char * const argv[], int &mDec, int &nzDec, int &nyDec, string &folder, int &n_s, string &rundef)
{
    if(argc<5){cout<<"Using set-up specified in main_BSE: "<<endl;rundef=rundef+"_0/";}
    else if(argc==5){cout<<"Reading Inn grid-points & folder: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); folder=(argv[4]); rundef=rundef+"_0/";}
    else if(argc==6){cout<<"Reading Inn grid-points & folder: "; mDec=atoi(argv[1]); nyDec=atoi(argv[2]); nzDec=atoi(argv[3]); folder=(argv[4]);  n_s=atoi(argv[5]); rundef=rundef+"_0/";}
    else{cout<<"Something is wrong here? Standart set-up runs. "<<endl;}

}


void Input_reader_file(int argc, char * const argv[], int &mDec, int &nzDec, int &nyDec, int &nyBSA, string &rundef, int &BSEcalctag, int &Chiraltag,
                  int &PauliVillartag, int &round, int &mQuark, int &nQuark, int &calc_quark3, int &calc_quark3_steps, int &formfactortag, int &decaytag,
                  int &n_s, double &quarkmass, double &Mresult2, double &Mdecay)
{

       string buffer = rundef+"run_info.txt";


       ifstream infile;
       infile.open(buffer.c_str(), ios::in);           //DSEreal
       if(infile.is_open())
       {
           string daten;
           while(getline(infile,daten))
           {
               //the following line trims white space from the beginning of the string
               daten.erase(daten.begin(), find_if(daten.begin(), daten.end(), not1(ptr_fun<int, int>(isspace))));

               if(daten[0] == '#') {continue;}
               stringstream(daten) >>mDec >>nzDec >>nyDec >>nyBSA >>BSEcalctag >>Chiraltag >>PauliVillartag >>round >>mQuark >>nQuark >>calc_quark3
                                     >>calc_quark3_steps >>formfactortag >>decaytag >>n_s >>quarkmass >>Mresult2 >>Mdecay;
               if(daten=="#") { continue;}
               if(daten=="") {continue;}

           }
       }else{cout<<"No data file with real quark found!"<<endl; return;}


       cout<<" info in. ";


}



void writer(string name, int n, double A[], complex<double> B[], double C)
{
    string buffer = name;
    ofstream f(buffer.c_str(),ios::app);
    assert(!f.fail());
    f<<endl;
    for(int i=0; i<n; i++)
    {
        f<<A[i]<<tab<<real(B[i])<<tab<<imag(B[i])<<tab<<C<<endl;
    }
    f.close();
    assert(!f.fail());
}

#endif // AIDS_H
