#define _USE_MATH_DEFINES
#include <iostream>
#include <fstream>
#include <cmath>
#include <string>
#include <complex>
#include <vector>
#include <math.h>
#include <cstdlib>
#include "interpolation.hpp"
#include <omp.h>

#ifndef ii
#define ii std::complex<double> (0.,1.)
#endif


#ifndef PI
#define PI M_PI
//#define PI 3.1415926535897932384626433832795
//#define PI 3.14159265358979323846264338327950288419716939937510
#endif

#ifndef tab
  #define tab "\t"
  #endif
  
using namespace std;

typedef vector<double> vecd;


/*Interpol::Interpol(int order, double aint, double bint, const char* interpoltype)
{
  if(interpoltype="Cheb")
  {
  N=order;
  a=aint;
  b=bint;

  for(int k=0; k<N; k++)
  {
   absang.push_back(PI*(k+0.5)/N);
   abs.push_back(cos(absang[k])); 
   absresize.push_back(abs[k]*0.5*(b-a)+0.5*(b+a));
  }
  coeff.resize(N);
  }
}
*/

//Spline general

void Interpol::bisec(double x, int &low, int &big)
{
  int n=storeabsc.size();
  int mid;
  low=0;
  big=n-1;
  bool ascnd=(storeabsc[n-1]>= storeabsc[0]);
  
  while(big-low>1)
  {
   mid=(big+low) >> 1;
   if(x >= storeabsc[mid] == ascnd)
   {
    low=mid; 
   }
   else
   {
    big=mid; 
   }
  }
  low=max(0,min(n,low));
  big=low+1;
}

void Interpol::spline(vecd &x, vecd &y, vecd &y2)		// Spline only long double up to now
{
  double  p,qn,sig,un;
  int n;
  n=x.size();
    
  vecd u;
  u.resize(n);

    y2[0]=u[0]=0.0;		//natural splines

  for (int i=1;i<n;i++) 
  {
    sig=(x[i]-x[i-1])/(x[i+1]-x[i-1]);
    p=sig*y2[i-1]+2.0;
    y2[i]=(sig-1.0)/p;
    u[i]=(y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]);
    u[i]=(6.0*u[i]/(x[i+1]-x[i-1])-sig*u[i-1])/p;
  }


  qn=un=0.0;			//natural splines

  y2[n-1]=(un-qn*u[n-2])/(qn*y2[n-2]+1.0);

  for(int k=n-2;k>0;k--)
  {
    y2[k]=y2[k]*y2[k+1]+u[k];
  }
  u.clear();
}

void Interpol::splint(vecd &xa, vecd &ya, vecd &y2a, double &x, double  &y, int &low, int &hig)
{
  double h,aI,aII;
  
  bool below =true;
  bool above =true;
  /*
  if(xa[low]>x)
    {
      low = low-1;
      while(xa[low]>x)
	{
	  low = low-1;
	}
      below=false;
    }
  
  if(xa[hig]<x)
    {
      hig = hig+1;
      while(xa[hig]<x)
	{
	  hig = hig+1;
	}
      above=false;
    }
  
  if(below)
    {
      while(xa[low+1]<x)
	{
	  low = low+1;
	}
	below=false;
    }
 
  if(above)
    {
      while(xa[hig-1]>x)
	{
	  hig = hig-1;
	}
  	above = false; 

    }
  */
  this->bisec(x,low, hig);

  h=xa[hig]-xa[low];
  if (h == 0.0); 
  aI=(xa[hig]-x)/h;
  aII=(x-xa[low])/h;
  y=aI*ya[low]+aII*ya[hig]+((aI*aI*aI-aI)*y2a[low]+(aII*aII*aII-aII)*y2a[hig])*(h*h)/6.0;
}

void Interpol::splintdiff(vecd &xa, vecd &ya, vecd &y2a, double &x, double  &dydx, int &low, int &hig)
{
  int n;
  n= xa.size();
  double h,A,B;
  bool below =true;
  bool above =true;
  

  if(xa[low]>x)
    {
      low = low-1;
      while(xa[low]>x)
	{
	  low = low-1;
	}
      below=false;
    }
  
  if(xa[hig]<x)
    {
      hig = hig+1;
      while(xa[hig]<x)
	{
	  hig = hig+1;
	}
      above=false;
    }
  
  if(below)
    {
      while(xa[low+1]<x)
	{
	  low = low+1;
	}
	below=false;
    }
 
  if(above)
    {
      while(xa[hig-1]>x)
	{
	  hig = hig-1;
	}
  	above = false; 

    }

  h=xa[hig]-xa[low];
  if (h == 0.0); 
  A=(xa[hig]-x)/h;
  B=(x-xa[low])/h;
  dydx=(ya[hig]-ya[low])/h-(3.0*A*A-1.0)/6.0*h*y2a[low]+(3.0*B*B-1.0)/6.0*h*y2a[hig];
  
}

//Spline with storage of values

void Interpol::spline_update_x(vecd &xk)
{
   storeabsc.clear();
     
   double maxvalue, minvalue;
   minvalue=xk[0];
   maxvalue=xk[xk.size()-1];
   for(int i=0; i<xk.size(); i++)
   {
      if(xk[i]<minvalue){minvalue=xk[i];}
      if(xk[i]>maxvalue){maxvalue=xk[i];}      
   }
  lowlimit=minvalue;
  uplimit=maxvalue;
  
  for(int i=0; i<xk.size(); i++)
  {
    storeabsc.push_back(log(xk[i]));
  }
}

void Interpol::spline_update_y(vecd &yk)
{
  if(storeabsc.size()==0)
  {
    cout << "need to store abscissas first, no data written to spline routine!!!" << endl;
  }
  else
  {
    storeval.clear();
    for(int i=0; i<yk.size(); i++)
    {
      storeval.push_back(yk[i]);
    }
    storey2a.resize(storeabsc.size());
    spline(storeabsc,storeval,storey2a);
  }
}

void Interpol::spline_update_all(vecd &xk, vecd &yk)
{
   storeabsc.clear();
   storeval.clear();
   
   
   double maxvalue, minvalue;
   minvalue=xk[0];
   maxvalue=xk[xk.size()-1];
   for(int i=0; i<xk.size(); i++)
   {
      if(xk[i]<minvalue){minvalue=xk[i];}
      if(xk[i]>maxvalue){maxvalue=xk[i];}      
   }
  lowlimit=minvalue;
  uplimit=maxvalue;
  
  for(int i=0; i<xk.size(); i++)
  {
    storeabsc.push_back(log(xk[i]));
    storeval.push_back(yk[i]);
  }
  storey2a.resize(storeabsc.size());
  spline(storeabsc,storeval,storey2a);
}

void Interpol::spline_readvalues(vecd &y)
{
  y.clear();
  
  for(int i=0; i<storeval.size(); i++)
  {
    y.push_back(storeval[i]);
  }  
}

double  Interpol::spline_interpol(double x)
{
  double  y;
  int low=0;
  int big=storeabsc.size()-1;
  double dummylogx;
 
  if(x<lowlimit)
  {
    y=storeval[low];
  }
  else if(x>uplimit)
  {
    y=storeval[big];
  }
  else 
  {
    dummylogx=log(x);
    splint(storeabsc,storeval,storey2a,dummylogx,y,low,big);
  }

  return y;
}

void Interpol::spline_interpol(vecd &x, vecd &y)
{
  int length=x.size();
  y.resize(length);
  double dummylogx;
  int low=0;
  int big=storeabsc.size()-1;
  
  for(int k=0; k<length; k++)
  {
    if(x[k]<lowlimit)
    {
      y[k]=storeval[low];
    }
    else if(x[k]>uplimit)
    {
      y[k]=storeval[big];
    }
    else
    {
      dummylogx=log(x[k]);
      splint(storeabsc,storeval,storey2a,dummylogx,y[k],low,big);
    }
  }
}

void Interpol::spline_interpol_dydx(vecd &x, vecd &dydx)
{
  int length=x.size();
  dydx.resize(length);
  int low=0;
  int big=storeabsc.size()-1;
  vecd dummylogx;
  
  for(int k=0; k<length; k++)
  {
    if(x[k]<lowlimit){dummylogx.push_back(log(lowlimit));}
    else if(x[k]>uplimit){dummylogx.push_back(log(uplimit));}
    else {dummylogx.push_back(log(x[k]));}
    
  }

  for(int k=0; k<length; k++)
  {
    splintdiff(storeabsc,storeval,storey2a,dummylogx[k],dydx[k],low,big);
  }
}

//Spline without storage of values

void Interpol::spline_interpol(vecd &xk, vecd &yk, vecd &x, vecd &y)
{
  int length=x.size();
  y.resize(length);
  int low=0;
  int big=xk.size()-1;
  vecd y2a;
  y2a.resize(xk.size());
  double maxvalue, minvalue;
  vecd dummylogx;
  
  minvalue=xk[0];
  maxvalue=xk[xk.size()-1];
  for(int i=0; i<xk.size(); i++)
  {
      if(xk[i]<minvalue){minvalue=xk[i];}
      if(xk[i]>maxvalue){maxvalue=xk[i];}      
  }
  lowlimit=minvalue;
  uplimit=maxvalue;

  for(int k=0; k<length; k++)
  {
    if(x[k]<lowlimit){dummylogx.push_back(log(lowlimit));}
    else if(x[k]>uplimit){dummylogx.push_back(log(uplimit));}
    else {dummylogx.push_back(log(x[k]));}
  }

  spline(xk,yk,y2a);

  for(int k=0; k<length; k++)
  {
    splint(xk,yk,y2a,dummylogx[k],y[k],low,big);
  }
}

void Interpol::spline_interpol_dydx(vecd &xk, vecd &yk, vecd &x, vecd &dydx)
{
  int length=x.size();
  dydx.resize(length);
  int low=0;
  int big=xk.size()-1;
  vecd y2a;
  y2a.resize(xk.size());
  double maxvalue, minvalue;
  vecd dummylogx;
  
  minvalue=xk[0];
  maxvalue=xk[xk.size()-1];
  for(int i=0; i<xk.size(); i++)
  {
      if(xk[i]<minvalue){minvalue=xk[i];}
      if(xk[i]>maxvalue){maxvalue=xk[i];}      
  }
  lowlimit=minvalue;
  uplimit=maxvalue;

  for(int k=0; k<length; k++)
  {
      if(x[k]<lowlimit){dummylogx.push_back(log(lowlimit));}
      else if(x[k]>uplimit){dummylogx.push_back(log(uplimit));}
      else {dummylogx.push_back(log(x[k]));}
  }

  spline(xk,yk,y2a);

  for(int k=0; k<length; k++)
  {
    splint(xk,yk,y2a,dummylogx[k],dydx[k],low,big);
  }
}
