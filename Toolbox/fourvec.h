#pragma once

#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <complex>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>

using namespace std;


class four_vec
{
  public:

    complex<double> inhalt[4];

 public:
    four_vec()
    {
        for(int i=0; i<4; i++)
        {
            inhalt[i]=0.0;
        }
    }

    four_vec(complex<double> k[4])
    {
        for(int i=0; i<4; i++)
        {
            inhalt[i]=k[i];
        }
    }
    four_vec(complex<double> k)
    {
        for(int i=0; i<4; i++)
        {
            inhalt[i]=k;
        }
    }



    void write()
    {
        for(int i=0; i<4; i++)
        {
            cout<<inhalt[i]<<" , ";
        }
        cout<<endl;
    }

    void spherical_fv(complex<double> p2, double z, double y)
    {
        inhalt[0]=0.0; inhalt[1]=sqrt(p2)*sqrt(1.0-z*z)*sqrt(1.0-y*y);
        inhalt[2]=sqrt(p2)*y*sqrt(1.0-z*z); inhalt[3]=sqrt(p2)*z;
    }



};


