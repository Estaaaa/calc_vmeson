#ifndef FOURVEC_OPS_H
#define FOURVEC_OPS_H


four_vec operator+(four_vec k, four_vec p)
{
    four_vec result;
    for(int i =0; i<4; i++)
    {
        result.inhalt[i]=k.inhalt[i]+p.inhalt[i];
    }
    return result;
}

four_vec operator-(four_vec k, four_vec p)
{
    four_vec result;
    for(int i =0; i<4; i++)
    {
        result.inhalt[i]=k.inhalt[i]-p.inhalt[i];
    }
    return result;
}

four_vec operator*(double k, four_vec p)
{
    four_vec result;
    for(int i =0; i<4; i++)
    {
        result.inhalt[i]=k*p.inhalt[i];
    }
    return result;
}

four_vec operator*(four_vec p, double k)
{
    four_vec result;
    for(int i =0; i<4; i++)
    {
        result.inhalt[i]=p.inhalt[i]*k;
    }
    return result;
}


four_vec operator*(complex<double> k, four_vec p)
{
    four_vec result;
    for(int i =0; i<4; i++)
    {
        result.inhalt[i]=k*p.inhalt[i];
    }
    return result;
}

four_vec operator*( four_vec p, complex<double> k)
{
    four_vec result;
    for(int i =0; i<4; i++)
    {
        result.inhalt[i]=p.inhalt[i]*k;
    }
    return result;
}

complex<double> operator*(four_vec k, four_vec p)
{
    complex<double> result=0.0;
    for(int i =0; i<4; i++)
    {
        result += k.inhalt[i]*p.inhalt[i];
    }
    return result;
}


#endif // FOURVEC_OPS_H
