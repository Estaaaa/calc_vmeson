#ifndef TOOLHEADER_H
#define TOOLHEADER_H


#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <cstdio>
#include <math.h>
#include <fstream>
#include <complex>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <time.h>
#include <omp.h>

#include "../../Eigen/Dense"
#include "interpolation.hpp"                //CW Spline Interplolation: compile interpolation.cpp as well!
#include "Aids.h"

using namespace Eigen;

#define Pi M_PI

#ifndef ii
#define ii std::complex<double> (0.,1.)
#endif

#ifndef i_
#define i_ std::complex<double> (0.,1.)
#endif

typedef complex<double> dcx;
typedef complex<double> cdouble;
typedef vector<dcx> vecdcx;
typedef vector<double> vecd;
typedef vector<int> vecint;
typedef vector<vecd> vecvecd;
typedef vector<vecint> vecvecint;
typedef vector<vecdcx> vecvecdx;

#include "gaul.h"
#include "fourvec.h"
#include "spline_1D.h"

typedef vector<four_vec> vecfv;


extern four_vec operator+(four_vec k, four_vec p);
extern four_vec operator-(four_vec k, four_vec p);
extern four_vec operator*(double k, four_vec p);
extern four_vec operator*(four_vec p, double k);
extern four_vec operator*(complex<double> k, four_vec p);
extern four_vec operator*( four_vec p, complex<double> k);
extern complex<double> operator*(four_vec k, four_vec p);


#endif // TOOLHEADER_H
