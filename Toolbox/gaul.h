
/*----------------------GAUS-LEGENDRE-------------------------------------------------------------------------*/

#define EPS 3.0e-11
/* Gauß-Legendre Integrationsmethode */
void gauleg(double x1, double x2, double x[], double w[], int n)
{
	int m,j,i;
	double z1,z,xm,xl,pp,p3,p2,p1;
	double xh[n+1],wh[n+1];
	
	m=(n+1)/2;
	xm=0.5*(x2+x1);
	xl=0.5*(x2-x1);
	for (i=1;i<=m;i++) 
	{
		z=cos(3.141592654*(i-0.25)/(n+0.5));		/*Calculating the roots*/
		do {
			p1=1.0;
			p2=0.0;
			for (j=1;j<=n;j++) 
			{
				p3=p2;
				p2=p1;
				p1=((2.0*j-1.0)*z*p2-(j-1.0)*p3)/j; /*Polynom calculation aka Legendre*/
			}
			pp=n*(z*p1-p2)/(z*z-1.0);				/*Derivative of Polynom Pn for the weights w(j)*/
			z1=z;
			z=z1-p1/pp;
		} 
		while (fabs(z-z1) > EPS);
		xh[i]=xm-xl*z;								/*Calculating the weights w(j) and the abscissas x(j)*/
		xh[n+1-i]=xm+xl*z;
		wh[i]=2.0*xl/((1.0-z*z)*pp*pp);				/* wj=2/(1-xj)^2*Pn^2 */
		wh[n+1-i]=wh[i];								/*symetric*/
	}
	
	for (int j=0; j<n; j++)							/*Rewriting index for C++ */
	{
		x[j]=xh[j+1];
		w[j]=wh[j+1];
	}
	
}
#undef EPS


void simpson(double x1, double x2, double x[], double w[], int n)
{
	if(n % 2 == 0){}
	else{n=n+1; cout<<"void:simpson:: the number of grif pionts was motified: n="<<n<<endl;
		cout<<"But that would work becaue of bad alloc. So Aborted"<<endl; return;}

	double h=(x2-x1)/n;

	for (int i = 0; i <n ; ++i)
	{
		x[i]=x1+i*h;
		w[i]=1.0*h;
	}
	w[0]=w[n-1]=0.5*h;

}
