cmake_minimum_required(VERSION 3.5)
project(meson_fast)
set(CMAKE_CXX_STANDARD 14)

######### select options

#set(ON_CLUSTER FALSE)
set(ON_CLUSTER TRUE)
#set(COMPILE_ON_NEW_NODES FALSE)
set(COMPILE_ON_NEW_NODES TRUE)

if(ON_CLUSTER)
    if(COMPILE_ON_NEW_NODES)
        set(bse_libs_build_folder cmake-build-release)
#        set(bse_libs_build_folder cmake-build-debug)
        set(SLEPC_ARCH arch_cluster_complex_cpp2) # new nodes
        set(PETSC_ARCH arch_cluster_complex_cpp2)
        set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -std=gnu++14 -march=native -O3 -fopenmp -fcx-limited-range") # new nodes
        set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -std=gnu++14 -Wall -g -fopenmp")
    else()
        set(bse_libs_build_folder cmake-build-release-old-nodes)
#        set(bse_libs_build_folder cmake-build-debug-old-nodes)
        set(SLEPC_ARCH cluster_old_nodes_test) # old nodes:
        set(PETSC_ARCH cluster_old_nodes_test)
        set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -std=gnu++14 -march=sandybridge -O3 -fopenmp -fcx-limited-range")  # old nodes!
        set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -std=gnu++14 -Wall -g -fopenmp")
    endif()
else()
    set(bse_libs_build_folder cmake-build-release)
#    set(bse_libs_build_folder cmake-build-debug)
    set(SLEPC_ARCH ubuntu_with_mpi_complex2)
    set(PETSC_ARCH ubuntu_with_mpi_complex2)
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -std=c++14 -march=native -O3 -fopenmp")
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -std=gnu++14 -Wall -g -fopenmp")
endif()

set(path_bselibs /home/paul/repos/libs/bselibs/)


# compiler
#set(CMAKE_CXX_COMPILER /usr/local/bin/g++-7)
#set(CMAKE_C_COMPILER /usr/local/bin/gcc-7)
#set(CMAKE_CXX_FLAGS "-std=c++11 -I ${SLEPC_DIR}/lib/slepc/conf/slepc_common")

### slepc infos
set(SLEPC_DIR ${path_bselibs}slepc-3.7.4)
set(PETSC_DIR ${path_bselibs}petsc-3.7.6)
#set(CMAKE_C_COMPILER ${PETSC_DIR}/${PETSC_ARCH}/bin/mpicc)
#set(CMAKE_CXX_COMPILER ${PETSC_DIR}/${PETSC_ARCH}/bin/mpicxx)
find_library(LIBLIBpets petsc PATHS ${PETSC_DIR}  ${PETSC_DIR}/${PETSC_ARCH}/lib REQUIRED)
find_library(LIBLIB slepc PATHS ${SLEPC_DIR}  ${SLEPC_DIR}/${SLEPC_ARCH}/lib REQUIRED)

## -fcx-limited-range If abfragen bei cdoubles fallen weg!
## -ffast-math ist bei Ofast mit drin, vorsicht assoziativ gesetz
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -D_GLIBCXX_DEBUG -Wall -O0 -std=c++14 -g")
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall")
#set(CMAKE_CXX_FLAGS "-std=gnu++14 -O3 -march=native -fopenmp")
#set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -std=gnu++14 -march=native -O3 -fopenmp -Wall")
#set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -std=c++14 -g -march=native -O3 -fopenmp -Wall")
#set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -std=c++14 -march=native -O3 -fopenmp")

#set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -std=gnu++14 -Wall -Og -g")   #### Valgrind!!!
### valgrind --leak-check=full ./meson_fast

#-Ofast  -fcx-limited-range") #  -fopenmp") // Ofast makes it very fast but produces incorrect results!!!
#set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -std=gnu++14 -Ofast -march=native -fcx-limited-range -fopenmp")
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -D_GLIBCXX_DEBUG")# -fopenmp")
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -fopenmp -march=native -O3")


## write output in projecto folder
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}")
# compile with -pg for gprof, resulting executable is cquark
# run the program as usual.
# type  gprof cquark gmon.out > analysis.txt

### manual petsc slepc includes
include_directories(${path_bselibs})
include_directories(${SLEPC_DIR}/include)
include_directories(${SLEPC_DIR}/${SLEPC_ARCH}/include)
include_directories(${PETSC_DIR}/${PETSC_ARCH}/include)
include_directories(${PETSC_DIR}/include)
find_library(LIBLIBpets petsc PATHS ${PETSC_DIR}  ${PETSC_DIR}/${PETSC_ARCH}/lib)
find_library(LIBLIB slepc PATHS ${SLEPC_DIR}  ${SLEPC_DIR}/${SLEPC_ARCH}/lib)

################### My LIB, COPY THIS ANYWHERE #####################
##### paths of .a lib files
## add path to library search path of cmake
list(APPEND CMAKE_PREFIX_PATH ${path_bselibs}${bse_libs_build_folder}/utilities)
list(APPEND CMAKE_PREFIX_PATH ${path_bselibs}${bse_libs_build_folder}/gluon)
list(APPEND CMAKE_PREFIX_PATH ${path_bselibs}${bse_libs_build_folder}/QuarkDse)
list(APPEND CMAKE_PREFIX_PATH ${path_bselibs}${bse_libs_build_folder}/quark)
list(APPEND CMAKE_PREFIX_PATH ${path_bselibs}${bse_libs_build_folder}/bound_state_modules)
## search through lib search path and call the library gluonlib.a Gluon
find_library(Gluon gluonlib REQUIRED)
find_library(Util utilitiesLib REQUIRED)
find_library(Quark Quarklib REQUIRED)
find_library(Dse QuarkDselib REQUIRED)
find_library(Bsm libBoundStateModules REQUIRED)
##### paths for header search path
include_directories(${path_bselibs}/bound_state_modules)
include_directories(${path_bselibs}/gluon)
include_directories(${path_bselibs}/qft_basic/QftObjects)
include_directories(${path_bselibs}/quark)
include_directories(${path_bselibs}/quark_interpolator)
include_directories(${path_bselibs}/QuarkDse)
include_directories(${path_bselibs}/types)
include_directories(${path_bselibs}/utilities)
include_directories(${path_bselibs}/utilities/grids)
include_directories(${path_bselibs}/utilities/integration/Gauleg)
include_directories(${path_bselibs}/utilities/integration/Quadrule)
include_directories(${path_bselibs}/utilities/templates)
include_directories(${path_bselibs}/utilities/useful)
#include_directories(${path_bselibs}/utilities/physics)
include_directories(${path_bselibs}/utilities/eigen_solvers)
include_directories(${path_bselibs})
include_directories(${path_bselibs}/utilities/useful)
include_directories(${path_bselibs}/utilities/interpolation)
include_directories(${path_bselibs}/utilities/interpolation/walter)
include_directories(${path_bselibs}/utilities/interpolation/walter/Interpolation)
include_directories(${path_bselibs}/utilities/interpolation/walter/Global_utility)
include_directories(${path_bselibs}/utilities/interpolation/cauchy)
include_directories(${path_bselibs}/utilities/eigen_solvers)
include_directories(${path_bselibs}/utilities_bse)
include_directories(${path_bselibs}/bound_state_modules)
include_directories(${path_bselibs}/slepc_functions)
include_directories(${path_bselibs}/utilities/root_finders)



###############################
add_subdirectory(source)

set(SOURCE_FILES
     #   ${path_bselibs}slepc_functions/slepc_eigensolver_for_bses.cpp
        source/main.cpp
#        source/main_test.cpp
        source/PhaseSpace.cpp
#        source/calc_kernels.cpp
        source/sis_meson.cpp
       # quark_speed_check/quark_speed_check.h source/MesonBse.cpp source/MesonBse.h source/ParametersBse.cpp source/ParametersBse.h
       source/MesonBse.cpp
        source/pions_decay.cpp
        source/tscheb.cpp
        )

add_executable(meson_fast ${SOURCE_FILES})

target_link_libraries(meson_fast
       # ${LIBLIB}
       # ${LIBLIBpets}
        ${Dse}
        ${Gluon}
        ${Quark}
        ${Bsm}
        ${Util}
        kernel_lib
        )
