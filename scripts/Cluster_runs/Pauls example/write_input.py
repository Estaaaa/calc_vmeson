import shutil
import os
import subprocess

## search and replace
def submit_job(input_array, input_vars) :

    print("input is: ", input_array)

    file = open("constants_model.txt","r")
    cont = file.read()

    for i in range(len(input_vars)) :
        cont=cont.replace( input_vars[i], input_array[i])

    ## dump to file
    outFile = open('constants.h',"w")
    outFile.write(cont)
    outFile.close()

    shutil.move('./constants.h', '../source/constants.h')

    batcmd='./run_on_cluster.sh'
    os.system(batcmd)
    print( 'submitted!')
    # result = subprocess.check_output(batcmd, shell=True)
    # print(result)


# input variables I want to replace are:
input_vars = [ 'var_nll', 'var_nyl', 'var_nzl', 'var_nspir', 'var_path']

myinput    = [
    ['64', '16', '16', '16', 'GE_PATH']
    ['16', '8', '8', '8', 'RW_PATH'],
    ['64', '16', '32', '16', 'GE_PATH'],
    ['64', '16', '48', '16', 'GE_PATH'],
    # ['64', '16', '64', '16', 'GE_PATH']
]
# GE_PATH NICOS_PATH or nothing for my path

# check I didn't forget arguments
for i in range(len(myinput)) :
    assert len(input_vars) == len(myinput[i])


for inp in myinput : submit_job( inp, input_vars)