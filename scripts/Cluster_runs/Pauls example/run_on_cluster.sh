#!/usr/bin/env bash

### move const.h to ouptut folder
dt=$(date '+%d_%m_%Y_%H_%M_%S_%N');
dirname=$dt;
cd ../output;
mkdir $dt;
cd ../source;
cp constants.h ../output/$dt/constants.h  # manipulate later for tetraquark project
cp main.cpp ../output/$dt/main.cpp
cd ../;

### build
#rm -r cmake-build-release
mkdir cmake-build-release
cd cmake-build-release
cmake -DCMAKE_BUILD_TYPE=Release ../
make
cd ../

#### copy the executable into output folder so it can run there
cp scripts/slurm_submit.sh output/$dt/slurm_submit.sh;
mv meson_fast output/$dt/meson_fast
cd output/$dt;
mkdir output;
pwd

### submit a job for this executable on the cluster
sbatch slurm_submit.sh

#return $dt