#!/bin/bash

#SBATCH --job-name=decay		## Name des Jobs.
#SBATCH --output=output_file		## Ausgabe (stdout) wird in die Datei "output_file" geschrieben. Da was sinnvollen hinschreiben um spaeter verschiedene Ausgaben unterscheiden zu koennen.
#SBATCH --nodes=1			## Verwende einen Node (fuer mehrere Nodes muesste man mit MPI arbeiten).
#SBATCH --constraint=new		## new = neue Nodes (24 Kerne), ghost = alte Nodes (16 Kerne).
##SBATCH --constraint=old		## new = neue Nodes (24 Kerne), ghost = alte Nodes (16 Kerne).

#SBATCH --ntasks=1			## Ein Job auf einem Node.
#SBATCH --cpus-per-task=24		## Anzahl der Kerne pro Job festlegen. new nodes
##SBATCH --cpus-per-task=16		## Anzahl der Kerne pro Job festlegen. old nodes fast
##SBATCH --cpus-per-task=8		## Anzahl der Kerne pro Job festlegen. old nodes cheat

# Ob man das wirklich braucht weiss ich nicht genau (laut wiki schon). Setze ich immer auf die gleiche Anzahl wie cpus-per-task.
export OMP_NUM_THREADS=24 # new fast
#export OMP_NUM_THREADS=16 # old fast
#export OMP_NUM_THREADS=8 # old cheat

# Hier dann was ausgefuehrt werden soll:
./meson_fast
