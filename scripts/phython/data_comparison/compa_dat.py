# -*- coding: utf-8 -*-

from default_imports import *
from scipy.interpolate import UnivariateSpline

# ------------------------------------------------

x_data = np.genfromtxt('data_test/F[0]_formfactor.txt', usecols = 0)
y_data = np.genfromtxt('data_test/F[0]_formfactor.txt', usecols = 4)
#y_error1 = np.genfromtxt('../Data/condensate_lattice_jhep2010-73.dat', usecols = 2)

#x_data2 = 1e3 * np.genfromtxt('../Data/order_parameter_mul_0.00_mus_0.00_d1_8.57.dat', usecols = 0)
#y_data2 = np.genfromtxt('../Data/order_parameter_mul_0.00_mus_0.00_d1_8.57.dat', usecols = 4)
y_data2_spline = UnivariateSpline(x_data, y_data, k = 3, s = 0)

x_data2_spline = np.linspace(0.0, 1.0, 192)

# ------------------------------------------------

my_color_blue='5E8CD1'

plt.figure(figsize=(3.0, 2.3))
plt.xlabel(r'$q^2$ [MeV]')
plt.ylabel(r'$F_{\mathcal{S}\gamma\gamma}$')

plt.plot(x_data2_spline, y_data2_spline(x_data2_spline), color = 'steelblue', linestyle = '-', zorder = 0)
plt.plot(x_data, y_data, color = 'steelblue', linestyle = '',marker='x',markersize=2.0, label = r'scalar', zorder = 1)

#plt.errorbar(x_data1, y_data1, y_error1, color = 'r', marker = 'o', markersize = 2.2, markeredgewidth = 0.7,
#			 linestyle = 'none', label = r'Lattice QCD', zorder = 0)

plt.xlim(0.0,1.0)
plt.ylim(0.0, 1.1)

plt.locator_params(axis = 'x', nbins = 6)
plt.legend(loc = 'upper right', frameon = False).get_frame().set_linewidth(0.7)
plt.tight_layout(0.3)
plt.savefig('test.pdf')
