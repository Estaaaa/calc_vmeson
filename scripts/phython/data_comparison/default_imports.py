# -*- coding: utf-8 -*-

import matplotlib as mpl

mpl.use('pgf')

_fontsize = 10

_custom_pgf_settings = {
	'axes.linewidth' : 0.7,
	'axes.autolimit_mode' : 'round_numbers',
	'axes.xmargin' : 0,
	'axes.ymargin' : 0,
	'errorbar.capsize' : 1.5,
	'font.family' : 'serif',
	'font.size' : _fontsize,
	'lines.linewidth' : 0.7,
	'legend.fancybox' : False,
	'legend.framealpha' : 1.0,
	'legend.edgecolor' : 'inherit',
	'legend.fontsize' : _fontsize - 2,
	'legend.borderpad' : 0.3,
	'legend.borderaxespad' : 0.5,
	'legend.labelspacing' : 0.4,
	'legend.handlelength' : 1.8,
	'legend.handletextpad' : 0.5,
	'legend.numpoints' : 1,
	'pgf.texsystem' : 'pdflatex',
	'pgf.preamble' : [
		r'\usepackage[T1]{fontenc}',
		r'\usepackage[utf8]{inputenc}',
		r'\usepackage{mathtools}',
		r'\usepackage{lmodern}',
		r'\usepackage[final]{microtype}',
	],
	'xtick.top' : False,
	'xtick.direction' : 'out',
	'xtick.major.size' : 3,
	'xtick.minor.size' : 2,
	'xtick.major.width' : 0.7,
	'xtick.minor.width' : 0.7,
	'xtick.major.pad' : 3,
	'xtick.minor.pad' : 3,
	'xtick.labelsize' : _fontsize,
	'ytick.right' : False,
	'ytick.direction' : 'out',
	'ytick.major.size' : 3,
	'ytick.minor.size' : 2,
	'ytick.major.width' : 0.7,
	'ytick.minor.width' : 0.7,
	'ytick.major.pad' : 3,
	'ytick.minor.pad' : 3,
	'ytick.labelsize' : _fontsize
}

mpl.rcParams.update(_custom_pgf_settings)

import numpy as np
import matplotlib.pyplot as plt
