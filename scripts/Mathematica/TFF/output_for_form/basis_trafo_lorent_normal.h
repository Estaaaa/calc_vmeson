// Matrix relating basis elements and Lorentz projectors
 
#include<typedefs.h>
#include<routing_amount_scalars.h>
#include<extra_def.h>
#include<tff_kernel_scalar_amp_lorenz.h>
#include<fv.h>


void realting_f_with_lorenz(VecCdoub& fprime, const fv<Cdoub>& p1, const fv<Cdoub>& p2, VecCdoub& f){

fprime[0]=(f[2]*((p1*p1) - (p1*p2) + (p2*p2)))/(-pow((p1*p2),2.0) + (p1*p1)*(p2*p2)) + (f[4]*(p1*p2))/(-2.0*pow((p1*p2),2.0) + 2.0*(p1*p1)*(p2*p2)) + (f[1]*((p1*p1) + (p1*p2) + (p2*p2)))/(-4.0*pow((p1*p2),2.0) + 4.0*(p1*p1)*(p2*p2)) + (f[3]*(pow((p1*p1),2.0) + pow((p2*p2),2.0)))/(4.0*pow((p1*p2),2.0) - 4.0*(p1*p1)*(p2*p2)); 

fprime[1]=f[1]/(4.0*pow((p1*p2),2.0) - 4.0*(p1*p1)*(p2*p2)) + f[4]/(2.0*pow((p1*p2),2.0) - 2.0*(p1*p1)*(p2*p2)) + (f[3]*(p1*p2))/(2.0*pow((p1*p2),2.0) - 2.0*(p1*p1)*(p2*p2)) + f[2]/(-pow((p1*p2),2.0) + (p1*p1)*(p2*p2)); 

fprime[2]=f[1]/(4.0*pow((p1*p2),2.0) - 4.0*(p1*p1)*(p2*p2)) + f[2]/(pow((p1*p2),2.0) - (p1*p1)*(p2*p2)) + (f[3]*((p1*p1) + (p2*p2)))/(-4.0*pow((p1*p2),2.0) + 4.0*(p1*p1)*(p2*p2)); 

fprime[3]=-(f[4]/(2.0*pow((p1*p2),2.0) - 2.0*(p1*p1)*(p2*p2))) + (3.0*f[3]*(p1*p2)*pow((p1*p1) - (p2*p2),2.0))/(8.0*pow(pow((p1*p2),2.0) - (p1*p1)*(p2*p2),2.0)) + (f[0]*(p1*p2))/(-2.0*pow((p1*p2),2.0) + 2.0*(p1*p1)*(p2*p2)) + (f[2]*((p1*p2)*(4.0*(p1*p2) - 3.0*(p2*p2)) + (p1*p1)*(-3.0*(p1*p2) + 2.0*(p2*p2))))/(2.*pow(pow((p1*p2),2.0) - (p1*p1)*(p2*p2),2.0)) - (f[1]*((p1*p1)*(3.0*(p1*p2) + 2.0*(p2*p2)) + (p1*p2)*(4.0*(p1*p2) + 3.0*(p2*p2))))/(8.0*pow(pow((p1*p2),2.0) - (p1*p1)*(p2*p2),2.0)); 

fprime[4]=-(f[0]/(2.0*pow((p1*p2),2.0) - 2.0*(p1*p1)*(p2*p2))) - (3.0*f[2]*((p1*p1) - 2.0*(p1*p2) + (p2*p2)))/(2.*pow(pow((p1*p2),2.0) - (p1*p1)*(p2*p2),2.0)) - (3.0*f[1]*((p1*p1) + 2.0*(p1*p2) + (p2*p2)))/(8.0*pow(pow((p1*p2),2.0) - (p1*p1)*(p2*p2),2.0)) + (f[3]*(3.0*pow((p1*p1),2.0) - 4.0*pow((p1*p2),2.0) - 2.0*(p1*p1)*(p2*p2) + 3.0*pow((p2*p2),2.0)))/(8.0*pow(pow((p1*p2),2.0) - (p1*p1)*(p2*p2),2.0)); 


 
}