#!/bin/bash

\cp output_scalarP/routing_scalar_products.cpp.txt ../../../source/bse/Routing/routing_scalar_products.cpp

\cp output_scalarP/routing_scalar_products.h.txt ../../../source/bse/Routing/routing_scalar_products.h

\cp output_scalarP/routing_amount_scalars.h.txt ../../../source/bse/Routing/routing_amount_scalars.h

\cp output_scalarP/dependence_of_all_kernels.h.txt ../../../source/bse/Routing/dependence_of_all_kernels.h
