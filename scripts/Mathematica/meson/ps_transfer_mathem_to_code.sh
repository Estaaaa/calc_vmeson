#!/bin/bash

\cp output_meson_ps/dependence_of_all_kernels.h.txt ../../../source/bse/Routing/dependence_of_all_kernels.h


\cp output_meson_ps/meson_ps.h.txt ../../../source/bse/Kernel/ps/meson_ps.h
\cp output_meson_ps/meson_ps_Renorm.h.txt ../../../source/bse/Kernel/ps/meson_ps_Renorm.h
\cp output_meson_ps/meson_ps_kernelall.h.txt ../../../source/bse/Kernel/ps/meson_ps_kernel_all.h
\cp ../../form_code/my/meson_ps/output/meson_ps_fpi.h.txt ../../../source/bse/Kernel/ps/meson_ps_fpi.h

\cp ../../form_code/my/meson_ps/output/meson_ps_Renorm_fv.h.txt ../../../source/bse/Kernel/ps/meson_ps_Renorm_fv.h
