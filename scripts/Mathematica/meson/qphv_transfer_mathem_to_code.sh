#!/bin/bash

#\cp output_scalarP/dependence_of_all_kernels.h.txt ../../source/bse/Routing/dependence_of_all_kernels.h


\cp output_meson_qphv/meson_qphv.h.txt ../../../source/bse/Kernel/qphv_all/qphv/meson_qphv.h
\cp output_meson_qphv/meson_qphv_Renorm.h.txt ../../../source/bse/Kernel/qphv_all/qphv/meson_qphv_Renorm.h
\cp output_meson_qphv/meson_qphv_kernelall.h.txt ../../../source/bse/Kernel/qphv_all/qphv/meson_qphv_kernel_all.h
\cp output_meson_qphv/meson_qphv_inhomogenous.h.txt ../../../source/bse/Kernel/qphv_all/qphv/meson_qphv_inhomogenous.h
\cp ../../form_code/my/meson_qphv/output/meson_qphv_fpi.h.txt ../../../source/bse/Kernel/qphv_all/qphv/meson_qphv_fpi.h

\cp ../../form_code/my/meson_qphv/output/meson_qphv_Renorm_fv.h.txt ../../../source/bse/Kernel/qphv_all/qphv//meson_qphv_Renorm_fv.h
