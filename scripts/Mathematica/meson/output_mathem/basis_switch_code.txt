new_vector({0,i,j}) = (-scalar.meson({0,i,j}) + vector.meson({0,i,j}) + vector.meson({5,i,j})*pow(scalars[34],2))/scalars[35] ; 

new_vector({1,i,j}) = (2.0*scalar.meson({3,i,j}) + vector.meson({2,i,j}) - 2.0*vector.meson({7,i,j}))/scalars[35] ; 

new_vector({2,i,j}) = -vector.meson({1,i,j}) ; 

new_vector({3,i,j}) = -2.0*vector.meson({3,i,j}) ; 

new_vector({4,i,j}) = (scalar.meson({2,i,j}) - vector.meson({4,i,j}))/scalars[35] ; 

new_vector({5,i,j}) = -((scalar.meson({1,i,j}) + vector.meson({6,i,j}))/scalars[35]) ; 

new_vector({6,i,j}) = -vector.meson({5,i,j}) ; 

new_vector({7,i,j}) = -2.0*scalar.meson({3,i,j}) + 2.0*vector.meson({7,i,j}) ; 
