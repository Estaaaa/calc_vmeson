#!/bin/bash

#\cp output_scalarP/dependence_of_all_kernels.h.txt ../../source/bse/Routing/dependence_of_all_kernels.h


\cp output_meson_qphv_trans/meson_qphv_trans.h.txt ../../../source/bse/Kernel/qphv_trans/meson_qphv_trans.h
\cp output_meson_qphv_trans/meson_qphv_trans_Renorm.h.txt ../../../source/bse/Kernel/qphv_trans/meson_qphv_trans_Renorm.h
\cp output_meson_qphv_trans/meson_qphv_trans_kernelall.h.txt ../../../source/bse/Kernel/qphv_trans/meson_qphv_trans_kernel_all.h
\cp output_meson_qphv_trans/meson_qphv_trans_inhomogenous.h.txt ../../../source/bse/Kernel/qphv_trans/meson_qphv_trans_inhomogenous.h
\cp ../../form_code/my/meson_qphv_trans/output/meson_qphv_trans_fpi.h.txt ../../../source/bse/Kernel/qphv_trans/meson_qphv_trans_fpi.h

\cp ../../form_code/my/meson_qphv_trans/output/meson_qphv_trans_Renorm_fv.h.txt ../../../source/bse/Kernel/qphv_trans/meson_qphv_trans_Renorm_fv.h
