#!/bin/bash

#\cp output_meson_ve/dependence_of_all_kernels.h.txt ../../source/bse/Routing/dependence_of_all_kernels.h


\cp output_meson_ve/meson_ve.h.txt ../../../source/bse/Kernel/ve/meson_ve.h
\cp output_meson_ve/meson_ve_Renorm.h.txt ../../../source/bse/Kernel/ve/meson_ve_Renorm.h
\cp output_meson_ve/meson_ve_kernelall.h.txt ../../../source/bse/Kernel/ve/meson_ve_kernel_all.h
\cp ../../form_code/my/meson_ve/output/meson_ve_fpi.h.txt ../../../source/bse/Kernel/ve/meson_ve_fpi.h

\cp ../../form_code/my/meson_ve/output/meson_ve_Renorm_fv.h.txt ../../../source/bse/Kernel/ve/meson_ve_Renorm_fv.h
