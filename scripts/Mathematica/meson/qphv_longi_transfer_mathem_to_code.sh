#!/bin/bash

#\cp output_scalarP/dependence_of_all_kernels.h.txt ../../source/bse/Routing/dependence_of_all_kernels.h


\cp output_meson_qphv_longi/meson_qphv_longi.h.txt ../../../source/bse/Kernel/qphv_all/qphv_longi/meson_qphv_longi.h
\cp output_meson_qphv_longi/meson_qphv_longi_Renorm.h.txt ../../../source/bse/Kernel/qphv_all/qphv_longi/meson_qphv_longi_Renorm.h
\cp output_meson_qphv_longi/meson_qphv_longi_kernelall.h.txt ../../../source/bse/Kernel/qphv_all/qphv_longi/meson_qphv_longi_kernel_all.h
#\cp output_meson_qphv_longi/meson_qphv_longi_inhomogenous.h.txt ../../../source/bse/Kernel/qphv_all//qphv_longi/meson_qphv_longi_inhomogenous.h
\cp ../../form_code/my/meson_qphv_longi/output/meson_qphv_longi_fpi.h.txt ../../../source/bse/Kernel/qphv_all/qphv_longi/meson_qphv_longi_fpi.h

\cp ../../form_code/my/meson_qphv_longi/output/meson_qphv_longi_Renorm_fv.h.txt ../../../source/bse/Kernel/qphv_all/qphv_longi/meson_qphv_longi_Renorm_fv.h
