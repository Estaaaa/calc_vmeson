******* Substitution for the Base **********
 
id ConjugateAmplitude(1, mu?, q?, P?) = gi_(1)*(i_*g_(1,mu)); 
id ConjugateAmplitude(2, mu?, q?, P?) = gi_(1)*(i_*g_(1,q)*q(mu)); 
id ConjugateAmplitude(3, mu?, q?, P?) = gi_(1)*(q(mu)); 
id ConjugateAmplitude(4, mu?, q?, P?) = gi_(1)*(i_*(-g_(1,q)*g_(1,mu)+g_(1,mu)*g_(1,q))); 
id ConjugateAmplitude(5, mu?, q?, P?) = gi_(1)*((-i_)*(-(P.P*g_(1,mu))+g_(1,P)*P(mu))); 
id ConjugateAmplitude(6, mu?, q?, P?) = gi_(1)*((q.P*(P.P*(g_(1,q)*g_(1,mu)-g_(1,mu)*g_(1,q))+(g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P))*P(mu)))/2); 
id ConjugateAmplitude(7, mu?, q?, P?) = gi_(1)*((g_(1,P)*g_(1,mu)-g_(1,mu)*g_(1,P))/2); 
id ConjugateAmplitude(8, mu?, q?, P?) = gi_(1)*((-i_/6)*(g_(1,P)*g_(1,q)*g_(1,mu)-g_(1,P)*g_(1,mu)*g_(1,q)-g_(1,q)*g_(1,P)*g_(1,mu)+g_(1,q)*g_(1,mu)*g_(1,P)+g_(1,mu)*g_(1,P)*g_(1,q)-g_(1,mu)*g_(1,q)*g_(1,P))); 
id ConjugateAmplitude(9, mu?, q?, P?) = gi_(1)*(q.P*P(mu)-P.P*q(mu)); 
id ConjugateAmplitude(10, mu?, q?, P?) = gi_(1)*(i_*g_(1,q)*(-(q.P*P(mu))+P.P*q(mu))); 
id ConjugateAmplitude(11, mu?, q?, P?) = gi_(1)*(i_*q.P*(q.P*g_(1,mu)-g_(1,P)*q(mu))); 
id ConjugateAmplitude(12, mu?, q?, P?) = gi_(1)*((q.P*(g_(1,q)*g_(1,mu)-g_(1,mu)*g_(1,q))+(g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P))*q(mu))/2); 

 
id Amplitude(1, mu?, q?, P?) = gi_(1)*(i_*g_(1,mu)); 
id Amplitude(2, mu?, q?, P?) = gi_(1)*(i_*g_(1,q)*q(mu)); 
id Amplitude(3, mu?, q?, P?) = gi_(1)*(q(mu)); 
id Amplitude(4, mu?, q?, P?) = gi_(1)*(i_*(-g_(1,q)*g_(1,mu)+g_(1,mu)*g_(1,q))); 
id Amplitude(5, mu?, q?, P?) = gi_(1)*((-i_)*(-(P.P*g_(1,mu))+g_(1,P)*P(mu))); 
id Amplitude(6, mu?, q?, P?) = gi_(1)*((q.P*(P.P*(g_(1,q)*g_(1,mu)-g_(1,mu)*g_(1,q))+(g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P))*P(mu)))/2); 
id Amplitude(7, mu?, q?, P?) = gi_(1)*((g_(1,P)*g_(1,mu)-g_(1,mu)*g_(1,P))/2); 
id Amplitude(8, mu?, q?, P?) = gi_(1)*((-i_/6)*(g_(1,P)*g_(1,q)*g_(1,mu)-g_(1,P)*g_(1,mu)*g_(1,q)-g_(1,q)*g_(1,P)*g_(1,mu)+g_(1,q)*g_(1,mu)*g_(1,P)+g_(1,mu)*g_(1,P)*g_(1,q)-g_(1,mu)*g_(1,q)*g_(1,P))); 
id Amplitude(9, mu?, q?, P?) = gi_(1)*(q.P*P(mu)-P.P*q(mu)); 
id Amplitude(10, mu?, q?, P?) = gi_(1)*(i_*g_(1,q)*(-(q.P*P(mu))+P.P*q(mu))); 
id Amplitude(11, mu?, q?, P?) = gi_(1)*(i_*q.P*(q.P*g_(1,mu)-g_(1,P)*q(mu))); 
id Amplitude(12, mu?, q?, P?) = gi_(1)*((q.P*(g_(1,q)*g_(1,mu)-g_(1,mu)*g_(1,q))+(g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P))*q(mu))/2); 

 
id Projector(1, mu?, q?, P?) = gi_(1)*((-i_)*(-(q.q*g_(1,P))+q.P*g_(1,q))*P(mu)); 
id Projector(2, mu?, q?, P?) = gi_(1)*(i_*(-(q.P*g_(1,P))+P.P*g_(1,q))*P(mu)); 
id Projector(3, mu?, q?, P?) = gi_(1)*(P(mu)); 
id Projector(4, mu?, q?, P?) = gi_(1)*((-i_)*(g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P))*P(mu)); 
id Projector(5, mu?, q?, P?) = gi_(1)*(i_*((q.q)^2*(-(P.P*g_(1,mu))+3*g_(1,P)*P(mu))+2*(q.P)^2*g_(1,q)*q(mu)+q.q*((q.P)^2*g_(1,mu)+P.P*g_(1,q)*q(mu)-3*q.P*(g_(1,q)*P(mu)+g_(1,P)*q(mu))))); 
id Projector(6, mu?, q?, P?) = gi_(1)*(-(((q.P)^2-P.P*q.q)*(-g_(1,q)*g_(1,mu)+g_(1,mu)*g_(1,q)))+g_(1,q)*g_(1,P)*(3*q.q*P(mu)-3*q.P*q(mu))+3*g_(1,P)*g_(1,q)*(-(q.q*P(mu))+q.P*q(mu))); 
id Projector(7, mu?, q?, P?) = gi_(1)*(q.q*(g_(1,P)*g_(1,mu)-g_(1,mu)*g_(1,P))+q.P*(-g_(1,q)*g_(1,mu)+g_(1,mu)*g_(1,q))+(-g_(1,P)*g_(1,q)+g_(1,q)*g_(1,P))*q(mu)); 
id Projector(8, mu?, q?, P?) = gi_(1)*(i_*(g_(1,P)*g_(1,q)*g_(1,mu)-g_(1,P)*g_(1,mu)*g_(1,q)-g_(1,q)*g_(1,P)*g_(1,mu)+g_(1,q)*g_(1,mu)*g_(1,P)+g_(1,mu)*g_(1,P)*g_(1,q)-g_(1,mu)*g_(1,q)*g_(1,P))); 
id Projector(9, mu?, q?, P?) = gi_(1)*(-(q.q*P(mu))+q.P*q(mu)); 
id Projector(10, mu?, q?, P?) = gi_(1)*((-i_)*(g_(1,q)*(-(((q.P)^2+2*P.P*q.q)*P(mu))+3*P.P*q.P*q(mu))+q.P*(((q.P)^2-P.P*q.q)*g_(1,mu)+g_(1,P)*(3*q.q*P(mu)-3*q.P*q(mu))))); 
id Projector(11, mu?, q?, P?) = gi_(1)*(i_*q.q*(q.P*(P.P*g_(1,mu)-3*g_(1,P)*P(mu))+2*P.P*g_(1,P)*q(mu))-i_*q.P*((q.P)^2*g_(1,mu)+3*P.P*g_(1,q)*q(mu)-q.P*(3*g_(1,q)*P(mu)+g_(1,P)*q(mu)))); 
id Projector(12, mu?, q?, P?) = gi_(1)*(((q.P)^2-P.P*q.q)*(-g_(1,P)*g_(1,mu)+g_(1,mu)*g_(1,P))+g_(1,P)*g_(1,q)*(3*q.P*P(mu)-3*P.P*q(mu))+3*g_(1,q)*g_(1,P)*(-(q.P*P(mu))+P.P*q(mu))); 

 
