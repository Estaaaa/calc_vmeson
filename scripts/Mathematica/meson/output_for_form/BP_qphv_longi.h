******* Substitution for the Base **********
 
id ConjugateAmplitude(1, mu?, q?, P?) = gi_(1)*(i_*g_(1,P)); 
id ConjugateAmplitude(2, mu?, q?, P?) = gi_(1)*(i_*q.P*g_(1,q)); 
id ConjugateAmplitude(3, mu?, q?, P?) = gi_(1)*(q.P); 
id ConjugateAmplitude(4, mu?, q?, P?) = gi_(1)*(q.P*(g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P))); 

 
id Amplitude(1, mu?, q?, P?) = gi_(1)*(i_*g_(1,P)); 
id Amplitude(2, mu?, q?, P?) = gi_(1)*(i_*q.P*g_(1,q)); 
id Amplitude(3, mu?, q?, P?) = gi_(1)*(q.P); 
id Amplitude(4, mu?, q?, P?) = gi_(1)*(q.P*(g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P))); 

 
id Projector(1, mu?, q?, P?) = gi_(1)*(i_*(q.q*g_(1,P)-q.P*g_(1,q))); 
id Projector(2, mu?, q?, P?) = gi_(1)*(i_*(-(q.P*g_(1,P))+P.P*g_(1,q))); 
id Projector(3, mu?, q?, P?) = gi_(1)*(1); 
id Projector(4, mu?, q?, P?) = gi_(1)*(g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P)); 

 
