******* Declarations for the Base **********
 *Non-commuting functions
F Basis, Projector 
 
******* Substitution for the Base **********
 
id Basis(0, mu?, P?, q?) = i_*(g_(0,mu)-(g_(0,P)*P(mu))/P.P); 
id Basis(1, mu?, P?, q?) = g_(0,mu)*g_(0,P)-P(mu); 
id Basis(2, mu?, P?, q?) = (q.P*((-q.P+g_(0,P)*g_(0,q))*P(mu)+P.P*(-g_(0,mu)*g_(0,q)+q(mu))))/P.P; 
id Basis(3, mu?, P?, q?) = i_*(g_(0,mu)*g_(0,P)*g_(0,q)-g_(0,mu)*g_(0,q)*g_(0,P)-2*g_(0,q)*P(mu)+2*g_(0,P)*q(mu)); 
id Basis(4, mu?, P?, q?) = -((q.P*P(mu))/P.P)+q(mu); 
id Basis(5, mu?, P?, q?) = (i_*q.P*g_(0,P)*(-(q.P*P(mu))+P.P*q(mu)))/P.P; 
id Basis(6, mu?, P?, q?) = (-i_)*g_(0,q)*(-((q.P*P(mu))/P.P)+q(mu)); 
id Basis(7, mu?, P?, q?) = ((g_(0,P)*g_(0,q)-g_(0,q)*g_(0,P))*(-(q.P*P(mu))+P.P*q(mu)))/P.P; 

 
id Projector(0, mu?, P?, q?) = ((-i_/8)*(((q.P)^2-P.P*q.q)*g_(0,mu)+g_(0,q)*(-(q.P*P(mu))+P.P*q(mu))+g_(0,P)*(q.q*P(mu)-q.P*q(mu))))/((q.P)^2-P.P*q.q); 
id Projector(1, mu?, P?, q?) = 2*q.P*(q.P*P(mu)+P.P*(g_(0,mu)*g_(0,q)-q(mu)))+g_(0,q)*g_(0,P)*(-(q.P*P(mu))+P.P*q(mu))-g_(0,P)*g_(0,q)*(q.P*P(mu)+P.P*q(mu)))/(16*P.P*(-(q.P)^2+P.P*q.q)); 
id Projector(2, mu?, P?, q?) = P.P*(-g_(0,mu)*g_(0,q)+q(mu)))/(8*((q.P)^3-P.P*q.P*q.q)); 
id Projector(3, mu?, P?, q?) = ((-i_/32)*(g_(0,mu)*g_(0,P)*g_(0,q)-g_(0,mu)*g_(0,q)*g_(0,P)-2*g_(0,q)*P(mu)+2*g_(0,P)*q(mu)))/((q.P)^2-P.P*q.q); 
id Projector(4, mu?, P?, q?) = (-(q.P*P(mu))+P.P*q(mu))/(-4*(q.P)^2+4*P.P*q.q); 
id Projector(5, mu?, P?, q?) = ((i_/8)*(-(q.q*(q.P*(P.P*g_(0,mu)-3*g_(0,P)*P(mu))+2*P.P*g_(0,P)*q(mu)))+q.P*((q.P)^2*g_(0,mu)+3*P.P*g_(0,q)*q(mu)-q.P*(3*g_(0,q)*P(mu)+g_(0,P)*q(mu)))))/(q.P*((q.P)^2-P.P*q.q)^2); 
id Projector(6, mu?, P?, q?) = ((i_/8)*(P.P*((q.P)^2-P.P*q.q)*g_(0,mu)+3*P.P*g_(0,q)*(-(q.P*P(mu))+P.P*q(mu))+g_(0,P)*((2*(q.P)^2+P.P*q.q)*P(mu)-3*P.P*q.P*q(mu))))/((q.P)^2-P.P*q.q)^2; 
id Projector(7, mu?, P?, q?) = 3*(-g_(0,P)*g_(0,q)+g_(0,q)*g_(0,P))*(-(q.P*P(mu))+P.P*q(mu)))/(32*((q.P)^2-P.P*q.q)^2); 
