#!/bin/bash

#cp output_meson_axial/dependence_of_all_kernels.h.txt ../../../source/bse/Routing/dependence_of_all_kernels.h


\cp output_meson_axial/meson_axial.h.txt ../../../source/bse/Kernel/axial/meson_axial.h
\cp output_meson_axial/meson_axial_Y.h.txt ../../../source/bse/Kernel/axial/meson_axial_Y.h
\cp output_meson_axial/meson_axial_Renorm.h.txt ../../../source/bse/Kernel/axial/meson_axial_Renorm.h
\cp output_meson_axial/meson_axial_kernelall.h.txt ../../../source/bse/Kernel/axial/meson_axial_kernel_all.h
#\cp ../../form_code/my/meson_axial/output/meson_axial_fpi.h.txt ../../../source/bse/Kernel/axial/meson_axial_fpi.h

\cp ../../form_code/my/meson_axial/output/meson_axial_Renorm_fv.h.txt ../../../source/bse/Kernel/axial/meson_axial_Renorm_fv.h
