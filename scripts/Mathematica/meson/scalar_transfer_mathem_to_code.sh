#!/bin/bash

#cp output_meson_scalar/dependence_of_all_kernels.h.txt ../../../source/bse/Routing/dependence_of_all_kernels.h


\cp output_meson_scalar/meson_scalar.h.txt ../../../source/bse/Kernel/scalar/meson_scalar.h
\cp output_meson_scalar/meson_scalar_Y.h.txt ../../../source/bse/Kernel/scalar/meson_scalar_Y.h
\cp output_meson_scalar/meson_scalar_Renorm.h.txt ../../../source/bse/Kernel/scalar/meson_scalar_Renorm.h
\cp output_meson_scalar/meson_scalar_kernelall.h.txt ../../../source/bse/Kernel/scalar/meson_scalar_kernel_all.h
#\cp ../../form_code/my/meson_scalar/output/meson_scalar_fpi.h.txt ../../../source/bse/Kernel/scalar/meson_scalar_fpi.h

\cp ../../form_code/my/meson_scalar/output/meson_scalar_Renorm_fv.h.txt ../../../source/bse/Kernel/scalar/meson_scalar_Renorm_fv.h
