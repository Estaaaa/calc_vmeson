#pragma once 

 #include "typedefs.h"
#include <array> 

#define K_N_SCALAR_PRODUCTS  45 
  #include "const_bse.h"   
   

namespace quark_p1 {
const int n_sphericals = 2; 
//const std::array<int,2> partial_indices = {K_N_LSQUARED, K_N_ZL};
const std::array<int,2> depends_on_sphericals = {2, 3};
const std::array<int,1> depends_on_scalar_products = {2};
}

namespace quark_p2 {
const int n_sphericals = 2; 
//const std::array<int,2> partial_indices = {K_N_LSQUARED, K_N_ZL};
const std::array<int,2> depends_on_sphericals = {2, 3};
const std::array<int,1> depends_on_scalar_products = {9};
}

namespace quark_p1_norm {
const int n_sphericals = 2; 
//const std::array<int,2> partial_indices = {K_N_PSQUARED, K_N_ZP};
const std::array<int,2> depends_on_sphericals = {0, 1};
const std::array<int,1> depends_on_scalar_products = {5};
}

namespace quark_p2_norm {
const int n_sphericals = 2; 
//const std::array<int,2> partial_indices = {K_N_PSQUARED, K_N_ZP};
const std::array<int,2> depends_on_sphericals = {0, 1};
const std::array<int,1> depends_on_scalar_products = {14};
}

namespace gluon {
const int n_sphericals = 5; 
//const std::array<int,5> partial_indices = {K_N_PSQUARED, K_N_ZP, K_N_LSQUARED, K_N_ZL, K_N_YL};
const std::array<int,5> depends_on_sphericals = {0, 1, 2, 3, 4};
const std::array<int,1> depends_on_scalar_products = {0};
}

namespace rotation_matrix {
const int n_sphericals = 2; 
//const std::array<int,2> partial_indices = {K_N_LSQUARED, K_N_ZL};
const std::array<int,2> depends_on_sphericals = {2, 3};
const std::array<int,10> depends_on_scalar_products = {2, 7, 9, 29, 31, 35, 37, 39, 43, 44};
}

namespace rotation_matrix_2 {
const int n_sphericals = 2; 
//const std::array<int,2> partial_indices = {K_N_LSQUARED, K_N_ZL};
const std::array<int,2> depends_on_sphericals = {2, 3};
const std::array<int,8> depends_on_scalar_products = {35, 43, 44, 31, 39, 29, 37, 7};
}

namespace Lmat {
const int n_sphericals = 4; 
//const std::array<int,4> partial_indices = {K_N_PSQUARED, K_N_ZP, K_N_LSQUARED, K_N_ZL};
const std::array<int,4> depends_on_sphericals = {0, 1, 2, 3};
const std::array<int,5> depends_on_scalar_products = {35, 34, 27, 43, 44};
}

namespace MNorm_old {
const int n_sphericals = 2; 
//const std::array<int,2> partial_indices = {K_N_LSQUARED, K_N_ZL};
const std::array<int,2> depends_on_sphericals = {2, 3};
const std::array<int,8> depends_on_scalar_products = {35, 43, 44, 31, 39, 29, 37, 7};
}

namespace MNorm {
const int n_sphericals = 2; 
//const std::array<int,2> partial_indices = {K_N_PSQUARED, K_N_ZP};
const std::array<int,2> depends_on_sphericals = {0, 1};
const std::array<int,8> depends_on_scalar_products = {35, 33, 20, 32, 19, 30, 17, 12};
}

namespace decay_constant {
const int n_sphericals = 2; 
//const std::array<int,2> partial_indices = {K_N_PSQUARED, K_N_ZP};
const std::array<int,2> depends_on_sphericals = {0, 1};
const std::array<int,8> depends_on_scalar_products = {35, 33, 20, 32, 19, 30, 17, 12};
}

namespace amplitude{
    const int n_sphericals = 2;
    const std::array<int,2> depends_on_sphericals = {0, 1};
    const std::array<int,2> depends_on_sphericals_cheb = {0, 5};
    const std::array<int,2> depends_on_sphericals_inside = {2, 3};
    const std::array<int,2> depends_on_sphericals_cheb_inside = {2, 5};
}

namespace kernel_all {
const int n_sphericals = 5; 
//const std::array<int,5> partial_indices = {K_N_PSQUARED, K_N_ZP, K_N_LSQUARED, K_N_ZL, K_N_YL};
const std::array<int,5> depends_on_sphericals = {0, 1, 2, 3, 4};
const std::array<int,12> depends_on_scalar_products = {35, 34, 27, 43, 44, 31, 24, 39, 29, 22, 37, 7};
}



 const vector<bool> K_necessary_sphericals = {true, true, true, true, true};
