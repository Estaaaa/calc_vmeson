module library_parse

    implicit none

	private

    ! Basic parameters
    double precision, parameter :: PI = 3.14159265358979323846264338327950d0
    double complex, parameter :: i_ = cmplx(0.d0, 1.d0)

    public parser

    type parser

        integer            :: nargs        = 0
        integer            :: unit_number  = 0
        integer            :: input_status = -1

        character(len=255) :: input_file_name
        character(len=255) :: args(1:25)


    contains

        procedure :: open        => parse_open
        procedure :: close       => parse_close

        procedure :: read        => parse_readline

        procedure :: get_real    => parse_get_real
        procedure :: get_integer => parse_get_integer

        procedure :: ltrim => ltrim_getarg

    end type parser

    type(parser) :: in_script

contains
!_______________________________________________________________________
!
function ltrim_getarg(this, j) result(lcstr)

    class(parser)  :: this
    integer :: j
    ! convert string to lower case
    character (len=len_trim(this%args(j))):: lcstr
    integer :: ilen, ioffset, iquote,  i, iav, iqc

    ilen=len_trim(this%args(j))
    ioffset=iachar('A')-iachar('a')
    iquote=0
    lcstr=this%args(j)

    do i=1,ilen
        iav=iachar(this%args(j)(i:i))
        if(iquote==0 .and. (iav==34 .or.iav==39)) then
            iquote=1
            iqc=iav
            cycle
        end if
        if(iquote==1 .and. iav==iqc) then
            iquote=0
            cycle
        end if
        if (iquote==1) cycle
        if(iav >= iachar('A') .and. iav <= iachar('Z')) then
            lcstr(i:i)=achar(iav-ioffset)
        else
            lcstr(i:i)=this%args(j)(i:i)
        end if
    end do

end function
!_______________________________________________________________________
!
double precision  function parse_get_real(this, i)

    class(parser)  :: this
    integer, intent(in) :: i

    integer :: ios

    call value_dr(this%args(i), parse_get_real, ios)

    ! ios is an error which we don't process

end function
!_______________________________________________________________________
!
integer function parse_get_integer(this, i)

    class(parser)  :: this
    integer, intent(in) :: i

    integer :: ios

    call value_di(this%args(i), parse_get_integer, ios)

    ! ios is an error which we don't process

end function
!_______________________________________________________________________
!
function parse_readline(this) result (input_status)

    class(parser) :: this

    character(len=255) :: line
    integer            :: input_status

    call readline(this%unit_number, line, input_status)

    this%input_status = input_status

    if(input_status.eq.0)then

        ! May be useful to parse strings as well.
        call parse(trim(line), '" =', this%args, this%nargs)

    end if

end function
!_______________________________________________________________________
!
subroutine parse_open(this, filename)

    class(parser) :: this

    character(len=*), intent(in) :: filename

    if(this%unit_number.gt.0)then
        write(*,'(A)')'# ERROR: parser, file already open.'
        stop
    end if

    this%input_file_name = filename
    open(newunit = this%unit_number, file=trim(filename), status = 'old', iostat=this%input_status)

    if(this%input_status.ne.0)then

        write(*,'(A)') "# ERROR: file_parse, error opening file "//trim(filename)
        stop

    endif

end subroutine
!_______________________________________________________________________
!
subroutine parse_close(this)

        class(parser) :: this

        close(this%unit_number)

        this%unit_number     =  0
        this%input_status    = -1
        this%input_file_name = ''

end subroutine
!_______________________________________________________________________
!
subroutine parse(str,delims,args,nargs)

        ! Parses the string 'str' into arguments args(1), ..., args(nargs) based on
        ! the delimiters contained in the string 'delims'. Preceding a delimiter in
        ! 'str' by a backslash (\) makes this particular instance not a delimiter.
        ! The integer output variable nargs contains the number of arguments found.

        character(len=*)              :: str,delims

        character(len=*),dimension(:) :: args
        integer, intent(inout)        :: nargs

        character(len=len_trim(str))  :: strsav
        integer                       :: ios, na, i, k, lenstr

        strsav=str
        call compact(str)
        na=size(args)
        do i=1,na
            args(i)=' '
        end do
        nargs=0
        lenstr=len_trim(str)
        if(lenstr==0) return
        k=0

        do
            if(len_trim(str) == 0) exit
            nargs=nargs+1
            call split(str,delims,args(nargs))
            call removebksl(args(nargs))
        end do
        str=strsav

end subroutine
!_______________________________________________________________________
!
subroutine compact(str)

        ! Converts multiple spaces and tabs to single spaces; deletes control characters;
        ! removes initial spaces.

        character(len=*):: str
        character(len=1):: ch
        character(len=len_trim(str)):: outstr
        integer :: lenstr, isp, k, i, ich

        str=adjustl(str)
        lenstr=len_trim(str)
        outstr=' '
        isp=0
        k=0

        do i=1,lenstr
            ch=str(i:i)
            ich=iachar(ch)

        select case(ich)

            case(9,32)     ! space or tab character
            if(isp==0) then
            k=k+1
            outstr(k:k)=' '
            end if
            isp=1

            case(33:)      ! not a space, quote, or control character
            k=k+1
            outstr(k:k)=ch
            isp=0

        end select

        end do

        str=adjustl(outstr)

end subroutine
!_______________________________________________________________________
!
subroutine value_dr(str,rnum,ios)

        ! Converts number string to a double precision real number

        character(len=*)::str
        double precision ::rnum
        integer :: ios, ilen, ipos

        ilen=len_trim(str)
        ipos=scan(str,'Ee')
        if(.not.is_digit(str(ilen:ilen)) .and. ipos/=0) then
            ios=3
            return
        end if
        read(str,*,iostat=ios) rnum

end subroutine
!_______________________________________________________________________
!
subroutine value_di(str,inum,ios)

        ! Converts number string to a double precision integer value

        character(len=*)::str
        integer :: inum, ios
        double precision  :: rnum

        call value_dr(str,rnum,ios)
        if(abs(rnum)>huge(inum)) then
            ios=15
            return
        end if
        inum=nint(rnum)

    end subroutine
!_______________________________________________________________________
!
subroutine readline(nunitr,line,ios)

	! Reads line from unit=nunitr, ignoring blank lines
	! and deleting comments beginning with an exclamation point(!)

	character (len=*):: line
	integer :: nunitr, ios, ipos

	do
  		read(nunitr,'(a)', iostat=ios) line      ! read input line
  		if(ios /= 0) return
  		line=adjustl(line)
  		ipos=index(line,'#')
  		if(ipos == 1) cycle
  		if(ipos /= 0) line=line(:ipos-1)
  		if(len_trim(line) /= 0) exit

  	end do

end subroutine
!_______________________________________________________________________
!
subroutine split(str,delims,before,sep)

	! Routine finds the first instance of a character from 'delims' in the
	! the string 'str'. The characters before the found delimiter are
	! output in 'before'. The characters after the found delimiter are
	! output in 'str'. The optional output character 'sep' contains the
	! found delimiter. A delimiter in 'str' is treated like an ordinary
	! character if it is preceded by a backslash (\). If the backslash
	! character is desired in 'str', then precede it with another backslash.

	character(len=*) :: str,delims,before
	character,optional :: sep
	logical :: pres
	character :: ch,cha
	integer :: k, ibsl,i, lenstr, ipos, iposa

	pres=present(sep)
	str=adjustl(str)
	call compact(str)
	lenstr=len_trim(str)
	if(lenstr == 0) return        ! string str is empty
	k=0
	ibsl=0                        ! backslash initially inactive
	before=' '
	do i=1,lenstr
		ch=str(i:i)
   		if(ibsl == 1) then          ! backslash active
   			k=k+1
   			before(k:k)=ch
   			ibsl=0
   			cycle
   		end if
   		if(ch == '\') then          ! backslash with backslash inactive
   			k=k+1
   			before(k:k)=ch
   			ibsl=1
   			cycle
   		end if
   		ipos=index(delims,ch)
   		if(ipos == 0) then          ! character is not a delimiter
   			k=k+1
   			before(k:k)=ch
   			cycle
   		end if
   		if(ch /= ' ') then          ! character is a delimiter that is not a space
   			str=str(i+1:)
   			if(pres) sep=ch
   			exit
   		end if
   		cha=str(i+1:i+1)            ! character is a space delimiter
   		iposa=index(delims,cha)
   		if(iposa > 0) then          ! next character is a delimiter
   			str=str(i+2:)
   			if(pres) sep=cha
   			exit
   		else
   			str=str(i+1:)
   			if(pres) sep=ch
   			exit
   		end if
   	end do
   	if(i >= lenstr) str=''
	str=adjustl(str)              ! remove initial spaces

end subroutine
!_______________________________________________________________________
!
subroutine removebksl(str)

	! Removes backslash (\) characters. Double backslashes (\\) are replaced
	! by a single backslash.

	character(len=*):: str
	character(len=1):: ch
	character(len=len_trim(str))::outstr
	integer :: lenstr, k, ibsl,i

	str=adjustl(str)
	lenstr=len_trim(str)
	outstr=' '
	k=0
	ibsl=0                        ! backslash initially inactive

	do i=1,lenstr
		ch=str(i:i)
  		if(ibsl == 1) then          ! backslash active
  			k=k+1
  			outstr(k:k)=ch
  			ibsl=0
  			cycle
  		end if
  		if(ch == '\') then          ! backslash with backslash inactive
  			ibsl=1
  			cycle
  		end if
  		k=k+1
  		outstr(k:k)=ch              ! non-backslash with backslash inactive
  	end do

  	str=adjustl(outstr)

end subroutine
!_______________________________________________________________________
!
function is_letter(ch) result(res)

	! Returns .true. if ch is a letter and .false. otherwise

	character :: ch
	logical :: res

	select case(ch)
	case('A':'Z','a':'z')
		res=.true.
	case default
		res=.false.
	end select

end function
!_______________________________________________________________________
!
function is_digit(ch) result(res)

	! Returns .true. if ch is a digit (0,1,...,9) and .false. otherwise

	character :: ch
	logical :: res

	select case(ch)
	case('0':'9')
		res=.true.
	case default
		res=.false.
	end select

end function
!_______________________________________________________________________
!
function lowercase(str) result(lcstr)

	! convert string to lower case
    character (len=*):: str
    character (len=len_trim(str)):: lcstr
    integer :: ilen, ioffset, iquote,  i, iav, iqc

    ilen=len_trim(str)
    ioffset=iachar('A')-iachar('a')
    iquote=0
    lcstr=str

    do i=1,ilen
        iav=iachar(str(i:i))
        if(iquote==0 .and. (iav==34 .or.iav==39)) then
            iquote=1
            iqc=iav
            cycle
        end if
        if(iquote==1 .and. iav==iqc) then
            iquote=0
            cycle
        end if
        if (iquote==1) cycle
        if(iav >= iachar('A') .and. iav <= iachar('Z')) then
            lcstr(i:i)=achar(iav-ioffset)
        else
            lcstr(i:i)=str(i:i)
        end if
    end do

end function
!_______________________________________________________________________
!
end module
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


   MODULE global


          ! Quark propagator

            integer NFlavors, qprop_nx, qprop_ny
            real(8) Lambda_QCD, Z2_Renormalization, m_Hat, gamma_m
            real(8), allocatable, dimension(:) :: qprop_x
            real(8), allocatable, dimension(:,:) :: qprop_y
            real(8), allocatable, dimension(:,:,:,:,:) :: qprop_f

          ! Misc

            real(8), PARAMETER ::  PI = 3.14159265358979D0
            complex(8), PARAMETER :: I = (0D0,1D0)
            complex(8), PARAMETER :: ComplexOne = (1D0,0D0)

            character(len=:), allocatable :: quark_filename
            !character(len=:), allocatable :: vertex_filename
            character(len=:), allocatable :: par_filename

            ! "__QP_complex_1_parabola_edit.dat"
      CONTAINS


      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


      SUBROUTINE DataInit_Global




            integer i1, iQ, iQ1, iQ2, nQMid
            real(8) Q2Mid, xGrid

             print '(A)','Reading '//par_filename
            open(10,file=par_filename)!'__QP_info.dat')
              DO i1=1,24
              	 read(10,*)
              END DO
              read(10,'(40X,I15)') NFlavors
              read(10,'(40X,F15.7)') Lambda_QCD
              DO i1=27,55
              	 read(10,*)
              END DO
              read(10,'(40X,F15.7)') Z2_Renormalization
              DO i1=57,60
              	 read(10,*)
              END DO
              read(10,'(40X,F15.7)') m_Hat

              gamma_m = 12D0/(33D0-2D0*NFlavors)
              m_Hat = m_Hat * 1.D-3

              DO i1=1,25 ! 65
              	read(10,*)
              END DO

              read(10,'(40X,I15)') qprop_nx
              read(10,'(40X,I15)') qprop_ny

            close(10)

      END SUBROUTINE DataInit_Global




   END MODULE global


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


   MODULE functions
   CONTAINS


 !  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 !
 !  Propagators
 !
 !  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 !  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



         SUBROUTINE Prepare_Quark_Propagator

                    USE global, ONLY: qprop_nx,qprop_ny,qprop_x,qprop_y,qprop_f,quark_filename
                    IMPLICIT NONE
                    integer j1,j2,j3,j4,j5
                    character(len=4) yname
                    character(len=20) zformat

                  ! This subroutine reads in the quark propagator dressing functions A(pqu), B(pqu),
                  ! M(pqu), sv(pqu), ss(pqu) from "_qprop_complex_plane.dat" and stores them in the array
                  ! qprop_f(:,:,:,:,:) with indices
                  !
                  !    j1: 1...qprop_nx
                  !    j2: 1...qprop_ny
                  !    j3: 1:5 (A,B,M,sv,ss)
                  !    j4: 1,2 (Re,Im)
                  !    j5: 0:3 (f, f', f'', f''')
                  !
                  ! Re(pqu) is stored in qprop_x(1:qprop_nx) and the associated Im(pqu) values
                  ! in qprop_y(1:qprop_nx,1:qprop_ny).
                  !
                  ! Plus, the second derivative of qprop_f in Im(pqu) direction is calculated
                  ! for each Re(pqu) value and stored in qprop_f2(:,:,:,:).
                  !
                  ! All of the arrays defined here are later used by the subroutines
                  ! QuarkPropagator, QuarkPropagatorD, QuarkPropagatorInv, and
                  ! QuarkPropagatorInvD, so this subroutine should be called once at
                  ! the beginning of each program.

                    allocate (qprop_x(qprop_nx))
                    allocate (qprop_y(qprop_nx,qprop_ny))
                    allocate (qprop_f(qprop_nx,qprop_ny,5,2,0:3))

                    qprop_f = 0D0

                    write(yname,'(I4)') qprop_ny
                    zformat = "(6X,D15.7,"//trim(yname)//"D15.7)"

                    print '(A)','Reading '//quark_filename
                    open(11,file=quark_filename)

                      read(11,*)
                      read(11,*)

                      DO j1=1,qprop_nx
                      	read(11,zformat) qprop_x(j1), qprop_y(j1,:)
                      END DO

                      read(11,*)

                    ! j4 = 1,2: Re, Im
                    ! j3 = 1,5: A, B, M, sv, ss
                    ! j5 = 0,3: 0th, 1st, 2nd, 3rd derivative

                      DO j5=0,3
                        DO j3=1,5
                          DO j4=1,2
                          	read(11,*)
                            DO j1=1,qprop_nx
                            	read(11,zformat) qprop_x(j1), qprop_f(j1,:,j3,j4,j5)
                            END DO
                            read(11,*)
                          END DO
                        END DO
                      END DO

                    close(11)


         END SUBROUTINE Prepare_Quark_Propagator


      !  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


         SUBROUTINE QuarkPropagator (pqu,f1,f2)

                    USE global, ONLY: qprop_nx,qprop_ny,qprop_x,qprop_y,qprop_f,I,Z2_Renormalization,m_Hat,Lambda_QCD,gamma_m

                    IMPLICIT NONE
                    complex(8),INTENT(IN):: pqu
                    complex(8),INTENT(OUT):: f1,f2
                    complex(8) sv,ss,Dsv,Dss,NN,DN,aa,mm,dm
                    integer k,mlo,mhi,nlo,nhi
                    real(8) re(2),im(2)

                  ! This subroutine returns for nder=0 the quark propagator's dressing functions
                  !  - nfun=1: A,B
                  !  - nfun=2: sv,ss
                  ! and for nder=1 the respective derivatives.

                             ! Cubic spline of a tabulated quark propagator (filename specified in
                             ! the routine "Prepare_Quark_Propagator" which must be called in the
                             ! beginning of each program) at the point pqu.

                               IF (REAL(pqu)<qprop_x(1)) THEN

                               	 print'(3X,A,F10.3,A,F10.3,A)', "   pqu = (", real(pqu), ", ", aimag(pqu), "): Q-Prop not defined!"   ! failsafe

                               ELSE IF (REAL(pqu)>qprop_x(qprop_nx-5)) THEN

                               	 aa = Z2_Renormalization
                               	 mm = m_Hat / ( 0.5D0*Log(pqu/Lambda_QCD**2) )**gamma_m
                               	 dm = -m_Hat*gamma_m/(2D0*pqu) / ( 0.5D0*Log(pqu/Lambda_QCD**2) )**(gamma_m+1D0)

                               	f1 = aa
                               	f2 = aa*mm

                               ELSE

                                 CALL Find_Position_2D (qprop_x,qprop_y,qprop_nx,qprop_ny,real(pqu),aimag(pqu),10,mlo,mhi,nlo,nhi,1)

                                 DO k=1,2

                                   CALL Spline_2D ( qprop_x(mlo:mhi),qprop_y(mlo:mhi,nlo:nhi), &
                                                               qprop_f(mlo:mhi,nlo:nhi,k,1,0),     &
                                                              -qprop_f(mlo:mhi,nlo:nhi,k,1,2),   &
                                                               mhi-mlo+1,nhi-nlo+1,                       &
                                                               real(pqu),aimag(pqu),re(k) )

                                   CALL Spline_2D ( qprop_x(mlo:mhi),qprop_y(mlo:mhi,nlo:nhi), &
                                                               qprop_f(mlo:mhi,nlo:nhi,k,2,0),     &
                                                              -qprop_f(mlo:mhi,nlo:nhi,k,2,2),   &
                                                               mhi-mlo+1,nhi-nlo+1,                       &
                                                               real(pqu),aimag(pqu),im(k) )

                                 END DO

                                 f1 = re(1)+I*im(1)
                                 f2 = re(2)+I*im(2)

                       	      END IF




         END SUBROUTINE QuarkPropagator






 !  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !  -------------------------------------------------------------------------------------
 !
 !  Numerical routines:
 !
 !  -------------------------------------------------------------------------------------
 !  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



            SUBROUTINE derivative_1st (x,y,n,y1,method)

               ! 1st derivative y1(x) of an array y(x) of dimension n

                 IMPLICIT NONE
                 integer, INTENT(IN) :: n,method
                 real(8), dimension (n), INTENT(IN) :: x,y
                 real(8), dimension (n), INTENT(OUT) :: y1
                 integer i
                 real(8) eps,xhi,xlo,yhi,ylo,y2(n),den,gam(n),r(n),h(n),alpha,dum(n)

                 SELECT CASE (method)

                 CASE(1) ! spline + difference quotients

                     eps = 1D-5

                     CALL derivative_2nd (x,y,n,y2,2)

                     DO i=2,n-1

                       IF (x(i)==0.D0) THEN
                       	xhi=eps
                       	xlo=-eps
                       ELSE
                         xhi=x(i)*(1+sign(eps,x(i)))
                         xlo=x(i)*(1-sign(eps,x(i)))
                       END IF

                       CALL spline (x,y,dum,y2,n,xlo,ylo,1)
                       CALL spline (x,y,dum,y2,n,xhi,yhi,1)

                       y1(i)=(yhi-ylo)/(xhi-xlo)

                     END DO

                   ! i=1

                     IF (x(1)==0D0) THEN
                     	xhi=eps
                     ELSE
                       xhi=x(1)*(1+sign(eps,x(1)))
                     END IF
                     CALL spline (x,y,dum,y2,n,xhi,yhi,1)
                     y1(1)=(yhi-y(1))/(xhi-x(1))

                   ! i=n

                     IF (x(n)==0D0) THEN
                     	xlo=-eps
                     ELSE
                       xlo=x(n)*(1-sign(eps,x(n)))
                     END IF
                     CALL spline (x,y,dum,y2,n,xlo,ylo,1)
                     y1(n)=(y(n)-ylo)/(x(n)-xlo)

                 CASE(2) ! tridiagonal algorithm with specified second derivatives at the boundaries

                     CALL derivative_2nd (x,y,n,y2,2)    ! for y2(1),y2(n)

                     h(1) = x(2)-x(1)
                     r(1) = 3D0*( (y(2)-y(1))/h(1)**2 - y2(1)/6D0 )
                     y1(1) = 0.5D0*r(1)*h(1)
                     gam(1) = 0.5D0

                     DO i=2,n

                     	h(i)=x(i+1)-x(i)

                       IF (i==n) THEN
                     		r(i) = 3D0*( y2(n)/6D0 - (y(n)-y(n-1))/h(n-1)**2 )
                       ELSE
                     	  r(i) = 3D0*( (y(i+1)-y(i))/h(i)**2 + (y(i)-y(i-1))/h(i-1)**2 )
                       END IF

                     	den = 2D0/h(i-1) + 2D0/h(i) - gam(i-1)/h(i-1)
                       IF ( den==0.D0 ) pause "   derivative_1st: tridag failed" ! No pivoting here.

                     	y1(i) = ( r(i)-y1(i-1)/h(i-1) )/den
                     	gam(i) = 1D0/(h(i)*den)

                     END DO

                     DO i=n-1,1,-1
                     	y1(i) = y1(i)-gam(i)*y1(i+1)
                     END DO

                 CASE(3) ! only difference quotients (not good)

                     DO i=2,n-1
                     	y1(i)=(y(i+1)-y(i-1))/(x(i+1)-x(i-1))
                     END DO

                     y1(1)=(y(2)-y(1))/(x(2)-x(1))
                     y1(n)=(y(n)-y(n-1))/(x(n)-x(n-1))

                 END SELECT

            END SUBROUTINE derivative_1st


          ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


            SUBROUTINE derivative_2nd (x,y,n,y2,method)

               ! 2nd derivative y2(x) of an array y(x) of dimension n

                 IMPLICIT NONE
                 integer, INTENT(IN) :: n,method
                 real(8), dimension (n), INTENT(IN) :: x,y
                 real(8), dimension (n), INTENT(OUT) :: y2
                 integer i
                 real(8) y1(n),den,gam(n),r(n),h(n),alpha

                 SELECT CASE (method)

                 CASE(1) ! spline + difference quotients

                     CALL derivative_1st(x,y,n,y1,1)
                     CALL derivative_1st(x,y1,n,y2,1)

                 CASE(2) ! tridiagonal algorithm with specified second derivatives
                         ! (via alpha) at the boundaries

                     alpha=1D0

                     y2(1)=0D0
                     gam(1)=0D0
                     h(1)=x(2)-x(1)
                     IF (h(1)==0.D0) print*, "   derivative_2nd: h(1)=0"

                     DO i=2,n-1

                       h(i)=x(i+1)-x(i)
                       IF (h(i)==0.D0) print*, "   derivative_2nd: h(i)=0"        ! may happen if resolution is higher than resolution in input/output file
                       den = 2D0*(h(i)+h(i-1)) - gam(i-1)*h(i-1)
                     	r(i) = 6D0*( (y(i+1)-y(i))/h(i) - (y(i)-y(i-1))/h(i-1) )

                       IF (i==2) den=den+alpha*h(1)
                       IF (i==n-1) den=den+alpha*h(n-1)
                       IF ( den==0.D0 ) pause "   derivative_2nd: tridag failed" ! No pivoting here. Or: x(i) are equal, e.g. 0

                     	y2(i) = ( r(i)-h(i-1)*y2(i-1)  )/den
                     	gam(i) = h(i)/den

                     END DO

                     DO i=n-2,2,-1
                     	y2(i) = y2(i)-gam(i)*y2(i+1)
                     END DO

                     y2(n) = alpha*y2(n-1)
                     y2(1) = alpha*y2(2)

                 END SELECT



            END SUBROUTINE derivative_2nd


          ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


            SUBROUTINE derivative_1st_complex(x,y,f,m,n,f1)

                   IMPLICIT NONE
                   integer, INTENT(IN) :: m,n
                   real(8), INTENT(IN) :: x(m),y(m,n)
                   complex(8), INTENT(IN) :: f(m,n)
                   complex(8), INTENT(OUT) :: f1(m,n)
                   real(8) :: re(m,n),im(m,n)
                   integer i

                 ! Given a 1dim. array x(1:m), and for each x value (with index i) a 1dim. array y(i,1:n),
                 ! this routine constructs the first derivatives of the rows of f, stored in the array f1(1:m,1:n).
                 ! If f is an analytic function and z(i,j)=x(i)+I*y(i,j), f1 is the complex derivative df/dz

                   DO i=1,m
                     CALL derivative_1st(y(i,:), real(f(i,:)),n,re(i,:),1)
                     CALL derivative_1st(y(i,:),aimag(f(i,:)),n,im(i,:),1)
                   END DO

                   f1 = -(0D0,1D0)*re + im

            END SUBROUTINE derivative_1st_complex


          ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


            SUBROUTINE derivative_2nd_complex(x,y,f,m,n,f2)

                   IMPLICIT NONE
                   integer, INTENT(IN) :: m,n
                   real(8), INTENT(IN) :: x(m),y(m,n)
                   complex(8), INTENT(IN) :: f(m,n)
                   complex(8), INTENT(OUT) :: f2(m,n)
                   real(8) :: re(m,n),im(m,n)
                   integer i

                 ! Given a 1dim. array x(1:m), and for each x value (with index i) a 1dim. array y(i,1:n),
                 ! this routine constructs the second derivatives of the rows of f, stored in the array f2(1:m,1:n).
                 ! If f is an analytic function and z(i,j)=x(i)+I*y(i,j), f2 is the complex derivative d2f/dz2

                   DO i=1,m
                     CALL derivative_2nd(y(i,:), real(f(i,:)),n,re(i,:),2)
                     CALL derivative_2nd(y(i,:),aimag(f(i,:)),n,im(i,:),2)
                   END DO

                   f2 = -re -(0D0,1D0)*im

            END SUBROUTINE derivative_2nd_complex


          ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


             SUBROUTINE Find_Position (xa,m,x,nder,mlo,mhi,direction)

                  ! Determines an interval around the position x (+-nder) given by mlo..mhi
                  ! large enough to allow for a reliable derivative (nder=10...20 should be ok).

                    IMPLICIT NONE
                    integer, INTENT(IN) :: m,nder,direction
                    integer, INTENT(OUT) :: mlo,mhi
                    real(8), INTENT(IN):: xa(m),x
                    integer i1,nlo1,nhi1,k1,iim,iin,mm,mlo1,mhi1

                      mlo=1
                      mhi=m

                      DO WHILE (mhi-mlo>1)
                        k1=(mhi+mlo)/2
                        IF (xa(k1)>x) THEN
                          mhi=k1
                        ELSE
                          mlo=k1
                        END IF
                      END DO

                      IF (direction==1) THEN
                        mlo = max(mlo-nder,1)
                        mhi = min(mhi+nder,m)
                      END IF



             END SUBROUTINE Find_Position



          ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


             SUBROUTINE Find_Position_2D (xa,ya,m,n,x,y,nder,mlo,mhi,nlo,nhi,direction)

                  ! Determines an interval around the position (x,y) (+-nder) given by
                  !   - mlo..mhi in x direction
                  !   - nlo..nhi in y direction,
                  ! large enough to allow for a reliable derivative (nder=10...20 should be ok).

                    IMPLICIT NONE
                    integer, INTENT(IN) :: m,n,nder,direction
                    integer, INTENT(OUT) :: mlo,mhi,nlo,nhi
                    real(8), INTENT(IN):: xa(m),ya(m,n),x,y
                    integer i1,nlo1,nhi1,k1,iim,iin,mm,mlo1,mhi1

                      mlo=1
                      mhi=m

                      DO WHILE (mhi-mlo>1)
                        k1=(mhi+mlo)/2
                        IF (xa(k1)>x) THEN
                          mhi=k1
                        ELSE
                          mlo=k1
                        END IF
                      END DO

                    ! Prepare for derivative in x direction
                      IF (direction==1) THEN
                        mlo = max(mlo-nder,1)
                        mhi = min(mhi+nder,m)
                      END IF

                      nlo = n+1   ! start values
                      nhi = 0

                      DO iim=mlo,mhi

                        nlo1=1
                        nhi1=n

                        DO WHILE (nhi1-nlo1>1)
                          k1=(nhi1+nlo1)/2
                          IF (ya(iim,k1)>y) THEN
                            nhi1=k1
                          ELSE
                            nlo1=k1
                          END IF
                        END DO

                        IF (nlo1<nlo) nlo=nlo1
                        IF (nhi1>nhi) nhi=nhi1

                      END DO

                    ! Prepare for derivative in y direction: better
                      IF (direction==2) THEN
                        nlo = max(nlo-nder,1)
                        nhi = min(nhi+nder,n)
                      END IF


             END SUBROUTINE Find_Position_2D


          ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


             SUBROUTINE Spline (xa,ya,y1a,y2a,n,x,y,typ)

                ! Spline interpolation, returns only one value: y(x)
                ! y1a, y2a not for every type necessary.

                  IMPLICIT NONE

                  integer, INTENT(IN) :: n,typ
                  real(8), INTENT(IN) :: xa(n),ya(n),y1a(n),y2a(n),x
                  real(8), INTENT(OUT) :: y
                  real(8) a,b,h,c,d,c0,c1,c2,c3,c4,c5
                  integer k,khi,klo

                ! Isolate x such that xa(khi) and xa(klo) are the
                ! nearest upper and lower grid points

                  klo=1
                  khi=n

                  DO WHILE (khi-klo>1)
                    k=(khi+klo)/2
                    IF (xa(k)>x) THEN
                      khi=k
                    ELSE
                      klo=k
                    END IF
                  END DO

                  h=xa(khi)-xa(klo)
                  IF (h==0.) THEN
                    PRINT*,"bad xa input in spline"  ! only if two xa's are equal (and equal to x)
                  END IF

                  SELECT CASE (typ)

                  CASE (1)  ! Cubic spline with given second derivatives --> y1a not used!

                    a = (xa(khi)-x)/h
                    b = (x-xa(klo))/h
                    y = a*ya(klo) + b*ya(khi) + (h**2)/6.D0* ((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))

                  CASE (2)  ! Cubic spline with given first derivatives ---> y2a not used!

                    a = (ya(khi)-ya(klo))/h
                    b = (y1a(khi)-y1a(klo))/h
                    c = -1D0/h**2*(a-(y1a(khi)+y1a(klo))/2D0)

                    y = ya(klo) + (x-xa(klo))* ( c*(2D0*x**2-xa(klo)*(x+xa(klo))-3D0*xa(khi)*(x-xa(klo)) ) + b*(x-xa(klo))/2D0 + y1a(klo) )

                  CASE (3)  ! Linear interpolation ---> y1a, y2a not used! (same result for case 1 with y2a = 0)

                    y = ya(khi) + (x-xa(khi))*(ya(khi)-ya(klo))/h

                  CASE (4)  ! Spline 5th order ---> both y1a, y2a necessary

                    c5 = (y2a(khi)-y2a(klo))/(2D0*h**3) -   3D0*(y1a(khi)+y1a(klo))/h**4 + 6D0*(ya(khi)-ya(klo))/h**5
                    c4 = (y2a(khi)+y2a(klo))/(4D0*h**2) - 0.5D0*(y1a(khi)-y1a(klo))/h**3 - 2.5D0*c5*(xa(khi)+xa(klo))
                    c3 = (y2a(khi)+y2a(klo))/(6D0*h)    -   2D0*c4*(xa(khi)+xa(klo)) - 10D0*c5*(xa(khi)**3-xa(klo)**3)/(3D0*h)
                    c2 = 0.5D0*y2a(klo) - 3D0*c3*xa(klo) - 6D0*c4*xa(klo)**2 - 10D0*c5*xa(klo)**3
                    c1 = y1a(klo) - 2D0*c2*xa(klo) - 3D0*c3*xa(klo)**2 - 4D0*c4*xa(klo)**3 - 5D0*c5*xa(klo)**4
                    c0 = ya(klo) - c1*xa(klo) - c2*xa(klo)**2 - c3*xa(klo)**3 - c4*xa(klo)**4 - c5*xa(klo)**5

                    y = c0 + c1*x + c2*x**2 + c3*x**3 + c4*x**4 + c5*x**5

                  END SELECT

             END SUBROUTINE Spline


          ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


             SUBROUTINE Spline_2D (xa,ya,fa,f2a,mm,nn,x,y,f)

                  ! Cubic spline with given second derivative in y direction

                    INTEGER, INTENT(IN) :: mm,nn
                    real(8), INTENT(IN) :: xa(mm),ya(mm,nn),fa(mm,nn),f2a(mm,nn),x,y
                    real(8), INTENT(OUT) :: f

                    integer i1,i2
                    real(8) dum1(mm),ftmp(mm),f2tmp(mm),dum2(nn),gtmp(nn),g2tmp(nn),ytmp(nn),y2a(mm,nn)


                    ! Calculates 2nd derivative in xa direction.
                    ! Splines each row i for fixed y.
                    !
                    ! First spline direction: ya
                    !   -> f2a = second derivative of the (real-valued) function in ya-direction!
                    !   -> If fa is the real (imaginary) part of a complex function g:
                    !      fa = Re(g) -> f2a = -Re(d^2g/dz^2)
                    !      fa = Im(g) -> f2a = -Im(d^2g/dz^2)

                      DO i1=1,mm
                        CALL spline(ya(i1,:),fa(i1,:),dum2(:),f2a(i1,:),nn,y,ftmp(i1),1)
                      END DO

                      CALL derivative_2nd (xa,ftmp,mm,f2tmp,2)
                      CALL spline (xa,ftmp,dum1,f2tmp,mm,x,f,1)


             END SUBROUTINE Spline_2D


          ! !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!




   END MODULE functions


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


   PROGRAM Interpolate_QPV


          USE global
 	  USE library_parse
          USE functions


IMPLICIT NONE

 integer i1,i2,nIn
 complex(8), allocatable :: k2(:), A(:), B(:)

 integer :: unit_number1, unit_number2, dummy
 type(parser) :: in_file

    ! Read command line arguments

    integer :: num_args
    character(len=128), dimension(:), allocatable :: args

    num_args = command_argument_count()

    if(num_args.ne.2)then
        print '(A)', "Usage:"
        print '(A)', "    ./a.out quark_filename par_filename"
        print '(A)', ""

        stop
    end if
    allocate(args(num_args))
    call get_command_argument(1,args(1))
    call get_command_argument(2,args(2))

    quark_filename = trim(adjustl(args(1)))
    par_filename   = trim(adjustl(args(2)))


CALL DataInit_Global
CALL Prepare_Quark_Propagator

!!!!!!!!!!!!!!!!!!!!!!!!!!! FIRST FILE
!!!!!!!!!!!!!!!!!!!!!!!!!!! FIRST FILE
!!!!!!!!!!!!!!!!!!!!!!!!!!! FIRST FILE
!
!    ! Open the grid file and count points
!	call in_file%open("quark_grid_1.txt")
!    dummy = in_file%read()
!    nIn   = in_file%get_integer(1)
!
!    ! Allocate memory and read in the points
!    allocate(k2(   1:nIn), A(    1:nIn), B(    1:nIn))
!    DO i1 = 1,nIn ! Read in the grid in advance
!        dummy = in_file%read()
!        k2(i1) = cmplx(in_file%get_real(1),in_file%get_real(2))
!    end do
!    call in_file%close() ! And close the grid file
!
!
!!$omp parallel do default(shared)  ! Interpolate the quark
!    do i1 = 1, nIn
!        CALL QuarkPropagator ( k2(i1), A(i1), B(i1) )
!    end do
!
!    ! Output the result
!    open( newunit = unit_number2, file="quark_grid_out_1.txt")
!    do i1 = 1, nIn
!        write( unit_number2, '(50ES15.7)') k2(i1), A(i1), B(i1)
!    end do
!    close(unit_number2)
!
!    ! Clean up memory
!    deallocate(k2, A, B)
!
!    !!!!!!!!!!!!!!!!!!!!!!!!!!! SECOND FILE
!    !!!!!!!!!!!!!!!!!!!!!!!!!!! SECOND FILE
!    !!!!!!!!!!!!!!!!!!!!!!!!!!! SECOND FILE
!
!
!    call in_file%open("quark_grid_2.txt")
!      dummy = in_file%read()
!      nIn   = in_file%get_integer(1)
!
!      ! Allocate memory and read in the points
!      allocate(k2(   1:nIn), A(    1:nIn), B(    1:nIn))
!      DO i1 = 1,nIn ! Read in the grid in advance
!          dummy = in_file%read()
!          k2(i1) = cmplx(in_file%get_real(1),in_file%get_real(2))
!      end do
!      call in_file%close() ! And close the grid file
!
!
!  !$omp parallel do default(shared)  ! Interpolate the quark
!      do i1 = 1, nIn
!          CALL QuarkPropagator ( k2(i1), A(i1), B(i1) )
!      end do
!
!      ! Output the result
!      open( newunit = unit_number2, file="quark_grid_out_2.txt")
!      do i1 = 1, nIn
!          write( unit_number2, '(50ES15.7)') k2(i1), A(i1), B(i1)
!      end do
!      close(unit_number2)
!
!      ! Clean up memory
!      deallocate(k2, A, B)
!
      !!!!!!!!!!!!!!!!!!!!!!!!!!! THIRD FILE
      !!!!!!!!!!!!!!!!!!!!!!!!!!! THIRD FILE
      !!!!!!!!!!!!!!!!!!!!!!!!!!! THIRD FILE


      call in_file%open("quark_grid_3.txt")
        dummy = in_file%read()
        nIn   = in_file%get_integer(1)

        ! Allocate memory and read in the points
        allocate(k2(   1:nIn), A(    1:nIn), B(    1:nIn))
        DO i1 = 1,nIn ! Read in the grid in advance
            dummy = in_file%read()
            k2(i1) = cmplx(in_file%get_real(1),in_file%get_real(2))
        end do
        call in_file%close() ! And close the grid file


    !$omp parallel do default(shared)  ! Interpolate the quark
        do i1 = 1, nIn
            CALL QuarkPropagator ( k2(i1), A(i1), B(i1) )
        end do

        ! Output the result
        open( newunit = unit_number2, file="quark_grid_out_3.txt")
        do i1 = 1, nIn
            write( unit_number2, '(50ES15.7)') k2(i1), A(i1), B(i1)
        end do
        close(unit_number2)

        ! Clean up memory
        deallocate(k2, A, B)


   END PROGRAM Interpolate_QPV


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
