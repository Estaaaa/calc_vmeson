#!/bin/bash



cd interpolators/0/
gfortran -o b.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v1.f90 
gfortran -o a.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v2.f90 
gfortran -o quark3.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v1.f90
gfortran -o quarkpm.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v2.f90



cd ../

cd 1/
gfortran -o b.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v1.f90 
gfortran -o a.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v2.f90 
gfortran -o quark3.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v1.f90
gfortran -o quarkpm.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v2.f90


cd ../

cd 2/
gfortran -o b.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v1.f90 
gfortran -o a.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v2.f90 
gfortran -o quark3.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v1.f90
gfortran -o quarkpm.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v2.f90


cd ../

cd 3/
gfortran -o b.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v1.f90 
gfortran -o a.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v2.f90 
gfortran -o quark3.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v1.f90
gfortran -o quarkpm.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v2.f90


cd ../
