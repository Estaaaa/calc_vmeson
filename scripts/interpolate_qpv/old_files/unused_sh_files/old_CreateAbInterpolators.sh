#!/bin/bash



cd interpolators/0/interpolator_smallQ/
gfortran -o b.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v1.f90 
gfortran -o a.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v2.f90 
gfortran -o quark3.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v1.f90
gfortran -o quarkpm.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v2.f90

cd ../interpolator_spacelike/
gfortran -o b.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v1.f90 
gfortran -o a.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v2.f90 
gfortran -o quark3.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v1.f90
gfortran -o quarkpm.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v2.f90



cd ../../

cd 1/interpolator_smallQ/
gfortran -o b.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v1.f90 
gfortran -o a.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v2.f90 
gfortran -o quark3.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v1.f90
gfortran -o quarkpm.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v2.f90

cd ../interpolator_spacelike/
gfortran -o b.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v1.f90 
gfortran -o a.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v2.f90 
gfortran -o quark3.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v1.f90
gfortran -o quarkpm.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v2.f90


cd ../../

cd 2/interpolator_smallQ/
gfortran -o b.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v1.f90 
gfortran -o a.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v2.f90 
gfortran -o quark3.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v1.f90
gfortran -o quarkpm.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v2.f90

cd ../interpolator_spacelike/
gfortran -o b.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v1.f90 
gfortran -o a.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v2.f90 
gfortran -o quark3.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v1.f90
gfortran -o quarkpm.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v2.f90


cd ../../

cd 3/interpolator_smallQ/
gfortran -o b.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v1.f90 
gfortran -o a.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v2.f90 
gfortran -o quark3.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v1.f90
gfortran -o quarkpm.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v2.f90

cd ../interpolator_spacelike/
gfortran -o b.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v1.f90 
gfortran -o a.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv_v2.f90 
gfortran -o quark3.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v1.f90
gfortran -o quarkpm.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-v2.f90

cd ../../
