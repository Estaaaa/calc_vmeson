#!/bin/bash

cd interpolator_smallQ/
gfortran -ffree-line-length-none -ffree-form _interpolate-qpv-v1.f90 
mv a.out b.out
gfortran -ffree-line-length-none -ffree-form _interpolate-qpv-v2.f90

cd ../interpolator_spacelike/
gfortran -ffree-line-length-none -ffree-form _interpolate-qpv-v1.f90 
mv a.out b.out
gfortran -ffree-line-length-none -ffree-form _interpolate-qpv-v2.f90
