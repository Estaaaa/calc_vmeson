#!/bin/bash



cd interpolators/Exe
gfortran -o vertex.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qpv.f90
gfortran -o quarkall.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-all.f90
#gfortran -o quarkpm.out -march=native -O3 -fopenmp -ffast-math -ffree-form -ffree-line-length-none _interpolate-qrk-pm.f90



cd ../
