#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
//#include <math_ops.h>

void kernelL_notopti_scalar(matCdoub& kern,
                              const ArrayScalarProducts& sp,
                const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){


    // local variables
    Cdoub denom=0.0;


    denom = 4.E+0;

    kern[0][0] =  1.2E+1*ang1;
 
    kern[0][0] = (1.0/ denom)* kern[0][0]; 
    kern[0][1] =   0;
 
    kern[0][1] = (1.0/ denom)* kern[0][1]; 
    kern[0][2] =   0;
 
    kern[0][2] = (1.0/ denom)* kern[0][2]; 
    kern[0][3] =   0;
 
    kern[0][3] = (1.0/ denom)* kern[0][3]; 

    denom =  - 4.E+0*P_P*P_p*p_p + 4.E+0*pow(P_p,3);

    kern[1][0] =   0;
 
    kern[1][0] = (1.0/ denom)* kern[1][0]; 
    kern[1][1] =  4.E+0*P_P*P_l*p_p*ang1 - 1.2E+1*P_p*pow(P_l,2)*p_p*
      ang0 - 4.E+0*P_p*pow(P_l,2)*l_l*ang0 + 4.E+0*P_p*pow(P_l,2)*ang1
       + 4.E+0*pow(P_p,2)*P_l*p_p*ang0 + 4.E+0*pow(P_p,2)*P_l*l_l*ang0
       - 8.E+0*pow(P_p,2)*P_l*ang1 + 8.E+0*pow(P_l,3)*p_p*ang0;
 
    kern[1][1] = (1.0/ denom)* kern[1][1]; 
    kern[1][2] =   - 6.E+0*P_p*p_p*ang1 + 2.E+0*P_p*pow(p_p,2)*ang0 - 
      2.E+0*P_p*l_l*ang1 - 2.E+0*P_p*pow(l_l,2)*ang0 + 4.E+0*P_p*ang2
       + 4.E+0*P_l*p_p*l_l*ang0 + 8.E+0*P_l*p_p*ang1 - 4.E+0*P_l*pow(
      p_p,2)*ang0;
 
    kern[1][2] = (1.0/ denom)* kern[1][2]; 
    kern[1][3] =   0;
 
    kern[1][3] = (1.0/ denom)* kern[1][3]; 

    denom = 4.E+0*P_P*p_p - 4.E+0*pow(P_p,2);

    kern[2][0] =   0;
 
    kern[2][0] = (1.0/ denom)* kern[2][0]; 
    kern[2][1] =   - 4.E+0*P_P*P_p*P_l*p_p*ang0 + 4.E+0*P_P*P_p*P_l*l_l
      *ang0 - 4.E+0*P_P*P_p*P_l*ang1 + 4.E+0*P_P*pow(P_l,2)*p_p*ang0 - 
      4.E+0*P_P*pow(P_l,2)*l_l*ang0 + 4.E+0*P_P*pow(P_l,2)*ang1 + 8.E+0
      *P_p*pow(P_l,3)*ang0 - 1.6E+1*pow(P_p,2)*pow(P_l,2)*ang0 + 8.E+0*
      pow(P_p,3)*P_l*ang0;
 
    kern[2][1] = (1.0/ denom)* kern[2][1]; 
    kern[2][2] =  4.E+0*P_P*p_p*l_l*ang0 - 2.E+0*P_P*p_p*ang1 - 2.E+0*
      P_P*pow(p_p,2)*ang0 - 2.E+0*P_P*l_l*ang1 - 2.E+0*P_P*pow(l_l,2)*
      ang0 + 4.E+0*P_P*ang2 - 4.E+0*P_p*P_l*p_p*ang0 + 4.E+0*P_p*P_l*
      l_l*ang0 + 8.E+0*P_p*P_l*ang1 + 4.E+0*pow(P_p,2)*p_p*ang0 - 4.E+0
      *pow(P_p,2)*l_l*ang0 - 4.E+0*pow(P_p,2)*ang1;
 
    kern[2][2] = (1.0/ denom)* kern[2][2]; 
    kern[2][3] =   0;
 
    kern[2][3] = (1.0/ denom)* kern[2][3]; 

    denom =  - 1.6E+1*P_P*p_p + 1.6E+1*pow(P_p,2);

    kern[3][0] =   0;
 
    kern[3][0] = (1.0/ denom)* kern[3][0]; 
    kern[3][1] =   0;
 
    kern[3][1] = (1.0/ denom)* kern[3][1]; 
    kern[3][2] =   0;
 
    kern[3][2] = (1.0/ denom)* kern[3][2]; 
    kern[3][3] =  1.6E+1*P_P*p_p*l_l*ang0 + 8.E+0*P_P*p_p*ang1 - 8.E+0*
      P_P*pow(p_p,2)*ang0 + 8.E+0*P_P*l_l*ang1 - 8.E+0*P_P*pow(l_l,2)*
      ang0 + 3.2E+1*P_p*P_l*p_p*ang0 + 3.2E+1*P_p*P_l*l_l*ang0 - 1.6E+1
      *P_p*P_l*ang1 - 3.2E+1*pow(P_p,2)*l_l*ang0 - 3.2E+1*pow(P_l,2)*
      p_p*ang0;
 
    kern[3][3] = (1.0/ denom)* kern[3][3]; 

}


void kernelY_noopti_scalar(matCdoub& kern,
  const ArrayScalarProducts& sp,  const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


    // local variables
    Cdoub denom;


    denom = 4.E+0;

    kern[0][0] =  4.E+0*ssp*ssm - 4.E+0*qm_qp*svp*svm;
 
    kern[0][0] = (1.0/ denom)* kern[0][0]; 
    kern[0][1] =   - 4.E+0*P_l*P_qm*ssp*svm - 4.E+0*P_l*P_qp*svp*ssm;
 
    kern[0][1] = (1.0/ denom)* kern[0][1]; 
    kern[0][2] =   - 4.E+0*l_qm*ssp*svm - 4.E+0*l_qp*svp*ssm;
 
    kern[0][2] = (1.0/ denom)* kern[0][2]; 
    kern[0][3] =  8.E+0*P_qm*l_qp*svp*svm - 8.E+0*P_qp*l_qm*svp*svm;
 
    kern[0][3] = (1.0/ denom)* kern[0][3]; 

    denom =  - 4.E+0*P_P*P_l*l_l + 4.E+0*pow(P_l,3);

    kern[1][0] =  4.E+0*P_l*l_qm*ssp*svm + 4.E+0*P_l*l_qp*svp*ssm - 
      4.E+0*P_qm*l_l*ssp*svm - 4.E+0*P_qp*l_l*svp*ssm;
 
    kern[1][0] = (1.0/ denom)* kern[1][0]; 
    kern[1][1] =   - 4.E+0*P_P*P_l*l_l*qm_qp*svp*svm - 4.E+0*P_P*P_l*
      l_l*ssp*ssm + 8.E+0*P_l*P_qm*P_qp*l_l*svp*svm - 4.E+0*pow(P_l,2)*
      P_qm*l_qp*svp*svm - 4.E+0*pow(P_l,2)*P_qp*l_qm*svp*svm + 4.E+0*
      pow(P_l,3)*qm_qp*svp*svm + 4.E+0*pow(P_l,3)*ssp*ssm;
 
    kern[1][1] = (1.0/ denom)* kern[1][1]; 
    kern[1][2] =   - 8.E+0*P_l*l_qm*l_qp*svp*svm + 4.E+0*P_qm*l_l*l_qp*
      svp*svm + 4.E+0*P_qp*l_l*l_qm*svp*svm;
 
    kern[1][2] = (1.0/ denom)* kern[1][2]; 
    kern[1][3] =   - 8.E+0*P_P*l_l*l_qm*ssp*svm + 8.E+0*P_P*l_l*l_qp*
      svp*ssm + 8.E+0*pow(P_l,2)*l_qm*ssp*svm - 8.E+0*pow(P_l,2)*l_qp*
      svp*ssm;
 
    kern[1][3] = (1.0/ denom)* kern[1][3]; 

    denom = 4.E+0*P_P*l_l - 4.E+0*pow(P_l,2);

    kern[2][0] =  4.E+0*P_P*l_qm*ssp*svm + 4.E+0*P_P*l_qp*svp*ssm - 
      4.E+0*P_l*P_qm*ssp*svm - 4.E+0*P_l*P_qp*svp*ssm;
 
    kern[2][0] = (1.0/ denom)* kern[2][0]; 
    kern[2][1] =   - 4.E+0*P_P*P_l*P_qm*l_qp*svp*svm - 4.E+0*P_P*P_l*
      P_qp*l_qm*svp*svm + 8.E+0*pow(P_l,2)*P_qm*P_qp*svp*svm;
 
    kern[2][1] = (1.0/ denom)* kern[2][1]; 
    kern[2][2] =  4.E+0*P_P*l_l*qm_qp*svp*svm + 4.E+0*P_P*l_l*ssp*ssm
       - 8.E+0*P_P*l_qm*l_qp*svp*svm + 4.E+0*P_l*P_qm*l_qp*svp*svm + 
      4.E+0*P_l*P_qp*l_qm*svp*svm - 4.E+0*pow(P_l,2)*qm_qp*svp*svm - 
      4.E+0*pow(P_l,2)*ssp*ssm;
 
    kern[2][2] = (1.0/ denom)* kern[2][2]; 
    kern[2][3] =   - 8.E+0*P_P*P_qm*l_l*ssp*svm + 8.E+0*P_P*P_qp*l_l*
      svp*ssm + 8.E+0*pow(P_l,2)*P_qm*ssp*svm - 8.E+0*pow(P_l,2)*P_qp*
      svp*ssm;
 
    kern[2][3] = (1.0/ denom)* kern[2][3]; 

    denom =  - 1.6E+1*P_P*l_l + 1.6E+1*pow(P_l,2);

    kern[3][0] =   - 8.E+0*P_qm*l_qp*svp*svm + 8.E+0*P_qp*l_qm*svp*svm;
 
    kern[3][0] = (1.0/ denom)* kern[3][0]; 
    kern[3][1] =  8.E+0*P_P*P_l*l_qm*ssp*svm - 8.E+0*P_P*P_l*l_qp*svp*
      ssm - 8.E+0*pow(P_l,2)*P_qm*ssp*svm + 8.E+0*pow(P_l,2)*P_qp*svp*
      ssm;
 
    kern[3][1] = (1.0/ denom)* kern[3][1]; 
    kern[3][2] =  8.E+0*P_l*l_qm*ssp*svm - 8.E+0*P_l*l_qp*svp*ssm - 
      8.E+0*P_qm*l_l*ssp*svm + 8.E+0*P_qp*l_l*svp*ssm;
 
    kern[3][2] = (1.0/ denom)* kern[3][2]; 
    kern[3][3] =  1.6E+1*P_P*l_l*qm_qp*svp*svm - 1.6E+1*P_P*l_l*ssp*ssm
       - 3.2E+1*P_P*l_qm*l_qp*svp*svm + 3.2E+1*P_l*P_qm*l_qp*svp*svm + 
      3.2E+1*P_l*P_qp*l_qm*svp*svm - 1.6E+1*pow(P_l,2)*qm_qp*svp*svm + 
      1.6E+1*pow(P_l,2)*ssp*ssm - 3.2E+1*P_qm*P_qp*l_l*svp*svm;
 
    kern[3][3] = (1.0/ denom)* kern[3][3]; 


}


void kernelL_scalar(matCdoub& kern,
              const ArrayScalarProducts& sp,
              const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){


    // local variables
    array<Cdoub, 25> w;
    Cdoub denom;


    w[1]=P_P;
    w[2]=P_l;
    w[3]=p_p;
    w[4]=P_p;
    w[5]=l_l;
   w[6]=1.2E+1*ang1;
   w[7]=2.E+0*ang1;
   w[8]=w[3]*ang0;
   w[9]=w[5]*ang0;
   w[10]=w[8] - w[7] + w[9];
   w[11]=pow(w[4],2);
   w[10]=w[10]*w[11];
   w[12]=2.E+0*w[2];
   w[13]=w[12]*w[8];
   w[14]=w[9] - ang1;
   w[15]= - 3.E+0*w[8] - w[14];
   w[15]=w[4]*w[15];
   w[15]=w[15] + w[13];
   w[15]=w[2]*w[15];
   w[16]=w[1]*w[3];
   w[17]=ang1*w[16];
   w[10]=w[15] + w[17] + w[10];
   w[15]=4.E+0*w[2];
   w[10]=w[10]*w[15];
   w[17]=w[9] + ang1;
   w[17]=w[17]*w[5];
   w[17]= - w[17] + 2.E+0*ang2;
   w[18]= - 3.E+0*ang1 + w[8];
   w[18]=w[3]*w[18];
   w[18]=w[18] + w[17];
   w[18]=w[4]*w[18];
   w[19]=w[8] - w[9];
   w[7]=w[7] - w[19];
   w[7]=w[7]*w[12];
   w[20]=w[3]*w[7];
   w[18]=w[18] + w[20];
   w[18]=2.E+0*w[18];
   w[20]=w[19] + ang1;
   w[20]=w[20]*w[1];
   w[21]=2.E+0*ang0;
   w[22]=w[11]*w[21];
   w[22]= - w[20] + w[22];
   w[22]=w[4]*w[22];
   w[23]=4.E+0*w[11];
   w[24]= - ang0*w[23];
   w[21]=w[2]*w[4]*w[21];
   w[20]=w[21] + w[20] + w[24];
   w[20]=w[2]*w[20];
   w[20]=w[22] + w[20];
   w[15]=w[20]*w[15];
   w[20]=2.E+0*w[9];
   w[21]=w[20] - w[8];
   w[22]= - ang1 + w[21];
   w[22]=w[3]*w[22];
   w[17]=w[22] + w[17];
   w[17]=w[1]*w[17];
   w[19]= - ang1 + w[19];
   w[19]=w[19]*w[11];
   w[7]=w[4]*w[7];
   w[7]=w[7] + w[17] + 2.E+0*w[19];
   w[7]=2.E+0*w[7];
   w[8]=2.E+0*w[8] + w[20] - ang1;
   w[8]=w[4]*w[8];
   w[8]=w[8] - w[13];
   w[8]=w[8]*w[12];
   w[12]=ang1 + w[21];
   w[12]=w[3]*w[12];
   w[13]= - w[5]*w[14];
   w[12]=w[13] + w[12];
   w[12]=w[1]*w[12];
   w[9]= - w[9]*w[23];
   w[8]=w[8] + w[12] + w[9];
   w[8]=8.E+0*w[8];
   w[9]=w[16] - w[11];
   w[11]=4.E+0*w[9];
   w[12]= - w[4]*w[11];
   w[9]= - 1.6E+1*w[9];


    denom = 4.E+0;

    kern[0][0] = (1.0/ denom)* w[6];
 
    kern[0][1] = (1.0/ denom)*  0;
 
    kern[0][2] = (1.0/ denom)*  0;
 
    kern[0][3] = (1.0/ denom)*  0;
 

    denom = w[12];

    kern[1][0] = (1.0/ denom)*  0;
 
    kern[1][1] = (1.0/ denom)* w[10];
 
    kern[1][2] = (1.0/ denom)* w[18];
 
    kern[1][3] = (1.0/ denom)*  0;
 

    denom = w[11];

    kern[2][0] = (1.0/ denom)*  0;
 
    kern[2][1] = (1.0/ denom)* w[15];
 
    kern[2][2] = (1.0/ denom)* w[7];
 
    kern[2][3] = (1.0/ denom)*  0;
 

    denom = w[9];

    kern[3][0] = (1.0/ denom)*  0;
 
    kern[3][1] = (1.0/ denom)*  0;
 
    kern[3][2] = (1.0/ denom)*  0;
 
    kern[3][3] = (1.0/ denom)* w[8];
 

}


void kernelY_scalar(matCdoub& kern,
  const ArrayScalarProducts& sp,  const Cdoub& svp,
  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){


    // local variables
    array<Cdoub, 32> w;
    Cdoub denom;


    w[1]=qm_qp;
    w[2]=P_l;
    w[3]=P_qm;
    w[4]=P_qp;
    w[5]=l_qm;
    w[6]=l_qp;
    w[7]=l_l;
    w[8]=P_P;
   w[9]=ssm*ssp;
   w[10]=svm*svp;
   w[11]=w[10]*w[1];
   w[12]=w[9] - w[11];
   w[13]=4.E+0*w[12];
   w[14]=ssp*svm;
   w[15]=w[3]*w[14];
   w[16]=ssm*svp;
   w[17]=w[4]*w[16];
   w[18]=w[15] + w[17];
   w[19]=4.E+0*w[2];
   w[20]= - w[18]*w[19];
   w[14]=w[5]*w[14];
   w[16]=w[6]*w[16];
   w[21]=w[14] + w[16];
   w[22]= - 4.E+0*w[21];
   w[23]=w[10]*w[5];
   w[24]=w[23]*w[4];
   w[10]=w[10]*w[3];
   w[25]=w[10]*w[6];
   w[26]=w[24] - w[25];
   w[26]=8.E+0*w[26];
   w[27]= - w[7]*w[18];
   w[28]=w[2]*w[21];
   w[27]=w[27] + w[28];
   w[27]=4.E+0*w[27];
   w[9]=w[9] + w[11];
   w[11]=w[9]*w[2];
   w[24]=w[24] + w[25];
   w[11]=w[11] - w[24];
   w[11]=w[11]*w[2];
   w[25]=w[8]*w[7];
   w[28]= - w[9]*w[25];
   w[10]=w[10]*w[4];
   w[10]=2.E+0*w[10];
   w[29]=w[10]*w[7];
   w[28]=w[11] + w[29] + w[28];
   w[28]=w[28]*w[19];
   w[30]=w[7]*w[24];
   w[23]=w[23]*w[6];
   w[23]=2.E+0*w[23];
   w[31]= - w[2]*w[23];
   w[30]=w[30] + w[31];
   w[30]=4.E+0*w[30];
   w[31]=pow(w[2],2);
   w[25]=w[31] - w[25];
   w[31]=8.E+0*w[25];
   w[14]=w[14] - w[16];
   w[16]=w[14]*w[31];
   w[21]=w[8]*w[21];
   w[18]= - w[2]*w[18];
   w[18]=w[21] + w[18];
   w[18]=4.E+0*w[18];
   w[21]= - w[8]*w[24];
   w[10]=w[2]*w[10];
   w[10]=w[21] + w[10];
   w[10]=w[10]*w[19];
   w[9]=w[7]*w[9];
   w[9]= - w[23] + w[9];
   w[9]=w[8]*w[9];
   w[9]=w[9] - w[11];
   w[9]=4.E+0*w[9];
   w[11]=w[15] - w[17];
   w[15]=w[11]*w[31];
   w[17]=w[8]*w[14];
   w[21]= - w[2]*w[11];
   w[17]=w[17] + w[21];
   w[17]=8.E+0*w[2]*w[17];
   w[11]= - w[7]*w[11];
   w[14]=w[2]*w[14];
   w[11]=w[11] + w[14];
   w[11]=8.E+0*w[11];
   w[14]=w[2]*w[12];
   w[14]=2.E+0*w[24] + w[14];
   w[14]=w[2]*w[14];
   w[12]= - w[7]*w[12];
   w[12]= - w[23] + w[12];
   w[12]=w[8]*w[12];
   w[12]=w[14] - w[29] + w[12];
   w[12]=1.6E+1*w[12];
   w[14]=w[25]*w[19];
   w[19]= - 4.E+0*w[25];
   w[21]=1.6E+1*w[25];


    denom = 4.E+0;

    kern[0][0] = (1.0/ denom)* w[13];
 
    kern[0][1] = (1.0/ denom)* w[20];
 
    kern[0][2] = (1.0/ denom)* w[22];
 
    kern[0][3] = (1.0/ denom)*  - w[26];
 

    denom = w[14];

    kern[1][0] = (1.0/ denom)* w[27];
 
    kern[1][1] = (1.0/ denom)* w[28];
 
    kern[1][2] = (1.0/ denom)* w[30];
 
    kern[1][3] = (1.0/ denom)* w[16];
 

    denom = w[19];

    kern[2][0] = (1.0/ denom)* w[18];
 
    kern[2][1] = (1.0/ denom)* w[10];
 
    kern[2][2] = (1.0/ denom)* w[9];
 
    kern[2][3] = (1.0/ denom)* w[15];
 

    denom = w[21];

    kern[3][0] = (1.0/ denom)* w[26];
 
    kern[3][1] = (1.0/ denom)* w[17];
 
    kern[3][2] = (1.0/ denom)* w[11];
 
    kern[3][3] = (1.0/ denom)* w[12];
 


}


