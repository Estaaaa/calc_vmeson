#!/bin/bash

cp output/meson_scalar.h.txt ../../../../../../source/bse/Kernel/scalar/meson_scalar.h
cp output/meson_scalar_fpi.h.txt ../../../../../../source/bse/Kernel/scalar/meson_scalar_fpi.h
cp output/meson_scalar_kernelall.h.txt ../../../../../../source/bse/Kernel/scalar/meson_scalar_kernel_all.h
cp output/meson_scalar_Renorm.h.txt ../../../../../../source/bse/Kernel/scalar/meson_scalar_Renorm.h
cp output/meson_scalar_Renorm_fv.h.txt ../../../../../../source/bse/Kernel/scalar/meson_scalar_Renorm_fv.h
cp output/meson_scalar_Y.h.txt ../../../../../../source/bse/Kernel/scalar/meson_scalar_Y.h
