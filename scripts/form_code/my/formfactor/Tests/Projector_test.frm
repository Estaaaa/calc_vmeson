* DEFAULTS
#-
#: continuationlines 50
#: indentspace 8


* DECLARATIONS.
CF gluon;
F quark, T1, T2, G1, ..., G8, Lp, Lm, Meson, Vertex;
CF PR;
CF sA, sB;
CF DZ, Z;
S x, n;

* Indices (Lorentz)
I al, al1, ..., al8, be, be1, ..., be8, mu, mu1, ..., mu8, nu, nu1, ..., nu8, ro, ro1, ..., ro8, si, si1, ..., si8, la, la1, ..., la8;

* Indices (Summation. We could define these as symbols instead)
I i, i1, ..., i8, j, j1, ..., j8, m, m1, ..., m8;
I alpha, alp;

* (four)-qphvs. The temporal vector u is for finite temperature. Cannot use w here
V bigP, P,  bigQ, p, p1, ..., p8, k, k1, ..., k8, q, q1, ..., q8, l, l1, ..., l8, t, t1, ..., t8, u, u1, ..., u8, v, v1, ..., v8, w1, ..., w8;
V qm, qp, q, p, bigPhat, bigQhat;
V rp, rm, Q, Qp;

cfunction DZ, DG;

function Projector, Amplitude, ConjugateAmplitude;
cfunction sum;
vector P;
symbol svp, ssp, svm, ssm, x, z, ang0,...,ang5;
symbol Z2;
symbol A3 B3, A1, B1, A2, B2, f1, ... ,f8, XC;
function AxProj, Rtrans;

********************************************************************************
** Define the whole BS kernel                                                 **
********************************************************************************

**Cross check for Projector
L Ptest = AxProj(Q,Qp,mu,nu,alp )* AxProj(Q,Qp,mu,nu,alp );

L test = e_(mu,nu,Q,Qp) *  e_(mu,nu,Q,Qp) ;
L test2 = Rtrans(Q,Qp, mu,nu)  * Rtrans(Q,Qp, mu,nu);

********************************************************************************
** Perform the substitutions                                                  **
********************************************************************************

** SUBSTITUTIONS.



.sort

id Rtrans(q?, p?, mu?, nu?) =  - d_(mu,nu) + 1/XC * (q.p * (q(mu)*p(nu)+q(nu)*p(mu)) -q.q *p(mu)*p(nu) - p.p *q(mu)*q(nu));

id AxProj(q?,p?, mu?, nu?, alp?) = e_(ro, nu, la, alp) * ( (q.q * d_(ro,mu) - q(ro)*q(mu)) * p(la) - (p.p * d_(ro,mu) - p(ro)*p(mu)) * q(la) );




* Perform any Dirac traces. Must do this for multiple spin lines if present.
Trace4,1;
.sort
Contract;
.sort
********************************************************************************
** Factorise the angular integrals                                            **
********************************************************************************


*Replacing the epsilon tensor by 0 when it's 0, since we are not replacing the explicit meaning of qm, qp.
.sort
id e_(P,l,qm,qp) = 0;
id e_(P,p,l,qm) = 0;
id e_(P,p,l,qp) = 0;
id e_(P,p,qm,qp) = 0;
id e_(p,l,qm,qp) = 0;

id XC = (Q.Qp*Q.Qp-Q.Q*Qp.Qp);


.sort

* Set the optimization level. O[0,1,2,3]
*Format O4, stats=on;
*.sort


* Assign variables "w" for the optimisation step
*ExtraSymbols,array,w;

* Change the output mode to that of Fortran 95
*Format C, .d0;
*Format float;
*Format Mathematica

********************************************************************************
**WRITING OUT STUFF
********************************************************************************
********************************************************************************

print Ptest;

#define FILENAME "<output/projector_test.txt>"
#write 'FILENAME' "P_P *P_P = %e", test
#write 'FILENAME' ""
#write 'FILENAME' "P_S *P_S = %e", test2
#write 'FILENAME' ""
#write 'FILENAME' "P_A *P_A = %e", Ptest



.end
