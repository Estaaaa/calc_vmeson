* DEFAULTS
#-
#: continuationlines 50
#: indentspace 4


* DECLARATIONS.
CF gluon;
F quark, T1, T2, G1, ..., G4, Lp, Lm, Meson, Vertex;
CF PR;
CF sA, sB;
CF DZ, Z;
S x, n;

* Indices (Lorentz)
I al, al1, ..., al4, be, be1, ..., be4, mu, mu1, ..., mu4, nu, nu1, ..., nu4, ro, ro1, ..., ro4, si, si1, ..., si4, la, la1, ..., la4;

* Indices (Summation. We could define these as symbols instead)
I i, i1, ..., i4, j, j1, ..., j4, m, m1, ..., m4;

* (four)-qphvs. The temporal vector u is for finite temperature. Cannot use w here
V bigP, P,  bigQ, p, p1, ..., p4, k, k1, ..., k4, q, q1, ..., q4, l, l1, ..., l4, t, t1, ..., t4, u, u1, ..., u4, v, v1, ..., v4, w1, ..., w4;
V qm, qp, q, p, bigPhat, bigQhat;
V rp, rm, Q, Qp;

cfunction DZ, DG;
function Projector, Amplitude, ConjugateAmplitude;
function CAmptff, Amptff, Ptff, DPtff, formfactor;
cfunction sum;
vector P;
symbol svp, ssp, svm, ssm, x, z, ang0,...,ang5;
symbol Z2, XC;
symbol A3 B3, A1, B1, A2, B2, f1, ... ,f4;

********************************************************************************
** Define the whole BS kernel                                                 **
********************************************************************************
* Trace of the BS wavefunction subject to the rainbow-ladder truncation.
local formfactorall = 0;
#do i1 = 1, 12
#do i2 = 1, 12
#do i3 = 1, 5

*Set-up with the amplitude not the wavefunction chi:
*-this takes too long to generate though.
*L formfactorall = formfactorall +     x^{('i1'-1)*12+'i2'}
*                                          * e_(mu,nu,Q,Qp)
*                                          * Vertex( 'i1', mu, rp, -Q )
*                                          * quark(k1)
*                                          * Meson(l,P)
*                                          * quark(k2)
*                                          * Vertex( 'i2', nu, rm, Qp )
*                                          * quark(k3);

***** Normal version ****************

*L formfactorall = formfactorall +     x^{( 'i3' + 5*('i2'-1) + 5*12*('i1'-1) )}
*                                          * Ptff('i3',Q, Qp, mu, nu)
*                                          * Vertex( 'i2', mu, rp, Qp )
*                                          * Meson(l, P)
*                                          * Vertex( 'i1', nu, rm, -Q )
*                                          * quark(k3);
*


*Easy set up:
*L formfactorall = formfactorall +     x^{('i1'-1)*12+'i2'}
*                                          * e_(mu,nu,Q,Qp)
*                                          * g_(1,mu)
*                                          * quark(k1)
*                                          * g5_(1)  * (f1)
*                                          * quark(k2)
*                                          * g_(1,nu)
*                                          * quark(k3);
*



.sort
#enddo
#enddo
#enddo

*Cross check for Projector
*L test = e_(mu,nu,Q,Qp) *  e_(mu,nu,Q,Qp) ;
*L test2 = Rtrans(Q,Qp, mu,nu)  * Rtrans(Q,Qp, mu,nu);
L test = Ptff(2,Q, Qp, mu, nu)* Vertex( 5, mu, rp, Qp )* Meson(l, P)* Vertex( 6, nu, rm, -Q )* quark(k3);


********************************************************************************
** Perform the substitutions                                                  **
********************************************************************************

** SUBSTITUTIONS.



.sort
id quark(p?)         = -i_*g_(1,p)*sA(p) + gi_(1)*sB(p);
id gluon(p?,mu?,nu?) = DZ(p)*PR(mu,nu,p);

id PR(mu?,nu?,p?) = d_(mu,nu) - p(mu)*p(nu)/p.p;

*id Rtrans(q?, p?, mu?, nu?) =  d_(mu,nu) + 1/XC * (q.p * (q(mu)*p(nu)+q(nu)*p(mu)) -q.q *p(mu)*p(nu) - p.p *q(mu)*q(nu));

id Meson(q?,P?) = f1 * Amplitude(1, q, P) + f2 *  Amplitude(2, q, P) + f3 * Amplitude(3, q, P) + f4 * Amplitude(4, q, P);
*id Meson(1, q?, P?) =  f1 *Amplitude(1,q,P);
*id Meson(2, q?, P?) =  f2 *Amplitude(2,q,P);
*id Meson(3, q?, P?) =  f3 *Amplitude(3,q,P);
*id Meson(4, q?, P?) =  f4 *Amplitude(4,q,P);

*id formfactor(q?,P?,mu?,nu?) = Amptff(1,q,P,mu,nu) + Amptff(2,q,P,mu,nu) +Amptff(3,q,P,mu,nu) + Amptff(4,q,P,mu,nu)+ Amptff(5,q,P,mu,nu);

* This included the Basis and the Projectors: Can be changed to pscalar, qphv, qphv
#include include/BP_qphv.h
#include include/BP_scalar_meson.h
#include include/scalar_tff_with.h

.sort

id sA(k3)  = A3;
id sB(k3)  = B3;
id sA(k1)  = A1;
id sB(k1)  = B1;
id sA(k2)  = A2;
id sB(k2)  = B2;

*id DZ(k)  = 1; * We are collecting angular integrals
*id k.k^n? = z^n;
*id k = l - p;
*.sort
*id k3= l + p/2;
.sort


**Replacements when testing for an easier set-up
*id k1 =  l + P/2;
*id k2 =  l- P/2;
*id Q =  (p+P)/2;
*id Qp = (p-P)/2;
*id P = Q-Qp;
.sort

* Perform any Dirac traces. Must do this for multiple spin lines if present.
Trace4,1;
.sort
Contract;
.sort
********************************************************************************
** Factorise the angular integrals                                            **
********************************************************************************

.sort

*id p.l = (l.l + p.p - z)/2;
*.sort
*id p.l = (l.l + p.p - z)/2;
*.sort
*id p.l = (l.l + p.p - z)/2;
*.sort
*id p.l = (l.l + p.p - z)/2;
*.sort
*id p.l = (l.l + p.p - z)/2;
*.sort
*#do i = 6,1,-1
*id z^'i' = ang{'i'-1};
*#enddo
.sort


* Id Statments for the usage as a C++ code

id bigP = P;
id bigP.bigP^n? = P.P^n;
*id 1/bigP.bigP = 1/P.P;
*id bigP^-1 = P^-1;
*id q = l;


*id Q = (p+P)/2;
*id Qp = (p-P)/2;
*.sort

*doesnt needed anymore?
*id p?.k?   = p.k;
*id p?.k?^-1= p.k^-1;


.sort

**WRITING OUT STUFF
********************************************************************************
********************************************************************************


* Set the optimization level. O[0,1,2,3]
*Format O4, stats=on;
*.sort


* Assign variables "w" for the optimisation step
*ExtraSymbols,array,w;

* Change the output mode to that of Fortran 95
Format C, .d0;
Format float;

********************************************************************************
**WRITING OUT STUFF
********************************************************************************
********************************************************************************

*print;
print test;

.end
