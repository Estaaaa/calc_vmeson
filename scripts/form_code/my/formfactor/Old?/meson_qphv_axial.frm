* DEFAULTS
#-
#: continuationlines 50
#: indentspace 8


* DECLARATIONS.
CF gluon;
F quark, T1, T2, G1, ..., G8, Lp, Lm, Meson, Vertex;
CF PR;
CF sA, sB;
CF DZ, Z;
S x, n;

* Indices (Lorentz)
I al, al1, ..., al8, be, be1, ..., be8, mu, mu1, ..., mu8, nu, nu1, ..., nu8, ro, ro1, ..., ro8, si, si1, ..., si8, la, la1, ..., la8;

* Indices (Summation. We could define these as symbols instead)
I i, i1, ..., i8, j, j1, ..., j8, m, m1, ..., m8;
I alpha, alp;

* (four)-qphvs. The temporal vector u is for finite temperature. Cannot use w here
V bigP, P,  bigQ, p, p1, ..., p8, k, k1, ..., k8, q, q1, ..., q8, l, l1, ..., l8, t, t1, ..., t8, u, u1, ..., u8, v, v1, ..., v8, w1, ..., w8;
V qm, qp, q, p, bigPhat, bigQhat;
V rp, rm, Q, Qp;

cfunction DZ, DG;

function Projector, Amplitude, ConjugateAmplitude;
cfunction sum;
vector P;
symbol svp, ssp, svm, ssm, x, z, ang0,...,ang5;
symbol Z2;
symbol A3 B3, A1, B1, A2, B2, f1, ... ,f8;
function AxProj;

********************************************************************************
** Define the whole BS kernel                                                 **
********************************************************************************
* Trace of the BS wavefunction subject to the rainbow-ladder truncation.
local formfactorall = 0;
#do i1 = 1, 12
#do i2 = 1, 12

*Set-up with the amplitude not the wavefunction chi:
*-this takes too long to generate though.
*L formfactorall = formfactorall +     x^{('i1'-1)*12+'i2'}
*                                          * e_(mu,nu,Q,Qp)
*                                          * Vertex( 'i1', mu, rp, -Q )
*                                          * quark(k1)
*                                          * Meson(l,P)
*                                          * quark(k2)
*                                          * Vertex( 'i2', nu, rm, Qp )
*                                          * quark(k3);

***** Normal version ****************
#do i3= 1,8
L formfactorall = formfactorall +     x^{('i1'-1)*12+'i2'}
                                          * AxProj(Q,Qp,mu,nu,alp )
                                          * Vertex( 'i2', mu, rp, Qp )
                                          * Meson( 'i3' , l,P, alp)
                                          * Vertex( 'i1', nu, rm, -Q )
                                          * quark(k3);

#enddo

*Easy set up:
*L formfactorall = formfactorall +     x^{('i1'-1)*12+'i2'}
*                                          * e_(mu,nu,Q,Qp)
*                                          * g_(1,mu)
*                                          * quark(k1)
*                                          * g5_(1)  * (f1)
*                                          * quark(k2)
*                                          * g_(1,nu)
*                                          * quark(k3);
*


.sort
#enddo
#enddo


**Cross check for Projector
L Ptest = AxProj(Q,Qp,mu,nu,alp )* AxProj(Q,Qp,mu,nu,alp );

********************************************************************************
** Perform the substitutions                                                  **
********************************************************************************

** SUBSTITUTIONS.



.sort
id quark(p?)         = -i_*g_(1,p)*sA(p) + gi_(1)*sB(p);
id gluon(p?,mu?,nu?) = DZ(p)*PR(mu,nu,p);

id PR(mu?,nu?,p?) = d_(mu,nu) - p(mu)*p(nu)/p.p;

id AxProj(q?,p?, mu?, nu?, alp?) = e_(ro, nu, la, alp) * ( (q.q * d_(ro,mu) - q(ro)*q(mu)) * p(la) - (p.p * d_(ro,mu) - p(ro)*p(mu)) * q(la)  );

*id Meson(q?,P?, mu?) = f1 * Amplitude(1, mu, q, P) + f2 *  Amplitude(2, mu, q, P) + f3 * Amplitude(3, mu, q, P) + f8 * Amplitude(8, mu, q, P) + f5 * Amplitude(5, mu, q, P) + f6 *  Amplitude(6, mu, q, P) + f7 * Amplitude(7, mu, q, P) + f8 * Amplitude(8, mu, q, P);
id Meson(1,q?,P?,mu?) = f1 * Amplitude(1,mu,q,P);
id Meson(2,q?,P?,mu?) = f2 * Amplitude(2,mu,q,P);
id Meson(3,q?,P?,mu?) = f3 * Amplitude(3,mu,q,P);
id Meson(4,q?,P?,mu?) = f4 * Amplitude(4,mu,q,P);
id Meson(5,q?,P?,mu?) = f5 * Amplitude(5,mu,q,P);
id Meson(6,q?,P?,mu?) = f6 * Amplitude(6,mu,q,P);
id Meson(7,q?,P?,mu?) = f7 * Amplitude(7,mu,q,P);
id Meson(8,q?,P?,mu?) = f8 * Amplitude(8,mu,q,P);

* This included the Basis and the Projectors: Can be changed to pscalar, qphv, qphv
#include include/BP_qphv.h
#include include/BP_axialv_meson.h

.sort

id sA(k3)  = A3;
id sB(k3)  = B3;
id sA(k1)  = A1;
id sB(k1)  = B1;
id sA(k2)  = A2;
id sB(k2)  = B2;

*id DZ(k)  = 1; * We are collecting angular integrals
*id k.k^n? = z^n;
*id k = l - p;
*.sort
*id k3= l + p/2;
.sort


**Replacements when testing for an easier set-up
*id k1 =  l + P/2;
*id k2 =  l- P/2;
*id Q =  (p+P)/2;
*id Qp = (p-P)/2;
*id P = Q-Qp;
.sort

* Perform any Dirac traces. Must do this for multiple spin lines if present.
Trace4,1;
.sort
Contract;
.sort
********************************************************************************
** Factorise the angular integrals                                            **
********************************************************************************

.sort

*id p.l = (l.l + p.p - z)/2;
*.sort
*id p.l = (l.l + p.p - z)/2;
*.sort
*id p.l = (l.l + p.p - z)/2;
*.sort
*id p.l = (l.l + p.p - z)/2;
*.sort
*id p.l = (l.l + p.p - z)/2;
*.sort
*#do i = 6,1,-1
*id z^'i' = ang{'i'-1};
*#enddo
.sort


* Id Statments for the usage as a C++ code

id bigP = P;
id bigP.bigP^n? = P.P^n;
*id 1/bigP.bigP = 1/P.P;
*id bigP^-1 = P^-1;
*id q = l;


*id Q = (p+P)/2;
*id Qp = (p-P)/2;
*.sort

*doesnt needed anymore?
*id p?.k?   = p.k;
*id p?.k?^-1= p.k^-1;


*Replacing the epsilon tensor by 0 when it's 0, since we are not replacing the explicit meaning of qm, qp.
.sort
id e_(P,l,qm,qp) = 0;
id e_(P,p,l,qm) = 0;
id e_(P,p,l,qp) = 0;
id e_(P,p,qm,qp) = 0;
id e_(p,l,qm,qp) = 0;


.sort

* Set the optimization level. O[0,1,2,3]
Format O4, stats=on;
.sort


* Assign variables "w" for the optimisation step
ExtraSymbols,array,w;

* Change the output mode to that of Fortran 95
Format C, .d0;
Format float;

********************************************************************************
**WRITING OUT STUFF
********************************************************************************
********************************************************************************

*print;

********************************************************************************
**Writing the kernel without optimization to disk                             **
********************************************************************************

* OUTPUT FILENAME
#define FILENAME "<output/axial_formfactor.txt>"

* Collect terms in "x"
B x;
.sort
#optimize formfactorall
B x;
.sort



* Extract coefficient of "x"
#do i = 1, 188
L formfactorall'i' = formfactorall[x^'i'];
#enddo
.sort

B f1, f2, f3, f8;
.sort


#write 'FILENAME' "#include <typedefs.h>"
#write 'FILENAME' "#include <routing_amount_scalars.h>"
#write 'FILENAME' "#include <extra_def.h>"
#write 'FILENAME' "#include <tff_kernel_axial.h> "
#write 'FILENAME' "#include <fv.h>"
#write 'FILENAME' ""
#write 'FILENAME' "void kernel_formfactor_axial(VecCdoub& kern,"
#write 'FILENAME' "const fv<Cdoub>& P, const fv<Cdoub>& Q, const fv<Cdoub>& Qp,"
#write 'FILENAME' "const fv<Cdoub>& p, const fv<Cdoub>& l, const fv<Cdoub>& rp,"
#write 'FILENAME' "const fv<Cdoub>& rm , const fv<Cdoub> & k3,"
#write 'FILENAME' "const VecCdoub& meson, "
#write 'FILENAME' "const Cdoub& A3, const Cdoub& B3){"
#write 'FILENAME' ""
#write 'FILENAME' ""
#write 'FILENAME' "//fourvector products"
#write 'FILENAME' "Cdoub P_P = P*P; Cdoub P_Q = P*Q; Cdoub P_Qp = P*Qp;"
#write 'FILENAME' "Cdoub P_k3 = P*k3; Cdoub P_l = P*l; Cdoub P_rp = P*rp;"
#write 'FILENAME' "Cdoub P_rm = P*rm; Cdoub Q_Q = Q*Q; Cdoub Q_Qp = Q*Qp;"
#write 'FILENAME' "Cdoub k3_Q = Q*k3; Cdoub l_Q = Q*l; Cdoub rp_Q = Q*rp;"
#write 'FILENAME' "Cdoub rm_Q = Q*rm; Cdoub Qp_Qp = Qp*Qp; Cdoub k3_Qp = Qp*k3;"
#write 'FILENAME' "Cdoub l_Qp = Qp*l; Cdoub rp_Qp = Qp*rp; Cdoub rm_Qp = Qp*rm;"
#write 'FILENAME' "Cdoub k3_k3 = k3*k3; Cdoub k3_l = k3*l; Cdoub k3_rp = k3*rp;"
#write 'FILENAME' "Cdoub k3_rm = k3*rm; Cdoub l_l = l*l; Cdoub l_rp = l*rp;"
#write 'FILENAME' "Cdoub l_rm = l*rm; Cdoub rp_rp = rp*rp; Cdoub rp_rm = rp*rm;"
#write 'FILENAME' "Cdoub rm_rm = rm*rm; "
#write 'FILENAME' ""
#write 'FILENAME' "Cdoub f1=meson[0]; Cdoub f2=meson[1]; Cdoub f3=meson[2];"
#write 'FILENAME' "Cdoub f4=meson[3]; Cdoub f5=meson[8]; Cdoub f6=meson[5];"
#write 'FILENAME' "Cdoub f7=meson[6]; Cdoub f8=meson[7]; "
#write 'FILENAME' ""
#write 'FILENAME' ""
#write 'FILENAME' "    // local variables"
#write 'FILENAME' "    array<Cdoub, {'optimmaxvar_'+1}> w;"
#write 'FILENAME' ""
#write 'FILENAME' "%O"
#do i = 1, 144
#write 'FILENAME' "    kern[%s] =  %e ", {'i'-1} , formfactorall{'i'}
#enddo
#write 'FILENAME' ""
#write 'FILENAME' "}"
#write 'FILENAME' ""


#clearoptimize





.end
