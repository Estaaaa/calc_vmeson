******* Substitution for the Base **********

id Amptff(1, q?, P?, mu?, nu?) = d_(mu,nu);
id Amptff(2, q?, P?, mu?, nu?) = P(mu)*q(nu);
id Amptff(3, q?, P?, mu?, nu?) = -(d_(mu,nu)*q.P)+P(nu)*q(mu);
id Amptff(4, q?, P?, mu?, nu?) = d_(mu,nu)*P.P*q.q-q.q*P(mu)*P(nu)+q.P*P(mu)*q(nu)-P.P*q(mu)*q(nu);


id CAmptff(1, q?, P?, mu?, nu?) = d_(mu,nu);
id CAmptff(2, q?, P?, mu?, nu?) = P(mu)*q(nu);
id CAmptff(3, q?, P?, mu?, nu?) = -(d_(mu,nu)*q.P)+P(nu)*q(mu);
id CAmptff(4, q?, P?, mu?, nu?) = d_(mu,nu)*P.P*q.q-q.q*P(mu)*P(nu)+q.P*P(mu)*q(nu)-P.P*q(mu)*q(nu);


id Ptff(1, q?, P?, mu?, nu?) = P.P*q(mu)*q(nu)+P(mu)*(q.q*P(nu)-2*q.P*q(nu));
id Ptff(2, q?, P?, mu?, nu?) = -(P.P*q.P*q(mu)*q(nu))+q.q*P(mu)*(-(q.P*P(nu))+2*P.P*q(nu));
id Ptff(3, q?, P?, mu?, nu?) = q(mu)*(((q.P)^2+2*P.P*q.q)*P(nu)-3*P.P*q.P*q(nu))+q.P*(d_(mu,nu)*(-(q.P)^2+P.P*q.q)+3*P(mu)*(-(q.q*P(nu))+q.P*q(nu)));
id Ptff(4, q?, P?, mu?, nu?) = -(P.P*q(mu)*(-3*q.P*q.q*P(nu)+((q.P)^2+2*P.P*q.q)*q(nu)))+q.q*(d_(mu,nu)*P.P*(-(q.P)^2+P.P*q.q)+P(mu)*(-(((q.P)^2+2*P.P*q.q)*P(nu))+3*P.P*q.P*q(nu)));


id DPtff(1, q?, P?, mu?, nu?) = -8*(q.P)^2+8*P.P*q.q; 
id DPtff(2, q?, P?, mu?, nu?) = 8*P.P*q.q*(-(q.P)^2+P.P*q.q);
id DPtff(3, q?, P?, mu?, nu?) = 8*((q.P)^2-P.P*q.q)^2;
id DPtff(4, q?, P?, mu?, nu?) = 8*P.P*q.q*((q.P)^2-P.P*q.q)^2;
