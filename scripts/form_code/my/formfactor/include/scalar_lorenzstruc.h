******* Substitution for the Base **********
 
id LorenzT(1, p1?, p2?, mu?, nu?) = d_(mu,nu); 
id LorenzT(2, p1?, p2?, mu?, nu?) = (p1(mu)-p2(mu))*(p1(nu)-p2(nu)); 
id LorenzT(3, p1?, p2?, mu?, nu?) = ((p1(mu)+p2(mu))*(p1(nu)+p2(nu)))/4; 
id LorenzT(4, p1?, p2?, mu?, nu?) = (2*p1(mu)*p1(nu)-2*p2(mu)*p2(nu))/(p1.p1-p2.p2); 
id LorenzT(5, p1?, p2?, mu?, nu?) = p1(nu)*p2(mu)-p1(mu)*p2(nu); 

 
