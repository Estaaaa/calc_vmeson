******* Substitution for the Base **********
 
id Amptff(1, q?, P?, mu?, nu?) = d_(mu,nu); 
id Amptff(2, q?, P?, mu?, nu?) = P(mu)*q(nu); 
id Amptff(3, q?, P?, mu?, nu?) = P(mu)*P(nu)+q(mu)*q(nu); 
id Amptff(4, q?, P?, mu?, nu?) = -(d_(mu,nu)*q.P)+P(nu)*q(mu); 
id Amptff(5, q?, P?, mu?, nu?) = d_(mu,nu)*P.P*q.q-q.q*P(mu)*P(nu)+q.P*P(mu)*q(nu)-P.P*q(mu)*q(nu); 

 
id CAmptff(1, q?, P?, mu?, nu?) = d_(mu,nu); 
id CAmptff(2, q?, P?, mu?, nu?) = P(mu)*q(nu); 
id CAmptff(3, q?, P?, mu?, nu?) = P(mu)*P(nu)+q(mu)*q(nu); 
id CAmptff(4, q?, P?, mu?, nu?) = -(d_(mu,nu)*q.P)+P(nu)*q(mu); 
id CAmptff(5, q?, P?, mu?, nu?) = d_(mu,nu)*P.P*q.q-q.q*P(mu)*P(nu)+q.P*P(mu)*q(nu)-P.P*q(mu)*q(nu); 

 
id Ptff(1, q?, P?, mu?, nu?) = (P.P)^2*q(mu)*q(nu)-P(mu)*((q.q)^2*P(nu)+q.P*(P.P-q.q)*q(nu)); 
id Ptff(2, q?, P?, mu?, nu?) = q.P*q(mu)*q(nu)+P(mu)*(-(q.P*P(nu))+(P.P-q.q)*q(nu)); 
id Ptff(3, q?, P?, mu?, nu?) = q.q*P(mu)*P(nu)-P.P*q(mu)*q(nu); 
id Ptff(4, q?, P?, mu?, nu?) = q(mu)*(((q.P)^2+2*P.P*q.q)*P(nu)-3*P.P*q.P*q(nu))+q.P*(d_(mu,nu)*(-(q.P)^2+P.P*q.q)+3*P(mu)*(-(q.q*P(nu))+q.P*q(nu))); 
id Ptff(5, q?, P?, mu?, nu?) = d_(mu,nu)*(P.P-q.q)*(-(q.P)^2+P.P*q.q)+P(mu)*(-((2*(q.P)^2+(P.P-3*q.q)*q.q)*P(nu))+3*q.P*(P.P-q.q)*q(nu))+q(mu)*(3*q.P*(P.P-q.q)*P(nu)+(2*(q.P)^2+P.P*(-3*P.P+q.q))*q(nu)); 

 
id DPtff(1, q?, P?, mu?, nu?) = 4*(P.P-q.q)*(-(q.P)^2+P.P*q.q); 
id DPtff(2, q?, P?, mu?, nu?) = 4*(P.P-q.q)*(-(q.P)^2+P.P*q.q); 
id DPtff(3, q?, P?, mu?, nu?) = 4*(P.P-q.q)*(-(q.P)^2+P.P*q.q); 
id DPtff(4, q?, P?, mu?, nu?) = 8*((q.P)^2-P.P*q.q)^2; 
id DPtff(5, q?, P?, mu?, nu?) = 8*(P.P-q.q)*((q.P)^2-P.P*q.q)^2; 
