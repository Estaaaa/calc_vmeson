* DEFAULTS
#-
#: continuationlines 50
#: indentspace 8


* DECLARATIONS.
CF gluon;
F quark, T1, T2, G1, ..., G8, Lp, Lm;
CF PR;
CF sA, sB;
CF DZ, Z;
S x, n;

* Indices (Lorentz)
I al, al1, ..., al8, be, be1, ..., be8, mu, mu1, ..., mu8, nu, nu1, ..., nu8, ro, ro1, ..., ro8, si, si1, ..., si8, la, la1, ..., la8;

* Indices (Summation. We could define these as symbols instead)
I i, i1, ..., i8, j, j1, ..., j8, m, m1, ..., m8;

* (four)-qphv_transs. The temporal vector u is for finite temperature. Cannot use w here
V bigP, P,  bigQ, p, p1, ..., p8, k, k1, ..., k8, q, q1, ..., q8, l, l1, ..., l8, t, t1, ..., t8, u, u1, ..., u8, v, v1, ..., v8, w1, ..., w8;
V qm, qp, q, p, bigPhat, bigQhat;

cfunction DZ, DG;

function Projector, Amplitude, ConjugateAmplitude;
cfunction sum;
vector P;
symbol svp, ssp, svm, ssm, x, z, ang0,...,ang5;
symbol Z2;

********************************************************************************
** Define the whole BS kernel                                                 **
********************************************************************************
* Trace of the BS wavefunction subject to the rainbow-ladder truncation.
local kernelall = 0;
#do i1 = 1, 8
#do i2 = 1, 8
L kernelall = kernelall + z*z *x^{   ('i1'-1)*8+'i2'} * Projector('i1',mu1 ,p ,bigP)
                                                  * g_(1,mu)
                                                  * quark(qp)
                                                  * Amplitude('i2',mu1, l, bigP)
                                                  * g_(1,nu)
                                                  *  quark(qm)
                                                  * gluon(k,mu,nu);
.sort
#enddo
#enddo

* Denominators of the projection and orthogonality check
#do i1  = 1, 8
#do i2  = 1, 8
L kernelall = kernelall+       x^{16+('i1'-1)*8+'i2'} * Projector('i1',mu, p, bigP)
                                                  * Amplitude('i2', mu, p, bigP);
.sort
#enddo
#enddo

********************************************************************************
** Define the trace of the BS kernel - divided in L and Y            L       **
********************************************************************************
* Trace of the BS wavefunction subject to the rainbow-ladder truncation.
local kernelL = 0;
#do i1 = 1, 8
#do i2 = 1, 8
L kernelL = kernelL + z*z *x^{   ('i1'-1)*8+'i2'} * Projector('i1',mu1 ,p ,bigP)
                                                  * g_(1,mu)
                                                  * Amplitude('i2',mu1, l, bigP)
                                                  * g_(1,nu)
                                                  * gluon(k,mu,nu);
.sort
#enddo
#enddo

* Denominators of the projection and orthogonality check
#do i1  = 1, 8
#do i2  = 1, 8
L kernelL = kernelL+       x^{16+('i1'-1)*8+'i2'} * Projector('i1',mu, p, bigP)
                                                  * Amplitude('i2', mu, p, bigP);
.sort
#enddo
#enddo


********************************************************************************
** Define the quark rotation matrix                Y                          **
********************************************************************************

* Projection for attaching quarks to the amplitude, thus forming the wavefunction.
local kernelY = 0;
#do i1 = 1, 8
#do i2 = 1, 8
L kernelY = kernelY +       x^{   ('i1'-1)*8+'i2'} * Projector('i1',mu, l, bigP)
                                                   * quark(qp) * Amplitude('i2',mu, l, bigP)
                                                   * quark(qm);
.sort
#enddo
#enddo

* Denominators of the projection and orthogonality check
#do i1  = 1, 8
#do i2  = 1, 8
L kernelY = kernelY +       x^{16+('i1'-1)*8+'i2'} * Projector('i1',mu, l, bigP)
                                                   * Amplitude('i2',mu, l, bigP);
.sort
#enddo
#enddo



********************************************************************************
** Define the normalisation integral                                          **
********************************************************************************
L normalisation = 0;
#do i1 = 1, 8
#do i2 = 1, 8
L normalisation = normalisation + x^{   ('i1'-1)*8+'i2'} * ConjugateAmplitude('i1', mu, l, -bigP)
                                                         * quark(qp)
                                                         * Amplitude('i2', mu, l, bigP)
                                                         * quark(qm);
.sort
#enddo
#enddo

********************************************************************************
** Define the decay rate integral                                          **
********************************************************************************
L fpi = 0;
#do i1 = 1, 8
L fpi = fpi + x^{('i1')}    * (-i_)            * Amplitude('i1',mu, l, -bigP)
                                               * quark(qp)
                                               * g_(1,mu)
                                               * quark(qm);
.sort
#enddo

********************************************************************************
** Define the Projection on the inhomogenous term I                         **
********************************************************************************

* Projection for attaching quarks to the amplitude, thus forming the wavefunction.
local kernelI = 0;
#do i1 = 1, 8
L kernelI = kernelI +       x^{('i1')}               * Projector('i1',mu, p, bigP)
                                                     * i_* g_(1,mu);
.sort
#enddo

* Denominators of the projection and orthogonality check
#do i1  = 1, 8
L kernelI = kernelI +       x^{8+('i1')}            * Projector('i1',mu, p, bigP)
                                                   * Amplitude('i1',mu, p, bigP);
.sort
#enddo


********************************************************************************
** Perform the substitutions                                                  **
********************************************************************************

** SUBSTITUTIONS.
* This included the Basis and the Projectors: Can be changed to pscalar, qphv_trans, qphv_trans
#include include/BP_qphv_trans.h


.sort
id quark(p?)         = -i_*g_(1,p)*sA(p) + gi_(1)*sB(p);
id gluon(p?,mu?,nu?) = DZ(p)*PR(mu,nu,p);

id PR(mu?,nu?,p?) = d_(mu,nu) - p(mu)*p(nu)/p.p;

.sort
id sA(qp)  = svp;
id sB(qp)  = ssp;
id sA(qm)  = svm;
id sB(qm)  = ssm;

id DZ(k)  = 1; * We are collecting angular integrals
id k.k^n? = z^n;
id k = l - p;
.sort

* Perform any Dirac traces. Must do this for multiple spin lines if present.
Trace8,1;
Contract;
********************************************************************************
** Factorise the angular integrals                                            **
********************************************************************************

.sort

id p.l = (l.l + p.p - z)/2;
.sort
id p.l = (l.l + p.p - z)/2;
.sort
id p.l = (l.l + p.p - z)/2;
.sort
id p.l = (l.l + p.p - z)/2;
.sort
id p.l = (l.l + p.p - z)/2;
.sort
#do i = 6,1,-1
id z^'i' = ang{'i'-1};
#enddo
.sort


* Id Statments for the usage as a C++ code

id bigP = P;
id bigP.bigP^n? = P.P^n;
*id 1/bigP.bigP = 1/P.P;
*id bigP^-1 = P^-1;
*id q = l;

*old:
*id p?.k?   = sum(p*k);
*id p?.k?^-1= sum(p*k)^-1;

id p?.k?   = p.k;
id p?.k?^-1= p.k^-1;


.sort

* Set the optimization level. O[0,1,2,3]
Format O8, stats=on;
.sort


* Assign variables "w" for the optimisation step
ExtraSymbols,array,w;

* Change the output mode to that of Fortran 95
Format C, .d0;
Format float;

********************************************************************************
**WRITING OUT STUFF
********************************************************************************
********************************************************************************

********************************************************************************
**Writing the kernel without optimization to disk                             **
********************************************************************************

* OUTPUT FILENAME
#define FILENAME "<output/meson_qphv_trans.h.txt>"

* Collect terms in "x"
B x;
.sort


* Extract coefficient of "x"
#do i = 1, 16
L kernelL'i' = kernelL[x^'i'];
#enddo
#do i = 1, 16
L kernelLoverlap'i' = kernelL[x^{16+'i'}];
#enddo
.sort

#write 'FILENAME' "#include <typedefs.h>"
#write 'FILENAME' "#include <routing_amount_scalars.h>"
#write 'FILENAME' "#include <extra_def.h>"
#write 'FILENAME' "//#include <math_ops.h>"
#write 'FILENAME' ""
#write 'FILENAME' "void kernelL_notopti_qphv_trans(matCdoub& kern,"
#write 'FILENAME' "                              const ArrayScalarProducts& sp,"
#write 'FILENAME' "                const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){"
#write 'FILENAME' ""
#write 'FILENAME' ""
#write 'FILENAME' "    // local variables"
#write 'FILENAME' "    Cdoub denom=0.0;"
*#write 'FILENAME' "    array< array< Cdoub, K_N_PROJECTORS_DIRAC>  "
*#write 'FILENAME' "               , K_N_BASE_ELEMENTS_DIRAC>  kern;"
#write 'FILENAME' ""
#do i1 = 1, 8
#write 'FILENAME' ""
#write 'FILENAME' "    denom = %e", kernelLoverlap{('i1'-1)*8+'i1'}
#do i2 = 1, 8
#write 'FILENAME' "    kern[%s][%s] =  %e ", {'i1'-1} , {'i2'-1},kernelL{('i1'-1)*8+'i2'}
#write 'FILENAME' "    kern[%s][%s] = (1.0/ denom)* kern[%s][%s]; ", {'i1'-1} , {'i2'-1}, {'i1'-1} ,{'i2'-1}
#enddo
#enddo
#write 'FILENAME' ""
#write 'FILENAME' "}"
#write 'FILENAME' ""


* Collect terms in "x"
B x;
.sort

* Extract coefficient of "x"
#do i = 1, 16
L kernelY'i' = kernelY[x^'i'];
#enddo
#do i = 1, 16
L kernelYoverlap'i' = kernelY[x^{16+'i'}];
#enddo
.sort


#write 'FILENAME' ""
#write 'FILENAME' "void kernelY_noopti_qphv_trans(matCdoub& kern,"
#write 'FILENAME' "  const ArrayScalarProducts& sp,  const Cdoub& svp,"
#write 'FILENAME' "  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){"
#write 'FILENAME' ""
#write 'FILENAME' ""
#write 'FILENAME' "    // local variables"
#write 'FILENAME' "    Cdoub denom;"
*#write 'FILENAME' "    array< array< Cdoub, K_N_PROJECTORS_DIRAC>  "
*#write 'FILENAME' "               , K_N_BASE_ELEMENTS_DIRAC>  kern;"
#write 'FILENAME' ""
#do i1 = 1, 8
#write 'FILENAME' ""
#write 'FILENAME' "    denom = %e", kernelYoverlap{('i1'-1)*8+'i1'}
#do i2 = 1, 8
#write 'FILENAME' "    kern[%s][%s] =  %e ", {'i1'-1} , {'i2'-1},kernelY{('i1'-1)*8+'i2'}
#write 'FILENAME' "    kern[%s][%s] = (1.0/ denom)* kern[%s][%s]; ", {'i1'-1} , {'i2'-1}, {'i1'-1} ,{'i2'-1}

*#write 'FILENAME' "    kern[%s][%s] = (1.0/ denom)* %e ", {'i1'-1} , {'i2'-1},kernelY{('i1'-1)*8+'i2'}
#enddo
#enddo
#write 'FILENAME' ""
*#write 'FILENAME' "  return kern;"
#write 'FILENAME' ""
#write 'FILENAME' "}"
#write 'FILENAME' ""

********************************************************************************
** Optimise the kernel trace and output to disk      kernel all               **
********************************************************************************

#define FILENAMEALL "<output/meson_qphv_trans_kernelall.h.txt>"

* Collect terms in "x"
B x;
.sort
#optimize kernelall
B x;
.sort


* Extract coefficient of "x"
#do i = 1, 16
L kernelall'i' = kernelall[x^'i'];
#enddo
#do i = 1, 16
L kernelalloverlap'i' = kernelall[x^{16+'i'}];
#enddo
.sort

#write 'FILENAMEALL' "#include <typedefs.h>"
#write 'FILENAMEALL' "#include <routing_amount_scalars.h>"
#write 'FILENAMEALL' "#include <extra_def.h>"
*#write 'FILENAMEALL' "//#include <math_ops.h>"
#write 'FILENAMEALL' ""
#write 'FILENAMEALL' "void kernelall_qphv_trans(matCdoub& kern,"
#write 'FILENAMEALL' "  const ArrayScalarProducts& sp,  const Cdoub& svp,"
#write 'FILENAMEALL' "  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ,"
#write 'FILENAMEALL' "                const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){"
#write 'FILENAMEALL' ""
#write 'FILENAMEALL' ""
#write 'FILENAMEALL' "    // local variables"
*#write 'FILENAMEALL' "    VecCdoub w; w.resize({'optimmaxvar_'+1});"
#write 'FILENAMEALL' "    array<Cdoub, {'optimmaxvar_'+1}> w;"
#write 'FILENAMEALL' "    Cdoub denom;"
*#write 'FILENAMEALL' "    array< array< Cdoub, K_N_PROJECTORS_DIRAC>  "
*#write 'FILENAMEALL' "               , K_N_BASE_ELEMENTS_DIRAC>  kern;"
#write 'FILENAMEALL' ""
#write 'FILENAMEALL' "%O"
#do i1 = 1, 8
#write 'FILENAMEALL' ""
#write 'FILENAMEALL' "    denom = %e", kernelalloverlap{('i1'-1)*8+'i1'}
#do i2 = 1, 8
#write 'FILENAMEALL' "    kern[%s][%s] = (1.0/ denom)* %e ", {'i1'-1} , {'i2'-1},kernelall{('i1'-1)*8+'i2'}
#enddo
#enddo
#write 'FILENAMEALL' ""
#write 'FILENAMEALL' "}"
#write 'FILENAMEALL' ""

#clearoptimize

********************************************************************************
** Optimise the kernel trace and output to disk      L                         **
********************************************************************************

* Collect terms in "x"
B x;
.sort
#optimize kernelL
B x;
.sort


* Extract coefficient of "x"
#do i = 1, 16
L kernelL'i' = kernelL[x^'i'];
#enddo
#do i = 1, 16
L kernelLoverlap'i' = kernelL[x^{16+'i'}];
#enddo
.sort

*#write 'FILENAME' "#include <typedefs.h>"
*#write 'FILENAME' "#include <routing_amount_scalars.h>"
*#write 'FILENAME' "#include <extra_def.h>"
*#write 'FILENAME' "//#include <math_ops.h>"
#write 'FILENAME' ""
#write 'FILENAME' "void kernelL_qphv_trans(matCdoub& kern,"
#write 'FILENAME' "              const ArrayScalarProducts& sp,"
#write 'FILENAME' "              const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){"
#write 'FILENAME' ""
#write 'FILENAME' ""
#write 'FILENAME' "    // local variables"
*#write 'FILENAME' "    VecCdoub w; w.resize({'optimmaxvar_'+1});"
#write 'FILENAME' "    array<Cdoub, {'optimmaxvar_'+1}> w;"
#write 'FILENAME' "    Cdoub denom;"
*#write 'FILENAME' "    array< array< Cdoub, K_N_PROJECTORS_DIRAC>  "
*#write 'FILENAME' "               , K_N_BASE_ELEMENTS_DIRAC>  kern;"
#write 'FILENAME' ""
#write 'FILENAME' "%O"
#do i1 = 1, 8
#write 'FILENAME' ""
#write 'FILENAME' "    denom = %e", kernelLoverlap{('i1'-1)*8+'i1'}
#do i2 = 1, 8
#write 'FILENAME' "    kern[%s][%s] = (1.0/ denom)* %e ", {'i1'-1} , {'i2'-1},kernelL{('i1'-1)*8+'i2'}
#enddo
#enddo
#write 'FILENAME' ""
#write 'FILENAME' "}"
#write 'FILENAME' ""


********************************************************
******write into a second file**************************
********************************************************
#define FILENOW "<output/meson_qphv_trans_L.h.txt>"


#write 'FILENOW' "#include <typedefs.h>"
#write 'FILENOW' "#include <routing_amount_scalars.h>"
#write 'FILENOW' "#include <extra_def.h>"
#write 'FILENOW' "//#include <math_ops.h>"
#write 'FILENOW' ""
#write 'FILENOW' "void kernelL_qphv_trans(matCdoub& kern,"
#write 'FILENOW' "              const ArrayScalarProducts& sp,"
#write 'FILENOW' "              const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){"
#write 'FILENOW' ""
#write 'FILENOW' ""
#write 'FILENOW' "    // local variables"
#write 'FILENOW' "    array<Cdoub, {'optimmaxvar_'+1}> w;"
#write 'FILENOW' "    Cdoub denom;"
#write 'FILENOW' ""
#write 'FILENOW' "%O"
#do i1 = 1, 8
#write 'FILENOW' ""
#write 'FILENOW' "    denom = %e", kernelLoverlap{('i1'-1)*8+'i1'}
#do i2 = 1, 8
#write 'FILENOW' "    kern[%s][%s] = (1.0/ denom)* %e ", {'i1'-1} , {'i2'-1},kernelL{('i1'-1)*8+'i2'}
#enddo
#enddo
#write 'FILENOW' ""
#write 'FILENOW' "}"
#write 'FILENOW' ""

#clearoptimize

********************************************************************************
** Optimise the kernel trace and output to disk       Y                        **
********************************************************************************

* Collect terms in "x"
B x;
.sort
#optimize kernelY
B x;
.sort

* Extract coefficient of "x"
#do i = 1, 16
L kernelY'i' = kernelY[x^'i'];
#enddo
#do i = 1, 16
L kernelYoverlap'i' = kernelY[x^{16+'i'}];
#enddo
.sort


#write 'FILENAME' ""
#write 'FILENAME' "void kernelY_qphv_trans(matCdoub& kern,"
#write 'FILENAME' "  const ArrayScalarProducts& sp,  const Cdoub& svp,"
#write 'FILENAME' "  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){"
#write 'FILENAME' ""
#write 'FILENAME' ""
#write 'FILENAME' "    // local variables"
*#write 'FILENAME' "    VecCdoub w; w.resize({'optimmaxvar_'+1});"
#write 'FILENAME' "    array<Cdoub, {'optimmaxvar_'+1}> w;"
#write 'FILENAME' "    Cdoub denom;"
*#write 'FILENAME' "    array< array< Cdoub, K_N_PROJECTORS_DIRAC>  "
*#write 'FILENAME' "               , K_N_BASE_ELEMENTS_DIRAC>  kern;"
#write 'FILENAME' ""
#write 'FILENAME' "%O"
#do i1 = 1, 8
#write 'FILENAME' ""
#write 'FILENAME' "    denom = %e", kernelYoverlap{('i1'-1)*8+'i1'}
#do i2 = 1, 8
#write 'FILENAME' "    kern[%s][%s] = (1.0/ denom)* %e ", {'i1'-1} , {'i2'-1},kernelY{('i1'-1)*8+'i2'}
#enddo
#enddo
#write 'FILENAME' ""
*#write 'FILENAME' "  return kern;"
#write 'FILENAME' ""
#write 'FILENAME' "}"
#write 'FILENAME' ""


********************************************************
******write into a second file**************************
********************************************************
#define FILENOW "<output/meson_qphv_trans_Y.h.txt>"


#write 'FILENOW' "#include <typedefs.h>"
#write 'FILENOW' "#include <routing_amount_scalars.h>"
#write 'FILENOW' "#include <extra_def.h>"
#write 'FILENOW' "//#include <math_ops.h>"
#write 'FILENOW' ""
#write 'FILENOW' "void kernelY_qphv_trans(matCdoub& kern,"
#write 'FILENOW' "       const ArrayScalarProducts& sp,"
#write 'FILENOW' "       const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){"
#write 'FILENOW' ""
#write 'FILENOW' ""
#write 'FILENOW' "    // local variables"
#write 'FILENOW' "    array<Cdoub, {'optimmaxvar_'+1}> w;"
#write 'FILENOW' "    Cdoub denom;"
#write 'FILENAME' ""
#write 'FILENOW' "%O"
#do i1 = 1, 8
#write 'FILENOW' ""
#write 'FILENOW' "    denom = %e", kernelYoverlap{('i1'-1)*8+'i1'}
#do i2 = 1, 8
#write 'FILENOW' "    kern[%s][%s] = (1.0/ denom)* %e ", {'i1'-1} , {'i2'-1},kernelY{('i1'-1)*8+'i2'}
#enddo
#enddo
#write 'FILENOW' ""
#write 'FILENOW' "}"
#write 'FILENOW' ""

#clearoptimize

********************************************************************************
** Optimise the normalisation and output to disk                              **
********************************************************************************

* Collect terms in "x"
B x;
.sort

* Extract coefficient of "x"
#do i = 1, 16
L normalisation'i' = normalisation[x^'i'];
#enddo
.sort

#define FILENAME2 "<output/meson_qphv_trans_Renorm.h.txt>"
#define FILENAME3 "<output/meson_qphv_trans_Renorm_fv.h.txt>"


#write 'FILENAME3' "#include <typedefs.h>"
#write 'FILENAME3' "#include <routing_amount_scalars.h>"
#write 'FILENAME3' "#include <extra_def.h>"
#write 'FILENAME3' " "
#write 'FILENAME3' "void normalisation_notopti_qphv_trans(matCdoub& kern,"
#write 'FILENAME3' "  Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,"
#write 'FILENAME3' "  Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,"
#write 'FILENAME3' "  const Cdoub& svp, const Cdoub& ssp, "
#write 'FILENAME3' "  const Cdoub& svm, const Cdoub& ssm ){"
#write 'FILENAME3' " "
#write 'FILENAME3' " "
#write 'FILENAME3' " "
#do i1 = 1, 8
#write 'FILENAME3' " "
#do i2 = 1, 8
#write 'FILENAME3' "    kern[%s][%s] = %e", {'i1'-1} , {'i2'-1}, normalisation{('i1'-1)*8+'i2'}
#enddo
#enddo
#write 'FILENAME3' ""
#write 'FILENAME3' "}"
#write 'FILENAME3' ""


*****************Writing out the optimized form*********************
* Collect terms in "x"
B x;
.sort
#optimize normalisation
B x;
.sort

* Extract coefficient of "x"
#do i = 1, 16
L normalisation'i' = normalisation[x^'i'];
#enddo
.sort

#write 'FILENAME2' "#include <typedefs.h>"
#write 'FILENAME2' "#include <routing_amount_scalars.h>"
#write 'FILENAME2' "#include <extra_def.h>"
#write 'FILENAME2' " "
#write 'FILENAME2' "void normalisation_qphv_trans(matCdoub& kern,"
#write 'FILENAME2' "  const ArrayScalarProducts& sp,  const Cdoub& svp,"
#write 'FILENAME2' "  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){"
#write 'FILENAME2' " "
#write 'FILENAME2' " "
#write 'FILENAME2' "     // local variables"
#write 'FILENAME2' "    array<Cdoub, {'optimmaxvar_'+1}> w;"
#write 'FILENAME2' " "
#write 'FILENAME2' " %O"
#do i1 = 1, 8
#write 'FILENAME2' " "
#do i2 = 1, 8
#write 'FILENAME2' "    kern[%s][%s] = %e", {'i1'-1} , {'i2'-1}, normalisation{('i1'-1)*8+'i2'}
#enddo
#enddo
#write 'FILENAME2' ""
#write 'FILENAME2' "}"
#write 'FILENAME2' ""


*****************Writing out the optimized form in extra file*****************


#write 'FILENAME3' "void normalisation_qphv_trans(matCdoub& kern,"
#write 'FILENAME3' "  Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,"
#write 'FILENAME3' "  Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,"
#write 'FILENAME3' "  const Cdoub& svp, const Cdoub& ssp, "
#write 'FILENAME3' "  const Cdoub& svm, const Cdoub& ssm ){"
#write 'FILENAME3' " "
#write 'FILENAME3' " "
#write 'FILENAME3' "     // local variables"
#write 'FILENAME3' "    array<Cdoub, {'optimmaxvar_'+1}> w;"
#write 'FILENAME3' " "
#write 'FILENAME3' " %O"
#do i1 = 1, 8
#write 'FILENAME3' " "
#do i2 = 1, 8
#write 'FILENAME3' "    kern[%s][%s] = %e", {'i1'-1} , {'i2'-1}, normalisation{('i1'-1)*8+'i2'}
#enddo
#enddo
#write 'FILENAME3' ""
#write 'FILENAME3' "}"
#write 'FILENAME3' ""

#clearoptimize

********************************************************************************
** Optimise the decayrate and output to disk                              **
********************************************************************************

* Collect terms in "x"
B x;
.sort

* Extract coefficient of "x"
#do i = 1, 8
L fpi'i' = fpi[x^'i'];
#enddo
.sort


#define FILENAME2 "<output/meson_qphv_trans_fpi.h.txt>"


#write 'FILENAME2' "#include <typedefs.h>"
#write 'FILENAME2' "#include <routing_amount_scalars.h>"
#write 'FILENAME2' "#include <extra_def.h>"
#write 'FILENAME2' " "
#write 'FILENAME2' "Cdoub calc_fpi_qphv_trans( "
#write 'FILENAME2' "  Cdoub &qm_qp,Cdoub &P_qm, Cdoub &P_qp, Cdoub &P_l,"
#write 'FILENAME2' "  Cdoub &l_qm, Cdoub &l_qp, Cdoub &P_P, Cdoub &l_l,"
#write 'FILENAME2' "  VecCdoub Gamma, const Cdoub& svp,"
#write 'FILENAME2' "  const Cdoub& ssp, const Cdoub& svm, const Cdoub& ssm ){"
#write 'FILENAME2' " "
#write 'FILENAME2' " "
#write 'FILENAME2' " Cdoub dummy, fpi=0.0;"
#write 'FILENAME2' " "
#do i1 = 1, 8
#write 'FILENAME2' "    dummy =  %e " , fpi{('i1')}
#write 'FILENAME2' "    fpi += Gamma[%s] * dummy; ", {'i1'-1}
#write 'FILENAME2' " "
#enddo


#write 'FILENAME2' ""
#write 'FILENAME2' "    return fpi;"
#write 'FILENAME2' ""
#write 'FILENAME2' "}"
#write 'FILENAME2' ""



********************************************************************************
** Writing out the inhomogenous part to disk                                 **
********************************************************************************

#define FILENAME8 "<output/meson_qphv_trans_inhomogenous_unpoti.h.txt>"

* Collect terms in "x"
B x;
.sort

* Extract coefficient of "x"
#do i = 1, 8
L kernelI'i' = kernelI[x^'i'];
#enddo
#do i = 1, 8
L kernelIoverlap'i' = kernelI[x^{8+'i'}];
#enddo
.sort

#write 'FILENAME8' "#include <typedefs.h>"
#write 'FILENAME8' "#include <routing_amount_scalars.h>"
#write 'FILENAME8' "#include <extra_def.h>"
#write 'FILENAME8' " "
#write 'FILENAME8' ""
#write 'FILENAME8' "void kernelInhomo_qphv_trans(VecCdoub& kern,"
#write 'FILENAME8' "  const ArrayScalarProducts& sp ){"
#write 'FILENAME8' ""
#write 'FILENAME8' ""
#write 'FILENAME8' "    // local variables"
#write 'FILENAME8' "    Cdoub denom;"
#write 'FILENAME8' "    array<Cdoub, {'optimmaxvar_'+1}> w;"
#write 'FILENAME8' ""
#do i1 = 1, 8
*#write 'FILENAME8' ""
*#write 'FILENAME8' "    denom = %e", kernelI[x^{8 +'i1'}]
*#write 'FILENAME8' "    kern[%s] = (1.0/ denom)* %e ", {'i1'-1} ,kernelI[x^'i1']
#write 'FILENAME8' "    denom = %e", kernelIoverlap{('i1')}
#write 'FILENAME8' "    kern[%s] = (1.0/ denom)* %e ", {'i1'-1} ,kernelI{('i1')}
#enddo
#write 'FILENAME8' ""
#write 'FILENAME8' ""
#write 'FILENAME8' "}"
#write 'FILENAME8' ""


** Writing out the inhomogenous part to disk   optimized                      **
********************************************************************************

#define FILENAME8 "<output/meson_qphv_trans_inhomogenous_old.h.txt>"

* Collect terms in "x"
B x;
.sort
#optimize kernelI
B x;
.sort

* Extract coefficient of "x"
#do i = 1, 8
L kernelI'i' = kernelI[x^'i'];
#enddo
#do i = 1, 8
L kernelIoverlap'i' = kernelI[x^{8+'i'}];
#enddo
.sort

#write 'FILENAME8' "#include <typedefs.h>"
#write 'FILENAME8' "#include <routing_amount_scalars.h>"
#write 'FILENAME8' "#include <extra_def.h>"
#write 'FILENAME8' " "
#write 'FILENAME8' ""
#write 'FILENAME8' "void kernelInhomo_qphv_trans(VecCdoub& kern,"
#write 'FILENAME8' "  const ArrayScalarProducts& sp ){"
#write 'FILENAME8' ""
#write 'FILENAME8' ""
#write 'FILENAME8' "    // local variables"
#write 'FILENAME8' "    Cdoub denom;"
#write 'FILENAME8' "    array<Cdoub, {'optimmaxvar_'+1}> w;"
#write 'FILENAME8' ""
#write 'FILENAME8'" %O"
#do i1 = 1, 8
*#write 'FILENAME8' ""
*#write 'FILENAME8' "    denom = %e", kernelI[x^{8 +'i1'}]
*#write 'FILENAME8' "    kern[%s] = (1.0/ denom)* %e ", {'i1'-1} ,kernelI[x^'i1']
#write 'FILENAME8' "    denom = %e", kernelIoverlap{('i1')}
#write 'FILENAME8' "    kern[%s] = (1.0/ denom)* %e ", {'i1'-1} ,kernelI{('i1')}
#enddo
#write 'FILENAME8' ""
#write 'FILENAME8' ""
#write 'FILENAME8' "}"
#write 'FILENAME8' ""



.end
