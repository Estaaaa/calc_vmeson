
********************************************************************************
** Seperated write out for longitudinal part and transvers part     Y & L     **
********************************************************************************

*Choose only the scalar or vector parts
#do i1 = 8, 12
#do i2 = 8, 12
L kernelLonly = kernelL[x^{   ('i1')*12+'i2'}];
L kernelLoverlaponly = kernelL[x^{144+ ('i1')*12+'i2'}];

#enddo
#enddo
.sort


* Collect terms in "x"
B x;
.sort
#optimize kernelLonly
B x;
.sort


* Extract coefficient of "x"
#do i = 1, 16
L kernelL'i' = kernelLonly[x^'i'];
#enddo
#do i = 1, 16
L kernelLoverlap'i' = kernelLoverlaponly[x^{144+'i'}];
#enddo
.sort

#write 'FILENAME' ""
#write 'FILENAME' "void kernelL_qphv_longi(matCdoub& kern,"
#write 'FILENAME' "              const ArrayScalarProducts& sp,"
#write 'FILENAME' "              const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){"
#write 'FILENAME' ""
#write 'FILENAME' ""
#write 'FILENAME' "    // local variables"
*#write 'FILENAME' "    VecCdoub w; w.resize({'optimmaxvar_'+1});"
#write 'FILENAME' "    array<Cdoub, {'optimmaxvar_'+1}> w;"
#write 'FILENAME' "    Cdoub denom;"
*#write 'FILENAME' "    array< array< Cdoub, K_N_PROJECTORS_DIRAC>  "
*#write 'FILENAME' "               , K_N_BASE_ELEMENTS_DIRAC>  kern;"
#write 'FILENAME' ""
#write 'FILENAME' "%O"
#do i1 = 1, 12
#write 'FILENAME' ""
#write 'FILENAME' "    denom = %e", kernelLoverlap{('i1'-1)*12+'i1'}
#do i2 = 1, 12
#write 'FILENAME' "    kern[%s][%s] = (1.0/ denom)* %e ", {'i1'-1} , {'i2'-1},kernelL{('i1'-1)*12+'i2'}
#enddo
#enddo
#write 'FILENAME' ""
#write 'FILENAME' "}"
#write 'FILENAME' ""

#clearoptimize
