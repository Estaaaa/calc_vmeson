* DEFAULTS
#-
#: continuationlines 50
#: indentspace 4


* DECLARATIONS.
CF gluon;
F quark, T1, T2, G1, ..., G4, Lp, Lm;
CF PR;
CF sA, sB;
CF DZ, Z;
S x, n;

* Indices (Lorentz)
I al, al1, ..., al4, be, be1, ..., be4, mu, mu1, ..., mu4, nu, nu1, ..., nu4, ro, ro1, ..., ro4, si, si1, ..., si4, la, la1, ..., la4;

* Indices (Summation. We could define these as symbols instead)
I i, i1, ..., i4, j, j1, ..., j4, m, m1, ..., m4;

* (four)-qphvs. The temporal vector u is for finite temperature. Cannot use w here
V bigP, P,  bigQ, p, p1, ..., p4, k, k1, ..., k4, q, q1, ..., q4, l, l1, ..., l4, t, t1, ..., t4, u, u1, ..., u4, v, v1, ..., v4, w1, ..., w4;
V qm, qp, q, p, bigPhat, bigQhat;

cfunction DZ, DG;

function Projector, Amplitude, ConjugateAmplitude;
cfunction sum;
vector P;
symbol svp, ssp, svm, ssm, x, z, ang0,...,ang5;
symbol Z2;

********************************************************************************
** Define the whole BS kernel                                                 **
********************************************************************************
* Trace of the BS wavefunction subject to the rainbow-ladder truncation.
local kernelall = 0;
#do i1 = 1, 12
#do i2 = 1, 12
L kernelall = kernelall + z*z *x^{   ('i1'-1)*12+'i2'} * Projector('i1',mu1 ,p ,bigP)
                                                  * g_(1,mu)
                                                  * quark(qp)
                                                  * Amplitude('i2',mu1, l, bigP)
                                                  * g_(1,nu)
                                                  *  quark(qm)
                                                  * gluon(k,mu,nu);
.sort
#enddo
#enddo

* Denominators of the projection and orthogonality check
#do i1  = 1, 12
#do i2  = 1, 12
L kernelall = kernelall+       x^{144+('i1'-1)*12+'i2'} * Projector('i1',mu, p, bigP)
                                                  * Amplitude('i2', mu, p, bigP);
.sort
#enddo
#enddo

********************************************************************************
** Define the trace of the BS kernel - divided in L and Y            L       **
********************************************************************************
* Trace of the BS wavefunction subject to the rainbow-ladder truncation.
local kernelL = 0;
#do i1 = 1, 12
#do i2 = 1, 12
L kernelL = kernelL + z*z *x^{   ('i1'-1)*12+'i2'} * Projector('i1',mu1 ,p ,bigP)
                                                  * g_(1,mu)
                                                  * Amplitude('i2',mu1, l, bigP)
                                                  * g_(1,nu)
                                                  * gluon(k,mu,nu);
.sort
#enddo
#enddo

* Denominators of the projection and orthogonality check
#do i1  = 1, 12
#do i2  = 1, 12
L kernelL = kernelL+       x^{144+('i1'-1)*12+'i2'} * Projector('i1',mu, p, bigP)
                                                  * Amplitude('i2', mu, p, bigP);
.sort
#enddo
#enddo


********************************************************************************
** Define the quark rotation matrix                Y                          **
********************************************************************************

* Projection for attaching quarks to the amplitude, thus forming the wavefunction.
local kernelY = 0;
#do i1 = 1, 12
#do i2 = 1, 12
L kernelY = kernelY +       x^{   ('i1'-1)*12+'i2'} * Projector('i1',mu, l, bigP)
                                                   * quark(qp) * Amplitude('i2',mu, l, bigP)
                                                   * quark(qm);
.sort
#enddo
#enddo

* Denominators of the projection and orthogonality check
#do i1  = 1, 12
#do i2  = 1, 12
L kernelY = kernelY +       x^{144+('i1'-1)*12+'i2'} * Projector('i1',mu, l, bigP)
                                                   * Amplitude('i2',mu, l, bigP);
.sort
#enddo
#enddo



********************************************************************************
** Define the normalisation integral                                          **
********************************************************************************
L normalisation = 0;
#do i1 = 1, 12
#do i2 = 1, 12
L normalisation = normalisation + x^{   ('i1'-1)*12+'i2'} * ConjugateAmplitude('i1', mu, l, -bigP)
                                                         * quark(qp)
                                                         * Amplitude('i2', mu, l, bigP)
                                                         * quark(qm);
.sort
#enddo
#enddo

********************************************************************************
** Define the decay rate integral                                          **
********************************************************************************
L fpi = 0;
#do i1 = 1, 12
L fpi = fpi + x^{('i1')}    * (-i_)            * Amplitude('i1',mu, l, -bigP)
                                               * quark(qp)
                                               * g_(1,mu)
                                               * quark(qm);
.sort
#enddo

********************************************************************************
** Define the Projection on the inhomogenous term I                         **
********************************************************************************

* Projection for attaching quarks to the amplitude, thus forming the wavefunction.
local kernelI = 0;
#do i1 = 1, 12
L kernelI = kernelI +       x^{('i1')}               * Projector('i1',mu, p, bigP)
                                                     * i_* g_(1,mu);
.sort
#enddo

* Denominators of the projection and orthogonality check
#do i1  = 1, 12
L kernelI = kernelI +       x^{12+('i1')}            * Projector('i1',mu, p, bigP)
                                                   * Amplitude('i1',mu, p, bigP);
.sort
#enddo


********************************************************************************
** Perform the substitutions                                                  **
********************************************************************************

** SUBSTITUTIONS.
* This included the Basis and the Projectors: Can be changed to pscalar, qphv, qphv
#include include/BP_qphv.h


.sort
id quark(p?)         = -i_*g_(1,p)*sA(p) + gi_(1)*sB(p);
id gluon(p?,mu?,nu?) = DZ(p)*PR(mu,nu,p);

id PR(mu?,nu?,p?) = d_(mu,nu) - p(mu)*p(nu)/p.p;

.sort
id sA(qp)  = svp;
id sB(qp)  = ssp;
id sA(qm)  = svm;
id sB(qm)  = ssm;

id DZ(k)  = 1; * We are collecting angular integrals
id k.k^n? = z^n;
id k = l - p;
.sort

* Perform any Dirac traces. Must do this for multiple spin lines if present.
Trace4,1;
Contract;
********************************************************************************
** Factorise the angular integrals                                            **
********************************************************************************

.sort

id p.l = (l.l + p.p - z)/2;
.sort
id p.l = (l.l + p.p - z)/2;
.sort
id p.l = (l.l + p.p - z)/2;
.sort
id p.l = (l.l + p.p - z)/2;
.sort
id p.l = (l.l + p.p - z)/2;
.sort
#do i = 6,1,-1
id z^'i' = ang{'i'-1};
#enddo
.sort


* Id Statments for the usage as a C++ code

id bigP = P;
id bigP.bigP^n? = P.P^n;
*id 1/bigP.bigP = 1/P.P;
*id bigP^-1 = P^-1;
*id q = l;

*old:
*id p?.k?   = sum(p*k);
*id p?.k?^-1= sum(p*k)^-1;

id p?.k?   = p.k;
id p?.k?^-1= p.k^-1;


.sort

* Set the optimization level. O[0,1,2,3]
Format O4, stats=on;
.sort


* Assign variables "w" for the optimisation step
ExtraSymbols,array,w;



********************************************************************************
**Output for Mathematica                       **
********************************************************************************

* Change the output mode to that of Fortran 95
Format Mathematica;

* OUTPUT FILENAME
#define FILENAMEMATHE "<output/mathematica_output.txt>"

* Collect terms in "x"
B x;
.sort
*#optimize kernelall
*B x;
*.sort


* Extract coefficient of "x"
#do i = 1, 144
L kernelall'i' = kernelall[x^'i'];
#enddo
#do i = 1, 144
L kernelalloverlap'i' = kernelall[x^{144+'i'}];
#enddo
.sort

*#write 'FILENAMEMATHE' ""
*#write 'FILENAMEMATHE' "%O"
#do i1 = 1, 12
#write 'FILENAMEMATHE' ""
#write 'FILENAMEMATHE' "    denom%s = %e", 'i1',  kernelalloverlap{('i1'-1)*12+'i1'}
#do i2 = 1, 12
#write 'FILENAMEMATHE' "    kern[%s][%s] = (1.0/denom%s)* %e ", {'i1'} , {'i2'}, 'i1', kernelall{('i1'-1)*12+'i2'}
#enddo
#enddo


*#clearoptimize


.end
