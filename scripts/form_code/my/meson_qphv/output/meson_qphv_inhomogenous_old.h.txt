#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
 

void kernelInhomo_qphv(VecCdoub& kern,
  const ArrayScalarProducts& sp ){


    // local variables
    Cdoub denom;
    array<Cdoub, 14> w;

 
    w[1]=P_P;
    w[2]=p_p;
    w[3]=P_p;
   w[4]=pow(w[3],2);
   w[5]=w[1]*w[2];
   w[6]=w[4] - w[5];
   w[7]=4.E+0*w[6];
   w[8]=4.E+0*w[3];
   w[9]=w[6]*w[8];
   w[10]=1.6E+1*w[6];
   w[11]= - w[4] + 2.E+0*w[5];
   w[4]=w[11]*w[4];
   w[5]=pow(w[5],2);
   w[4]=w[4] - w[5];
   w[5]= - 8.E+0*w[4];
   w[11]=1.6E+1*w[4];
   w[12]= - w[3]*w[11];
   w[6]=4.8E+1*w[6];
   w[13]=8.E+0*w[3];
   w[4]=w[4]*w[13];

    denom = w[7];

    kern[0] = (1.0/ denom)* w[7];
 
    denom = w[9];

    kern[1] = (1.0/ denom)*  0;
 
    denom = w[8];

    kern[2] = (1.0/ denom)*  0;
 
    denom = w[10];

    kern[3] = (1.0/ denom)*  0;
 
    denom = w[5];

    kern[4] = (1.0/ denom)*  0;
 
    denom = w[12];

    kern[5] = (1.0/ denom)*  0;
 
    denom = w[10];

    kern[6] = (1.0/ denom)*  0;
 
    denom = w[6];

    kern[7] = (1.0/ denom)*  0;
 
    denom = w[9];

    kern[8] = (1.0/ denom)*  0;
 
    denom =  - w[4];

    kern[9] = (1.0/ denom)*  0;
 
    denom =  - w[4];

    kern[10] = (1.0/ denom)*  0;
 
    denom =  - w[11];

    kern[11] = (1.0/ denom)*  0;
 


}

