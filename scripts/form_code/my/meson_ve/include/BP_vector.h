******* Substitution for the Base **********

id ConjugateAmplitude(1, mu?, q?, P?) = gi_(1)*(i_*(g_(1,mu)-(g_(1,P)*P(mu))/P.P));
id ConjugateAmplitude(2, mu?, q?, P?) = gi_(1)*(g_(1,mu)*g_(1,P)-P(mu));
id ConjugateAmplitude(3, mu?, q?, P?) = gi_(1)*((q.P*((-q.P+g_(1,P)*g_(1,q))*P(mu)+P.P*(-g_(1,mu)*g_(1,q)+q(mu))))/P.P);
id ConjugateAmplitude(4, mu?, q?, P?) = gi_(1)*(i_*(g_(1,mu)*g_(1,P)*g_(1,q)-g_(1,mu)*g_(1,q)*g_(1,P)-2*g_(1,q)*P(mu)+2*g_(1,P)*q(mu)));
id ConjugateAmplitude(5, mu?, q?, P?) = gi_(1)*(-((q.P*P(mu))/P.P)+q(mu));
id ConjugateAmplitude(6, mu?, q?, P?) = gi_(1)*((i_*q.P*g_(1,P)*(-(q.P*P(mu))+P.P*q(mu)))/P.P);
id ConjugateAmplitude(7, mu?, q?, P?) = gi_(1)*((-i_)*g_(1,q)*(-((q.P*P(mu))/P.P)+q(mu)));
id ConjugateAmplitude(8, mu?, q?, P?) = gi_(1)*(((g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P))*(-(q.P*P(mu))+P.P*q(mu)))/P.P);


id Amplitude(1, mu?, q?, P?) = gi_(1)*(i_*(g_(1,mu)-(g_(1,P)*P(mu))/P.P));
id Amplitude(2, mu?, q?, P?) = gi_(1)*(g_(1,mu)*g_(1,P)-P(mu));
id Amplitude(3, mu?, q?, P?) = gi_(1)*((q.P*((-q.P+g_(1,P)*g_(1,q))*P(mu)+P.P*(-g_(1,mu)*g_(1,q)+q(mu))))/P.P);
id Amplitude(4, mu?, q?, P?) = gi_(1)*(i_*(g_(1,mu)*g_(1,P)*g_(1,q)-g_(1,mu)*g_(1,q)*g_(1,P)-2*g_(1,q)*P(mu)+2*g_(1,P)*q(mu)));
id Amplitude(5, mu?, q?, P?) = gi_(1)*(-((q.P*P(mu))/P.P)+q(mu));
id Amplitude(6, mu?, q?, P?) = gi_(1)*((i_*q.P*g_(1,P)*(-(q.P*P(mu))+P.P*q(mu)))/P.P);
id Amplitude(7, mu?, q?, P?) = gi_(1)*((-i_)*g_(1,q)*(-((q.P*P(mu))/P.P)+q(mu)));
id Amplitude(8, mu?, q?, P?) = gi_(1)*(((g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P))*(-(q.P*P(mu))+P.P*q(mu)))/P.P);


id Projector(1, mu?, q?, P?) = gi_(1)*((-i_)*(((q.P)^2-P.P*q.q)*g_(1,mu)+g_(1,q)*(-(q.P*P(mu))+P.P*q(mu))+g_(1,P)*(q.q*P(mu)-q.P*q(mu))));
id Projector(2, mu?, q?, P?) = gi_(1)*(2*P.P*q.q*(-g_(1,mu)*g_(1,P)+P(mu))+2*q.P*(q.P*P(mu)+P.P*(g_(1,mu)*g_(1,q)-q(mu)))+g_(1,q)*g_(1,P)*(-(q.P*P(mu))+P.P*q(mu))-g_(1,P)*g_(1,q)*(q.P*P(mu)+P.P*q(mu)));
id Projector(3, mu?, q?, P?) = gi_(1)*(q.P*(g_(1,mu)*g_(1,P)-2*P(mu))+g_(1,P)*g_(1,q)*P(mu)+P.P*(-g_(1,mu)*g_(1,q)+q(mu)));
id Projector(4, mu?, q?, P?) = gi_(1)*((-i_)*(g_(1,mu)*g_(1,P)*g_(1,q)-g_(1,mu)*g_(1,q)*g_(1,P)-2*g_(1,q)*P(mu)+2*g_(1,P)*q(mu)));
id Projector(5, mu?, q?, P?) = gi_(1)*(-(q.P*P(mu))+P.P*q(mu));
id Projector(6, mu?, q?, P?) = gi_(1)*(i_*(-(q.q*(q.P*(P.P*g_(1,mu)-3*g_(1,P)*P(mu))+2*P.P*g_(1,P)*q(mu)))+q.P*((q.P)^2*g_(1,mu)+3*P.P*g_(1,q)*q(mu)-q.P*(3*g_(1,q)*P(mu)+g_(1,P)*q(mu)))));
id Projector(7, mu?, q?, P?) = gi_(1)*(i_*(P.P*((q.P)^2-P.P*q.q)*g_(1,mu)+3*P.P*g_(1,q)*(-(q.P*P(mu))+P.P*q(mu))+g_(1,P)*((2*(q.P)^2+P.P*q.q)*P(mu)-3*P.P*q.P*q(mu))));
id Projector(8, mu?, q?, P?) = gi_(1)*(2*((q.P)^2-P.P*q.q)*(g_(1,mu)*g_(1,P)-P(mu))+3*(-g_(1,P)*g_(1,q)+g_(1,q)*g_(1,P))*(-(q.P*P(mu))+P.P*q(mu)));
