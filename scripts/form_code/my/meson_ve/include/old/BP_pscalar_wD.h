******* Substitution for the Base **********
 
id Basis(1, P?, q?) = g5_(1); 
id Basis(2, P?, q?) = i_*g_(1,P)*g5_(1); 
id Basis(3, P?, q?) = i_*q.P*g_(1,q)*g5_(1); 
id Basis(4, P?, q?) = -g_(1,P)*g_(1,q)*g5_(1)+g_(1,q)*g_(1,P)*g5_(1); 

 
id Projector(1, P?, q?) = g5_(1)/4; 
id Projector(2, P?, q?) = ((i_/4)*(-(q.q*g_(1,P)*g5_(1))+q.P*g_(1,q)*g5_(1)))/((q.P)^2-P.P*q.q); 
id Projector(3, P?, q?) = ((i_/4)*(q.P*g_(1,P)*g5_(1)-P.P*g_(1,q)*g5_(1)))/((q.P)^3-P.P*q.P*q.q); 
id Projector(4, P?, q?) = (-g_(1,P)*g_(1,q)*g5_(1)+g_(1,q)*g_(1,P)*g5_(1))/(16*((q.P)^2-P.P*q.q)); 
