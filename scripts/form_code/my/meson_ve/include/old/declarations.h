********************** Define the symbols to be used ***************************

* Commuting propagators and vertices (with respect to Gamma Matrices)
CF gluon,  ghost,  vertex3g,  vertexAcc,  vertex4g;
CF gluon0, ghost0, vertex3g0, vertexAcc0, vertex4g0;
CF gluonT, ghostT, vertex3gT, vertexAccT, vertex4gT;

* projectors for the gluon at zero and finite temperature
CF PR, PT, PL, PZ;
S zeta, zeta1, ..., zeta4;

* Scalar and vector dressing functions of the quark (functions of momentum)
CF sA, sB, sC, sD; * C and D are for finite temperature

* Scalar dressing function of the gluon (function of momentum)
CF Z, ZT, ZL;      * ZT and ZL are for finite temperature

* Scalar dressing function of the ghost (function of momentum)
CF G;

* Scalar dressing function of the qg vertex (function of momentum)
CF Vqg, sin;

* Scalar dressing function of the 3g vertex (function of momentum)
CF V3g;

* Scalar dressing function of the gh-gl vertex (function of momentum)
CF Vgh;

* Non-commuting propagators and vertices (with respect to Gamma Matrices)
F quark,  vertexqg,  vertqg1, ...,  vertqg8,  projqg1, ...,  projqg8;
F quark0,  vertexqg0;
F quarkT, vertexqgT, vertqgT1, ..., vertqgT32, projqgT1, ..., projqgT32;

* Symbols. We use "x^n" to select polynomial coefficients when combining
* multiple terms that should be optimized simultaneously.
S x, x1, ..., x4, y, y1, ..., y4, n;

* Renormalisation constants
S Z1, Z1F, Z1t;

* Indices (Lorentz)
I al, al1, ..., al4, be, be1, ..., be4, mu, mu1, ..., mu4, nu, nu1, ..., nu4, ro, ro1, ..., ro4, si, si1, ..., si4, la, la1, ..., la4;

* Indices (Summation. We could define these as symbols instead)
I i, i1, ..., i4, j, j1, ..., j4, m, m1, ..., m4;

* (four)-vectors. The temporal vector u is for finite temperature.
V p, p1, ..., p4, k, k1, ..., k4, q, q1, ..., q4, l, l1, ..., l4, t, t1, ..., t4, u, u1, ..., u4, v, v1, ..., v4, w1, ..., w4;
V Q, P;
