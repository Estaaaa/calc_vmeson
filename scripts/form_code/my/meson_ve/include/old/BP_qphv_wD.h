******* Declarations for the Base **********
 *Non-commuting functions
F Basis, Projector 
 
******* Substitution for the Base **********
 
id Basis(0, Q?, k?) = Q.Q*g_(0,mu)-g_(0,Q)*Q(mu); 
id Basis(1, Q?, k?) = (g_(0,k)*g_(0,Q)-g_(0,Q)*g_(0,k))*Q(mu)); 
id Basis(2, Q?, k?) = (i_/2)*(-g_(0,Q)*g_(0,mu)+g_(0,mu)*g_(0,Q)); 
id Basis(3, Q?, k?) = g_(0,mu)*g_(0,k)*g_(0,Q)-g_(0,mu)*g_(0,Q)*g_(0,k))/6; 
id Basis(4, Q?, k?) = i_*(Q.Q*k(mu)-Q.k*Q(mu)); 
id Basis(5, Q?, k?) = g_(0,k)*(Q.Q*k(mu)-Q.k*Q(mu)); 
id Basis(6, Q?, k?) = Q.k*(Q.k*g_(0,mu)-g_(0,Q)*k(mu)); 
id Basis(7, Q?, k?) = Q.k*(-g_(0,k)*g_(0,mu)+g_(0,mu)*g_(0,k))); 

 
id Projector(0, Q?, k?) = Q(mu)))/(8*Q.Q*((Q.k)^2-k.k*Q.Q)^2); 
id Projector(1, Q?, k?) = k.k*(g_(0,k)*g_(0,Q)-g_(0,Q)*g_(0,k))*Q(mu))))/(Q.k*Q.Q*((Q.k)^2-k.k*Q.Q)^2); 
id Projector(2, Q?, k?) = k.k*(g_(0,Q)*g_(0,mu)-g_(0,mu)*g_(0,Q))))/((Q.k)^2-k.k*Q.Q); 
id Projector(3, Q?, k?) = g_(0,mu)*g_(0,k)*g_(0,Q)-g_(0,mu)*g_(0,Q)*g_(0,k))/(48*(Q.k)^2-48*k.k*Q.Q); 
id Projector(4, Q?, k?) = ((i_/4)*(-(Q.Q*k(mu))+Q.k*Q(mu)))/(Q.Q*(-(Q.k)^2+k.k*Q.Q)); 
id Projector(5, Q?, k?) = g_(0,k)*Q(mu))))/(8*Q.Q*((Q.k)^2-k.k*Q.Q)^2); 
id Projector(6, Q?, k?) = g_(0,k)*(3*Q.Q*k(mu)-3*Q.k*Q(mu))))/(8*Q.k*((Q.k)^2-k.k*Q.Q)^2); 
id Projector(7, Q?, k?) = (-(Q.Q*k(mu))+Q.k*Q(mu))))/((Q.k)^2-k.k*Q.Q)^2; 
