* DEFAULTS
#-
#: continuationlines 50
#: indentspace 4

* OUTPUT FILENAME
#define FILENAME "<output/meson_ve.h.txt>"

* DECLARATIONS.
CF gluon;
F quark, T1, T2, G1, ..., G4, Lp, Lm;
CF PR;
CF sA, sB;
CF DZ, Z;
S x, n;

* Indices (Lorentz)
I al, al1, ..., al4, be, be1, ..., be4, mu, mu1, ..., mu4, nu, nu1, ..., nu4, ro, ro1, ..., ro4, si, si1, ..., si4, la, la1, ..., la4;

* Indices (Summation. We could define these as symbols instead)
I i, i1, ..., i4, j, j1, ..., j4, m, m1, ..., m4;

* (four)-vectors. The temporal vector u is for finite temperature. Cannot use w here
V bigP, P,  bigQ, p, p1, ..., p4, k, k1, ..., k4, q, q1, ..., q4, l, l1, ..., l4, t, t1, ..., t4, u, u1, ..., u4, v, v1, ..., v4, w1, ..., w4;
V qm, qp, q, p, bigPhat, bigQhat;

cfunction DZ, DG;

function Projector, Amplitude, ConjugateAmplitude;
cfunction sum;
vector P;
symbol svp, ssp, svm, ssm, x, z, ang0,...,ang5;

********************************************************************************
** Define the trace of the BS kernel                                          **
********************************************************************************
* Trace of the BS wavefunction subject to the rainbow-ladder truncation.
local kernelL = 0;
#do i1 = 1, 8
#do i2 = 1, 8
L kernelL = kernelL + z*z *x^{   ('i1'-1)*8+'i2'} * Projector('i1',mu1,p,bigP)
                                                  * g_(1,mu)
                                                  * Amplitude('i2',mu1,l,bigP)
                                                  * g_(1,nu)
                                                  * gluon(k,mu,nu);
.sort
#enddo
#enddo

* Denominators of the projection and orthogonality check
#do i1  = 1, 8
#do i2  = 1, 8
L kernelL = kernelL+       x^{64+('i1'-1)*8+'i2'} * Projector('i1',mu,p,bigP)
                                                  * Amplitude('i2',mu,p,bigP);
.sort
#enddo
#enddo


********************************************************************************
** Define the quark rotation matrix                                           **
********************************************************************************

* Projection for attaching quarks to the amplitude, thus forming the wavefunction.
local kernelY = 0;
#do i1 = 1, 8
#do i2 = 1, 8
L kernelY = kernelY +       x^{   ('i1'-1)*8+'i2'} * Projector('i1',mu,l,bigP)
                                                   * quark(qp) * Amplitude('i2',mu,l,bigP)
                                                   * quark(qm);
.sort
#enddo
#enddo

* Denominators of the projection and orthogonality check
#do i1  = 1, 8
#do i2  = 1, 8
L kernelY = kernelY +       x^{64+('i1'-1)*8+'i2'} * Projector('i1',mu,l,bigP)
                                                   * Amplitude('i2',mu,l,bigP);
.sort
#enddo
#enddo



********************************************************************************
** Define the normalisation integral                                          **
********************************************************************************
L normalisation = 0;
#do i1 = 1, 8
#do i2 = 1, 8
L normalisation = normalisation + x^{   ('i1'-1)*8+'i2'} * ConjugateAmplitude('i1',mu,l,-bigP)
                                                         * quark(qp)
                                                         * Amplitude('i2',mu,l,bigP)
                                                         * quark(qm);
.sort
#enddo
#enddo

********************************************************************************
** Perform the substitutions                                                  **
********************************************************************************

** SUBSTITUTIONS.
* This included the Basis and the Projectors: Can be changed to pscalar, vector, qphv
#include include/BP_vector.h


.sort
id quark(p?)         = -i_*g_(1,p)*sA(p) + gi_(1)*sB(p);
id gluon(p?,mu?,nu?) = DZ(p)*PR(mu,nu,p);

id PR(mu?,nu?,p?) = d_(mu,nu) - p(mu)*p(nu)/p.p;

.sort
id sA(qp)  = svp;
id sB(qp)  = ssp;
id sA(qm)  = svm;
id sB(qm)  = ssm;

id DZ(k)  = 1; * We are collecting angular integrals
id k.k^n? = z^n;
id k = l - p;
.sort

* Perform any Dirac traces. Must do this for multiple spin lines if present.
Trace4,1;
Contract;
********************************************************************************
** Factorise the angular integrals                                            **
********************************************************************************

.sort

id p.l = (l.l + p.p - z)/2;
.sort
id p.l = (l.l + p.p - z)/2;
.sort
id p.l = (l.l + p.p - z)/2;
.sort
id p.l = (l.l + p.p - z)/2;
.sort
id p.l = (l.l + p.p - z)/2;
.sort
#do i = 6,1,-1
id z^'i' = ang{'i'-1};
#enddo
.sort


* Id Statments for the usage as a C++ code

id bigP = P;
*id q = l;

*old:
*id p?.k?   = sum(p*k);
*id p?.k?^-1= sum(p*k)^-1;

id p?.k?   = p.k;
id p?.k?^-1= p.k^-1;


.sort

* Set the optimization level. O[0,1,2,3]
Format O4, stats=on;
.sort


* Assign variables "w" for the optimisation step
ExtraSymbols,array,w;

* Change the output mode to that of Fortran 95
Format C, .d0;
Format float;

********************************************************************************
** Optimise the kernel trace and output to disk                               **
********************************************************************************

* Collect terms in "x"
B x;
.sort
#optimize kernelL
B x;
.sort


* Extract coefficient of "x"
#do i = 1, 64
L kernelL'i' = kernelL[x^'i'];
#enddo
#do i = 1, 64
L kernelLoverlap'i' = kernelL[x^{64+'i'}];
#enddo
.sort

#write 'FILENAME' "#include <typedefs.h>"
#write 'FILENAME' "#include <routing_amount_scalars.h>"
#write 'FILENAME' "#include <extra_def.h>"
#write 'FILENAME' "#include <math_ops.h>"
#write 'FILENAME' ""
#write 'FILENAME' "void kernelL(array<array<Cdoub, K_N_PROJECTORS_DIRAC>,"
#write 'FILENAME' "                            K_N_BASE_ELEMENTS_DIRAC>& kern,"
#write 'FILENAME' "                              const ArrayScalarProducts& sp,"
#write 'FILENAME' "                const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){"
#write 'FILENAME' ""
*#write 'FILENAME' "using namespace esther_pow;"
#write 'FILENAME' ""
#write 'FILENAME' "    // local variables"
*#write 'FILENAME' "    VecCdoub w; w.resize({'optimmaxvar_'+1});"
#write 'FILENAME' "    array<Cdoub, {'optimmaxvar_'+1}> w;"
#write 'FILENAME' "    Cdoub denom;"
*#write 'FILENAME' "    array< array< Cdoub, K_N_PROJECTORS_DIRAC>  "
*#write 'FILENAME' "               , K_N_BASE_ELEMENTS_DIRAC>  kern;"
#write 'FILENAME' ""
#write 'FILENAME' "%O"
#do i1 = 1, 8
#write 'FILENAME' ""
#write 'FILENAME' "    denom = %e", kernelLoverlap{('i1'-1)*8+'i1'}
#do i2 = 1, 8
#write 'FILENAME' "    kern[%s][%s] = (1.0/ denom)* %e ", {'i1'-1} , {'i2'-1},kernelL{('i1'-1)*8+'i2'}
#enddo
#enddo
#write 'FILENAME' ""
#write 'FILENAME' "}"
#write 'FILENAME' ""


********************************************************
******write into a second file**************************
********************************************************
#define FILENOW "<output/meson_ve_L.h.txt>"


#write 'FILENOW' "#include <typedefs.h>"
#write 'FILENOW' "#include <routing_amount_scalars.h>"
#write 'FILENOW' "#include <extra_def.h>"
#write 'FILENOW' "#include <math_ops.h>"
#write 'FILENOW' ""
#write 'FILENOW' "void kernelL(array<array<Cdoub, K_N_PROJECTORS_DIRAC>,"
#write 'FILENOW' "                            K_N_BASE_ELEMENTS_DIRAC>& kern,"
#write 'FILENOW' "                              const ArrayScalarProducts& sp,"
#write 'FILENOW' "                const array<Cdoub, K_ORDER_OF_ANGLE>& ang ){"
#write 'FILENOW' ""
*#write 'FILENOW' "using namespace esther_pow;"
#write 'FILENOW' ""
#write 'FILENOW' "    // local variables"
#write 'FILENOW' "    array<Cdoub, {'optimmaxvar_'+1}> w;"
#write 'FILENOW' "    Cdoub denom;"
#write 'FILENOW' ""
#write 'FILENOW' "%O"
#do i1 = 1, 8
#write 'FILENOW' ""
#write 'FILENOW' "    denom = %e", kernelLoverlap{('i1'-1)*8+'i1'}
#do i2 = 1, 8
#write 'FILENOW' "    kern[%s][%s] = (1.0/ denom)* %e ", {'i1'-1} , {'i2'-1},kernelL{('i1'-1)*8+'i2'}
#enddo
#enddo
#write 'FILENOW' ""
#write 'FILENOW' "}"
#write 'FILENOW' ""

#clearoptimize

********************************************************************************
** Optimise the quark rotation and output to disk                             **
********************************************************************************

* Collect terms in "x"
B x;
.sort
#optimize kernelY
B x;
.sort

* Extract coefficient of "x"
#do i = 1, 64
L kernelY'i' = kernelY[x^'i'];
#enddo
#do i = 1, 64
L kernelYoverlap'i' = kernelY[x^{64+'i'}];
#enddo
.sort


#write 'FILENAME' ""
*#write 'FILENAME' "array<array<Cdoub,K_N_PROJECTORS_DIRAC>,K_N_BASE_ELEMENTS_DIRAC> kernelY( ArrayScalarProducts& sp, Cdoub svm, Cdoub ssm, Cdoub svp, Cdoub ssp ){"
#write 'FILENAME' "void kernelY(array<array<Cdoub, K_N_PROJECTORS_DIRAC>,"
#write 'FILENAME' "                            K_N_BASE_ELEMENTS_DIRAC>& kern,"
#write 'FILENAME' "  const ArrayScalarProducts& sp,  const Cdoub& svm,"
#write 'FILENAME' "  const Cdoub& ssm, const Cdoub& svp, const Cdoub& ssp ){"
#write 'FILENAME' ""
*#write 'FILENAME' "using namespace esther_pow;"
#write 'FILENAME' ""
#write 'FILENAME' "    // local variables"
*#write 'FILENAME' "    VecCdoub w; w.resize({'optimmaxvar_'+1});"
#write 'FILENAME' "    array<Cdoub, {'optimmaxvar_'+1}> w;"
#write 'FILENAME' "    Cdoub denom;"
*#write 'FILENAME' "    array< array< Cdoub, K_N_PROJECTORS_DIRAC>  "
*#write 'FILENAME' "               , K_N_BASE_ELEMENTS_DIRAC>  kern;"
#write 'FILENAME' ""
#write 'FILENAME' "%O"
#do i1 = 1, 8
#write 'FILENAME' ""
#write 'FILENAME' "    denom = %e", kernelYoverlap{('i1'-1)*8+'i1'}
#do i2 = 1, 8
#write 'FILENAME' "    kern[%s][%s] = (1.0/ denom)* %e ", {'i1'-1} , {'i2'-1},kernelY{('i1'-1)*8+'i2'}
#enddo
#enddo
#write 'FILENAME' ""
*#write 'FILENAME' "  return kern;"
#write 'FILENAME' ""
#write 'FILENAME' "}"
#write 'FILENAME' ""

********************************************************
******write into a second file**************************
********************************************************
#define FILENOW "<output/meson_ve_Y.h>"

#write 'FILENOW' "array<array<Cdoub,K_N_PROJECTORS_DIRAC>,K_N_BASE_ELEMENTS_DIRAC> kernelY( ArrayScalarProducts& sp, Cdoub svm, Cdoub ssm, Cdoub svp, Cdoub ssp ){"
#write 'FILENOW' ""
*#write 'FILENOW' "using namespace esther_pow;"
#write 'FILENOW' ""
#write 'FILENOW' "    // local variables"
#write 'FILENOW' "    vector<complex<double>> w[{'optimmaxvar_'+1}];"
#write 'FILENOW' "    array<array<Cdoub,K_N_PROJECTORS_DIRAC>,K_N_BASE_ELEMENTS_DIRAC>"
#write 'FILENOW' "                                            kern;"
#write 'FILENOW' ""
#write 'FILENOW' "%O"
#do i1 = 1, 8
#write 'FILENOW' ""
#write 'FILENOW' "    denom = %e", kernelYoverlap{('i1'-1)*8+'i1'}
#do i2 = 1, 8
#write 'FILENOW' "    kern[%s][%s] = (1.0/ denom)* %e ", {'i1'-1} , {'i2'-1}, kernelY{('i1'-1)*8+'i2'}
#enddo
#enddo
#write 'FILENOW' ""
*#write 'FILENOW' "  return kern;"
#write 'FILENOW' ""
#write 'FILENOW' "}"
#write 'FILENOW' ""

#clearoptimize

********************************************************************************
** Optimise the normalisation and output to disk                              **
********************************************************************************

* Collect terms in "x"
B x;
.sort
#optimize normalisation
B x;
.sort

* Extract coefficient of "x"
#do i = 1, 64
L normalisation'i' = normalisation[x^'i'];
#enddo
.sort

#define FILENAME2 "<output/meson_ve_Renorm.h>"

#write 'FILENAME2' " void normalisation(){"
#write 'FILENAME2' " "
*#write 'FILENAME2' " using namespace esther_pow;"
#write 'FILENAME2' " "
#write 'FILENAME2' "     // local variables"
#write 'FILENAME2' "     VecCdoub w; w.resize({'optimmaxvar_'+1});"
#write 'FILENAME2' "     Cdoub denom;"
#write 'FILENAME2' "     array< array< Cdoub, K_N_PROJECTORS_DIRAC>  "
#write 'FILENAME2' "                , K_N_BASE_ELEMENTS_DIRAC>  kern;"
#write 'FILENAME2' " "
#write 'FILENAME2' " %O"
#do i1 = 1, 8
#write 'FILENAME2' ""
#do i2 = 1, 8
#write 'FILENAME2' "    kern[%s][%s] = %e", {'i1'-1} , {'i2'-1}, normalisation{('i1'-1)*8+'i2'}
#enddo
#enddo
#write 'FILENAME2' ""
#write 'FILENAME2' "}"
#write 'FILENAME2' ""

#clearoptimize

.end
