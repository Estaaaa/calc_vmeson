#!/bin/bash

cp output/meson_axial.h.txt ../../../../source/bse/Kernel/axial/meson_axial.h
cp output/meson_axial_fpi.h.txt ../../../../source/bse/Kernel/axial/meson_axial_fpi.h
cp output/meson_axial_kernelall.h.txt ../../../../source/bse/Kernel/axial/meson_axial_kernel_all.h
cp output/meson_axial_Renorm.h.txt ../../../../source/bse/Kernel/axial/meson_axial_Renorm.h
cp output/meson_axial_Renorm_fv.h.txt ../../../../source/bse/Kernel/axial/meson_axial_Renorm_fv.h
cp output/meson_axial_Y.h.txt ../../../../source/bse/Kernel/axial/meson_axial_Y.h
