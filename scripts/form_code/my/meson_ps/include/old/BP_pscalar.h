******* Substitution for the Base **********
 
id Basis(1, P?, q?) = g5_(1); 
id Basis(2, P?, q?) = i_*g_(1,P)*g5_(1); 
id Basis(3, P?, q?) = i_*q.P*g_(1,q)*g5_(1); 
id Basis(4, P?, q?) = -g_(1,P)*g_(1,q)*g5_(1)+g_(1,q)*g_(1,P)*g5_(1); 

 
id Projector(1, P?, q?) = g5_(1); 
id Projector(2, P?, q?) = i_*(-(q.q*g_(1,P)*g5_(1))+q.P*g_(1,q)*g5_(1)); 
id Projector(3, P?, q?) = i_*(q.P*g_(1,P)*g5_(1)-P.P*g_(1,q)*g5_(1)); 
id Projector(4, P?, q?) = -g_(1,P)*g_(1,q)*g5_(1)+g_(1,q)*g_(1,P)*g5_(1); 

 
id Denorminator(1, P?, q?) = 4; 
id Denorminator(2, P?, q?) = 4*((q.P)^2-P.P*q.q); 
id Denorminator(3, P?, q?) = 4*((q.P)^3-P.P*q.P*q.q); 
id Denorminator(4, P?, q?) = 16*((q.P)^2-P.P*q.q); 
