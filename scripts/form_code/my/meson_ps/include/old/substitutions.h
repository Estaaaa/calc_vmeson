******************** Standard substitutions for propagators ********************

* Vacuum Propagators
id quark(p?)         = -i_*g_(1,p)*sA(p) + gi_(1)*sB(p);
id gluon(p?,mu?,nu?) = DZ(p)*PR(mu,nu,p);


************* Generic substitutions such as transverse projectors  *************

* Vacuum projectors
id PR(mu?,nu?,p?) = d_(mu,nu) - p(mu)*p(nu)/p.p;
id PZ(mu?,nu?,p?,zeta?) = d_(mu,nu) - zeta * p(mu)*p(nu)/p.p;
