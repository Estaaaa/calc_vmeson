******* Declarations for the Base **********
 *Non-commuting functions
F Basis, Projector, Denorminator
 
******* Substitution for the Base **********
 
id Basis(0, P?, q?) = g_(0,mu); 
id Basis(1, P?, q?) = i_*g_(0,mu)*g_(0,P); 
id Basis(2, P?, q?) = i_*g_(0,mu)*g_(0,q); 
id Basis(3, P?, q?) = (g_(0,mu)*g_(0,P)*g_(0,q)-g_(0,mu)*g_(0,q)*g_(0,P))/2; 
id Basis(4, P?, q?) = q(mu); 
id Basis(5, P?, q?) = i_*g_(0,P)*q(mu); 
id Basis(6, P?, q?) = i_*g_(0,q)*q(mu); 
id Basis(7, P?, q?) = ((g_(0,P)*g_(0,q)-g_(0,q)*g_(0,P))*q(mu))/2; 
id Basis(8, P?, q?) = P(mu); 
id Basis(9, P?, q?) = i_*g_(0,P)*P(mu); 
id Basis(10, P?, q?) = i_*g_(0,q)*P(mu); 
id Basis(11, P?, q?) = ((g_(0,P)*g_(0,q)-g_(0,q)*g_(0,P))*P(mu))/2; 

 
id Projector(0, P?, q?) = g_(0,P)*(q.q*P(mu)-q.P*q(mu)); 
id Projector(1, P?, q?) = (-2*q.P-g_(0,P)*g_(0,q)+g_(0,q)*g_(0,P))*q(mu)); 
id Projector(2, P?, q?) = 2*P.P*(-g_(0,mu)*g_(0,q)+q(mu))); 
id Projector(3, P?, q?) = 2*g_(0,q)*P(mu)+2*g_(0,P)*q(mu); 
id Projector(4, P?, q?) = 2*P.P*(g_(0,mu)*g_(0,q)+q(mu)); 
id Projector(5, P?, q?) = g_(0,P)-2*(g_(0,q)*P(mu)+g_(0,P)*q(mu))))); 
id Projector(6, P?, q?) = g_(0,P)*((2*(q.P)^2+P.P*q.q)*P(mu)-3*P.P*q.P*q(mu))); 
id Projector(7, P?, q?) = 3*(-g_(0,P)*g_(0,q)+g_(0,q)*g_(0,P))*(-(q.P*P(mu))+P.P*q(mu)); 
id Projector(8, P?, q?) = (2*q.P-g_(0,P)*g_(0,q)+g_(0,q)*g_(0,P))*q(mu); 
id Projector(9, P?, q?) = P.P*g_(0,q)*q(mu)-3*q.P*(g_(0,q)*P(mu)+g_(0,P)*q(mu)))); 
id Projector(10, P?, q?) = g_(0,P)-4*(g_(0,q)*P(mu)+g_(0,P)*q(mu))))); 
id Projector(11, P?, q?) = 3*g_(0,P)*g_(0,q)*(-(q.q*P(mu))+q.P*q(mu)); 

 
id Denrominator(0, P?, q?) = 8*((q.P)^2-P.P*q.q); 
id Denrominator(1, P?, q?) = 16*((q.P)^2-P.P*q.q); 
id Denrominator(2, P?, q?) = 16*((q.P)^2-P.P*q.q); 
id Denrominator(3, P?, q?) = 16*(q.P)^2-16*P.P*q.q; 
id Denrominator(4, P?, q?) = 16*((q.P)^2-P.P*q.q); 
id Denrominator(5, P?, q?) = 16*((q.P)^2-P.P*q.q)^2; 
id Denrominator(6, P?, q?) = 8*((q.P)^2-P.P*q.q)^2; 
id Denrominator(7, P?, q?) = 16*((q.P)^2-P.P*q.q)^2; 
id Denrominator(8, P?, q?) = 16*((q.P)^2-P.P*q.q); 
id Denrominator(9, P?, q?) = 8*((q.P)^2-P.P*q.q)^2; 
id Denrominator(10, P?, q?) = 16*((q.P)^2-P.P*q.q)^2; 
id Denrominator(11, P?, q?) = 16*((q.P)^2-P.P*q.q)^2; 
