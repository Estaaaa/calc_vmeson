******* Declarations for the Base **********
 *Non-commuting functions
F Basis, Projector, Denorminator
 
******* Substitution for the Base **********
 
id Basis(0, Q?, k?) = Q.Q*g_(0,mu)-g_(0,Q)*Q(mu); 
id Basis(1, Q?, k?) = (g_(0,k)*g_(0,Q)-g_(0,Q)*g_(0,k))*Q(mu)); 
id Basis(2, Q?, k?) = (i_/2)*(-g_(0,Q)*g_(0,mu)+g_(0,mu)*g_(0,Q)); 
id Basis(3, Q?, k?) = g_(0,mu)*g_(0,k)*g_(0,Q)-g_(0,mu)*g_(0,Q)*g_(0,k))/6; 
id Basis(4, Q?, k?) = i_*(Q.Q*k(mu)-Q.k*Q(mu)); 
id Basis(5, Q?, k?) = g_(0,k)*(Q.Q*k(mu)-Q.k*Q(mu)); 
id Basis(6, Q?, k?) = Q.k*(Q.k*g_(0,mu)-g_(0,Q)*k(mu)); 
id Basis(7, Q?, k?) = Q.k*(-g_(0,k)*g_(0,mu)+g_(0,mu)*g_(0,k))); 

 
id Projector(0, Q?, k?) = k.k*g_(0,Q)*(-3*Q.k*Q.Q*k(mu)+(2*(Q.k)^2+k.k*Q.Q)*Q(mu)); 
id Projector(1, Q?, k?) = k.k*(g_(0,k)*g_(0,Q)-g_(0,Q)*g_(0,k))*Q(mu))); 
id Projector(2, Q?, k?) = k.k*(g_(0,Q)*g_(0,mu)-g_(0,mu)*g_(0,Q))); 
id Projector(3, Q?, k?) = g_(0,mu)*g_(0,k)*g_(0,Q)-g_(0,mu)*g_(0,Q)*g_(0,k); 
id Projector(4, Q?, k?) = i_*(-(Q.Q*k(mu))+Q.k*Q(mu)); 
id Projector(5, Q?, k?) = Q.Q*((Q.k)^2*g_(0,mu)+k.k*g_(0,Q)*Q(mu)-3*Q.k*(g_(0,Q)*k(mu)+g_(0,k)*Q(mu))); 
id Projector(6, Q?, k?) = Q.k*(((Q.k)^2-k.k*Q.Q)*g_(0,mu)+g_(0,k)*(3*Q.Q*k(mu)-3*Q.k*Q(mu))); 
id Projector(7, Q?, k?) = (3*Q.Q*k(mu)-3*Q.k*Q(mu))+3*g_(0,Q)*g_(0,k)*(-(Q.Q*k(mu))+Q.k*Q(mu))); 

 
id Denrominator(0, Q?, k?) = 8*Q.Q*((Q.k)^2-k.k*Q.Q)^2; 
id Denrominator(1, Q?, k?) = 16*Q.k*Q.Q*((Q.k)^2-k.k*Q.Q)^2; 
id Denrominator(2, Q?, k?) = 16*((Q.k)^2-k.k*Q.Q); 
id Denrominator(3, Q?, k?) = 48*(Q.k)^2-48*k.k*Q.Q; 
id Denrominator(4, Q?, k?) = 4*Q.Q*(-(Q.k)^2+k.k*Q.Q); 
id Denrominator(5, Q?, k?) = 8*Q.Q*((Q.k)^2-k.k*Q.Q)^2; 
id Denrominator(6, Q?, k?) = 8*Q.k*((Q.k)^2-k.k*Q.Q)^2; 
id Denrominator(7, Q?, k?) = 16*((Q.k)^2-k.k*Q.Q)^2; 
