******* Substitution for the Base **********
 
id Amplitude(1, q?, P?) = g5_(1); 
id Amplitude(2, q?, P?) = i_*g_(1,P)*g5_(1); 
id Amplitude(3, q?, P?) = i_*q.P*g_(1,q)*g5_(1); 
id Amplitude(4, q?, P?) = -g_(1,P)*g_(1,q)*g5_(1)+g_(1,q)*g_(1,P)*g5_(1); 

 
id ConjugateAmplitude(1, q?, P?) = g5_(1); 
id ConjugateAmplitude(2, q?, P?) = i_*g_(1,P)*g5_(1); 
id ConjugateAmplitude(3, q?, P?) = i_*q.P*g_(1,q)*g5_(1); 
id ConjugateAmplitude(4, q?, P?) = -g_(1,P)*g_(1,q)*g5_(1)+g_(1,q)*g_(1,P)*g5_(1); 

 
id Projector(1, q?, P?) = g5_(1); 
id Projector(2, q?, P?) = i_*(-(q.q*g_(1,P)*g5_(1))+q.P*g_(1,q)*g5_(1)); 
id Projector(3, q?, P?) = i_*(q.P*g_(1,P)*g5_(1)-P.P*g_(1,q)*g5_(1)); 
id Projector(4, q?, P?) = -g_(1,P)*g_(1,q)*g5_(1)+g_(1,q)*g_(1,P)*g5_(1); 
