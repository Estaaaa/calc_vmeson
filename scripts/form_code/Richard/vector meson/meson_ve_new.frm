* DEFAULTS
#-
#: continuationlines 50
#: indentspace 4

* OUTPUT FILENAME
#define FILENAME "<output/meson_ve.f90>"

* DECLARATIONS.
CF gluon;
F quark;
CF PR;
CF sA, sB;
CF DZ, Z;
S x, n;

* Indices (Lorentz)
I al, al1, ..., al4, be, be1, ..., be4, mu, mu1, ..., mu4, nu, nu1, ..., nu4, ro, ro1, ..., ro4, si, si1, ..., si4, la, la1, ..., la4;

* Indices (Summation. We could define these as symbols instead)
I i, i1, ..., i4, j, j1, ..., j4, m, m1, ..., m4;

* (four)-vectors. The temporal vector u is for finite temperature. Cannot use w here
V bigP, bigQ, p, p1, ..., p4, k, k1, ..., k4, q, q1, ..., q4, l, l1, ..., l4, t, t1, ..., t4, u, u1, ..., u4, v, v1, ..., v4, w1, ..., w4;
V qm, qp, q, p, bigPhat, bigQhat;



cfunction DZ, DG;

function Projector, Amplitude, ConjugateAmplitude;
cfunction sum;
vector P;
symbol svp, ssp, svm, ssm, x, z, ang0,...,ang5;

********************************************************************************
** Define the trace of the BS kernel                                          **
********************************************************************************

local kernelL = 0;
#do i1 = 1, 8
#do i2 = 1, 8
L kernelL = kernelL + z*z *x^{   ('i1'-1)*8+'i2'} * Projector('i1',mu1,p,bigP)*g_(1,mu)*Amplitude('i2',mu1,q,bigP)*g_(1,nu)*gluon(k,mu,nu);
.sort
#enddo
#enddo

#do i1  = 1, 8
#do i2  = 1, 8
L kernelL = kernelL+       x^{64+('i1'-1)*8+'i2'} * Projector('i1',mu,p,bigP)*Amplitude('i2',mu,p,bigP);
.sort
#enddo
#enddo


********************************************************************************
** Define the quark rotation matrix                                           **
********************************************************************************

local kernelY = 0;
#do i1 = 1, 8
#do i2 = 1, 8
L kernelY = kernelY+x^{   ('i1'-1)*8+'i2'}*Projector('i1',mu,q,bigP)*quark(qp)*Amplitude('i2',mu,q,bigP)*quark(qm);
.sort
#enddo
#enddo

#do i1  = 1, 8
#do i2  = 1, 8
L kernelY = kernelY+x^{64+('i1'-1)*8+'i2'}*Projector('i1',mu,q,bigP)*Amplitude('i2',mu,q,bigP);
.sort
#enddo
#enddo




********************************************************************************
** Define the normalisation integral                                          **
********************************************************************************
L normalisation = 0;
#do i1 = 1, 8
#do i2 = 1, 8
L normalisation = normalisation + x^{   ('i1'-1)*8+'i2'}*ConjugateAmplitude('i1',mu,q,-P)*quark(qp)*Amplitude('i2',mu,q,P)*quark(qm);
.sort
#enddo
#enddo

********************************************************************************
** Perform the substitutions                                                  **
********************************************************************************

******* Substitution for the Base **********

id ConjugateAmplitude(1, mu?, q?, P?) = gi_(1)*(i_*(g_(1,mu)-(g_(1,P)*P(mu))/P.P));
id ConjugateAmplitude(2, mu?, q?, P?) = gi_(1)*(g_(1,mu)*g_(1,P)-P(mu));
id ConjugateAmplitude(3, mu?, q?, P?) = gi_(1)*((q.P*((-q.P+g_(1,P)*g_(1,q))*P(mu)+P.P*(-g_(1,mu)*g_(1,q)+q(mu))))/P.P);
id ConjugateAmplitude(4, mu?, q?, P?) = gi_(1)*(i_*(g_(1,mu)*g_(1,P)*g_(1,q)-g_(1,mu)*g_(1,q)*g_(1,P)-2*g_(1,q)*P(mu)+2*g_(1,P)*q(mu)));
id ConjugateAmplitude(5, mu?, q?, P?) = gi_(1)*(-((q.P*P(mu))/P.P)+q(mu));
id ConjugateAmplitude(6, mu?, q?, P?) = gi_(1)*((i_*q.P*g_(1,P)*(-(q.P*P(mu))+P.P*q(mu)))/P.P);
id ConjugateAmplitude(7, mu?, q?, P?) = gi_(1)*((-i_)*g_(1,q)*(-((q.P*P(mu))/P.P)+q(mu)));
id ConjugateAmplitude(8, mu?, q?, P?) = gi_(1)*(((g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P))*(-(q.P*P(mu))+P.P*q(mu)))/P.P);


id Amplitude(1, mu?, q?, P?) = gi_(1)*(i_*(g_(1,mu)-(g_(1,P)*P(mu))/P.P));
id Amplitude(2, mu?, q?, P?) = gi_(1)*(g_(1,mu)*g_(1,P)-P(mu));
id Amplitude(3, mu?, q?, P?) = gi_(1)*((q.P*((-q.P+g_(1,P)*g_(1,q))*P(mu)+P.P*(-g_(1,mu)*g_(1,q)+q(mu))))/P.P);
id Amplitude(4, mu?, q?, P?) = gi_(1)*(i_*(g_(1,mu)*g_(1,P)*g_(1,q)-g_(1,mu)*g_(1,q)*g_(1,P)-2*g_(1,q)*P(mu)+2*g_(1,P)*q(mu)));
id Amplitude(5, mu?, q?, P?) = gi_(1)*(-((q.P*P(mu))/P.P)+q(mu));
id Amplitude(6, mu?, q?, P?) = gi_(1)*((i_*q.P*g_(1,P)*(-(q.P*P(mu))+P.P*q(mu)))/P.P);
id Amplitude(7, mu?, q?, P?) = gi_(1)*((-i_)*g_(1,q)*(-((q.P*P(mu))/P.P)+q(mu)));
id Amplitude(8, mu?, q?, P?) = gi_(1)*(((g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P))*(-(q.P*P(mu))+P.P*q(mu)))/P.P);


id Projector(1, mu?, q?, P?) = gi_(1)*((-i_)*(((q.P)^2-P.P*q.q)*g_(1,mu)+g_(1,q)*(-(q.P*P(mu))+P.P*q(mu))+g_(1,P)*(q.q*P(mu)-q.P*q(mu))));
id Projector(2, mu?, q?, P?) = gi_(1)*(2*q.P*(q.P*P(mu)+P.P*(g_(1,mu)*g_(1,q)-q(mu)))+g_(1,q)*g_(1,P)*(-(q.P*P(mu))+P.P*q(mu))-g_(1,P)*g_(1,q)*(q.P*P(mu)+P.P*q(mu)));
id Projector(3, mu?, q?, P?) = gi_(1)*(P.P*(-g_(1,mu)*g_(1,q)+q(mu)));
id Projector(4, mu?, q?, P?) = gi_(1)*((-i_)*(g_(1,mu)*g_(1,P)*g_(1,q)-g_(1,mu)*g_(1,q)*g_(1,P)-2*g_(1,q)*P(mu)+2*g_(1,P)*q(mu)));
id Projector(5, mu?, q?, P?) = gi_(1)*(-(q.P*P(mu))+P.P*q(mu));
id Projector(6, mu?, q?, P?) = gi_(1)*(i_*(-(q.q*(q.P*(P.P*g_(1,mu)-3*g_(1,P)*P(mu))+2*P.P*g_(1,P)*q(mu)))+q.P*((q.P)^2*g_(1,mu)+3*P.P*g_(1,q)*q(mu)-q.P*(3*g_(1,q)*P(mu)+g_(1,P)*q(mu)))));
id Projector(7, mu?, q?, P?) = gi_(1)*(i_*(P.P*((q.P)^2-P.P*q.q)*g_(1,mu)+3*P.P*g_(1,q)*(-(q.P*P(mu))+P.P*q(mu))+g_(1,P)*((2*(q.P)^2+P.P*q.q)*P(mu)-3*P.P*q.P*q(mu))));
id Projector(8, mu?, q?, P?) = gi_(1)*(3*(-g_(1,P)*g_(1,q)+g_(1,q)*g_(1,P))*(-(q.P*P(mu))+P.P*q(mu)));






id quark(p?)         = -i_*g_(1,p)*sA(p) + gi_(1)*sB(p);
id gluon(p?,mu?,nu?) = DZ(p)*PR(mu,nu,p);

id PR(mu?,nu?,p?) = d_(mu,nu) - p(mu)*p(nu)/p.p;

.sort
id sA(qp) = svp;
id sB(qp) = ssp;
id sA(qm) = svm;
id sB(qm) = ssm;

id DZ(k)  = 1; * We are collecting angular integrals
id k.k^n? = z^n;
id k = q - p;
.sort

* Perform any Dirac traces. Must do this for multiple spin lines if present.
Trace4,1;

********************************************************************************
** Factorise the angular integrals                                            **
********************************************************************************

.sort
id bigPhat.bigPhat = 1;
id bigQhat.bigQhat = 1;
.sort

id p.q = (q.q + p.p - z)/2;
.sort
id p.q = (q.q + p.p - z)/2;
.sort
id p.q = (q.q + p.p - z)/2;
.sort
id p.q = (q.q + p.p - z)/2;
.sort
id p.q = (q.q + p.p - z)/2;
.sort
#do i = 6,1,-1
id z^'i' = ang{'i'-1};
#enddo
.sort
id p?.k?   = sum(p*k);
id p?.k?^-1= sum(p*k)^-1;
.sort

* Set the optimization level. O[0,1,2,3]
Format O4, stats=on;
.sort


* Assign variables "w" for the optimisation step
ExtraSymbols,array,w;

* Change the output mode to that of Fortran 95
Format Fortran90, .d0;


********************************************************************************
** Optimise the kernel trace and output to disk                               **
********************************************************************************

* Collect terms in "x"
B x;
.sort
#optimize kernelL
B x;
.sort


* Extract coefficient of "x"
#do i = 1, 64
L kernelL'i' = kernelL[x^'i'];
#enddo
#do i = 1, 64
L kernelLoverlap'i' = kernelL[x^{64+'i'}];
#enddo
.sort


#write 'FILENAME' "subroutine kernel()"
#write 'FILENAME' ""
#write 'FILENAME' "    ! local variables"
#write 'FILENAME' "    real(dp) :: w(`optimmaxvar_')"
#write 'FILENAME' ""
#write 'FILENAME' "%O"
#do i1 = 1, 8
#write 'FILENAME' ""
#write 'FILENAME' "    denom = %e", kernelLoverlap{('i1'-1)*8+'i1'}
#do i2 = 1, 8
#write 'FILENAME' "    kern(%s,%s) = %e / denom", 'i1','i2',kernelL{('i1'-1)*8+'i2'}
#enddo
#enddo
#write 'FILENAME' ""
#write 'FILENAME' "end subroutine"
#write 'FILENAME' ""

#clearoptimize

********************************************************************************
** Optimise the quark rotation and output to disk                             **
********************************************************************************

* Collect terms in "x"
B x;
.sort
#optimize kernelY
B x;
.sort

* Extract coefficient of "x"
#do i = 1, 64
L kernelY'i' = kernelY[x^'i'];
#enddo
#do i = 1, 64
L kernelYoverlap'i' = kernelY[x^{64+'i'}];
#enddo
.sort

#write 'FILENAME' "subroutine kernelY()"
#write 'FILENAME' ""
#write 'FILENAME' "    ! local variables"
#write 'FILENAME' "    real(dp) :: w(`optimmaxvar_')"
#write 'FILENAME' ""
#write 'FILENAME' "%O"
#do i1 = 1, 8
#write 'FILENAME' ""
#write 'FILENAME' "    denom = %e", kernelYoverlap{('i1'-1)*8+'i1'}
#do i2 = 1, 8
#write 'FILENAME' "    kern(%s,%s) = %e / denom", 'i1','i2',kernelY{('i1'-1)*8+'i2'}
#enddo
#enddo
#write 'FILENAME' ""
#write 'FILENAME' "end subroutine"
#write 'FILENAME' ""

#clearoptimize

********************************************************************************
** Optimise the normalisation and output to disk                              **
********************************************************************************

* Collect terms in "x"
B x;
.sort
#optimize normalisation
B x;
.sort

* Extract coefficient of "x"
#do i = 1, 64
L normalisation'i' = normalisation[x^'i'];
#enddo
.sort

#write 'FILENAME' "subroutine normalisation()"
#write 'FILENAME' ""
#write 'FILENAME' "    ! local variables"
#write 'FILENAME' "    real(dp) :: w(`optimmaxvar_')"
#write 'FILENAME' ""
#write 'FILENAME' "%O"
#do i1 = 1, 8
#write 'FILENAME' ""
#do i2 = 1, 8
#write 'FILENAME' "    kern(%s,%s) = %e", 'i1','i2',normalisation{('i1'-1)*8+'i2'}
#enddo
#enddo
#write 'FILENAME' ""
#write 'FILENAME' "end subroutine"
#write 'FILENAME' ""

#clearoptimize

.end
