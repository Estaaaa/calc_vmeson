* DEFAULTS
#-
#: continuationlines 50
#: indentspace 4

* DECLARATIONS.
CF gluon;
F quark, T1, T2, G1, ..., G4, Lp, Lm, Meson, Vertex;
CF PR;
CF sA, sB;
CF DZ, Z;
S x, n;

* Indices (Lorentz)
I al, al1, ..., al4, be, be1, ..., be4, mu, mu1, ..., mu4, nu, nu1, ..., nu4, ro, ro1, ..., ro4, si, si1, ..., si4, la, la1, ..., la4;

* Indices (Summation. We could define these as symbols instead)
I i, i1, ..., i4, j, j1, ..., j4, m, m1, ..., m4;

* (four)-qphvs. The temporal vector u is for finite temperature. Cannot use w here
V bigP, P,  bigQ, p, p1, ..., p4, k, k1, ..., k4, q, q1, ..., q4, l, l1, ..., l4, t, t1, ..., t4, u, u1, ..., u4, v, v1, ..., v4, w1, ..., w4;
V qm, qp, q, p, bigPhat, bigQhat;
V rp, rm, Q, Qp;

cfunction DZ, DG;
function Projector, Amplitude, ConjugateAmplitude;
function CAmptff, Amptff, Ptff, DPtff, formfactor;
cfunction sum;
vector P;
symbol svp, ssp, svm, ssm, x, z, ang0,...,ang5;
symbol Z2, XC;
symbol A3 B3, A1, B1, A2, B2, f1, ... ,f4;

********************************************************************************
** Define the whole BS kernel                                                 **
********************************************************************************
* Trace of the BS wavefunction subject to the rainbow-ladder truncation.
local formfactorall = 0;
#do i3 = 1, 5

*Set-up with the amplitude not the wavefunction chi:
*-this takes too long to generate though.
L formfactorall = formfactorall +    x^{( 'i3')}
                                          * Ptff('i3',Q, Qp, mu, nu)
                                          * Vertex( 1, nu, rp, Qp )
                                          * quark(k1)
                                          * Meson(l, P)
                                          * quark(k2)
                                          * Vertex( 1, mu, rm, -Q )
                                          * quark(k3);




.sort
#enddo


#do i = 1, 5
L denom'i' = DPtff('i', Q, Qp, mu, nu );
#enddo
*.sort

********************************************************************************
** Perform the substitutions                                                  **
********************************************************************************

** SUBSTITUTIONS.



.sort
id quark(p?)         = -i_*g_(1,p)*sA(p) + gi_(1)*sB(p);
id gluon(p?,mu?,nu?) = DZ(p)*PR(mu,nu,p);

id PR(mu?,nu?,p?) = d_(mu,nu) - p(mu)*p(nu)/p.p;

*id Rtrans(q?, p?, mu?, nu?) =  d_(mu,nu) + 1/XC * (q.p * (q(mu)*p(nu)+q(nu)*p(mu)) -q.q *p(mu)*p(nu) - p.p *q(mu)*q(nu));

id Meson(q?,P?) = f1 * Amplitude(1, q, P) + f2 *  Amplitude(2, q, P) + f3 * Amplitude(3, q, P) + f4 * Amplitude(4, q, P);
*id Meson(1, q?, P?) =  f1 *Amplitude(1,q,P);
*id Meson(2, q?, P?) =  f2 *Amplitude(2,q,P);
*id Meson(3, q?, P?) =  f3 *Amplitude(3,q,P);
*id Meson(4, q?, P?) =  f4 *Amplitude(4,q,P);

*id formfactor(q?,P?,mu?,nu?) = Amptff(1,q,P,mu,nu) + Amptff(2,q,P,mu,nu) +Amptff(3,q,P,mu,nu) + Amptff(4,q,P,mu,nu)+ Amptff(5,q,P,mu,nu);

* This included the Basis and the Projectors: Can be changed to pscalar, qphv, qphv
#include include/BP_qphv.h
#include include/BP_scalar_meson.h
#include include/scalar_tff_full.h

.sort

id sA(k3)  = A3;
id sB(k3)  = B3;
id sA(k1)  = A1;
id sB(k1)  = B1;
id sA(k2)  = A2;
id sB(k2)  = B2;

*id DZ(k)  = 1; * We are collecting angular integrals
*id k.k^n? = z^n;
*id k = l - p;
*.sort
*id k3= l + p/2;
.sort


**Replacements when testing for an easier set-up
id k1 =  l + P/2;
id k2 =  l- P/2;
*id Q =  (p+P)/2;
*id Qp = (p-P)/2;
*id P = Q-Qp;
.sort

* Perform any Dirac traces. Must do this for multiple spin lines if present.
Trace4,1;
.sort
Contract;
.sort
********************************************************************************
** Factorise the angular integrals                                            **
********************************************************************************

.sort

*id p.l = (l.l + p.p - z)/2;
*.sort
*id p.l = (l.l + p.p - z)/2;
*.sort
*id p.l = (l.l + p.p - z)/2;
*.sort
*id p.l = (l.l + p.p - z)/2;
*.sort
*id p.l = (l.l + p.p - z)/2;
*.sort
*#do i = 6,1,-1
*id z^'i' = ang{'i'-1};
*#enddo
.sort


* Id Statments for the usage as a C++ code

id bigP = P;
id bigP.bigP^n? = P.P^n;
*id 1/bigP.bigP = 1/P.P;
*id bigP^-1 = P^-1;
*id q = l;


*id Q = (p+P)/2;
*id Qp = (p-P)/2;
*.sort

*doesnt needed anymore?
*id p?.k?   = p.k;
*id p?.k?^-1= p.k^-1;


.sort
id f2=0;
id f3=0;
id f4=0;
.sort
* Change output mode to C:
*Format Fortran90, ._r8;
Format C, .d0;
Format float;


**WRITING OUT STUFF MORE STUFF AND OPTIMIZATION:
********************************************************************************
********************************************************************************


* Set the optimization level. O[0,1,2,3]
Format O4, stats=on;
.sort

* Assign variables "w" for the optimisation step
ExtraSymbols,array,w;


********************************************************************************
**WRITING OUT STUFF
********************************************************************************
********************************************************************************

*print;
*print formfactorall;


********************************************************************************
**Writing the kernel with optimization to disk                             **
********************************************************************************

* OUTPUT FILENAME
#define FILENAME "<output/easy_scalar_ff.cpp>"

* Collect terms in "x"
B x;
.sort
#optimize formfactorall
B x;
.sort


* Extract coefficient of "x"
#do i = 1, 5
L formfactorall'i' = formfactorall[x^'i'];
#enddo
.sort

B f1, f2, f3, f4;
.sort


#write 'FILENAME' "#include <typedefs.h>"
#write 'FILENAME' "#include <routing_amount_scalars.h>"
#write 'FILENAME' "#include <extra_def.h>"
#write 'FILENAME' "#include <tff_kernel_scalar_amp_mf.h> "
#write 'FILENAME' "#include <fv.h>"
#write 'FILENAME' ""
#write 'FILENAME' "void easy_kernel_formfactor_scalar_amp_mf(VecCdoub& kern,"
#write 'FILENAME' "const fv<Cdoub>& P, const fv<Cdoub>& Q, const fv<Cdoub>& Qp,"
#write 'FILENAME' "const fv<Cdoub>& p, const fv<Cdoub>& l, const fv<Cdoub>& rp,"
#write 'FILENAME' "const fv<Cdoub>& rm , const fv<Cdoub> & k3,"
#write 'FILENAME' "const VecCdoub& meson, "
#write 'FILENAME' "const Cdoub& A3, const Cdoub& B3,"
#write 'FILENAME' "const Cdoub& A1, const Cdoub& B1,"
#write 'FILENAME' "const Cdoub& A2, const Cdoub& B2){"
#write 'FILENAME' ""
#write 'FILENAME' ""
#write 'FILENAME' "//fourvector products"
#write 'FILENAME' "Cdoub P_P = P*P; Cdoub P_Q = P*Q; Cdoub P_Qp = P*Qp;"
#write 'FILENAME' "Cdoub P_k3 = P*k3; Cdoub P_l = P*l; Cdoub P_rp = P*rp;"
#write 'FILENAME' "Cdoub P_rm = P*rm; Cdoub Q_Q = Q*Q; Cdoub Q_Qp = Q*Qp;"
#write 'FILENAME' "Cdoub k3_Q = Q*k3; Cdoub l_Q = Q*l; Cdoub rp_Q = Q*rp;"
#write 'FILENAME' "Cdoub rm_Q = Q*rm; Cdoub Qp_Qp = Qp*Qp; Cdoub k3_Qp = Qp*k3;"
#write 'FILENAME' "Cdoub l_Qp = Qp*l; Cdoub rp_Qp = Qp*rp; Cdoub rm_Qp = Qp*rm;"
#write 'FILENAME' "Cdoub k3_k3 = k3*k3; Cdoub k3_l = k3*l; Cdoub k3_rp = k3*rp;"
#write 'FILENAME' "Cdoub k3_rm = k3*rm; Cdoub l_l = l*l; Cdoub l_rp = l*rp;"
#write 'FILENAME' "Cdoub l_rm = l*rm; Cdoub rp_rp = rp*rp; Cdoub rp_rm = rp*rm;"
#write 'FILENAME' "Cdoub rm_rm = rm*rm; "
#write 'FILENAME' ""
#write 'FILENAME' "Cdoub f1=meson[0]; Cdoub f2=meson[1]; Cdoub f3=meson[2];"
#write 'FILENAME' "Cdoub f4=meson[3];"
#write 'FILENAME' ""
#write 'FILENAME' ""
*#write 'FILENAME' "    Cdoub XC =pow(Q_Qp,2.0) - Q_Q*Qp_Qp; "
#write 'FILENAME' ""
#write 'FILENAME' "    // local variables"
#write 'FILENAME' "    array<Cdoub, {'optimmaxvar_'+1}> w;"
#write 'FILENAME' ""
#write 'FILENAME' "%O"
*#do i = 1, 5
*#write 'FILENAME' "    denom[%s] =  %e ", {'i'} , denom{'i'}
*#enddo
#do i = 1, 5
#write 'FILENAME' "    kern[%s] =  %e ", {'i'-1} , formfactorall{'i'}
#enddo
#write 'FILENAME' ""
#write 'FILENAME' "}"
#write 'FILENAME' ""


#clearoptimize




.end
