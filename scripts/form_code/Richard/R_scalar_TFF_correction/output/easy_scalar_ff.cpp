#include <typedefs.h>
#include <routing_amount_scalars.h>
#include <extra_def.h>
#include <tff_kernel_scalar_amp_mf.h> 
#include <fv.h>

void easy_kernel_formfactor_scalar_amp_mf(VecCdoub& kern,
const fv<Cdoub>& P, const fv<Cdoub>& Q, const fv<Cdoub>& Qp,
const fv<Cdoub>& p, const fv<Cdoub>& l, const fv<Cdoub>& rp,
const fv<Cdoub>& rm , const fv<Cdoub> & k3,
const VecCdoub& meson, 
const Cdoub& A3, const Cdoub& B3,
const Cdoub& A1, const Cdoub& B1,
const Cdoub& A2, const Cdoub& B2){


//fourvector products
Cdoub P_P = P*P; Cdoub P_Q = P*Q; Cdoub P_Qp = P*Qp;
Cdoub P_k3 = P*k3; Cdoub P_l = P*l; Cdoub P_rp = P*rp;
Cdoub P_rm = P*rm; Cdoub Q_Q = Q*Q; Cdoub Q_Qp = Q*Qp;
Cdoub k3_Q = Q*k3; Cdoub l_Q = Q*l; Cdoub rp_Q = Q*rp;
Cdoub rm_Q = Q*rm; Cdoub Qp_Qp = Qp*Qp; Cdoub k3_Qp = Qp*k3;
Cdoub l_Qp = Qp*l; Cdoub rp_Qp = Qp*rp; Cdoub rm_Qp = Qp*rm;
Cdoub k3_k3 = k3*k3; Cdoub k3_l = k3*l; Cdoub k3_rp = k3*rp;
Cdoub k3_rm = k3*rm; Cdoub l_l = l*l; Cdoub l_rp = l*rp;
Cdoub l_rm = l*rm; Cdoub rp_rp = rp*rp; Cdoub rp_rm = rp*rm;
Cdoub rm_rm = rm*rm; 

Cdoub f1=meson[0]; Cdoub f2=meson[1]; Cdoub f3=meson[2];
Cdoub f4=meson[3];



    // local variables
    array<Cdoub, 31> w;


    w[1]=P_P;
    w[2]=Q_Q;
    w[3]=Q_Qp;
    w[4]=Qp_Qp;
    w[5]=P_k3;
    w[6]=P_Q;
    w[7]=k3_Q;
    w[8]=k3_Qp;
    w[9]=l_Qp;
    w[10]=P_Qp;
    w[11]=l_Q;
    w[12]=k3_l;
    w[13]=l_l;
   w[14]=A2*B1;
   w[15]=B2*A1;
   w[16]=w[14] + w[15];
   w[17]=2.E+0*w[11];
   w[17]=w[16]*w[17];
   w[14]=w[14] - w[15];
   w[15]=w[14]*w[6];
   w[15]=w[17] - w[15];
   w[17]=w[15]*w[7];
   w[18]=A3*w[17];
   w[19]=2.E+0*w[12];
   w[19]=w[16]*w[19];
   w[20]=w[14]*w[5];
   w[19]=w[19] - w[20];
   w[20]=2.E+0*A3;
   w[21]=w[19]*w[20];
   w[22]=B2*B1;
   w[23]=A2*A1;
   w[24]=w[23]*w[13];
   w[22]=w[22] - w[24];
   w[24]=w[23]*w[1];
   w[22]=w[24] + 4.E+0*w[22];
   w[22]=w[22]*B3;
   w[21]=w[22] + w[21];
   w[22]=w[2]*w[21];
   w[22]= - 4.E+0*w[18] + w[22];
   w[22]=w[4]*w[22];
   w[24]=pow(w[2],2);
   w[25]= - w[21]*w[24];
   w[22]=w[25] + w[22];
   w[22]=w[4]*w[22];
   w[25]=2.E+0*w[9];
   w[16]=w[16]*w[25];
   w[14]=w[14]*w[10];
   w[14]=w[16] - w[14];
   w[16]=w[14]*w[7];
   w[15]=w[15]*w[8];
   w[15]=w[16] + w[15];
   w[16]=w[15]*A3;
   w[25]=w[10]*w[11];
   w[26]=w[9]*w[6];
   w[25]=w[25] - w[26];
   w[23]=w[23]*w[25];
   w[25]=2.E+0*B3;
   w[25]=w[23]*w[25];
   w[26]=w[16] + w[25];
   w[27]=w[4] - w[2];
   w[26]=w[27]*w[26];
   w[21]= - w[3]*w[21]*w[27];
   w[21]=2.E+0*w[26] + w[21];
   w[21]=w[3]*w[21];
   w[14]=w[14]*w[8];
   w[24]=w[14]*w[24]*A3;
   w[21]=w[21] + 4.E+0*w[24] + w[22];
   w[21]=f1*w[21];
   w[17]=w[17] - w[14];
   w[20]=w[17]*w[20];
   w[22]= - w[3]*w[20];
   w[22]=w[22] - w[26];
   w[22]=2.E+0*f1*w[22];
   w[26]=w[2]*A3;
   w[14]=w[26]*w[14];
   w[27]=w[4]*w[18];
   w[27]= - w[14] + w[27];
   w[28]=4.E+0*f1;
   w[27]=w[27]*w[28];
   w[29]=w[19]*w[26];
   w[18]= - w[29] + 3.E+0*w[18];
   w[18]=w[18]*w[4];
   w[23]=B3*w[23];
   w[23]=w[23] + w[16];
   w[30]= - w[3]*A3*w[19];
   w[23]=2.E+0*w[23] + w[30];
   w[23]=w[3]*w[23];
   w[14]=w[23] - 3.E+0*w[14] - w[18];
   w[14]=w[3]*w[14];
   w[16]= - w[25] + w[16];
   w[16]=w[4]*w[2]*w[16];
   w[14]=w[16] + w[14];
   w[14]=w[14]*w[28];
   w[16]= - A3*w[17];
   w[16]=w[16] + w[29];
   w[16]=w[2]*w[16];
   w[16]=w[16] + w[18];
   w[16]=w[4]*w[16];
   w[17]=w[4]*A3;
   w[17]=w[17] - w[26];
   w[15]= - w[15]*w[17];
   w[17]=w[19]*w[17];
   w[17]= - w[20] + w[17];
   w[17]=w[3]*w[17];
   w[15]=3.E+0*w[15] + w[17];
   w[15]=w[3]*w[15];
   w[15]=w[15] - 3.E+0*w[24] + w[16];
   w[15]=w[15]*w[28];

    kern[0] =   + w[21];
 
    kern[1] =   + w[22];
 
    kern[2] =   + w[27];
 
    kern[3] =   + w[14];
 
    kern[4] =   + w[15];
 

}

