******* Substitution for the Base **********
**worked with this code June 2018.


id Vertex(1, mu?, q?, P?) = gi_(1)*(i_*g_(1,mu));
id Vertex(2, mu?, q?, P?) = gi_(1)*(i_*g_(1,q)*q(mu));
id Vertex(3, mu?, q?, P?) = gi_(1)*(q(mu));
id Vertex(4, mu?, q?, P?) = gi_(1)*((g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P))*q(mu));
id Vertex(5, mu?, q?, P?) = gi_(1)*((-i_)*(-(P.P*g_(1,mu))+g_(1,P)*P(mu)));
id Vertex(6, mu?, q?, P?) = gi_(1)*((q.P*(P.P*(g_(1,q)*g_(1,mu)-g_(1,mu)*g_(1,q))+(g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P))*P(mu)))/2);
id Vertex(7, mu?, q?, P?) = gi_(1)*((g_(1,P)*g_(1,mu)-g_(1,mu)*g_(1,P))/2);
id Vertex(8, mu?, q?, P?) = gi_(1)*((-i_/6)*(g_(1,P)*g_(1,q)*g_(1,mu)-g_(1,P)*g_(1,mu)*g_(1,q)-g_(1,q)*g_(1,P)*g_(1,mu)+g_(1,q)*g_(1,mu)*g_(1,P)+g_(1,mu)*g_(1,P)*g_(1,q)-g_(1,mu)*g_(1,q)*g_(1,P)));
id Vertex(9, mu?, q?, P?) = gi_(1)*(q.P*P(mu)-P.P*q(mu));
id Vertex(10, mu?, q?, P?) = gi_(1)*(i_*g_(1,q)*(-(q.P*P(mu))+P.P*q(mu)));
id Vertex(11, mu?, q?, P?) = gi_(1)*(i_*q.P*(q.P*g_(1,mu)-g_(1,P)*q(mu)));
id Vertex(12, mu?, q?, P?) = gi_(1)*((q.P*(g_(1,q)*g_(1,mu)-g_(1,mu)*g_(1,q))+(g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P))*q(mu))/2);
