******* Substitution for the Base **********
 
id ConjugateAmplitude(1, q?, P?) = gi_(1)*(1); 
id ConjugateAmplitude(2, q?, P?) = gi_(1)*((-i_)*q.P*g_(1,P)); 
id ConjugateAmplitude(3, q?, P?) = gi_(1)*((-i_)*g_(1,q)); 
id ConjugateAmplitude(4, q?, P?) = gi_(1)*(g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P)); 

 
id Amplitude(1, q?, P?) = gi_(1)*(1); 
id Amplitude(2, q?, P?) = gi_(1)*((-i_)*q.P*g_(1,P)); 
id Amplitude(3, q?, P?) = gi_(1)*((-i_)*g_(1,q)); 
id Amplitude(4, q?, P?) = gi_(1)*(g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P)); 

 
id Projector(1, q?, P?) = gi_(1)*(1); 
id Projector(2, q?, P?) = gi_(1)*(i_*(-(q.q*g_(1,P))+q.P*g_(1,q))); 
id Projector(3, q?, P?) = gi_(1)*(i_*(-(q.P*g_(1,P))+P.P*g_(1,q))); 
id Projector(4, q?, P?) = gi_(1)*(g_(1,P)*g_(1,q)-g_(1,q)*g_(1,P)); 

 
