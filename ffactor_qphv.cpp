/*
 * ffactor_BC.h
 *
 * includes the form factor integral
 *
 *
 * ---------------------------------------------------------------------
 * 23.09.15 Esther Weil
 *
 *Content: pigamma_all_ballchui  FORM CODE: decay_BC_Chi
 *
 *
 *pigamma_all: Calculates the pi->gamma gamma form factor including all BSA (E,F,G,H)
 * with the Ball Chui vertex
 *
 * ---------------------------------------------------------------------
 */

#include "ffactor_qphv.h"


#include "Toolbox/fourvec_ops.h"


void trace_decay_qph(MatrixXcd &Chi, complex<double> &x1, complex<double> &y1, complex<double> &z1, complex<double> &x2, complex<double> &y2, complex<double> &z2, complex<double> &fl10, complex<double> &fl11, complex<double> &fl12, complex<double> &fl13,
                     complex<double> &fl14, complex<double> &fl15, complex<double> &fl16, complex<double> &fl17, complex<double> &fl20, complex<double> &fl21, complex<double> fl22, complex<double> &fl23, complex<double> &fl24, complex<double> &fl25, complex<double> &fl26, complex<double> &fl27,
                     complex<double> &Ak3, complex<double> &Bk3, four_vec &k3, four_vec &k4, four_vec &k5, four_vec &P, four_vec &q, four_vec &p, four_vec &l1, four_vec &l2,
                     complex<double> &E, complex<double> &F, complex<double> &G, complex<double> &H)
{

    complex<double> P_k4, P_k5, q_k4, q_k5, k4_q, k5_q, P_q, q_p, k4_k4, k5_k5, k4_k5;
    complex<double> P_l1, P_l2, l1_l1, l1_l2, l2_l2, q_l1, q_l2, k3_k5, k3_k4, k5_l2, k4_l2, k3_l2, k5_l1, k4_l1, k3_l1, P_k3, k3_q;


    P_k4=P*k4; P_k5=P*k5; q_k4=q*k4; q_k5=q*k5; k5_q=k5*q; k4_q=q*k4;
    P_l1=P*l1; P_l2=P*l2, l1_l1=l1*l1; l2_l2=l2*l2; l1_l2=l1*l2; q_l1=q*l1; q_l2=q*l2; k3_k4=k3*k4; k3_k5=k3*k5;
    k3_l1=k3*l1; k3_l2=k3*l2; k4_l1=k4*l1; k4_l2=k4*l2; k5_l1=k5*l1; k5_l2=k5*l2; P_k3=P*k3; k3_q=k3*q;

    P_q=P*q;
    q_p=q*p;
    k4_k4=k4*k4;
    k5_k5=k5*k5;
    k4_k5=k4*k5;



     //#########################################################################################

       Chi(0,0) =
           + H * ( 1.6E+1*x1*x2*P_k4*k5_q*Bk3 - 1.6E+1*x1*x2*P_k5*k4_q*Bk3
              );

          Chi(0,0) +=  + G * ( 8.E+0*x1*x2*P_q*k3_k4*k5_q*Ak3 - 8.E+0*x1*
             x2*P_q*k3_k5*k4_q*Ak3 );

          Chi(0,0) +=  + F * (  - 8.E+0*x1*x2*P_k4*k3_k5*Ak3 + 8.E+0*x1*
             x2*P_k5*k3_k4*Ak3 );

       Chi(0,1) =
           + H * ( 1.6E+1*x1*y2*P_k4*k5_q*l2_l2*Bk3 - 1.6E+1*x1*y2*P_k4*
             k5_l2*q_l2*Bk3 - 1.6E+1*x1*y2*P_k5*k4_q*l2_l2*Bk3 + 1.6E+1*x1*
             y2*P_k5*k4_l2*q_l2*Bk3 + 1.6E+1*x1*y2*P_l2*k4_q*k5_l2*Bk3 -
             1.6E+1*x1*y2*P_l2*k4_l2*k5_q*Bk3 );

          Chi(0,1) +=  + G * ( 8.E+0*x1*y2*P_q*k3_k4*k5_q*l2_l2*Ak3 -
             8.E+0*x1*y2*P_q*k3_k4*k5_l2*q_l2*Ak3 - 8.E+0*x1*y2*P_q*k3_k5*
             k4_q*l2_l2*Ak3 + 8.E+0*x1*y2*P_q*k3_k5*k4_l2*q_l2*Ak3 + 8.E+0*
             x1*y2*P_q*k3_l2*k4_q*k5_l2*Ak3 - 8.E+0*x1*y2*P_q*k3_l2*k4_l2*
             k5_q*Ak3 );

          Chi(0,1) +=  + F * (  - 8.E+0*x1*y2*P_k4*k3_k5*l2_l2*Ak3 +
             8.E+0*x1*y2*P_k4*k3_l2*k5_l2*Ak3 + 8.E+0*x1*y2*P_k5*k3_k4*
             l2_l2*Ak3 - 8.E+0*x1*y2*P_k5*k3_l2*k4_l2*Ak3 - 8.E+0*x1*y2*
             P_l2*k3_k4*k5_l2*Ak3 + 8.E+0*x1*y2*P_l2*k3_k5*k4_l2*Ak3 );

       Chi(0,2) =
           + H * ( 1.6E+1*x1*z2*P_k4*k3_k5*q_l2*Ak3 - 1.6E+1*x1*z2*P_k4*
             k3_l2*k5_q*Ak3 - 1.6E+1*x1*z2*P_k5*k3_k4*q_l2*Ak3 + 1.6E+1*x1*
             z2*P_k5*k3_l2*k4_q*Ak3 + 1.6E+1*x1*z2*P_l2*k3_k4*k5_q*Ak3 -
             1.6E+1*x1*z2*P_l2*k3_k5*k4_q*Ak3 );

       Chi(0,3) =
           + H * ( 1.6E+1*x1*P_k4*k5_k5*k5_q*Bk3*fl20 - 1.6E+1*x1*P_k5*k4_q
             *k5_k5*Bk3*fl20 );

          Chi(0,3) +=  + G * ( 8.E+0*x1*P_q*k3_k4*k5_k5*k5_q*Ak3*fl20 -
             8.E+0*x1*P_q*k3_k5*k4_q*k5_k5*Ak3*fl20 );

          Chi(0,3) +=  + F * (  - 8.E+0*x1*P_k4*k3_k5*k5_k5*Ak3*fl20 +
             8.E+0*x1*P_k5*k3_k4*k5_k5*Ak3*fl20 );

       Chi(0,4) =
           + H * (  - 8.E+0*x1*P_k4*k3_k5*k5_k5*k5_l2*q_l2*Ak3*fl21 - 8.E+0
             *x1*P_k4*k3_l2*k5_k5*k5_q*k5_l2*Ak3*fl21 + 8.E+0*x1*P_k5*k3_k4
             *k5_k5*k5_l2*q_l2*Ak3*fl21 + 8.E+0*x1*P_k5*k3_l2*k4_q*k5_k5*
             k5_l2*Ak3*fl21 - 8.E+0*x1*P_l2*k3_k4*k5_k5*k5_q*k5_l2*Ak3*fl21
              + 8.E+0*x1*P_l2*k3_k5*k4_q*k5_k5*k5_l2*Ak3*fl21 );

          Chi(0,4) +=  + G * (  - 8.E+0*x1*P_q*k4_q*k5_k5*pow(k5_l2,2)*
             Bk3*fl21 + 8.E+0*x1*P_q*k4_l2*k5_k5*k5_q*k5_l2*Bk3*fl21 );

          Chi(0,4) +=  + F * (  - 8.E+0*x1*P_k4*k5_k5*pow(k5_l2,2)*Bk3*
             fl21 + 8.E+0*x1*P_k5*k4_l2*k5_k5*k5_l2*Bk3*fl21 );

          Chi(0,4) +=  + E * (  - 8.E+0*x1*k3_k4*k5_k5*pow(k5_l2,2)*Ak3*
             fl21 + 8.E+0*x1*k3_k5*k4_l2*k5_k5*k5_l2*Ak3*fl21 );

       Chi(0,5) =
           + H * (  - 1.6E+1*x1*P_k4*k3_k5*k5_q*Ak3*fl22 + 1.6E+1*x1*P_k5*
             k3_k5*k4_q*Ak3*fl22 );

          Chi(0,5) +=  + G * ( 8.E+0*x1*P_q*k4_k5*k5_q*Bk3*fl22 - 8.E+0*
             x1*P_q*k4_q*k5_k5*Bk3*fl22 );

          Chi(0,5) +=  + F * (  - 8.E+0*x1*P_k4*k5_k5*Bk3*fl22 + 8.E+0*x1
             *P_k5*k4_k5*Bk3*fl22 );

          Chi(0,5) +=  + E * (  - 8.E+0*x1*k3_k4*k5_k5*Ak3*fl22 + 8.E+0*
             x1*k3_k5*k4_k5*Ak3*fl22 );

       Chi(0,6) =
           + H * (  - 8.E+0*x1*P_k4*k5_k5*q_l2*Bk3*fl23 + 8.E+0*x1*P_k4*
             k5_q*k5_l2*Bk3*fl23 + 8.E+0*x1*P_k5*k4_k5*q_l2*Bk3*fl23 -
             8.E+0*x1*P_k5*k4_q*k5_l2*Bk3*fl23 - 8.E+0*x1*P_l2*k4_k5*k5_q*
             Bk3*fl23 + 8.E+0*x1*P_l2*k4_q*k5_k5*Bk3*fl23 );

          Chi(0,6) +=  + G * (  - 4.E+0*x1*P_q*k3_k4*k5_k5*q_l2*Ak3*fl23
              + 4.E+0*x1*P_q*k3_k4*k5_q*k5_l2*Ak3*fl23 + 4.E+0*x1*P_q*k3_k5
             *k4_k5*q_l2*Ak3*fl23 + 4.E+0*x1*P_q*k3_k5*k4_q*k5_l2*Ak3*fl23
              - 8.E+0*x1*P_q*k3_k5*k4_l2*k5_q*Ak3*fl23 + 4.E+0*x1*P_q*k3_l2
             *k4_k5*k5_q*Ak3*fl23 - 4.E+0*x1*P_q*k3_l2*k4_q*k5_k5*Ak3*fl23
              );

          Chi(0,6) +=  + F * ( 4.E+0*x1*P_k4*k3_k5*k5_l2*Ak3*fl23 - 4.E+0
             *x1*P_k4*k3_l2*k5_k5*Ak3*fl23 + 4.E+0*x1*P_k5*k3_k4*k5_l2*Ak3*
             fl23 - 8.E+0*x1*P_k5*k3_k5*k4_l2*Ak3*fl23 + 4.E+0*x1*P_k5*
             k3_l2*k4_k5*Ak3*fl23 - 4.E+0*x1*P_l2*k3_k4*k5_k5*Ak3*fl23 +
             4.E+0*x1*P_l2*k3_k5*k4_k5*Ak3*fl23 );

          Chi(0,6) +=  + E * (  - 8.E+0*x1*k4_k5*k5_l2*Bk3*fl23 + 8.E+0*
             x1*k4_l2*k5_k5*Bk3*fl23 );

       Chi(0,7) =
           + H * (  - 8.E+0*x1*P_k4*k3_k5*k5_k5*q_l2*Ak3*fl24 + 8.E+0*x1*
             P_k4*k3_l2*k5_k5*k5_q*Ak3*fl24 + 8.E+0*x1*P_k5*k3_k4*k5_k5*
             q_l2*Ak3*fl24 - 8.E+0*x1*P_k5*k3_l2*k4_q*k5_k5*Ak3*fl24 -
             8.E+0*x1*P_l2*k3_k4*k5_k5*k5_q*Ak3*fl24 + 8.E+0*x1*P_l2*k3_k5*
             k4_q*k5_k5*Ak3*fl24 );

       Chi(0,8) =
           + H * ( 8.E+0*x1*P_k4*k5_k5*k5_q*l2_l2*Bk3*fl25 - 8.E+0*x1*P_k4*
             k5_k5*k5_l2*q_l2*Bk3*fl25 - 8.E+0*x1*P_k5*k4_q*k5_k5*l2_l2*Bk3
             *fl25 + 8.E+0*x1*P_k5*k4_l2*k5_k5*q_l2*Bk3*fl25 + 8.E+0*x1*
             P_l2*k4_q*k5_k5*k5_l2*Bk3*fl25 - 8.E+0*x1*P_l2*k4_l2*k5_k5*
             k5_q*Bk3*fl25 );

          Chi(0,8) +=  + G * ( 4.E+0*x1*P_q*k3_k4*k5_k5*k5_q*l2_l2*Ak3*
             fl25 - 4.E+0*x1*P_q*k3_k4*k5_k5*k5_l2*q_l2*Ak3*fl25 - 4.E+0*x1
             *P_q*k3_k5*k4_q*k5_k5*l2_l2*Ak3*fl25 + 4.E+0*x1*P_q*k3_k5*
             k4_l2*k5_k5*q_l2*Ak3*fl25 + 4.E+0*x1*P_q*k3_l2*k4_q*k5_k5*
             k5_l2*Ak3*fl25 - 4.E+0*x1*P_q*k3_l2*k4_l2*k5_k5*k5_q*Ak3*fl25
              );

          Chi(0,8) +=  + F * (  - 4.E+0*x1*P_k4*k3_k5*k5_k5*l2_l2*Ak3*
             fl25 + 4.E+0*x1*P_k4*k3_l2*k5_k5*k5_l2*Ak3*fl25 + 4.E+0*x1*
             P_k5*k3_k4*k5_k5*l2_l2*Ak3*fl25 - 4.E+0*x1*P_k5*k3_l2*k4_l2*
             k5_k5*Ak3*fl25 - 4.E+0*x1*P_l2*k3_k4*k5_k5*k5_l2*Ak3*fl25 +
             4.E+0*x1*P_l2*k3_k5*k4_l2*k5_k5*Ak3*fl25 );

       Chi(0,9) =
           + H * ( 8.E+0*x1*P_k4*k5_k5*k5_l2*q_l2*Bk3*fl26 + 8.E+0*x1*P_k4*
             k5_q*pow(k5_l2,2)*Bk3*fl26 - 8.E+0*x1*P_k5*k4_k5*k5_l2*q_l2*
             Bk3*fl26 - 8.E+0*x1*P_k5*k4_q*pow(k5_l2,2)*Bk3*fl26 + 8.E+0*x1
             *P_l2*k4_k5*k5_q*k5_l2*Bk3*fl26 - 8.E+0*x1*P_l2*k4_q*k5_k5*
             k5_l2*Bk3*fl26 );

          Chi(0,9) +=  + G * ( 4.E+0*x1*P_q*k3_k4*k5_k5*k5_l2*q_l2*Ak3*
             fl26 + 4.E+0*x1*P_q*k3_k4*k5_q*pow(k5_l2,2)*Ak3*fl26 - 4.E+0*
             x1*P_q*k3_k5*k4_k5*k5_l2*q_l2*Ak3*fl26 - 4.E+0*x1*P_q*k3_k5*
             k4_q*pow(k5_l2,2)*Ak3*fl26 + 4.E+0*x1*P_q*k3_l2*k4_k5*k5_q*
             k5_l2*Ak3*fl26 - 4.E+0*x1*P_q*k3_l2*k4_q*k5_k5*k5_l2*Ak3*fl26
              );

          Chi(0,9) +=  + F * (  - 4.E+0*x1*P_k4*k3_k5*pow(k5_l2,2)*Ak3*
             fl26 - 4.E+0*x1*P_k4*k3_l2*k5_k5*k5_l2*Ak3*fl26 + 4.E+0*x1*
             P_k5*k3_k4*pow(k5_l2,2)*Ak3*fl26 + 4.E+0*x1*P_k5*k3_l2*k4_k5*
             k5_l2*Ak3*fl26 + 4.E+0*x1*P_l2*k3_k4*k5_k5*k5_l2*Ak3*fl26 -
             4.E+0*x1*P_l2*k3_k5*k4_k5*k5_l2*Ak3*fl26 );

       Chi(0,10) =
           + H * ( 8.E+0*x1*P_k3*k4_k5*k5_q*l2_l2*Ak3*fl27 - 8.E+0*x1*P_k3*
             k4_k5*k5_l2*q_l2*Ak3*fl27 - 8.E+0*x1*P_k3*k4_q*k5_k5*l2_l2*Ak3
             *fl27 + 8.E+0*x1*P_k3*k4_q*pow(k5_l2,2)*Ak3*fl27 + 8.E+0*x1*
             P_k3*k4_l2*k5_k5*q_l2*Ak3*fl27 - 8.E+0*x1*P_k3*k4_l2*k5_q*
             k5_l2*Ak3*fl27 - 1.6E+1*x1*P_k4*k3_k5*k5_q*l2_l2*Ak3*fl27 +
             8.E+0*x1*P_k4*k3_k5*k5_l2*q_l2*Ak3*fl27 + 8.E+0*x1*P_k4*k3_q*
             k5_k5*l2_l2*Ak3*fl27 - 8.E+0*x1*P_k4*k3_q*pow(k5_l2,2)*Ak3*
             fl27 - 1.6E+1*x1*P_k4*k3_l2*k5_k5*q_l2*Ak3*fl27 + 8.E+0*x1*
             P_k4*k3_l2*k5_q*k5_l2*Ak3*fl27 + 8.E+0*x1*P_k5*k3_k4*k5_l2*
             q_l2*Ak3*fl27 + 1.6E+1*x1*P_k5*k3_k5*k4_q*l2_l2*Ak3*fl27 -
             1.6E+1*x1*P_k5*k3_k5*k4_l2*q_l2*Ak3*fl27 - 8.E+0*x1*P_k5*k3_q*
             k4_k5*l2_l2*Ak3*fl27 + 8.E+0*x1*P_k5*k3_q*k4_l2*k5_l2*Ak3*fl27
              + 1.6E+1*x1*P_k5*k3_l2*k4_k5*q_l2*Ak3*fl27 - 8.E+0*x1*P_k5*
             k3_l2*k4_q*k5_l2*Ak3*fl27 - 8.E+0*x1*P_l2*k3_k4*k5_q*k5_l2*Ak3
             *fl27 - 8.E+0*x1*P_l2*k3_k5*k4_q*k5_l2*Ak3*fl27 + 1.6E+1*x1*
             P_l2*k3_k5*k4_l2*k5_q*Ak3*fl27 + 8.E+0*x1*P_l2*k3_q*k4_k5*
             k5_l2*Ak3*fl27 - 8.E+0*x1*P_l2*k3_q*k4_l2*k5_k5*Ak3*fl27 -
             1.6E+1*x1*P_l2*k3_l2*k4_k5*k5_q*Ak3*fl27 + 1.6E+1*x1*P_l2*
             k3_l2*k4_q*k5_k5*Ak3*fl27 );

          Chi(0,10) +=  + G * ( 4.E+0*x1*P_q*k4_k5*k5_q*l2_l2*Bk3*fl27 -
             4.E+0*x1*P_q*k4_k5*k5_l2*q_l2*Bk3*fl27 - 4.E+0*x1*P_q*k4_q*
             k5_k5*l2_l2*Bk3*fl27 - 4.E+0*x1*P_q*k4_q*pow(k5_l2,2)*Bk3*fl27
              + 4.E+0*x1*P_q*k4_l2*k5_k5*q_l2*Bk3*fl27 + 4.E+0*x1*P_q*k4_l2
             *k5_q*k5_l2*Bk3*fl27 );

          Chi(0,10) +=  + F * (  - 4.E+0*x1*P_k4*k5_k5*l2_l2*Bk3*fl27 -
             4.E+0*x1*P_k4*pow(k5_l2,2)*Bk3*fl27 + 4.E+0*x1*P_k5*k4_k5*
             l2_l2*Bk3*fl27 + 4.E+0*x1*P_k5*k4_l2*k5_l2*Bk3*fl27 - 4.E+0*x1
             *P_l2*k4_k5*k5_l2*Bk3*fl27 + 4.E+0*x1*P_l2*k4_l2*k5_k5*Bk3*
             fl27 );

          Chi(0,10) +=  + E * (  - 4.E+0*x1*k3_k4*k5_k5*l2_l2*Ak3*fl27 -
             4.E+0*x1*k3_k4*pow(k5_l2,2)*Ak3*fl27 + 4.E+0*x1*k3_k5*k4_k5*
             l2_l2*Ak3*fl27 + 4.E+0*x1*k3_k5*k4_l2*k5_l2*Ak3*fl27 - 4.E+0*
             x1*k3_l2*k4_k5*k5_l2*Ak3*fl27 + 4.E+0*x1*k3_l2*k4_l2*k5_k5*Ak3
             *fl27 );

       Chi(1,0) =
           + H * ( 1.6E+1*x2*y1*P_k4*k5_q*l1_l1*Bk3 - 1.6E+1*x2*y1*P_k4*
             k5_l1*q_l1*Bk3 - 1.6E+1*x2*y1*P_k5*k4_q*l1_l1*Bk3 + 1.6E+1*x2*
             y1*P_k5*k4_l1*q_l1*Bk3 + 1.6E+1*x2*y1*P_l1*k4_q*k5_l1*Bk3 -
             1.6E+1*x2*y1*P_l1*k4_l1*k5_q*Bk3 );

          Chi(1,0) +=  + G * ( 8.E+0*x2*y1*P_q*k3_k4*k5_q*l1_l1*Ak3 -
             8.E+0*x2*y1*P_q*k3_k4*k5_l1*q_l1*Ak3 - 8.E+0*x2*y1*P_q*k3_k5*
             k4_q*l1_l1*Ak3 + 8.E+0*x2*y1*P_q*k3_k5*k4_l1*q_l1*Ak3 + 8.E+0*
             x2*y1*P_q*k3_l1*k4_q*k5_l1*Ak3 - 8.E+0*x2*y1*P_q*k3_l1*k4_l1*
             k5_q*Ak3 );

          Chi(1,0) +=  + F * (  - 8.E+0*x2*y1*P_k4*k3_k5*l1_l1*Ak3 +
             8.E+0*x2*y1*P_k4*k3_l1*k5_l1*Ak3 + 8.E+0*x2*y1*P_k5*k3_k4*
             l1_l1*Ak3 - 8.E+0*x2*y1*P_k5*k3_l1*k4_l1*Ak3 - 8.E+0*x2*y1*
             P_l1*k3_k4*k5_l1*Ak3 + 8.E+0*x2*y1*P_l1*k3_k5*k4_l1*Ak3 );

       Chi(1,1) =
           + H * ( 3.2E+1*y1*y2*P_k4*k5_q*l1_l1*l2_l2*Bk3 - 3.2E+1*y1*y2*
             P_k4*k5_q*pow(l1_l2,2)*Bk3 - 3.2E+1*y1*y2*P_k4*k5_l1*q_l1*
             l2_l2*Bk3 + 3.2E+1*y1*y2*P_k4*k5_l1*q_l2*l1_l2*Bk3 + 3.2E+1*y1
             *y2*P_k4*k5_l2*q_l1*l1_l2*Bk3 - 3.2E+1*y1*y2*P_k4*k5_l2*q_l2*
             l1_l1*Bk3 - 3.2E+1*y1*y2*P_k5*k4_q*l1_l1*l2_l2*Bk3 + 3.2E+1*y1
             *y2*P_k5*k4_q*pow(l1_l2,2)*Bk3 + 3.2E+1*y1*y2*P_k5*k4_l1*q_l1*
             l2_l2*Bk3 - 3.2E+1*y1*y2*P_k5*k4_l1*q_l2*l1_l2*Bk3 - 3.2E+1*y1
             *y2*P_k5*k4_l2*q_l1*l1_l2*Bk3 + 3.2E+1*y1*y2*P_k5*k4_l2*q_l2*
             l1_l1*Bk3 + 3.2E+1*y1*y2*P_l1*k4_q*k5_l1*l2_l2*Bk3 - 3.2E+1*y1
             *y2*P_l1*k4_q*k5_l2*l1_l2*Bk3 - 3.2E+1*y1*y2*P_l1*k4_l1*k5_q*
             l2_l2*Bk3 + 3.2E+1*y1*y2*P_l1*k4_l1*k5_l2*q_l2*Bk3 + 3.2E+1*y1
             *y2*P_l1*k4_l2*k5_q*l1_l2*Bk3 - 3.2E+1*y1*y2*P_l1*k4_l2*k5_l1*
             q_l2*Bk3 - 3.2E+1*y1*y2*P_l2*k4_q*k5_l1*l1_l2*Bk3 + 3.2E+1*y1*
             y2*P_l2*k4_q*k5_l2*l1_l1*Bk3 + 3.2E+1*y1*y2*P_l2*k4_l1*k5_q*
             l1_l2*Bk3 - 3.2E+1*y1*y2*P_l2*k4_l1*k5_l2*q_l1*Bk3 - 3.2E+1*y1
             *y2*P_l2*k4_l2*k5_q*l1_l1*Bk3 + 3.2E+1*y1*y2*P_l2*k4_l2*k5_l1*
             q_l1*Bk3 );

          Chi(1,1) +=  + G * ( 1.6E+1*y1*y2*P_q*k3_k4*k5_q*l1_l1*l2_l2*
             Ak3 - 1.6E+1*y1*y2*P_q*k3_k4*k5_q*pow(l1_l2,2)*Ak3 - 1.6E+1*y1
             *y2*P_q*k3_k4*k5_l1*q_l1*l2_l2*Ak3 + 1.6E+1*y1*y2*P_q*k3_k4*
             k5_l1*q_l2*l1_l2*Ak3 + 1.6E+1*y1*y2*P_q*k3_k4*k5_l2*q_l1*l1_l2
             *Ak3 - 1.6E+1*y1*y2*P_q*k3_k4*k5_l2*q_l2*l1_l1*Ak3 - 1.6E+1*y1
             *y2*P_q*k3_k5*k4_q*l1_l1*l2_l2*Ak3 + 1.6E+1*y1*y2*P_q*k3_k5*
             k4_q*pow(l1_l2,2)*Ak3 + 1.6E+1*y1*y2*P_q*k3_k5*k4_l1*q_l1*
             l2_l2*Ak3 - 1.6E+1*y1*y2*P_q*k3_k5*k4_l1*q_l2*l1_l2*Ak3 -
             1.6E+1*y1*y2*P_q*k3_k5*k4_l2*q_l1*l1_l2*Ak3 + 1.6E+1*y1*y2*P_q
             *k3_k5*k4_l2*q_l2*l1_l1*Ak3 + 1.6E+1*y1*y2*P_q*k3_l1*k4_q*
             k5_l1*l2_l2*Ak3 - 1.6E+1*y1*y2*P_q*k3_l1*k4_q*k5_l2*l1_l2*Ak3
              - 1.6E+1*y1*y2*P_q*k3_l1*k4_l1*k5_q*l2_l2*Ak3 + 1.6E+1*y1*y2*
             P_q*k3_l1*k4_l1*k5_l2*q_l2*Ak3 + 1.6E+1*y1*y2*P_q*k3_l1*k4_l2*
             k5_q*l1_l2*Ak3 - 1.6E+1*y1*y2*P_q*k3_l1*k4_l2*k5_l1*q_l2*Ak3
              - 1.6E+1*y1*y2*P_q*k3_l2*k4_q*k5_l1*l1_l2*Ak3 + 1.6E+1*y1*y2*
             P_q*k3_l2*k4_q*k5_l2*l1_l1*Ak3 + 1.6E+1*y1*y2*P_q*k3_l2*k4_l1*
             k5_q*l1_l2*Ak3 - 1.6E+1*y1*y2*P_q*k3_l2*k4_l1*k5_l2*q_l1*Ak3
              - 1.6E+1*y1*y2*P_q*k3_l2*k4_l2*k5_q*l1_l1*Ak3 + 1.6E+1*y1*y2*
             P_q*k3_l2*k4_l2*k5_l1*q_l1*Ak3 );

          Chi(1,1) +=  + F * (  - 1.6E+1*y1*y2*P_k4*k3_k5*l1_l1*l2_l2*Ak3
              + 1.6E+1*y1*y2*P_k4*k3_k5*pow(l1_l2,2)*Ak3 + 1.6E+1*y1*y2*
             P_k4*k3_l1*k5_l1*l2_l2*Ak3 - 1.6E+1*y1*y2*P_k4*k3_l1*k5_l2*
             l1_l2*Ak3 - 1.6E+1*y1*y2*P_k4*k3_l2*k5_l1*l1_l2*Ak3 + 1.6E+1*
             y1*y2*P_k4*k3_l2*k5_l2*l1_l1*Ak3 + 1.6E+1*y1*y2*P_k5*k3_k4*
             l1_l1*l2_l2*Ak3 - 1.6E+1*y1*y2*P_k5*k3_k4*pow(l1_l2,2)*Ak3 -
             1.6E+1*y1*y2*P_k5*k3_l1*k4_l1*l2_l2*Ak3 + 1.6E+1*y1*y2*P_k5*
             k3_l1*k4_l2*l1_l2*Ak3 + 1.6E+1*y1*y2*P_k5*k3_l2*k4_l1*l1_l2*
             Ak3 - 1.6E+1*y1*y2*P_k5*k3_l2*k4_l2*l1_l1*Ak3 - 1.6E+1*y1*y2*
             P_l1*k3_k4*k5_l1*l2_l2*Ak3 + 1.6E+1*y1*y2*P_l1*k3_k4*k5_l2*
             l1_l2*Ak3 + 1.6E+1*y1*y2*P_l1*k3_k5*k4_l1*l2_l2*Ak3 - 1.6E+1*
             y1*y2*P_l1*k3_k5*k4_l2*l1_l2*Ak3 - 1.6E+1*y1*y2*P_l1*k3_l2*
             k4_l1*k5_l2*Ak3 + 1.6E+1*y1*y2*P_l1*k3_l2*k4_l2*k5_l1*Ak3 +
             1.6E+1*y1*y2*P_l2*k3_k4*k5_l1*l1_l2*Ak3 - 1.6E+1*y1*y2*P_l2*
             k3_k4*k5_l2*l1_l1*Ak3 - 1.6E+1*y1*y2*P_l2*k3_k5*k4_l1*l1_l2*
             Ak3 + 1.6E+1*y1*y2*P_l2*k3_k5*k4_l2*l1_l1*Ak3 + 1.6E+1*y1*y2*
             P_l2*k3_l1*k4_l1*k5_l2*Ak3 - 1.6E+1*y1*y2*P_l2*k3_l1*k4_l2*
             k5_l1*Ak3 );

       Chi(1,2) =
           + H * (  - 3.2E+1*y1*z2*P_k4*k3_k5*q_l1*l1_l2*Ak3 + 3.2E+1*y1*z2
             *P_k4*k3_k5*q_l2*l1_l1*Ak3 + 3.2E+1*y1*z2*P_k4*k3_l1*k5_q*
             l1_l2*Ak3 - 3.2E+1*y1*z2*P_k4*k3_l1*k5_l1*q_l2*Ak3 - 3.2E+1*y1
             *z2*P_k4*k3_l2*k5_q*l1_l1*Ak3 + 3.2E+1*y1*z2*P_k4*k3_l2*k5_l1*
             q_l1*Ak3 + 3.2E+1*y1*z2*P_k5*k3_k4*q_l1*l1_l2*Ak3 - 3.2E+1*y1*
             z2*P_k5*k3_k4*q_l2*l1_l1*Ak3 - 3.2E+1*y1*z2*P_k5*k3_l1*k4_q*
             l1_l2*Ak3 + 3.2E+1*y1*z2*P_k5*k3_l1*k4_l1*q_l2*Ak3 + 3.2E+1*y1
             *z2*P_k5*k3_l2*k4_q*l1_l1*Ak3 - 3.2E+1*y1*z2*P_k5*k3_l2*k4_l1*
             q_l1*Ak3 - 3.2E+1*y1*z2*P_l1*k3_k4*k5_q*l1_l2*Ak3 + 3.2E+1*y1*
             z2*P_l1*k3_k4*k5_l1*q_l2*Ak3 + 3.2E+1*y1*z2*P_l1*k3_k5*k4_q*
             l1_l2*Ak3 - 3.2E+1*y1*z2*P_l1*k3_k5*k4_l1*q_l2*Ak3 - 3.2E+1*y1
             *z2*P_l1*k3_l2*k4_q*k5_l1*Ak3 + 3.2E+1*y1*z2*P_l1*k3_l2*k4_l1*
             k5_q*Ak3 + 3.2E+1*y1*z2*P_l2*k3_k4*k5_q*l1_l1*Ak3 - 3.2E+1*y1*
             z2*P_l2*k3_k4*k5_l1*q_l1*Ak3 - 3.2E+1*y1*z2*P_l2*k3_k5*k4_q*
             l1_l1*Ak3 + 3.2E+1*y1*z2*P_l2*k3_k5*k4_l1*q_l1*Ak3 + 3.2E+1*y1
             *z2*P_l2*k3_l1*k4_q*k5_l1*Ak3 - 3.2E+1*y1*z2*P_l2*k3_l1*k4_l1*
             k5_q*Ak3 );

       Chi(1,3) =
           + H * ( 1.6E+1*y1*P_k4*k5_k5*k5_q*l1_l1*Bk3*fl20 - 1.6E+1*y1*
             P_k4*k5_k5*k5_l1*q_l1*Bk3*fl20 - 1.6E+1*y1*P_k5*k4_q*k5_k5*
             l1_l1*Bk3*fl20 + 1.6E+1*y1*P_k5*k4_l1*k5_k5*q_l1*Bk3*fl20 +
             1.6E+1*y1*P_l1*k4_q*k5_k5*k5_l1*Bk3*fl20 - 1.6E+1*y1*P_l1*
             k4_l1*k5_k5*k5_q*Bk3*fl20 );

          Chi(1,3) +=  + G * ( 8.E+0*y1*P_q*k3_k4*k5_k5*k5_q*l1_l1*Ak3*
             fl20 - 8.E+0*y1*P_q*k3_k4*k5_k5*k5_l1*q_l1*Ak3*fl20 - 8.E+0*y1
             *P_q*k3_k5*k4_q*k5_k5*l1_l1*Ak3*fl20 + 8.E+0*y1*P_q*k3_k5*
             k4_l1*k5_k5*q_l1*Ak3*fl20 + 8.E+0*y1*P_q*k3_l1*k4_q*k5_k5*
             k5_l1*Ak3*fl20 - 8.E+0*y1*P_q*k3_l1*k4_l1*k5_k5*k5_q*Ak3*fl20
              );

          Chi(1,3) +=  + F * (  - 8.E+0*y1*P_k4*k3_k5*k5_k5*l1_l1*Ak3*
             fl20 + 8.E+0*y1*P_k4*k3_l1*k5_k5*k5_l1*Ak3*fl20 + 8.E+0*y1*
             P_k5*k3_k4*k5_k5*l1_l1*Ak3*fl20 - 8.E+0*y1*P_k5*k3_l1*k4_l1*
             k5_k5*Ak3*fl20 - 8.E+0*y1*P_l1*k3_k4*k5_k5*k5_l1*Ak3*fl20 +
             8.E+0*y1*P_l1*k3_k5*k4_l1*k5_k5*Ak3*fl20 );

       Chi(1,4) =
           + H * (  - 1.6E+1*y1*P_k3*k4_q*k5_k5*k5_l1*k5_l2*l1_l2*Ak3*fl21
              + 1.6E+1*y1*P_k3*k4_q*k5_k5*pow(k5_l2,2)*l1_l1*Ak3*fl21 +
             1.6E+1*y1*P_k3*k4_l1*k5_k5*k5_q*k5_l2*l1_l2*Ak3*fl21 - 1.6E+1*
             y1*P_k3*k4_l1*k5_k5*pow(k5_l2,2)*q_l1*Ak3*fl21 - 1.6E+1*y1*
             P_k3*k4_l2*k5_k5*k5_q*k5_l2*l1_l1*Ak3*fl21 + 1.6E+1*y1*P_k3*
             k4_l2*k5_k5*k5_l1*k5_l2*q_l1*Ak3*fl21 - 1.6E+1*y1*P_k4*k3_k5*
             k5_k5*k5_l2*q_l1*l1_l2*Ak3*fl21 + 1.6E+1*y1*P_k4*k3_q*k5_k5*
             k5_l1*k5_l2*l1_l2*Ak3*fl21 - 1.6E+1*y1*P_k4*k3_q*k5_k5*pow(
             k5_l2,2)*l1_l1*Ak3*fl21 - 1.6E+1*y1*P_k4*k3_l1*k5_k5*k5_q*
             k5_l2*l1_l2*Ak3*fl21 + 3.2E+1*y1*P_k4*k3_l1*k5_k5*pow(k5_l2,2)
             *q_l1*Ak3*fl21 + 1.6E+1*y1*P_k5*k3_k4*k5_k5*k5_l2*q_l1*l1_l2*
             Ak3*fl21 - 1.6E+1*y1*P_k5*k3_q*k4_l1*k5_k5*k5_l2*l1_l2*Ak3*
             fl21 + 1.6E+1*y1*P_k5*k3_q*k4_l2*k5_k5*k5_l2*l1_l1*Ak3*fl21 +
             1.6E+1*y1*P_k5*k3_l1*k4_q*k5_k5*k5_l2*l1_l2*Ak3*fl21 - 3.2E+1*
             y1*P_k5*k3_l1*k4_l2*k5_k5*k5_l2*q_l1*Ak3*fl21 - 1.6E+1*y1*P_l1
             *k3_k4*k5_k5*k5_q*k5_l2*l1_l2*Ak3*fl21 + 1.6E+1*y1*P_l1*k3_k5*
             k4_q*k5_k5*k5_l2*l1_l2*Ak3*fl21 + 1.6E+1*y1*P_l1*k3_q*k4_l1*
             k5_k5*pow(k5_l2,2)*Ak3*fl21 - 1.6E+1*y1*P_l1*k3_q*k4_l2*k5_k5*
             k5_l1*k5_l2*Ak3*fl21 - 3.2E+1*y1*P_l1*k3_l1*k4_q*k5_k5*pow(
             k5_l2,2)*Ak3*fl21 + 3.2E+1*y1*P_l1*k3_l1*k4_l2*k5_k5*k5_q*
             k5_l2*Ak3*fl21 );

          Chi(1,4) +=  + G * ( 8.E+0*y1*P_q*k4_q*k5_k5*k5_l1*k5_l2*l1_l2*
             Bk3*fl21 - 8.E+0*y1*P_q*k4_q*k5_k5*pow(k5_l2,2)*l1_l1*Bk3*fl21
              - 8.E+0*y1*P_q*k4_l1*k5_k5*k5_q*k5_l2*l1_l2*Bk3*fl21 + 8.E+0*
             y1*P_q*k4_l1*k5_k5*pow(k5_l2,2)*q_l1*Bk3*fl21 + 8.E+0*y1*P_q*
             k4_l2*k5_k5*k5_q*k5_l2*l1_l1*Bk3*fl21 - 8.E+0*y1*P_q*k4_l2*
             k5_k5*k5_l1*k5_l2*q_l1*Bk3*fl21 );

          Chi(1,4) +=  + F * ( 8.E+0*y1*P_k4*k5_k5*k5_l1*k5_l2*l1_l2*Bk3*
             fl21 - 8.E+0*y1*P_k4*k5_k5*pow(k5_l2,2)*l1_l1*Bk3*fl21 - 8.E+0
             *y1*P_k5*k4_l1*k5_k5*k5_l2*l1_l2*Bk3*fl21 + 8.E+0*y1*P_k5*
             k4_l2*k5_k5*k5_l2*l1_l1*Bk3*fl21 + 8.E+0*y1*P_l1*k4_l1*k5_k5*
             pow(k5_l2,2)*Bk3*fl21 - 8.E+0*y1*P_l1*k4_l2*k5_k5*k5_l1*k5_l2*
             Bk3*fl21 );

          Chi(1,4) +=  + E * ( 8.E+0*y1*k3_k4*k5_k5*k5_l1*k5_l2*l1_l2*Ak3
             *fl21 - 8.E+0*y1*k3_k4*k5_k5*pow(k5_l2,2)*l1_l1*Ak3*fl21 -
             8.E+0*y1*k3_k5*k4_l1*k5_k5*k5_l2*l1_l2*Ak3*fl21 + 8.E+0*y1*
             k3_k5*k4_l2*k5_k5*k5_l2*l1_l1*Ak3*fl21 + 8.E+0*y1*k3_l1*k4_l1*
             k5_k5*pow(k5_l2,2)*Ak3*fl21 - 8.E+0*y1*k3_l1*k4_l2*k5_k5*k5_l1
             *k5_l2*Ak3*fl21 );

       Chi(1,5) =
           + H * (  - 1.6E+1*y1*P_k3*k4_k5*k5_q*l1_l1*Ak3*fl22 + 1.6E+1*y1*
             P_k3*k4_k5*k5_l1*q_l1*Ak3*fl22 + 1.6E+1*y1*P_k3*k4_q*k5_k5*
             l1_l1*Ak3*fl22 - 1.6E+1*y1*P_k3*k4_q*pow(k5_l1,2)*Ak3*fl22 -
             1.6E+1*y1*P_k3*k4_l1*k5_k5*q_l1*Ak3*fl22 + 1.6E+1*y1*P_k3*
             k4_l1*k5_q*k5_l1*Ak3*fl22 - 1.6E+1*y1*P_k4*k3_k5*k5_l1*q_l1*
             Ak3*fl22 - 1.6E+1*y1*P_k4*k3_q*k5_k5*l1_l1*Ak3*fl22 + 1.6E+1*
             y1*P_k4*k3_q*pow(k5_l1,2)*Ak3*fl22 + 3.2E+1*y1*P_k4*k3_l1*
             k5_k5*q_l1*Ak3*fl22 - 1.6E+1*y1*P_k4*k3_l1*k5_q*k5_l1*Ak3*fl22
              + 1.6E+1*y1*P_k5*k3_k4*k5_l1*q_l1*Ak3*fl22 + 1.6E+1*y1*P_k5*
             k3_q*k4_k5*l1_l1*Ak3*fl22 - 1.6E+1*y1*P_k5*k3_q*k4_l1*k5_l1*
             Ak3*fl22 - 3.2E+1*y1*P_k5*k3_l1*k4_k5*q_l1*Ak3*fl22 + 1.6E+1*
             y1*P_k5*k3_l1*k4_q*k5_l1*Ak3*fl22 - 1.6E+1*y1*P_l1*k3_k4*k5_q*
             k5_l1*Ak3*fl22 + 1.6E+1*y1*P_l1*k3_k5*k4_q*k5_l1*Ak3*fl22 -
             1.6E+1*y1*P_l1*k3_q*k4_k5*k5_l1*Ak3*fl22 + 1.6E+1*y1*P_l1*k3_q
             *k4_l1*k5_k5*Ak3*fl22 + 3.2E+1*y1*P_l1*k3_l1*k4_k5*k5_q*Ak3*
             fl22 - 3.2E+1*y1*P_l1*k3_l1*k4_q*k5_k5*Ak3*fl22 );

          Chi(1,5) +=  + G * ( 8.E+0*y1*P_q*k4_k5*k5_q*l1_l1*Bk3*fl22 -
             8.E+0*y1*P_q*k4_k5*k5_l1*q_l1*Bk3*fl22 - 8.E+0*y1*P_q*k4_q*
             k5_k5*l1_l1*Bk3*fl22 + 8.E+0*y1*P_q*k4_q*pow(k5_l1,2)*Bk3*fl22
              + 8.E+0*y1*P_q*k4_l1*k5_k5*q_l1*Bk3*fl22 - 8.E+0*y1*P_q*k4_l1
             *k5_q*k5_l1*Bk3*fl22 );

          Chi(1,5) +=  + F * (  - 8.E+0*y1*P_k4*k5_k5*l1_l1*Bk3*fl22 +
             8.E+0*y1*P_k4*pow(k5_l1,2)*Bk3*fl22 + 8.E+0*y1*P_k5*k4_k5*
             l1_l1*Bk3*fl22 - 8.E+0*y1*P_k5*k4_l1*k5_l1*Bk3*fl22 - 8.E+0*y1
             *P_l1*k4_k5*k5_l1*Bk3*fl22 + 8.E+0*y1*P_l1*k4_l1*k5_k5*Bk3*
             fl22 );

          Chi(1,5) +=  + E * (  - 8.E+0*y1*k3_k4*k5_k5*l1_l1*Ak3*fl22 +
             8.E+0*y1*k3_k4*pow(k5_l1,2)*Ak3*fl22 + 8.E+0*y1*k3_k5*k4_k5*
             l1_l1*Ak3*fl22 - 8.E+0*y1*k3_k5*k4_l1*k5_l1*Ak3*fl22 - 8.E+0*
             y1*k3_l1*k4_k5*k5_l1*Ak3*fl22 + 8.E+0*y1*k3_l1*k4_l1*k5_k5*Ak3
             *fl22 );

       Chi(1,6) =
           + H * (  - 1.6E+1*y1*P_k4*k5_k5*q_l1*l1_l2*Bk3*fl23 + 1.6E+1*y1*
             P_k4*k5_l1*k5_l2*q_l1*Bk3*fl23 + 1.6E+1*y1*P_k5*k4_k5*q_l1*
             l1_l2*Bk3*fl23 - 1.6E+1*y1*P_k5*k4_l2*k5_l1*q_l1*Bk3*fl23 -
             1.6E+1*y1*P_l1*k4_k5*k5_q*l1_l2*Bk3*fl23 + 1.6E+1*y1*P_l1*k4_q
             *k5_k5*l1_l2*Bk3*fl23 - 1.6E+1*y1*P_l1*k4_q*k5_l1*k5_l2*Bk3*
             fl23 + 1.6E+1*y1*P_l1*k4_l2*k5_q*k5_l1*Bk3*fl23 );

          Chi(1,6) +=  + G * (  - 8.E+0*y1*P_q*k3_k4*k5_k5*q_l1*l1_l2*Ak3
             *fl23 + 8.E+0*y1*P_q*k3_k4*k5_l1*k5_l2*q_l1*Ak3*fl23 + 8.E+0*
             y1*P_q*k3_k5*k4_k5*q_l1*l1_l2*Ak3*fl23 - 8.E+0*y1*P_q*k3_k5*
             k4_l2*k5_l1*q_l1*Ak3*fl23 - 8.E+0*y1*P_q*k3_q*k4_k5*k5_l1*
             l1_l2*Ak3*fl23 + 8.E+0*y1*P_q*k3_q*k4_k5*k5_l2*l1_l1*Ak3*fl23
              + 8.E+0*y1*P_q*k3_q*k4_l1*k5_k5*l1_l2*Ak3*fl23 - 8.E+0*y1*P_q
             *k3_q*k4_l1*k5_l1*k5_l2*Ak3*fl23 - 8.E+0*y1*P_q*k3_q*k4_l2*
             k5_k5*l1_l1*Ak3*fl23 + 8.E+0*y1*P_q*k3_q*k4_l2*pow(k5_l1,2)*
             Ak3*fl23 + 8.E+0*y1*P_q*k3_l1*k4_k5*k5_q*l1_l2*Ak3*fl23 -
             1.6E+1*y1*P_q*k3_l1*k4_k5*k5_l2*q_l1*Ak3*fl23 - 8.E+0*y1*P_q*
             k3_l1*k4_q*k5_k5*l1_l2*Ak3*fl23 + 8.E+0*y1*P_q*k3_l1*k4_q*
             k5_l1*k5_l2*Ak3*fl23 + 1.6E+1*y1*P_q*k3_l1*k4_l2*k5_k5*q_l1*
             Ak3*fl23 - 8.E+0*y1*P_q*k3_l1*k4_l2*k5_q*k5_l1*Ak3*fl23 );

          Chi(1,6) +=  + F * (  - 8.E+0*y1*P_k3*k4_k5*k5_l1*l1_l2*Ak3*
             fl23 + 8.E+0*y1*P_k3*k4_k5*k5_l2*l1_l1*Ak3*fl23 + 8.E+0*y1*
             P_k3*k4_l1*k5_k5*l1_l2*Ak3*fl23 - 8.E+0*y1*P_k3*k4_l1*k5_l1*
             k5_l2*Ak3*fl23 - 8.E+0*y1*P_k3*k4_l2*k5_k5*l1_l1*Ak3*fl23 +
             8.E+0*y1*P_k3*k4_l2*pow(k5_l1,2)*Ak3*fl23 - 8.E+0*y1*P_k4*
             k3_l1*k5_k5*l1_l2*Ak3*fl23 + 8.E+0*y1*P_k4*k3_l1*k5_l1*k5_l2*
             Ak3*fl23 + 8.E+0*y1*P_k5*k3_l1*k4_k5*l1_l2*Ak3*fl23 - 8.E+0*y1
             *P_k5*k3_l1*k4_l2*k5_l1*Ak3*fl23 - 8.E+0*y1*P_l1*k3_k4*k5_k5*
             l1_l2*Ak3*fl23 + 8.E+0*y1*P_l1*k3_k4*k5_l1*k5_l2*Ak3*fl23 +
             8.E+0*y1*P_l1*k3_k5*k4_k5*l1_l2*Ak3*fl23 - 8.E+0*y1*P_l1*k3_k5
             *k4_l2*k5_l1*Ak3*fl23 - 1.6E+1*y1*P_l1*k3_l1*k4_k5*k5_l2*Ak3*
             fl23 + 1.6E+1*y1*P_l1*k3_l1*k4_l2*k5_k5*Ak3*fl23 );

          Chi(1,6) +=  + E * ( 8.E+0*y1*k4_k5*k5_l1*l1_l2*Bk3*fl23 -
             8.E+0*y1*k4_k5*k5_l2*l1_l1*Bk3*fl23 - 8.E+0*y1*k4_l1*k5_k5*
             l1_l2*Bk3*fl23 + 8.E+0*y1*k4_l1*k5_l1*k5_l2*Bk3*fl23 + 8.E+0*
             y1*k4_l2*k5_k5*l1_l1*Bk3*fl23 - 8.E+0*y1*k4_l2*pow(k5_l1,2)*
             Bk3*fl23 );

       Chi(1,7) =
           + H * ( 1.6E+1*y1*P_k4*k3_k5*k5_k5*q_l1*l1_l2*Ak3*fl24 - 1.6E+1*
             y1*P_k4*k3_k5*k5_k5*q_l2*l1_l1*Ak3*fl24 - 1.6E+1*y1*P_k4*k3_l1
             *k5_k5*k5_q*l1_l2*Ak3*fl24 + 1.6E+1*y1*P_k4*k3_l1*k5_k5*k5_l1*
             q_l2*Ak3*fl24 + 1.6E+1*y1*P_k4*k3_l2*k5_k5*k5_q*l1_l1*Ak3*fl24
              - 1.6E+1*y1*P_k4*k3_l2*k5_k5*k5_l1*q_l1*Ak3*fl24 - 1.6E+1*y1*
             P_k5*k3_k4*k5_k5*q_l1*l1_l2*Ak3*fl24 + 1.6E+1*y1*P_k5*k3_k4*
             k5_k5*q_l2*l1_l1*Ak3*fl24 + 1.6E+1*y1*P_k5*k3_l1*k4_q*k5_k5*
             l1_l2*Ak3*fl24 - 1.6E+1*y1*P_k5*k3_l1*k4_l1*k5_k5*q_l2*Ak3*
             fl24 - 1.6E+1*y1*P_k5*k3_l2*k4_q*k5_k5*l1_l1*Ak3*fl24 + 1.6E+1
             *y1*P_k5*k3_l2*k4_l1*k5_k5*q_l1*Ak3*fl24 + 1.6E+1*y1*P_l1*
             k3_k4*k5_k5*k5_q*l1_l2*Ak3*fl24 - 1.6E+1*y1*P_l1*k3_k4*k5_k5*
             k5_l1*q_l2*Ak3*fl24 - 1.6E+1*y1*P_l1*k3_k5*k4_q*k5_k5*l1_l2*
             Ak3*fl24 + 1.6E+1*y1*P_l1*k3_k5*k4_l1*k5_k5*q_l2*Ak3*fl24 +
             1.6E+1*y1*P_l1*k3_l2*k4_q*k5_k5*k5_l1*Ak3*fl24 - 1.6E+1*y1*
             P_l1*k3_l2*k4_l1*k5_k5*k5_q*Ak3*fl24 - 1.6E+1*y1*P_l2*k3_k4*
             k5_k5*k5_q*l1_l1*Ak3*fl24 + 1.6E+1*y1*P_l2*k3_k4*k5_k5*k5_l1*
             q_l1*Ak3*fl24 + 1.6E+1*y1*P_l2*k3_k5*k4_q*k5_k5*l1_l1*Ak3*fl24
              - 1.6E+1*y1*P_l2*k3_k5*k4_l1*k5_k5*q_l1*Ak3*fl24 - 1.6E+1*y1*
             P_l2*k3_l1*k4_q*k5_k5*k5_l1*Ak3*fl24 + 1.6E+1*y1*P_l2*k3_l1*
             k4_l1*k5_k5*k5_q*Ak3*fl24 );

       Chi(1,8) =
           + H * ( 1.6E+1*y1*P_k4*k5_k5*k5_q*l1_l1*l2_l2*Bk3*fl25 - 1.6E+1*
             y1*P_k4*k5_k5*k5_q*pow(l1_l2,2)*Bk3*fl25 - 1.6E+1*y1*P_k4*
             k5_k5*k5_l1*q_l1*l2_l2*Bk3*fl25 + 1.6E+1*y1*P_k4*k5_k5*k5_l1*
             q_l2*l1_l2*Bk3*fl25 + 1.6E+1*y1*P_k4*k5_k5*k5_l2*q_l1*l1_l2*
             Bk3*fl25 - 1.6E+1*y1*P_k4*k5_k5*k5_l2*q_l2*l1_l1*Bk3*fl25 -
             1.6E+1*y1*P_k5*k4_q*k5_k5*l1_l1*l2_l2*Bk3*fl25 + 1.6E+1*y1*
             P_k5*k4_q*k5_k5*pow(l1_l2,2)*Bk3*fl25 + 1.6E+1*y1*P_k5*k4_l1*
             k5_k5*q_l1*l2_l2*Bk3*fl25 - 1.6E+1*y1*P_k5*k4_l1*k5_k5*q_l2*
             l1_l2*Bk3*fl25 - 1.6E+1*y1*P_k5*k4_l2*k5_k5*q_l1*l1_l2*Bk3*
             fl25 + 1.6E+1*y1*P_k5*k4_l2*k5_k5*q_l2*l1_l1*Bk3*fl25 + 1.6E+1
             *y1*P_l1*k4_q*k5_k5*k5_l1*l2_l2*Bk3*fl25 - 1.6E+1*y1*P_l1*k4_q
             *k5_k5*k5_l2*l1_l2*Bk3*fl25 - 1.6E+1*y1*P_l1*k4_l1*k5_k5*k5_q*
             l2_l2*Bk3*fl25 + 1.6E+1*y1*P_l1*k4_l1*k5_k5*k5_l2*q_l2*Bk3*
             fl25 + 1.6E+1*y1*P_l1*k4_l2*k5_k5*k5_q*l1_l2*Bk3*fl25 - 1.6E+1
             *y1*P_l1*k4_l2*k5_k5*k5_l1*q_l2*Bk3*fl25 - 1.6E+1*y1*P_l2*k4_q
             *k5_k5*k5_l1*l1_l2*Bk3*fl25 + 1.6E+1*y1*P_l2*k4_q*k5_k5*k5_l2*
             l1_l1*Bk3*fl25 + 1.6E+1*y1*P_l2*k4_l1*k5_k5*k5_q*l1_l2*Bk3*
             fl25 - 1.6E+1*y1*P_l2*k4_l1*k5_k5*k5_l2*q_l1*Bk3*fl25 - 1.6E+1
             *y1*P_l2*k4_l2*k5_k5*k5_q*l1_l1*Bk3*fl25 + 1.6E+1*y1*P_l2*
             k4_l2*k5_k5*k5_l1*q_l1*Bk3*fl25 );

          Chi(1,8) +=  + G * ( 8.E+0*y1*P_q*k3_k4*k5_k5*k5_q*l1_l1*l2_l2*
             Ak3*fl25 - 8.E+0*y1*P_q*k3_k4*k5_k5*k5_q*pow(l1_l2,2)*Ak3*fl25
              - 8.E+0*y1*P_q*k3_k4*k5_k5*k5_l1*q_l1*l2_l2*Ak3*fl25 + 8.E+0*
             y1*P_q*k3_k4*k5_k5*k5_l1*q_l2*l1_l2*Ak3*fl25 + 8.E+0*y1*P_q*
             k3_k4*k5_k5*k5_l2*q_l1*l1_l2*Ak3*fl25 - 8.E+0*y1*P_q*k3_k4*
             k5_k5*k5_l2*q_l2*l1_l1*Ak3*fl25 - 8.E+0*y1*P_q*k3_k5*k4_q*
             k5_k5*l1_l1*l2_l2*Ak3*fl25 + 8.E+0*y1*P_q*k3_k5*k4_q*k5_k5*
             pow(l1_l2,2)*Ak3*fl25 + 8.E+0*y1*P_q*k3_k5*k4_l1*k5_k5*q_l1*
             l2_l2*Ak3*fl25 - 8.E+0*y1*P_q*k3_k5*k4_l1*k5_k5*q_l2*l1_l2*Ak3
             *fl25 - 8.E+0*y1*P_q*k3_k5*k4_l2*k5_k5*q_l1*l1_l2*Ak3*fl25 +
             8.E+0*y1*P_q*k3_k5*k4_l2*k5_k5*q_l2*l1_l1*Ak3*fl25 + 8.E+0*y1*
             P_q*k3_l1*k4_q*k5_k5*k5_l1*l2_l2*Ak3*fl25 - 8.E+0*y1*P_q*k3_l1
             *k4_q*k5_k5*k5_l2*l1_l2*Ak3*fl25 - 8.E+0*y1*P_q*k3_l1*k4_l1*
             k5_k5*k5_q*l2_l2*Ak3*fl25 + 8.E+0*y1*P_q*k3_l1*k4_l1*k5_k5*
             k5_l2*q_l2*Ak3*fl25 + 8.E+0*y1*P_q*k3_l1*k4_l2*k5_k5*k5_q*
             l1_l2*Ak3*fl25 - 8.E+0*y1*P_q*k3_l1*k4_l2*k5_k5*k5_l1*q_l2*Ak3
             *fl25 - 8.E+0*y1*P_q*k3_l2*k4_q*k5_k5*k5_l1*l1_l2*Ak3*fl25 +
             8.E+0*y1*P_q*k3_l2*k4_q*k5_k5*k5_l2*l1_l1*Ak3*fl25 + 8.E+0*y1*
             P_q*k3_l2*k4_l1*k5_k5*k5_q*l1_l2*Ak3*fl25 - 8.E+0*y1*P_q*k3_l2
             *k4_l1*k5_k5*k5_l2*q_l1*Ak3*fl25 - 8.E+0*y1*P_q*k3_l2*k4_l2*
             k5_k5*k5_q*l1_l1*Ak3*fl25 + 8.E+0*y1*P_q*k3_l2*k4_l2*k5_k5*
             k5_l1*q_l1*Ak3*fl25 );

          Chi(1,8) +=  + F * (  - 8.E+0*y1*P_k4*k3_k5*k5_k5*l1_l1*l2_l2*
             Ak3*fl25 + 8.E+0*y1*P_k4*k3_k5*k5_k5*pow(l1_l2,2)*Ak3*fl25 +
             8.E+0*y1*P_k4*k3_l1*k5_k5*k5_l1*l2_l2*Ak3*fl25 - 8.E+0*y1*P_k4
             *k3_l1*k5_k5*k5_l2*l1_l2*Ak3*fl25 - 8.E+0*y1*P_k4*k3_l2*k5_k5*
             k5_l1*l1_l2*Ak3*fl25 + 8.E+0*y1*P_k4*k3_l2*k5_k5*k5_l2*l1_l1*
             Ak3*fl25 + 8.E+0*y1*P_k5*k3_k4*k5_k5*l1_l1*l2_l2*Ak3*fl25 -
             8.E+0*y1*P_k5*k3_k4*k5_k5*pow(l1_l2,2)*Ak3*fl25 - 8.E+0*y1*
             P_k5*k3_l1*k4_l1*k5_k5*l2_l2*Ak3*fl25 + 8.E+0*y1*P_k5*k3_l1*
             k4_l2*k5_k5*l1_l2*Ak3*fl25 + 8.E+0*y1*P_k5*k3_l2*k4_l1*k5_k5*
             l1_l2*Ak3*fl25 - 8.E+0*y1*P_k5*k3_l2*k4_l2*k5_k5*l1_l1*Ak3*
             fl25 - 8.E+0*y1*P_l1*k3_k4*k5_k5*k5_l1*l2_l2*Ak3*fl25 + 8.E+0*
             y1*P_l1*k3_k4*k5_k5*k5_l2*l1_l2*Ak3*fl25 + 8.E+0*y1*P_l1*k3_k5
             *k4_l1*k5_k5*l2_l2*Ak3*fl25 - 8.E+0*y1*P_l1*k3_k5*k4_l2*k5_k5*
             l1_l2*Ak3*fl25 - 8.E+0*y1*P_l1*k3_l2*k4_l1*k5_k5*k5_l2*Ak3*
             fl25 + 8.E+0*y1*P_l1*k3_l2*k4_l2*k5_k5*k5_l1*Ak3*fl25 + 8.E+0*
             y1*P_l2*k3_k4*k5_k5*k5_l1*l1_l2*Ak3*fl25 - 8.E+0*y1*P_l2*k3_k4
             *k5_k5*k5_l2*l1_l1*Ak3*fl25 - 8.E+0*y1*P_l2*k3_k5*k4_l1*k5_k5*
             l1_l2*Ak3*fl25 + 8.E+0*y1*P_l2*k3_k5*k4_l2*k5_k5*l1_l1*Ak3*
             fl25 + 8.E+0*y1*P_l2*k3_l1*k4_l1*k5_k5*k5_l2*Ak3*fl25 - 8.E+0*
             y1*P_l2*k3_l1*k4_l2*k5_k5*k5_l1*Ak3*fl25 );

       Chi(1,9) =
           + H * (  - 1.6E+1*y1*P_k4*k5_k5*k5_l2*q_l1*l1_l2*Bk3*fl26 +
             1.6E+1*y1*P_k4*k5_k5*k5_l2*q_l2*l1_l1*Bk3*fl26 + 1.6E+1*y1*
             P_k4*k5_q*k5_l1*k5_l2*l1_l2*Bk3*fl26 - 1.6E+1*y1*P_k4*pow(
             k5_l1,2)*k5_l2*q_l2*Bk3*fl26 + 1.6E+1*y1*P_k5*k4_k5*k5_l2*q_l1
             *l1_l2*Bk3*fl26 - 1.6E+1*y1*P_k5*k4_k5*k5_l2*q_l2*l1_l1*Bk3*
             fl26 - 1.6E+1*y1*P_k5*k4_q*k5_l1*k5_l2*l1_l2*Bk3*fl26 + 1.6E+1
             *y1*P_k5*k4_l1*k5_l1*k5_l2*q_l2*Bk3*fl26 - 1.6E+1*y1*P_l1*
             k4_k5*k5_q*k5_l2*l1_l2*Bk3*fl26 + 1.6E+1*y1*P_l1*k4_k5*k5_l1*
             k5_l2*q_l2*Bk3*fl26 + 1.6E+1*y1*P_l1*k4_q*k5_k5*k5_l2*l1_l2*
             Bk3*fl26 - 1.6E+1*y1*P_l1*k4_l1*k5_k5*k5_l2*q_l2*Bk3*fl26 +
             1.6E+1*y1*P_l2*k4_k5*k5_q*k5_l2*l1_l1*Bk3*fl26 - 1.6E+1*y1*
             P_l2*k4_k5*k5_l1*k5_l2*q_l1*Bk3*fl26 - 1.6E+1*y1*P_l2*k4_q*
             k5_k5*k5_l2*l1_l1*Bk3*fl26 + 1.6E+1*y1*P_l2*k4_q*pow(k5_l1,2)*
             k5_l2*Bk3*fl26 + 1.6E+1*y1*P_l2*k4_l1*k5_k5*k5_l2*q_l1*Bk3*
             fl26 - 1.6E+1*y1*P_l2*k4_l1*k5_q*k5_l1*k5_l2*Bk3*fl26 );

          Chi(1,9) +=  + G * (  - 8.E+0*y1*P_q*k3_k4*k5_k5*k5_l2*q_l1*
             l1_l2*Ak3*fl26 + 8.E+0*y1*P_q*k3_k4*k5_k5*k5_l2*q_l2*l1_l1*Ak3
             *fl26 + 8.E+0*y1*P_q*k3_k4*k5_q*k5_l1*k5_l2*l1_l2*Ak3*fl26 -
             8.E+0*y1*P_q*k3_k4*pow(k5_l1,2)*k5_l2*q_l2*Ak3*fl26 + 8.E+0*y1
             *P_q*k3_k5*k4_k5*k5_l2*q_l1*l1_l2*Ak3*fl26 - 8.E+0*y1*P_q*
             k3_k5*k4_k5*k5_l2*q_l2*l1_l1*Ak3*fl26 - 8.E+0*y1*P_q*k3_k5*
             k4_q*k5_l1*k5_l2*l1_l2*Ak3*fl26 + 8.E+0*y1*P_q*k3_k5*k4_l1*
             k5_l1*k5_l2*q_l2*Ak3*fl26 - 8.E+0*y1*P_q*k3_l1*k4_k5*k5_q*
             k5_l2*l1_l2*Ak3*fl26 + 8.E+0*y1*P_q*k3_l1*k4_k5*k5_l1*k5_l2*
             q_l2*Ak3*fl26 + 8.E+0*y1*P_q*k3_l1*k4_q*k5_k5*k5_l2*l1_l2*Ak3*
             fl26 - 8.E+0*y1*P_q*k3_l1*k4_l1*k5_k5*k5_l2*q_l2*Ak3*fl26 +
             8.E+0*y1*P_q*k3_l2*k4_k5*k5_q*k5_l2*l1_l1*Ak3*fl26 - 8.E+0*y1*
             P_q*k3_l2*k4_k5*k5_l1*k5_l2*q_l1*Ak3*fl26 - 8.E+0*y1*P_q*k3_l2
             *k4_q*k5_k5*k5_l2*l1_l1*Ak3*fl26 + 8.E+0*y1*P_q*k3_l2*k4_q*
             pow(k5_l1,2)*k5_l2*Ak3*fl26 + 8.E+0*y1*P_q*k3_l2*k4_l1*k5_k5*
             k5_l2*q_l1*Ak3*fl26 - 8.E+0*y1*P_q*k3_l2*k4_l1*k5_q*k5_l1*
             k5_l2*Ak3*fl26 );

          Chi(1,9) +=  + F * (  - 8.E+0*y1*P_k4*k3_k5*k5_l1*k5_l2*l1_l2*
             Ak3*fl26 + 8.E+0*y1*P_k4*k3_l1*k5_k5*k5_l2*l1_l2*Ak3*fl26 -
             8.E+0*y1*P_k4*k3_l2*k5_k5*k5_l2*l1_l1*Ak3*fl26 + 8.E+0*y1*P_k4
             *k3_l2*pow(k5_l1,2)*k5_l2*Ak3*fl26 + 8.E+0*y1*P_k5*k3_k4*k5_l1
             *k5_l2*l1_l2*Ak3*fl26 - 8.E+0*y1*P_k5*k3_l1*k4_k5*k5_l2*l1_l2*
             Ak3*fl26 + 8.E+0*y1*P_k5*k3_l2*k4_k5*k5_l2*l1_l1*Ak3*fl26 -
             8.E+0*y1*P_k5*k3_l2*k4_l1*k5_l1*k5_l2*Ak3*fl26 - 8.E+0*y1*P_l1
             *k3_k4*k5_k5*k5_l2*l1_l2*Ak3*fl26 + 8.E+0*y1*P_l1*k3_k5*k4_k5*
             k5_l2*l1_l2*Ak3*fl26 - 8.E+0*y1*P_l1*k3_l2*k4_k5*k5_l1*k5_l2*
             Ak3*fl26 + 8.E+0*y1*P_l1*k3_l2*k4_l1*k5_k5*k5_l2*Ak3*fl26 +
             8.E+0*y1*P_l2*k3_k4*k5_k5*k5_l2*l1_l1*Ak3*fl26 - 8.E+0*y1*P_l2
             *k3_k4*pow(k5_l1,2)*k5_l2*Ak3*fl26 - 8.E+0*y1*P_l2*k3_k5*k4_k5
             *k5_l2*l1_l1*Ak3*fl26 + 8.E+0*y1*P_l2*k3_k5*k4_l1*k5_l1*k5_l2*
             Ak3*fl26 + 8.E+0*y1*P_l2*k3_l1*k4_k5*k5_l1*k5_l2*Ak3*fl26 -
             8.E+0*y1*P_l2*k3_l1*k4_l1*k5_k5*k5_l2*Ak3*fl26 );

       Chi(1,10) =
           + H * (  - 1.6E+1*y1*P_k3*k4_q*k5_l1*k5_l2*l1_l2*Ak3*fl27 +
             1.6E+1*y1*P_k3*k4_q*pow(k5_l2,2)*l1_l1*Ak3*fl27 + 1.6E+1*y1*
             P_k3*k4_l1*k5_q*k5_l2*l1_l2*Ak3*fl27 - 1.6E+1*y1*P_k3*k4_l1*
             pow(k5_l2,2)*q_l1*Ak3*fl27 - 1.6E+1*y1*P_k3*k4_l2*k5_q*k5_l2*
             l1_l1*Ak3*fl27 + 1.6E+1*y1*P_k3*k4_l2*k5_l1*k5_l2*q_l1*Ak3*
             fl27 - 1.2E+1*y1*P_k4*k3_k5*k5_q*l1_l1*l2_l2*Ak3*fl27 + 1.2E+1
             *y1*P_k4*k3_k5*k5_q*pow(l1_l2,2)*Ak3*fl27 - 4.E+0*y1*P_k4*
             k3_k5*k5_l1*q_l1*l2_l2*Ak3*fl27 - 1.2E+1*y1*P_k4*k3_k5*k5_l1*
             q_l2*l1_l2*Ak3*fl27 - 1.2E+1*y1*P_k4*k3_k5*k5_l2*q_l1*l1_l2*
             Ak3*fl27 + 1.2E+1*y1*P_k4*k3_k5*k5_l2*q_l2*l1_l1*Ak3*fl27 -
             4.E+0*y1*P_k4*k3_q*k5_k5*l1_l1*l2_l2*Ak3*fl27 + 4.E+0*y1*P_k4*
             k3_q*k5_k5*pow(l1_l2,2)*Ak3*fl27 + 8.E+0*y1*P_k4*k3_q*k5_l1*
             k5_l2*l1_l2*Ak3*fl27 + 4.E+0*y1*P_k4*k3_q*pow(k5_l1,2)*l2_l2*
             Ak3*fl27 - 1.2E+1*y1*P_k4*k3_q*pow(k5_l2,2)*l1_l1*Ak3*fl27 +
             2.E+1*y1*P_k4*k3_l1*k5_k5*q_l1*l2_l2*Ak3*fl27 - 4.E+0*y1*P_k4*
             k3_l1*k5_k5*q_l2*l1_l2*Ak3*fl27 - 4.E+0*y1*P_k4*k3_l1*k5_q*
             k5_l1*l2_l2*Ak3*fl27 - 1.2E+1*y1*P_k4*k3_l1*k5_q*k5_l2*l1_l2*
             Ak3*fl27 + 4.E+0*y1*P_k4*k3_l1*k5_l1*k5_l2*q_l2*Ak3*fl27 +
             1.2E+1*y1*P_k4*k3_l1*pow(k5_l2,2)*q_l1*Ak3*fl27 - 4.E+0*y1*
             P_k4*k3_l2*k5_k5*q_l1*l1_l2*Ak3*fl27 - 1.2E+1*y1*P_k4*k3_l2*
             k5_k5*q_l2*l1_l1*Ak3*fl27 - 1.2E+1*y1*P_k4*k3_l2*k5_q*k5_l1*
             l1_l2*Ak3*fl27 + 1.2E+1*y1*P_k4*k3_l2*k5_q*k5_l2*l1_l1*Ak3*
             fl27 + 4.E+0*y1*P_k4*k3_l2*k5_l1*k5_l2*q_l1*Ak3*fl27 + 1.2E+1*
             y1*P_k4*k3_l2*pow(k5_l1,2)*q_l2*Ak3*fl27 - 4.E+0*y1*P_k5*k3_k4
             *k5_q*l1_l1*l2_l2*Ak3*fl27 + 4.E+0*y1*P_k5*k3_k4*k5_q*pow(
             l1_l2,2)*Ak3*fl27 + 2.E+1*y1*P_k5*k3_k4*k5_l1*q_l1*l2_l2*Ak3*
             fl27 - 4.E+0*y1*P_k5*k3_k4*k5_l1*q_l2*l1_l2*Ak3*fl27 - 4.E+0*
             y1*P_k5*k3_k4*k5_l2*q_l1*l1_l2*Ak3*fl27 + 4.E+0*y1*P_k5*k3_k4*
             k5_l2*q_l2*l1_l1*Ak3*fl27 + 1.6E+1*y1*P_k5*k3_k5*k4_q*l1_l1*
             l2_l2*Ak3*fl27 - 1.6E+1*y1*P_k5*k3_k5*k4_q*pow(l1_l2,2)*Ak3*
             fl27 - 1.6E+1*y1*P_k5*k3_k5*k4_l1*q_l1*l2_l2*Ak3*fl27 + 1.6E+1
             *y1*P_k5*k3_k5*k4_l1*q_l2*l1_l2*Ak3*fl27 + 1.6E+1*y1*P_k5*
             k3_k5*k4_l2*q_l1*l1_l2*Ak3*fl27 - 1.6E+1*y1*P_k5*k3_k5*k4_l2*
             q_l2*l1_l1*Ak3*fl27 + 4.E+0*y1*P_k5*k3_q*k4_k5*l1_l1*l2_l2*Ak3
             *fl27 - 4.E+0*y1*P_k5*k3_q*k4_k5*pow(l1_l2,2)*Ak3*fl27 - 4.E+0
             *y1*P_k5*k3_q*k4_l1*k5_l1*l2_l2*Ak3*fl27 - 1.2E+1*y1*P_k5*k3_q
             *k4_l1*k5_l2*l1_l2*Ak3*fl27 + 4.E+0*y1*P_k5*k3_q*k4_l2*k5_l1*
             l1_l2*Ak3*fl27 + 1.2E+1*y1*P_k5*k3_q*k4_l2*k5_l2*l1_l1*Ak3*
             fl27 - 2.E+1*y1*P_k5*k3_l1*k4_k5*q_l1*l2_l2*Ak3*fl27 + 4.E+0*
             y1*P_k5*k3_l1*k4_k5*q_l2*l1_l2*Ak3*fl27 + 1.6E+1*y1*P_k5*k3_l1
             *k4_q*k5_l2*l1_l2*Ak3*fl27 + 4.E+0*y1*P_k5*k3_l1*k4_l1*k5_q*
             l2_l2*Ak3*fl27 - 4.E+0*y1*P_k5*k3_l1*k4_l1*k5_l2*q_l2*Ak3*fl27
              - 4.E+0*y1*P_k5*k3_l1*k4_l2*k5_q*l1_l2*Ak3*fl27 - 1.2E+1*y1*
             P_k5*k3_l1*k4_l2*k5_l2*q_l1*Ak3*fl27 + 4.E+0*y1*P_k5*k3_l2*
             k4_k5*q_l1*l1_l2*Ak3*fl27 + 1.2E+1*y1*P_k5*k3_l2*k4_k5*q_l2*
             l1_l1*Ak3*fl27 + 1.6E+1*y1*P_k5*k3_l2*k4_q*k5_l1*l1_l2*Ak3*
             fl27 - 1.6E+1*y1*P_k5*k3_l2*k4_q*k5_l2*l1_l1*Ak3*fl27 - 4.E+0*
             y1*P_k5*k3_l2*k4_l1*k5_q*l1_l2*Ak3*fl27 - 1.2E+1*y1*P_k5*k3_l2
             *k4_l1*k5_l1*q_l2*Ak3*fl27 + 1.6E+1*y1*P_k5*k3_l2*k4_l1*k5_l2*
             q_l1*Ak3*fl27 + 4.E+0*y1*P_k5*k3_l2*k4_l2*k5_q*l1_l1*Ak3*fl27
              - 2.E+1*y1*P_k5*k3_l2*k4_l2*k5_l1*q_l1*Ak3*fl27 );

          Chi(1,10) +=  + H * ( 4.E+0*y1*P_q*k3_k4*k5_k5*l1_l1*l2_l2*Ak3*
             fl27 - 4.E+0*y1*P_q*k3_k4*k5_k5*pow(l1_l2,2)*Ak3*fl27 + 8.E+0*
             y1*P_q*k3_k4*k5_l1*k5_l2*l1_l2*Ak3*fl27 - 4.E+0*y1*P_q*k3_k4*
             pow(k5_l1,2)*l2_l2*Ak3*fl27 - 4.E+0*y1*P_q*k3_k4*pow(k5_l2,2)*
             l1_l1*Ak3*fl27 - 4.E+0*y1*P_q*k3_k5*k4_k5*l1_l1*l2_l2*Ak3*fl27
              + 4.E+0*y1*P_q*k3_k5*k4_k5*pow(l1_l2,2)*Ak3*fl27 + 4.E+0*y1*
             P_q*k3_k5*k4_l1*k5_l1*l2_l2*Ak3*fl27 - 4.E+0*y1*P_q*k3_k5*
             k4_l1*k5_l2*l1_l2*Ak3*fl27 - 4.E+0*y1*P_q*k3_k5*k4_l2*k5_l1*
             l1_l2*Ak3*fl27 + 4.E+0*y1*P_q*k3_k5*k4_l2*k5_l2*l1_l1*Ak3*fl27
              + 4.E+0*y1*P_q*k3_l1*k4_k5*k5_l1*l2_l2*Ak3*fl27 - 4.E+0*y1*
             P_q*k3_l1*k4_k5*k5_l2*l1_l2*Ak3*fl27 - 4.E+0*y1*P_q*k3_l1*
             k4_l1*k5_k5*l2_l2*Ak3*fl27 + 4.E+0*y1*P_q*k3_l1*k4_l1*pow(
             k5_l2,2)*Ak3*fl27 + 4.E+0*y1*P_q*k3_l1*k4_l2*k5_k5*l1_l2*Ak3*
             fl27 - 4.E+0*y1*P_q*k3_l1*k4_l2*k5_l1*k5_l2*Ak3*fl27 - 4.E+0*
             y1*P_q*k3_l2*k4_k5*k5_l1*l1_l2*Ak3*fl27 + 4.E+0*y1*P_q*k3_l2*
             k4_k5*k5_l2*l1_l1*Ak3*fl27 + 4.E+0*y1*P_q*k3_l2*k4_l1*k5_k5*
             l1_l2*Ak3*fl27 - 4.E+0*y1*P_q*k3_l2*k4_l1*k5_l1*k5_l2*Ak3*fl27
              - 4.E+0*y1*P_q*k3_l2*k4_l2*k5_k5*l1_l1*Ak3*fl27 + 4.E+0*y1*
             P_q*k3_l2*k4_l2*pow(k5_l1,2)*Ak3*fl27 - 4.E+0*y1*P_l1*k3_k4*
             k5_k5*q_l1*l2_l2*Ak3*fl27 - 1.2E+1*y1*P_l1*k3_k4*k5_k5*q_l2*
             l1_l2*Ak3*fl27 - 1.2E+1*y1*P_l1*k3_k4*k5_q*k5_l1*l2_l2*Ak3*
             fl27 - 4.E+0*y1*P_l1*k3_k4*k5_q*k5_l2*l1_l2*Ak3*fl27 + 1.2E+1*
             y1*P_l1*k3_k4*k5_l1*k5_l2*q_l2*Ak3*fl27 + 4.E+0*y1*P_l1*k3_k4*
             pow(k5_l2,2)*q_l1*Ak3*fl27 + 4.E+0*y1*P_l1*k3_k5*k4_k5*q_l1*
             l2_l2*Ak3*fl27 + 1.2E+1*y1*P_l1*k3_k5*k4_k5*q_l2*l1_l2*Ak3*
             fl27 + 1.6E+1*y1*P_l1*k3_k5*k4_q*k5_l2*l1_l2*Ak3*fl27 + 1.2E+1
             *y1*P_l1*k3_k5*k4_l1*k5_q*l2_l2*Ak3*fl27 - 1.2E+1*y1*P_l1*
             k3_k5*k4_l1*k5_l2*q_l2*Ak3*fl27 - 1.2E+1*y1*P_l1*k3_k5*k4_l2*
             k5_q*l1_l2*Ak3*fl27 - 4.E+0*y1*P_l1*k3_k5*k4_l2*k5_l2*q_l1*Ak3
             *fl27 - 4.E+0*y1*P_l1*k3_q*k4_k5*k5_l1*l2_l2*Ak3*fl27 + 4.E+0*
             y1*P_l1*k3_q*k4_k5*k5_l2*l1_l2*Ak3*fl27 + 4.E+0*y1*P_l1*k3_q*
             k4_l1*k5_k5*l2_l2*Ak3*fl27 + 1.2E+1*y1*P_l1*k3_q*k4_l1*pow(
             k5_l2,2)*Ak3*fl27 - 4.E+0*y1*P_l1*k3_q*k4_l2*k5_k5*l1_l2*Ak3*
             fl27 - 1.2E+1*y1*P_l1*k3_q*k4_l2*k5_l1*k5_l2*Ak3*fl27 + 1.6E+1
             *y1*P_l1*k3_l1*k4_k5*k5_q*l2_l2*Ak3*fl27 - 1.6E+1*y1*P_l1*
             k3_l1*k4_k5*k5_l2*q_l2*Ak3*fl27 - 1.6E+1*y1*P_l1*k3_l1*k4_q*
             k5_k5*l2_l2*Ak3*fl27 - 1.6E+1*y1*P_l1*k3_l1*k4_q*pow(k5_l2,2)*
             Ak3*fl27 + 1.6E+1*y1*P_l1*k3_l1*k4_l2*k5_k5*q_l2*Ak3*fl27 +
             1.6E+1*y1*P_l1*k3_l1*k4_l2*k5_q*k5_l2*Ak3*fl27 - 1.2E+1*y1*
             P_l1*k3_l2*k4_k5*k5_l1*q_l2*Ak3*fl27 - 4.E+0*y1*P_l1*k3_l2*
             k4_k5*k5_l2*q_l1*Ak3*fl27 + 1.2E+1*y1*P_l1*k3_l2*k4_l1*k5_k5*
             q_l2*Ak3*fl27 - 1.2E+1*y1*P_l1*k3_l2*k4_l1*k5_q*k5_l2*Ak3*fl27
              + 4.E+0*y1*P_l1*k3_l2*k4_l2*k5_k5*q_l1*Ak3*fl27 + 1.2E+1*y1*
             P_l1*k3_l2*k4_l2*k5_q*k5_l1*Ak3*fl27 + 2.E+1*y1*P_l2*k3_k4*
             k5_k5*q_l1*l1_l2*Ak3*fl27 - 4.E+0*y1*P_l2*k3_k4*k5_k5*q_l2*
             l1_l1*Ak3*fl27 - 4.E+0*y1*P_l2*k3_k4*k5_q*k5_l1*l1_l2*Ak3*fl27
              + 4.E+0*y1*P_l2*k3_k4*k5_q*k5_l2*l1_l1*Ak3*fl27 - 2.E+1*y1*
             P_l2*k3_k4*k5_l1*k5_l2*q_l1*Ak3*fl27 + 4.E+0*y1*P_l2*k3_k4*
             pow(k5_l1,2)*q_l2*Ak3*fl27 - 2.E+1*y1*P_l2*k3_k5*k4_k5*q_l1*
             l1_l2*Ak3*fl27 + 4.E+0*y1*P_l2*k3_k5*k4_k5*q_l2*l1_l1*Ak3*fl27
              + 1.6E+1*y1*P_l2*k3_k5*k4_q*k5_l1*l1_l2*Ak3*fl27 );

          Chi(1,10) +=  + H * (  - 1.6E+1*y1*P_l2*k3_k5*k4_q*k5_l2*l1_l1*
             Ak3*fl27 - 1.2E+1*y1*P_l2*k3_k5*k4_l1*k5_q*l1_l2*Ak3*fl27 -
             4.E+0*y1*P_l2*k3_k5*k4_l1*k5_l1*q_l2*Ak3*fl27 + 1.6E+1*y1*P_l2
             *k3_k5*k4_l1*k5_l2*q_l1*Ak3*fl27 + 1.2E+1*y1*P_l2*k3_k5*k4_l2*
             k5_q*l1_l1*Ak3*fl27 + 4.E+0*y1*P_l2*k3_k5*k4_l2*k5_l1*q_l1*Ak3
             *fl27 + 4.E+0*y1*P_l2*k3_q*k4_k5*k5_l1*l1_l2*Ak3*fl27 - 4.E+0*
             y1*P_l2*k3_q*k4_k5*k5_l2*l1_l1*Ak3*fl27 - 4.E+0*y1*P_l2*k3_q*
             k4_l1*k5_k5*l1_l2*Ak3*fl27 + 4.E+0*y1*P_l2*k3_q*k4_l1*k5_l1*
             k5_l2*Ak3*fl27 + 4.E+0*y1*P_l2*k3_q*k4_l2*k5_k5*l1_l1*Ak3*fl27
              - 4.E+0*y1*P_l2*k3_q*k4_l2*pow(k5_l1,2)*Ak3*fl27 - 4.E+0*y1*
             P_l2*k3_l1*k4_k5*k5_l1*q_l2*Ak3*fl27 + 2.E+1*y1*P_l2*k3_l1*
             k4_k5*k5_l2*q_l1*Ak3*fl27 + 4.E+0*y1*P_l2*k3_l1*k4_l1*k5_k5*
             q_l2*Ak3*fl27 - 4.E+0*y1*P_l2*k3_l1*k4_l1*k5_q*k5_l2*Ak3*fl27
              - 2.E+1*y1*P_l2*k3_l1*k4_l2*k5_k5*q_l1*Ak3*fl27 + 4.E+0*y1*
             P_l2*k3_l1*k4_l2*k5_q*k5_l1*Ak3*fl27 - 1.6E+1*y1*P_l2*k3_l2*
             k4_k5*k5_q*l1_l1*Ak3*fl27 + 1.6E+1*y1*P_l2*k3_l2*k4_k5*k5_l1*
             q_l1*Ak3*fl27 + 1.6E+1*y1*P_l2*k3_l2*k4_q*k5_k5*l1_l1*Ak3*fl27
              - 1.6E+1*y1*P_l2*k3_l2*k4_q*pow(k5_l1,2)*Ak3*fl27 - 1.6E+1*y1
             *P_l2*k3_l2*k4_l1*k5_k5*q_l1*Ak3*fl27 + 1.6E+1*y1*P_l2*k3_l2*
             k4_l1*k5_q*k5_l1*Ak3*fl27 );

          Chi(1,10) +=  + G * ( 8.E+0*y1*P_q*k4_k5*k5_q*l1_l1*l2_l2*Bk3*
             fl27 - 8.E+0*y1*P_q*k4_k5*k5_q*pow(l1_l2,2)*Bk3*fl27 - 8.E+0*
             y1*P_q*k4_k5*k5_l1*q_l1*l2_l2*Bk3*fl27 + 8.E+0*y1*P_q*k4_k5*
             k5_l1*q_l2*l1_l2*Bk3*fl27 + 8.E+0*y1*P_q*k4_k5*k5_l2*q_l1*
             l1_l2*Bk3*fl27 - 8.E+0*y1*P_q*k4_k5*k5_l2*q_l2*l1_l1*Bk3*fl27
              - 8.E+0*y1*P_q*k4_q*k5_k5*l1_l1*l2_l2*Bk3*fl27 + 8.E+0*y1*P_q
             *k4_q*k5_k5*pow(l1_l2,2)*Bk3*fl27 - 8.E+0*y1*P_q*k4_q*k5_l1*
             k5_l2*l1_l2*Bk3*fl27 + 8.E+0*y1*P_q*k4_q*pow(k5_l1,2)*l2_l2*
             Bk3*fl27 + 8.E+0*y1*P_q*k4_l1*k5_k5*q_l1*l2_l2*Bk3*fl27 -
             8.E+0*y1*P_q*k4_l1*k5_k5*q_l2*l1_l2*Bk3*fl27 - 8.E+0*y1*P_q*
             k4_l1*k5_q*k5_l1*l2_l2*Bk3*fl27 + 8.E+0*y1*P_q*k4_l1*k5_l1*
             k5_l2*q_l2*Bk3*fl27 - 8.E+0*y1*P_q*k4_l2*k5_k5*q_l1*l1_l2*Bk3*
             fl27 + 8.E+0*y1*P_q*k4_l2*k5_k5*q_l2*l1_l1*Bk3*fl27 + 8.E+0*y1
             *P_q*k4_l2*k5_q*k5_l1*l1_l2*Bk3*fl27 - 8.E+0*y1*P_q*k4_l2*pow(
             k5_l1,2)*q_l2*Bk3*fl27 );

          Chi(1,10) +=  + F * (  - 8.E+0*y1*P_k4*k5_k5*l1_l1*l2_l2*Bk3*
             fl27 + 8.E+0*y1*P_k4*k5_k5*pow(l1_l2,2)*Bk3*fl27 - 8.E+0*y1*
             P_k4*k5_l1*k5_l2*l1_l2*Bk3*fl27 + 8.E+0*y1*P_k4*pow(k5_l1,2)*
             l2_l2*Bk3*fl27 + 8.E+0*y1*P_k5*k4_k5*l1_l1*l2_l2*Bk3*fl27 -
             8.E+0*y1*P_k5*k4_k5*pow(l1_l2,2)*Bk3*fl27 - 8.E+0*y1*P_k5*
             k4_l1*k5_l1*l2_l2*Bk3*fl27 + 8.E+0*y1*P_k5*k4_l2*k5_l1*l1_l2*
             Bk3*fl27 - 8.E+0*y1*P_l1*k4_k5*k5_l1*l2_l2*Bk3*fl27 + 8.E+0*y1
             *P_l1*k4_k5*k5_l2*l1_l2*Bk3*fl27 + 8.E+0*y1*P_l1*k4_l1*k5_k5*
             l2_l2*Bk3*fl27 - 8.E+0*y1*P_l1*k4_l2*k5_k5*l1_l2*Bk3*fl27 +
             8.E+0*y1*P_l2*k4_k5*k5_l1*l1_l2*Bk3*fl27 - 8.E+0*y1*P_l2*k4_k5
             *k5_l2*l1_l1*Bk3*fl27 - 8.E+0*y1*P_l2*k4_l1*k5_k5*l1_l2*Bk3*
             fl27 + 8.E+0*y1*P_l2*k4_l1*k5_l1*k5_l2*Bk3*fl27 + 8.E+0*y1*
             P_l2*k4_l2*k5_k5*l1_l1*Bk3*fl27 - 8.E+0*y1*P_l2*k4_l2*pow(
             k5_l1,2)*Bk3*fl27 );

          Chi(1,10) +=  + E * (  - 8.E+0*y1*k3_k4*k5_k5*l1_l1*l2_l2*Ak3*
             fl27 + 8.E+0*y1*k3_k4*k5_k5*pow(l1_l2,2)*Ak3*fl27 - 8.E+0*y1*
             k3_k4*k5_l1*k5_l2*l1_l2*Ak3*fl27 + 8.E+0*y1*k3_k4*pow(k5_l1,2)
             *l2_l2*Ak3*fl27 + 8.E+0*y1*k3_k5*k4_k5*l1_l1*l2_l2*Ak3*fl27 -
             8.E+0*y1*k3_k5*k4_k5*pow(l1_l2,2)*Ak3*fl27 - 8.E+0*y1*k3_k5*
             k4_l1*k5_l1*l2_l2*Ak3*fl27 + 8.E+0*y1*k3_k5*k4_l2*k5_l1*l1_l2*
             Ak3*fl27 - 8.E+0*y1*k3_l1*k4_k5*k5_l1*l2_l2*Ak3*fl27 + 8.E+0*
             y1*k3_l1*k4_k5*k5_l2*l1_l2*Ak3*fl27 + 8.E+0*y1*k3_l1*k4_l1*
             k5_k5*l2_l2*Ak3*fl27 - 8.E+0*y1*k3_l1*k4_l2*k5_k5*l1_l2*Ak3*
             fl27 + 8.E+0*y1*k3_l2*k4_k5*k5_l1*l1_l2*Ak3*fl27 - 8.E+0*y1*
             k3_l2*k4_k5*k5_l2*l1_l1*Ak3*fl27 - 8.E+0*y1*k3_l2*k4_l1*k5_k5*
             l1_l2*Ak3*fl27 + 8.E+0*y1*k3_l2*k4_l1*k5_l1*k5_l2*Ak3*fl27 +
             8.E+0*y1*k3_l2*k4_l2*k5_k5*l1_l1*Ak3*fl27 - 8.E+0*y1*k3_l2*
             k4_l2*pow(k5_l1,2)*Ak3*fl27 );

       Chi(2,0) =
           + H * ( 1.6E+1*x2*z1*P_k4*k3_k5*q_l1*Ak3 - 1.6E+1*x2*z1*P_k4*
             k3_l1*k5_q*Ak3 - 1.6E+1*x2*z1*P_k5*k3_k4*q_l1*Ak3 + 1.6E+1*x2*
             z1*P_k5*k3_l1*k4_q*Ak3 + 1.6E+1*x2*z1*P_l1*k3_k4*k5_q*Ak3 -
             1.6E+1*x2*z1*P_l1*k3_k5*k4_q*Ak3 );

       Chi(2,1) =
           + H * ( 3.2E+1*y2*z1*P_k4*k3_k5*q_l1*l2_l2*Ak3 - 3.2E+1*y2*z1*
             P_k4*k3_k5*q_l2*l1_l2*Ak3 - 3.2E+1*y2*z1*P_k4*k3_l1*k5_q*l2_l2
             *Ak3 + 3.2E+1*y2*z1*P_k4*k3_l1*k5_l2*q_l2*Ak3 + 3.2E+1*y2*z1*
             P_k4*k3_l2*k5_q*l1_l2*Ak3 - 3.2E+1*y2*z1*P_k4*k3_l2*k5_l2*q_l1
             *Ak3 - 3.2E+1*y2*z1*P_k5*k3_k4*q_l1*l2_l2*Ak3 + 3.2E+1*y2*z1*
             P_k5*k3_k4*q_l2*l1_l2*Ak3 + 3.2E+1*y2*z1*P_k5*k3_l1*k4_q*l2_l2
             *Ak3 - 3.2E+1*y2*z1*P_k5*k3_l1*k4_l2*q_l2*Ak3 - 3.2E+1*y2*z1*
             P_k5*k3_l2*k4_q*l1_l2*Ak3 + 3.2E+1*y2*z1*P_k5*k3_l2*k4_l2*q_l1
             *Ak3 + 3.2E+1*y2*z1*P_l1*k3_k4*k5_q*l2_l2*Ak3 - 3.2E+1*y2*z1*
             P_l1*k3_k4*k5_l2*q_l2*Ak3 - 3.2E+1*y2*z1*P_l1*k3_k5*k4_q*l2_l2
             *Ak3 + 3.2E+1*y2*z1*P_l1*k3_k5*k4_l2*q_l2*Ak3 + 3.2E+1*y2*z1*
             P_l1*k3_l2*k4_q*k5_l2*Ak3 - 3.2E+1*y2*z1*P_l1*k3_l2*k4_l2*k5_q
             *Ak3 - 3.2E+1*y2*z1*P_l2*k3_k4*k5_q*l1_l2*Ak3 + 3.2E+1*y2*z1*
             P_l2*k3_k4*k5_l2*q_l1*Ak3 + 3.2E+1*y2*z1*P_l2*k3_k5*k4_q*l1_l2
             *Ak3 - 3.2E+1*y2*z1*P_l2*k3_k5*k4_l2*q_l1*Ak3 - 3.2E+1*y2*z1*
             P_l2*k3_l1*k4_q*k5_l2*Ak3 + 3.2E+1*y2*z1*P_l2*k3_l1*k4_l2*k5_q
             *Ak3 );

       Chi(2,2) = 0;

       Chi(2,3) =
           + H * ( 1.6E+1*z1*P_k4*k3_k5*k5_k5*q_l1*Ak3*fl20 - 1.6E+1*z1*
             P_k4*k3_l1*k5_k5*k5_q*Ak3*fl20 - 1.6E+1*z1*P_k5*k3_k4*k5_k5*
             q_l1*Ak3*fl20 + 1.6E+1*z1*P_k5*k3_l1*k4_q*k5_k5*Ak3*fl20 +
             1.6E+1*z1*P_l1*k3_k4*k5_k5*k5_q*Ak3*fl20 - 1.6E+1*z1*P_l1*
             k3_k5*k4_q*k5_k5*Ak3*fl20 );

       Chi(2,4) =
           + H * (  - 1.6E+1*z1*P_k4*k5_k5*k5_q*k5_l2*l1_l2*Bk3*fl21 +
             1.6E+1*z1*P_k4*k5_k5*pow(k5_l2,2)*q_l1*Bk3*fl21 + 1.6E+1*z1*
             P_k5*k4_q*k5_k5*k5_l2*l1_l2*Bk3*fl21 - 1.6E+1*z1*P_k5*k4_l2*
             k5_k5*k5_l2*q_l1*Bk3*fl21 - 1.6E+1*z1*P_l1*k4_q*k5_k5*pow(
             k5_l2,2)*Bk3*fl21 + 1.6E+1*z1*P_l1*k4_l2*k5_k5*k5_q*k5_l2*Bk3*
             fl21 );

          Chi(2,4) +=  + G * ( 8.E+0*z1*P_q*k3_k4*k5_k5*k5_q*k5_l2*l1_l2*
             Ak3*fl21 - 8.E+0*z1*P_q*k3_k4*k5_k5*pow(k5_l2,2)*q_l1*Ak3*fl21
              - 8.E+0*z1*P_q*k3_k5*k4_q*k5_k5*k5_l2*l1_l2*Ak3*fl21 + 8.E+0*
             z1*P_q*k3_k5*k4_l2*k5_k5*k5_l2*q_l1*Ak3*fl21 + 8.E+0*z1*P_q*
             k3_l1*k4_q*k5_k5*pow(k5_l2,2)*Ak3*fl21 - 8.E+0*z1*P_q*k3_l1*
             k4_l2*k5_k5*k5_q*k5_l2*Ak3*fl21 );

          Chi(2,4) +=  + F * (  - 8.E+0*z1*P_k4*k3_k5*k5_k5*k5_l2*l1_l2*
             Ak3*fl21 + 8.E+0*z1*P_k4*k3_l1*k5_k5*pow(k5_l2,2)*Ak3*fl21 +
             8.E+0*z1*P_k5*k3_k4*k5_k5*k5_l2*l1_l2*Ak3*fl21 - 8.E+0*z1*P_k5
             *k3_l1*k4_l2*k5_k5*k5_l2*Ak3*fl21 - 8.E+0*z1*P_l1*k3_k4*k5_k5*
             pow(k5_l2,2)*Ak3*fl21 + 8.E+0*z1*P_l1*k3_k5*k4_l2*k5_k5*k5_l2*
             Ak3*fl21 );

       Chi(2,5) =
           + H * ( 1.6E+1*z1*P_k4*k5_k5*q_l1*Bk3*fl22 - 1.6E+1*z1*P_k4*k5_q
             *k5_l1*Bk3*fl22 - 1.6E+1*z1*P_k5*k4_k5*q_l1*Bk3*fl22 + 1.6E+1*
             z1*P_k5*k4_q*k5_l1*Bk3*fl22 + 1.6E+1*z1*P_l1*k4_k5*k5_q*Bk3*
             fl22 - 1.6E+1*z1*P_l1*k4_q*k5_k5*Bk3*fl22 );

          Chi(2,5) +=  + G * (  - 8.E+0*z1*P_q*k3_k4*k5_k5*q_l1*Ak3*fl22
              + 8.E+0*z1*P_q*k3_k4*k5_q*k5_l1*Ak3*fl22 + 8.E+0*z1*P_q*k3_k5
             *k4_k5*q_l1*Ak3*fl22 - 8.E+0*z1*P_q*k3_k5*k4_q*k5_l1*Ak3*fl22
              - 8.E+0*z1*P_q*k3_l1*k4_k5*k5_q*Ak3*fl22 + 8.E+0*z1*P_q*k3_l1
             *k4_q*k5_k5*Ak3*fl22 );

          Chi(2,5) +=  + F * (  - 8.E+0*z1*P_k4*k3_k5*k5_l1*Ak3*fl22 +
             8.E+0*z1*P_k4*k3_l1*k5_k5*Ak3*fl22 + 8.E+0*z1*P_k5*k3_k4*k5_l1
             *Ak3*fl22 - 8.E+0*z1*P_k5*k3_l1*k4_k5*Ak3*fl22 - 8.E+0*z1*P_l1
             *k3_k4*k5_k5*Ak3*fl22 + 8.E+0*z1*P_l1*k3_k5*k4_k5*Ak3*fl22 );

       Chi(2,6) =
           + H * ( 1.6E+1*z1*P_k3*k4_k5*k5_q*l1_l2*Ak3*fl23 - 1.6E+1*z1*
             P_k3*k4_k5*k5_l2*q_l1*Ak3*fl23 - 1.6E+1*z1*P_k3*k4_q*k5_k5*
             l1_l2*Ak3*fl23 + 1.6E+1*z1*P_k3*k4_q*k5_l1*k5_l2*Ak3*fl23 +
             1.6E+1*z1*P_k3*k4_l2*k5_k5*q_l1*Ak3*fl23 - 1.6E+1*z1*P_k3*
             k4_l2*k5_q*k5_l1*Ak3*fl23 + 1.6E+1*z1*P_k4*k3_q*k5_k5*l1_l2*
             Ak3*fl23 - 1.6E+1*z1*P_k4*k3_q*k5_l1*k5_l2*Ak3*fl23 - 1.6E+1*
             z1*P_k5*k3_q*k4_k5*l1_l2*Ak3*fl23 + 1.6E+1*z1*P_k5*k3_q*k4_l2*
             k5_l1*Ak3*fl23 + 1.6E+1*z1*P_l1*k3_q*k4_k5*k5_l2*Ak3*fl23 -
             1.6E+1*z1*P_l1*k3_q*k4_l2*k5_k5*Ak3*fl23 );

          Chi(2,6) +=  + G * ( 8.E+0*z1*P_q*k4_k5*k5_q*l1_l2*Bk3*fl23 -
             8.E+0*z1*P_q*k4_k5*k5_l2*q_l1*Bk3*fl23 - 8.E+0*z1*P_q*k4_q*
             k5_k5*l1_l2*Bk3*fl23 + 8.E+0*z1*P_q*k4_q*k5_l1*k5_l2*Bk3*fl23
              + 8.E+0*z1*P_q*k4_l2*k5_k5*q_l1*Bk3*fl23 - 8.E+0*z1*P_q*k4_l2
             *k5_q*k5_l1*Bk3*fl23 );

          Chi(2,6) +=  + F * (  - 8.E+0*z1*P_k4*k5_k5*l1_l2*Bk3*fl23 +
             8.E+0*z1*P_k4*k5_l1*k5_l2*Bk3*fl23 + 8.E+0*z1*P_k5*k4_k5*l1_l2
             *Bk3*fl23 - 8.E+0*z1*P_k5*k4_l2*k5_l1*Bk3*fl23 - 8.E+0*z1*P_l1
             *k4_k5*k5_l2*Bk3*fl23 + 8.E+0*z1*P_l1*k4_l2*k5_k5*Bk3*fl23 );

          Chi(2,6) +=  + E * ( 8.E+0*z1*k3_k4*k5_k5*l1_l2*Ak3*fl23 -
             8.E+0*z1*k3_k4*k5_l1*k5_l2*Ak3*fl23 - 8.E+0*z1*k3_k5*k4_k5*
             l1_l2*Ak3*fl23 + 8.E+0*z1*k3_k5*k4_l2*k5_l1*Ak3*fl23 + 8.E+0*
             z1*k3_l1*k4_k5*k5_l2*Ak3*fl23 - 8.E+0*z1*k3_l1*k4_l2*k5_k5*Ak3
             *fl23 );

       Chi(2,7) = 0;

       Chi(2,8) =
           + H * ( 1.6E+1*z1*P_k4*k3_k5*k5_k5*q_l1*l2_l2*Ak3*fl25 - 1.6E+1*
             z1*P_k4*k3_k5*k5_k5*q_l2*l1_l2*Ak3*fl25 - 1.6E+1*z1*P_k4*k3_l1
             *k5_k5*k5_q*l2_l2*Ak3*fl25 + 1.6E+1*z1*P_k4*k3_l1*k5_k5*k5_l2*
             q_l2*Ak3*fl25 + 1.6E+1*z1*P_k4*k3_l2*k5_k5*k5_q*l1_l2*Ak3*fl25
              - 1.6E+1*z1*P_k4*k3_l2*k5_k5*k5_l2*q_l1*Ak3*fl25 - 1.6E+1*z1*
             P_k5*k3_k4*k5_k5*q_l1*l2_l2*Ak3*fl25 + 1.6E+1*z1*P_k5*k3_k4*
             k5_k5*q_l2*l1_l2*Ak3*fl25 + 1.6E+1*z1*P_k5*k3_l1*k4_q*k5_k5*
             l2_l2*Ak3*fl25 - 1.6E+1*z1*P_k5*k3_l1*k4_l2*k5_k5*q_l2*Ak3*
             fl25 - 1.6E+1*z1*P_k5*k3_l2*k4_q*k5_k5*l1_l2*Ak3*fl25 + 1.6E+1
             *z1*P_k5*k3_l2*k4_l2*k5_k5*q_l1*Ak3*fl25 + 1.6E+1*z1*P_l1*
             k3_k4*k5_k5*k5_q*l2_l2*Ak3*fl25 - 1.6E+1*z1*P_l1*k3_k4*k5_k5*
             k5_l2*q_l2*Ak3*fl25 - 1.6E+1*z1*P_l1*k3_k5*k4_q*k5_k5*l2_l2*
             Ak3*fl25 + 1.6E+1*z1*P_l1*k3_k5*k4_l2*k5_k5*q_l2*Ak3*fl25 +
             1.6E+1*z1*P_l1*k3_l2*k4_q*k5_k5*k5_l2*Ak3*fl25 - 1.6E+1*z1*
             P_l1*k3_l2*k4_l2*k5_k5*k5_q*Ak3*fl25 - 1.6E+1*z1*P_l2*k3_k4*
             k5_k5*k5_q*l1_l2*Ak3*fl25 + 1.6E+1*z1*P_l2*k3_k4*k5_k5*k5_l2*
             q_l1*Ak3*fl25 + 1.6E+1*z1*P_l2*k3_k5*k4_q*k5_k5*l1_l2*Ak3*fl25
              - 1.6E+1*z1*P_l2*k3_k5*k4_l2*k5_k5*q_l1*Ak3*fl25 - 1.6E+1*z1*
             P_l2*k3_l1*k4_q*k5_k5*k5_l2*Ak3*fl25 + 1.6E+1*z1*P_l2*k3_l1*
             k4_l2*k5_k5*k5_q*Ak3*fl25 );

       Chi(2,9) =
           + H * ( 1.6E+1*z1*P_k4*k3_k5*k5_l1*k5_l2*q_l2*Ak3*fl26 - 1.6E+1*
             z1*P_k4*k3_l1*k5_k5*k5_l2*q_l2*Ak3*fl26 + 1.6E+1*z1*P_k4*k3_l2
             *k5_k5*k5_l2*q_l1*Ak3*fl26 - 1.6E+1*z1*P_k4*k3_l2*k5_q*k5_l1*
             k5_l2*Ak3*fl26 - 1.6E+1*z1*P_k5*k3_k4*k5_l1*k5_l2*q_l2*Ak3*
             fl26 + 1.6E+1*z1*P_k5*k3_l1*k4_k5*k5_l2*q_l2*Ak3*fl26 - 1.6E+1
             *z1*P_k5*k3_l2*k4_k5*k5_l2*q_l1*Ak3*fl26 + 1.6E+1*z1*P_k5*
             k3_l2*k4_q*k5_l1*k5_l2*Ak3*fl26 + 1.6E+1*z1*P_l1*k3_k4*k5_k5*
             k5_l2*q_l2*Ak3*fl26 - 1.6E+1*z1*P_l1*k3_k5*k4_k5*k5_l2*q_l2*
             Ak3*fl26 + 1.6E+1*z1*P_l1*k3_l2*k4_k5*k5_q*k5_l2*Ak3*fl26 -
             1.6E+1*z1*P_l1*k3_l2*k4_q*k5_k5*k5_l2*Ak3*fl26 - 1.6E+1*z1*
             P_l2*k3_k4*k5_k5*k5_l2*q_l1*Ak3*fl26 + 1.6E+1*z1*P_l2*k3_k4*
             k5_q*k5_l1*k5_l2*Ak3*fl26 + 1.6E+1*z1*P_l2*k3_k5*k4_k5*k5_l2*
             q_l1*Ak3*fl26 - 1.6E+1*z1*P_l2*k3_k5*k4_q*k5_l1*k5_l2*Ak3*fl26
              - 1.6E+1*z1*P_l2*k3_l1*k4_k5*k5_q*k5_l2*Ak3*fl26 + 1.6E+1*z1*
             P_l2*k3_l1*k4_q*k5_k5*k5_l2*Ak3*fl26 );

       Chi(2,10) =
           + H * ( 1.6E+1*z1*P_k4*k5_k5*q_l1*l2_l2*Bk3*fl27 - 1.6E+1*z1*
             P_k4*k5_k5*q_l2*l1_l2*Bk3*fl27 - 1.6E+1*z1*P_k4*k5_q*k5_l1*
             l2_l2*Bk3*fl27 + 1.6E+1*z1*P_k4*k5_l1*k5_l2*q_l2*Bk3*fl27 -
             1.6E+1*z1*P_k5*k4_k5*q_l1*l2_l2*Bk3*fl27 + 1.6E+1*z1*P_k5*
             k4_k5*q_l2*l1_l2*Bk3*fl27 + 1.6E+1*z1*P_k5*k4_q*k5_l1*l2_l2*
             Bk3*fl27 - 1.6E+1*z1*P_k5*k4_l2*k5_l1*q_l2*Bk3*fl27 + 1.6E+1*
             z1*P_l1*k4_k5*k5_q*l2_l2*Bk3*fl27 - 1.6E+1*z1*P_l1*k4_k5*k5_l2
             *q_l2*Bk3*fl27 - 1.6E+1*z1*P_l1*k4_q*k5_k5*l2_l2*Bk3*fl27 +
             1.6E+1*z1*P_l1*k4_l2*k5_k5*q_l2*Bk3*fl27 - 1.6E+1*z1*P_l2*
             k4_k5*k5_q*l1_l2*Bk3*fl27 + 1.6E+1*z1*P_l2*k4_k5*k5_l2*q_l1*
             Bk3*fl27 + 1.6E+1*z1*P_l2*k4_q*k5_k5*l1_l2*Bk3*fl27 - 1.6E+1*
             z1*P_l2*k4_q*k5_l1*k5_l2*Bk3*fl27 - 1.6E+1*z1*P_l2*k4_l2*k5_k5
             *q_l1*Bk3*fl27 + 1.6E+1*z1*P_l2*k4_l2*k5_q*k5_l1*Bk3*fl27 );

          Chi(2,10) +=  + G * (  - 8.E+0*z1*P_q*k3_k4*k5_k5*q_l1*l2_l2*
             Ak3*fl27 + 8.E+0*z1*P_q*k3_k4*k5_k5*q_l2*l1_l2*Ak3*fl27 +
             8.E+0*z1*P_q*k3_k4*k5_q*k5_l1*l2_l2*Ak3*fl27 - 8.E+0*z1*P_q*
             k3_k4*k5_l1*k5_l2*q_l2*Ak3*fl27 + 8.E+0*z1*P_q*k3_k5*k4_k5*
             q_l1*l2_l2*Ak3*fl27 - 8.E+0*z1*P_q*k3_k5*k4_k5*q_l2*l1_l2*Ak3*
             fl27 - 8.E+0*z1*P_q*k3_k5*k4_q*k5_l1*l2_l2*Ak3*fl27 + 8.E+0*z1
             *P_q*k3_k5*k4_l2*k5_l1*q_l2*Ak3*fl27 - 8.E+0*z1*P_q*k3_l1*
             k4_k5*k5_q*l2_l2*Ak3*fl27 + 8.E+0*z1*P_q*k3_l1*k4_k5*k5_l2*
             q_l2*Ak3*fl27 + 8.E+0*z1*P_q*k3_l1*k4_q*k5_k5*l2_l2*Ak3*fl27
              - 8.E+0*z1*P_q*k3_l1*k4_l2*k5_k5*q_l2*Ak3*fl27 + 8.E+0*z1*P_q
             *k3_l2*k4_k5*k5_q*l1_l2*Ak3*fl27 - 8.E+0*z1*P_q*k3_l2*k4_k5*
             k5_l2*q_l1*Ak3*fl27 - 8.E+0*z1*P_q*k3_l2*k4_q*k5_k5*l1_l2*Ak3*
             fl27 + 8.E+0*z1*P_q*k3_l2*k4_q*k5_l1*k5_l2*Ak3*fl27 + 8.E+0*z1
             *P_q*k3_l2*k4_l2*k5_k5*q_l1*Ak3*fl27 - 8.E+0*z1*P_q*k3_l2*
             k4_l2*k5_q*k5_l1*Ak3*fl27 );

          Chi(2,10) +=  + F * (  - 8.E+0*z1*P_k4*k3_k5*k5_l1*l2_l2*Ak3*
             fl27 + 8.E+0*z1*P_k4*k3_l1*k5_k5*l2_l2*Ak3*fl27 - 8.E+0*z1*
             P_k4*k3_l2*k5_k5*l1_l2*Ak3*fl27 + 8.E+0*z1*P_k4*k3_l2*k5_l1*
             k5_l2*Ak3*fl27 + 8.E+0*z1*P_k5*k3_k4*k5_l1*l2_l2*Ak3*fl27 -
             8.E+0*z1*P_k5*k3_l1*k4_k5*l2_l2*Ak3*fl27 + 8.E+0*z1*P_k5*k3_l2
             *k4_k5*l1_l2*Ak3*fl27 - 8.E+0*z1*P_k5*k3_l2*k4_l2*k5_l1*Ak3*
             fl27 - 8.E+0*z1*P_l1*k3_k4*k5_k5*l2_l2*Ak3*fl27 + 8.E+0*z1*
             P_l1*k3_k5*k4_k5*l2_l2*Ak3*fl27 - 8.E+0*z1*P_l1*k3_l2*k4_k5*
             k5_l2*Ak3*fl27 + 8.E+0*z1*P_l1*k3_l2*k4_l2*k5_k5*Ak3*fl27 +
             8.E+0*z1*P_l2*k3_k4*k5_k5*l1_l2*Ak3*fl27 - 8.E+0*z1*P_l2*k3_k4
             *k5_l1*k5_l2*Ak3*fl27 - 8.E+0*z1*P_l2*k3_k5*k4_k5*l1_l2*Ak3*
             fl27 + 8.E+0*z1*P_l2*k3_k5*k4_l2*k5_l1*Ak3*fl27 + 8.E+0*z1*
             P_l2*k3_l1*k4_k5*k5_l2*Ak3*fl27 - 8.E+0*z1*P_l2*k3_l1*k4_l2*
             k5_k5*Ak3*fl27 );

       Chi(3,0) =
           + H * ( 1.6E+1*x2*P_k4*k4_k4*k5_q*Bk3*fl10 - 1.6E+1*x2*P_k5*
             k4_k4*k4_q*Bk3*fl10 );

          Chi(3,0) +=  + G * ( 8.E+0*x2*P_q*k3_k4*k4_k4*k5_q*Ak3*fl10 -
             8.E+0*x2*P_q*k3_k5*k4_k4*k4_q*Ak3*fl10 );

          Chi(3,0) +=  + F * (  - 8.E+0*x2*P_k4*k3_k5*k4_k4*Ak3*fl10 +
             8.E+0*x2*P_k5*k3_k4*k4_k4*Ak3*fl10 );

       Chi(3,1) =
           + H * ( 1.6E+1*y2*P_k4*k4_k4*k5_q*l2_l2*Bk3*fl10 - 1.6E+1*y2*
             P_k4*k4_k4*k5_l2*q_l2*Bk3*fl10 - 1.6E+1*y2*P_k5*k4_k4*k4_q*
             l2_l2*Bk3*fl10 + 1.6E+1*y2*P_k5*k4_k4*k4_l2*q_l2*Bk3*fl10 +
             1.6E+1*y2*P_l2*k4_k4*k4_q*k5_l2*Bk3*fl10 - 1.6E+1*y2*P_l2*
             k4_k4*k4_l2*k5_q*Bk3*fl10 );

          Chi(3,1) +=  + G * ( 8.E+0*y2*P_q*k3_k4*k4_k4*k5_q*l2_l2*Ak3*
             fl10 - 8.E+0*y2*P_q*k3_k4*k4_k4*k5_l2*q_l2*Ak3*fl10 - 8.E+0*y2
             *P_q*k3_k5*k4_k4*k4_q*l2_l2*Ak3*fl10 + 8.E+0*y2*P_q*k3_k5*
             k4_k4*k4_l2*q_l2*Ak3*fl10 + 8.E+0*y2*P_q*k3_l2*k4_k4*k4_q*
             k5_l2*Ak3*fl10 - 8.E+0*y2*P_q*k3_l2*k4_k4*k4_l2*k5_q*Ak3*fl10
              );

          Chi(3,1) +=  + F * (  - 8.E+0*y2*P_k4*k3_k5*k4_k4*l2_l2*Ak3*
             fl10 + 8.E+0*y2*P_k4*k3_l2*k4_k4*k5_l2*Ak3*fl10 + 8.E+0*y2*
             P_k5*k3_k4*k4_k4*l2_l2*Ak3*fl10 - 8.E+0*y2*P_k5*k3_l2*k4_k4*
             k4_l2*Ak3*fl10 - 8.E+0*y2*P_l2*k3_k4*k4_k4*k5_l2*Ak3*fl10 +
             8.E+0*y2*P_l2*k3_k5*k4_k4*k4_l2*Ak3*fl10 );

       Chi(3,2) =
           + H * ( 1.6E+1*z2*P_k4*k3_k5*k4_k4*q_l2*Ak3*fl10 - 1.6E+1*z2*
             P_k4*k3_l2*k4_k4*k5_q*Ak3*fl10 - 1.6E+1*z2*P_k5*k3_k4*k4_k4*
             q_l2*Ak3*fl10 + 1.6E+1*z2*P_k5*k3_l2*k4_k4*k4_q*Ak3*fl10 +
             1.6E+1*z2*P_l2*k3_k4*k4_k4*k5_q*Ak3*fl10 - 1.6E+1*z2*P_l2*
             k3_k5*k4_k4*k4_q*Ak3*fl10 );

       Chi(3,3) =
           + H * ( 1.6E+1*P_k4*k4_k4*k5_k5*k5_q*Bk3*fl10*fl20 - 1.6E+1*P_k5
             *k4_k4*k4_q*k5_k5*Bk3*fl10*fl20 );

          Chi(3,3) +=  + G * ( 8.E+0*P_q*k3_k4*k4_k4*k5_k5*k5_q*Ak3*fl10*
             fl20 - 8.E+0*P_q*k3_k5*k4_k4*k4_q*k5_k5*Ak3*fl10*fl20 );

          Chi(3,3) +=  + F * (  - 8.E+0*P_k4*k3_k5*k4_k4*k5_k5*Ak3*fl10*
             fl20 + 8.E+0*P_k5*k3_k4*k4_k4*k5_k5*Ak3*fl10*fl20 );

       Chi(3,4) =
           + H * (  - 8.E+0*P_k4*k3_k5*k4_k4*k5_k5*k5_l2*q_l2*Ak3*fl10*fl21
              - 8.E+0*P_k4*k3_l2*k4_k4*k5_k5*k5_q*k5_l2*Ak3*fl10*fl21 +
             8.E+0*P_k5*k3_k4*k4_k4*k5_k5*k5_l2*q_l2*Ak3*fl10*fl21 + 8.E+0*
             P_k5*k3_l2*k4_k4*k4_q*k5_k5*k5_l2*Ak3*fl10*fl21 - 8.E+0*P_l2*
             k3_k4*k4_k4*k5_k5*k5_q*k5_l2*Ak3*fl10*fl21 + 8.E+0*P_l2*k3_k5*
             k4_k4*k4_q*k5_k5*k5_l2*Ak3*fl10*fl21 );

          Chi(3,4) +=  + G * (  - 8.E+0*P_q*k4_k4*k4_q*k5_k5*pow(k5_l2,2)
             *Bk3*fl10*fl21 + 8.E+0*P_q*k4_k4*k4_l2*k5_k5*k5_q*k5_l2*Bk3*
             fl10*fl21 );

          Chi(3,4) +=  + F * (  - 8.E+0*P_k4*k4_k4*k5_k5*pow(k5_l2,2)*Bk3
             *fl10*fl21 + 8.E+0*P_k5*k4_k4*k4_l2*k5_k5*k5_l2*Bk3*fl10*fl21
              );

          Chi(3,4) +=  + E * (  - 8.E+0*k3_k4*k4_k4*k5_k5*pow(k5_l2,2)*
             Ak3*fl10*fl21 + 8.E+0*k3_k5*k4_k4*k4_l2*k5_k5*k5_l2*Ak3*fl10*
             fl21 );

       Chi(3,5) =
           + H * (  - 1.6E+1*P_k4*k3_k5*k4_k4*k5_q*Ak3*fl10*fl22 + 1.6E+1*
             P_k5*k3_k5*k4_k4*k4_q*Ak3*fl10*fl22 );

          Chi(3,5) +=  + G * ( 8.E+0*P_q*k4_k4*k4_k5*k5_q*Bk3*fl10*fl22
              - 8.E+0*P_q*k4_k4*k4_q*k5_k5*Bk3*fl10*fl22 );

          Chi(3,5) +=  + F * (  - 8.E+0*P_k4*k4_k4*k5_k5*Bk3*fl10*fl22 +
             8.E+0*P_k5*k4_k4*k4_k5*Bk3*fl10*fl22 );

          Chi(3,5) +=  + E * (  - 8.E+0*k3_k4*k4_k4*k5_k5*Ak3*fl10*fl22
              + 8.E+0*k3_k5*k4_k4*k4_k5*Ak3*fl10*fl22 );

       Chi(3,6) =
           + H * (  - 8.E+0*P_k4*k4_k4*k5_k5*q_l2*Bk3*fl10*fl23 + 8.E+0*
             P_k4*k4_k4*k5_q*k5_l2*Bk3*fl10*fl23 + 8.E+0*P_k5*k4_k4*k4_k5*
             q_l2*Bk3*fl10*fl23 - 8.E+0*P_k5*k4_k4*k4_q*k5_l2*Bk3*fl10*fl23
              - 8.E+0*P_l2*k4_k4*k4_k5*k5_q*Bk3*fl10*fl23 + 8.E+0*P_l2*
             k4_k4*k4_q*k5_k5*Bk3*fl10*fl23 );

          Chi(3,6) +=  + G * (  - 4.E+0*P_q*k3_k4*k4_k4*k5_k5*q_l2*Ak3*
             fl10*fl23 + 4.E+0*P_q*k3_k4*k4_k4*k5_q*k5_l2*Ak3*fl10*fl23 +
             4.E+0*P_q*k3_k5*k4_k4*k4_k5*q_l2*Ak3*fl10*fl23 + 4.E+0*P_q*
             k3_k5*k4_k4*k4_q*k5_l2*Ak3*fl10*fl23 - 8.E+0*P_q*k3_k5*k4_k4*
             k4_l2*k5_q*Ak3*fl10*fl23 + 4.E+0*P_q*k3_l2*k4_k4*k4_k5*k5_q*
             Ak3*fl10*fl23 - 4.E+0*P_q*k3_l2*k4_k4*k4_q*k5_k5*Ak3*fl10*fl23
              );

          Chi(3,6) +=  + F * ( 4.E+0*P_k4*k3_k5*k4_k4*k5_l2*Ak3*fl10*fl23
              - 4.E+0*P_k4*k3_l2*k4_k4*k5_k5*Ak3*fl10*fl23 + 4.E+0*P_k5*
             k3_k4*k4_k4*k5_l2*Ak3*fl10*fl23 - 8.E+0*P_k5*k3_k5*k4_k4*k4_l2
             *Ak3*fl10*fl23 + 4.E+0*P_k5*k3_l2*k4_k4*k4_k5*Ak3*fl10*fl23 -
             4.E+0*P_l2*k3_k4*k4_k4*k5_k5*Ak3*fl10*fl23 + 4.E+0*P_l2*k3_k5*
             k4_k4*k4_k5*Ak3*fl10*fl23 );

          Chi(3,6) +=  + E * (  - 8.E+0*k4_k4*k4_k5*k5_l2*Bk3*fl10*fl23
              + 8.E+0*k4_k4*k4_l2*k5_k5*Bk3*fl10*fl23 );

       Chi(3,7) =
           + H * (  - 8.E+0*P_k4*k3_k5*k4_k4*k5_k5*q_l2*Ak3*fl10*fl24 +
             8.E+0*P_k4*k3_l2*k4_k4*k5_k5*k5_q*Ak3*fl10*fl24 + 8.E+0*P_k5*
             k3_k4*k4_k4*k5_k5*q_l2*Ak3*fl10*fl24 - 8.E+0*P_k5*k3_l2*k4_k4*
             k4_q*k5_k5*Ak3*fl10*fl24 - 8.E+0*P_l2*k3_k4*k4_k4*k5_k5*k5_q*
             Ak3*fl10*fl24 + 8.E+0*P_l2*k3_k5*k4_k4*k4_q*k5_k5*Ak3*fl10*
             fl24 );

       Chi(3,8) =
           + H * ( 8.E+0*P_k4*k4_k4*k5_k5*k5_q*l2_l2*Bk3*fl10*fl25 - 8.E+0*
             P_k4*k4_k4*k5_k5*k5_l2*q_l2*Bk3*fl10*fl25 - 8.E+0*P_k5*k4_k4*
             k4_q*k5_k5*l2_l2*Bk3*fl10*fl25 + 8.E+0*P_k5*k4_k4*k4_l2*k5_k5*
             q_l2*Bk3*fl10*fl25 + 8.E+0*P_l2*k4_k4*k4_q*k5_k5*k5_l2*Bk3*
             fl10*fl25 - 8.E+0*P_l2*k4_k4*k4_l2*k5_k5*k5_q*Bk3*fl10*fl25 );

          Chi(3,8) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k4*k5_k5*k5_q*l2_l2*Ak3
             *fl10*fl25 - 4.E+0*P_q*k3_k4*k4_k4*k5_k5*k5_l2*q_l2*Ak3*fl10*
             fl25 - 4.E+0*P_q*k3_k5*k4_k4*k4_q*k5_k5*l2_l2*Ak3*fl10*fl25 +
             4.E+0*P_q*k3_k5*k4_k4*k4_l2*k5_k5*q_l2*Ak3*fl10*fl25 + 4.E+0*
             P_q*k3_l2*k4_k4*k4_q*k5_k5*k5_l2*Ak3*fl10*fl25 - 4.E+0*P_q*
             k3_l2*k4_k4*k4_l2*k5_k5*k5_q*Ak3*fl10*fl25 );

          Chi(3,8) +=  + F * (  - 4.E+0*P_k4*k3_k5*k4_k4*k5_k5*l2_l2*Ak3*
             fl10*fl25 + 4.E+0*P_k4*k3_l2*k4_k4*k5_k5*k5_l2*Ak3*fl10*fl25
              + 4.E+0*P_k5*k3_k4*k4_k4*k5_k5*l2_l2*Ak3*fl10*fl25 - 4.E+0*
             P_k5*k3_l2*k4_k4*k4_l2*k5_k5*Ak3*fl10*fl25 - 4.E+0*P_l2*k3_k4*
             k4_k4*k5_k5*k5_l2*Ak3*fl10*fl25 + 4.E+0*P_l2*k3_k5*k4_k4*k4_l2
             *k5_k5*Ak3*fl10*fl25 );

       Chi(3,9) =
           + H * ( 8.E+0*P_k4*k4_k4*k5_k5*k5_l2*q_l2*Bk3*fl10*fl26 + 8.E+0*
             P_k4*k4_k4*k5_q*pow(k5_l2,2)*Bk3*fl10*fl26 - 8.E+0*P_k5*k4_k4*
             k4_k5*k5_l2*q_l2*Bk3*fl10*fl26 - 8.E+0*P_k5*k4_k4*k4_q*pow(
             k5_l2,2)*Bk3*fl10*fl26 + 8.E+0*P_l2*k4_k4*k4_k5*k5_q*k5_l2*Bk3
             *fl10*fl26 - 8.E+0*P_l2*k4_k4*k4_q*k5_k5*k5_l2*Bk3*fl10*fl26 )
             ;

          Chi(3,9) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k4*k5_k5*k5_l2*q_l2*Ak3
             *fl10*fl26 + 4.E+0*P_q*k3_k4*k4_k4*k5_q*pow(k5_l2,2)*Ak3*fl10*
             fl26 - 4.E+0*P_q*k3_k5*k4_k4*k4_k5*k5_l2*q_l2*Ak3*fl10*fl26 -
             4.E+0*P_q*k3_k5*k4_k4*k4_q*pow(k5_l2,2)*Ak3*fl10*fl26 + 4.E+0*
             P_q*k3_l2*k4_k4*k4_k5*k5_q*k5_l2*Ak3*fl10*fl26 - 4.E+0*P_q*
             k3_l2*k4_k4*k4_q*k5_k5*k5_l2*Ak3*fl10*fl26 );

          Chi(3,9) +=  + F * (  - 4.E+0*P_k4*k3_k5*k4_k4*pow(k5_l2,2)*Ak3
             *fl10*fl26 - 4.E+0*P_k4*k3_l2*k4_k4*k5_k5*k5_l2*Ak3*fl10*fl26
              + 4.E+0*P_k5*k3_k4*k4_k4*pow(k5_l2,2)*Ak3*fl10*fl26 + 4.E+0*
             P_k5*k3_l2*k4_k4*k4_k5*k5_l2*Ak3*fl10*fl26 + 4.E+0*P_l2*k3_k4*
             k4_k4*k5_k5*k5_l2*Ak3*fl10*fl26 - 4.E+0*P_l2*k3_k5*k4_k4*k4_k5
             *k5_l2*Ak3*fl10*fl26 );

       Chi(3,10) =
           + H * ( 8.E+0*P_k3*k4_k4*k4_k5*k5_q*l2_l2*Ak3*fl10*fl27 - 8.E+0*
             P_k3*k4_k4*k4_k5*k5_l2*q_l2*Ak3*fl10*fl27 - 8.E+0*P_k3*k4_k4*
             k4_q*k5_k5*l2_l2*Ak3*fl10*fl27 + 8.E+0*P_k3*k4_k4*k4_q*pow(
             k5_l2,2)*Ak3*fl10*fl27 + 8.E+0*P_k3*k4_k4*k4_l2*k5_k5*q_l2*Ak3
             *fl10*fl27 - 8.E+0*P_k3*k4_k4*k4_l2*k5_q*k5_l2*Ak3*fl10*fl27
              - 1.6E+1*P_k4*k3_k5*k4_k4*k5_q*l2_l2*Ak3*fl10*fl27 + 8.E+0*
             P_k4*k3_k5*k4_k4*k5_l2*q_l2*Ak3*fl10*fl27 + 8.E+0*P_k4*k3_q*
             k4_k4*k5_k5*l2_l2*Ak3*fl10*fl27 - 8.E+0*P_k4*k3_q*k4_k4*pow(
             k5_l2,2)*Ak3*fl10*fl27 - 1.6E+1*P_k4*k3_l2*k4_k4*k5_k5*q_l2*
             Ak3*fl10*fl27 + 8.E+0*P_k4*k3_l2*k4_k4*k5_q*k5_l2*Ak3*fl10*
             fl27 + 8.E+0*P_k5*k3_k4*k4_k4*k5_l2*q_l2*Ak3*fl10*fl27 +
             1.6E+1*P_k5*k3_k5*k4_k4*k4_q*l2_l2*Ak3*fl10*fl27 - 1.6E+1*P_k5
             *k3_k5*k4_k4*k4_l2*q_l2*Ak3*fl10*fl27 - 8.E+0*P_k5*k3_q*k4_k4*
             k4_k5*l2_l2*Ak3*fl10*fl27 + 8.E+0*P_k5*k3_q*k4_k4*k4_l2*k5_l2*
             Ak3*fl10*fl27 + 1.6E+1*P_k5*k3_l2*k4_k4*k4_k5*q_l2*Ak3*fl10*
             fl27 - 8.E+0*P_k5*k3_l2*k4_k4*k4_q*k5_l2*Ak3*fl10*fl27 - 8.E+0
             *P_l2*k3_k4*k4_k4*k5_q*k5_l2*Ak3*fl10*fl27 - 8.E+0*P_l2*k3_k5*
             k4_k4*k4_q*k5_l2*Ak3*fl10*fl27 + 1.6E+1*P_l2*k3_k5*k4_k4*k4_l2
             *k5_q*Ak3*fl10*fl27 + 8.E+0*P_l2*k3_q*k4_k4*k4_k5*k5_l2*Ak3*
             fl10*fl27 - 8.E+0*P_l2*k3_q*k4_k4*k4_l2*k5_k5*Ak3*fl10*fl27 -
             1.6E+1*P_l2*k3_l2*k4_k4*k4_k5*k5_q*Ak3*fl10*fl27 + 1.6E+1*P_l2
             *k3_l2*k4_k4*k4_q*k5_k5*Ak3*fl10*fl27 );

          Chi(3,10) +=  + G * ( 4.E+0*P_q*k4_k4*k4_k5*k5_q*l2_l2*Bk3*fl10
             *fl27 - 4.E+0*P_q*k4_k4*k4_k5*k5_l2*q_l2*Bk3*fl10*fl27 - 4.E+0
             *P_q*k4_k4*k4_q*k5_k5*l2_l2*Bk3*fl10*fl27 - 4.E+0*P_q*k4_k4*
             k4_q*pow(k5_l2,2)*Bk3*fl10*fl27 + 4.E+0*P_q*k4_k4*k4_l2*k5_k5*
             q_l2*Bk3*fl10*fl27 + 4.E+0*P_q*k4_k4*k4_l2*k5_q*k5_l2*Bk3*fl10
             *fl27 );

          Chi(3,10) +=  + F * (  - 4.E+0*P_k4*k4_k4*k5_k5*l2_l2*Bk3*fl10*
             fl27 - 4.E+0*P_k4*k4_k4*pow(k5_l2,2)*Bk3*fl10*fl27 + 4.E+0*
             P_k5*k4_k4*k4_k5*l2_l2*Bk3*fl10*fl27 + 4.E+0*P_k5*k4_k4*k4_l2*
             k5_l2*Bk3*fl10*fl27 - 4.E+0*P_l2*k4_k4*k4_k5*k5_l2*Bk3*fl10*
             fl27 + 4.E+0*P_l2*k4_k4*k4_l2*k5_k5*Bk3*fl10*fl27 );

          Chi(3,10) +=  + E * (  - 4.E+0*k3_k4*k4_k4*k5_k5*l2_l2*Ak3*fl10
             *fl27 - 4.E+0*k3_k4*k4_k4*pow(k5_l2,2)*Ak3*fl10*fl27 + 4.E+0*
             k3_k5*k4_k4*k4_k5*l2_l2*Ak3*fl10*fl27 + 4.E+0*k3_k5*k4_k4*
             k4_l2*k5_l2*Ak3*fl10*fl27 - 4.E+0*k3_l2*k4_k4*k4_k5*k5_l2*Ak3*
             fl10*fl27 + 4.E+0*k3_l2*k4_k4*k4_l2*k5_k5*Ak3*fl10*fl27 );

       Chi(4,0) =
           + H * (  - 8.E+0*x2*P_k4*k3_k5*k4_k4*k4_l1*q_l1*Ak3*fl11 - 8.E+0
             *x2*P_k4*k3_l1*k4_k4*k4_l1*k5_q*Ak3*fl11 + 8.E+0*x2*P_k5*k3_k4
             *k4_k4*k4_l1*q_l1*Ak3*fl11 + 8.E+0*x2*P_k5*k3_l1*k4_k4*k4_q*
             k4_l1*Ak3*fl11 - 8.E+0*x2*P_l1*k3_k4*k4_k4*k4_l1*k5_q*Ak3*fl11
              + 8.E+0*x2*P_l1*k3_k5*k4_k4*k4_q*k4_l1*Ak3*fl11 );

          Chi(4,0) +=  + G * (  - 8.E+0*x2*P_q*k4_k4*k4_q*k4_l1*k5_l1*Bk3
             *fl11 + 8.E+0*x2*P_q*k4_k4*pow(k4_l1,2)*k5_q*Bk3*fl11 );

          Chi(4,0) +=  + F * (  - 8.E+0*x2*P_k4*k4_k4*k4_l1*k5_l1*Bk3*
             fl11 + 8.E+0*x2*P_k5*k4_k4*pow(k4_l1,2)*Bk3*fl11 );

          Chi(4,0) +=  + E * ( 8.E+0*x2*k3_k4*k4_k4*k4_l1*k5_l1*Ak3*fl11
              - 8.E+0*x2*k3_k5*k4_k4*pow(k4_l1,2)*Ak3*fl11 );

       Chi(4,1) =
           + H * ( 1.6E+1*y2*P_k3*k4_k4*k4_q*k4_l1*k5_l1*l2_l2*Ak3*fl11 -
             1.6E+1*y2*P_k3*k4_k4*k4_q*k4_l1*k5_l2*l1_l2*Ak3*fl11 + 1.6E+1*
             y2*P_k3*k4_k4*k4_l1*k4_l2*k5_q*l1_l2*Ak3*fl11 - 1.6E+1*y2*P_k3
             *k4_k4*k4_l1*k4_l2*k5_l1*q_l2*Ak3*fl11 - 1.6E+1*y2*P_k3*k4_k4*
             pow(k4_l1,2)*k5_q*l2_l2*Ak3*fl11 + 1.6E+1*y2*P_k3*k4_k4*pow(
             k4_l1,2)*k5_l2*q_l2*Ak3*fl11 - 1.6E+1*y2*P_k4*k3_k5*k4_k4*
             k4_l1*q_l2*l1_l2*Ak3*fl11 - 1.6E+1*y2*P_k4*k3_q*k4_k4*k4_l1*
             k5_l1*l2_l2*Ak3*fl11 + 1.6E+1*y2*P_k4*k3_q*k4_k4*k4_l1*k5_l2*
             l1_l2*Ak3*fl11 - 1.6E+1*y2*P_k4*k3_l2*k4_k4*k4_l1*k5_q*l1_l2*
             Ak3*fl11 + 3.2E+1*y2*P_k4*k3_l2*k4_k4*k4_l1*k5_l1*q_l2*Ak3*
             fl11 + 1.6E+1*y2*P_k5*k3_k4*k4_k4*k4_l1*q_l2*l1_l2*Ak3*fl11 -
             1.6E+1*y2*P_k5*k3_q*k4_k4*k4_l1*k4_l2*l1_l2*Ak3*fl11 + 1.6E+1*
             y2*P_k5*k3_q*k4_k4*pow(k4_l1,2)*l2_l2*Ak3*fl11 + 1.6E+1*y2*
             P_k5*k3_l2*k4_k4*k4_q*k4_l1*l1_l2*Ak3*fl11 - 3.2E+1*y2*P_k5*
             k3_l2*k4_k4*pow(k4_l1,2)*q_l2*Ak3*fl11 - 1.6E+1*y2*P_l2*k3_k4*
             k4_k4*k4_l1*k5_q*l1_l2*Ak3*fl11 + 1.6E+1*y2*P_l2*k3_k5*k4_k4*
             k4_q*k4_l1*l1_l2*Ak3*fl11 + 1.6E+1*y2*P_l2*k3_q*k4_k4*k4_l1*
             k4_l2*k5_l1*Ak3*fl11 - 1.6E+1*y2*P_l2*k3_q*k4_k4*pow(k4_l1,2)*
             k5_l2*Ak3*fl11 - 3.2E+1*y2*P_l2*k3_l2*k4_k4*k4_q*k4_l1*k5_l1*
             Ak3*fl11 + 3.2E+1*y2*P_l2*k3_l2*k4_k4*pow(k4_l1,2)*k5_q*Ak3*
             fl11 );

          Chi(4,1) +=  + G * (  - 8.E+0*y2*P_q*k4_k4*k4_q*k4_l1*k5_l1*
             l2_l2*Bk3*fl11 + 8.E+0*y2*P_q*k4_k4*k4_q*k4_l1*k5_l2*l1_l2*Bk3
             *fl11 - 8.E+0*y2*P_q*k4_k4*k4_l1*k4_l2*k5_q*l1_l2*Bk3*fl11 +
             8.E+0*y2*P_q*k4_k4*k4_l1*k4_l2*k5_l1*q_l2*Bk3*fl11 + 8.E+0*y2*
             P_q*k4_k4*pow(k4_l1,2)*k5_q*l2_l2*Bk3*fl11 - 8.E+0*y2*P_q*
             k4_k4*pow(k4_l1,2)*k5_l2*q_l2*Bk3*fl11 );

          Chi(4,1) +=  + F * (  - 8.E+0*y2*P_k4*k4_k4*k4_l1*k5_l1*l2_l2*
             Bk3*fl11 + 8.E+0*y2*P_k4*k4_k4*k4_l1*k5_l2*l1_l2*Bk3*fl11 -
             8.E+0*y2*P_k5*k4_k4*k4_l1*k4_l2*l1_l2*Bk3*fl11 + 8.E+0*y2*P_k5
             *k4_k4*pow(k4_l1,2)*l2_l2*Bk3*fl11 + 8.E+0*y2*P_l2*k4_k4*k4_l1
             *k4_l2*k5_l1*Bk3*fl11 - 8.E+0*y2*P_l2*k4_k4*pow(k4_l1,2)*k5_l2
             *Bk3*fl11 );

          Chi(4,1) +=  + E * ( 8.E+0*y2*k3_k4*k4_k4*k4_l1*k5_l1*l2_l2*Ak3
             *fl11 - 8.E+0*y2*k3_k4*k4_k4*k4_l1*k5_l2*l1_l2*Ak3*fl11 +
             8.E+0*y2*k3_k5*k4_k4*k4_l1*k4_l2*l1_l2*Ak3*fl11 - 8.E+0*y2*
             k3_k5*k4_k4*pow(k4_l1,2)*l2_l2*Ak3*fl11 - 8.E+0*y2*k3_l2*k4_k4
             *k4_l1*k4_l2*k5_l1*Ak3*fl11 + 8.E+0*y2*k3_l2*k4_k4*pow(
             k4_l1,2)*k5_l2*Ak3*fl11 );

       Chi(4,2) =
           + H * (  - 1.6E+1*z2*P_k4*k4_k4*k4_l1*k5_q*l1_l2*Bk3*fl11 +
             1.6E+1*z2*P_k4*k4_k4*k4_l1*k5_l1*q_l2*Bk3*fl11 + 1.6E+1*z2*
             P_k5*k4_k4*k4_q*k4_l1*l1_l2*Bk3*fl11 - 1.6E+1*z2*P_k5*k4_k4*
             pow(k4_l1,2)*q_l2*Bk3*fl11 - 1.6E+1*z2*P_l2*k4_k4*k4_q*k4_l1*
             k5_l1*Bk3*fl11 + 1.6E+1*z2*P_l2*k4_k4*pow(k4_l1,2)*k5_q*Bk3*
             fl11 );

          Chi(4,2) +=  + G * ( 8.E+0*z2*P_q*k3_k4*k4_k4*k4_l1*k5_q*l1_l2*
             Ak3*fl11 - 8.E+0*z2*P_q*k3_k4*k4_k4*k4_l1*k5_l1*q_l2*Ak3*fl11
              - 8.E+0*z2*P_q*k3_k5*k4_k4*k4_q*k4_l1*l1_l2*Ak3*fl11 + 8.E+0*
             z2*P_q*k3_k5*k4_k4*pow(k4_l1,2)*q_l2*Ak3*fl11 + 8.E+0*z2*P_q*
             k3_l2*k4_k4*k4_q*k4_l1*k5_l1*Ak3*fl11 - 8.E+0*z2*P_q*k3_l2*
             k4_k4*pow(k4_l1,2)*k5_q*Ak3*fl11 );

          Chi(4,2) +=  + F * (  - 8.E+0*z2*P_k4*k3_k5*k4_k4*k4_l1*l1_l2*
             Ak3*fl11 + 8.E+0*z2*P_k4*k3_l2*k4_k4*k4_l1*k5_l1*Ak3*fl11 +
             8.E+0*z2*P_k5*k3_k4*k4_k4*k4_l1*l1_l2*Ak3*fl11 - 8.E+0*z2*P_k5
             *k3_l2*k4_k4*pow(k4_l1,2)*Ak3*fl11 - 8.E+0*z2*P_l2*k3_k4*k4_k4
             *k4_l1*k5_l1*Ak3*fl11 + 8.E+0*z2*P_l2*k3_k5*k4_k4*pow(k4_l1,2)
             *Ak3*fl11 );

       Chi(4,3) =
           + H * (  - 8.E+0*P_k4*k3_k5*k4_k4*k4_l1*k5_k5*q_l1*Ak3*fl11*fl20
              - 8.E+0*P_k4*k3_l1*k4_k4*k4_l1*k5_k5*k5_q*Ak3*fl11*fl20 +
             8.E+0*P_k5*k3_k4*k4_k4*k4_l1*k5_k5*q_l1*Ak3*fl11*fl20 + 8.E+0*
             P_k5*k3_l1*k4_k4*k4_q*k4_l1*k5_k5*Ak3*fl11*fl20 - 8.E+0*P_l1*
             k3_k4*k4_k4*k4_l1*k5_k5*k5_q*Ak3*fl11*fl20 + 8.E+0*P_l1*k3_k5*
             k4_k4*k4_q*k4_l1*k5_k5*Ak3*fl11*fl20 );

          Chi(4,3) +=  + G * (  - 8.E+0*P_q*k4_k4*k4_q*k4_l1*k5_k5*k5_l1*
             Bk3*fl11*fl20 + 8.E+0*P_q*k4_k4*pow(k4_l1,2)*k5_k5*k5_q*Bk3*
             fl11*fl20 );

          Chi(4,3) +=  + F * (  - 8.E+0*P_k4*k4_k4*k4_l1*k5_k5*k5_l1*Bk3*
             fl11*fl20 + 8.E+0*P_k5*k4_k4*pow(k4_l1,2)*k5_k5*Bk3*fl11*fl20
              );

          Chi(4,3) +=  + E * ( 8.E+0*k3_k4*k4_k4*k4_l1*k5_k5*k5_l1*Ak3*
             fl11*fl20 - 8.E+0*k3_k5*k4_k4*pow(k4_l1,2)*k5_k5*Ak3*fl11*fl20
              );

       Chi(4,4) =
           + H * (  - 8.E+0*P_k4*k4_k4*k4_l1*k5_k5*k5_l1*k5_l2*q_l2*Bk3*
             fl11*fl21 - 8.E+0*P_k4*k4_k4*k4_l1*k5_k5*pow(k5_l2,2)*q_l1*Bk3
             *fl11*fl21 + 8.E+0*P_k5*k4_k4*k4_l1*k4_l2*k5_k5*k5_l2*q_l1*Bk3
             *fl11*fl21 + 8.E+0*P_k5*k4_k4*pow(k4_l1,2)*k5_k5*k5_l2*q_l2*
             Bk3*fl11*fl21 + 8.E+0*P_l1*k4_k4*k4_q*k4_l1*k5_k5*pow(k5_l2,2)
             *Bk3*fl11*fl21 - 8.E+0*P_l1*k4_k4*k4_l1*k4_l2*k5_k5*k5_q*k5_l2
             *Bk3*fl11*fl21 + 8.E+0*P_l2*k4_k4*k4_q*k4_l1*k5_k5*k5_l1*k5_l2
             *Bk3*fl11*fl21 - 8.E+0*P_l2*k4_k4*pow(k4_l1,2)*k5_k5*k5_q*
             k5_l2*Bk3*fl11*fl21 );

          Chi(4,4) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k4*k4_l1*k5_k5*k5_l1*
             k5_l2*q_l2*Ak3*fl11*fl21 + 4.E+0*P_q*k3_k4*k4_k4*k4_l1*k5_k5*
             pow(k5_l2,2)*q_l1*Ak3*fl11*fl21 - 4.E+0*P_q*k3_k5*k4_k4*k4_l1*
             k4_l2*k5_k5*k5_l2*q_l1*Ak3*fl11*fl21 - 4.E+0*P_q*k3_k5*k4_k4*
             pow(k4_l1,2)*k5_k5*k5_l2*q_l2*Ak3*fl11*fl21 + 4.E+0*P_q*k3_l1*
             k4_k4*k4_q*k4_l1*k5_k5*pow(k5_l2,2)*Ak3*fl11*fl21 - 4.E+0*P_q*
             k3_l1*k4_k4*k4_l1*k4_l2*k5_k5*k5_q*k5_l2*Ak3*fl11*fl21 + 4.E+0
             *P_q*k3_l2*k4_k4*k4_q*k4_l1*k5_k5*k5_l1*k5_l2*Ak3*fl11*fl21 -
             4.E+0*P_q*k3_l2*k4_k4*pow(k4_l1,2)*k5_k5*k5_q*k5_l2*Ak3*fl11*
             fl21 );

          Chi(4,4) +=  + F * ( 4.E+0*P_k4*k3_l1*k4_k4*k4_l1*k5_k5*pow(
             k5_l2,2)*Ak3*fl11*fl21 + 4.E+0*P_k4*k3_l2*k4_k4*k4_l1*k5_k5*
             k5_l1*k5_l2*Ak3*fl11*fl21 - 4.E+0*P_k5*k3_l1*k4_k4*k4_l1*k4_l2
             *k5_k5*k5_l2*Ak3*fl11*fl21 - 4.E+0*P_k5*k3_l2*k4_k4*pow(
             k4_l1,2)*k5_k5*k5_l2*Ak3*fl11*fl21 + 4.E+0*P_l1*k3_k4*k4_k4*
             k4_l1*k5_k5*pow(k5_l2,2)*Ak3*fl11*fl21 - 4.E+0*P_l1*k3_k5*
             k4_k4*k4_l1*k4_l2*k5_k5*k5_l2*Ak3*fl11*fl21 + 4.E+0*P_l2*k3_k4
             *k4_k4*k4_l1*k5_k5*k5_l1*k5_l2*Ak3*fl11*fl21 - 4.E+0*P_l2*
             k3_k5*k4_k4*pow(k4_l1,2)*k5_k5*k5_l2*Ak3*fl11*fl21 );

          Chi(4,4) +=  + E * ( 8.E+0*k4_k4*k4_l1*k4_l2*k5_k5*k5_l1*k5_l2*
             Bk3*fl11*fl21 - 8.E+0*k4_k4*pow(k4_l1,2)*k5_k5*pow(k5_l2,2)*
             Bk3*fl11*fl21 );

       Chi(4,5) =
           + H * (  - 8.E+0*P_k4*k4_k4*k4_l1*k5_k5*q_l1*Bk3*fl11*fl22 -
             8.E+0*P_k4*k4_k4*k4_l1*k5_q*k5_l1*Bk3*fl11*fl22 + 8.E+0*P_k5*
             k4_k4*k4_k5*k4_l1*q_l1*Bk3*fl11*fl22 + 8.E+0*P_k5*k4_k4*k4_q*
             k4_l1*k5_l1*Bk3*fl11*fl22 - 8.E+0*P_l1*k4_k4*k4_k5*k4_l1*k5_q*
             Bk3*fl11*fl22 + 8.E+0*P_l1*k4_k4*k4_q*k4_l1*k5_k5*Bk3*fl11*
             fl22 );

          Chi(4,5) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k4*k4_l1*k5_k5*q_l1*Ak3
             *fl11*fl22 + 4.E+0*P_q*k3_k4*k4_k4*k4_l1*k5_q*k5_l1*Ak3*fl11*
             fl22 - 4.E+0*P_q*k3_k5*k4_k4*k4_k5*k4_l1*q_l1*Ak3*fl11*fl22 +
             4.E+0*P_q*k3_k5*k4_k4*k4_q*k4_l1*k5_l1*Ak3*fl11*fl22 - 8.E+0*
             P_q*k3_k5*k4_k4*pow(k4_l1,2)*k5_q*Ak3*fl11*fl22 - 4.E+0*P_q*
             k3_l1*k4_k4*k4_k5*k4_l1*k5_q*Ak3*fl11*fl22 + 4.E+0*P_q*k3_l1*
             k4_k4*k4_q*k4_l1*k5_k5*Ak3*fl11*fl22 );

          Chi(4,5) +=  + F * ( 4.E+0*P_k4*k3_k5*k4_k4*k4_l1*k5_l1*Ak3*
             fl11*fl22 + 4.E+0*P_k4*k3_l1*k4_k4*k4_l1*k5_k5*Ak3*fl11*fl22
              + 4.E+0*P_k5*k3_k4*k4_k4*k4_l1*k5_l1*Ak3*fl11*fl22 - 8.E+0*
             P_k5*k3_k5*k4_k4*pow(k4_l1,2)*Ak3*fl11*fl22 - 4.E+0*P_k5*k3_l1
             *k4_k4*k4_k5*k4_l1*Ak3*fl11*fl22 + 4.E+0*P_l1*k3_k4*k4_k4*
             k4_l1*k5_k5*Ak3*fl11*fl22 - 4.E+0*P_l1*k3_k5*k4_k4*k4_k5*k4_l1
             *Ak3*fl11*fl22 );

          Chi(4,5) +=  + E * ( 8.E+0*k4_k4*k4_k5*k4_l1*k5_l1*Bk3*fl11*
             fl22 - 8.E+0*k4_k4*pow(k4_l1,2)*k5_k5*Bk3*fl11*fl22 );

       Chi(4,6) =
           + H * ( 8.E+0*P_k3*k4_k4*k4_k5*k4_l1*k5_l1*q_l2*Ak3*fl11*fl23 -
             8.E+0*P_k3*k4_k4*k4_k5*k4_l1*k5_l2*q_l1*Ak3*fl11*fl23 + 8.E+0*
             P_k3*k4_k4*k4_l1*k4_l2*k5_k5*q_l1*Ak3*fl11*fl23 - 8.E+0*P_k3*
             k4_k4*k4_l1*k4_l2*k5_q*k5_l1*Ak3*fl11*fl23 - 8.E+0*P_k3*k4_k4*
             pow(k4_l1,2)*k5_k5*q_l2*Ak3*fl11*fl23 + 8.E+0*P_k3*k4_k4*pow(
             k4_l1,2)*k5_q*k5_l2*Ak3*fl11*fl23 + 8.E+0*P_k4*k3_k5*k4_k4*
             k4_l1*k5_l2*q_l1*Ak3*fl11*fl23 + 8.E+0*P_k4*k3_l1*k4_k4*k4_l1*
             k5_k5*q_l2*Ak3*fl11*fl23 - 8.E+0*P_k4*k3_l1*k4_k4*k4_l1*k5_q*
             k5_l2*Ak3*fl11*fl23 - 8.E+0*P_k4*k3_l2*k4_k4*k4_l1*k5_k5*q_l1*
             Ak3*fl11*fl23 + 8.E+0*P_k5*k3_k4*k4_k4*k4_l1*k5_l2*q_l1*Ak3*
             fl11*fl23 - 1.6E+1*P_k5*k3_k5*k4_k4*k4_l1*k4_l2*q_l1*Ak3*fl11*
             fl23 + 8.E+0*P_k5*k3_q*k4_k4*k4_l1*k4_l2*k5_l1*Ak3*fl11*fl23
              - 8.E+0*P_k5*k3_q*k4_k4*pow(k4_l1,2)*k5_l2*Ak3*fl11*fl23 -
             8.E+0*P_k5*k3_l1*k4_k4*k4_k5*k4_l1*q_l2*Ak3*fl11*fl23 + 8.E+0*
             P_k5*k3_l1*k4_k4*k4_q*k4_l1*k5_l2*Ak3*fl11*fl23 + 8.E+0*P_k5*
             k3_l2*k4_k4*k4_k5*k4_l1*q_l1*Ak3*fl11*fl23 + 8.E+0*P_l1*k3_k4*
             k4_k4*k4_l1*k5_k5*q_l2*Ak3*fl11*fl23 - 8.E+0*P_l1*k3_k4*k4_k4*
             k4_l1*k5_q*k5_l2*Ak3*fl11*fl23 - 8.E+0*P_l1*k3_k5*k4_k4*k4_k5*
             k4_l1*q_l2*Ak3*fl11*fl23 - 8.E+0*P_l1*k3_k5*k4_k4*k4_q*k4_l1*
             k5_l2*Ak3*fl11*fl23 + 1.6E+1*P_l1*k3_k5*k4_k4*k4_l1*k4_l2*k5_q
             *Ak3*fl11*fl23 + 8.E+0*P_l1*k3_q*k4_k4*k4_k5*k4_l1*k5_l2*Ak3*
             fl11*fl23 - 8.E+0*P_l1*k3_q*k4_k4*k4_l1*k4_l2*k5_k5*Ak3*fl11*
             fl23 - 8.E+0*P_l1*k3_l2*k4_k4*k4_k5*k4_l1*k5_q*Ak3*fl11*fl23
              + 8.E+0*P_l1*k3_l2*k4_k4*k4_q*k4_l1*k5_k5*Ak3*fl11*fl23 -
             8.E+0*P_l2*k3_k4*k4_k4*k4_l1*k5_k5*q_l1*Ak3*fl11*fl23 + 8.E+0*
             P_l2*k3_k5*k4_k4*k4_k5*k4_l1*q_l1*Ak3*fl11*fl23 - 8.E+0*P_l2*
             k3_q*k4_k4*k4_k5*k4_l1*k5_l1*Ak3*fl11*fl23 + 8.E+0*P_l2*k3_q*
             k4_k4*pow(k4_l1,2)*k5_k5*Ak3*fl11*fl23 + 8.E+0*P_l2*k3_l1*
             k4_k4*k4_k5*k4_l1*k5_q*Ak3*fl11*fl23 - 8.E+0*P_l2*k3_l1*k4_k4*
             k4_q*k4_l1*k5_k5*Ak3*fl11*fl23 );

          Chi(4,6) +=  + G * ( 4.E+0*P_q*k4_k4*k4_k5*k4_l1*k5_l1*q_l2*Bk3
             *fl11*fl23 + 4.E+0*P_q*k4_k4*k4_k5*k4_l1*k5_l2*q_l1*Bk3*fl11*
             fl23 - 4.E+0*P_q*k4_k4*k4_l1*k4_l2*k5_k5*q_l1*Bk3*fl11*fl23 -
             4.E+0*P_q*k4_k4*k4_l1*k4_l2*k5_q*k5_l1*Bk3*fl11*fl23 - 4.E+0*
             P_q*k4_k4*pow(k4_l1,2)*k5_k5*q_l2*Bk3*fl11*fl23 + 4.E+0*P_q*
             k4_k4*pow(k4_l1,2)*k5_q*k5_l2*Bk3*fl11*fl23 );

          Chi(4,6) +=  + F * (  - 4.E+0*P_k5*k4_k4*k4_l1*k4_l2*k5_l1*Bk3*
             fl11*fl23 + 4.E+0*P_k5*k4_k4*pow(k4_l1,2)*k5_l2*Bk3*fl11*fl23
              + 4.E+0*P_l1*k4_k4*k4_k5*k4_l1*k5_l2*Bk3*fl11*fl23 - 4.E+0*
             P_l1*k4_k4*k4_l1*k4_l2*k5_k5*Bk3*fl11*fl23 + 4.E+0*P_l2*k4_k4*
             k4_k5*k4_l1*k5_l1*Bk3*fl11*fl23 - 4.E+0*P_l2*k4_k4*pow(
             k4_l1,2)*k5_k5*Bk3*fl11*fl23 );

          Chi(4,6) +=  + E * (  - 4.E+0*k3_k5*k4_k4*k4_l1*k4_l2*k5_l1*Ak3
             *fl11*fl23 + 4.E+0*k3_k5*k4_k4*pow(k4_l1,2)*k5_l2*Ak3*fl11*
             fl23 + 4.E+0*k3_l1*k4_k4*k4_k5*k4_l1*k5_l2*Ak3*fl11*fl23 -
             4.E+0*k3_l1*k4_k4*k4_l1*k4_l2*k5_k5*Ak3*fl11*fl23 + 4.E+0*
             k3_l2*k4_k4*k4_k5*k4_l1*k5_l1*Ak3*fl11*fl23 - 4.E+0*k3_l2*
             k4_k4*pow(k4_l1,2)*k5_k5*Ak3*fl11*fl23 );

       Chi(4,7) =
           + H * ( 8.E+0*P_k4*k4_k4*k4_l1*k5_k5*k5_q*l1_l2*Bk3*fl11*fl24 -
             8.E+0*P_k4*k4_k4*k4_l1*k5_k5*k5_l1*q_l2*Bk3*fl11*fl24 - 8.E+0*
             P_k5*k4_k4*k4_q*k4_l1*k5_k5*l1_l2*Bk3*fl11*fl24 + 8.E+0*P_k5*
             k4_k4*pow(k4_l1,2)*k5_k5*q_l2*Bk3*fl11*fl24 + 8.E+0*P_l2*k4_k4
             *k4_q*k4_l1*k5_k5*k5_l1*Bk3*fl11*fl24 - 8.E+0*P_l2*k4_k4*pow(
             k4_l1,2)*k5_k5*k5_q*Bk3*fl11*fl24 );

          Chi(4,7) +=  + G * (  - 4.E+0*P_q*k3_k4*k4_k4*k4_l1*k5_k5*k5_q*
             l1_l2*Ak3*fl11*fl24 + 4.E+0*P_q*k3_k4*k4_k4*k4_l1*k5_k5*k5_l1*
             q_l2*Ak3*fl11*fl24 + 4.E+0*P_q*k3_k5*k4_k4*k4_q*k4_l1*k5_k5*
             l1_l2*Ak3*fl11*fl24 - 4.E+0*P_q*k3_k5*k4_k4*pow(k4_l1,2)*k5_k5
             *q_l2*Ak3*fl11*fl24 - 4.E+0*P_q*k3_l2*k4_k4*k4_q*k4_l1*k5_k5*
             k5_l1*Ak3*fl11*fl24 + 4.E+0*P_q*k3_l2*k4_k4*pow(k4_l1,2)*k5_k5
             *k5_q*Ak3*fl11*fl24 );

          Chi(4,7) +=  + F * ( 4.E+0*P_k4*k3_k5*k4_k4*k4_l1*k5_k5*l1_l2*
             Ak3*fl11*fl24 - 4.E+0*P_k4*k3_l2*k4_k4*k4_l1*k5_k5*k5_l1*Ak3*
             fl11*fl24 - 4.E+0*P_k5*k3_k4*k4_k4*k4_l1*k5_k5*l1_l2*Ak3*fl11*
             fl24 + 4.E+0*P_k5*k3_l2*k4_k4*pow(k4_l1,2)*k5_k5*Ak3*fl11*fl24
              + 4.E+0*P_l2*k3_k4*k4_k4*k4_l1*k5_k5*k5_l1*Ak3*fl11*fl24 -
             4.E+0*P_l2*k3_k5*k4_k4*pow(k4_l1,2)*k5_k5*Ak3*fl11*fl24 );

       Chi(4,8) =
           + H * ( 8.E+0*P_k3*k4_k4*k4_q*k4_l1*k5_k5*k5_l1*l2_l2*Ak3*fl11*
             fl25 - 8.E+0*P_k3*k4_k4*k4_q*k4_l1*k5_k5*k5_l2*l1_l2*Ak3*fl11*
             fl25 + 8.E+0*P_k3*k4_k4*k4_l1*k4_l2*k5_k5*k5_q*l1_l2*Ak3*fl11*
             fl25 - 8.E+0*P_k3*k4_k4*k4_l1*k4_l2*k5_k5*k5_l1*q_l2*Ak3*fl11*
             fl25 - 8.E+0*P_k3*k4_k4*pow(k4_l1,2)*k5_k5*k5_q*l2_l2*Ak3*fl11
             *fl25 + 8.E+0*P_k3*k4_k4*pow(k4_l1,2)*k5_k5*k5_l2*q_l2*Ak3*
             fl11*fl25 - 8.E+0*P_k4*k3_k5*k4_k4*k4_l1*k5_k5*q_l2*l1_l2*Ak3*
             fl11*fl25 - 8.E+0*P_k4*k3_q*k4_k4*k4_l1*k5_k5*k5_l1*l2_l2*Ak3*
             fl11*fl25 + 8.E+0*P_k4*k3_q*k4_k4*k4_l1*k5_k5*k5_l2*l1_l2*Ak3*
             fl11*fl25 - 8.E+0*P_k4*k3_l2*k4_k4*k4_l1*k5_k5*k5_q*l1_l2*Ak3*
             fl11*fl25 + 1.6E+1*P_k4*k3_l2*k4_k4*k4_l1*k5_k5*k5_l1*q_l2*Ak3
             *fl11*fl25 + 8.E+0*P_k5*k3_k4*k4_k4*k4_l1*k5_k5*q_l2*l1_l2*Ak3
             *fl11*fl25 - 8.E+0*P_k5*k3_q*k4_k4*k4_l1*k4_l2*k5_k5*l1_l2*Ak3
             *fl11*fl25 + 8.E+0*P_k5*k3_q*k4_k4*pow(k4_l1,2)*k5_k5*l2_l2*
             Ak3*fl11*fl25 + 8.E+0*P_k5*k3_l2*k4_k4*k4_q*k4_l1*k5_k5*l1_l2*
             Ak3*fl11*fl25 - 1.6E+1*P_k5*k3_l2*k4_k4*pow(k4_l1,2)*k5_k5*
             q_l2*Ak3*fl11*fl25 - 8.E+0*P_l2*k3_k4*k4_k4*k4_l1*k5_k5*k5_q*
             l1_l2*Ak3*fl11*fl25 + 8.E+0*P_l2*k3_k5*k4_k4*k4_q*k4_l1*k5_k5*
             l1_l2*Ak3*fl11*fl25 + 8.E+0*P_l2*k3_q*k4_k4*k4_l1*k4_l2*k5_k5*
             k5_l1*Ak3*fl11*fl25 - 8.E+0*P_l2*k3_q*k4_k4*pow(k4_l1,2)*k5_k5
             *k5_l2*Ak3*fl11*fl25 - 1.6E+1*P_l2*k3_l2*k4_k4*k4_q*k4_l1*
             k5_k5*k5_l1*Ak3*fl11*fl25 + 1.6E+1*P_l2*k3_l2*k4_k4*pow(
             k4_l1,2)*k5_k5*k5_q*Ak3*fl11*fl25 );

          Chi(4,8) +=  + G * (  - 4.E+0*P_q*k4_k4*k4_q*k4_l1*k5_k5*k5_l1*
             l2_l2*Bk3*fl11*fl25 + 4.E+0*P_q*k4_k4*k4_q*k4_l1*k5_k5*k5_l2*
             l1_l2*Bk3*fl11*fl25 - 4.E+0*P_q*k4_k4*k4_l1*k4_l2*k5_k5*k5_q*
             l1_l2*Bk3*fl11*fl25 + 4.E+0*P_q*k4_k4*k4_l1*k4_l2*k5_k5*k5_l1*
             q_l2*Bk3*fl11*fl25 + 4.E+0*P_q*k4_k4*pow(k4_l1,2)*k5_k5*k5_q*
             l2_l2*Bk3*fl11*fl25 - 4.E+0*P_q*k4_k4*pow(k4_l1,2)*k5_k5*k5_l2
             *q_l2*Bk3*fl11*fl25 );

          Chi(4,8) +=  + F * (  - 4.E+0*P_k4*k4_k4*k4_l1*k5_k5*k5_l1*
             l2_l2*Bk3*fl11*fl25 + 4.E+0*P_k4*k4_k4*k4_l1*k5_k5*k5_l2*l1_l2
             *Bk3*fl11*fl25 - 4.E+0*P_k5*k4_k4*k4_l1*k4_l2*k5_k5*l1_l2*Bk3*
             fl11*fl25 + 4.E+0*P_k5*k4_k4*pow(k4_l1,2)*k5_k5*l2_l2*Bk3*fl11
             *fl25 + 4.E+0*P_l2*k4_k4*k4_l1*k4_l2*k5_k5*k5_l1*Bk3*fl11*fl25
              - 4.E+0*P_l2*k4_k4*pow(k4_l1,2)*k5_k5*k5_l2*Bk3*fl11*fl25 );

          Chi(4,8) +=  + E * ( 4.E+0*k3_k4*k4_k4*k4_l1*k5_k5*k5_l1*l2_l2*
             Ak3*fl11*fl25 - 4.E+0*k3_k4*k4_k4*k4_l1*k5_k5*k5_l2*l1_l2*Ak3*
             fl11*fl25 + 4.E+0*k3_k5*k4_k4*k4_l1*k4_l2*k5_k5*l1_l2*Ak3*fl11
             *fl25 - 4.E+0*k3_k5*k4_k4*pow(k4_l1,2)*k5_k5*l2_l2*Ak3*fl11*
             fl25 - 4.E+0*k3_l2*k4_k4*k4_l1*k4_l2*k5_k5*k5_l1*Ak3*fl11*fl25
              + 4.E+0*k3_l2*k4_k4*pow(k4_l1,2)*k5_k5*k5_l2*Ak3*fl11*fl25 );

       Chi(4,9) =
           + H * (  - 8.E+0*P_k3*k4_k4*k4_k5*k4_l1*k5_q*k5_l2*l1_l2*Ak3*
             fl11*fl26 + 8.E+0*P_k3*k4_k4*k4_k5*k4_l1*k5_l1*k5_l2*q_l2*Ak3*
             fl11*fl26 + 8.E+0*P_k3*k4_k4*k4_q*k4_l1*k5_k5*k5_l2*l1_l2*Ak3*
             fl11*fl26 - 8.E+0*P_k3*k4_k4*k4_q*k4_l1*k5_l1*pow(k5_l2,2)*Ak3
             *fl11*fl26 - 8.E+0*P_k3*k4_k4*pow(k4_l1,2)*k5_k5*k5_l2*q_l2*
             Ak3*fl11*fl26 + 8.E+0*P_k3*k4_k4*pow(k4_l1,2)*k5_q*pow(
             k5_l2,2)*Ak3*fl11*fl26 + 1.6E+1*P_k4*k3_k5*k4_k4*k4_l1*k5_q*
             k5_l2*l1_l2*Ak3*fl11*fl26 - 8.E+0*P_k4*k3_k5*k4_k4*k4_l1*k5_l1
             *k5_l2*q_l2*Ak3*fl11*fl26 - 8.E+0*P_k4*k3_k5*k4_k4*k4_l1*pow(
             k5_l2,2)*q_l1*Ak3*fl11*fl26 - 8.E+0*P_k4*k3_q*k4_k4*k4_l1*
             k5_k5*k5_l2*l1_l2*Ak3*fl11*fl26 + 8.E+0*P_k4*k3_q*k4_k4*k4_l1*
             k5_l1*pow(k5_l2,2)*Ak3*fl11*fl26 - 8.E+0*P_k4*k3_l1*k4_k4*
             k4_l1*k5_q*pow(k5_l2,2)*Ak3*fl11*fl26 - 8.E+0*P_k4*k3_l2*k4_k4
             *k4_l1*k5_q*k5_l1*k5_l2*Ak3*fl11*fl26 - 8.E+0*P_k5*k3_k4*k4_k4
             *k4_l1*k5_l1*k5_l2*q_l2*Ak3*fl11*fl26 + 8.E+0*P_k5*k3_k4*k4_k4
             *k4_l1*pow(k5_l2,2)*q_l1*Ak3*fl11*fl26 - 1.6E+1*P_k5*k3_k5*
             k4_k4*k4_q*k4_l1*k5_l2*l1_l2*Ak3*fl11*fl26 + 1.6E+1*P_k5*k3_k5
             *k4_k4*pow(k4_l1,2)*k5_l2*q_l2*Ak3*fl11*fl26 + 8.E+0*P_k5*k3_q
             *k4_k4*k4_k5*k4_l1*k5_l2*l1_l2*Ak3*fl11*fl26 - 8.E+0*P_k5*k3_q
             *k4_k4*pow(k4_l1,2)*pow(k5_l2,2)*Ak3*fl11*fl26 + 8.E+0*P_k5*
             k3_l1*k4_k4*k4_q*k4_l1*pow(k5_l2,2)*Ak3*fl11*fl26 + 8.E+0*P_k5
             *k3_l2*k4_k4*k4_q*k4_l1*k5_l1*k5_l2*Ak3*fl11*fl26 - 8.E+0*P_l1
             *k3_k4*k4_k4*k4_l1*k5_q*pow(k5_l2,2)*Ak3*fl11*fl26 + 8.E+0*
             P_l1*k3_k5*k4_k4*k4_q*k4_l1*pow(k5_l2,2)*Ak3*fl11*fl26 + 8.E+0
             *P_l2*k3_k4*k4_k4*k4_l1*k5_q*k5_l1*k5_l2*Ak3*fl11*fl26 + 8.E+0
             *P_l2*k3_k5*k4_k4*k4_q*k4_l1*k5_l1*k5_l2*Ak3*fl11*fl26 -
             1.6E+1*P_l2*k3_k5*k4_k4*pow(k4_l1,2)*k5_q*k5_l2*Ak3*fl11*fl26
              - 8.E+0*P_l2*k3_q*k4_k4*k4_k5*k4_l1*k5_l1*k5_l2*Ak3*fl11*fl26
              + 8.E+0*P_l2*k3_q*k4_k4*pow(k4_l1,2)*k5_k5*k5_l2*Ak3*fl11*
             fl26 );

          Chi(4,9) +=  + G * ( 4.E+0*P_q*k4_k4*k4_k5*k4_l1*k5_q*k5_l2*
             l1_l2*Bk3*fl11*fl26 - 4.E+0*P_q*k4_k4*k4_k5*k4_l1*k5_l1*k5_l2*
             q_l2*Bk3*fl11*fl26 - 4.E+0*P_q*k4_k4*k4_q*k4_l1*k5_k5*k5_l2*
             l1_l2*Bk3*fl11*fl26 - 4.E+0*P_q*k4_k4*k4_q*k4_l1*k5_l1*pow(
             k5_l2,2)*Bk3*fl11*fl26 + 4.E+0*P_q*k4_k4*pow(k4_l1,2)*k5_k5*
             k5_l2*q_l2*Bk3*fl11*fl26 + 4.E+0*P_q*k4_k4*pow(k4_l1,2)*k5_q*
             pow(k5_l2,2)*Bk3*fl11*fl26 );

          Chi(4,9) +=  + F * (  - 4.E+0*P_k4*k4_k4*k4_l1*k5_k5*k5_l2*
             l1_l2*Bk3*fl11*fl26 - 4.E+0*P_k4*k4_k4*k4_l1*k5_l1*pow(
             k5_l2,2)*Bk3*fl11*fl26 + 4.E+0*P_k5*k4_k4*k4_k5*k4_l1*k5_l2*
             l1_l2*Bk3*fl11*fl26 + 4.E+0*P_k5*k4_k4*pow(k4_l1,2)*pow(
             k5_l2,2)*Bk3*fl11*fl26 - 4.E+0*P_l2*k4_k4*k4_k5*k4_l1*k5_l1*
             k5_l2*Bk3*fl11*fl26 + 4.E+0*P_l2*k4_k4*pow(k4_l1,2)*k5_k5*
             k5_l2*Bk3*fl11*fl26 );

          Chi(4,9) +=  + E * ( 4.E+0*k3_k4*k4_k4*k4_l1*k5_k5*k5_l2*l1_l2*
             Ak3*fl11*fl26 + 4.E+0*k3_k4*k4_k4*k4_l1*k5_l1*pow(k5_l2,2)*Ak3
             *fl11*fl26 - 4.E+0*k3_k5*k4_k4*k4_k5*k4_l1*k5_l2*l1_l2*Ak3*
             fl11*fl26 - 4.E+0*k3_k5*k4_k4*pow(k4_l1,2)*pow(k5_l2,2)*Ak3*
             fl11*fl26 + 4.E+0*k3_l2*k4_k4*k4_k5*k4_l1*k5_l1*k5_l2*Ak3*fl11
             *fl26 - 4.E+0*k3_l2*k4_k4*pow(k4_l1,2)*k5_k5*k5_l2*Ak3*fl11*
             fl26 );

       Chi(4,10) =
           + H * (  - 8.E+0*P_k4*k4_k4*k4_l1*k5_k5*q_l2*l1_l2*Bk3*fl11*fl27
              - 8.E+0*P_k4*k4_k4*k4_l1*k5_q*k5_l1*l2_l2*Bk3*fl11*fl27 +
             8.E+0*P_k4*k4_k4*k4_l1*k5_q*k5_l2*l1_l2*Bk3*fl11*fl27 - 8.E+0*
             P_k4*k4_k4*k4_l1*pow(k5_l2,2)*q_l1*Bk3*fl11*fl27 + 8.E+0*P_k5*
             k4_k4*k4_k5*k4_l1*q_l2*l1_l2*Bk3*fl11*fl27 + 8.E+0*P_k5*k4_k4*
             k4_q*k4_l1*k5_l1*l2_l2*Bk3*fl11*fl27 - 8.E+0*P_k5*k4_k4*k4_q*
             k4_l1*k5_l2*l1_l2*Bk3*fl11*fl27 - 8.E+0*P_k5*k4_k4*k4_l1*k4_l2
             *k5_l1*q_l2*Bk3*fl11*fl27 + 8.E+0*P_k5*k4_k4*k4_l1*k4_l2*k5_l2
             *q_l1*Bk3*fl11*fl27 + 8.E+0*P_k5*k4_k4*pow(k4_l1,2)*k5_l2*q_l2
             *Bk3*fl11*fl27 + 8.E+0*P_l1*k4_k4*k4_q*k4_l1*pow(k5_l2,2)*Bk3*
             fl11*fl27 - 8.E+0*P_l1*k4_k4*k4_l1*k4_l2*k5_q*k5_l2*Bk3*fl11*
             fl27 - 8.E+0*P_l2*k4_k4*k4_k5*k4_l1*k5_q*l1_l2*Bk3*fl11*fl27
              + 8.E+0*P_l2*k4_k4*k4_q*k4_l1*k5_k5*l1_l2*Bk3*fl11*fl27 +
             8.E+0*P_l2*k4_k4*k4_l1*k4_l2*k5_q*k5_l1*Bk3*fl11*fl27 - 8.E+0*
             P_l2*k4_k4*pow(k4_l1,2)*k5_q*k5_l2*Bk3*fl11*fl27 );

          Chi(4,10) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k4*k4_l1*k5_k5*q_l2*
             l1_l2*Ak3*fl11*fl27 + 4.E+0*P_q*k3_k4*k4_k4*k4_l1*k5_q*k5_l1*
             l2_l2*Ak3*fl11*fl27 - 4.E+0*P_q*k3_k4*k4_k4*k4_l1*k5_q*k5_l2*
             l1_l2*Ak3*fl11*fl27 + 4.E+0*P_q*k3_k4*k4_k4*k4_l1*pow(k5_l2,2)
             *q_l1*Ak3*fl11*fl27 - 4.E+0*P_q*k3_k5*k4_k4*k4_k5*k4_l1*q_l2*
             l1_l2*Ak3*fl11*fl27 + 4.E+0*P_q*k3_k5*k4_k4*k4_q*k4_l1*k5_l1*
             l2_l2*Ak3*fl11*fl27 - 4.E+0*P_q*k3_k5*k4_k4*k4_q*k4_l1*k5_l2*
             l1_l2*Ak3*fl11*fl27 + 8.E+0*P_q*k3_k5*k4_k4*k4_l1*k4_l2*k5_q*
             l1_l2*Ak3*fl11*fl27 - 4.E+0*P_q*k3_k5*k4_k4*k4_l1*k4_l2*k5_l1*
             q_l2*Ak3*fl11*fl27 - 4.E+0*P_q*k3_k5*k4_k4*k4_l1*k4_l2*k5_l2*
             q_l1*Ak3*fl11*fl27 - 8.E+0*P_q*k3_k5*k4_k4*pow(k4_l1,2)*k5_q*
             l2_l2*Ak3*fl11*fl27 + 4.E+0*P_q*k3_k5*k4_k4*pow(k4_l1,2)*k5_l2
             *q_l2*Ak3*fl11*fl27 - 4.E+0*P_q*k3_q*k4_k4*k4_k5*k4_l1*k5_l1*
             l2_l2*Ak3*fl11*fl27 + 4.E+0*P_q*k3_q*k4_k4*k4_k5*k4_l1*k5_l2*
             l1_l2*Ak3*fl11*fl27 - 4.E+0*P_q*k3_q*k4_k4*k4_l1*k4_l2*k5_k5*
             l1_l2*Ak3*fl11*fl27 + 4.E+0*P_q*k3_q*k4_k4*k4_l1*k4_l2*k5_l1*
             k5_l2*Ak3*fl11*fl27 + 4.E+0*P_q*k3_q*k4_k4*pow(k4_l1,2)*k5_k5*
             l2_l2*Ak3*fl11*fl27 - 4.E+0*P_q*k3_q*k4_k4*pow(k4_l1,2)*pow(
             k5_l2,2)*Ak3*fl11*fl27 + 4.E+0*P_q*k3_l1*k4_k4*k4_q*k4_l1*pow(
             k5_l2,2)*Ak3*fl11*fl27 - 4.E+0*P_q*k3_l1*k4_k4*k4_l1*k4_l2*
             k5_q*k5_l2*Ak3*fl11*fl27 - 4.E+0*P_q*k3_l2*k4_k4*k4_k5*k4_l1*
             k5_q*l1_l2*Ak3*fl11*fl27 + 8.E+0*P_q*k3_l2*k4_k4*k4_k5*k4_l1*
             k5_l1*q_l2*Ak3*fl11*fl27 + 4.E+0*P_q*k3_l2*k4_k4*k4_q*k4_l1*
             k5_k5*l1_l2*Ak3*fl11*fl27 - 4.E+0*P_q*k3_l2*k4_k4*k4_l1*k4_l2*
             k5_q*k5_l1*Ak3*fl11*fl27 - 8.E+0*P_q*k3_l2*k4_k4*pow(k4_l1,2)*
             k5_k5*q_l2*Ak3*fl11*fl27 + 4.E+0*P_q*k3_l2*k4_k4*pow(k4_l1,2)*
             k5_q*k5_l2*Ak3*fl11*fl27 );

          Chi(4,10) +=  + F * (  - 4.E+0*P_k3*k4_k4*k4_k5*k4_l1*k5_l1*
             l2_l2*Ak3*fl11*fl27 + 4.E+0*P_k3*k4_k4*k4_k5*k4_l1*k5_l2*l1_l2
             *Ak3*fl11*fl27 - 4.E+0*P_k3*k4_k4*k4_l1*k4_l2*k5_k5*l1_l2*Ak3*
             fl11*fl27 + 4.E+0*P_k3*k4_k4*k4_l1*k4_l2*k5_l1*k5_l2*Ak3*fl11*
             fl27 + 4.E+0*P_k3*k4_k4*pow(k4_l1,2)*k5_k5*l2_l2*Ak3*fl11*fl27
              - 4.E+0*P_k3*k4_k4*pow(k4_l1,2)*pow(k5_l2,2)*Ak3*fl11*fl27 +
             4.E+0*P_k4*k3_k5*k4_k4*k4_l1*k5_l1*l2_l2*Ak3*fl11*fl27 - 4.E+0
             *P_k4*k3_k5*k4_k4*k4_l1*k5_l2*l1_l2*Ak3*fl11*fl27 + 4.E+0*P_k4
             *k3_l1*k4_k4*k4_l1*pow(k5_l2,2)*Ak3*fl11*fl27 + 4.E+0*P_k4*
             k3_l2*k4_k4*k4_l1*k5_k5*l1_l2*Ak3*fl11*fl27 + 4.E+0*P_k5*k3_k4
             *k4_k4*k4_l1*k5_l1*l2_l2*Ak3*fl11*fl27 - 4.E+0*P_k5*k3_k4*
             k4_k4*k4_l1*k5_l2*l1_l2*Ak3*fl11*fl27 + 8.E+0*P_k5*k3_k5*k4_k4
             *k4_l1*k4_l2*l1_l2*Ak3*fl11*fl27 - 8.E+0*P_k5*k3_k5*k4_k4*pow(
             k4_l1,2)*l2_l2*Ak3*fl11*fl27 - 4.E+0*P_k5*k3_l1*k4_k4*k4_l1*
             k4_l2*k5_l2*Ak3*fl11*fl27 - 4.E+0*P_k5*k3_l2*k4_k4*k4_k5*k4_l1
             *l1_l2*Ak3*fl11*fl27 - 4.E+0*P_k5*k3_l2*k4_k4*k4_l1*k4_l2*
             k5_l1*Ak3*fl11*fl27 + 4.E+0*P_k5*k3_l2*k4_k4*pow(k4_l1,2)*
             k5_l2*Ak3*fl11*fl27 + 4.E+0*P_l1*k3_k4*k4_k4*k4_l1*pow(
             k5_l2,2)*Ak3*fl11*fl27 - 4.E+0*P_l1*k3_k5*k4_k4*k4_l1*k4_l2*
             k5_l2*Ak3*fl11*fl27 + 4.E+0*P_l2*k3_k4*k4_k4*k4_l1*k5_k5*l1_l2
             *Ak3*fl11*fl27 - 4.E+0*P_l2*k3_k5*k4_k4*k4_k5*k4_l1*l1_l2*Ak3*
             fl11*fl27 - 4.E+0*P_l2*k3_k5*k4_k4*k4_l1*k4_l2*k5_l1*Ak3*fl11*
             fl27 + 4.E+0*P_l2*k3_k5*k4_k4*pow(k4_l1,2)*k5_l2*Ak3*fl11*fl27
              + 8.E+0*P_l2*k3_l2*k4_k4*k4_k5*k4_l1*k5_l1*Ak3*fl11*fl27 -
             8.E+0*P_l2*k3_l2*k4_k4*pow(k4_l1,2)*k5_k5*Ak3*fl11*fl27 );

          Chi(4,10) +=  + E * ( 4.E+0*k4_k4*k4_k5*k4_l1*k5_l1*l2_l2*Bk3*
             fl11*fl27 - 4.E+0*k4_k4*k4_k5*k4_l1*k5_l2*l1_l2*Bk3*fl11*fl27
              + 4.E+0*k4_k4*k4_l1*k4_l2*k5_k5*l1_l2*Bk3*fl11*fl27 + 4.E+0*
             k4_k4*k4_l1*k4_l2*k5_l1*k5_l2*Bk3*fl11*fl27 - 4.E+0*k4_k4*pow(
             k4_l1,2)*k5_k5*l2_l2*Bk3*fl11*fl27 - 4.E+0*k4_k4*pow(k4_l1,2)*
             pow(k5_l2,2)*Bk3*fl11*fl27 );

       Chi(5,0) =
           + H * (  - 1.6E+1*x2*P_k4*k3_k4*k5_q*Ak3*fl12 + 1.6E+1*x2*P_k5*
             k3_k4*k4_q*Ak3*fl12 );

          Chi(5,0) +=  + G * ( 8.E+0*x2*P_q*k4_k4*k5_q*Bk3*fl12 - 8.E+0*
             x2*P_q*k4_k5*k4_q*Bk3*fl12 );

          Chi(5,0) +=  + F * (  - 8.E+0*x2*P_k4*k4_k5*Bk3*fl12 + 8.E+0*x2
             *P_k5*k4_k4*Bk3*fl12 );

          Chi(5,0) +=  + E * ( 8.E+0*x2*k3_k4*k4_k5*Ak3*fl12 - 8.E+0*x2*
             k3_k5*k4_k4*Ak3*fl12 );

       Chi(5,1) =
           + H * (  - 1.6E+1*y2*P_k3*k4_k4*k5_q*l2_l2*Ak3*fl12 + 1.6E+1*y2*
             P_k3*k4_k4*k5_l2*q_l2*Ak3*fl12 + 1.6E+1*y2*P_k3*k4_k5*k4_q*
             l2_l2*Ak3*fl12 - 1.6E+1*y2*P_k3*k4_k5*k4_l2*q_l2*Ak3*fl12 -
             1.6E+1*y2*P_k3*k4_q*k4_l2*k5_l2*Ak3*fl12 + 1.6E+1*y2*P_k3*pow(
             k4_l2,2)*k5_q*Ak3*fl12 - 1.6E+1*y2*P_k4*k3_k5*k4_l2*q_l2*Ak3*
             fl12 - 1.6E+1*y2*P_k4*k3_q*k4_k5*l2_l2*Ak3*fl12 + 1.6E+1*y2*
             P_k4*k3_q*k4_l2*k5_l2*Ak3*fl12 + 3.2E+1*y2*P_k4*k3_l2*k4_k5*
             q_l2*Ak3*fl12 - 1.6E+1*y2*P_k4*k3_l2*k4_l2*k5_q*Ak3*fl12 +
             1.6E+1*y2*P_k5*k3_k4*k4_l2*q_l2*Ak3*fl12 + 1.6E+1*y2*P_k5*k3_q
             *k4_k4*l2_l2*Ak3*fl12 - 1.6E+1*y2*P_k5*k3_q*pow(k4_l2,2)*Ak3*
             fl12 - 3.2E+1*y2*P_k5*k3_l2*k4_k4*q_l2*Ak3*fl12 + 1.6E+1*y2*
             P_k5*k3_l2*k4_q*k4_l2*Ak3*fl12 - 1.6E+1*y2*P_l2*k3_k4*k4_l2*
             k5_q*Ak3*fl12 + 1.6E+1*y2*P_l2*k3_k5*k4_q*k4_l2*Ak3*fl12 -
             1.6E+1*y2*P_l2*k3_q*k4_k4*k5_l2*Ak3*fl12 + 1.6E+1*y2*P_l2*k3_q
             *k4_k5*k4_l2*Ak3*fl12 + 3.2E+1*y2*P_l2*k3_l2*k4_k4*k5_q*Ak3*
             fl12 - 3.2E+1*y2*P_l2*k3_l2*k4_k5*k4_q*Ak3*fl12 );

          Chi(5,1) +=  + G * ( 8.E+0*y2*P_q*k4_k4*k5_q*l2_l2*Bk3*fl12 -
             8.E+0*y2*P_q*k4_k4*k5_l2*q_l2*Bk3*fl12 - 8.E+0*y2*P_q*k4_k5*
             k4_q*l2_l2*Bk3*fl12 + 8.E+0*y2*P_q*k4_k5*k4_l2*q_l2*Bk3*fl12
              + 8.E+0*y2*P_q*k4_q*k4_l2*k5_l2*Bk3*fl12 - 8.E+0*y2*P_q*pow(
             k4_l2,2)*k5_q*Bk3*fl12 );

          Chi(5,1) +=  + F * (  - 8.E+0*y2*P_k4*k4_k5*l2_l2*Bk3*fl12 +
             8.E+0*y2*P_k4*k4_l2*k5_l2*Bk3*fl12 + 8.E+0*y2*P_k5*k4_k4*l2_l2
             *Bk3*fl12 - 8.E+0*y2*P_k5*pow(k4_l2,2)*Bk3*fl12 - 8.E+0*y2*
             P_l2*k4_k4*k5_l2*Bk3*fl12 + 8.E+0*y2*P_l2*k4_k5*k4_l2*Bk3*fl12
              );

          Chi(5,1) +=  + E * ( 8.E+0*y2*k3_k4*k4_k5*l2_l2*Ak3*fl12 -
             8.E+0*y2*k3_k4*k4_l2*k5_l2*Ak3*fl12 - 8.E+0*y2*k3_k5*k4_k4*
             l2_l2*Ak3*fl12 + 8.E+0*y2*k3_k5*pow(k4_l2,2)*Ak3*fl12 + 8.E+0*
             y2*k3_l2*k4_k4*k5_l2*Ak3*fl12 - 8.E+0*y2*k3_l2*k4_k5*k4_l2*Ak3
             *fl12 );

       Chi(5,2) =
           + H * ( 1.6E+1*z2*P_k4*k4_k5*q_l2*Bk3*fl12 - 1.6E+1*z2*P_k4*
             k4_l2*k5_q*Bk3*fl12 - 1.6E+1*z2*P_k5*k4_k4*q_l2*Bk3*fl12 +
             1.6E+1*z2*P_k5*k4_q*k4_l2*Bk3*fl12 + 1.6E+1*z2*P_l2*k4_k4*k5_q
             *Bk3*fl12 - 1.6E+1*z2*P_l2*k4_k5*k4_q*Bk3*fl12 );

          Chi(5,2) +=  + G * (  - 8.E+0*z2*P_q*k3_k4*k4_k5*q_l2*Ak3*fl12
              + 8.E+0*z2*P_q*k3_k4*k4_l2*k5_q*Ak3*fl12 + 8.E+0*z2*P_q*k3_k5
             *k4_k4*q_l2*Ak3*fl12 - 8.E+0*z2*P_q*k3_k5*k4_q*k4_l2*Ak3*fl12
              - 8.E+0*z2*P_q*k3_l2*k4_k4*k5_q*Ak3*fl12 + 8.E+0*z2*P_q*k3_l2
             *k4_k5*k4_q*Ak3*fl12 );

          Chi(5,2) +=  + F * (  - 8.E+0*z2*P_k4*k3_k5*k4_l2*Ak3*fl12 +
             8.E+0*z2*P_k4*k3_l2*k4_k5*Ak3*fl12 + 8.E+0*z2*P_k5*k3_k4*k4_l2
             *Ak3*fl12 - 8.E+0*z2*P_k5*k3_l2*k4_k4*Ak3*fl12 - 8.E+0*z2*P_l2
             *k3_k4*k4_k5*Ak3*fl12 + 8.E+0*z2*P_l2*k3_k5*k4_k4*Ak3*fl12 );

       Chi(5,3) =
           + H * (  - 1.6E+1*P_k4*k3_k4*k5_k5*k5_q*Ak3*fl12*fl20 + 1.6E+1*
             P_k5*k3_k4*k4_q*k5_k5*Ak3*fl12*fl20 );

          Chi(5,3) +=  + G * ( 8.E+0*P_q*k4_k4*k5_k5*k5_q*Bk3*fl12*fl20
              - 8.E+0*P_q*k4_k5*k4_q*k5_k5*Bk3*fl12*fl20 );

          Chi(5,3) +=  + F * (  - 8.E+0*P_k4*k4_k5*k5_k5*Bk3*fl12*fl20 +
             8.E+0*P_k5*k4_k4*k5_k5*Bk3*fl12*fl20 );

          Chi(5,3) +=  + E * ( 8.E+0*k3_k4*k4_k5*k5_k5*Ak3*fl12*fl20 -
             8.E+0*k3_k5*k4_k4*k5_k5*Ak3*fl12*fl20 );

       Chi(5,4) =
           + H * (  - 8.E+0*P_k4*k4_k5*k5_k5*k5_l2*q_l2*Bk3*fl12*fl21 -
             8.E+0*P_k4*k4_l2*k5_k5*k5_q*k5_l2*Bk3*fl12*fl21 + 8.E+0*P_k5*
             k4_k4*k5_k5*k5_l2*q_l2*Bk3*fl12*fl21 + 8.E+0*P_k5*k4_q*k4_l2*
             k5_k5*k5_l2*Bk3*fl12*fl21 - 8.E+0*P_l2*k4_k4*k5_k5*k5_q*k5_l2*
             Bk3*fl12*fl21 + 8.E+0*P_l2*k4_k5*k4_q*k5_k5*k5_l2*Bk3*fl12*
             fl21 );

          Chi(5,4) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k5*k5_k5*k5_l2*q_l2*Ak3
             *fl12*fl21 + 8.E+0*P_q*k3_k4*k4_q*k5_k5*pow(k5_l2,2)*Ak3*fl12*
             fl21 - 4.E+0*P_q*k3_k4*k4_l2*k5_k5*k5_q*k5_l2*Ak3*fl12*fl21 -
             4.E+0*P_q*k3_k5*k4_k4*k5_k5*k5_l2*q_l2*Ak3*fl12*fl21 - 4.E+0*
             P_q*k3_k5*k4_q*k4_l2*k5_k5*k5_l2*Ak3*fl12*fl21 - 4.E+0*P_q*
             k3_l2*k4_k4*k5_k5*k5_q*k5_l2*Ak3*fl12*fl21 + 4.E+0*P_q*k3_l2*
             k4_k5*k4_q*k5_k5*k5_l2*Ak3*fl12*fl21 );

          Chi(5,4) +=  + F * ( 8.E+0*P_k4*k3_k4*k5_k5*pow(k5_l2,2)*Ak3*
             fl12*fl21 - 4.E+0*P_k4*k3_k5*k4_l2*k5_k5*k5_l2*Ak3*fl12*fl21
              + 4.E+0*P_k4*k3_l2*k4_k5*k5_k5*k5_l2*Ak3*fl12*fl21 - 4.E+0*
             P_k5*k3_k4*k4_l2*k5_k5*k5_l2*Ak3*fl12*fl21 - 4.E+0*P_k5*k3_l2*
             k4_k4*k5_k5*k5_l2*Ak3*fl12*fl21 + 4.E+0*P_l2*k3_k4*k4_k5*k5_k5
             *k5_l2*Ak3*fl12*fl21 - 4.E+0*P_l2*k3_k5*k4_k4*k5_k5*k5_l2*Ak3*
             fl12*fl21 );

          Chi(5,4) +=  + E * (  - 8.E+0*k4_k4*k5_k5*pow(k5_l2,2)*Bk3*fl12
             *fl21 + 8.E+0*k4_k5*k4_l2*k5_k5*k5_l2*Bk3*fl12*fl21 );

       Chi(5,5) =
           + H * (  - 1.6E+1*P_k4*k4_k5*k5_q*Bk3*fl12*fl22 + 1.6E+1*P_k5*
             k4_k5*k4_q*Bk3*fl12*fl22 );

          Chi(5,5) +=  + G * ( 8.E+0*P_q*k3_k4*k4_q*k5_k5*Ak3*fl12*fl22
              - 8.E+0*P_q*k3_k5*k4_k4*k5_q*Ak3*fl12*fl22 );

          Chi(5,5) +=  + F * ( 8.E+0*P_k4*k3_k4*k5_k5*Ak3*fl12*fl22 -
             8.E+0*P_k5*k3_k5*k4_k4*Ak3*fl12*fl22 );

          Chi(5,5) +=  + E * (  - 8.E+0*k4_k4*k5_k5*Bk3*fl12*fl22 + 8.E+0
             *pow(k4_k5,2)*Bk3*fl12*fl22 );

       Chi(5,6) =
           + H * (  - 8.E+0*P_k3*k4_k4*k5_k5*q_l2*Ak3*fl12*fl23 + 8.E+0*
             P_k3*k4_k4*k5_q*k5_l2*Ak3*fl12*fl23 - 8.E+0*P_k3*k4_k5*k4_q*
             k5_l2*Ak3*fl12*fl23 - 8.E+0*P_k3*k4_k5*k4_l2*k5_q*Ak3*fl12*
             fl23 + 8.E+0*P_k3*pow(k4_k5,2)*q_l2*Ak3*fl12*fl23 + 8.E+0*P_k3
             *k4_q*k4_l2*k5_k5*Ak3*fl12*fl23 + 1.6E+1*P_k4*k3_k4*k5_k5*q_l2
             *Ak3*fl12*fl23 - 1.6E+1*P_k4*k3_k4*k5_q*k5_l2*Ak3*fl12*fl23 -
             8.E+0*P_k4*k3_k5*k4_k5*q_l2*Ak3*fl12*fl23 + 1.6E+1*P_k4*k3_k5*
             k4_l2*k5_q*Ak3*fl12*fl23 + 8.E+0*P_k4*k3_q*k4_k5*k5_l2*Ak3*
             fl12*fl23 - 8.E+0*P_k4*k3_q*k4_l2*k5_k5*Ak3*fl12*fl23 - 8.E+0*
             P_k4*k3_l2*k4_k5*k5_q*Ak3*fl12*fl23 - 8.E+0*P_k5*k3_k4*k4_k5*
             q_l2*Ak3*fl12*fl23 + 1.6E+1*P_k5*k3_k4*k4_q*k5_l2*Ak3*fl12*
             fl23 - 1.6E+1*P_k5*k3_k5*k4_q*k4_l2*Ak3*fl12*fl23 - 8.E+0*P_k5
             *k3_q*k4_k4*k5_l2*Ak3*fl12*fl23 + 8.E+0*P_k5*k3_q*k4_k5*k4_l2*
             Ak3*fl12*fl23 + 8.E+0*P_k5*k3_l2*k4_k5*k4_q*Ak3*fl12*fl23 +
             8.E+0*P_l2*k3_k4*k4_k5*k5_q*Ak3*fl12*fl23 - 1.6E+1*P_l2*k3_k4*
             k4_q*k5_k5*Ak3*fl12*fl23 + 8.E+0*P_l2*k3_k5*k4_k5*k4_q*Ak3*
             fl12*fl23 + 8.E+0*P_l2*k3_q*k4_k4*k5_k5*Ak3*fl12*fl23 - 8.E+0*
             P_l2*k3_q*pow(k4_k5,2)*Ak3*fl12*fl23 );

          Chi(5,6) +=  + G * (  - 4.E+0*P_q*k4_k4*k5_k5*q_l2*Bk3*fl12*
             fl23 + 4.E+0*P_q*k4_k4*k5_q*k5_l2*Bk3*fl12*fl23 + 4.E+0*P_q*
             k4_k5*k4_q*k5_l2*Bk3*fl12*fl23 - 4.E+0*P_q*k4_k5*k4_l2*k5_q*
             Bk3*fl12*fl23 + 4.E+0*P_q*pow(k4_k5,2)*q_l2*Bk3*fl12*fl23 -
             4.E+0*P_q*k4_q*k4_l2*k5_k5*Bk3*fl12*fl23 );

          Chi(5,6) +=  + F * ( 4.E+0*P_k4*k4_k5*k5_l2*Bk3*fl12*fl23 -
             4.E+0*P_k4*k4_l2*k5_k5*Bk3*fl12*fl23 + 4.E+0*P_k5*k4_k4*k5_l2*
             Bk3*fl12*fl23 - 4.E+0*P_k5*k4_k5*k4_l2*Bk3*fl12*fl23 - 4.E+0*
             P_l2*k4_k4*k5_k5*Bk3*fl12*fl23 + 4.E+0*P_l2*pow(k4_k5,2)*Bk3*
             fl12*fl23 );

          Chi(5,6) +=  + E * ( 4.E+0*k3_k4*k4_k5*k5_l2*Ak3*fl12*fl23 -
             4.E+0*k3_k4*k4_l2*k5_k5*Ak3*fl12*fl23 + 4.E+0*k3_k5*k4_k4*
             k5_l2*Ak3*fl12*fl23 - 4.E+0*k3_k5*k4_k5*k4_l2*Ak3*fl12*fl23 -
             4.E+0*k3_l2*k4_k4*k5_k5*Ak3*fl12*fl23 + 4.E+0*k3_l2*pow(
             k4_k5,2)*Ak3*fl12*fl23 );

       Chi(5,7) =
           + H * (  - 8.E+0*P_k4*k4_k5*k5_k5*q_l2*Bk3*fl12*fl24 + 8.E+0*
             P_k4*k4_l2*k5_k5*k5_q*Bk3*fl12*fl24 + 8.E+0*P_k5*k4_k4*k5_k5*
             q_l2*Bk3*fl12*fl24 - 8.E+0*P_k5*k4_q*k4_l2*k5_k5*Bk3*fl12*fl24
              - 8.E+0*P_l2*k4_k4*k5_k5*k5_q*Bk3*fl12*fl24 + 8.E+0*P_l2*
             k4_k5*k4_q*k5_k5*Bk3*fl12*fl24 );

          Chi(5,7) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k5*k5_k5*q_l2*Ak3*fl12*
             fl24 - 4.E+0*P_q*k3_k4*k4_l2*k5_k5*k5_q*Ak3*fl12*fl24 - 4.E+0*
             P_q*k3_k5*k4_k4*k5_k5*q_l2*Ak3*fl12*fl24 + 4.E+0*P_q*k3_k5*
             k4_q*k4_l2*k5_k5*Ak3*fl12*fl24 + 4.E+0*P_q*k3_l2*k4_k4*k5_k5*
             k5_q*Ak3*fl12*fl24 - 4.E+0*P_q*k3_l2*k4_k5*k4_q*k5_k5*Ak3*fl12
             *fl24 );

          Chi(5,7) +=  + F * ( 4.E+0*P_k4*k3_k5*k4_l2*k5_k5*Ak3*fl12*fl24
              - 4.E+0*P_k4*k3_l2*k4_k5*k5_k5*Ak3*fl12*fl24 - 4.E+0*P_k5*
             k3_k4*k4_l2*k5_k5*Ak3*fl12*fl24 + 4.E+0*P_k5*k3_l2*k4_k4*k5_k5
             *Ak3*fl12*fl24 + 4.E+0*P_l2*k3_k4*k4_k5*k5_k5*Ak3*fl12*fl24 -
             4.E+0*P_l2*k3_k5*k4_k4*k5_k5*Ak3*fl12*fl24 );

       Chi(5,8) =
           + H * (  - 8.E+0*P_k3*k4_k4*k5_k5*k5_q*l2_l2*Ak3*fl12*fl25 +
             8.E+0*P_k3*k4_k4*k5_k5*k5_l2*q_l2*Ak3*fl12*fl25 + 8.E+0*P_k3*
             k4_k5*k4_q*k5_k5*l2_l2*Ak3*fl12*fl25 - 8.E+0*P_k3*k4_k5*k4_l2*
             k5_k5*q_l2*Ak3*fl12*fl25 - 8.E+0*P_k3*k4_q*k4_l2*k5_k5*k5_l2*
             Ak3*fl12*fl25 + 8.E+0*P_k3*pow(k4_l2,2)*k5_k5*k5_q*Ak3*fl12*
             fl25 - 8.E+0*P_k4*k3_k5*k4_l2*k5_k5*q_l2*Ak3*fl12*fl25 - 8.E+0
             *P_k4*k3_q*k4_k5*k5_k5*l2_l2*Ak3*fl12*fl25 + 8.E+0*P_k4*k3_q*
             k4_l2*k5_k5*k5_l2*Ak3*fl12*fl25 + 1.6E+1*P_k4*k3_l2*k4_k5*
             k5_k5*q_l2*Ak3*fl12*fl25 - 8.E+0*P_k4*k3_l2*k4_l2*k5_k5*k5_q*
             Ak3*fl12*fl25 + 8.E+0*P_k5*k3_k4*k4_l2*k5_k5*q_l2*Ak3*fl12*
             fl25 + 8.E+0*P_k5*k3_q*k4_k4*k5_k5*l2_l2*Ak3*fl12*fl25 - 8.E+0
             *P_k5*k3_q*pow(k4_l2,2)*k5_k5*Ak3*fl12*fl25 - 1.6E+1*P_k5*
             k3_l2*k4_k4*k5_k5*q_l2*Ak3*fl12*fl25 + 8.E+0*P_k5*k3_l2*k4_q*
             k4_l2*k5_k5*Ak3*fl12*fl25 - 8.E+0*P_l2*k3_k4*k4_l2*k5_k5*k5_q*
             Ak3*fl12*fl25 + 8.E+0*P_l2*k3_k5*k4_q*k4_l2*k5_k5*Ak3*fl12*
             fl25 - 8.E+0*P_l2*k3_q*k4_k4*k5_k5*k5_l2*Ak3*fl12*fl25 + 8.E+0
             *P_l2*k3_q*k4_k5*k4_l2*k5_k5*Ak3*fl12*fl25 + 1.6E+1*P_l2*k3_l2
             *k4_k4*k5_k5*k5_q*Ak3*fl12*fl25 - 1.6E+1*P_l2*k3_l2*k4_k5*k4_q
             *k5_k5*Ak3*fl12*fl25 );

          Chi(5,8) +=  + G * ( 4.E+0*P_q*k4_k4*k5_k5*k5_q*l2_l2*Bk3*fl12*
             fl25 - 4.E+0*P_q*k4_k4*k5_k5*k5_l2*q_l2*Bk3*fl12*fl25 - 4.E+0*
             P_q*k4_k5*k4_q*k5_k5*l2_l2*Bk3*fl12*fl25 + 4.E+0*P_q*k4_k5*
             k4_l2*k5_k5*q_l2*Bk3*fl12*fl25 + 4.E+0*P_q*k4_q*k4_l2*k5_k5*
             k5_l2*Bk3*fl12*fl25 - 4.E+0*P_q*pow(k4_l2,2)*k5_k5*k5_q*Bk3*
             fl12*fl25 );

          Chi(5,8) +=  + F * (  - 4.E+0*P_k4*k4_k5*k5_k5*l2_l2*Bk3*fl12*
             fl25 + 4.E+0*P_k4*k4_l2*k5_k5*k5_l2*Bk3*fl12*fl25 + 4.E+0*P_k5
             *k4_k4*k5_k5*l2_l2*Bk3*fl12*fl25 - 4.E+0*P_k5*pow(k4_l2,2)*
             k5_k5*Bk3*fl12*fl25 - 4.E+0*P_l2*k4_k4*k5_k5*k5_l2*Bk3*fl12*
             fl25 + 4.E+0*P_l2*k4_k5*k4_l2*k5_k5*Bk3*fl12*fl25 );

          Chi(5,8) +=  + E * ( 4.E+0*k3_k4*k4_k5*k5_k5*l2_l2*Ak3*fl12*
             fl25 - 4.E+0*k3_k4*k4_l2*k5_k5*k5_l2*Ak3*fl12*fl25 - 4.E+0*
             k3_k5*k4_k4*k5_k5*l2_l2*Ak3*fl12*fl25 + 4.E+0*k3_k5*pow(
             k4_l2,2)*k5_k5*Ak3*fl12*fl25 + 4.E+0*k3_l2*k4_k4*k5_k5*k5_l2*
             Ak3*fl12*fl25 - 4.E+0*k3_l2*k4_k5*k4_l2*k5_k5*Ak3*fl12*fl25 );

       Chi(5,9) =
           + H * (  - 8.E+0*P_k3*k4_k4*k5_k5*k5_l2*q_l2*Ak3*fl12*fl26 +
             8.E+0*P_k3*k4_k4*k5_q*pow(k5_l2,2)*Ak3*fl12*fl26 - 8.E+0*P_k3*
             k4_k5*k4_q*pow(k5_l2,2)*Ak3*fl12*fl26 - 8.E+0*P_k3*k4_k5*k4_l2
             *k5_q*k5_l2*Ak3*fl12*fl26 + 8.E+0*P_k3*pow(k4_k5,2)*k5_l2*q_l2
             *Ak3*fl12*fl26 + 8.E+0*P_k3*k4_q*k4_l2*k5_k5*k5_l2*Ak3*fl12*
             fl26 - 1.6E+1*P_k4*k3_k4*k5_q*pow(k5_l2,2)*Ak3*fl12*fl26 -
             8.E+0*P_k4*k3_k5*k4_k5*k5_l2*q_l2*Ak3*fl12*fl26 + 1.6E+1*P_k4*
             k3_k5*k4_l2*k5_q*k5_l2*Ak3*fl12*fl26 + 8.E+0*P_k4*k3_q*k4_k5*
             pow(k5_l2,2)*Ak3*fl12*fl26 - 8.E+0*P_k4*k3_q*k4_l2*k5_k5*k5_l2
             *Ak3*fl12*fl26 - 8.E+0*P_k4*k3_l2*k4_k5*k5_q*k5_l2*Ak3*fl12*
             fl26 - 8.E+0*P_k5*k3_k4*k4_k5*k5_l2*q_l2*Ak3*fl12*fl26 +
             1.6E+1*P_k5*k3_k4*k4_q*pow(k5_l2,2)*Ak3*fl12*fl26 + 1.6E+1*
             P_k5*k3_k5*k4_k4*k5_l2*q_l2*Ak3*fl12*fl26 - 1.6E+1*P_k5*k3_k5*
             k4_q*k4_l2*k5_l2*Ak3*fl12*fl26 - 8.E+0*P_k5*k3_q*k4_k4*pow(
             k5_l2,2)*Ak3*fl12*fl26 + 8.E+0*P_k5*k3_q*k4_k5*k4_l2*k5_l2*Ak3
             *fl12*fl26 + 8.E+0*P_k5*k3_l2*k4_k5*k4_q*k5_l2*Ak3*fl12*fl26
              + 8.E+0*P_l2*k3_k4*k4_k5*k5_q*k5_l2*Ak3*fl12*fl26 - 1.6E+1*
             P_l2*k3_k5*k4_k4*k5_q*k5_l2*Ak3*fl12*fl26 + 8.E+0*P_l2*k3_k5*
             k4_k5*k4_q*k5_l2*Ak3*fl12*fl26 + 8.E+0*P_l2*k3_q*k4_k4*k5_k5*
             k5_l2*Ak3*fl12*fl26 - 8.E+0*P_l2*k3_q*pow(k4_k5,2)*k5_l2*Ak3*
             fl12*fl26 );

          Chi(5,9) +=  + G * ( 4.E+0*P_q*k4_k4*k5_k5*k5_l2*q_l2*Bk3*fl12*
             fl26 + 4.E+0*P_q*k4_k4*k5_q*pow(k5_l2,2)*Bk3*fl12*fl26 - 4.E+0
             *P_q*k4_k5*k4_q*pow(k5_l2,2)*Bk3*fl12*fl26 + 4.E+0*P_q*k4_k5*
             k4_l2*k5_q*k5_l2*Bk3*fl12*fl26 - 4.E+0*P_q*pow(k4_k5,2)*k5_l2*
             q_l2*Bk3*fl12*fl26 - 4.E+0*P_q*k4_q*k4_l2*k5_k5*k5_l2*Bk3*fl12
             *fl26 );

          Chi(5,9) +=  + F * (  - 4.E+0*P_k4*k4_k5*pow(k5_l2,2)*Bk3*fl12*
             fl26 - 4.E+0*P_k4*k4_l2*k5_k5*k5_l2*Bk3*fl12*fl26 + 4.E+0*P_k5
             *k4_k4*pow(k5_l2,2)*Bk3*fl12*fl26 + 4.E+0*P_k5*k4_k5*k4_l2*
             k5_l2*Bk3*fl12*fl26 + 4.E+0*P_l2*k4_k4*k5_k5*k5_l2*Bk3*fl12*
             fl26 - 4.E+0*P_l2*pow(k4_k5,2)*k5_l2*Bk3*fl12*fl26 );

          Chi(5,9) +=  + E * ( 4.E+0*k3_k4*k4_k5*pow(k5_l2,2)*Ak3*fl12*
             fl26 + 4.E+0*k3_k4*k4_l2*k5_k5*k5_l2*Ak3*fl12*fl26 - 4.E+0*
             k3_k5*k4_k4*pow(k5_l2,2)*Ak3*fl12*fl26 - 4.E+0*k3_k5*k4_k5*
             k4_l2*k5_l2*Ak3*fl12*fl26 - 4.E+0*k3_l2*k4_k4*k5_k5*k5_l2*Ak3*
             fl12*fl26 + 4.E+0*k3_l2*pow(k4_k5,2)*k5_l2*Ak3*fl12*fl26 );

       Chi(5,10) =
           + H * (  - 8.E+0*P_k4*k4_k5*k5_q*l2_l2*Bk3*fl12*fl27 - 8.E+0*
             P_k4*k4_l2*k5_k5*q_l2*Bk3*fl12*fl27 + 8.E+0*P_k5*k4_k4*k5_l2*
             q_l2*Bk3*fl12*fl27 + 8.E+0*P_k5*k4_k5*k4_q*l2_l2*Bk3*fl12*fl27
              - 8.E+0*P_l2*k4_k4*k5_q*k5_l2*Bk3*fl12*fl27 + 8.E+0*P_l2*k4_q
             *k4_l2*k5_k5*Bk3*fl12*fl27 );

          Chi(5,10) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k5*k5_q*l2_l2*Ak3*fl12
             *fl27 + 8.E+0*P_q*k3_k4*k4_q*pow(k5_l2,2)*Ak3*fl12*fl27 +
             4.E+0*P_q*k3_k4*k4_l2*k5_k5*q_l2*Ak3*fl12*fl27 - 8.E+0*P_q*
             k3_k4*k4_l2*k5_q*k5_l2*Ak3*fl12*fl27 - 8.E+0*P_q*k3_k5*k4_k4*
             k5_q*l2_l2*Ak3*fl12*fl27 + 4.E+0*P_q*k3_k5*k4_k4*k5_l2*q_l2*
             Ak3*fl12*fl27 + 4.E+0*P_q*k3_k5*k4_k5*k4_q*l2_l2*Ak3*fl12*fl27
              - 8.E+0*P_q*k3_k5*k4_k5*k4_l2*q_l2*Ak3*fl12*fl27 - 8.E+0*P_q*
             k3_k5*k4_q*k4_l2*k5_l2*Ak3*fl12*fl27 + 8.E+0*P_q*k3_k5*pow(
             k4_l2,2)*k5_q*Ak3*fl12*fl27 + 4.E+0*P_q*k3_q*k4_k4*k5_k5*l2_l2
             *Ak3*fl12*fl27 - 4.E+0*P_q*k3_q*k4_k4*pow(k5_l2,2)*Ak3*fl12*
             fl27 + 8.E+0*P_q*k3_q*k4_k5*k4_l2*k5_l2*Ak3*fl12*fl27 - 4.E+0*
             P_q*k3_q*pow(k4_k5,2)*l2_l2*Ak3*fl12*fl27 - 4.E+0*P_q*k3_q*
             pow(k4_l2,2)*k5_k5*Ak3*fl12*fl27 - 8.E+0*P_q*k3_l2*k4_k4*k5_k5
             *q_l2*Ak3*fl12*fl27 + 4.E+0*P_q*k3_l2*k4_k4*k5_q*k5_l2*Ak3*
             fl12*fl27 - 8.E+0*P_q*k3_l2*k4_k5*k4_l2*k5_q*Ak3*fl12*fl27 +
             8.E+0*P_q*k3_l2*pow(k4_k5,2)*q_l2*Ak3*fl12*fl27 + 4.E+0*P_q*
             k3_l2*k4_q*k4_l2*k5_k5*Ak3*fl12*fl27 );

          Chi(5,10) +=  + F * ( 4.E+0*P_k3*k4_k4*k5_k5*l2_l2*Ak3*fl12*
             fl27 - 4.E+0*P_k3*k4_k4*pow(k5_l2,2)*Ak3*fl12*fl27 + 8.E+0*
             P_k3*k4_k5*k4_l2*k5_l2*Ak3*fl12*fl27 - 4.E+0*P_k3*pow(k4_k5,2)
             *l2_l2*Ak3*fl12*fl27 - 4.E+0*P_k3*pow(k4_l2,2)*k5_k5*Ak3*fl12*
             fl27 + 8.E+0*P_k4*k3_k4*pow(k5_l2,2)*Ak3*fl12*fl27 + 4.E+0*
             P_k4*k3_k5*k4_k5*l2_l2*Ak3*fl12*fl27 - 8.E+0*P_k4*k3_k5*k4_l2*
             k5_l2*Ak3*fl12*fl27 + 4.E+0*P_k4*k3_l2*k4_l2*k5_k5*Ak3*fl12*
             fl27 + 4.E+0*P_k5*k3_k4*k4_k5*l2_l2*Ak3*fl12*fl27 - 8.E+0*P_k5
             *k3_k4*k4_l2*k5_l2*Ak3*fl12*fl27 - 8.E+0*P_k5*k3_k5*k4_k4*
             l2_l2*Ak3*fl12*fl27 + 8.E+0*P_k5*k3_k5*pow(k4_l2,2)*Ak3*fl12*
             fl27 + 4.E+0*P_k5*k3_l2*k4_k4*k5_l2*Ak3*fl12*fl27 - 8.E+0*P_k5
             *k3_l2*k4_k5*k4_l2*Ak3*fl12*fl27 + 4.E+0*P_l2*k3_k4*k4_l2*
             k5_k5*Ak3*fl12*fl27 + 4.E+0*P_l2*k3_k5*k4_k4*k5_l2*Ak3*fl12*
             fl27 - 8.E+0*P_l2*k3_k5*k4_k5*k4_l2*Ak3*fl12*fl27 - 8.E+0*P_l2
             *k3_l2*k4_k4*k5_k5*Ak3*fl12*fl27 + 8.E+0*P_l2*k3_l2*pow(
             k4_k5,2)*Ak3*fl12*fl27 );

          Chi(5,10) +=  + E * (  - 4.E+0*k4_k4*k5_k5*l2_l2*Bk3*fl12*fl27
              - 4.E+0*k4_k4*pow(k5_l2,2)*Bk3*fl12*fl27 + 4.E+0*pow(k4_k5,2)
             *l2_l2*Bk3*fl12*fl27 + 4.E+0*pow(k4_l2,2)*k5_k5*Bk3*fl12*fl27
              );

       Chi(6,0) =
           + H * (  - 8.E+0*x2*P_k4*k4_k5*q_l1*Bk3*fl13 + 8.E+0*x2*P_k4*
             k4_l1*k5_q*Bk3*fl13 + 8.E+0*x2*P_k5*k4_k4*q_l1*Bk3*fl13 -
             8.E+0*x2*P_k5*k4_q*k4_l1*Bk3*fl13 - 8.E+0*x2*P_l1*k4_k4*k5_q*
             Bk3*fl13 + 8.E+0*x2*P_l1*k4_k5*k4_q*Bk3*fl13 );

          Chi(6,0) +=  + G * (  - 4.E+0*x2*P_q*k3_k4*k4_k5*q_l1*Ak3*fl13
              + 8.E+0*x2*P_q*k3_k4*k4_q*k5_l1*Ak3*fl13 - 4.E+0*x2*P_q*k3_k4
             *k4_l1*k5_q*Ak3*fl13 + 4.E+0*x2*P_q*k3_k5*k4_k4*q_l1*Ak3*fl13
              - 4.E+0*x2*P_q*k3_k5*k4_q*k4_l1*Ak3*fl13 + 4.E+0*x2*P_q*k3_l1
             *k4_k4*k5_q*Ak3*fl13 - 4.E+0*x2*P_q*k3_l1*k4_k5*k4_q*Ak3*fl13
              );

          Chi(6,0) +=  + F * ( 8.E+0*x2*P_k4*k3_k4*k5_l1*Ak3*fl13 - 4.E+0
             *x2*P_k4*k3_k5*k4_l1*Ak3*fl13 - 4.E+0*x2*P_k4*k3_l1*k4_k5*Ak3*
             fl13 - 4.E+0*x2*P_k5*k3_k4*k4_l1*Ak3*fl13 + 4.E+0*x2*P_k5*
             k3_l1*k4_k4*Ak3*fl13 - 4.E+0*x2*P_l1*k3_k4*k4_k5*Ak3*fl13 +
             4.E+0*x2*P_l1*k3_k5*k4_k4*Ak3*fl13 );

          Chi(6,0) +=  + E * ( 8.E+0*x2*k4_k4*k5_l1*Bk3*fl13 - 8.E+0*x2*
             k4_k5*k4_l1*Bk3*fl13 );

       Chi(6,1) =
           + H * (  - 1.6E+1*y2*P_k4*k4_k5*q_l2*l1_l2*Bk3*fl13 + 1.6E+1*y2*
             P_k4*k4_l2*k5_l1*q_l2*Bk3*fl13 + 1.6E+1*y2*P_k5*k4_k4*q_l2*
             l1_l2*Bk3*fl13 - 1.6E+1*y2*P_k5*k4_l1*k4_l2*q_l2*Bk3*fl13 -
             1.6E+1*y2*P_l2*k4_k4*k5_q*l1_l2*Bk3*fl13 + 1.6E+1*y2*P_l2*
             k4_k5*k4_q*l1_l2*Bk3*fl13 - 1.6E+1*y2*P_l2*k4_q*k4_l2*k5_l1*
             Bk3*fl13 + 1.6E+1*y2*P_l2*k4_l1*k4_l2*k5_q*Bk3*fl13 );

          Chi(6,1) +=  + G * (  - 8.E+0*y2*P_q*k3_k4*k4_k5*q_l2*l1_l2*Ak3
             *fl13 + 8.E+0*y2*P_q*k3_k4*k4_l2*k5_l1*q_l2*Ak3*fl13 + 8.E+0*
             y2*P_q*k3_k5*k4_k4*q_l2*l1_l2*Ak3*fl13 - 8.E+0*y2*P_q*k3_k5*
             k4_l1*k4_l2*q_l2*Ak3*fl13 + 8.E+0*y2*P_q*k3_q*k4_k4*k5_l1*
             l2_l2*Ak3*fl13 - 8.E+0*y2*P_q*k3_q*k4_k4*k5_l2*l1_l2*Ak3*fl13
              - 8.E+0*y2*P_q*k3_q*k4_k5*k4_l1*l2_l2*Ak3*fl13 + 8.E+0*y2*P_q
             *k3_q*k4_k5*k4_l2*l1_l2*Ak3*fl13 + 8.E+0*y2*P_q*k3_q*k4_l1*
             k4_l2*k5_l2*Ak3*fl13 - 8.E+0*y2*P_q*k3_q*pow(k4_l2,2)*k5_l1*
             Ak3*fl13 + 8.E+0*y2*P_q*k3_l2*k4_k4*k5_q*l1_l2*Ak3*fl13 -
             1.6E+1*y2*P_q*k3_l2*k4_k4*k5_l1*q_l2*Ak3*fl13 - 8.E+0*y2*P_q*
             k3_l2*k4_k5*k4_q*l1_l2*Ak3*fl13 + 1.6E+1*y2*P_q*k3_l2*k4_k5*
             k4_l1*q_l2*Ak3*fl13 + 8.E+0*y2*P_q*k3_l2*k4_q*k4_l2*k5_l1*Ak3*
             fl13 - 8.E+0*y2*P_q*k3_l2*k4_l1*k4_l2*k5_q*Ak3*fl13 );

          Chi(6,1) +=  + F * ( 8.E+0*y2*P_k3*k4_k4*k5_l1*l2_l2*Ak3*fl13
              - 8.E+0*y2*P_k3*k4_k4*k5_l2*l1_l2*Ak3*fl13 - 8.E+0*y2*P_k3*
             k4_k5*k4_l1*l2_l2*Ak3*fl13 + 8.E+0*y2*P_k3*k4_k5*k4_l2*l1_l2*
             Ak3*fl13 + 8.E+0*y2*P_k3*k4_l1*k4_l2*k5_l2*Ak3*fl13 - 8.E+0*y2
             *P_k3*pow(k4_l2,2)*k5_l1*Ak3*fl13 - 8.E+0*y2*P_k4*k3_l2*k4_k5*
             l1_l2*Ak3*fl13 + 8.E+0*y2*P_k4*k3_l2*k4_l2*k5_l1*Ak3*fl13 +
             8.E+0*y2*P_k5*k3_l2*k4_k4*l1_l2*Ak3*fl13 - 8.E+0*y2*P_k5*k3_l2
             *k4_l1*k4_l2*Ak3*fl13 - 8.E+0*y2*P_l2*k3_k4*k4_k5*l1_l2*Ak3*
             fl13 + 8.E+0*y2*P_l2*k3_k4*k4_l2*k5_l1*Ak3*fl13 + 8.E+0*y2*
             P_l2*k3_k5*k4_k4*l1_l2*Ak3*fl13 - 8.E+0*y2*P_l2*k3_k5*k4_l1*
             k4_l2*Ak3*fl13 - 1.6E+1*y2*P_l2*k3_l2*k4_k4*k5_l1*Ak3*fl13 +
             1.6E+1*y2*P_l2*k3_l2*k4_k5*k4_l1*Ak3*fl13 );

          Chi(6,1) +=  + E * ( 8.E+0*y2*k4_k4*k5_l1*l2_l2*Bk3*fl13 -
             8.E+0*y2*k4_k4*k5_l2*l1_l2*Bk3*fl13 - 8.E+0*y2*k4_k5*k4_l1*
             l2_l2*Bk3*fl13 + 8.E+0*y2*k4_k5*k4_l2*l1_l2*Bk3*fl13 + 8.E+0*
             y2*k4_l1*k4_l2*k5_l2*Bk3*fl13 - 8.E+0*y2*pow(k4_l2,2)*k5_l1*
             Bk3*fl13 );

       Chi(6,2) =
           + H * ( 1.6E+1*z2*P_k3*k4_k4*k5_q*l1_l2*Ak3*fl13 - 1.6E+1*z2*
             P_k3*k4_k4*k5_l1*q_l2*Ak3*fl13 - 1.6E+1*z2*P_k3*k4_k5*k4_q*
             l1_l2*Ak3*fl13 + 1.6E+1*z2*P_k3*k4_k5*k4_l1*q_l2*Ak3*fl13 +
             1.6E+1*z2*P_k3*k4_q*k4_l2*k5_l1*Ak3*fl13 - 1.6E+1*z2*P_k3*
             k4_l1*k4_l2*k5_q*Ak3*fl13 + 1.6E+1*z2*P_k4*k3_q*k4_k5*l1_l2*
             Ak3*fl13 - 1.6E+1*z2*P_k4*k3_q*k4_l2*k5_l1*Ak3*fl13 - 1.6E+1*
             z2*P_k5*k3_q*k4_k4*l1_l2*Ak3*fl13 + 1.6E+1*z2*P_k5*k3_q*k4_l1*
             k4_l2*Ak3*fl13 + 1.6E+1*z2*P_l2*k3_q*k4_k4*k5_l1*Ak3*fl13 -
             1.6E+1*z2*P_l2*k3_q*k4_k5*k4_l1*Ak3*fl13 );

          Chi(6,2) +=  + G * ( 8.E+0*z2*P_q*k4_k4*k5_q*l1_l2*Bk3*fl13 -
             8.E+0*z2*P_q*k4_k4*k5_l1*q_l2*Bk3*fl13 - 8.E+0*z2*P_q*k4_k5*
             k4_q*l1_l2*Bk3*fl13 + 8.E+0*z2*P_q*k4_k5*k4_l1*q_l2*Bk3*fl13
              + 8.E+0*z2*P_q*k4_q*k4_l2*k5_l1*Bk3*fl13 - 8.E+0*z2*P_q*k4_l1
             *k4_l2*k5_q*Bk3*fl13 );

          Chi(6,2) +=  + F * (  - 8.E+0*z2*P_k4*k4_k5*l1_l2*Bk3*fl13 +
             8.E+0*z2*P_k4*k4_l2*k5_l1*Bk3*fl13 + 8.E+0*z2*P_k5*k4_k4*l1_l2
             *Bk3*fl13 - 8.E+0*z2*P_k5*k4_l1*k4_l2*Bk3*fl13 - 8.E+0*z2*P_l2
             *k4_k4*k5_l1*Bk3*fl13 + 8.E+0*z2*P_l2*k4_k5*k4_l1*Bk3*fl13 );

          Chi(6,2) +=  + E * (  - 8.E+0*z2*k3_k4*k4_k5*l1_l2*Ak3*fl13 +
             8.E+0*z2*k3_k4*k4_l2*k5_l1*Ak3*fl13 + 8.E+0*z2*k3_k5*k4_k4*
             l1_l2*Ak3*fl13 - 8.E+0*z2*k3_k5*k4_l1*k4_l2*Ak3*fl13 - 8.E+0*
             z2*k3_l2*k4_k4*k5_l1*Ak3*fl13 + 8.E+0*z2*k3_l2*k4_k5*k4_l1*Ak3
             *fl13 );

       Chi(6,3) =
           + H * (  - 8.E+0*P_k4*k4_k5*k5_k5*q_l1*Bk3*fl13*fl20 + 8.E+0*
             P_k4*k4_l1*k5_k5*k5_q*Bk3*fl13*fl20 + 8.E+0*P_k5*k4_k4*k5_k5*
             q_l1*Bk3*fl13*fl20 - 8.E+0*P_k5*k4_q*k4_l1*k5_k5*Bk3*fl13*fl20
              - 8.E+0*P_l1*k4_k4*k5_k5*k5_q*Bk3*fl13*fl20 + 8.E+0*P_l1*
             k4_k5*k4_q*k5_k5*Bk3*fl13*fl20 );

          Chi(6,3) +=  + G * (  - 4.E+0*P_q*k3_k4*k4_k5*k5_k5*q_l1*Ak3*
             fl13*fl20 + 8.E+0*P_q*k3_k4*k4_q*k5_k5*k5_l1*Ak3*fl13*fl20 -
             4.E+0*P_q*k3_k4*k4_l1*k5_k5*k5_q*Ak3*fl13*fl20 + 4.E+0*P_q*
             k3_k5*k4_k4*k5_k5*q_l1*Ak3*fl13*fl20 - 4.E+0*P_q*k3_k5*k4_q*
             k4_l1*k5_k5*Ak3*fl13*fl20 + 4.E+0*P_q*k3_l1*k4_k4*k5_k5*k5_q*
             Ak3*fl13*fl20 - 4.E+0*P_q*k3_l1*k4_k5*k4_q*k5_k5*Ak3*fl13*fl20
              );

          Chi(6,3) +=  + F * ( 8.E+0*P_k4*k3_k4*k5_k5*k5_l1*Ak3*fl13*fl20
              - 4.E+0*P_k4*k3_k5*k4_l1*k5_k5*Ak3*fl13*fl20 - 4.E+0*P_k4*
             k3_l1*k4_k5*k5_k5*Ak3*fl13*fl20 - 4.E+0*P_k5*k3_k4*k4_l1*k5_k5
             *Ak3*fl13*fl20 + 4.E+0*P_k5*k3_l1*k4_k4*k5_k5*Ak3*fl13*fl20 -
             4.E+0*P_l1*k3_k4*k4_k5*k5_k5*Ak3*fl13*fl20 + 4.E+0*P_l1*k3_k5*
             k4_k4*k5_k5*Ak3*fl13*fl20 );

          Chi(6,3) +=  + E * ( 8.E+0*k4_k4*k5_k5*k5_l1*Bk3*fl13*fl20 -
             8.E+0*k4_k5*k4_l1*k5_k5*Bk3*fl13*fl20 );

       Chi(6,4) =
           + H * (  - 8.E+0*P_k3*k4_k4*k5_k5*k5_l1*k5_l2*q_l2*Ak3*fl13*fl21
              + 8.E+0*P_k3*k4_k4*k5_k5*pow(k5_l2,2)*q_l1*Ak3*fl13*fl21 +
             8.E+0*P_k3*k4_k5*k4_l1*k5_k5*k5_l2*q_l2*Ak3*fl13*fl21 - 8.E+0*
             P_k3*k4_k5*k4_l2*k5_k5*k5_l2*q_l1*Ak3*fl13*fl21 - 8.E+0*P_k3*
             k4_q*k4_l1*k5_k5*pow(k5_l2,2)*Ak3*fl13*fl21 + 8.E+0*P_k3*k4_q*
             k4_l2*k5_k5*k5_l1*k5_l2*Ak3*fl13*fl21 + 1.6E+1*P_k4*k3_k4*
             k5_k5*k5_l1*k5_l2*q_l2*Ak3*fl13*fl21 - 8.E+0*P_k4*k3_k5*k4_l1*
             k5_k5*k5_l2*q_l2*Ak3*fl13*fl21 + 8.E+0*P_k4*k3_q*k4_l1*k5_k5*
             pow(k5_l2,2)*Ak3*fl13*fl21 - 8.E+0*P_k4*k3_q*k4_l2*k5_k5*k5_l1
             *k5_l2*Ak3*fl13*fl21 - 8.E+0*P_k4*k3_l1*k4_k5*k5_k5*k5_l2*q_l2
             *Ak3*fl13*fl21 + 8.E+0*P_k4*k3_l2*k4_k5*k5_k5*k5_l2*q_l1*Ak3*
             fl13*fl21 - 8.E+0*P_k4*k3_l2*k4_l1*k5_k5*k5_q*k5_l2*Ak3*fl13*
             fl21 - 8.E+0*P_k5*k3_k4*k4_l1*k5_k5*k5_l2*q_l2*Ak3*fl13*fl21
              + 8.E+0*P_k5*k3_l1*k4_k4*k5_k5*k5_l2*q_l2*Ak3*fl13*fl21 -
             8.E+0*P_k5*k3_l2*k4_k4*k5_k5*k5_l2*q_l1*Ak3*fl13*fl21 + 8.E+0*
             P_k5*k3_l2*k4_q*k4_l1*k5_k5*k5_l2*Ak3*fl13*fl21 - 8.E+0*P_l1*
             k3_k4*k4_k5*k5_k5*k5_l2*q_l2*Ak3*fl13*fl21 + 8.E+0*P_l1*k3_k5*
             k4_k4*k5_k5*k5_l2*q_l2*Ak3*fl13*fl21 - 8.E+0*P_l1*k3_q*k4_k4*
             k5_k5*pow(k5_l2,2)*Ak3*fl13*fl21 + 8.E+0*P_l1*k3_q*k4_k5*k4_l2
             *k5_k5*k5_l2*Ak3*fl13*fl21 + 8.E+0*P_l1*k3_l2*k4_k4*k5_k5*k5_q
             *k5_l2*Ak3*fl13*fl21 - 8.E+0*P_l1*k3_l2*k4_k5*k4_q*k5_k5*k5_l2
             *Ak3*fl13*fl21 + 8.E+0*P_l2*k3_k4*k4_k5*k5_k5*k5_l2*q_l1*Ak3*
             fl13*fl21 - 1.6E+1*P_l2*k3_k4*k4_q*k5_k5*k5_l1*k5_l2*Ak3*fl13*
             fl21 + 8.E+0*P_l2*k3_k4*k4_l1*k5_k5*k5_q*k5_l2*Ak3*fl13*fl21
              - 8.E+0*P_l2*k3_k5*k4_k4*k5_k5*k5_l2*q_l1*Ak3*fl13*fl21 +
             8.E+0*P_l2*k3_k5*k4_q*k4_l1*k5_k5*k5_l2*Ak3*fl13*fl21 + 8.E+0*
             P_l2*k3_q*k4_k4*k5_k5*k5_l1*k5_l2*Ak3*fl13*fl21 - 8.E+0*P_l2*
             k3_q*k4_k5*k4_l1*k5_k5*k5_l2*Ak3*fl13*fl21 - 8.E+0*P_l2*k3_l1*
             k4_k4*k5_k5*k5_q*k5_l2*Ak3*fl13*fl21 + 8.E+0*P_l2*k3_l1*k4_k5*
             k4_q*k5_k5*k5_l2*Ak3*fl13*fl21 );

          Chi(6,4) +=  + G * ( 4.E+0*P_q*k4_k4*k5_k5*k5_l1*k5_l2*q_l2*Bk3
             *fl13*fl21 + 4.E+0*P_q*k4_k4*k5_k5*pow(k5_l2,2)*q_l1*Bk3*fl13*
             fl21 - 4.E+0*P_q*k4_k5*k4_l1*k5_k5*k5_l2*q_l2*Bk3*fl13*fl21 -
             4.E+0*P_q*k4_k5*k4_l2*k5_k5*k5_l2*q_l1*Bk3*fl13*fl21 - 4.E+0*
             P_q*k4_q*k4_l1*k5_k5*pow(k5_l2,2)*Bk3*fl13*fl21 + 4.E+0*P_q*
             k4_q*k4_l2*k5_k5*k5_l1*k5_l2*Bk3*fl13*fl21 );

          Chi(6,4) +=  + F * (  - 4.E+0*P_k4*k4_l1*k5_k5*pow(k5_l2,2)*Bk3
             *fl13*fl21 + 4.E+0*P_k4*k4_l2*k5_k5*k5_l1*k5_l2*Bk3*fl13*fl21
              + 4.E+0*P_l1*k4_k4*k5_k5*pow(k5_l2,2)*Bk3*fl13*fl21 - 4.E+0*
             P_l1*k4_k5*k4_l2*k5_k5*k5_l2*Bk3*fl13*fl21 + 4.E+0*P_l2*k4_k4*
             k5_k5*k5_l1*k5_l2*Bk3*fl13*fl21 - 4.E+0*P_l2*k4_k5*k4_l1*k5_k5
             *k5_l2*Bk3*fl13*fl21 );

          Chi(6,4) +=  + E * ( 4.E+0*k3_k4*k4_l1*k5_k5*pow(k5_l2,2)*Ak3*
             fl13*fl21 - 4.E+0*k3_k4*k4_l2*k5_k5*k5_l1*k5_l2*Ak3*fl13*fl21
              - 4.E+0*k3_l1*k4_k4*k5_k5*pow(k5_l2,2)*Ak3*fl13*fl21 + 4.E+0*
             k3_l1*k4_k5*k4_l2*k5_k5*k5_l2*Ak3*fl13*fl21 - 4.E+0*k3_l2*
             k4_k4*k5_k5*k5_l1*k5_l2*Ak3*fl13*fl21 + 4.E+0*k3_l2*k4_k5*
             k4_l1*k5_k5*k5_l2*Ak3*fl13*fl21 );

       Chi(6,5) =
           + H * ( 8.E+0*P_k3*k4_k4*k5_k5*q_l1*Ak3*fl13*fl22 - 8.E+0*P_k3*
             k4_k4*k5_q*k5_l1*Ak3*fl13*fl22 + 8.E+0*P_k3*k4_k5*k4_q*k5_l1*
             Ak3*fl13*fl22 + 8.E+0*P_k3*k4_k5*k4_l1*k5_q*Ak3*fl13*fl22 -
             8.E+0*P_k3*pow(k4_k5,2)*q_l1*Ak3*fl13*fl22 - 8.E+0*P_k3*k4_q*
             k4_l1*k5_k5*Ak3*fl13*fl22 + 1.6E+1*P_k4*k3_k4*k5_q*k5_l1*Ak3*
             fl13*fl22 + 8.E+0*P_k4*k3_k5*k4_k5*q_l1*Ak3*fl13*fl22 - 1.6E+1
             *P_k4*k3_k5*k4_l1*k5_q*Ak3*fl13*fl22 - 8.E+0*P_k4*k3_q*k4_k5*
             k5_l1*Ak3*fl13*fl22 + 8.E+0*P_k4*k3_q*k4_l1*k5_k5*Ak3*fl13*
             fl22 - 8.E+0*P_k4*k3_l1*k4_k5*k5_q*Ak3*fl13*fl22 + 8.E+0*P_k5*
             k3_k4*k4_k5*q_l1*Ak3*fl13*fl22 - 1.6E+1*P_k5*k3_k4*k4_q*k5_l1*
             Ak3*fl13*fl22 - 1.6E+1*P_k5*k3_k5*k4_k4*q_l1*Ak3*fl13*fl22 +
             1.6E+1*P_k5*k3_k5*k4_q*k4_l1*Ak3*fl13*fl22 + 8.E+0*P_k5*k3_q*
             k4_k4*k5_l1*Ak3*fl13*fl22 - 8.E+0*P_k5*k3_q*k4_k5*k4_l1*Ak3*
             fl13*fl22 + 8.E+0*P_k5*k3_l1*k4_k5*k4_q*Ak3*fl13*fl22 - 8.E+0*
             P_l1*k3_k4*k4_k5*k5_q*Ak3*fl13*fl22 + 1.6E+1*P_l1*k3_k5*k4_k4*
             k5_q*Ak3*fl13*fl22 - 8.E+0*P_l1*k3_k5*k4_k5*k4_q*Ak3*fl13*fl22
              - 8.E+0*P_l1*k3_q*k4_k4*k5_k5*Ak3*fl13*fl22 + 8.E+0*P_l1*k3_q
             *pow(k4_k5,2)*Ak3*fl13*fl22 );

          Chi(6,5) +=  + G * ( 4.E+0*P_q*k4_k4*k5_k5*q_l1*Bk3*fl13*fl22
              + 4.E+0*P_q*k4_k4*k5_q*k5_l1*Bk3*fl13*fl22 + 4.E+0*P_q*k4_k5*
             k4_q*k5_l1*Bk3*fl13*fl22 - 4.E+0*P_q*k4_k5*k4_l1*k5_q*Bk3*fl13
             *fl22 - 4.E+0*P_q*pow(k4_k5,2)*q_l1*Bk3*fl13*fl22 - 4.E+0*P_q*
             k4_q*k4_l1*k5_k5*Bk3*fl13*fl22 );

          Chi(6,5) +=  + F * ( 4.E+0*P_k4*k4_k5*k5_l1*Bk3*fl13*fl22 -
             4.E+0*P_k4*k4_l1*k5_k5*Bk3*fl13*fl22 + 4.E+0*P_k5*k4_k4*k5_l1*
             Bk3*fl13*fl22 - 4.E+0*P_k5*k4_k5*k4_l1*Bk3*fl13*fl22 + 4.E+0*
             P_l1*k4_k4*k5_k5*Bk3*fl13*fl22 - 4.E+0*P_l1*pow(k4_k5,2)*Bk3*
             fl13*fl22 );

          Chi(6,5) +=  + E * (  - 4.E+0*k3_k4*k4_k5*k5_l1*Ak3*fl13*fl22
              + 4.E+0*k3_k4*k4_l1*k5_k5*Ak3*fl13*fl22 - 4.E+0*k3_k5*k4_k4*
             k5_l1*Ak3*fl13*fl22 + 4.E+0*k3_k5*k4_k5*k4_l1*Ak3*fl13*fl22 -
             4.E+0*k3_l1*k4_k4*k5_k5*Ak3*fl13*fl22 + 4.E+0*k3_l1*pow(
             k4_k5,2)*Ak3*fl13*fl22 );

       Chi(6,6) =
           + H * ( 8.E+0*P_k4*k4_k5*k5_l1*q_l2*Bk3*fl13*fl23 - 8.E+0*P_k4*
             k4_l1*k5_k5*q_l2*Bk3*fl13*fl23 + 8.E+0*P_k4*k4_l1*k5_q*k5_l2*
             Bk3*fl13*fl23 - 8.E+0*P_k4*k4_l2*k5_q*k5_l1*Bk3*fl13*fl23 +
             8.E+0*P_k5*k4_k4*k5_l2*q_l1*Bk3*fl13*fl23 - 8.E+0*P_k5*k4_k5*
             k4_l2*q_l1*Bk3*fl13*fl23 - 8.E+0*P_k5*k4_q*k4_l1*k5_l2*Bk3*
             fl13*fl23 + 8.E+0*P_k5*k4_q*k4_l2*k5_l1*Bk3*fl13*fl23 + 8.E+0*
             P_l1*k4_k4*k5_k5*q_l2*Bk3*fl13*fl23 - 8.E+0*P_l1*k4_k4*k5_q*
             k5_l2*Bk3*fl13*fl23 + 8.E+0*P_l1*k4_k5*k4_l2*k5_q*Bk3*fl13*
             fl23 - 8.E+0*P_l1*pow(k4_k5,2)*q_l2*Bk3*fl13*fl23 - 8.E+0*P_l2
             *k4_k4*k5_k5*q_l1*Bk3*fl13*fl23 - 8.E+0*P_l2*k4_k5*k4_q*k5_l1*
             Bk3*fl13*fl23 + 8.E+0*P_l2*pow(k4_k5,2)*q_l1*Bk3*fl13*fl23 +
             8.E+0*P_l2*k4_q*k4_l1*k5_k5*Bk3*fl13*fl23 );

          Chi(6,6) +=  + G * (  - 4.E+0*P_q*k3_k4*k4_k5*k5_l1*q_l2*Ak3*
             fl13*fl23 + 4.E+0*P_q*k3_k4*k4_l1*k5_k5*q_l2*Ak3*fl13*fl23 -
             4.E+0*P_q*k3_k4*k4_l1*k5_q*k5_l2*Ak3*fl13*fl23 + 4.E+0*P_q*
             k3_k4*k4_l2*k5_q*k5_l1*Ak3*fl13*fl23 - 4.E+0*P_q*k3_k5*k4_k4*
             k5_l2*q_l1*Ak3*fl13*fl23 + 4.E+0*P_q*k3_k5*k4_k5*k4_l2*q_l1*
             Ak3*fl13*fl23 + 4.E+0*P_q*k3_k5*k4_q*k4_l1*k5_l2*Ak3*fl13*fl23
              - 4.E+0*P_q*k3_k5*k4_q*k4_l2*k5_l1*Ak3*fl13*fl23 - 4.E+0*P_q*
             k3_l1*k4_k4*k5_k5*q_l2*Ak3*fl13*fl23 + 4.E+0*P_q*k3_l1*k4_k4*
             k5_q*k5_l2*Ak3*fl13*fl23 - 4.E+0*P_q*k3_l1*k4_k5*k4_l2*k5_q*
             Ak3*fl13*fl23 + 4.E+0*P_q*k3_l1*pow(k4_k5,2)*q_l2*Ak3*fl13*
             fl23 + 4.E+0*P_q*k3_l2*k4_k4*k5_k5*q_l1*Ak3*fl13*fl23 + 4.E+0*
             P_q*k3_l2*k4_k5*k4_q*k5_l1*Ak3*fl13*fl23 - 4.E+0*P_q*k3_l2*
             pow(k4_k5,2)*q_l1*Ak3*fl13*fl23 - 4.E+0*P_q*k3_l2*k4_q*k4_l1*
             k5_k5*Ak3*fl13*fl23 );

          Chi(6,6) +=  + F * ( 4.E+0*P_k4*k3_k5*k4_l1*k5_l2*Ak3*fl13*fl23
              - 4.E+0*P_k4*k3_k5*k4_l2*k5_l1*Ak3*fl13*fl23 + 4.E+0*P_k4*
             k3_l2*k4_k5*k5_l1*Ak3*fl13*fl23 - 4.E+0*P_k4*k3_l2*k4_l1*k5_k5
             *Ak3*fl13*fl23 - 4.E+0*P_k5*k3_k4*k4_l1*k5_l2*Ak3*fl13*fl23 +
             4.E+0*P_k5*k3_k4*k4_l2*k5_l1*Ak3*fl13*fl23 + 4.E+0*P_k5*k3_l1*
             k4_k4*k5_l2*Ak3*fl13*fl23 - 4.E+0*P_k5*k3_l1*k4_k5*k4_l2*Ak3*
             fl13*fl23 - 4.E+0*P_l1*k3_k5*k4_k4*k5_l2*Ak3*fl13*fl23 + 4.E+0
             *P_l1*k3_k5*k4_k5*k4_l2*Ak3*fl13*fl23 + 4.E+0*P_l1*k3_l2*k4_k4
             *k5_k5*Ak3*fl13*fl23 - 4.E+0*P_l1*k3_l2*pow(k4_k5,2)*Ak3*fl13*
             fl23 - 4.E+0*P_l2*k3_k4*k4_k5*k5_l1*Ak3*fl13*fl23 + 4.E+0*P_l2
             *k3_k4*k4_l1*k5_k5*Ak3*fl13*fl23 - 4.E+0*P_l2*k3_l1*k4_k4*
             k5_k5*Ak3*fl13*fl23 + 4.E+0*P_l2*k3_l1*pow(k4_k5,2)*Ak3*fl13*
             fl23 );

       Chi(6,7) =
           + H * (  - 8.E+0*P_k3*k4_k4*k5_k5*k5_q*l1_l2*Ak3*fl13*fl24 +
             8.E+0*P_k3*k4_k4*k5_k5*k5_l1*q_l2*Ak3*fl13*fl24 + 8.E+0*P_k3*
             k4_k5*k4_q*k5_k5*l1_l2*Ak3*fl13*fl24 - 8.E+0*P_k3*k4_k5*k4_l1*
             k5_k5*q_l2*Ak3*fl13*fl24 - 8.E+0*P_k3*k4_q*k4_l2*k5_k5*k5_l1*
             Ak3*fl13*fl24 + 8.E+0*P_k3*k4_l1*k4_l2*k5_k5*k5_q*Ak3*fl13*
             fl24 - 8.E+0*P_k4*k3_q*k4_k5*k5_k5*l1_l2*Ak3*fl13*fl24 + 8.E+0
             *P_k4*k3_q*k4_l2*k5_k5*k5_l1*Ak3*fl13*fl24 + 8.E+0*P_k5*k3_q*
             k4_k4*k5_k5*l1_l2*Ak3*fl13*fl24 - 8.E+0*P_k5*k3_q*k4_l1*k4_l2*
             k5_k5*Ak3*fl13*fl24 - 8.E+0*P_l2*k3_q*k4_k4*k5_k5*k5_l1*Ak3*
             fl13*fl24 + 8.E+0*P_l2*k3_q*k4_k5*k4_l1*k5_k5*Ak3*fl13*fl24 );

          Chi(6,7) +=  + G * (  - 4.E+0*P_q*k4_k4*k5_k5*k5_q*l1_l2*Bk3*
             fl13*fl24 + 4.E+0*P_q*k4_k4*k5_k5*k5_l1*q_l2*Bk3*fl13*fl24 +
             4.E+0*P_q*k4_k5*k4_q*k5_k5*l1_l2*Bk3*fl13*fl24 - 4.E+0*P_q*
             k4_k5*k4_l1*k5_k5*q_l2*Bk3*fl13*fl24 - 4.E+0*P_q*k4_q*k4_l2*
             k5_k5*k5_l1*Bk3*fl13*fl24 + 4.E+0*P_q*k4_l1*k4_l2*k5_k5*k5_q*
             Bk3*fl13*fl24 );

          Chi(6,7) +=  + F * ( 4.E+0*P_k4*k4_k5*k5_k5*l1_l2*Bk3*fl13*fl24
              - 4.E+0*P_k4*k4_l2*k5_k5*k5_l1*Bk3*fl13*fl24 - 4.E+0*P_k5*
             k4_k4*k5_k5*l1_l2*Bk3*fl13*fl24 + 4.E+0*P_k5*k4_l1*k4_l2*k5_k5
             *Bk3*fl13*fl24 + 4.E+0*P_l2*k4_k4*k5_k5*k5_l1*Bk3*fl13*fl24 -
             4.E+0*P_l2*k4_k5*k4_l1*k5_k5*Bk3*fl13*fl24 );

          Chi(6,7) +=  + E * ( 4.E+0*k3_k4*k4_k5*k5_k5*l1_l2*Ak3*fl13*
             fl24 - 4.E+0*k3_k4*k4_l2*k5_k5*k5_l1*Ak3*fl13*fl24 - 4.E+0*
             k3_k5*k4_k4*k5_k5*l1_l2*Ak3*fl13*fl24 + 4.E+0*k3_k5*k4_l1*
             k4_l2*k5_k5*Ak3*fl13*fl24 + 4.E+0*k3_l2*k4_k4*k5_k5*k5_l1*Ak3*
             fl13*fl24 - 4.E+0*k3_l2*k4_k5*k4_l1*k5_k5*Ak3*fl13*fl24 );

       Chi(6,8) =
           + H * (  - 8.E+0*P_k4*k4_k5*k5_k5*q_l2*l1_l2*Bk3*fl13*fl25 +
             8.E+0*P_k4*k4_l2*k5_k5*k5_l1*q_l2*Bk3*fl13*fl25 + 8.E+0*P_k5*
             k4_k4*k5_k5*q_l2*l1_l2*Bk3*fl13*fl25 - 8.E+0*P_k5*k4_l1*k4_l2*
             k5_k5*q_l2*Bk3*fl13*fl25 - 8.E+0*P_l2*k4_k4*k5_k5*k5_q*l1_l2*
             Bk3*fl13*fl25 + 8.E+0*P_l2*k4_k5*k4_q*k5_k5*l1_l2*Bk3*fl13*
             fl25 - 8.E+0*P_l2*k4_q*k4_l2*k5_k5*k5_l1*Bk3*fl13*fl25 + 8.E+0
             *P_l2*k4_l1*k4_l2*k5_k5*k5_q*Bk3*fl13*fl25 );

          Chi(6,8) +=  + G * (  - 4.E+0*P_q*k3_k4*k4_k5*k5_k5*q_l2*l1_l2*
             Ak3*fl13*fl25 + 4.E+0*P_q*k3_k4*k4_l2*k5_k5*k5_l1*q_l2*Ak3*
             fl13*fl25 + 4.E+0*P_q*k3_k5*k4_k4*k5_k5*q_l2*l1_l2*Ak3*fl13*
             fl25 - 4.E+0*P_q*k3_k5*k4_l1*k4_l2*k5_k5*q_l2*Ak3*fl13*fl25 +
             4.E+0*P_q*k3_q*k4_k4*k5_k5*k5_l1*l2_l2*Ak3*fl13*fl25 - 4.E+0*
             P_q*k3_q*k4_k4*k5_k5*k5_l2*l1_l2*Ak3*fl13*fl25 - 4.E+0*P_q*
             k3_q*k4_k5*k4_l1*k5_k5*l2_l2*Ak3*fl13*fl25 + 4.E+0*P_q*k3_q*
             k4_k5*k4_l2*k5_k5*l1_l2*Ak3*fl13*fl25 + 4.E+0*P_q*k3_q*k4_l1*
             k4_l2*k5_k5*k5_l2*Ak3*fl13*fl25 - 4.E+0*P_q*k3_q*pow(k4_l2,2)*
             k5_k5*k5_l1*Ak3*fl13*fl25 + 4.E+0*P_q*k3_l2*k4_k4*k5_k5*k5_q*
             l1_l2*Ak3*fl13*fl25 - 8.E+0*P_q*k3_l2*k4_k4*k5_k5*k5_l1*q_l2*
             Ak3*fl13*fl25 - 4.E+0*P_q*k3_l2*k4_k5*k4_q*k5_k5*l1_l2*Ak3*
             fl13*fl25 + 8.E+0*P_q*k3_l2*k4_k5*k4_l1*k5_k5*q_l2*Ak3*fl13*
             fl25 + 4.E+0*P_q*k3_l2*k4_q*k4_l2*k5_k5*k5_l1*Ak3*fl13*fl25 -
             4.E+0*P_q*k3_l2*k4_l1*k4_l2*k5_k5*k5_q*Ak3*fl13*fl25 );

          Chi(6,8) +=  + F * ( 4.E+0*P_k3*k4_k4*k5_k5*k5_l1*l2_l2*Ak3*
             fl13*fl25 - 4.E+0*P_k3*k4_k4*k5_k5*k5_l2*l1_l2*Ak3*fl13*fl25
              - 4.E+0*P_k3*k4_k5*k4_l1*k5_k5*l2_l2*Ak3*fl13*fl25 + 4.E+0*
             P_k3*k4_k5*k4_l2*k5_k5*l1_l2*Ak3*fl13*fl25 + 4.E+0*P_k3*k4_l1*
             k4_l2*k5_k5*k5_l2*Ak3*fl13*fl25 - 4.E+0*P_k3*pow(k4_l2,2)*
             k5_k5*k5_l1*Ak3*fl13*fl25 - 4.E+0*P_k4*k3_l2*k4_k5*k5_k5*l1_l2
             *Ak3*fl13*fl25 + 4.E+0*P_k4*k3_l2*k4_l2*k5_k5*k5_l1*Ak3*fl13*
             fl25 + 4.E+0*P_k5*k3_l2*k4_k4*k5_k5*l1_l2*Ak3*fl13*fl25 -
             4.E+0*P_k5*k3_l2*k4_l1*k4_l2*k5_k5*Ak3*fl13*fl25 - 4.E+0*P_l2*
             k3_k4*k4_k5*k5_k5*l1_l2*Ak3*fl13*fl25 + 4.E+0*P_l2*k3_k4*k4_l2
             *k5_k5*k5_l1*Ak3*fl13*fl25 + 4.E+0*P_l2*k3_k5*k4_k4*k5_k5*
             l1_l2*Ak3*fl13*fl25 - 4.E+0*P_l2*k3_k5*k4_l1*k4_l2*k5_k5*Ak3*
             fl13*fl25 - 8.E+0*P_l2*k3_l2*k4_k4*k5_k5*k5_l1*Ak3*fl13*fl25
              + 8.E+0*P_l2*k3_l2*k4_k5*k4_l1*k5_k5*Ak3*fl13*fl25 );

          Chi(6,8) +=  + E * ( 4.E+0*k4_k4*k5_k5*k5_l1*l2_l2*Bk3*fl13*
             fl25 - 4.E+0*k4_k4*k5_k5*k5_l2*l1_l2*Bk3*fl13*fl25 - 4.E+0*
             k4_k5*k4_l1*k5_k5*l2_l2*Bk3*fl13*fl25 + 4.E+0*k4_k5*k4_l2*
             k5_k5*l1_l2*Bk3*fl13*fl25 + 4.E+0*k4_l1*k4_l2*k5_k5*k5_l2*Bk3*
             fl13*fl25 - 4.E+0*pow(k4_l2,2)*k5_k5*k5_l1*Bk3*fl13*fl25 );

       Chi(6,9) =
           + H * ( 8.E+0*P_k4*k4_k5*k5_q*k5_l2*l1_l2*Bk3*fl13*fl26 - 8.E+0*
             P_k4*k4_k5*pow(k5_l2,2)*q_l1*Bk3*fl13*fl26 + 8.E+0*P_k4*k4_l1*
             k5_q*pow(k5_l2,2)*Bk3*fl13*fl26 - 8.E+0*P_k4*k4_l2*k5_q*k5_l1*
             k5_l2*Bk3*fl13*fl26 - 8.E+0*P_k5*k4_k4*k5_l1*k5_l2*q_l2*Bk3*
             fl13*fl26 + 8.E+0*P_k5*k4_k4*pow(k5_l2,2)*q_l1*Bk3*fl13*fl26
              - 8.E+0*P_k5*k4_k5*k4_q*k5_l2*l1_l2*Bk3*fl13*fl26 + 8.E+0*
             P_k5*k4_k5*k4_l1*k5_l2*q_l2*Bk3*fl13*fl26 - 8.E+0*P_k5*k4_q*
             k4_l1*pow(k5_l2,2)*Bk3*fl13*fl26 + 8.E+0*P_k5*k4_q*k4_l2*k5_l1
             *k5_l2*Bk3*fl13*fl26 - 8.E+0*P_l1*k4_k4*k5_q*pow(k5_l2,2)*Bk3*
             fl13*fl26 + 8.E+0*P_l1*k4_k5*k4_q*pow(k5_l2,2)*Bk3*fl13*fl26
              + 8.E+0*P_l2*k4_k4*k5_q*k5_l1*k5_l2*Bk3*fl13*fl26 - 8.E+0*
             P_l2*k4_k5*k4_l1*k5_q*k5_l2*Bk3*fl13*fl26 );

          Chi(6,9) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k5*k5_q*k5_l2*l1_l2*Ak3
             *fl13*fl26 - 4.E+0*P_q*k3_k4*k4_k5*pow(k5_l2,2)*q_l1*Ak3*fl13*
             fl26 + 8.E+0*P_q*k3_k4*k4_q*k5_l1*pow(k5_l2,2)*Ak3*fl13*fl26
              - 4.E+0*P_q*k3_k4*k4_l1*k5_q*pow(k5_l2,2)*Ak3*fl13*fl26 -
             4.E+0*P_q*k3_k4*k4_l2*k5_q*k5_l1*k5_l2*Ak3*fl13*fl26 - 8.E+0*
             P_q*k3_k5*k4_k4*k5_q*k5_l2*l1_l2*Ak3*fl13*fl26 + 4.E+0*P_q*
             k3_k5*k4_k4*k5_l1*k5_l2*q_l2*Ak3*fl13*fl26 + 4.E+0*P_q*k3_k5*
             k4_k4*pow(k5_l2,2)*q_l1*Ak3*fl13*fl26 + 4.E+0*P_q*k3_k5*k4_k5*
             k4_q*k5_l2*l1_l2*Ak3*fl13*fl26 - 4.E+0*P_q*k3_k5*k4_k5*k4_l1*
             k5_l2*q_l2*Ak3*fl13*fl26 - 4.E+0*P_q*k3_k5*k4_q*k4_l1*pow(
             k5_l2,2)*Ak3*fl13*fl26 - 4.E+0*P_q*k3_k5*k4_q*k4_l2*k5_l1*
             k5_l2*Ak3*fl13*fl26 + 8.E+0*P_q*k3_k5*k4_l1*k4_l2*k5_q*k5_l2*
             Ak3*fl13*fl26 + 4.E+0*P_q*k3_q*k4_k4*k5_k5*k5_l2*l1_l2*Ak3*
             fl13*fl26 - 4.E+0*P_q*k3_q*k4_k4*k5_l1*pow(k5_l2,2)*Ak3*fl13*
             fl26 + 4.E+0*P_q*k3_q*k4_k5*k4_l1*pow(k5_l2,2)*Ak3*fl13*fl26
              + 4.E+0*P_q*k3_q*k4_k5*k4_l2*k5_l1*k5_l2*Ak3*fl13*fl26 -
             4.E+0*P_q*k3_q*pow(k4_k5,2)*k5_l2*l1_l2*Ak3*fl13*fl26 - 4.E+0*
             P_q*k3_q*k4_l1*k4_l2*k5_k5*k5_l2*Ak3*fl13*fl26 + 4.E+0*P_q*
             k3_l1*k4_k4*k5_q*pow(k5_l2,2)*Ak3*fl13*fl26 - 4.E+0*P_q*k3_l1*
             k4_k5*k4_q*pow(k5_l2,2)*Ak3*fl13*fl26 + 4.E+0*P_q*k3_l2*k4_k4*
             k5_q*k5_l1*k5_l2*Ak3*fl13*fl26 - 4.E+0*P_q*k3_l2*k4_k5*k4_l1*
             k5_q*k5_l2*Ak3*fl13*fl26 );

          Chi(6,9) +=  + F * ( 4.E+0*P_k3*k4_k4*k5_k5*k5_l2*l1_l2*Ak3*
             fl13*fl26 - 4.E+0*P_k3*k4_k4*k5_l1*pow(k5_l2,2)*Ak3*fl13*fl26
              + 4.E+0*P_k3*k4_k5*k4_l1*pow(k5_l2,2)*Ak3*fl13*fl26 + 4.E+0*
             P_k3*k4_k5*k4_l2*k5_l1*k5_l2*Ak3*fl13*fl26 - 4.E+0*P_k3*pow(
             k4_k5,2)*k5_l2*l1_l2*Ak3*fl13*fl26 - 4.E+0*P_k3*k4_l1*k4_l2*
             k5_k5*k5_l2*Ak3*fl13*fl26 + 8.E+0*P_k4*k3_k4*k5_l1*pow(
             k5_l2,2)*Ak3*fl13*fl26 + 4.E+0*P_k4*k3_k5*k4_k5*k5_l2*l1_l2*
             Ak3*fl13*fl26 - 4.E+0*P_k4*k3_k5*k4_l1*pow(k5_l2,2)*Ak3*fl13*
             fl26 - 4.E+0*P_k4*k3_k5*k4_l2*k5_l1*k5_l2*Ak3*fl13*fl26 -
             4.E+0*P_k4*k3_l1*k4_k5*pow(k5_l2,2)*Ak3*fl13*fl26 + 4.E+0*P_k5
             *k3_k4*k4_k5*k5_l2*l1_l2*Ak3*fl13*fl26 - 4.E+0*P_k5*k3_k4*
             k4_l1*pow(k5_l2,2)*Ak3*fl13*fl26 - 4.E+0*P_k5*k3_k4*k4_l2*
             k5_l1*k5_l2*Ak3*fl13*fl26 - 8.E+0*P_k5*k3_k5*k4_k4*k5_l2*l1_l2
             *Ak3*fl13*fl26 + 8.E+0*P_k5*k3_k5*k4_l1*k4_l2*k5_l2*Ak3*fl13*
             fl26 + 4.E+0*P_k5*k3_l1*k4_k4*pow(k5_l2,2)*Ak3*fl13*fl26 +
             4.E+0*P_k5*k3_l2*k4_k4*k5_l1*k5_l2*Ak3*fl13*fl26 - 4.E+0*P_k5*
             k3_l2*k4_k5*k4_l1*k5_l2*Ak3*fl13*fl26 - 4.E+0*P_l1*k3_k4*k4_k5
             *pow(k5_l2,2)*Ak3*fl13*fl26 + 4.E+0*P_l1*k3_k5*k4_k4*pow(
             k5_l2,2)*Ak3*fl13*fl26 + 4.E+0*P_l2*k3_k5*k4_k4*k5_l1*k5_l2*
             Ak3*fl13*fl26 - 4.E+0*P_l2*k3_k5*k4_k5*k4_l1*k5_l2*Ak3*fl13*
             fl26 );

          Chi(6,9) +=  + E * ( 4.E+0*k4_k4*k5_k5*k5_l2*l1_l2*Bk3*fl13*
             fl26 + 4.E+0*k4_k4*k5_l1*pow(k5_l2,2)*Bk3*fl13*fl26 - 4.E+0*
             k4_k5*k4_l1*pow(k5_l2,2)*Bk3*fl13*fl26 + 4.E+0*k4_k5*k4_l2*
             k5_l1*k5_l2*Bk3*fl13*fl26 - 4.E+0*pow(k4_k5,2)*k5_l2*l1_l2*Bk3
             *fl13*fl26 - 4.E+0*k4_l1*k4_l2*k5_k5*k5_l2*Bk3*fl13*fl26 );

       Chi(6,10) =
           + H * ( 3.333333333E+0*P_k3*k4_k4*k5_k5*q_l1*l2_l2*Ak3*fl13*fl27
              + 4.666666666E+0*P_k3*k4_k4*k5_k5*q_l2*l1_l2*Ak3*fl13*fl27 +
             4.666666666E+0*P_k3*k4_k4*k5_q*k5_l1*l2_l2*Ak3*fl13*fl27 -
             4.666666666E+0*P_k3*k4_k4*k5_q*k5_l2*l1_l2*Ak3*fl13*fl27 -
             1.266666666E+1*P_k3*k4_k4*k5_l1*k5_l2*q_l2*Ak3*fl13*fl27 +
             4.666666666E+0*P_k3*k4_k4*pow(k5_l2,2)*q_l1*Ak3*fl13*fl27 +
             3.333333333E+0*P_k3*k4_k5*k4_q*k5_l1*l2_l2*Ak3*fl13*fl27 -
             3.333333333E+0*P_k3*k4_k5*k4_q*k5_l2*l1_l2*Ak3*fl13*fl27 -
             4.666666666E+0*P_k3*k4_k5*k4_l1*k5_q*l2_l2*Ak3*fl13*fl27 +
             1.266666666E+1*P_k3*k4_k5*k4_l1*k5_l2*q_l2*Ak3*fl13*fl27 +
             4.666666666E+0*P_k3*k4_k5*k4_l2*k5_q*l1_l2*Ak3*fl13*fl27 +
             4.666666666E+0*P_k3*k4_k5*k4_l2*k5_l1*q_l2*Ak3*fl13*fl27 -
             1.333333333E+0*P_k3*k4_k5*k4_l2*k5_l2*q_l1*Ak3*fl13*fl27 -
             3.333333333E+0*P_k3*pow(k4_k5,2)*q_l1*l2_l2*Ak3*fl13*fl27 -
             4.666666666E+0*P_k3*pow(k4_k5,2)*q_l2*l1_l2*Ak3*fl13*fl27 -
             3.333333333E+0*P_k3*k4_q*k4_l1*k5_k5*l2_l2*Ak3*fl13*fl27 -
             4.666666666E+0*P_k3*k4_q*k4_l1*pow(k5_l2,2)*Ak3*fl13*fl27 +
             3.333333333E+0*P_k3*k4_q*k4_l2*k5_k5*l1_l2*Ak3*fl13*fl27 +
             4.666666666E+0*P_k3*k4_q*k4_l2*k5_l1*k5_l2*Ak3*fl13*fl27 -
             4.666666666E+0*P_k3*k4_l1*k4_l2*k5_k5*q_l2*Ak3*fl13*fl27 +
             4.666666666E+0*P_k3*k4_l1*k4_l2*k5_q*k5_l2*Ak3*fl13*fl27 -
             3.333333333E+0*P_k3*pow(k4_l2,2)*k5_k5*q_l1*Ak3*fl13*fl27 -
             4.666666666E+0*P_k3*pow(k4_l2,2)*k5_q*k5_l1*Ak3*fl13*fl27 -
             3.333333333E+0*P_k4*k3_k4*k5_k5*q_l1*l2_l2*Ak3*fl13*fl27 +
             3.333333333E+0*P_k4*k3_k4*k5_k5*q_l2*l1_l2*Ak3*fl13*fl27 +
             3.333333333E+0*P_k4*k3_k4*k5_q*k5_l1*l2_l2*Ak3*fl13*fl27 -
             3.333333333E+0*P_k4*k3_k4*k5_q*k5_l2*l1_l2*Ak3*fl13*fl27 +
             1.266666666E+1*P_k4*k3_k4*k5_l1*k5_l2*q_l2*Ak3*fl13*fl27 +
             3.333333333E+0*P_k4*k3_k4*pow(k5_l2,2)*q_l1*Ak3*fl13*fl27 +
             3.333333333E+0*P_k4*k3_k5*k4_k5*q_l1*l2_l2*Ak3*fl13*fl27 +
             4.666666666E+0*P_k4*k3_k5*k4_k5*q_l2*l1_l2*Ak3*fl13*fl27 -
             1.333333333E+0*P_k4*k3_k5*k4_q*k5_l1*l2_l2*Ak3*fl13*fl27 +
             1.333333333E+0*P_k4*k3_k5*k4_q*k5_l2*l1_l2*Ak3*fl13*fl27 -
             2.E+0*P_k4*k3_k5*k4_l1*k5_q*l2_l2*Ak3*fl13*fl27 - 6.E+0*P_k4*
             k3_k5*k4_l1*k5_l2*q_l2*Ak3*fl13*fl27 + 2.E+0*P_k4*k3_k5*k4_l2*
             k5_q*l1_l2*Ak3*fl13*fl27 - 6.666666666E+0*P_k4*k3_k5*k4_l2*
             k5_l1*q_l2*Ak3*fl13*fl27 - 3.333333333E+0*P_k4*k3_k5*k4_l2*
             k5_l2*q_l1*Ak3*fl13*fl27 - 2.E+0*P_k4*k3_q*k4_k5*k5_l1*l2_l2*
             Ak3*fl13*fl27 + 2.E+0*P_k4*k3_q*k4_k5*k5_l2*l1_l2*Ak3*fl13*
             fl27 + 2.E+0*P_k4*k3_q*k4_l1*k5_k5*l2_l2*Ak3*fl13*fl27 + 6.E+0
             *P_k4*k3_q*k4_l1*pow(k5_l2,2)*Ak3*fl13*fl27 - 2.E+0*P_k4*k3_q*
             k4_l2*k5_k5*l1_l2*Ak3*fl13*fl27 - 6.E+0*P_k4*k3_q*k4_l2*k5_l1*
             k5_l2*Ak3*fl13*fl27 - 1.333333333E+0*P_k4*k3_l1*k4_k5*k5_q*
             l2_l2*Ak3*fl13*fl27 - 6.666666666E+0*P_k4*k3_l1*k4_k5*k5_l2*
             q_l2*Ak3*fl13*fl27 + 1.333333333E+0*P_k4*k3_l1*k4_q*k5_k5*
             l2_l2*Ak3*fl13*fl27 - 1.333333333E+0*P_k4*k3_l1*k4_q*pow(
             k5_l2,2)*Ak3*fl13*fl27 - 1.333333333E+0*P_k4*k3_l1*k4_l2*k5_k5
             *q_l2*Ak3*fl13*fl27 + 1.333333333E+0*P_k4*k3_l1*k4_l2*k5_q*
             k5_l2*Ak3*fl13*fl27 - 6.666666666E+0*P_k4*k3_l2*k4_k5*k5_q*
             l1_l2*Ak3*fl13*fl27 + 2.E+0*P_k4*k3_l2*k4_k5*k5_l1*q_l2*Ak3*
             fl13*fl27 );

          Chi(6,10) +=  + H * ( 4.666666666E+0*P_k4*k3_l2*k4_k5*k5_l2*
             q_l1*Ak3*fl13*fl27 - 1.333333333E+0*P_k4*k3_l2*k4_q*k5_k5*
             l1_l2*Ak3*fl13*fl27 + 1.333333333E+0*P_k4*k3_l2*k4_q*k5_l1*
             k5_l2*Ak3*fl13*fl27 - 2.E+0*P_k4*k3_l2*k4_l1*k5_k5*q_l2*Ak3*
             fl13*fl27 - 6.E+0*P_k4*k3_l2*k4_l1*k5_q*k5_l2*Ak3*fl13*fl27 +
             3.333333333E+0*P_k4*k3_l2*k4_l2*k5_k5*q_l1*Ak3*fl13*fl27 +
             4.666666666E+0*P_k4*k3_l2*k4_l2*k5_q*k5_l1*Ak3*fl13*fl27 +
             3.333333333E+0*P_k5*k3_k4*k4_k5*q_l1*l2_l2*Ak3*fl13*fl27 +
             4.666666666E+0*P_k5*k3_k4*k4_k5*q_l2*l1_l2*Ak3*fl13*fl27 -
             2.E+0*P_k5*k3_k4*k4_q*k5_l1*l2_l2*Ak3*fl13*fl27 + 2.E+0*P_k5*
             k3_k4*k4_q*k5_l2*l1_l2*Ak3*fl13*fl27 - 1.333333333E+0*P_k5*
             k3_k4*k4_l1*k5_q*l2_l2*Ak3*fl13*fl27 - 6.666666666E+0*P_k5*
             k3_k4*k4_l1*k5_l2*q_l2*Ak3*fl13*fl27 + 1.333333333E+0*P_k5*
             k3_k4*k4_l2*k5_q*l1_l2*Ak3*fl13*fl27 - 6.E+0*P_k5*k3_k4*k4_l2*
             k5_l1*q_l2*Ak3*fl13*fl27 - 3.333333333E+0*P_k5*k3_k4*k4_l2*
             k5_l2*q_l1*Ak3*fl13*fl27 - 3.333333333E+0*P_k5*k3_k5*k4_k4*
             q_l1*l2_l2*Ak3*fl13*fl27 - 1.266666666E+1*P_k5*k3_k5*k4_k4*
             q_l2*l1_l2*Ak3*fl13*fl27 + 3.333333333E+0*P_k5*k3_k5*k4_q*
             k4_l1*l2_l2*Ak3*fl13*fl27 - 3.333333333E+0*P_k5*k3_k5*k4_q*
             k4_l2*l1_l2*Ak3*fl13*fl27 + 1.266666666E+1*P_k5*k3_k5*k4_l1*
             k4_l2*q_l2*Ak3*fl13*fl27 + 3.333333333E+0*P_k5*k3_k5*pow(
             k4_l2,2)*q_l1*Ak3*fl13*fl27 - 6.E+0*P_k5*k3_q*k4_k4*k5_l1*
             l2_l2*Ak3*fl13*fl27 + 6.E+0*P_k5*k3_q*k4_k4*k5_l2*l1_l2*Ak3*
             fl13*fl27 + 6.E+0*P_k5*k3_q*k4_k5*k4_l1*l2_l2*Ak3*fl13*fl27 -
             6.E+0*P_k5*k3_q*k4_k5*k4_l2*l1_l2*Ak3*fl13*fl27 - 6.E+0*P_k5*
             k3_q*k4_l1*k4_l2*k5_l2*Ak3*fl13*fl27 + 6.E+0*P_k5*k3_q*pow(
             k4_l2,2)*k5_l1*Ak3*fl13*fl27 + 1.333333333E+0*P_k5*k3_l1*k4_k4
             *k5_q*l2_l2*Ak3*fl13*fl27 + 6.666666666E+0*P_k5*k3_l1*k4_k4*
             k5_l2*q_l2*Ak3*fl13*fl27 - 1.333333333E+0*P_k5*k3_l1*k4_k5*
             k4_q*l2_l2*Ak3*fl13*fl27 + 1.333333333E+0*P_k5*k3_l1*k4_k5*
             k4_l2*q_l2*Ak3*fl13*fl27 + 1.333333333E+0*P_k5*k3_l1*k4_q*
             k4_l2*k5_l2*Ak3*fl13*fl27 - 1.333333333E+0*P_k5*k3_l1*pow(
             k4_l2,2)*k5_q*Ak3*fl13*fl27 - 1.333333333E+0*P_k5*k3_l2*k4_k4*
             k5_q*l1_l2*Ak3*fl13*fl27 + 1.4E+1*P_k5*k3_l2*k4_k4*k5_l1*q_l2*
             Ak3*fl13*fl27 - 4.666666666E+0*P_k5*k3_l2*k4_k4*k5_l2*q_l1*Ak3
             *fl13*fl27 + 9.333333333E+0*P_k5*k3_l2*k4_k5*k4_q*l1_l2*Ak3*
             fl13*fl27 - 1.4E+1*P_k5*k3_l2*k4_k5*k4_l1*q_l2*Ak3*fl13*fl27
              - 3.333333333E+0*P_k5*k3_l2*k4_k5*k4_l2*q_l1*Ak3*fl13*fl27 +
             4.666666666E+0*P_k5*k3_l2*k4_q*k4_l1*k5_l2*Ak3*fl13*fl27 -
             6.E+0*P_k5*k3_l2*k4_q*k4_l2*k5_l1*Ak3*fl13*fl27 +
             1.333333333E+0*P_k5*k3_l2*k4_l1*k4_l2*k5_q*Ak3*fl13*fl27 -
             1.333333333E+0*P_q*k3_k4*k4_k5*k5_l1*l2_l2*Ak3*fl13*fl27 +
             1.333333333E+0*P_q*k3_k4*k4_k5*k5_l2*l1_l2*Ak3*fl13*fl27 +
             1.333333333E+0*P_q*k3_k4*k4_l1*k5_k5*l2_l2*Ak3*fl13*fl27 -
             1.333333333E+0*P_q*k3_k4*k4_l1*pow(k5_l2,2)*Ak3*fl13*fl27 -
             1.333333333E+0*P_q*k3_k4*k4_l2*k5_k5*l1_l2*Ak3*fl13*fl27 +
             1.333333333E+0*P_q*k3_k4*k4_l2*k5_l1*k5_l2*Ak3*fl13*fl27 +
             1.333333333E+0*P_q*k3_k5*k4_k4*k5_l1*l2_l2*Ak3*fl13*fl27 -
             1.333333333E+0*P_q*k3_k5*k4_k4*k5_l2*l1_l2*Ak3*fl13*fl27 -
             1.333333333E+0*P_q*k3_k5*k4_k5*k4_l1*l2_l2*Ak3*fl13*fl27 +
             1.333333333E+0*P_q*k3_k5*k4_k5*k4_l2*l1_l2*Ak3*fl13*fl27 );

          Chi(6,10) +=  + H * ( 1.333333333E+0*P_q*k3_k5*k4_l1*k4_l2*
             k5_l2*Ak3*fl13*fl27 - 1.333333333E+0*P_q*k3_k5*pow(k4_l2,2)*
             k5_l1*Ak3*fl13*fl27 - 1.333333333E+0*P_q*k3_l1*k4_k4*k5_k5*
             l2_l2*Ak3*fl13*fl27 + 1.333333333E+0*P_q*k3_l1*k4_k4*pow(
             k5_l2,2)*Ak3*fl13*fl27 - 2.666666666E+0*P_q*k3_l1*k4_k5*k4_l2*
             k5_l2*Ak3*fl13*fl27 + 1.333333333E+0*P_q*k3_l1*pow(k4_k5,2)*
             l2_l2*Ak3*fl13*fl27 + 1.333333333E+0*P_q*k3_l1*pow(k4_l2,2)*
             k5_k5*Ak3*fl13*fl27 + 1.333333333E+0*P_q*k3_l2*k4_k4*k5_k5*
             l1_l2*Ak3*fl13*fl27 - 1.333333333E+0*P_q*k3_l2*k4_k4*k5_l1*
             k5_l2*Ak3*fl13*fl27 + 1.333333333E+0*P_q*k3_l2*k4_k5*k4_l1*
             k5_l2*Ak3*fl13*fl27 + 1.333333333E+0*P_q*k3_l2*k4_k5*k4_l2*
             k5_l1*Ak3*fl13*fl27 - 1.333333333E+0*P_q*k3_l2*pow(k4_k5,2)*
             l1_l2*Ak3*fl13*fl27 - 1.333333333E+0*P_q*k3_l2*k4_l1*k4_l2*
             k5_k5*Ak3*fl13*fl27 - 2.E+0*P_l1*k3_k4*k4_k5*k5_q*l2_l2*Ak3*
             fl13*fl27 - 6.E+0*P_l1*k3_k4*k4_k5*k5_l2*q_l2*Ak3*fl13*fl27 +
             2.E+0*P_l1*k3_k4*k4_q*k5_k5*l2_l2*Ak3*fl13*fl27 - 2.E+0*P_l1*
             k3_k4*k4_q*pow(k5_l2,2)*Ak3*fl13*fl27 - 2.E+0*P_l1*k3_k4*k4_l2
             *k5_k5*q_l2*Ak3*fl13*fl27 + 2.E+0*P_l1*k3_k4*k4_l2*k5_q*k5_l2*
             Ak3*fl13*fl27 + 2.E+0*P_l1*k3_k5*k4_k4*k5_q*l2_l2*Ak3*fl13*
             fl27 + 6.E+0*P_l1*k3_k5*k4_k4*k5_l2*q_l2*Ak3*fl13*fl27 - 2.E+0
             *P_l1*k3_k5*k4_k5*k4_q*l2_l2*Ak3*fl13*fl27 + 2.E+0*P_l1*k3_k5*
             k4_k5*k4_l2*q_l2*Ak3*fl13*fl27 + 2.E+0*P_l1*k3_k5*k4_q*k4_l2*
             k5_l2*Ak3*fl13*fl27 - 2.E+0*P_l1*k3_k5*pow(k4_l2,2)*k5_q*Ak3*
             fl13*fl27 - 2.E+0*P_l1*k3_q*k4_k4*k5_k5*l2_l2*Ak3*fl13*fl27 -
             6.E+0*P_l1*k3_q*k4_k4*pow(k5_l2,2)*Ak3*fl13*fl27 + 4.E+0*P_l1*
             k3_q*k4_k5*k4_l2*k5_l2*Ak3*fl13*fl27 + 2.E+0*P_l1*k3_q*pow(
             k4_k5,2)*l2_l2*Ak3*fl13*fl27 + 2.E+0*P_l1*k3_q*pow(k4_l2,2)*
             k5_k5*Ak3*fl13*fl27 + 2.E+0*P_l1*k3_l2*k4_k4*k5_k5*q_l2*Ak3*
             fl13*fl27 + 6.E+0*P_l1*k3_l2*k4_k4*k5_q*k5_l2*Ak3*fl13*fl27 -
             6.E+0*P_l1*k3_l2*k4_k5*k4_q*k5_l2*Ak3*fl13*fl27 + 2.E+0*P_l1*
             k3_l2*k4_k5*k4_l2*k5_q*Ak3*fl13*fl27 - 2.E+0*P_l1*k3_l2*pow(
             k4_k5,2)*q_l2*Ak3*fl13*fl27 - 2.E+0*P_l1*k3_l2*k4_q*k4_l2*
             k5_k5*Ak3*fl13*fl27 - 6.E+0*P_l2*k3_k4*k4_k5*k5_q*l1_l2*Ak3*
             fl13*fl27 + 1.333333333E+0*P_l2*k3_k4*k4_k5*k5_l1*q_l2*Ak3*
             fl13*fl27 + 4.666666666E+0*P_l2*k3_k4*k4_k5*k5_l2*q_l1*Ak3*
             fl13*fl27 - 2.E+0*P_l2*k3_k4*k4_q*k5_k5*l1_l2*Ak3*fl13*fl27 -
             1.4E+1*P_l2*k3_k4*k4_q*k5_l1*k5_l2*Ak3*fl13*fl27 -
             1.333333333E+0*P_l2*k3_k4*k4_l1*k5_k5*q_l2*Ak3*fl13*fl27 +
             9.333333333E+0*P_l2*k3_k4*k4_l1*k5_q*k5_l2*Ak3*fl13*fl27 +
             3.333333333E+0*P_l2*k3_k4*k4_l2*k5_k5*q_l1*Ak3*fl13*fl27 +
             4.666666666E+0*P_l2*k3_k4*k4_l2*k5_q*k5_l1*Ak3*fl13*fl27 +
             1.4E+1*P_l2*k3_k5*k4_k4*k5_q*l1_l2*Ak3*fl13*fl27 -
             1.333333333E+0*P_l2*k3_k5*k4_k4*k5_l1*q_l2*Ak3*fl13*fl27 -
             4.666666666E+0*P_l2*k3_k5*k4_k4*k5_l2*q_l1*Ak3*fl13*fl27 -
             6.E+0*P_l2*k3_k5*k4_k5*k4_q*l1_l2*Ak3*fl13*fl27 +
             1.333333333E+0*P_l2*k3_k5*k4_k5*k4_l1*q_l2*Ak3*fl13*fl27 -
             3.333333333E+0*P_l2*k3_k5*k4_k5*k4_l2*q_l1*Ak3*fl13*fl27 +
             4.666666666E+0*P_l2*k3_k5*k4_q*k4_l1*k5_l2*Ak3*fl13*fl27 +
             9.333333333E+0*P_l2*k3_k5*k4_q*k4_l2*k5_l1*Ak3*fl13*fl27 -
             1.4E+1*P_l2*k3_k5*k4_l1*k4_l2*k5_q*Ak3*fl13*fl27 - 6.E+0*P_l2*
             k3_q*k4_k4*k5_k5*l1_l2*Ak3*fl13*fl27 );

          Chi(6,10) +=  + H * ( 1.4E+1*P_l2*k3_q*k4_k4*k5_l1*k5_l2*Ak3*
             fl13*fl27 - 1.4E+1*P_l2*k3_q*k4_k5*k4_l1*k5_l2*Ak3*fl13*fl27
              - 6.E+0*P_l2*k3_q*k4_k5*k4_l2*k5_l1*Ak3*fl13*fl27 + 6.E+0*
             P_l2*k3_q*pow(k4_k5,2)*l1_l2*Ak3*fl13*fl27 + 6.E+0*P_l2*k3_q*
             k4_l1*k4_l2*k5_k5*Ak3*fl13*fl27 + 1.333333333E+0*P_l2*k3_l1*
             k4_k4*k5_k5*q_l2*Ak3*fl13*fl27 - 9.333333333E+0*P_l2*k3_l1*
             k4_k4*k5_q*k5_l2*Ak3*fl13*fl27 + 9.333333333E+0*P_l2*k3_l1*
             k4_k5*k4_q*k5_l2*Ak3*fl13*fl27 + 1.333333333E+0*P_l2*k3_l1*
             k4_k5*k4_l2*k5_q*Ak3*fl13*fl27 - 1.333333333E+0*P_l2*k3_l1*
             pow(k4_k5,2)*q_l2*Ak3*fl13*fl27 - 1.333333333E+0*P_l2*k3_l1*
             k4_q*k4_l2*k5_k5*Ak3*fl13*fl27 - 3.333333333E+0*P_l2*k3_l2*
             k4_k4*k5_k5*q_l1*Ak3*fl13*fl27 - 1.266666666E+1*P_l2*k3_l2*
             k4_k4*k5_q*k5_l1*Ak3*fl13*fl27 - 3.333333333E+0*P_l2*k3_l2*
             k4_k5*k4_q*k5_l1*Ak3*fl13*fl27 + 1.266666666E+1*P_l2*k3_l2*
             k4_k5*k4_l1*k5_q*Ak3*fl13*fl27 + 3.333333333E+0*P_l2*k3_l2*
             pow(k4_k5,2)*q_l1*Ak3*fl13*fl27 + 3.333333333E+0*P_l2*k3_l2*
             k4_q*k4_l1*k5_k5*Ak3*fl13*fl27 );

          Chi(6,10) +=  + G * ( 4.E+0*P_q*k4_k4*k5_k5*q_l2*l1_l2*Bk3*fl13
             *fl27 + 4.E+0*P_q*k4_k4*k5_q*k5_l1*l2_l2*Bk3*fl13*fl27 - 4.E+0
             *P_q*k4_k4*k5_q*k5_l2*l1_l2*Bk3*fl13*fl27 + 4.E+0*P_q*k4_k4*
             pow(k5_l2,2)*q_l1*Bk3*fl13*fl27 - 4.E+0*P_q*k4_k5*k4_l1*k5_q*
             l2_l2*Bk3*fl13*fl27 + 4.E+0*P_q*k4_k5*k4_l2*k5_q*l1_l2*Bk3*
             fl13*fl27 + 4.E+0*P_q*k4_k5*k4_l2*k5_l1*q_l2*Bk3*fl13*fl27 -
             4.E+0*P_q*k4_k5*k4_l2*k5_l2*q_l1*Bk3*fl13*fl27 - 4.E+0*P_q*
             pow(k4_k5,2)*q_l2*l1_l2*Bk3*fl13*fl27 - 4.E+0*P_q*k4_q*k4_l1*
             pow(k5_l2,2)*Bk3*fl13*fl27 + 4.E+0*P_q*k4_q*k4_l2*k5_l1*k5_l2*
             Bk3*fl13*fl27 - 4.E+0*P_q*k4_l1*k4_l2*k5_k5*q_l2*Bk3*fl13*fl27
              + 4.E+0*P_q*k4_l1*k4_l2*k5_q*k5_l2*Bk3*fl13*fl27 - 4.E+0*P_q*
             pow(k4_l2,2)*k5_q*k5_l1*Bk3*fl13*fl27 );

          Chi(6,10) +=  + F * (  - 4.E+0*P_k4*k4_l1*pow(k5_l2,2)*Bk3*fl13
             *fl27 + 4.E+0*P_k4*k4_l2*k5_l1*k5_l2*Bk3*fl13*fl27 + 4.E+0*
             P_k5*k4_k4*k5_l1*l2_l2*Bk3*fl13*fl27 - 4.E+0*P_k5*k4_k4*k5_l2*
             l1_l2*Bk3*fl13*fl27 - 4.E+0*P_k5*k4_k5*k4_l1*l2_l2*Bk3*fl13*
             fl27 + 4.E+0*P_k5*k4_k5*k4_l2*l1_l2*Bk3*fl13*fl27 + 4.E+0*P_k5
             *k4_l1*k4_l2*k5_l2*Bk3*fl13*fl27 - 4.E+0*P_k5*pow(k4_l2,2)*
             k5_l1*Bk3*fl13*fl27 + 4.E+0*P_l1*k4_k4*pow(k5_l2,2)*Bk3*fl13*
             fl27 - 4.E+0*P_l1*k4_k5*k4_l2*k5_l2*Bk3*fl13*fl27 + 4.E+0*P_l2
             *k4_k4*k5_k5*l1_l2*Bk3*fl13*fl27 + 4.E+0*P_l2*k4_k5*k4_l2*
             k5_l1*Bk3*fl13*fl27 - 4.E+0*P_l2*pow(k4_k5,2)*l1_l2*Bk3*fl13*
             fl27 - 4.E+0*P_l2*k4_l1*k4_l2*k5_k5*Bk3*fl13*fl27 );

          Chi(6,10) +=  + E * ( 4.E+0*k3_k4*k4_l1*pow(k5_l2,2)*Ak3*fl13*
             fl27 - 4.E+0*k3_k4*k4_l2*k5_l1*k5_l2*Ak3*fl13*fl27 - 4.E+0*
             k3_k5*k4_k4*k5_l1*l2_l2*Ak3*fl13*fl27 + 4.E+0*k3_k5*k4_k4*
             k5_l2*l1_l2*Ak3*fl13*fl27 + 4.E+0*k3_k5*k4_k5*k4_l1*l2_l2*Ak3*
             fl13*fl27 - 4.E+0*k3_k5*k4_k5*k4_l2*l1_l2*Ak3*fl13*fl27 -
             4.E+0*k3_k5*k4_l1*k4_l2*k5_l2*Ak3*fl13*fl27 + 4.E+0*k3_k5*pow(
             k4_l2,2)*k5_l1*Ak3*fl13*fl27 - 4.E+0*k3_l1*k4_k4*pow(k5_l2,2)*
             Ak3*fl13*fl27 + 4.E+0*k3_l1*k4_k5*k4_l2*k5_l2*Ak3*fl13*fl27 -
             4.E+0*k3_l2*k4_k4*k5_k5*l1_l2*Ak3*fl13*fl27 - 4.E+0*k3_l2*
             k4_k5*k4_l2*k5_l1*Ak3*fl13*fl27 + 4.E+0*k3_l2*pow(k4_k5,2)*
             l1_l2*Ak3*fl13*fl27 + 4.E+0*k3_l2*k4_l1*k4_l2*k5_k5*Ak3*fl13*
             fl27 );

       Chi(7,0) =
           + H * (  - 8.E+0*x2*P_k4*k3_k5*k4_k4*q_l1*Ak3*fl14 + 8.E+0*x2*
             P_k4*k3_l1*k4_k4*k5_q*Ak3*fl14 + 8.E+0*x2*P_k5*k3_k4*k4_k4*
             q_l1*Ak3*fl14 - 8.E+0*x2*P_k5*k3_l1*k4_k4*k4_q*Ak3*fl14 -
             8.E+0*x2*P_l1*k3_k4*k4_k4*k5_q*Ak3*fl14 + 8.E+0*x2*P_l1*k3_k5*
             k4_k4*k4_q*Ak3*fl14 );

       Chi(7,1) =
           + H * (  - 1.6E+1*y2*P_k4*k3_k5*k4_k4*q_l1*l2_l2*Ak3*fl14 +
             1.6E+1*y2*P_k4*k3_k5*k4_k4*q_l2*l1_l2*Ak3*fl14 + 1.6E+1*y2*
             P_k4*k3_l1*k4_k4*k5_q*l2_l2*Ak3*fl14 - 1.6E+1*y2*P_k4*k3_l1*
             k4_k4*k5_l2*q_l2*Ak3*fl14 - 1.6E+1*y2*P_k4*k3_l2*k4_k4*k5_q*
             l1_l2*Ak3*fl14 + 1.6E+1*y2*P_k4*k3_l2*k4_k4*k5_l2*q_l1*Ak3*
             fl14 + 1.6E+1*y2*P_k5*k3_k4*k4_k4*q_l1*l2_l2*Ak3*fl14 - 1.6E+1
             *y2*P_k5*k3_k4*k4_k4*q_l2*l1_l2*Ak3*fl14 - 1.6E+1*y2*P_k5*
             k3_l1*k4_k4*k4_q*l2_l2*Ak3*fl14 + 1.6E+1*y2*P_k5*k3_l1*k4_k4*
             k4_l2*q_l2*Ak3*fl14 + 1.6E+1*y2*P_k5*k3_l2*k4_k4*k4_q*l1_l2*
             Ak3*fl14 - 1.6E+1*y2*P_k5*k3_l2*k4_k4*k4_l2*q_l1*Ak3*fl14 -
             1.6E+1*y2*P_l1*k3_k4*k4_k4*k5_q*l2_l2*Ak3*fl14 + 1.6E+1*y2*
             P_l1*k3_k4*k4_k4*k5_l2*q_l2*Ak3*fl14 + 1.6E+1*y2*P_l1*k3_k5*
             k4_k4*k4_q*l2_l2*Ak3*fl14 - 1.6E+1*y2*P_l1*k3_k5*k4_k4*k4_l2*
             q_l2*Ak3*fl14 - 1.6E+1*y2*P_l1*k3_l2*k4_k4*k4_q*k5_l2*Ak3*fl14
              + 1.6E+1*y2*P_l1*k3_l2*k4_k4*k4_l2*k5_q*Ak3*fl14 + 1.6E+1*y2*
             P_l2*k3_k4*k4_k4*k5_q*l1_l2*Ak3*fl14 - 1.6E+1*y2*P_l2*k3_k4*
             k4_k4*k5_l2*q_l1*Ak3*fl14 - 1.6E+1*y2*P_l2*k3_k5*k4_k4*k4_q*
             l1_l2*Ak3*fl14 + 1.6E+1*y2*P_l2*k3_k5*k4_k4*k4_l2*q_l1*Ak3*
             fl14 + 1.6E+1*y2*P_l2*k3_l1*k4_k4*k4_q*k5_l2*Ak3*fl14 - 1.6E+1
             *y2*P_l2*k3_l1*k4_k4*k4_l2*k5_q*Ak3*fl14 );

       Chi(7,2) = 0;

       Chi(7,3) =
           + H * (  - 8.E+0*P_k4*k3_k5*k4_k4*k5_k5*q_l1*Ak3*fl14*fl20 +
             8.E+0*P_k4*k3_l1*k4_k4*k5_k5*k5_q*Ak3*fl14*fl20 + 8.E+0*P_k5*
             k3_k4*k4_k4*k5_k5*q_l1*Ak3*fl14*fl20 - 8.E+0*P_k5*k3_l1*k4_k4*
             k4_q*k5_k5*Ak3*fl14*fl20 - 8.E+0*P_l1*k3_k4*k4_k4*k5_k5*k5_q*
             Ak3*fl14*fl20 + 8.E+0*P_l1*k3_k5*k4_k4*k4_q*k5_k5*Ak3*fl14*
             fl20 );

       Chi(7,4) =
           + H * ( 8.E+0*P_k4*k4_k4*k5_k5*k5_q*k5_l2*l1_l2*Bk3*fl14*fl21 -
             8.E+0*P_k4*k4_k4*k5_k5*pow(k5_l2,2)*q_l1*Bk3*fl14*fl21 - 8.E+0
             *P_k5*k4_k4*k4_q*k5_k5*k5_l2*l1_l2*Bk3*fl14*fl21 + 8.E+0*P_k5*
             k4_k4*k4_l2*k5_k5*k5_l2*q_l1*Bk3*fl14*fl21 + 8.E+0*P_l1*k4_k4*
             k4_q*k5_k5*pow(k5_l2,2)*Bk3*fl14*fl21 - 8.E+0*P_l1*k4_k4*k4_l2
             *k5_k5*k5_q*k5_l2*Bk3*fl14*fl21 );

          Chi(7,4) +=  + G * (  - 4.E+0*P_q*k3_k4*k4_k4*k5_k5*k5_q*k5_l2*
             l1_l2*Ak3*fl14*fl21 + 4.E+0*P_q*k3_k4*k4_k4*k5_k5*pow(k5_l2,2)
             *q_l1*Ak3*fl14*fl21 + 4.E+0*P_q*k3_k5*k4_k4*k4_q*k5_k5*k5_l2*
             l1_l2*Ak3*fl14*fl21 - 4.E+0*P_q*k3_k5*k4_k4*k4_l2*k5_k5*k5_l2*
             q_l1*Ak3*fl14*fl21 - 4.E+0*P_q*k3_l1*k4_k4*k4_q*k5_k5*pow(
             k5_l2,2)*Ak3*fl14*fl21 + 4.E+0*P_q*k3_l1*k4_k4*k4_l2*k5_k5*
             k5_q*k5_l2*Ak3*fl14*fl21 );

          Chi(7,4) +=  + F * ( 4.E+0*P_k4*k3_k5*k4_k4*k5_k5*k5_l2*l1_l2*
             Ak3*fl14*fl21 - 4.E+0*P_k4*k3_l1*k4_k4*k5_k5*pow(k5_l2,2)*Ak3*
             fl14*fl21 - 4.E+0*P_k5*k3_k4*k4_k4*k5_k5*k5_l2*l1_l2*Ak3*fl14*
             fl21 + 4.E+0*P_k5*k3_l1*k4_k4*k4_l2*k5_k5*k5_l2*Ak3*fl14*fl21
              + 4.E+0*P_l1*k3_k4*k4_k4*k5_k5*pow(k5_l2,2)*Ak3*fl14*fl21 -
             4.E+0*P_l1*k3_k5*k4_k4*k4_l2*k5_k5*k5_l2*Ak3*fl14*fl21 );

       Chi(7,5) =
           + H * (  - 8.E+0*P_k4*k4_k4*k5_k5*q_l1*Bk3*fl14*fl22 + 8.E+0*
             P_k4*k4_k4*k5_q*k5_l1*Bk3*fl14*fl22 + 8.E+0*P_k5*k4_k4*k4_k5*
             q_l1*Bk3*fl14*fl22 - 8.E+0*P_k5*k4_k4*k4_q*k5_l1*Bk3*fl14*fl22
              - 8.E+0*P_l1*k4_k4*k4_k5*k5_q*Bk3*fl14*fl22 + 8.E+0*P_l1*
             k4_k4*k4_q*k5_k5*Bk3*fl14*fl22 );

          Chi(7,5) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k4*k5_k5*q_l1*Ak3*fl14*
             fl22 - 4.E+0*P_q*k3_k4*k4_k4*k5_q*k5_l1*Ak3*fl14*fl22 - 4.E+0*
             P_q*k3_k5*k4_k4*k4_k5*q_l1*Ak3*fl14*fl22 + 4.E+0*P_q*k3_k5*
             k4_k4*k4_q*k5_l1*Ak3*fl14*fl22 + 4.E+0*P_q*k3_l1*k4_k4*k4_k5*
             k5_q*Ak3*fl14*fl22 - 4.E+0*P_q*k3_l1*k4_k4*k4_q*k5_k5*Ak3*fl14
             *fl22 );

          Chi(7,5) +=  + F * ( 4.E+0*P_k4*k3_k5*k4_k4*k5_l1*Ak3*fl14*fl22
              - 4.E+0*P_k4*k3_l1*k4_k4*k5_k5*Ak3*fl14*fl22 - 4.E+0*P_k5*
             k3_k4*k4_k4*k5_l1*Ak3*fl14*fl22 + 4.E+0*P_k5*k3_l1*k4_k4*k4_k5
             *Ak3*fl14*fl22 + 4.E+0*P_l1*k3_k4*k4_k4*k5_k5*Ak3*fl14*fl22 -
             4.E+0*P_l1*k3_k5*k4_k4*k4_k5*Ak3*fl14*fl22 );

       Chi(7,6) =
           + H * (  - 8.E+0*P_k3*k4_k4*k4_k5*k5_q*l1_l2*Ak3*fl14*fl23 +
             8.E+0*P_k3*k4_k4*k4_k5*k5_l2*q_l1*Ak3*fl14*fl23 + 8.E+0*P_k3*
             k4_k4*k4_q*k5_k5*l1_l2*Ak3*fl14*fl23 - 8.E+0*P_k3*k4_k4*k4_q*
             k5_l1*k5_l2*Ak3*fl14*fl23 - 8.E+0*P_k3*k4_k4*k4_l2*k5_k5*q_l1*
             Ak3*fl14*fl23 + 8.E+0*P_k3*k4_k4*k4_l2*k5_q*k5_l1*Ak3*fl14*
             fl23 - 8.E+0*P_k4*k3_q*k4_k4*k5_k5*l1_l2*Ak3*fl14*fl23 + 8.E+0
             *P_k4*k3_q*k4_k4*k5_l1*k5_l2*Ak3*fl14*fl23 + 8.E+0*P_k5*k3_q*
             k4_k4*k4_k5*l1_l2*Ak3*fl14*fl23 - 8.E+0*P_k5*k3_q*k4_k4*k4_l2*
             k5_l1*Ak3*fl14*fl23 - 8.E+0*P_l1*k3_q*k4_k4*k4_k5*k5_l2*Ak3*
             fl14*fl23 + 8.E+0*P_l1*k3_q*k4_k4*k4_l2*k5_k5*Ak3*fl14*fl23 );

          Chi(7,6) +=  + G * (  - 4.E+0*P_q*k4_k4*k4_k5*k5_q*l1_l2*Bk3*
             fl14*fl23 + 4.E+0*P_q*k4_k4*k4_k5*k5_l2*q_l1*Bk3*fl14*fl23 +
             4.E+0*P_q*k4_k4*k4_q*k5_k5*l1_l2*Bk3*fl14*fl23 - 4.E+0*P_q*
             k4_k4*k4_q*k5_l1*k5_l2*Bk3*fl14*fl23 - 4.E+0*P_q*k4_k4*k4_l2*
             k5_k5*q_l1*Bk3*fl14*fl23 + 4.E+0*P_q*k4_k4*k4_l2*k5_q*k5_l1*
             Bk3*fl14*fl23 );

          Chi(7,6) +=  + F * ( 4.E+0*P_k4*k4_k4*k5_k5*l1_l2*Bk3*fl14*fl23
              - 4.E+0*P_k4*k4_k4*k5_l1*k5_l2*Bk3*fl14*fl23 - 4.E+0*P_k5*
             k4_k4*k4_k5*l1_l2*Bk3*fl14*fl23 + 4.E+0*P_k5*k4_k4*k4_l2*k5_l1
             *Bk3*fl14*fl23 + 4.E+0*P_l1*k4_k4*k4_k5*k5_l2*Bk3*fl14*fl23 -
             4.E+0*P_l1*k4_k4*k4_l2*k5_k5*Bk3*fl14*fl23 );

          Chi(7,6) +=  + E * (  - 4.E+0*k3_k4*k4_k4*k5_k5*l1_l2*Ak3*fl14*
             fl23 + 4.E+0*k3_k4*k4_k4*k5_l1*k5_l2*Ak3*fl14*fl23 + 4.E+0*
             k3_k5*k4_k4*k4_k5*l1_l2*Ak3*fl14*fl23 - 4.E+0*k3_k5*k4_k4*
             k4_l2*k5_l1*Ak3*fl14*fl23 - 4.E+0*k3_l1*k4_k4*k4_k5*k5_l2*Ak3*
             fl14*fl23 + 4.E+0*k3_l1*k4_k4*k4_l2*k5_k5*Ak3*fl14*fl23 );

       Chi(7,7) = 0;

       Chi(7,8) =
           + H * (  - 8.E+0*P_k4*k3_k5*k4_k4*k5_k5*q_l1*l2_l2*Ak3*fl14*fl25
              + 8.E+0*P_k4*k3_k5*k4_k4*k5_k5*q_l2*l1_l2*Ak3*fl14*fl25 +
             8.E+0*P_k4*k3_l1*k4_k4*k5_k5*k5_q*l2_l2*Ak3*fl14*fl25 - 8.E+0*
             P_k4*k3_l1*k4_k4*k5_k5*k5_l2*q_l2*Ak3*fl14*fl25 - 8.E+0*P_k4*
             k3_l2*k4_k4*k5_k5*k5_q*l1_l2*Ak3*fl14*fl25 + 8.E+0*P_k4*k3_l2*
             k4_k4*k5_k5*k5_l2*q_l1*Ak3*fl14*fl25 + 8.E+0*P_k5*k3_k4*k4_k4*
             k5_k5*q_l1*l2_l2*Ak3*fl14*fl25 - 8.E+0*P_k5*k3_k4*k4_k4*k5_k5*
             q_l2*l1_l2*Ak3*fl14*fl25 - 8.E+0*P_k5*k3_l1*k4_k4*k4_q*k5_k5*
             l2_l2*Ak3*fl14*fl25 + 8.E+0*P_k5*k3_l1*k4_k4*k4_l2*k5_k5*q_l2*
             Ak3*fl14*fl25 + 8.E+0*P_k5*k3_l2*k4_k4*k4_q*k5_k5*l1_l2*Ak3*
             fl14*fl25 - 8.E+0*P_k5*k3_l2*k4_k4*k4_l2*k5_k5*q_l1*Ak3*fl14*
             fl25 - 8.E+0*P_l1*k3_k4*k4_k4*k5_k5*k5_q*l2_l2*Ak3*fl14*fl25
              + 8.E+0*P_l1*k3_k4*k4_k4*k5_k5*k5_l2*q_l2*Ak3*fl14*fl25 +
             8.E+0*P_l1*k3_k5*k4_k4*k4_q*k5_k5*l2_l2*Ak3*fl14*fl25 - 8.E+0*
             P_l1*k3_k5*k4_k4*k4_l2*k5_k5*q_l2*Ak3*fl14*fl25 - 8.E+0*P_l1*
             k3_l2*k4_k4*k4_q*k5_k5*k5_l2*Ak3*fl14*fl25 + 8.E+0*P_l1*k3_l2*
             k4_k4*k4_l2*k5_k5*k5_q*Ak3*fl14*fl25 + 8.E+0*P_l2*k3_k4*k4_k4*
             k5_k5*k5_q*l1_l2*Ak3*fl14*fl25 - 8.E+0*P_l2*k3_k4*k4_k4*k5_k5*
             k5_l2*q_l1*Ak3*fl14*fl25 - 8.E+0*P_l2*k3_k5*k4_k4*k4_q*k5_k5*
             l1_l2*Ak3*fl14*fl25 + 8.E+0*P_l2*k3_k5*k4_k4*k4_l2*k5_k5*q_l1*
             Ak3*fl14*fl25 + 8.E+0*P_l2*k3_l1*k4_k4*k4_q*k5_k5*k5_l2*Ak3*
             fl14*fl25 - 8.E+0*P_l2*k3_l1*k4_k4*k4_l2*k5_k5*k5_q*Ak3*fl14*
             fl25 );

       Chi(7,9) =
           + H * (  - 8.E+0*P_k4*k3_k5*k4_k4*k5_l1*k5_l2*q_l2*Ak3*fl14*fl26
              + 8.E+0*P_k4*k3_l1*k4_k4*k5_k5*k5_l2*q_l2*Ak3*fl14*fl26 -
             8.E+0*P_k4*k3_l2*k4_k4*k5_k5*k5_l2*q_l1*Ak3*fl14*fl26 + 8.E+0*
             P_k4*k3_l2*k4_k4*k5_q*k5_l1*k5_l2*Ak3*fl14*fl26 + 8.E+0*P_k5*
             k3_k4*k4_k4*k5_l1*k5_l2*q_l2*Ak3*fl14*fl26 - 8.E+0*P_k5*k3_l1*
             k4_k4*k4_k5*k5_l2*q_l2*Ak3*fl14*fl26 + 8.E+0*P_k5*k3_l2*k4_k4*
             k4_k5*k5_l2*q_l1*Ak3*fl14*fl26 - 8.E+0*P_k5*k3_l2*k4_k4*k4_q*
             k5_l1*k5_l2*Ak3*fl14*fl26 - 8.E+0*P_l1*k3_k4*k4_k4*k5_k5*k5_l2
             *q_l2*Ak3*fl14*fl26 + 8.E+0*P_l1*k3_k5*k4_k4*k4_k5*k5_l2*q_l2*
             Ak3*fl14*fl26 - 8.E+0*P_l1*k3_l2*k4_k4*k4_k5*k5_q*k5_l2*Ak3*
             fl14*fl26 + 8.E+0*P_l1*k3_l2*k4_k4*k4_q*k5_k5*k5_l2*Ak3*fl14*
             fl26 + 8.E+0*P_l2*k3_k4*k4_k4*k5_k5*k5_l2*q_l1*Ak3*fl14*fl26
              - 8.E+0*P_l2*k3_k4*k4_k4*k5_q*k5_l1*k5_l2*Ak3*fl14*fl26 -
             8.E+0*P_l2*k3_k5*k4_k4*k4_k5*k5_l2*q_l1*Ak3*fl14*fl26 + 8.E+0*
             P_l2*k3_k5*k4_k4*k4_q*k5_l1*k5_l2*Ak3*fl14*fl26 + 8.E+0*P_l2*
             k3_l1*k4_k4*k4_k5*k5_q*k5_l2*Ak3*fl14*fl26 - 8.E+0*P_l2*k3_l1*
             k4_k4*k4_q*k5_k5*k5_l2*Ak3*fl14*fl26 );

       Chi(7,10) =
           + H * (  - 8.E+0*P_k4*k4_k4*k5_k5*q_l1*l2_l2*Bk3*fl14*fl27 +
             8.E+0*P_k4*k4_k4*k5_k5*q_l2*l1_l2*Bk3*fl14*fl27 + 8.E+0*P_k4*
             k4_k4*k5_q*k5_l1*l2_l2*Bk3*fl14*fl27 - 8.E+0*P_k4*k4_k4*k5_l1*
             k5_l2*q_l2*Bk3*fl14*fl27 + 8.E+0*P_k5*k4_k4*k4_k5*q_l1*l2_l2*
             Bk3*fl14*fl27 - 8.E+0*P_k5*k4_k4*k4_k5*q_l2*l1_l2*Bk3*fl14*
             fl27 - 8.E+0*P_k5*k4_k4*k4_q*k5_l1*l2_l2*Bk3*fl14*fl27 + 8.E+0
             *P_k5*k4_k4*k4_l2*k5_l1*q_l2*Bk3*fl14*fl27 - 8.E+0*P_l1*k4_k4*
             k4_k5*k5_q*l2_l2*Bk3*fl14*fl27 + 8.E+0*P_l1*k4_k4*k4_k5*k5_l2*
             q_l2*Bk3*fl14*fl27 + 8.E+0*P_l1*k4_k4*k4_q*k5_k5*l2_l2*Bk3*
             fl14*fl27 - 8.E+0*P_l1*k4_k4*k4_l2*k5_k5*q_l2*Bk3*fl14*fl27 +
             8.E+0*P_l2*k4_k4*k4_k5*k5_q*l1_l2*Bk3*fl14*fl27 - 8.E+0*P_l2*
             k4_k4*k4_k5*k5_l2*q_l1*Bk3*fl14*fl27 - 8.E+0*P_l2*k4_k4*k4_q*
             k5_k5*l1_l2*Bk3*fl14*fl27 + 8.E+0*P_l2*k4_k4*k4_q*k5_l1*k5_l2*
             Bk3*fl14*fl27 + 8.E+0*P_l2*k4_k4*k4_l2*k5_k5*q_l1*Bk3*fl14*
             fl27 - 8.E+0*P_l2*k4_k4*k4_l2*k5_q*k5_l1*Bk3*fl14*fl27 );

          Chi(7,10) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k4*k5_k5*q_l1*l2_l2*
             Ak3*fl14*fl27 - 4.E+0*P_q*k3_k4*k4_k4*k5_k5*q_l2*l1_l2*Ak3*
             fl14*fl27 - 4.E+0*P_q*k3_k4*k4_k4*k5_q*k5_l1*l2_l2*Ak3*fl14*
             fl27 + 4.E+0*P_q*k3_k4*k4_k4*k5_l1*k5_l2*q_l2*Ak3*fl14*fl27 -
             4.E+0*P_q*k3_k5*k4_k4*k4_k5*q_l1*l2_l2*Ak3*fl14*fl27 + 4.E+0*
             P_q*k3_k5*k4_k4*k4_k5*q_l2*l1_l2*Ak3*fl14*fl27 + 4.E+0*P_q*
             k3_k5*k4_k4*k4_q*k5_l1*l2_l2*Ak3*fl14*fl27 - 4.E+0*P_q*k3_k5*
             k4_k4*k4_l2*k5_l1*q_l2*Ak3*fl14*fl27 + 4.E+0*P_q*k3_l1*k4_k4*
             k4_k5*k5_q*l2_l2*Ak3*fl14*fl27 - 4.E+0*P_q*k3_l1*k4_k4*k4_k5*
             k5_l2*q_l2*Ak3*fl14*fl27 - 4.E+0*P_q*k3_l1*k4_k4*k4_q*k5_k5*
             l2_l2*Ak3*fl14*fl27 + 4.E+0*P_q*k3_l1*k4_k4*k4_l2*k5_k5*q_l2*
             Ak3*fl14*fl27 - 4.E+0*P_q*k3_l2*k4_k4*k4_k5*k5_q*l1_l2*Ak3*
             fl14*fl27 + 4.E+0*P_q*k3_l2*k4_k4*k4_k5*k5_l2*q_l1*Ak3*fl14*
             fl27 + 4.E+0*P_q*k3_l2*k4_k4*k4_q*k5_k5*l1_l2*Ak3*fl14*fl27 -
             4.E+0*P_q*k3_l2*k4_k4*k4_q*k5_l1*k5_l2*Ak3*fl14*fl27 - 4.E+0*
             P_q*k3_l2*k4_k4*k4_l2*k5_k5*q_l1*Ak3*fl14*fl27 + 4.E+0*P_q*
             k3_l2*k4_k4*k4_l2*k5_q*k5_l1*Ak3*fl14*fl27 );

          Chi(7,10) +=  + F * ( 4.E+0*P_k4*k3_k5*k4_k4*k5_l1*l2_l2*Ak3*
             fl14*fl27 - 4.E+0*P_k4*k3_l1*k4_k4*k5_k5*l2_l2*Ak3*fl14*fl27
              + 4.E+0*P_k4*k3_l2*k4_k4*k5_k5*l1_l2*Ak3*fl14*fl27 - 4.E+0*
             P_k4*k3_l2*k4_k4*k5_l1*k5_l2*Ak3*fl14*fl27 - 4.E+0*P_k5*k3_k4*
             k4_k4*k5_l1*l2_l2*Ak3*fl14*fl27 + 4.E+0*P_k5*k3_l1*k4_k4*k4_k5
             *l2_l2*Ak3*fl14*fl27 - 4.E+0*P_k5*k3_l2*k4_k4*k4_k5*l1_l2*Ak3*
             fl14*fl27 + 4.E+0*P_k5*k3_l2*k4_k4*k4_l2*k5_l1*Ak3*fl14*fl27
              + 4.E+0*P_l1*k3_k4*k4_k4*k5_k5*l2_l2*Ak3*fl14*fl27 - 4.E+0*
             P_l1*k3_k5*k4_k4*k4_k5*l2_l2*Ak3*fl14*fl27 + 4.E+0*P_l1*k3_l2*
             k4_k4*k4_k5*k5_l2*Ak3*fl14*fl27 - 4.E+0*P_l1*k3_l2*k4_k4*k4_l2
             *k5_k5*Ak3*fl14*fl27 - 4.E+0*P_l2*k3_k4*k4_k4*k5_k5*l1_l2*Ak3*
             fl14*fl27 + 4.E+0*P_l2*k3_k4*k4_k4*k5_l1*k5_l2*Ak3*fl14*fl27
              + 4.E+0*P_l2*k3_k5*k4_k4*k4_k5*l1_l2*Ak3*fl14*fl27 - 4.E+0*
             P_l2*k3_k5*k4_k4*k4_l2*k5_l1*Ak3*fl14*fl27 - 4.E+0*P_l2*k3_l1*
             k4_k4*k4_k5*k5_l2*Ak3*fl14*fl27 + 4.E+0*P_l2*k3_l1*k4_k4*k4_l2
             *k5_k5*Ak3*fl14*fl27 );

       Chi(8,0) =
           + H * ( 8.E+0*x2*P_k4*k4_k4*k5_q*l1_l1*Bk3*fl15 - 8.E+0*x2*P_k4*
             k4_k4*k5_l1*q_l1*Bk3*fl15 - 8.E+0*x2*P_k5*k4_k4*k4_q*l1_l1*Bk3
             *fl15 + 8.E+0*x2*P_k5*k4_k4*k4_l1*q_l1*Bk3*fl15 + 8.E+0*x2*
             P_l1*k4_k4*k4_q*k5_l1*Bk3*fl15 - 8.E+0*x2*P_l1*k4_k4*k4_l1*
             k5_q*Bk3*fl15 );

          Chi(8,0) +=  + G * ( 4.E+0*x2*P_q*k3_k4*k4_k4*k5_q*l1_l1*Ak3*
             fl15 - 4.E+0*x2*P_q*k3_k4*k4_k4*k5_l1*q_l1*Ak3*fl15 - 4.E+0*x2
             *P_q*k3_k5*k4_k4*k4_q*l1_l1*Ak3*fl15 + 4.E+0*x2*P_q*k3_k5*
             k4_k4*k4_l1*q_l1*Ak3*fl15 + 4.E+0*x2*P_q*k3_l1*k4_k4*k4_q*
             k5_l1*Ak3*fl15 - 4.E+0*x2*P_q*k3_l1*k4_k4*k4_l1*k5_q*Ak3*fl15
              );

          Chi(8,0) +=  + F * (  - 4.E+0*x2*P_k4*k3_k5*k4_k4*l1_l1*Ak3*
             fl15 + 4.E+0*x2*P_k4*k3_l1*k4_k4*k5_l1*Ak3*fl15 + 4.E+0*x2*
             P_k5*k3_k4*k4_k4*l1_l1*Ak3*fl15 - 4.E+0*x2*P_k5*k3_l1*k4_k4*
             k4_l1*Ak3*fl15 - 4.E+0*x2*P_l1*k3_k4*k4_k4*k5_l1*Ak3*fl15 +
             4.E+0*x2*P_l1*k3_k5*k4_k4*k4_l1*Ak3*fl15 );

       Chi(8,1) =
           + H * ( 1.6E+1*y2*P_k4*k4_k4*k5_q*l1_l1*l2_l2*Bk3*fl15 - 1.6E+1*
             y2*P_k4*k4_k4*k5_q*pow(l1_l2,2)*Bk3*fl15 - 1.6E+1*y2*P_k4*
             k4_k4*k5_l1*q_l1*l2_l2*Bk3*fl15 + 1.6E+1*y2*P_k4*k4_k4*k5_l1*
             q_l2*l1_l2*Bk3*fl15 + 1.6E+1*y2*P_k4*k4_k4*k5_l2*q_l1*l1_l2*
             Bk3*fl15 - 1.6E+1*y2*P_k4*k4_k4*k5_l2*q_l2*l1_l1*Bk3*fl15 -
             1.6E+1*y2*P_k5*k4_k4*k4_q*l1_l1*l2_l2*Bk3*fl15 + 1.6E+1*y2*
             P_k5*k4_k4*k4_q*pow(l1_l2,2)*Bk3*fl15 + 1.6E+1*y2*P_k5*k4_k4*
             k4_l1*q_l1*l2_l2*Bk3*fl15 - 1.6E+1*y2*P_k5*k4_k4*k4_l1*q_l2*
             l1_l2*Bk3*fl15 - 1.6E+1*y2*P_k5*k4_k4*k4_l2*q_l1*l1_l2*Bk3*
             fl15 + 1.6E+1*y2*P_k5*k4_k4*k4_l2*q_l2*l1_l1*Bk3*fl15 + 1.6E+1
             *y2*P_l1*k4_k4*k4_q*k5_l1*l2_l2*Bk3*fl15 - 1.6E+1*y2*P_l1*
             k4_k4*k4_q*k5_l2*l1_l2*Bk3*fl15 - 1.6E+1*y2*P_l1*k4_k4*k4_l1*
             k5_q*l2_l2*Bk3*fl15 + 1.6E+1*y2*P_l1*k4_k4*k4_l1*k5_l2*q_l2*
             Bk3*fl15 + 1.6E+1*y2*P_l1*k4_k4*k4_l2*k5_q*l1_l2*Bk3*fl15 -
             1.6E+1*y2*P_l1*k4_k4*k4_l2*k5_l1*q_l2*Bk3*fl15 - 1.6E+1*y2*
             P_l2*k4_k4*k4_q*k5_l1*l1_l2*Bk3*fl15 + 1.6E+1*y2*P_l2*k4_k4*
             k4_q*k5_l2*l1_l1*Bk3*fl15 + 1.6E+1*y2*P_l2*k4_k4*k4_l1*k5_q*
             l1_l2*Bk3*fl15 - 1.6E+1*y2*P_l2*k4_k4*k4_l1*k5_l2*q_l1*Bk3*
             fl15 - 1.6E+1*y2*P_l2*k4_k4*k4_l2*k5_q*l1_l1*Bk3*fl15 + 1.6E+1
             *y2*P_l2*k4_k4*k4_l2*k5_l1*q_l1*Bk3*fl15 );

          Chi(8,1) +=  + G * ( 8.E+0*y2*P_q*k3_k4*k4_k4*k5_q*l1_l1*l2_l2*
             Ak3*fl15 - 8.E+0*y2*P_q*k3_k4*k4_k4*k5_q*pow(l1_l2,2)*Ak3*fl15
              - 8.E+0*y2*P_q*k3_k4*k4_k4*k5_l1*q_l1*l2_l2*Ak3*fl15 + 8.E+0*
             y2*P_q*k3_k4*k4_k4*k5_l1*q_l2*l1_l2*Ak3*fl15 + 8.E+0*y2*P_q*
             k3_k4*k4_k4*k5_l2*q_l1*l1_l2*Ak3*fl15 - 8.E+0*y2*P_q*k3_k4*
             k4_k4*k5_l2*q_l2*l1_l1*Ak3*fl15 - 8.E+0*y2*P_q*k3_k5*k4_k4*
             k4_q*l1_l1*l2_l2*Ak3*fl15 + 8.E+0*y2*P_q*k3_k5*k4_k4*k4_q*pow(
             l1_l2,2)*Ak3*fl15 + 8.E+0*y2*P_q*k3_k5*k4_k4*k4_l1*q_l1*l2_l2*
             Ak3*fl15 - 8.E+0*y2*P_q*k3_k5*k4_k4*k4_l1*q_l2*l1_l2*Ak3*fl15
              - 8.E+0*y2*P_q*k3_k5*k4_k4*k4_l2*q_l1*l1_l2*Ak3*fl15 + 8.E+0*
             y2*P_q*k3_k5*k4_k4*k4_l2*q_l2*l1_l1*Ak3*fl15 + 8.E+0*y2*P_q*
             k3_l1*k4_k4*k4_q*k5_l1*l2_l2*Ak3*fl15 - 8.E+0*y2*P_q*k3_l1*
             k4_k4*k4_q*k5_l2*l1_l2*Ak3*fl15 - 8.E+0*y2*P_q*k3_l1*k4_k4*
             k4_l1*k5_q*l2_l2*Ak3*fl15 + 8.E+0*y2*P_q*k3_l1*k4_k4*k4_l1*
             k5_l2*q_l2*Ak3*fl15 + 8.E+0*y2*P_q*k3_l1*k4_k4*k4_l2*k5_q*
             l1_l2*Ak3*fl15 - 8.E+0*y2*P_q*k3_l1*k4_k4*k4_l2*k5_l1*q_l2*Ak3
             *fl15 - 8.E+0*y2*P_q*k3_l2*k4_k4*k4_q*k5_l1*l1_l2*Ak3*fl15 +
             8.E+0*y2*P_q*k3_l2*k4_k4*k4_q*k5_l2*l1_l1*Ak3*fl15 + 8.E+0*y2*
             P_q*k3_l2*k4_k4*k4_l1*k5_q*l1_l2*Ak3*fl15 - 8.E+0*y2*P_q*k3_l2
             *k4_k4*k4_l1*k5_l2*q_l1*Ak3*fl15 - 8.E+0*y2*P_q*k3_l2*k4_k4*
             k4_l2*k5_q*l1_l1*Ak3*fl15 + 8.E+0*y2*P_q*k3_l2*k4_k4*k4_l2*
             k5_l1*q_l1*Ak3*fl15 );

          Chi(8,1) +=  + F * (  - 8.E+0*y2*P_k4*k3_k5*k4_k4*l1_l1*l2_l2*
             Ak3*fl15 + 8.E+0*y2*P_k4*k3_k5*k4_k4*pow(l1_l2,2)*Ak3*fl15 +
             8.E+0*y2*P_k4*k3_l1*k4_k4*k5_l1*l2_l2*Ak3*fl15 - 8.E+0*y2*P_k4
             *k3_l1*k4_k4*k5_l2*l1_l2*Ak3*fl15 - 8.E+0*y2*P_k4*k3_l2*k4_k4*
             k5_l1*l1_l2*Ak3*fl15 + 8.E+0*y2*P_k4*k3_l2*k4_k4*k5_l2*l1_l1*
             Ak3*fl15 + 8.E+0*y2*P_k5*k3_k4*k4_k4*l1_l1*l2_l2*Ak3*fl15 -
             8.E+0*y2*P_k5*k3_k4*k4_k4*pow(l1_l2,2)*Ak3*fl15 - 8.E+0*y2*
             P_k5*k3_l1*k4_k4*k4_l1*l2_l2*Ak3*fl15 + 8.E+0*y2*P_k5*k3_l1*
             k4_k4*k4_l2*l1_l2*Ak3*fl15 + 8.E+0*y2*P_k5*k3_l2*k4_k4*k4_l1*
             l1_l2*Ak3*fl15 - 8.E+0*y2*P_k5*k3_l2*k4_k4*k4_l2*l1_l1*Ak3*
             fl15 - 8.E+0*y2*P_l1*k3_k4*k4_k4*k5_l1*l2_l2*Ak3*fl15 + 8.E+0*
             y2*P_l1*k3_k4*k4_k4*k5_l2*l1_l2*Ak3*fl15 + 8.E+0*y2*P_l1*k3_k5
             *k4_k4*k4_l1*l2_l2*Ak3*fl15 - 8.E+0*y2*P_l1*k3_k5*k4_k4*k4_l2*
             l1_l2*Ak3*fl15 - 8.E+0*y2*P_l1*k3_l2*k4_k4*k4_l1*k5_l2*Ak3*
             fl15 + 8.E+0*y2*P_l1*k3_l2*k4_k4*k4_l2*k5_l1*Ak3*fl15 + 8.E+0*
             y2*P_l2*k3_k4*k4_k4*k5_l1*l1_l2*Ak3*fl15 - 8.E+0*y2*P_l2*k3_k4
             *k4_k4*k5_l2*l1_l1*Ak3*fl15 - 8.E+0*y2*P_l2*k3_k5*k4_k4*k4_l1*
             l1_l2*Ak3*fl15 + 8.E+0*y2*P_l2*k3_k5*k4_k4*k4_l2*l1_l1*Ak3*
             fl15 + 8.E+0*y2*P_l2*k3_l1*k4_k4*k4_l1*k5_l2*Ak3*fl15 - 8.E+0*
             y2*P_l2*k3_l1*k4_k4*k4_l2*k5_l1*Ak3*fl15 );

       Chi(8,2) =
           + H * (  - 1.6E+1*z2*P_k4*k3_k5*k4_k4*q_l1*l1_l2*Ak3*fl15 +
             1.6E+1*z2*P_k4*k3_k5*k4_k4*q_l2*l1_l1*Ak3*fl15 + 1.6E+1*z2*
             P_k4*k3_l1*k4_k4*k5_q*l1_l2*Ak3*fl15 - 1.6E+1*z2*P_k4*k3_l1*
             k4_k4*k5_l1*q_l2*Ak3*fl15 - 1.6E+1*z2*P_k4*k3_l2*k4_k4*k5_q*
             l1_l1*Ak3*fl15 + 1.6E+1*z2*P_k4*k3_l2*k4_k4*k5_l1*q_l1*Ak3*
             fl15 + 1.6E+1*z2*P_k5*k3_k4*k4_k4*q_l1*l1_l2*Ak3*fl15 - 1.6E+1
             *z2*P_k5*k3_k4*k4_k4*q_l2*l1_l1*Ak3*fl15 - 1.6E+1*z2*P_k5*
             k3_l1*k4_k4*k4_q*l1_l2*Ak3*fl15 + 1.6E+1*z2*P_k5*k3_l1*k4_k4*
             k4_l1*q_l2*Ak3*fl15 + 1.6E+1*z2*P_k5*k3_l2*k4_k4*k4_q*l1_l1*
             Ak3*fl15 - 1.6E+1*z2*P_k5*k3_l2*k4_k4*k4_l1*q_l1*Ak3*fl15 -
             1.6E+1*z2*P_l1*k3_k4*k4_k4*k5_q*l1_l2*Ak3*fl15 + 1.6E+1*z2*
             P_l1*k3_k4*k4_k4*k5_l1*q_l2*Ak3*fl15 + 1.6E+1*z2*P_l1*k3_k5*
             k4_k4*k4_q*l1_l2*Ak3*fl15 - 1.6E+1*z2*P_l1*k3_k5*k4_k4*k4_l1*
             q_l2*Ak3*fl15 - 1.6E+1*z2*P_l1*k3_l2*k4_k4*k4_q*k5_l1*Ak3*fl15
              + 1.6E+1*z2*P_l1*k3_l2*k4_k4*k4_l1*k5_q*Ak3*fl15 + 1.6E+1*z2*
             P_l2*k3_k4*k4_k4*k5_q*l1_l1*Ak3*fl15 - 1.6E+1*z2*P_l2*k3_k4*
             k4_k4*k5_l1*q_l1*Ak3*fl15 - 1.6E+1*z2*P_l2*k3_k5*k4_k4*k4_q*
             l1_l1*Ak3*fl15 + 1.6E+1*z2*P_l2*k3_k5*k4_k4*k4_l1*q_l1*Ak3*
             fl15 + 1.6E+1*z2*P_l2*k3_l1*k4_k4*k4_q*k5_l1*Ak3*fl15 - 1.6E+1
             *z2*P_l2*k3_l1*k4_k4*k4_l1*k5_q*Ak3*fl15 );

       Chi(8,3) =
           + H * ( 8.E+0*P_k4*k4_k4*k5_k5*k5_q*l1_l1*Bk3*fl15*fl20 - 8.E+0*
             P_k4*k4_k4*k5_k5*k5_l1*q_l1*Bk3*fl15*fl20 - 8.E+0*P_k5*k4_k4*
             k4_q*k5_k5*l1_l1*Bk3*fl15*fl20 + 8.E+0*P_k5*k4_k4*k4_l1*k5_k5*
             q_l1*Bk3*fl15*fl20 + 8.E+0*P_l1*k4_k4*k4_q*k5_k5*k5_l1*Bk3*
             fl15*fl20 - 8.E+0*P_l1*k4_k4*k4_l1*k5_k5*k5_q*Bk3*fl15*fl20 );

          Chi(8,3) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k4*k5_k5*k5_q*l1_l1*Ak3
             *fl15*fl20 - 4.E+0*P_q*k3_k4*k4_k4*k5_k5*k5_l1*q_l1*Ak3*fl15*
             fl20 - 4.E+0*P_q*k3_k5*k4_k4*k4_q*k5_k5*l1_l1*Ak3*fl15*fl20 +
             4.E+0*P_q*k3_k5*k4_k4*k4_l1*k5_k5*q_l1*Ak3*fl15*fl20 + 4.E+0*
             P_q*k3_l1*k4_k4*k4_q*k5_k5*k5_l1*Ak3*fl15*fl20 - 4.E+0*P_q*
             k3_l1*k4_k4*k4_l1*k5_k5*k5_q*Ak3*fl15*fl20 );

          Chi(8,3) +=  + F * (  - 4.E+0*P_k4*k3_k5*k4_k4*k5_k5*l1_l1*Ak3*
             fl15*fl20 + 4.E+0*P_k4*k3_l1*k4_k4*k5_k5*k5_l1*Ak3*fl15*fl20
              + 4.E+0*P_k5*k3_k4*k4_k4*k5_k5*l1_l1*Ak3*fl15*fl20 - 4.E+0*
             P_k5*k3_l1*k4_k4*k4_l1*k5_k5*Ak3*fl15*fl20 - 4.E+0*P_l1*k3_k4*
             k4_k4*k5_k5*k5_l1*Ak3*fl15*fl20 + 4.E+0*P_l1*k3_k5*k4_k4*k4_l1
             *k5_k5*Ak3*fl15*fl20 );

       Chi(8,4) =
           + H * (  - 8.E+0*P_k3*k4_k4*k4_q*k5_k5*k5_l1*k5_l2*l1_l2*Ak3*
             fl15*fl21 + 8.E+0*P_k3*k4_k4*k4_q*k5_k5*pow(k5_l2,2)*l1_l1*Ak3
             *fl15*fl21 + 8.E+0*P_k3*k4_k4*k4_l1*k5_k5*k5_q*k5_l2*l1_l2*Ak3
             *fl15*fl21 - 8.E+0*P_k3*k4_k4*k4_l1*k5_k5*pow(k5_l2,2)*q_l1*
             Ak3*fl15*fl21 - 8.E+0*P_k3*k4_k4*k4_l2*k5_k5*k5_q*k5_l2*l1_l1*
             Ak3*fl15*fl21 + 8.E+0*P_k3*k4_k4*k4_l2*k5_k5*k5_l1*k5_l2*q_l1*
             Ak3*fl15*fl21 - 8.E+0*P_k4*k3_k5*k4_k4*k5_k5*k5_l2*q_l1*l1_l2*
             Ak3*fl15*fl21 + 8.E+0*P_k4*k3_q*k4_k4*k5_k5*k5_l1*k5_l2*l1_l2*
             Ak3*fl15*fl21 - 8.E+0*P_k4*k3_q*k4_k4*k5_k5*pow(k5_l2,2)*l1_l1
             *Ak3*fl15*fl21 - 8.E+0*P_k4*k3_l1*k4_k4*k5_k5*k5_q*k5_l2*l1_l2
             *Ak3*fl15*fl21 + 1.6E+1*P_k4*k3_l1*k4_k4*k5_k5*pow(k5_l2,2)*
             q_l1*Ak3*fl15*fl21 + 8.E+0*P_k5*k3_k4*k4_k4*k5_k5*k5_l2*q_l1*
             l1_l2*Ak3*fl15*fl21 - 8.E+0*P_k5*k3_q*k4_k4*k4_l1*k5_k5*k5_l2*
             l1_l2*Ak3*fl15*fl21 + 8.E+0*P_k5*k3_q*k4_k4*k4_l2*k5_k5*k5_l2*
             l1_l1*Ak3*fl15*fl21 + 8.E+0*P_k5*k3_l1*k4_k4*k4_q*k5_k5*k5_l2*
             l1_l2*Ak3*fl15*fl21 - 1.6E+1*P_k5*k3_l1*k4_k4*k4_l2*k5_k5*
             k5_l2*q_l1*Ak3*fl15*fl21 - 8.E+0*P_l1*k3_k4*k4_k4*k5_k5*k5_q*
             k5_l2*l1_l2*Ak3*fl15*fl21 + 8.E+0*P_l1*k3_k5*k4_k4*k4_q*k5_k5*
             k5_l2*l1_l2*Ak3*fl15*fl21 + 8.E+0*P_l1*k3_q*k4_k4*k4_l1*k5_k5*
             pow(k5_l2,2)*Ak3*fl15*fl21 - 8.E+0*P_l1*k3_q*k4_k4*k4_l2*k5_k5
             *k5_l1*k5_l2*Ak3*fl15*fl21 - 1.6E+1*P_l1*k3_l1*k4_k4*k4_q*
             k5_k5*pow(k5_l2,2)*Ak3*fl15*fl21 + 1.6E+1*P_l1*k3_l1*k4_k4*
             k4_l2*k5_k5*k5_q*k5_l2*Ak3*fl15*fl21 );

          Chi(8,4) +=  + G * ( 4.E+0*P_q*k4_k4*k4_q*k5_k5*k5_l1*k5_l2*
             l1_l2*Bk3*fl15*fl21 - 4.E+0*P_q*k4_k4*k4_q*k5_k5*pow(k5_l2,2)*
             l1_l1*Bk3*fl15*fl21 - 4.E+0*P_q*k4_k4*k4_l1*k5_k5*k5_q*k5_l2*
             l1_l2*Bk3*fl15*fl21 + 4.E+0*P_q*k4_k4*k4_l1*k5_k5*pow(k5_l2,2)
             *q_l1*Bk3*fl15*fl21 + 4.E+0*P_q*k4_k4*k4_l2*k5_k5*k5_q*k5_l2*
             l1_l1*Bk3*fl15*fl21 - 4.E+0*P_q*k4_k4*k4_l2*k5_k5*k5_l1*k5_l2*
             q_l1*Bk3*fl15*fl21 );

          Chi(8,4) +=  + F * ( 4.E+0*P_k4*k4_k4*k5_k5*k5_l1*k5_l2*l1_l2*
             Bk3*fl15*fl21 - 4.E+0*P_k4*k4_k4*k5_k5*pow(k5_l2,2)*l1_l1*Bk3*
             fl15*fl21 - 4.E+0*P_k5*k4_k4*k4_l1*k5_k5*k5_l2*l1_l2*Bk3*fl15*
             fl21 + 4.E+0*P_k5*k4_k4*k4_l2*k5_k5*k5_l2*l1_l1*Bk3*fl15*fl21
              + 4.E+0*P_l1*k4_k4*k4_l1*k5_k5*pow(k5_l2,2)*Bk3*fl15*fl21 -
             4.E+0*P_l1*k4_k4*k4_l2*k5_k5*k5_l1*k5_l2*Bk3*fl15*fl21 );

          Chi(8,4) +=  + E * ( 4.E+0*k3_k4*k4_k4*k5_k5*k5_l1*k5_l2*l1_l2*
             Ak3*fl15*fl21 - 4.E+0*k3_k4*k4_k4*k5_k5*pow(k5_l2,2)*l1_l1*Ak3
             *fl15*fl21 - 4.E+0*k3_k5*k4_k4*k4_l1*k5_k5*k5_l2*l1_l2*Ak3*
             fl15*fl21 + 4.E+0*k3_k5*k4_k4*k4_l2*k5_k5*k5_l2*l1_l1*Ak3*fl15
             *fl21 + 4.E+0*k3_l1*k4_k4*k4_l1*k5_k5*pow(k5_l2,2)*Ak3*fl15*
             fl21 - 4.E+0*k3_l1*k4_k4*k4_l2*k5_k5*k5_l1*k5_l2*Ak3*fl15*fl21
              );

       Chi(8,5) =
           + H * (  - 8.E+0*P_k3*k4_k4*k4_k5*k5_q*l1_l1*Ak3*fl15*fl22 +
             8.E+0*P_k3*k4_k4*k4_k5*k5_l1*q_l1*Ak3*fl15*fl22 + 8.E+0*P_k3*
             k4_k4*k4_q*k5_k5*l1_l1*Ak3*fl15*fl22 - 8.E+0*P_k3*k4_k4*k4_q*
             pow(k5_l1,2)*Ak3*fl15*fl22 - 8.E+0*P_k3*k4_k4*k4_l1*k5_k5*q_l1
             *Ak3*fl15*fl22 + 8.E+0*P_k3*k4_k4*k4_l1*k5_q*k5_l1*Ak3*fl15*
             fl22 - 8.E+0*P_k4*k3_k5*k4_k4*k5_l1*q_l1*Ak3*fl15*fl22 - 8.E+0
             *P_k4*k3_q*k4_k4*k5_k5*l1_l1*Ak3*fl15*fl22 + 8.E+0*P_k4*k3_q*
             k4_k4*pow(k5_l1,2)*Ak3*fl15*fl22 + 1.6E+1*P_k4*k3_l1*k4_k4*
             k5_k5*q_l1*Ak3*fl15*fl22 - 8.E+0*P_k4*k3_l1*k4_k4*k5_q*k5_l1*
             Ak3*fl15*fl22 + 8.E+0*P_k5*k3_k4*k4_k4*k5_l1*q_l1*Ak3*fl15*
             fl22 + 8.E+0*P_k5*k3_q*k4_k4*k4_k5*l1_l1*Ak3*fl15*fl22 - 8.E+0
             *P_k5*k3_q*k4_k4*k4_l1*k5_l1*Ak3*fl15*fl22 - 1.6E+1*P_k5*k3_l1
             *k4_k4*k4_k5*q_l1*Ak3*fl15*fl22 + 8.E+0*P_k5*k3_l1*k4_k4*k4_q*
             k5_l1*Ak3*fl15*fl22 - 8.E+0*P_l1*k3_k4*k4_k4*k5_q*k5_l1*Ak3*
             fl15*fl22 + 8.E+0*P_l1*k3_k5*k4_k4*k4_q*k5_l1*Ak3*fl15*fl22 -
             8.E+0*P_l1*k3_q*k4_k4*k4_k5*k5_l1*Ak3*fl15*fl22 + 8.E+0*P_l1*
             k3_q*k4_k4*k4_l1*k5_k5*Ak3*fl15*fl22 + 1.6E+1*P_l1*k3_l1*k4_k4
             *k4_k5*k5_q*Ak3*fl15*fl22 - 1.6E+1*P_l1*k3_l1*k4_k4*k4_q*k5_k5
             *Ak3*fl15*fl22 );

          Chi(8,5) +=  + G * ( 4.E+0*P_q*k4_k4*k4_k5*k5_q*l1_l1*Bk3*fl15*
             fl22 - 4.E+0*P_q*k4_k4*k4_k5*k5_l1*q_l1*Bk3*fl15*fl22 - 4.E+0*
             P_q*k4_k4*k4_q*k5_k5*l1_l1*Bk3*fl15*fl22 + 4.E+0*P_q*k4_k4*
             k4_q*pow(k5_l1,2)*Bk3*fl15*fl22 + 4.E+0*P_q*k4_k4*k4_l1*k5_k5*
             q_l1*Bk3*fl15*fl22 - 4.E+0*P_q*k4_k4*k4_l1*k5_q*k5_l1*Bk3*fl15
             *fl22 );

          Chi(8,5) +=  + F * (  - 4.E+0*P_k4*k4_k4*k5_k5*l1_l1*Bk3*fl15*
             fl22 + 4.E+0*P_k4*k4_k4*pow(k5_l1,2)*Bk3*fl15*fl22 + 4.E+0*
             P_k5*k4_k4*k4_k5*l1_l1*Bk3*fl15*fl22 - 4.E+0*P_k5*k4_k4*k4_l1*
             k5_l1*Bk3*fl15*fl22 - 4.E+0*P_l1*k4_k4*k4_k5*k5_l1*Bk3*fl15*
             fl22 + 4.E+0*P_l1*k4_k4*k4_l1*k5_k5*Bk3*fl15*fl22 );

          Chi(8,5) +=  + E * (  - 4.E+0*k3_k4*k4_k4*k5_k5*l1_l1*Ak3*fl15*
             fl22 + 4.E+0*k3_k4*k4_k4*pow(k5_l1,2)*Ak3*fl15*fl22 + 4.E+0*
             k3_k5*k4_k4*k4_k5*l1_l1*Ak3*fl15*fl22 - 4.E+0*k3_k5*k4_k4*
             k4_l1*k5_l1*Ak3*fl15*fl22 - 4.E+0*k3_l1*k4_k4*k4_k5*k5_l1*Ak3*
             fl15*fl22 + 4.E+0*k3_l1*k4_k4*k4_l1*k5_k5*Ak3*fl15*fl22 );

       Chi(8,6) =
           + H * (  - 8.E+0*P_k4*k4_k4*k5_k5*q_l1*l1_l2*Bk3*fl15*fl23 +
             8.E+0*P_k4*k4_k4*k5_l1*k5_l2*q_l1*Bk3*fl15*fl23 + 8.E+0*P_k5*
             k4_k4*k4_k5*q_l1*l1_l2*Bk3*fl15*fl23 - 8.E+0*P_k5*k4_k4*k4_l2*
             k5_l1*q_l1*Bk3*fl15*fl23 - 8.E+0*P_l1*k4_k4*k4_k5*k5_q*l1_l2*
             Bk3*fl15*fl23 + 8.E+0*P_l1*k4_k4*k4_q*k5_k5*l1_l2*Bk3*fl15*
             fl23 - 8.E+0*P_l1*k4_k4*k4_q*k5_l1*k5_l2*Bk3*fl15*fl23 + 8.E+0
             *P_l1*k4_k4*k4_l2*k5_q*k5_l1*Bk3*fl15*fl23 );

          Chi(8,6) +=  + G * (  - 4.E+0*P_q*k3_k4*k4_k4*k5_k5*q_l1*l1_l2*
             Ak3*fl15*fl23 + 4.E+0*P_q*k3_k4*k4_k4*k5_l1*k5_l2*q_l1*Ak3*
             fl15*fl23 + 4.E+0*P_q*k3_k5*k4_k4*k4_k5*q_l1*l1_l2*Ak3*fl15*
             fl23 - 4.E+0*P_q*k3_k5*k4_k4*k4_l2*k5_l1*q_l1*Ak3*fl15*fl23 -
             4.E+0*P_q*k3_q*k4_k4*k4_k5*k5_l1*l1_l2*Ak3*fl15*fl23 + 4.E+0*
             P_q*k3_q*k4_k4*k4_k5*k5_l2*l1_l1*Ak3*fl15*fl23 + 4.E+0*P_q*
             k3_q*k4_k4*k4_l1*k5_k5*l1_l2*Ak3*fl15*fl23 - 4.E+0*P_q*k3_q*
             k4_k4*k4_l1*k5_l1*k5_l2*Ak3*fl15*fl23 - 4.E+0*P_q*k3_q*k4_k4*
             k4_l2*k5_k5*l1_l1*Ak3*fl15*fl23 + 4.E+0*P_q*k3_q*k4_k4*k4_l2*
             pow(k5_l1,2)*Ak3*fl15*fl23 + 4.E+0*P_q*k3_l1*k4_k4*k4_k5*k5_q*
             l1_l2*Ak3*fl15*fl23 - 8.E+0*P_q*k3_l1*k4_k4*k4_k5*k5_l2*q_l1*
             Ak3*fl15*fl23 - 4.E+0*P_q*k3_l1*k4_k4*k4_q*k5_k5*l1_l2*Ak3*
             fl15*fl23 + 4.E+0*P_q*k3_l1*k4_k4*k4_q*k5_l1*k5_l2*Ak3*fl15*
             fl23 + 8.E+0*P_q*k3_l1*k4_k4*k4_l2*k5_k5*q_l1*Ak3*fl15*fl23 -
             4.E+0*P_q*k3_l1*k4_k4*k4_l2*k5_q*k5_l1*Ak3*fl15*fl23 );

          Chi(8,6) +=  + F * (  - 4.E+0*P_k3*k4_k4*k4_k5*k5_l1*l1_l2*Ak3*
             fl15*fl23 + 4.E+0*P_k3*k4_k4*k4_k5*k5_l2*l1_l1*Ak3*fl15*fl23
              + 4.E+0*P_k3*k4_k4*k4_l1*k5_k5*l1_l2*Ak3*fl15*fl23 - 4.E+0*
             P_k3*k4_k4*k4_l1*k5_l1*k5_l2*Ak3*fl15*fl23 - 4.E+0*P_k3*k4_k4*
             k4_l2*k5_k5*l1_l1*Ak3*fl15*fl23 + 4.E+0*P_k3*k4_k4*k4_l2*pow(
             k5_l1,2)*Ak3*fl15*fl23 - 4.E+0*P_k4*k3_l1*k4_k4*k5_k5*l1_l2*
             Ak3*fl15*fl23 + 4.E+0*P_k4*k3_l1*k4_k4*k5_l1*k5_l2*Ak3*fl15*
             fl23 + 4.E+0*P_k5*k3_l1*k4_k4*k4_k5*l1_l2*Ak3*fl15*fl23 -
             4.E+0*P_k5*k3_l1*k4_k4*k4_l2*k5_l1*Ak3*fl15*fl23 - 4.E+0*P_l1*
             k3_k4*k4_k4*k5_k5*l1_l2*Ak3*fl15*fl23 + 4.E+0*P_l1*k3_k4*k4_k4
             *k5_l1*k5_l2*Ak3*fl15*fl23 + 4.E+0*P_l1*k3_k5*k4_k4*k4_k5*
             l1_l2*Ak3*fl15*fl23 - 4.E+0*P_l1*k3_k5*k4_k4*k4_l2*k5_l1*Ak3*
             fl15*fl23 - 8.E+0*P_l1*k3_l1*k4_k4*k4_k5*k5_l2*Ak3*fl15*fl23
              + 8.E+0*P_l1*k3_l1*k4_k4*k4_l2*k5_k5*Ak3*fl15*fl23 );

          Chi(8,6) +=  + E * ( 4.E+0*k4_k4*k4_k5*k5_l1*l1_l2*Bk3*fl15*
             fl23 - 4.E+0*k4_k4*k4_k5*k5_l2*l1_l1*Bk3*fl15*fl23 - 4.E+0*
             k4_k4*k4_l1*k5_k5*l1_l2*Bk3*fl15*fl23 + 4.E+0*k4_k4*k4_l1*
             k5_l1*k5_l2*Bk3*fl15*fl23 + 4.E+0*k4_k4*k4_l2*k5_k5*l1_l1*Bk3*
             fl15*fl23 - 4.E+0*k4_k4*k4_l2*pow(k5_l1,2)*Bk3*fl15*fl23 );

       Chi(8,7) =
           + H * ( 8.E+0*P_k4*k3_k5*k4_k4*k5_k5*q_l1*l1_l2*Ak3*fl15*fl24 -
             8.E+0*P_k4*k3_k5*k4_k4*k5_k5*q_l2*l1_l1*Ak3*fl15*fl24 - 8.E+0*
             P_k4*k3_l1*k4_k4*k5_k5*k5_q*l1_l2*Ak3*fl15*fl24 + 8.E+0*P_k4*
             k3_l1*k4_k4*k5_k5*k5_l1*q_l2*Ak3*fl15*fl24 + 8.E+0*P_k4*k3_l2*
             k4_k4*k5_k5*k5_q*l1_l1*Ak3*fl15*fl24 - 8.E+0*P_k4*k3_l2*k4_k4*
             k5_k5*k5_l1*q_l1*Ak3*fl15*fl24 - 8.E+0*P_k5*k3_k4*k4_k4*k5_k5*
             q_l1*l1_l2*Ak3*fl15*fl24 + 8.E+0*P_k5*k3_k4*k4_k4*k5_k5*q_l2*
             l1_l1*Ak3*fl15*fl24 + 8.E+0*P_k5*k3_l1*k4_k4*k4_q*k5_k5*l1_l2*
             Ak3*fl15*fl24 - 8.E+0*P_k5*k3_l1*k4_k4*k4_l1*k5_k5*q_l2*Ak3*
             fl15*fl24 - 8.E+0*P_k5*k3_l2*k4_k4*k4_q*k5_k5*l1_l1*Ak3*fl15*
             fl24 + 8.E+0*P_k5*k3_l2*k4_k4*k4_l1*k5_k5*q_l1*Ak3*fl15*fl24
              + 8.E+0*P_l1*k3_k4*k4_k4*k5_k5*k5_q*l1_l2*Ak3*fl15*fl24 -
             8.E+0*P_l1*k3_k4*k4_k4*k5_k5*k5_l1*q_l2*Ak3*fl15*fl24 - 8.E+0*
             P_l1*k3_k5*k4_k4*k4_q*k5_k5*l1_l2*Ak3*fl15*fl24 + 8.E+0*P_l1*
             k3_k5*k4_k4*k4_l1*k5_k5*q_l2*Ak3*fl15*fl24 + 8.E+0*P_l1*k3_l2*
             k4_k4*k4_q*k5_k5*k5_l1*Ak3*fl15*fl24 - 8.E+0*P_l1*k3_l2*k4_k4*
             k4_l1*k5_k5*k5_q*Ak3*fl15*fl24 - 8.E+0*P_l2*k3_k4*k4_k4*k5_k5*
             k5_q*l1_l1*Ak3*fl15*fl24 + 8.E+0*P_l2*k3_k4*k4_k4*k5_k5*k5_l1*
             q_l1*Ak3*fl15*fl24 + 8.E+0*P_l2*k3_k5*k4_k4*k4_q*k5_k5*l1_l1*
             Ak3*fl15*fl24 - 8.E+0*P_l2*k3_k5*k4_k4*k4_l1*k5_k5*q_l1*Ak3*
             fl15*fl24 - 8.E+0*P_l2*k3_l1*k4_k4*k4_q*k5_k5*k5_l1*Ak3*fl15*
             fl24 + 8.E+0*P_l2*k3_l1*k4_k4*k4_l1*k5_k5*k5_q*Ak3*fl15*fl24 )
             ;

       Chi(8,8) =
           + H * ( 8.E+0*P_k4*k4_k4*k5_k5*k5_q*l1_l1*l2_l2*Bk3*fl15*fl25 -
             8.E+0*P_k4*k4_k4*k5_k5*k5_q*pow(l1_l2,2)*Bk3*fl15*fl25 - 8.E+0
             *P_k4*k4_k4*k5_k5*k5_l1*q_l1*l2_l2*Bk3*fl15*fl25 + 8.E+0*P_k4*
             k4_k4*k5_k5*k5_l1*q_l2*l1_l2*Bk3*fl15*fl25 + 8.E+0*P_k4*k4_k4*
             k5_k5*k5_l2*q_l1*l1_l2*Bk3*fl15*fl25 - 8.E+0*P_k4*k4_k4*k5_k5*
             k5_l2*q_l2*l1_l1*Bk3*fl15*fl25 - 8.E+0*P_k5*k4_k4*k4_q*k5_k5*
             l1_l1*l2_l2*Bk3*fl15*fl25 + 8.E+0*P_k5*k4_k4*k4_q*k5_k5*pow(
             l1_l2,2)*Bk3*fl15*fl25 + 8.E+0*P_k5*k4_k4*k4_l1*k5_k5*q_l1*
             l2_l2*Bk3*fl15*fl25 - 8.E+0*P_k5*k4_k4*k4_l1*k5_k5*q_l2*l1_l2*
             Bk3*fl15*fl25 - 8.E+0*P_k5*k4_k4*k4_l2*k5_k5*q_l1*l1_l2*Bk3*
             fl15*fl25 + 8.E+0*P_k5*k4_k4*k4_l2*k5_k5*q_l2*l1_l1*Bk3*fl15*
             fl25 + 8.E+0*P_l1*k4_k4*k4_q*k5_k5*k5_l1*l2_l2*Bk3*fl15*fl25
              - 8.E+0*P_l1*k4_k4*k4_q*k5_k5*k5_l2*l1_l2*Bk3*fl15*fl25 -
             8.E+0*P_l1*k4_k4*k4_l1*k5_k5*k5_q*l2_l2*Bk3*fl15*fl25 + 8.E+0*
             P_l1*k4_k4*k4_l1*k5_k5*k5_l2*q_l2*Bk3*fl15*fl25 + 8.E+0*P_l1*
             k4_k4*k4_l2*k5_k5*k5_q*l1_l2*Bk3*fl15*fl25 - 8.E+0*P_l1*k4_k4*
             k4_l2*k5_k5*k5_l1*q_l2*Bk3*fl15*fl25 - 8.E+0*P_l2*k4_k4*k4_q*
             k5_k5*k5_l1*l1_l2*Bk3*fl15*fl25 + 8.E+0*P_l2*k4_k4*k4_q*k5_k5*
             k5_l2*l1_l1*Bk3*fl15*fl25 + 8.E+0*P_l2*k4_k4*k4_l1*k5_k5*k5_q*
             l1_l2*Bk3*fl15*fl25 - 8.E+0*P_l2*k4_k4*k4_l1*k5_k5*k5_l2*q_l1*
             Bk3*fl15*fl25 - 8.E+0*P_l2*k4_k4*k4_l2*k5_k5*k5_q*l1_l1*Bk3*
             fl15*fl25 + 8.E+0*P_l2*k4_k4*k4_l2*k5_k5*k5_l1*q_l1*Bk3*fl15*
             fl25 );

          Chi(8,8) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k4*k5_k5*k5_q*l1_l1*
             l2_l2*Ak3*fl15*fl25 - 4.E+0*P_q*k3_k4*k4_k4*k5_k5*k5_q*pow(
             l1_l2,2)*Ak3*fl15*fl25 - 4.E+0*P_q*k3_k4*k4_k4*k5_k5*k5_l1*
             q_l1*l2_l2*Ak3*fl15*fl25 + 4.E+0*P_q*k3_k4*k4_k4*k5_k5*k5_l1*
             q_l2*l1_l2*Ak3*fl15*fl25 + 4.E+0*P_q*k3_k4*k4_k4*k5_k5*k5_l2*
             q_l1*l1_l2*Ak3*fl15*fl25 - 4.E+0*P_q*k3_k4*k4_k4*k5_k5*k5_l2*
             q_l2*l1_l1*Ak3*fl15*fl25 - 4.E+0*P_q*k3_k5*k4_k4*k4_q*k5_k5*
             l1_l1*l2_l2*Ak3*fl15*fl25 + 4.E+0*P_q*k3_k5*k4_k4*k4_q*k5_k5*
             pow(l1_l2,2)*Ak3*fl15*fl25 + 4.E+0*P_q*k3_k5*k4_k4*k4_l1*k5_k5
             *q_l1*l2_l2*Ak3*fl15*fl25 - 4.E+0*P_q*k3_k5*k4_k4*k4_l1*k5_k5*
             q_l2*l1_l2*Ak3*fl15*fl25 - 4.E+0*P_q*k3_k5*k4_k4*k4_l2*k5_k5*
             q_l1*l1_l2*Ak3*fl15*fl25 + 4.E+0*P_q*k3_k5*k4_k4*k4_l2*k5_k5*
             q_l2*l1_l1*Ak3*fl15*fl25 + 4.E+0*P_q*k3_l1*k4_k4*k4_q*k5_k5*
             k5_l1*l2_l2*Ak3*fl15*fl25 - 4.E+0*P_q*k3_l1*k4_k4*k4_q*k5_k5*
             k5_l2*l1_l2*Ak3*fl15*fl25 - 4.E+0*P_q*k3_l1*k4_k4*k4_l1*k5_k5*
             k5_q*l2_l2*Ak3*fl15*fl25 + 4.E+0*P_q*k3_l1*k4_k4*k4_l1*k5_k5*
             k5_l2*q_l2*Ak3*fl15*fl25 + 4.E+0*P_q*k3_l1*k4_k4*k4_l2*k5_k5*
             k5_q*l1_l2*Ak3*fl15*fl25 - 4.E+0*P_q*k3_l1*k4_k4*k4_l2*k5_k5*
             k5_l1*q_l2*Ak3*fl15*fl25 - 4.E+0*P_q*k3_l2*k4_k4*k4_q*k5_k5*
             k5_l1*l1_l2*Ak3*fl15*fl25 + 4.E+0*P_q*k3_l2*k4_k4*k4_q*k5_k5*
             k5_l2*l1_l1*Ak3*fl15*fl25 + 4.E+0*P_q*k3_l2*k4_k4*k4_l1*k5_k5*
             k5_q*l1_l2*Ak3*fl15*fl25 - 4.E+0*P_q*k3_l2*k4_k4*k4_l1*k5_k5*
             k5_l2*q_l1*Ak3*fl15*fl25 - 4.E+0*P_q*k3_l2*k4_k4*k4_l2*k5_k5*
             k5_q*l1_l1*Ak3*fl15*fl25 + 4.E+0*P_q*k3_l2*k4_k4*k4_l2*k5_k5*
             k5_l1*q_l1*Ak3*fl15*fl25 );

          Chi(8,8) +=  + F * (  - 4.E+0*P_k4*k3_k5*k4_k4*k5_k5*l1_l1*
             l2_l2*Ak3*fl15*fl25 + 4.E+0*P_k4*k3_k5*k4_k4*k5_k5*pow(
             l1_l2,2)*Ak3*fl15*fl25 + 4.E+0*P_k4*k3_l1*k4_k4*k5_k5*k5_l1*
             l2_l2*Ak3*fl15*fl25 - 4.E+0*P_k4*k3_l1*k4_k4*k5_k5*k5_l2*l1_l2
             *Ak3*fl15*fl25 - 4.E+0*P_k4*k3_l2*k4_k4*k5_k5*k5_l1*l1_l2*Ak3*
             fl15*fl25 + 4.E+0*P_k4*k3_l2*k4_k4*k5_k5*k5_l2*l1_l1*Ak3*fl15*
             fl25 + 4.E+0*P_k5*k3_k4*k4_k4*k5_k5*l1_l1*l2_l2*Ak3*fl15*fl25
              - 4.E+0*P_k5*k3_k4*k4_k4*k5_k5*pow(l1_l2,2)*Ak3*fl15*fl25 -
             4.E+0*P_k5*k3_l1*k4_k4*k4_l1*k5_k5*l2_l2*Ak3*fl15*fl25 + 4.E+0
             *P_k5*k3_l1*k4_k4*k4_l2*k5_k5*l1_l2*Ak3*fl15*fl25 + 4.E+0*P_k5
             *k3_l2*k4_k4*k4_l1*k5_k5*l1_l2*Ak3*fl15*fl25 - 4.E+0*P_k5*
             k3_l2*k4_k4*k4_l2*k5_k5*l1_l1*Ak3*fl15*fl25 - 4.E+0*P_l1*k3_k4
             *k4_k4*k5_k5*k5_l1*l2_l2*Ak3*fl15*fl25 + 4.E+0*P_l1*k3_k4*
             k4_k4*k5_k5*k5_l2*l1_l2*Ak3*fl15*fl25 + 4.E+0*P_l1*k3_k5*k4_k4
             *k4_l1*k5_k5*l2_l2*Ak3*fl15*fl25 - 4.E+0*P_l1*k3_k5*k4_k4*
             k4_l2*k5_k5*l1_l2*Ak3*fl15*fl25 - 4.E+0*P_l1*k3_l2*k4_k4*k4_l1
             *k5_k5*k5_l2*Ak3*fl15*fl25 + 4.E+0*P_l1*k3_l2*k4_k4*k4_l2*
             k5_k5*k5_l1*Ak3*fl15*fl25 + 4.E+0*P_l2*k3_k4*k4_k4*k5_k5*k5_l1
             *l1_l2*Ak3*fl15*fl25 - 4.E+0*P_l2*k3_k4*k4_k4*k5_k5*k5_l2*
             l1_l1*Ak3*fl15*fl25 - 4.E+0*P_l2*k3_k5*k4_k4*k4_l1*k5_k5*l1_l2
             *Ak3*fl15*fl25 + 4.E+0*P_l2*k3_k5*k4_k4*k4_l2*k5_k5*l1_l1*Ak3*
             fl15*fl25 + 4.E+0*P_l2*k3_l1*k4_k4*k4_l1*k5_k5*k5_l2*Ak3*fl15*
             fl25 - 4.E+0*P_l2*k3_l1*k4_k4*k4_l2*k5_k5*k5_l1*Ak3*fl15*fl25
              );

       Chi(8,9) =
           + H * (  - 8.E+0*P_k4*k4_k4*k5_k5*k5_l2*q_l1*l1_l2*Bk3*fl15*fl26
              + 8.E+0*P_k4*k4_k4*k5_k5*k5_l2*q_l2*l1_l1*Bk3*fl15*fl26 +
             8.E+0*P_k4*k4_k4*k5_q*k5_l1*k5_l2*l1_l2*Bk3*fl15*fl26 - 8.E+0*
             P_k4*k4_k4*pow(k5_l1,2)*k5_l2*q_l2*Bk3*fl15*fl26 + 8.E+0*P_k5*
             k4_k4*k4_k5*k5_l2*q_l1*l1_l2*Bk3*fl15*fl26 - 8.E+0*P_k5*k4_k4*
             k4_k5*k5_l2*q_l2*l1_l1*Bk3*fl15*fl26 - 8.E+0*P_k5*k4_k4*k4_q*
             k5_l1*k5_l2*l1_l2*Bk3*fl15*fl26 + 8.E+0*P_k5*k4_k4*k4_l1*k5_l1
             *k5_l2*q_l2*Bk3*fl15*fl26 - 8.E+0*P_l1*k4_k4*k4_k5*k5_q*k5_l2*
             l1_l2*Bk3*fl15*fl26 + 8.E+0*P_l1*k4_k4*k4_k5*k5_l1*k5_l2*q_l2*
             Bk3*fl15*fl26 + 8.E+0*P_l1*k4_k4*k4_q*k5_k5*k5_l2*l1_l2*Bk3*
             fl15*fl26 - 8.E+0*P_l1*k4_k4*k4_l1*k5_k5*k5_l2*q_l2*Bk3*fl15*
             fl26 + 8.E+0*P_l2*k4_k4*k4_k5*k5_q*k5_l2*l1_l1*Bk3*fl15*fl26
              - 8.E+0*P_l2*k4_k4*k4_k5*k5_l1*k5_l2*q_l1*Bk3*fl15*fl26 -
             8.E+0*P_l2*k4_k4*k4_q*k5_k5*k5_l2*l1_l1*Bk3*fl15*fl26 + 8.E+0*
             P_l2*k4_k4*k4_q*pow(k5_l1,2)*k5_l2*Bk3*fl15*fl26 + 8.E+0*P_l2*
             k4_k4*k4_l1*k5_k5*k5_l2*q_l1*Bk3*fl15*fl26 - 8.E+0*P_l2*k4_k4*
             k4_l1*k5_q*k5_l1*k5_l2*Bk3*fl15*fl26 );

          Chi(8,9) +=  + G * (  - 4.E+0*P_q*k3_k4*k4_k4*k5_k5*k5_l2*q_l1*
             l1_l2*Ak3*fl15*fl26 + 4.E+0*P_q*k3_k4*k4_k4*k5_k5*k5_l2*q_l2*
             l1_l1*Ak3*fl15*fl26 + 4.E+0*P_q*k3_k4*k4_k4*k5_q*k5_l1*k5_l2*
             l1_l2*Ak3*fl15*fl26 - 4.E+0*P_q*k3_k4*k4_k4*pow(k5_l1,2)*k5_l2
             *q_l2*Ak3*fl15*fl26 + 4.E+0*P_q*k3_k5*k4_k4*k4_k5*k5_l2*q_l1*
             l1_l2*Ak3*fl15*fl26 - 4.E+0*P_q*k3_k5*k4_k4*k4_k5*k5_l2*q_l2*
             l1_l1*Ak3*fl15*fl26 - 4.E+0*P_q*k3_k5*k4_k4*k4_q*k5_l1*k5_l2*
             l1_l2*Ak3*fl15*fl26 + 4.E+0*P_q*k3_k5*k4_k4*k4_l1*k5_l1*k5_l2*
             q_l2*Ak3*fl15*fl26 - 4.E+0*P_q*k3_l1*k4_k4*k4_k5*k5_q*k5_l2*
             l1_l2*Ak3*fl15*fl26 + 4.E+0*P_q*k3_l1*k4_k4*k4_k5*k5_l1*k5_l2*
             q_l2*Ak3*fl15*fl26 + 4.E+0*P_q*k3_l1*k4_k4*k4_q*k5_k5*k5_l2*
             l1_l2*Ak3*fl15*fl26 - 4.E+0*P_q*k3_l1*k4_k4*k4_l1*k5_k5*k5_l2*
             q_l2*Ak3*fl15*fl26 + 4.E+0*P_q*k3_l2*k4_k4*k4_k5*k5_q*k5_l2*
             l1_l1*Ak3*fl15*fl26 - 4.E+0*P_q*k3_l2*k4_k4*k4_k5*k5_l1*k5_l2*
             q_l1*Ak3*fl15*fl26 - 4.E+0*P_q*k3_l2*k4_k4*k4_q*k5_k5*k5_l2*
             l1_l1*Ak3*fl15*fl26 + 4.E+0*P_q*k3_l2*k4_k4*k4_q*pow(k5_l1,2)*
             k5_l2*Ak3*fl15*fl26 + 4.E+0*P_q*k3_l2*k4_k4*k4_l1*k5_k5*k5_l2*
             q_l1*Ak3*fl15*fl26 - 4.E+0*P_q*k3_l2*k4_k4*k4_l1*k5_q*k5_l1*
             k5_l2*Ak3*fl15*fl26 );

          Chi(8,9) +=  + F * (  - 4.E+0*P_k4*k3_k5*k4_k4*k5_l1*k5_l2*
             l1_l2*Ak3*fl15*fl26 + 4.E+0*P_k4*k3_l1*k4_k4*k5_k5*k5_l2*l1_l2
             *Ak3*fl15*fl26 - 4.E+0*P_k4*k3_l2*k4_k4*k5_k5*k5_l2*l1_l1*Ak3*
             fl15*fl26 + 4.E+0*P_k4*k3_l2*k4_k4*pow(k5_l1,2)*k5_l2*Ak3*fl15
             *fl26 + 4.E+0*P_k5*k3_k4*k4_k4*k5_l1*k5_l2*l1_l2*Ak3*fl15*fl26
              - 4.E+0*P_k5*k3_l1*k4_k4*k4_k5*k5_l2*l1_l2*Ak3*fl15*fl26 +
             4.E+0*P_k5*k3_l2*k4_k4*k4_k5*k5_l2*l1_l1*Ak3*fl15*fl26 - 4.E+0
             *P_k5*k3_l2*k4_k4*k4_l1*k5_l1*k5_l2*Ak3*fl15*fl26 - 4.E+0*P_l1
             *k3_k4*k4_k4*k5_k5*k5_l2*l1_l2*Ak3*fl15*fl26 + 4.E+0*P_l1*
             k3_k5*k4_k4*k4_k5*k5_l2*l1_l2*Ak3*fl15*fl26 - 4.E+0*P_l1*k3_l2
             *k4_k4*k4_k5*k5_l1*k5_l2*Ak3*fl15*fl26 + 4.E+0*P_l1*k3_l2*
             k4_k4*k4_l1*k5_k5*k5_l2*Ak3*fl15*fl26 + 4.E+0*P_l2*k3_k4*k4_k4
             *k5_k5*k5_l2*l1_l1*Ak3*fl15*fl26 - 4.E+0*P_l2*k3_k4*k4_k4*pow(
             k5_l1,2)*k5_l2*Ak3*fl15*fl26 - 4.E+0*P_l2*k3_k5*k4_k4*k4_k5*
             k5_l2*l1_l1*Ak3*fl15*fl26 + 4.E+0*P_l2*k3_k5*k4_k4*k4_l1*k5_l1
             *k5_l2*Ak3*fl15*fl26 + 4.E+0*P_l2*k3_l1*k4_k4*k4_k5*k5_l1*
             k5_l2*Ak3*fl15*fl26 - 4.E+0*P_l2*k3_l1*k4_k4*k4_l1*k5_k5*k5_l2
             *Ak3*fl15*fl26 );

       Chi(8,10) =
           + H * (  - 8.E+0*P_k3*k4_k4*k4_q*k5_l1*k5_l2*l1_l2*Ak3*fl15*fl27
              + 8.E+0*P_k3*k4_k4*k4_q*pow(k5_l2,2)*l1_l1*Ak3*fl15*fl27 +
             8.E+0*P_k3*k4_k4*k4_l1*k5_q*k5_l2*l1_l2*Ak3*fl15*fl27 - 8.E+0*
             P_k3*k4_k4*k4_l1*pow(k5_l2,2)*q_l1*Ak3*fl15*fl27 - 8.E+0*P_k3*
             k4_k4*k4_l2*k5_q*k5_l2*l1_l1*Ak3*fl15*fl27 + 8.E+0*P_k3*k4_k4*
             k4_l2*k5_l1*k5_l2*q_l1*Ak3*fl15*fl27 - 6.E+0*P_k4*k3_k5*k4_k4*
             k5_q*l1_l1*l2_l2*Ak3*fl15*fl27 + 6.E+0*P_k4*k3_k5*k4_k4*k5_q*
             pow(l1_l2,2)*Ak3*fl15*fl27 - 2.E+0*P_k4*k3_k5*k4_k4*k5_l1*q_l1
             *l2_l2*Ak3*fl15*fl27 - 6.E+0*P_k4*k3_k5*k4_k4*k5_l1*q_l2*l1_l2
             *Ak3*fl15*fl27 - 6.E+0*P_k4*k3_k5*k4_k4*k5_l2*q_l1*l1_l2*Ak3*
             fl15*fl27 + 6.E+0*P_k4*k3_k5*k4_k4*k5_l2*q_l2*l1_l1*Ak3*fl15*
             fl27 - 2.E+0*P_k4*k3_q*k4_k4*k5_k5*l1_l1*l2_l2*Ak3*fl15*fl27
              + 2.E+0*P_k4*k3_q*k4_k4*k5_k5*pow(l1_l2,2)*Ak3*fl15*fl27 +
             4.E+0*P_k4*k3_q*k4_k4*k5_l1*k5_l2*l1_l2*Ak3*fl15*fl27 + 2.E+0*
             P_k4*k3_q*k4_k4*pow(k5_l1,2)*l2_l2*Ak3*fl15*fl27 - 6.E+0*P_k4*
             k3_q*k4_k4*pow(k5_l2,2)*l1_l1*Ak3*fl15*fl27 + 1.E+1*P_k4*k3_l1
             *k4_k4*k5_k5*q_l1*l2_l2*Ak3*fl15*fl27 - 2.E+0*P_k4*k3_l1*k4_k4
             *k5_k5*q_l2*l1_l2*Ak3*fl15*fl27 - 2.E+0*P_k4*k3_l1*k4_k4*k5_q*
             k5_l1*l2_l2*Ak3*fl15*fl27 - 6.E+0*P_k4*k3_l1*k4_k4*k5_q*k5_l2*
             l1_l2*Ak3*fl15*fl27 + 2.E+0*P_k4*k3_l1*k4_k4*k5_l1*k5_l2*q_l2*
             Ak3*fl15*fl27 + 6.E+0*P_k4*k3_l1*k4_k4*pow(k5_l2,2)*q_l1*Ak3*
             fl15*fl27 - 2.E+0*P_k4*k3_l2*k4_k4*k5_k5*q_l1*l1_l2*Ak3*fl15*
             fl27 - 6.E+0*P_k4*k3_l2*k4_k4*k5_k5*q_l2*l1_l1*Ak3*fl15*fl27
              - 6.E+0*P_k4*k3_l2*k4_k4*k5_q*k5_l1*l1_l2*Ak3*fl15*fl27 +
             6.E+0*P_k4*k3_l2*k4_k4*k5_q*k5_l2*l1_l1*Ak3*fl15*fl27 + 2.E+0*
             P_k4*k3_l2*k4_k4*k5_l1*k5_l2*q_l1*Ak3*fl15*fl27 + 6.E+0*P_k4*
             k3_l2*k4_k4*pow(k5_l1,2)*q_l2*Ak3*fl15*fl27 - 2.E+0*P_k5*k3_k4
             *k4_k4*k5_q*l1_l1*l2_l2*Ak3*fl15*fl27 + 2.E+0*P_k5*k3_k4*k4_k4
             *k5_q*pow(l1_l2,2)*Ak3*fl15*fl27 + 1.E+1*P_k5*k3_k4*k4_k4*
             k5_l1*q_l1*l2_l2*Ak3*fl15*fl27 - 2.E+0*P_k5*k3_k4*k4_k4*k5_l1*
             q_l2*l1_l2*Ak3*fl15*fl27 - 2.E+0*P_k5*k3_k4*k4_k4*k5_l2*q_l1*
             l1_l2*Ak3*fl15*fl27 + 2.E+0*P_k5*k3_k4*k4_k4*k5_l2*q_l2*l1_l1*
             Ak3*fl15*fl27 + 8.E+0*P_k5*k3_k5*k4_k4*k4_q*l1_l1*l2_l2*Ak3*
             fl15*fl27 - 8.E+0*P_k5*k3_k5*k4_k4*k4_q*pow(l1_l2,2)*Ak3*fl15*
             fl27 - 8.E+0*P_k5*k3_k5*k4_k4*k4_l1*q_l1*l2_l2*Ak3*fl15*fl27
              + 8.E+0*P_k5*k3_k5*k4_k4*k4_l1*q_l2*l1_l2*Ak3*fl15*fl27 +
             8.E+0*P_k5*k3_k5*k4_k4*k4_l2*q_l1*l1_l2*Ak3*fl15*fl27 - 8.E+0*
             P_k5*k3_k5*k4_k4*k4_l2*q_l2*l1_l1*Ak3*fl15*fl27 + 2.E+0*P_k5*
             k3_q*k4_k4*k4_k5*l1_l1*l2_l2*Ak3*fl15*fl27 - 2.E+0*P_k5*k3_q*
             k4_k4*k4_k5*pow(l1_l2,2)*Ak3*fl15*fl27 - 2.E+0*P_k5*k3_q*k4_k4
             *k4_l1*k5_l1*l2_l2*Ak3*fl15*fl27 - 6.E+0*P_k5*k3_q*k4_k4*k4_l1
             *k5_l2*l1_l2*Ak3*fl15*fl27 + 2.E+0*P_k5*k3_q*k4_k4*k4_l2*k5_l1
             *l1_l2*Ak3*fl15*fl27 + 6.E+0*P_k5*k3_q*k4_k4*k4_l2*k5_l2*l1_l1
             *Ak3*fl15*fl27 - 1.E+1*P_k5*k3_l1*k4_k4*k4_k5*q_l1*l2_l2*Ak3*
             fl15*fl27 + 2.E+0*P_k5*k3_l1*k4_k4*k4_k5*q_l2*l1_l2*Ak3*fl15*
             fl27 + 8.E+0*P_k5*k3_l1*k4_k4*k4_q*k5_l2*l1_l2*Ak3*fl15*fl27
              + 2.E+0*P_k5*k3_l1*k4_k4*k4_l1*k5_q*l2_l2*Ak3*fl15*fl27 -
             2.E+0*P_k5*k3_l1*k4_k4*k4_l1*k5_l2*q_l2*Ak3*fl15*fl27 - 2.E+0*
             P_k5*k3_l1*k4_k4*k4_l2*k5_q*l1_l2*Ak3*fl15*fl27 - 6.E+0*P_k5*
             k3_l1*k4_k4*k4_l2*k5_l2*q_l1*Ak3*fl15*fl27 + 2.E+0*P_k5*k3_l2*
             k4_k4*k4_k5*q_l1*l1_l2*Ak3*fl15*fl27 );

          Chi(8,10) +=  + H * ( 6.E+0*P_k5*k3_l2*k4_k4*k4_k5*q_l2*l1_l1*
             Ak3*fl15*fl27 + 8.E+0*P_k5*k3_l2*k4_k4*k4_q*k5_l1*l1_l2*Ak3*
             fl15*fl27 - 8.E+0*P_k5*k3_l2*k4_k4*k4_q*k5_l2*l1_l1*Ak3*fl15*
             fl27 - 2.E+0*P_k5*k3_l2*k4_k4*k4_l1*k5_q*l1_l2*Ak3*fl15*fl27
              - 6.E+0*P_k5*k3_l2*k4_k4*k4_l1*k5_l1*q_l2*Ak3*fl15*fl27 +
             8.E+0*P_k5*k3_l2*k4_k4*k4_l1*k5_l2*q_l1*Ak3*fl15*fl27 + 2.E+0*
             P_k5*k3_l2*k4_k4*k4_l2*k5_q*l1_l1*Ak3*fl15*fl27 - 1.E+1*P_k5*
             k3_l2*k4_k4*k4_l2*k5_l1*q_l1*Ak3*fl15*fl27 + 2.E+0*P_q*k3_k4*
             k4_k4*k5_k5*l1_l1*l2_l2*Ak3*fl15*fl27 - 2.E+0*P_q*k3_k4*k4_k4*
             k5_k5*pow(l1_l2,2)*Ak3*fl15*fl27 + 4.E+0*P_q*k3_k4*k4_k4*k5_l1
             *k5_l2*l1_l2*Ak3*fl15*fl27 - 2.E+0*P_q*k3_k4*k4_k4*pow(
             k5_l1,2)*l2_l2*Ak3*fl15*fl27 - 2.E+0*P_q*k3_k4*k4_k4*pow(
             k5_l2,2)*l1_l1*Ak3*fl15*fl27 - 2.E+0*P_q*k3_k5*k4_k4*k4_k5*
             l1_l1*l2_l2*Ak3*fl15*fl27 + 2.E+0*P_q*k3_k5*k4_k4*k4_k5*pow(
             l1_l2,2)*Ak3*fl15*fl27 + 2.E+0*P_q*k3_k5*k4_k4*k4_l1*k5_l1*
             l2_l2*Ak3*fl15*fl27 - 2.E+0*P_q*k3_k5*k4_k4*k4_l1*k5_l2*l1_l2*
             Ak3*fl15*fl27 - 2.E+0*P_q*k3_k5*k4_k4*k4_l2*k5_l1*l1_l2*Ak3*
             fl15*fl27 + 2.E+0*P_q*k3_k5*k4_k4*k4_l2*k5_l2*l1_l1*Ak3*fl15*
             fl27 + 2.E+0*P_q*k3_l1*k4_k4*k4_k5*k5_l1*l2_l2*Ak3*fl15*fl27
              - 2.E+0*P_q*k3_l1*k4_k4*k4_k5*k5_l2*l1_l2*Ak3*fl15*fl27 -
             2.E+0*P_q*k3_l1*k4_k4*k4_l1*k5_k5*l2_l2*Ak3*fl15*fl27 + 2.E+0*
             P_q*k3_l1*k4_k4*k4_l1*pow(k5_l2,2)*Ak3*fl15*fl27 + 2.E+0*P_q*
             k3_l1*k4_k4*k4_l2*k5_k5*l1_l2*Ak3*fl15*fl27 - 2.E+0*P_q*k3_l1*
             k4_k4*k4_l2*k5_l1*k5_l2*Ak3*fl15*fl27 - 2.E+0*P_q*k3_l2*k4_k4*
             k4_k5*k5_l1*l1_l2*Ak3*fl15*fl27 + 2.E+0*P_q*k3_l2*k4_k4*k4_k5*
             k5_l2*l1_l1*Ak3*fl15*fl27 + 2.E+0*P_q*k3_l2*k4_k4*k4_l1*k5_k5*
             l1_l2*Ak3*fl15*fl27 - 2.E+0*P_q*k3_l2*k4_k4*k4_l1*k5_l1*k5_l2*
             Ak3*fl15*fl27 - 2.E+0*P_q*k3_l2*k4_k4*k4_l2*k5_k5*l1_l1*Ak3*
             fl15*fl27 + 2.E+0*P_q*k3_l2*k4_k4*k4_l2*pow(k5_l1,2)*Ak3*fl15*
             fl27 - 2.E+0*P_l1*k3_k4*k4_k4*k5_k5*q_l1*l2_l2*Ak3*fl15*fl27
              - 6.E+0*P_l1*k3_k4*k4_k4*k5_k5*q_l2*l1_l2*Ak3*fl15*fl27 -
             6.E+0*P_l1*k3_k4*k4_k4*k5_q*k5_l1*l2_l2*Ak3*fl15*fl27 - 2.E+0*
             P_l1*k3_k4*k4_k4*k5_q*k5_l2*l1_l2*Ak3*fl15*fl27 + 6.E+0*P_l1*
             k3_k4*k4_k4*k5_l1*k5_l2*q_l2*Ak3*fl15*fl27 + 2.E+0*P_l1*k3_k4*
             k4_k4*pow(k5_l2,2)*q_l1*Ak3*fl15*fl27 + 2.E+0*P_l1*k3_k5*k4_k4
             *k4_k5*q_l1*l2_l2*Ak3*fl15*fl27 + 6.E+0*P_l1*k3_k5*k4_k4*k4_k5
             *q_l2*l1_l2*Ak3*fl15*fl27 + 8.E+0*P_l1*k3_k5*k4_k4*k4_q*k5_l2*
             l1_l2*Ak3*fl15*fl27 + 6.E+0*P_l1*k3_k5*k4_k4*k4_l1*k5_q*l2_l2*
             Ak3*fl15*fl27 - 6.E+0*P_l1*k3_k5*k4_k4*k4_l1*k5_l2*q_l2*Ak3*
             fl15*fl27 - 6.E+0*P_l1*k3_k5*k4_k4*k4_l2*k5_q*l1_l2*Ak3*fl15*
             fl27 - 2.E+0*P_l1*k3_k5*k4_k4*k4_l2*k5_l2*q_l1*Ak3*fl15*fl27
              - 2.E+0*P_l1*k3_q*k4_k4*k4_k5*k5_l1*l2_l2*Ak3*fl15*fl27 +
             2.E+0*P_l1*k3_q*k4_k4*k4_k5*k5_l2*l1_l2*Ak3*fl15*fl27 + 2.E+0*
             P_l1*k3_q*k4_k4*k4_l1*k5_k5*l2_l2*Ak3*fl15*fl27 + 6.E+0*P_l1*
             k3_q*k4_k4*k4_l1*pow(k5_l2,2)*Ak3*fl15*fl27 - 2.E+0*P_l1*k3_q*
             k4_k4*k4_l2*k5_k5*l1_l2*Ak3*fl15*fl27 - 6.E+0*P_l1*k3_q*k4_k4*
             k4_l2*k5_l1*k5_l2*Ak3*fl15*fl27 + 8.E+0*P_l1*k3_l1*k4_k4*k4_k5
             *k5_q*l2_l2*Ak3*fl15*fl27 - 8.E+0*P_l1*k3_l1*k4_k4*k4_k5*k5_l2
             *q_l2*Ak3*fl15*fl27 - 8.E+0*P_l1*k3_l1*k4_k4*k4_q*k5_k5*l2_l2*
             Ak3*fl15*fl27 - 8.E+0*P_l1*k3_l1*k4_k4*k4_q*pow(k5_l2,2)*Ak3*
             fl15*fl27 );

          Chi(8,10) +=  + H * ( 8.E+0*P_l1*k3_l1*k4_k4*k4_l2*k5_k5*q_l2*
             Ak3*fl15*fl27 + 8.E+0*P_l1*k3_l1*k4_k4*k4_l2*k5_q*k5_l2*Ak3*
             fl15*fl27 - 6.E+0*P_l1*k3_l2*k4_k4*k4_k5*k5_l1*q_l2*Ak3*fl15*
             fl27 - 2.E+0*P_l1*k3_l2*k4_k4*k4_k5*k5_l2*q_l1*Ak3*fl15*fl27
              + 6.E+0*P_l1*k3_l2*k4_k4*k4_l1*k5_k5*q_l2*Ak3*fl15*fl27 -
             6.E+0*P_l1*k3_l2*k4_k4*k4_l1*k5_q*k5_l2*Ak3*fl15*fl27 + 2.E+0*
             P_l1*k3_l2*k4_k4*k4_l2*k5_k5*q_l1*Ak3*fl15*fl27 + 6.E+0*P_l1*
             k3_l2*k4_k4*k4_l2*k5_q*k5_l1*Ak3*fl15*fl27 + 1.E+1*P_l2*k3_k4*
             k4_k4*k5_k5*q_l1*l1_l2*Ak3*fl15*fl27 - 2.E+0*P_l2*k3_k4*k4_k4*
             k5_k5*q_l2*l1_l1*Ak3*fl15*fl27 - 2.E+0*P_l2*k3_k4*k4_k4*k5_q*
             k5_l1*l1_l2*Ak3*fl15*fl27 + 2.E+0*P_l2*k3_k4*k4_k4*k5_q*k5_l2*
             l1_l1*Ak3*fl15*fl27 - 1.E+1*P_l2*k3_k4*k4_k4*k5_l1*k5_l2*q_l1*
             Ak3*fl15*fl27 + 2.E+0*P_l2*k3_k4*k4_k4*pow(k5_l1,2)*q_l2*Ak3*
             fl15*fl27 - 1.E+1*P_l2*k3_k5*k4_k4*k4_k5*q_l1*l1_l2*Ak3*fl15*
             fl27 + 2.E+0*P_l2*k3_k5*k4_k4*k4_k5*q_l2*l1_l1*Ak3*fl15*fl27
              + 8.E+0*P_l2*k3_k5*k4_k4*k4_q*k5_l1*l1_l2*Ak3*fl15*fl27 -
             8.E+0*P_l2*k3_k5*k4_k4*k4_q*k5_l2*l1_l1*Ak3*fl15*fl27 - 6.E+0*
             P_l2*k3_k5*k4_k4*k4_l1*k5_q*l1_l2*Ak3*fl15*fl27 - 2.E+0*P_l2*
             k3_k5*k4_k4*k4_l1*k5_l1*q_l2*Ak3*fl15*fl27 + 8.E+0*P_l2*k3_k5*
             k4_k4*k4_l1*k5_l2*q_l1*Ak3*fl15*fl27 + 6.E+0*P_l2*k3_k5*k4_k4*
             k4_l2*k5_q*l1_l1*Ak3*fl15*fl27 + 2.E+0*P_l2*k3_k5*k4_k4*k4_l2*
             k5_l1*q_l1*Ak3*fl15*fl27 + 2.E+0*P_l2*k3_q*k4_k4*k4_k5*k5_l1*
             l1_l2*Ak3*fl15*fl27 - 2.E+0*P_l2*k3_q*k4_k4*k4_k5*k5_l2*l1_l1*
             Ak3*fl15*fl27 - 2.E+0*P_l2*k3_q*k4_k4*k4_l1*k5_k5*l1_l2*Ak3*
             fl15*fl27 + 2.E+0*P_l2*k3_q*k4_k4*k4_l1*k5_l1*k5_l2*Ak3*fl15*
             fl27 + 2.E+0*P_l2*k3_q*k4_k4*k4_l2*k5_k5*l1_l1*Ak3*fl15*fl27
              - 2.E+0*P_l2*k3_q*k4_k4*k4_l2*pow(k5_l1,2)*Ak3*fl15*fl27 -
             2.E+0*P_l2*k3_l1*k4_k4*k4_k5*k5_l1*q_l2*Ak3*fl15*fl27 + 1.E+1*
             P_l2*k3_l1*k4_k4*k4_k5*k5_l2*q_l1*Ak3*fl15*fl27 + 2.E+0*P_l2*
             k3_l1*k4_k4*k4_l1*k5_k5*q_l2*Ak3*fl15*fl27 - 2.E+0*P_l2*k3_l1*
             k4_k4*k4_l1*k5_q*k5_l2*Ak3*fl15*fl27 - 1.E+1*P_l2*k3_l1*k4_k4*
             k4_l2*k5_k5*q_l1*Ak3*fl15*fl27 + 2.E+0*P_l2*k3_l1*k4_k4*k4_l2*
             k5_q*k5_l1*Ak3*fl15*fl27 - 8.E+0*P_l2*k3_l2*k4_k4*k4_k5*k5_q*
             l1_l1*Ak3*fl15*fl27 + 8.E+0*P_l2*k3_l2*k4_k4*k4_k5*k5_l1*q_l1*
             Ak3*fl15*fl27 + 8.E+0*P_l2*k3_l2*k4_k4*k4_q*k5_k5*l1_l1*Ak3*
             fl15*fl27 - 8.E+0*P_l2*k3_l2*k4_k4*k4_q*pow(k5_l1,2)*Ak3*fl15*
             fl27 - 8.E+0*P_l2*k3_l2*k4_k4*k4_l1*k5_k5*q_l1*Ak3*fl15*fl27
              + 8.E+0*P_l2*k3_l2*k4_k4*k4_l1*k5_q*k5_l1*Ak3*fl15*fl27 );

          Chi(8,10) +=  + G * ( 4.E+0*P_q*k4_k4*k4_k5*k5_q*l1_l1*l2_l2*
             Bk3*fl15*fl27 - 4.E+0*P_q*k4_k4*k4_k5*k5_q*pow(l1_l2,2)*Bk3*
             fl15*fl27 - 4.E+0*P_q*k4_k4*k4_k5*k5_l1*q_l1*l2_l2*Bk3*fl15*
             fl27 + 4.E+0*P_q*k4_k4*k4_k5*k5_l1*q_l2*l1_l2*Bk3*fl15*fl27 +
             4.E+0*P_q*k4_k4*k4_k5*k5_l2*q_l1*l1_l2*Bk3*fl15*fl27 - 4.E+0*
             P_q*k4_k4*k4_k5*k5_l2*q_l2*l1_l1*Bk3*fl15*fl27 - 4.E+0*P_q*
             k4_k4*k4_q*k5_k5*l1_l1*l2_l2*Bk3*fl15*fl27 + 4.E+0*P_q*k4_k4*
             k4_q*k5_k5*pow(l1_l2,2)*Bk3*fl15*fl27 - 4.E+0*P_q*k4_k4*k4_q*
             k5_l1*k5_l2*l1_l2*Bk3*fl15*fl27 + 4.E+0*P_q*k4_k4*k4_q*pow(
             k5_l1,2)*l2_l2*Bk3*fl15*fl27 + 4.E+0*P_q*k4_k4*k4_l1*k5_k5*
             q_l1*l2_l2*Bk3*fl15*fl27 - 4.E+0*P_q*k4_k4*k4_l1*k5_k5*q_l2*
             l1_l2*Bk3*fl15*fl27 - 4.E+0*P_q*k4_k4*k4_l1*k5_q*k5_l1*l2_l2*
             Bk3*fl15*fl27 + 4.E+0*P_q*k4_k4*k4_l1*k5_l1*k5_l2*q_l2*Bk3*
             fl15*fl27 - 4.E+0*P_q*k4_k4*k4_l2*k5_k5*q_l1*l1_l2*Bk3*fl15*
             fl27 + 4.E+0*P_q*k4_k4*k4_l2*k5_k5*q_l2*l1_l1*Bk3*fl15*fl27 +
             4.E+0*P_q*k4_k4*k4_l2*k5_q*k5_l1*l1_l2*Bk3*fl15*fl27 - 4.E+0*
             P_q*k4_k4*k4_l2*pow(k5_l1,2)*q_l2*Bk3*fl15*fl27 );

          Chi(8,10) +=  + F * (  - 4.E+0*P_k4*k4_k4*k5_k5*l1_l1*l2_l2*Bk3
             *fl15*fl27 + 4.E+0*P_k4*k4_k4*k5_k5*pow(l1_l2,2)*Bk3*fl15*fl27
              - 4.E+0*P_k4*k4_k4*k5_l1*k5_l2*l1_l2*Bk3*fl15*fl27 + 4.E+0*
             P_k4*k4_k4*pow(k5_l1,2)*l2_l2*Bk3*fl15*fl27 + 4.E+0*P_k5*k4_k4
             *k4_k5*l1_l1*l2_l2*Bk3*fl15*fl27 - 4.E+0*P_k5*k4_k4*k4_k5*pow(
             l1_l2,2)*Bk3*fl15*fl27 - 4.E+0*P_k5*k4_k4*k4_l1*k5_l1*l2_l2*
             Bk3*fl15*fl27 + 4.E+0*P_k5*k4_k4*k4_l2*k5_l1*l1_l2*Bk3*fl15*
             fl27 - 4.E+0*P_l1*k4_k4*k4_k5*k5_l1*l2_l2*Bk3*fl15*fl27 +
             4.E+0*P_l1*k4_k4*k4_k5*k5_l2*l1_l2*Bk3*fl15*fl27 + 4.E+0*P_l1*
             k4_k4*k4_l1*k5_k5*l2_l2*Bk3*fl15*fl27 - 4.E+0*P_l1*k4_k4*k4_l2
             *k5_k5*l1_l2*Bk3*fl15*fl27 + 4.E+0*P_l2*k4_k4*k4_k5*k5_l1*
             l1_l2*Bk3*fl15*fl27 - 4.E+0*P_l2*k4_k4*k4_k5*k5_l2*l1_l1*Bk3*
             fl15*fl27 - 4.E+0*P_l2*k4_k4*k4_l1*k5_k5*l1_l2*Bk3*fl15*fl27
              + 4.E+0*P_l2*k4_k4*k4_l1*k5_l1*k5_l2*Bk3*fl15*fl27 + 4.E+0*
             P_l2*k4_k4*k4_l2*k5_k5*l1_l1*Bk3*fl15*fl27 - 4.E+0*P_l2*k4_k4*
             k4_l2*pow(k5_l1,2)*Bk3*fl15*fl27 );

          Chi(8,10) +=  + E * (  - 4.E+0*k3_k4*k4_k4*k5_k5*l1_l1*l2_l2*
             Ak3*fl15*fl27 + 4.E+0*k3_k4*k4_k4*k5_k5*pow(l1_l2,2)*Ak3*fl15*
             fl27 - 4.E+0*k3_k4*k4_k4*k5_l1*k5_l2*l1_l2*Ak3*fl15*fl27 +
             4.E+0*k3_k4*k4_k4*pow(k5_l1,2)*l2_l2*Ak3*fl15*fl27 + 4.E+0*
             k3_k5*k4_k4*k4_k5*l1_l1*l2_l2*Ak3*fl15*fl27 - 4.E+0*k3_k5*
             k4_k4*k4_k5*pow(l1_l2,2)*Ak3*fl15*fl27 - 4.E+0*k3_k5*k4_k4*
             k4_l1*k5_l1*l2_l2*Ak3*fl15*fl27 + 4.E+0*k3_k5*k4_k4*k4_l2*
             k5_l1*l1_l2*Ak3*fl15*fl27 - 4.E+0*k3_l1*k4_k4*k4_k5*k5_l1*
             l2_l2*Ak3*fl15*fl27 + 4.E+0*k3_l1*k4_k4*k4_k5*k5_l2*l1_l2*Ak3*
             fl15*fl27 + 4.E+0*k3_l1*k4_k4*k4_l1*k5_k5*l2_l2*Ak3*fl15*fl27
              - 4.E+0*k3_l1*k4_k4*k4_l2*k5_k5*l1_l2*Ak3*fl15*fl27 + 4.E+0*
             k3_l2*k4_k4*k4_k5*k5_l1*l1_l2*Ak3*fl15*fl27 - 4.E+0*k3_l2*
             k4_k4*k4_k5*k5_l2*l1_l1*Ak3*fl15*fl27 - 4.E+0*k3_l2*k4_k4*
             k4_l1*k5_k5*l1_l2*Ak3*fl15*fl27 + 4.E+0*k3_l2*k4_k4*k4_l1*
             k5_l1*k5_l2*Ak3*fl15*fl27 + 4.E+0*k3_l2*k4_k4*k4_l2*k5_k5*
             l1_l1*Ak3*fl15*fl27 - 4.E+0*k3_l2*k4_k4*k4_l2*pow(k5_l1,2)*Ak3
             *fl15*fl27 );

       Chi(9,0) =
           + H * ( 8.E+0*x2*P_k4*k4_k5*k4_l1*q_l1*Bk3*fl16 + 8.E+0*x2*P_k4*
             pow(k4_l1,2)*k5_q*Bk3*fl16 - 8.E+0*x2*P_k5*k4_k4*k4_l1*q_l1*
             Bk3*fl16 - 8.E+0*x2*P_k5*k4_q*pow(k4_l1,2)*Bk3*fl16 + 8.E+0*x2
             *P_l1*k4_k4*k4_l1*k5_q*Bk3*fl16 - 8.E+0*x2*P_l1*k4_k5*k4_q*
             k4_l1*Bk3*fl16 );

          Chi(9,0) +=  + G * ( 4.E+0*x2*P_q*k3_k4*k4_k5*k4_l1*q_l1*Ak3*
             fl16 + 4.E+0*x2*P_q*k3_k4*pow(k4_l1,2)*k5_q*Ak3*fl16 - 4.E+0*
             x2*P_q*k3_k5*k4_k4*k4_l1*q_l1*Ak3*fl16 - 4.E+0*x2*P_q*k3_k5*
             k4_q*pow(k4_l1,2)*Ak3*fl16 + 4.E+0*x2*P_q*k3_l1*k4_k4*k4_l1*
             k5_q*Ak3*fl16 - 4.E+0*x2*P_q*k3_l1*k4_k5*k4_q*k4_l1*Ak3*fl16 )
             ;

          Chi(9,0) +=  + F * (  - 4.E+0*x2*P_k4*k3_k5*pow(k4_l1,2)*Ak3*
             fl16 - 4.E+0*x2*P_k4*k3_l1*k4_k5*k4_l1*Ak3*fl16 + 4.E+0*x2*
             P_k5*k3_k4*pow(k4_l1,2)*Ak3*fl16 + 4.E+0*x2*P_k5*k3_l1*k4_k4*
             k4_l1*Ak3*fl16 + 4.E+0*x2*P_l1*k3_k4*k4_k5*k4_l1*Ak3*fl16 -
             4.E+0*x2*P_l1*k3_k5*k4_k4*k4_l1*Ak3*fl16 );

       Chi(9,1) =
           + H * ( 1.6E+1*y2*P_k4*k4_k5*k4_l1*q_l1*l2_l2*Bk3*fl16 - 1.6E+1*
             y2*P_k4*k4_k5*k4_l1*q_l2*l1_l2*Bk3*fl16 + 1.6E+1*y2*P_k4*k4_l1
             *k4_l2*k5_q*l1_l2*Bk3*fl16 - 1.6E+1*y2*P_k4*k4_l1*k4_l2*k5_l2*
             q_l1*Bk3*fl16 - 1.6E+1*y2*P_k5*k4_k4*k4_l1*q_l1*l2_l2*Bk3*fl16
              + 1.6E+1*y2*P_k5*k4_k4*k4_l1*q_l2*l1_l2*Bk3*fl16 - 1.6E+1*y2*
             P_k5*k4_q*k4_l1*k4_l2*l1_l2*Bk3*fl16 + 1.6E+1*y2*P_k5*k4_l1*
             pow(k4_l2,2)*q_l1*Bk3*fl16 + 1.6E+1*y2*P_l1*k4_k4*k4_l1*k5_q*
             l2_l2*Bk3*fl16 - 1.6E+1*y2*P_l1*k4_k4*k4_l1*k5_l2*q_l2*Bk3*
             fl16 - 1.6E+1*y2*P_l1*k4_k5*k4_q*k4_l1*l2_l2*Bk3*fl16 + 1.6E+1
             *y2*P_l1*k4_k5*k4_l1*k4_l2*q_l2*Bk3*fl16 + 1.6E+1*y2*P_l1*k4_q
             *k4_l1*k4_l2*k5_l2*Bk3*fl16 - 1.6E+1*y2*P_l1*k4_l1*pow(
             k4_l2,2)*k5_q*Bk3*fl16 - 1.6E+1*y2*P_l2*k4_k4*k4_l1*k5_q*l1_l2
             *Bk3*fl16 + 1.6E+1*y2*P_l2*k4_k4*k4_l1*k5_l2*q_l1*Bk3*fl16 +
             1.6E+1*y2*P_l2*k4_k5*k4_q*k4_l1*l1_l2*Bk3*fl16 - 1.6E+1*y2*
             P_l2*k4_k5*k4_l1*k4_l2*q_l1*Bk3*fl16 );

          Chi(9,1) +=  + G * ( 8.E+0*y2*P_q*k3_k4*k4_k5*k4_l1*q_l1*l2_l2*
             Ak3*fl16 - 8.E+0*y2*P_q*k3_k4*k4_k5*k4_l1*q_l2*l1_l2*Ak3*fl16
              + 8.E+0*y2*P_q*k3_k4*k4_l1*k4_l2*k5_q*l1_l2*Ak3*fl16 - 8.E+0*
             y2*P_q*k3_k4*k4_l1*k4_l2*k5_l2*q_l1*Ak3*fl16 - 8.E+0*y2*P_q*
             k3_k5*k4_k4*k4_l1*q_l1*l2_l2*Ak3*fl16 + 8.E+0*y2*P_q*k3_k5*
             k4_k4*k4_l1*q_l2*l1_l2*Ak3*fl16 - 8.E+0*y2*P_q*k3_k5*k4_q*
             k4_l1*k4_l2*l1_l2*Ak3*fl16 + 8.E+0*y2*P_q*k3_k5*k4_l1*pow(
             k4_l2,2)*q_l1*Ak3*fl16 + 8.E+0*y2*P_q*k3_l1*k4_k4*k4_l1*k5_q*
             l2_l2*Ak3*fl16 - 8.E+0*y2*P_q*k3_l1*k4_k4*k4_l1*k5_l2*q_l2*Ak3
             *fl16 - 8.E+0*y2*P_q*k3_l1*k4_k5*k4_q*k4_l1*l2_l2*Ak3*fl16 +
             8.E+0*y2*P_q*k3_l1*k4_k5*k4_l1*k4_l2*q_l2*Ak3*fl16 + 8.E+0*y2*
             P_q*k3_l1*k4_q*k4_l1*k4_l2*k5_l2*Ak3*fl16 - 8.E+0*y2*P_q*k3_l1
             *k4_l1*pow(k4_l2,2)*k5_q*Ak3*fl16 - 8.E+0*y2*P_q*k3_l2*k4_k4*
             k4_l1*k5_q*l1_l2*Ak3*fl16 + 8.E+0*y2*P_q*k3_l2*k4_k4*k4_l1*
             k5_l2*q_l1*Ak3*fl16 + 8.E+0*y2*P_q*k3_l2*k4_k5*k4_q*k4_l1*
             l1_l2*Ak3*fl16 - 8.E+0*y2*P_q*k3_l2*k4_k5*k4_l1*k4_l2*q_l1*Ak3
             *fl16 );

          Chi(9,1) +=  + F * (  - 8.E+0*y2*P_k4*k3_k5*k4_l1*k4_l2*l1_l2*
             Ak3*fl16 - 8.E+0*y2*P_k4*k3_l1*k4_k5*k4_l1*l2_l2*Ak3*fl16 +
             8.E+0*y2*P_k4*k3_l1*k4_l1*k4_l2*k5_l2*Ak3*fl16 + 8.E+0*y2*P_k4
             *k3_l2*k4_k5*k4_l1*l1_l2*Ak3*fl16 + 8.E+0*y2*P_k5*k3_k4*k4_l1*
             k4_l2*l1_l2*Ak3*fl16 + 8.E+0*y2*P_k5*k3_l1*k4_k4*k4_l1*l2_l2*
             Ak3*fl16 - 8.E+0*y2*P_k5*k3_l1*k4_l1*pow(k4_l2,2)*Ak3*fl16 -
             8.E+0*y2*P_k5*k3_l2*k4_k4*k4_l1*l1_l2*Ak3*fl16 + 8.E+0*y2*P_l1
             *k3_k4*k4_k5*k4_l1*l2_l2*Ak3*fl16 - 8.E+0*y2*P_l1*k3_k4*k4_l1*
             k4_l2*k5_l2*Ak3*fl16 - 8.E+0*y2*P_l1*k3_k5*k4_k4*k4_l1*l2_l2*
             Ak3*fl16 + 8.E+0*y2*P_l1*k3_k5*k4_l1*pow(k4_l2,2)*Ak3*fl16 +
             8.E+0*y2*P_l1*k3_l2*k4_k4*k4_l1*k5_l2*Ak3*fl16 - 8.E+0*y2*P_l1
             *k3_l2*k4_k5*k4_l1*k4_l2*Ak3*fl16 - 8.E+0*y2*P_l2*k3_k4*k4_k5*
             k4_l1*l1_l2*Ak3*fl16 + 8.E+0*y2*P_l2*k3_k5*k4_k4*k4_l1*l1_l2*
             Ak3*fl16 - 8.E+0*y2*P_l2*k3_l1*k4_k4*k4_l1*k5_l2*Ak3*fl16 +
             8.E+0*y2*P_l2*k3_l1*k4_k5*k4_l1*k4_l2*Ak3*fl16 );

       Chi(9,2) =
           + H * ( 1.6E+1*z2*P_k4*k3_k5*k4_l1*k4_l2*q_l1*Ak3*fl16 + 1.6E+1*
             z2*P_k4*k3_l1*k4_k5*k4_l1*q_l2*Ak3*fl16 - 1.6E+1*z2*P_k4*k3_l1
             *k4_l1*k4_l2*k5_q*Ak3*fl16 - 1.6E+1*z2*P_k4*k3_l2*k4_k5*k4_l1*
             q_l1*Ak3*fl16 - 1.6E+1*z2*P_k5*k3_k4*k4_l1*k4_l2*q_l1*Ak3*fl16
              - 1.6E+1*z2*P_k5*k3_l1*k4_k4*k4_l1*q_l2*Ak3*fl16 + 1.6E+1*z2*
             P_k5*k3_l1*k4_q*k4_l1*k4_l2*Ak3*fl16 + 1.6E+1*z2*P_k5*k3_l2*
             k4_k4*k4_l1*q_l1*Ak3*fl16 - 1.6E+1*z2*P_l1*k3_k4*k4_k5*k4_l1*
             q_l2*Ak3*fl16 + 1.6E+1*z2*P_l1*k3_k4*k4_l1*k4_l2*k5_q*Ak3*fl16
              + 1.6E+1*z2*P_l1*k3_k5*k4_k4*k4_l1*q_l2*Ak3*fl16 - 1.6E+1*z2*
             P_l1*k3_k5*k4_q*k4_l1*k4_l2*Ak3*fl16 - 1.6E+1*z2*P_l1*k3_l2*
             k4_k4*k4_l1*k5_q*Ak3*fl16 + 1.6E+1*z2*P_l1*k3_l2*k4_k5*k4_q*
             k4_l1*Ak3*fl16 + 1.6E+1*z2*P_l2*k3_k4*k4_k5*k4_l1*q_l1*Ak3*
             fl16 - 1.6E+1*z2*P_l2*k3_k5*k4_k4*k4_l1*q_l1*Ak3*fl16 + 1.6E+1
             *z2*P_l2*k3_l1*k4_k4*k4_l1*k5_q*Ak3*fl16 - 1.6E+1*z2*P_l2*
             k3_l1*k4_k5*k4_q*k4_l1*Ak3*fl16 );

       Chi(9,3) =
           + H * ( 8.E+0*P_k4*k4_k5*k4_l1*k5_k5*q_l1*Bk3*fl16*fl20 + 8.E+0*
             P_k4*pow(k4_l1,2)*k5_k5*k5_q*Bk3*fl16*fl20 - 8.E+0*P_k5*k4_k4*
             k4_l1*k5_k5*q_l1*Bk3*fl16*fl20 - 8.E+0*P_k5*k4_q*pow(k4_l1,2)*
             k5_k5*Bk3*fl16*fl20 + 8.E+0*P_l1*k4_k4*k4_l1*k5_k5*k5_q*Bk3*
             fl16*fl20 - 8.E+0*P_l1*k4_k5*k4_q*k4_l1*k5_k5*Bk3*fl16*fl20 );

          Chi(9,3) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k5*k4_l1*k5_k5*q_l1*Ak3
             *fl16*fl20 + 4.E+0*P_q*k3_k4*pow(k4_l1,2)*k5_k5*k5_q*Ak3*fl16*
             fl20 - 4.E+0*P_q*k3_k5*k4_k4*k4_l1*k5_k5*q_l1*Ak3*fl16*fl20 -
             4.E+0*P_q*k3_k5*k4_q*pow(k4_l1,2)*k5_k5*Ak3*fl16*fl20 + 4.E+0*
             P_q*k3_l1*k4_k4*k4_l1*k5_k5*k5_q*Ak3*fl16*fl20 - 4.E+0*P_q*
             k3_l1*k4_k5*k4_q*k4_l1*k5_k5*Ak3*fl16*fl20 );

          Chi(9,3) +=  + F * (  - 4.E+0*P_k4*k3_k5*pow(k4_l1,2)*k5_k5*Ak3
             *fl16*fl20 - 4.E+0*P_k4*k3_l1*k4_k5*k4_l1*k5_k5*Ak3*fl16*fl20
              + 4.E+0*P_k5*k3_k4*pow(k4_l1,2)*k5_k5*Ak3*fl16*fl20 + 4.E+0*
             P_k5*k3_l1*k4_k4*k4_l1*k5_k5*Ak3*fl16*fl20 + 4.E+0*P_l1*k3_k4*
             k4_k5*k4_l1*k5_k5*Ak3*fl16*fl20 - 4.E+0*P_l1*k3_k5*k4_k4*k4_l1
             *k5_k5*Ak3*fl16*fl20 );

       Chi(9,4) =
           + H * (  - 8.E+0*P_k3*k4_k4*k4_l1*k5_k5*k5_q*k5_l2*l1_l2*Ak3*
             fl16*fl21 + 8.E+0*P_k3*k4_k4*k4_l1*k5_k5*pow(k5_l2,2)*q_l1*Ak3
             *fl16*fl21 + 8.E+0*P_k3*k4_k5*k4_q*k4_l1*k5_k5*k5_l2*l1_l2*Ak3
             *fl16*fl21 - 8.E+0*P_k3*k4_k5*k4_l1*k4_l2*k5_k5*k5_l2*q_l1*Ak3
             *fl16*fl21 - 8.E+0*P_k3*k4_q*pow(k4_l1,2)*k5_k5*pow(k5_l2,2)*
             Ak3*fl16*fl21 + 8.E+0*P_k3*pow(k4_l1,2)*k4_l2*k5_k5*k5_q*k5_l2
             *Ak3*fl16*fl21 + 1.6E+1*P_k4*k3_k4*k4_l1*k5_k5*k5_q*k5_l2*
             l1_l2*Ak3*fl16*fl21 - 1.6E+1*P_k4*k3_k4*k4_l1*k5_k5*pow(
             k5_l2,2)*q_l1*Ak3*fl16*fl21 + 8.E+0*P_k4*k3_k5*k4_l1*k4_l2*
             k5_k5*k5_l2*q_l1*Ak3*fl16*fl21 - 8.E+0*P_k4*k3_k5*pow(k4_l1,2)
             *k5_k5*k5_l2*q_l2*Ak3*fl16*fl21 - 8.E+0*P_k4*k3_q*k4_k5*k4_l1*
             k5_k5*k5_l2*l1_l2*Ak3*fl16*fl21 + 8.E+0*P_k4*k3_q*pow(k4_l1,2)
             *k5_k5*pow(k5_l2,2)*Ak3*fl16*fl21 - 8.E+0*P_k4*k3_l1*k4_l1*
             k4_l2*k5_k5*k5_q*k5_l2*Ak3*fl16*fl21 - 8.E+0*P_k4*k3_l2*pow(
             k4_l1,2)*k5_k5*k5_q*k5_l2*Ak3*fl16*fl21 - 1.6E+1*P_k5*k3_k4*
             k4_q*k4_l1*k5_k5*k5_l2*l1_l2*Ak3*fl16*fl21 + 8.E+0*P_k5*k3_k4*
             k4_l1*k4_l2*k5_k5*k5_l2*q_l1*Ak3*fl16*fl21 + 8.E+0*P_k5*k3_k4*
             pow(k4_l1,2)*k5_k5*k5_l2*q_l2*Ak3*fl16*fl21 + 8.E+0*P_k5*k3_q*
             k4_k4*k4_l1*k5_k5*k5_l2*l1_l2*Ak3*fl16*fl21 - 8.E+0*P_k5*k3_q*
             pow(k4_l1,2)*k4_l2*k5_k5*k5_l2*Ak3*fl16*fl21 + 8.E+0*P_k5*
             k3_l1*k4_q*k4_l1*k4_l2*k5_k5*k5_l2*Ak3*fl16*fl21 + 8.E+0*P_k5*
             k3_l2*k4_q*pow(k4_l1,2)*k5_k5*k5_l2*Ak3*fl16*fl21 + 1.6E+1*
             P_l1*k3_k4*k4_q*k4_l1*k5_k5*pow(k5_l2,2)*Ak3*fl16*fl21 - 8.E+0
             *P_l1*k3_k4*k4_l1*k4_l2*k5_k5*k5_q*k5_l2*Ak3*fl16*fl21 - 8.E+0
             *P_l1*k3_k5*k4_q*k4_l1*k4_l2*k5_k5*k5_l2*Ak3*fl16*fl21 - 8.E+0
             *P_l1*k3_q*k4_k4*k4_l1*k5_k5*pow(k5_l2,2)*Ak3*fl16*fl21 +
             8.E+0*P_l1*k3_q*k4_k5*k4_l1*k4_l2*k5_k5*k5_l2*Ak3*fl16*fl21 -
             8.E+0*P_l2*k3_k4*pow(k4_l1,2)*k5_k5*k5_q*k5_l2*Ak3*fl16*fl21
              + 8.E+0*P_l2*k3_k5*k4_q*pow(k4_l1,2)*k5_k5*k5_l2*Ak3*fl16*
             fl21 );

          Chi(9,4) +=  + G * ( 4.E+0*P_q*k4_k4*k4_l1*k5_k5*k5_q*k5_l2*
             l1_l2*Bk3*fl16*fl21 - 4.E+0*P_q*k4_k4*k4_l1*k5_k5*pow(k5_l2,2)
             *q_l1*Bk3*fl16*fl21 - 4.E+0*P_q*k4_k5*k4_q*k4_l1*k5_k5*k5_l2*
             l1_l2*Bk3*fl16*fl21 + 4.E+0*P_q*k4_k5*k4_l1*k4_l2*k5_k5*k5_l2*
             q_l1*Bk3*fl16*fl21 - 4.E+0*P_q*k4_q*pow(k4_l1,2)*k5_k5*pow(
             k5_l2,2)*Bk3*fl16*fl21 + 4.E+0*P_q*pow(k4_l1,2)*k4_l2*k5_k5*
             k5_q*k5_l2*Bk3*fl16*fl21 );

          Chi(9,4) +=  + F * (  - 4.E+0*P_k4*k4_k5*k4_l1*k5_k5*k5_l2*
             l1_l2*Bk3*fl16*fl21 - 4.E+0*P_k4*pow(k4_l1,2)*k5_k5*pow(
             k5_l2,2)*Bk3*fl16*fl21 + 4.E+0*P_k5*k4_k4*k4_l1*k5_k5*k5_l2*
             l1_l2*Bk3*fl16*fl21 + 4.E+0*P_k5*pow(k4_l1,2)*k4_l2*k5_k5*
             k5_l2*Bk3*fl16*fl21 - 4.E+0*P_l1*k4_k4*k4_l1*k5_k5*pow(
             k5_l2,2)*Bk3*fl16*fl21 + 4.E+0*P_l1*k4_k5*k4_l1*k4_l2*k5_k5*
             k5_l2*Bk3*fl16*fl21 );

          Chi(9,4) +=  + E * (  - 4.E+0*k3_k4*k4_k5*k4_l1*k5_k5*k5_l2*
             l1_l2*Ak3*fl16*fl21 - 4.E+0*k3_k4*pow(k4_l1,2)*k5_k5*pow(
             k5_l2,2)*Ak3*fl16*fl21 + 4.E+0*k3_k5*k4_k4*k4_l1*k5_k5*k5_l2*
             l1_l2*Ak3*fl16*fl21 + 4.E+0*k3_k5*pow(k4_l1,2)*k4_l2*k5_k5*
             k5_l2*Ak3*fl16*fl21 - 4.E+0*k3_l1*k4_k4*k4_l1*k5_k5*pow(
             k5_l2,2)*Ak3*fl16*fl21 + 4.E+0*k3_l1*k4_k5*k4_l1*k4_l2*k5_k5*
             k5_l2*Ak3*fl16*fl21 );

       Chi(9,5) =
           + H * ( 8.E+0*P_k3*k4_k4*k4_l1*k5_k5*q_l1*Ak3*fl16*fl22 - 8.E+0*
             P_k3*k4_k4*k4_l1*k5_q*k5_l1*Ak3*fl16*fl22 + 8.E+0*P_k3*k4_k5*
             k4_q*k4_l1*k5_l1*Ak3*fl16*fl22 + 8.E+0*P_k3*k4_k5*pow(k4_l1,2)
             *k5_q*Ak3*fl16*fl22 - 8.E+0*P_k3*pow(k4_k5,2)*k4_l1*q_l1*Ak3*
             fl16*fl22 - 8.E+0*P_k3*k4_q*pow(k4_l1,2)*k5_k5*Ak3*fl16*fl22
              - 1.6E+1*P_k4*k3_k4*k4_l1*k5_k5*q_l1*Ak3*fl16*fl22 + 1.6E+1*
             P_k4*k3_k4*k4_l1*k5_q*k5_l1*Ak3*fl16*fl22 + 8.E+0*P_k4*k3_k5*
             k4_k5*k4_l1*q_l1*Ak3*fl16*fl22 - 1.6E+1*P_k4*k3_k5*pow(
             k4_l1,2)*k5_q*Ak3*fl16*fl22 - 8.E+0*P_k4*k3_q*k4_k5*k4_l1*
             k5_l1*Ak3*fl16*fl22 + 8.E+0*P_k4*k3_q*pow(k4_l1,2)*k5_k5*Ak3*
             fl16*fl22 - 8.E+0*P_k4*k3_l1*k4_k5*k4_l1*k5_q*Ak3*fl16*fl22 +
             8.E+0*P_k5*k3_k4*k4_k5*k4_l1*q_l1*Ak3*fl16*fl22 - 1.6E+1*P_k5*
             k3_k4*k4_q*k4_l1*k5_l1*Ak3*fl16*fl22 + 1.6E+1*P_k5*k3_k5*k4_q*
             pow(k4_l1,2)*Ak3*fl16*fl22 + 8.E+0*P_k5*k3_q*k4_k4*k4_l1*k5_l1
             *Ak3*fl16*fl22 - 8.E+0*P_k5*k3_q*k4_k5*pow(k4_l1,2)*Ak3*fl16*
             fl22 + 8.E+0*P_k5*k3_l1*k4_k5*k4_q*k4_l1*Ak3*fl16*fl22 - 8.E+0
             *P_l1*k3_k4*k4_k5*k4_l1*k5_q*Ak3*fl16*fl22 + 1.6E+1*P_l1*k3_k4
             *k4_q*k4_l1*k5_k5*Ak3*fl16*fl22 - 8.E+0*P_l1*k3_k5*k4_k5*k4_q*
             k4_l1*Ak3*fl16*fl22 - 8.E+0*P_l1*k3_q*k4_k4*k4_l1*k5_k5*Ak3*
             fl16*fl22 + 8.E+0*P_l1*k3_q*pow(k4_k5,2)*k4_l1*Ak3*fl16*fl22 )
             ;

          Chi(9,5) +=  + G * (  - 4.E+0*P_q*k4_k4*k4_l1*k5_k5*q_l1*Bk3*
             fl16*fl22 + 4.E+0*P_q*k4_k4*k4_l1*k5_q*k5_l1*Bk3*fl16*fl22 -
             4.E+0*P_q*k4_k5*k4_q*k4_l1*k5_l1*Bk3*fl16*fl22 + 4.E+0*P_q*
             k4_k5*pow(k4_l1,2)*k5_q*Bk3*fl16*fl22 + 4.E+0*P_q*pow(k4_k5,2)
             *k4_l1*q_l1*Bk3*fl16*fl22 - 4.E+0*P_q*k4_q*pow(k4_l1,2)*k5_k5*
             Bk3*fl16*fl22 );

          Chi(9,5) +=  + F * (  - 4.E+0*P_k4*k4_k5*k4_l1*k5_l1*Bk3*fl16*
             fl22 - 4.E+0*P_k4*pow(k4_l1,2)*k5_k5*Bk3*fl16*fl22 + 4.E+0*
             P_k5*k4_k4*k4_l1*k5_l1*Bk3*fl16*fl22 + 4.E+0*P_k5*k4_k5*pow(
             k4_l1,2)*Bk3*fl16*fl22 - 4.E+0*P_l1*k4_k4*k4_l1*k5_k5*Bk3*fl16
             *fl22 + 4.E+0*P_l1*pow(k4_k5,2)*k4_l1*Bk3*fl16*fl22 );

          Chi(9,5) +=  + E * (  - 4.E+0*k3_k4*k4_k5*k4_l1*k5_l1*Ak3*fl16*
             fl22 - 4.E+0*k3_k4*pow(k4_l1,2)*k5_k5*Ak3*fl16*fl22 + 4.E+0*
             k3_k5*k4_k4*k4_l1*k5_l1*Ak3*fl16*fl22 + 4.E+0*k3_k5*k4_k5*pow(
             k4_l1,2)*Ak3*fl16*fl22 - 4.E+0*k3_l1*k4_k4*k4_l1*k5_k5*Ak3*
             fl16*fl22 + 4.E+0*k3_l1*pow(k4_k5,2)*k4_l1*Ak3*fl16*fl22 );

       Chi(9,6) =
           + H * ( 8.E+0*P_k4*k4_k5*k4_l1*k5_q*l1_l2*Bk3*fl16*fl23 - 8.E+0*
             P_k4*k4_k5*k4_l1*k5_l2*q_l1*Bk3*fl16*fl23 + 8.E+0*P_k4*k4_l1*
             k4_l2*k5_k5*q_l1*Bk3*fl16*fl23 - 8.E+0*P_k4*k4_l1*k4_l2*k5_q*
             k5_l1*Bk3*fl16*fl23 - 8.E+0*P_k4*pow(k4_l1,2)*k5_k5*q_l2*Bk3*
             fl16*fl23 + 8.E+0*P_k4*pow(k4_l1,2)*k5_q*k5_l2*Bk3*fl16*fl23
              - 8.E+0*P_k5*k4_k5*k4_q*k4_l1*l1_l2*Bk3*fl16*fl23 + 8.E+0*
             P_k5*k4_k5*pow(k4_l1,2)*q_l2*Bk3*fl16*fl23 + 8.E+0*P_k5*k4_q*
             k4_l1*k4_l2*k5_l1*Bk3*fl16*fl23 - 8.E+0*P_k5*k4_q*pow(k4_l1,2)
             *k5_l2*Bk3*fl16*fl23 + 8.E+0*P_l1*k4_k5*k4_q*k4_l1*k5_l2*Bk3*
             fl16*fl23 - 8.E+0*P_l1*k4_q*k4_l1*k4_l2*k5_k5*Bk3*fl16*fl23 -
             8.E+0*P_l2*k4_k5*pow(k4_l1,2)*k5_q*Bk3*fl16*fl23 + 8.E+0*P_l2*
             k4_q*pow(k4_l1,2)*k5_k5*Bk3*fl16*fl23 );

          Chi(9,6) +=  + G * (  - 4.E+0*P_q*k3_k4*k4_k5*k4_l1*k5_q*l1_l2*
             Ak3*fl16*fl23 + 4.E+0*P_q*k3_k4*k4_k5*k4_l1*k5_l2*q_l1*Ak3*
             fl16*fl23 + 8.E+0*P_q*k3_k4*k4_q*k4_l1*k5_k5*l1_l2*Ak3*fl16*
             fl23 - 8.E+0*P_q*k3_k4*k4_q*k4_l1*k5_l1*k5_l2*Ak3*fl16*fl23 -
             4.E+0*P_q*k3_k4*k4_l1*k4_l2*k5_k5*q_l1*Ak3*fl16*fl23 + 4.E+0*
             P_q*k3_k4*k4_l1*k4_l2*k5_q*k5_l1*Ak3*fl16*fl23 - 4.E+0*P_q*
             k3_k4*pow(k4_l1,2)*k5_k5*q_l2*Ak3*fl16*fl23 + 4.E+0*P_q*k3_k4*
             pow(k4_l1,2)*k5_q*k5_l2*Ak3*fl16*fl23 - 4.E+0*P_q*k3_k5*k4_k5*
             k4_q*k4_l1*l1_l2*Ak3*fl16*fl23 + 4.E+0*P_q*k3_k5*k4_k5*pow(
             k4_l1,2)*q_l2*Ak3*fl16*fl23 + 4.E+0*P_q*k3_k5*k4_q*k4_l1*k4_l2
             *k5_l1*Ak3*fl16*fl23 + 4.E+0*P_q*k3_k5*k4_q*pow(k4_l1,2)*k5_l2
             *Ak3*fl16*fl23 - 8.E+0*P_q*k3_k5*pow(k4_l1,2)*k4_l2*k5_q*Ak3*
             fl16*fl23 - 4.E+0*P_q*k3_q*k4_k4*k4_l1*k5_k5*l1_l2*Ak3*fl16*
             fl23 + 4.E+0*P_q*k3_q*k4_k4*k4_l1*k5_l1*k5_l2*Ak3*fl16*fl23 -
             4.E+0*P_q*k3_q*k4_k5*k4_l1*k4_l2*k5_l1*Ak3*fl16*fl23 - 4.E+0*
             P_q*k3_q*k4_k5*pow(k4_l1,2)*k5_l2*Ak3*fl16*fl23 + 4.E+0*P_q*
             k3_q*pow(k4_k5,2)*k4_l1*l1_l2*Ak3*fl16*fl23 + 4.E+0*P_q*k3_q*
             pow(k4_l1,2)*k4_l2*k5_k5*Ak3*fl16*fl23 + 4.E+0*P_q*k3_l1*k4_k5
             *k4_q*k4_l1*k5_l2*Ak3*fl16*fl23 - 4.E+0*P_q*k3_l1*k4_q*k4_l1*
             k4_l2*k5_k5*Ak3*fl16*fl23 + 4.E+0*P_q*k3_l2*k4_k5*pow(k4_l1,2)
             *k5_q*Ak3*fl16*fl23 - 4.E+0*P_q*k3_l2*k4_q*pow(k4_l1,2)*k5_k5*
             Ak3*fl16*fl23 );

          Chi(9,6) +=  + F * (  - 4.E+0*P_k3*k4_k4*k4_l1*k5_k5*l1_l2*Ak3*
             fl16*fl23 + 4.E+0*P_k3*k4_k4*k4_l1*k5_l1*k5_l2*Ak3*fl16*fl23
              - 4.E+0*P_k3*k4_k5*k4_l1*k4_l2*k5_l1*Ak3*fl16*fl23 - 4.E+0*
             P_k3*k4_k5*pow(k4_l1,2)*k5_l2*Ak3*fl16*fl23 + 4.E+0*P_k3*pow(
             k4_k5,2)*k4_l1*l1_l2*Ak3*fl16*fl23 + 4.E+0*P_k3*pow(k4_l1,2)*
             k4_l2*k5_k5*Ak3*fl16*fl23 + 8.E+0*P_k4*k3_k4*k4_l1*k5_k5*l1_l2
             *Ak3*fl16*fl23 - 8.E+0*P_k4*k3_k4*k4_l1*k5_l1*k5_l2*Ak3*fl16*
             fl23 - 4.E+0*P_k4*k3_k5*k4_k5*k4_l1*l1_l2*Ak3*fl16*fl23 +
             4.E+0*P_k4*k3_k5*k4_l1*k4_l2*k5_l1*Ak3*fl16*fl23 + 4.E+0*P_k4*
             k3_k5*pow(k4_l1,2)*k5_l2*Ak3*fl16*fl23 + 4.E+0*P_k4*k3_l1*
             k4_k5*k4_l1*k5_l2*Ak3*fl16*fl23 - 4.E+0*P_k4*k3_l1*k4_l1*k4_l2
             *k5_k5*Ak3*fl16*fl23 - 4.E+0*P_k4*k3_l2*pow(k4_l1,2)*k5_k5*Ak3
             *fl16*fl23 - 4.E+0*P_k5*k3_k4*k4_k5*k4_l1*l1_l2*Ak3*fl16*fl23
              + 4.E+0*P_k5*k3_k4*k4_l1*k4_l2*k5_l1*Ak3*fl16*fl23 + 4.E+0*
             P_k5*k3_k4*pow(k4_l1,2)*k5_l2*Ak3*fl16*fl23 - 8.E+0*P_k5*k3_k5
             *pow(k4_l1,2)*k4_l2*Ak3*fl16*fl23 + 4.E+0*P_k5*k3_l2*k4_k5*
             pow(k4_l1,2)*Ak3*fl16*fl23 + 4.E+0*P_l1*k3_k4*k4_k5*k4_l1*
             k5_l2*Ak3*fl16*fl23 - 4.E+0*P_l1*k3_k4*k4_l1*k4_l2*k5_k5*Ak3*
             fl16*fl23 - 4.E+0*P_l2*k3_k4*pow(k4_l1,2)*k5_k5*Ak3*fl16*fl23
              + 4.E+0*P_l2*k3_k5*k4_k5*pow(k4_l1,2)*Ak3*fl16*fl23 );

          Chi(9,6) +=  + E * ( 4.E+0*k4_k4*k4_l1*k5_k5*l1_l2*Bk3*fl16*
             fl23 - 4.E+0*k4_k4*k4_l1*k5_l1*k5_l2*Bk3*fl16*fl23 + 4.E+0*
             k4_k5*k4_l1*k4_l2*k5_l1*Bk3*fl16*fl23 - 4.E+0*k4_k5*pow(
             k4_l1,2)*k5_l2*Bk3*fl16*fl23 - 4.E+0*pow(k4_k5,2)*k4_l1*l1_l2*
             Bk3*fl16*fl23 + 4.E+0*pow(k4_l1,2)*k4_l2*k5_k5*Bk3*fl16*fl23 )
             ;

       Chi(9,7) =
           + H * (  - 8.E+0*P_k4*k3_k5*k4_l1*k4_l2*k5_k5*q_l1*Ak3*fl16*fl24
              - 8.E+0*P_k4*k3_l1*k4_k5*k4_l1*k5_k5*q_l2*Ak3*fl16*fl24 +
             8.E+0*P_k4*k3_l1*k4_l1*k4_l2*k5_k5*k5_q*Ak3*fl16*fl24 + 8.E+0*
             P_k4*k3_l2*k4_k5*k4_l1*k5_k5*q_l1*Ak3*fl16*fl24 + 8.E+0*P_k5*
             k3_k4*k4_l1*k4_l2*k5_k5*q_l1*Ak3*fl16*fl24 + 8.E+0*P_k5*k3_l1*
             k4_k4*k4_l1*k5_k5*q_l2*Ak3*fl16*fl24 - 8.E+0*P_k5*k3_l1*k4_q*
             k4_l1*k4_l2*k5_k5*Ak3*fl16*fl24 - 8.E+0*P_k5*k3_l2*k4_k4*k4_l1
             *k5_k5*q_l1*Ak3*fl16*fl24 + 8.E+0*P_l1*k3_k4*k4_k5*k4_l1*k5_k5
             *q_l2*Ak3*fl16*fl24 - 8.E+0*P_l1*k3_k4*k4_l1*k4_l2*k5_k5*k5_q*
             Ak3*fl16*fl24 - 8.E+0*P_l1*k3_k5*k4_k4*k4_l1*k5_k5*q_l2*Ak3*
             fl16*fl24 + 8.E+0*P_l1*k3_k5*k4_q*k4_l1*k4_l2*k5_k5*Ak3*fl16*
             fl24 + 8.E+0*P_l1*k3_l2*k4_k4*k4_l1*k5_k5*k5_q*Ak3*fl16*fl24
              - 8.E+0*P_l1*k3_l2*k4_k5*k4_q*k4_l1*k5_k5*Ak3*fl16*fl24 -
             8.E+0*P_l2*k3_k4*k4_k5*k4_l1*k5_k5*q_l1*Ak3*fl16*fl24 + 8.E+0*
             P_l2*k3_k5*k4_k4*k4_l1*k5_k5*q_l1*Ak3*fl16*fl24 - 8.E+0*P_l2*
             k3_l1*k4_k4*k4_l1*k5_k5*k5_q*Ak3*fl16*fl24 + 8.E+0*P_l2*k3_l1*
             k4_k5*k4_q*k4_l1*k5_k5*Ak3*fl16*fl24 );

       Chi(9,8) =
           + H * ( 8.E+0*P_k4*k4_k5*k4_l1*k5_k5*q_l1*l2_l2*Bk3*fl16*fl25 -
             8.E+0*P_k4*k4_k5*k4_l1*k5_k5*q_l2*l1_l2*Bk3*fl16*fl25 + 8.E+0*
             P_k4*k4_l1*k4_l2*k5_k5*k5_q*l1_l2*Bk3*fl16*fl25 - 8.E+0*P_k4*
             k4_l1*k4_l2*k5_k5*k5_l2*q_l1*Bk3*fl16*fl25 - 8.E+0*P_k5*k4_k4*
             k4_l1*k5_k5*q_l1*l2_l2*Bk3*fl16*fl25 + 8.E+0*P_k5*k4_k4*k4_l1*
             k5_k5*q_l2*l1_l2*Bk3*fl16*fl25 - 8.E+0*P_k5*k4_q*k4_l1*k4_l2*
             k5_k5*l1_l2*Bk3*fl16*fl25 + 8.E+0*P_k5*k4_l1*pow(k4_l2,2)*
             k5_k5*q_l1*Bk3*fl16*fl25 + 8.E+0*P_l1*k4_k4*k4_l1*k5_k5*k5_q*
             l2_l2*Bk3*fl16*fl25 - 8.E+0*P_l1*k4_k4*k4_l1*k5_k5*k5_l2*q_l2*
             Bk3*fl16*fl25 - 8.E+0*P_l1*k4_k5*k4_q*k4_l1*k5_k5*l2_l2*Bk3*
             fl16*fl25 + 8.E+0*P_l1*k4_k5*k4_l1*k4_l2*k5_k5*q_l2*Bk3*fl16*
             fl25 + 8.E+0*P_l1*k4_q*k4_l1*k4_l2*k5_k5*k5_l2*Bk3*fl16*fl25
              - 8.E+0*P_l1*k4_l1*pow(k4_l2,2)*k5_k5*k5_q*Bk3*fl16*fl25 -
             8.E+0*P_l2*k4_k4*k4_l1*k5_k5*k5_q*l1_l2*Bk3*fl16*fl25 + 8.E+0*
             P_l2*k4_k4*k4_l1*k5_k5*k5_l2*q_l1*Bk3*fl16*fl25 + 8.E+0*P_l2*
             k4_k5*k4_q*k4_l1*k5_k5*l1_l2*Bk3*fl16*fl25 - 8.E+0*P_l2*k4_k5*
             k4_l1*k4_l2*k5_k5*q_l1*Bk3*fl16*fl25 );

          Chi(9,8) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k5*k4_l1*k5_k5*q_l1*
             l2_l2*Ak3*fl16*fl25 - 4.E+0*P_q*k3_k4*k4_k5*k4_l1*k5_k5*q_l2*
             l1_l2*Ak3*fl16*fl25 + 4.E+0*P_q*k3_k4*k4_l1*k4_l2*k5_k5*k5_q*
             l1_l2*Ak3*fl16*fl25 - 4.E+0*P_q*k3_k4*k4_l1*k4_l2*k5_k5*k5_l2*
             q_l1*Ak3*fl16*fl25 - 4.E+0*P_q*k3_k5*k4_k4*k4_l1*k5_k5*q_l1*
             l2_l2*Ak3*fl16*fl25 + 4.E+0*P_q*k3_k5*k4_k4*k4_l1*k5_k5*q_l2*
             l1_l2*Ak3*fl16*fl25 - 4.E+0*P_q*k3_k5*k4_q*k4_l1*k4_l2*k5_k5*
             l1_l2*Ak3*fl16*fl25 + 4.E+0*P_q*k3_k5*k4_l1*pow(k4_l2,2)*k5_k5
             *q_l1*Ak3*fl16*fl25 + 4.E+0*P_q*k3_l1*k4_k4*k4_l1*k5_k5*k5_q*
             l2_l2*Ak3*fl16*fl25 - 4.E+0*P_q*k3_l1*k4_k4*k4_l1*k5_k5*k5_l2*
             q_l2*Ak3*fl16*fl25 - 4.E+0*P_q*k3_l1*k4_k5*k4_q*k4_l1*k5_k5*
             l2_l2*Ak3*fl16*fl25 + 4.E+0*P_q*k3_l1*k4_k5*k4_l1*k4_l2*k5_k5*
             q_l2*Ak3*fl16*fl25 + 4.E+0*P_q*k3_l1*k4_q*k4_l1*k4_l2*k5_k5*
             k5_l2*Ak3*fl16*fl25 - 4.E+0*P_q*k3_l1*k4_l1*pow(k4_l2,2)*k5_k5
             *k5_q*Ak3*fl16*fl25 - 4.E+0*P_q*k3_l2*k4_k4*k4_l1*k5_k5*k5_q*
             l1_l2*Ak3*fl16*fl25 + 4.E+0*P_q*k3_l2*k4_k4*k4_l1*k5_k5*k5_l2*
             q_l1*Ak3*fl16*fl25 + 4.E+0*P_q*k3_l2*k4_k5*k4_q*k4_l1*k5_k5*
             l1_l2*Ak3*fl16*fl25 - 4.E+0*P_q*k3_l2*k4_k5*k4_l1*k4_l2*k5_k5*
             q_l1*Ak3*fl16*fl25 );

          Chi(9,8) +=  + F * (  - 4.E+0*P_k4*k3_k5*k4_l1*k4_l2*k5_k5*
             l1_l2*Ak3*fl16*fl25 - 4.E+0*P_k4*k3_l1*k4_k5*k4_l1*k5_k5*l2_l2
             *Ak3*fl16*fl25 + 4.E+0*P_k4*k3_l1*k4_l1*k4_l2*k5_k5*k5_l2*Ak3*
             fl16*fl25 + 4.E+0*P_k4*k3_l2*k4_k5*k4_l1*k5_k5*l1_l2*Ak3*fl16*
             fl25 + 4.E+0*P_k5*k3_k4*k4_l1*k4_l2*k5_k5*l1_l2*Ak3*fl16*fl25
              + 4.E+0*P_k5*k3_l1*k4_k4*k4_l1*k5_k5*l2_l2*Ak3*fl16*fl25 -
             4.E+0*P_k5*k3_l1*k4_l1*pow(k4_l2,2)*k5_k5*Ak3*fl16*fl25 -
             4.E+0*P_k5*k3_l2*k4_k4*k4_l1*k5_k5*l1_l2*Ak3*fl16*fl25 + 4.E+0
             *P_l1*k3_k4*k4_k5*k4_l1*k5_k5*l2_l2*Ak3*fl16*fl25 - 4.E+0*P_l1
             *k3_k4*k4_l1*k4_l2*k5_k5*k5_l2*Ak3*fl16*fl25 - 4.E+0*P_l1*
             k3_k5*k4_k4*k4_l1*k5_k5*l2_l2*Ak3*fl16*fl25 + 4.E+0*P_l1*k3_k5
             *k4_l1*pow(k4_l2,2)*k5_k5*Ak3*fl16*fl25 + 4.E+0*P_l1*k3_l2*
             k4_k4*k4_l1*k5_k5*k5_l2*Ak3*fl16*fl25 - 4.E+0*P_l1*k3_l2*k4_k5
             *k4_l1*k4_l2*k5_k5*Ak3*fl16*fl25 - 4.E+0*P_l2*k3_k4*k4_k5*
             k4_l1*k5_k5*l1_l2*Ak3*fl16*fl25 + 4.E+0*P_l2*k3_k5*k4_k4*k4_l1
             *k5_k5*l1_l2*Ak3*fl16*fl25 - 4.E+0*P_l2*k3_l1*k4_k4*k4_l1*
             k5_k5*k5_l2*Ak3*fl16*fl25 + 4.E+0*P_l2*k3_l1*k4_k5*k4_l1*k4_l2
             *k5_k5*Ak3*fl16*fl25 );

       Chi(9,9) =
           + H * ( 8.E+0*P_k4*k4_k5*k4_l1*k5_l1*k5_l2*q_l2*Bk3*fl16*fl26 +
             8.E+0*P_k4*k4_l1*k4_l2*k5_k5*k5_l2*q_l1*Bk3*fl16*fl26 - 8.E+0*
             P_k4*k4_l1*k4_l2*k5_q*k5_l1*k5_l2*Bk3*fl16*fl26 + 8.E+0*P_k4*
             pow(k4_l1,2)*k5_q*pow(k5_l2,2)*Bk3*fl16*fl26 - 8.E+0*P_k5*
             k4_k4*k4_l1*k5_l1*k5_l2*q_l2*Bk3*fl16*fl26 - 8.E+0*P_k5*k4_k5*
             k4_l1*k4_l2*k5_l2*q_l1*Bk3*fl16*fl26 + 8.E+0*P_k5*k4_q*k4_l1*
             k4_l2*k5_l1*k5_l2*Bk3*fl16*fl26 - 8.E+0*P_k5*k4_q*pow(k4_l1,2)
             *pow(k5_l2,2)*Bk3*fl16*fl26 + 8.E+0*P_l1*k4_k4*k4_l1*k5_k5*
             k5_l2*q_l2*Bk3*fl16*fl26 + 8.E+0*P_l1*k4_k5*k4_l1*k4_l2*k5_q*
             k5_l2*Bk3*fl16*fl26 - 8.E+0*P_l1*pow(k4_k5,2)*k4_l1*k5_l2*q_l2
             *Bk3*fl16*fl26 - 8.E+0*P_l1*k4_q*k4_l1*k4_l2*k5_k5*k5_l2*Bk3*
             fl16*fl26 - 8.E+0*P_l2*k4_k4*k4_l1*k5_k5*k5_l2*q_l1*Bk3*fl16*
             fl26 + 8.E+0*P_l2*k4_k4*k4_l1*k5_q*k5_l1*k5_l2*Bk3*fl16*fl26
              - 8.E+0*P_l2*k4_k5*k4_q*k4_l1*k5_l1*k5_l2*Bk3*fl16*fl26 +
             8.E+0*P_l2*pow(k4_k5,2)*k4_l1*k5_l2*q_l1*Bk3*fl16*fl26 );

          Chi(9,9) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k5*k4_l1*k5_l1*k5_l2*
             q_l2*Ak3*fl16*fl26 + 4.E+0*P_q*k3_k4*k4_l1*k4_l2*k5_k5*k5_l2*
             q_l1*Ak3*fl16*fl26 - 4.E+0*P_q*k3_k4*k4_l1*k4_l2*k5_q*k5_l1*
             k5_l2*Ak3*fl16*fl26 + 4.E+0*P_q*k3_k4*pow(k4_l1,2)*k5_q*pow(
             k5_l2,2)*Ak3*fl16*fl26 - 4.E+0*P_q*k3_k5*k4_k4*k4_l1*k5_l1*
             k5_l2*q_l2*Ak3*fl16*fl26 - 4.E+0*P_q*k3_k5*k4_k5*k4_l1*k4_l2*
             k5_l2*q_l1*Ak3*fl16*fl26 + 4.E+0*P_q*k3_k5*k4_q*k4_l1*k4_l2*
             k5_l1*k5_l2*Ak3*fl16*fl26 - 4.E+0*P_q*k3_k5*k4_q*pow(k4_l1,2)*
             pow(k5_l2,2)*Ak3*fl16*fl26 + 4.E+0*P_q*k3_l1*k4_k4*k4_l1*k5_k5
             *k5_l2*q_l2*Ak3*fl16*fl26 + 4.E+0*P_q*k3_l1*k4_k5*k4_l1*k4_l2*
             k5_q*k5_l2*Ak3*fl16*fl26 - 4.E+0*P_q*k3_l1*pow(k4_k5,2)*k4_l1*
             k5_l2*q_l2*Ak3*fl16*fl26 - 4.E+0*P_q*k3_l1*k4_q*k4_l1*k4_l2*
             k5_k5*k5_l2*Ak3*fl16*fl26 - 4.E+0*P_q*k3_l2*k4_k4*k4_l1*k5_k5*
             k5_l2*q_l1*Ak3*fl16*fl26 + 4.E+0*P_q*k3_l2*k4_k4*k4_l1*k5_q*
             k5_l1*k5_l2*Ak3*fl16*fl26 - 4.E+0*P_q*k3_l2*k4_k5*k4_q*k4_l1*
             k5_l1*k5_l2*Ak3*fl16*fl26 + 4.E+0*P_q*k3_l2*pow(k4_k5,2)*k4_l1
             *k5_l2*q_l1*Ak3*fl16*fl26 );

          Chi(9,9) +=  + F * ( 4.E+0*P_k4*k3_k5*k4_l1*k4_l2*k5_l1*k5_l2*
             Ak3*fl16*fl26 - 4.E+0*P_k4*k3_k5*pow(k4_l1,2)*pow(k5_l2,2)*Ak3
             *fl16*fl26 - 4.E+0*P_k4*k3_l1*k4_l1*k4_l2*k5_k5*k5_l2*Ak3*fl16
             *fl26 - 4.E+0*P_k4*k3_l2*k4_k5*k4_l1*k5_l1*k5_l2*Ak3*fl16*fl26
              - 4.E+0*P_k5*k3_k4*k4_l1*k4_l2*k5_l1*k5_l2*Ak3*fl16*fl26 +
             4.E+0*P_k5*k3_k4*pow(k4_l1,2)*pow(k5_l2,2)*Ak3*fl16*fl26 +
             4.E+0*P_k5*k3_l1*k4_k5*k4_l1*k4_l2*k5_l2*Ak3*fl16*fl26 + 4.E+0
             *P_k5*k3_l2*k4_k4*k4_l1*k5_l1*k5_l2*Ak3*fl16*fl26 + 4.E+0*P_l1
             *k3_k4*k4_l1*k4_l2*k5_k5*k5_l2*Ak3*fl16*fl26 - 4.E+0*P_l1*
             k3_k5*k4_k5*k4_l1*k4_l2*k5_l2*Ak3*fl16*fl26 - 4.E+0*P_l1*k3_l2
             *k4_k4*k4_l1*k5_k5*k5_l2*Ak3*fl16*fl26 + 4.E+0*P_l1*k3_l2*pow(
             k4_k5,2)*k4_l1*k5_l2*Ak3*fl16*fl26 + 4.E+0*P_l2*k3_k4*k4_k5*
             k4_l1*k5_l1*k5_l2*Ak3*fl16*fl26 - 4.E+0*P_l2*k3_k5*k4_k4*k4_l1
             *k5_l1*k5_l2*Ak3*fl16*fl26 + 4.E+0*P_l2*k3_l1*k4_k4*k4_l1*
             k5_k5*k5_l2*Ak3*fl16*fl26 - 4.E+0*P_l2*k3_l1*pow(k4_k5,2)*
             k4_l1*k5_l2*Ak3*fl16*fl26 );

       Chi(9,10) =
           + H * ( 4.E+0*P_k3*k4_k4*k4_l1*k5_k5*q_l1*l2_l2*Ak3*fl16*fl27 -
             4.E+0*P_k3*k4_k4*k4_l1*k5_k5*q_l2*l1_l2*Ak3*fl16*fl27 - 4.E+0*
             P_k3*k4_k4*k4_l1*k5_q*k5_l1*l2_l2*Ak3*fl16*fl27 - 4.E+0*P_k3*
             k4_k4*k4_l1*k5_q*k5_l2*l1_l2*Ak3*fl16*fl27 + 4.E+0*P_k3*k4_k4*
             k4_l1*k5_l1*k5_l2*q_l2*Ak3*fl16*fl27 + 4.E+0*P_k3*k4_k4*k4_l1*
             pow(k5_l2,2)*q_l1*Ak3*fl16*fl27 + 4.E+0*P_k3*k4_k5*k4_q*k4_l1*
             k5_l1*l2_l2*Ak3*fl16*fl27 + 4.E+0*P_k3*k4_k5*k4_q*k4_l1*k5_l2*
             l1_l2*Ak3*fl16*fl27 - 4.E+0*P_k3*k4_k5*k4_l1*k4_l2*k5_q*l1_l2*
             Ak3*fl16*fl27 - 4.E+0*P_k3*k4_k5*k4_l1*k4_l2*k5_l1*q_l2*Ak3*
             fl16*fl27 + 1.2E+1*P_k3*k4_k5*pow(k4_l1,2)*k5_q*l2_l2*Ak3*fl16
             *fl27 - 1.2E+1*P_k3*k4_k5*pow(k4_l1,2)*k5_l2*q_l2*Ak3*fl16*
             fl27 - 4.E+0*P_k3*pow(k4_k5,2)*k4_l1*q_l1*l2_l2*Ak3*fl16*fl27
              + 4.E+0*P_k3*pow(k4_k5,2)*k4_l1*q_l2*l1_l2*Ak3*fl16*fl27 +
             4.E+0*P_k3*k4_q*k4_l1*k4_l2*k5_k5*l1_l2*Ak3*fl16*fl27 - 4.E+0*
             P_k3*k4_q*k4_l1*k4_l2*k5_l1*k5_l2*Ak3*fl16*fl27 - 1.2E+1*P_k3*
             k4_q*pow(k4_l1,2)*k5_k5*l2_l2*Ak3*fl16*fl27 + 4.E+0*P_k3*k4_q*
             pow(k4_l1,2)*pow(k5_l2,2)*Ak3*fl16*fl27 - 4.E+0*P_k3*k4_l1*
             pow(k4_l2,2)*k5_k5*q_l1*Ak3*fl16*fl27 + 4.E+0*P_k3*k4_l1*pow(
             k4_l2,2)*k5_q*k5_l1*Ak3*fl16*fl27 + 1.2E+1*P_k3*pow(k4_l1,2)*
             k4_l2*k5_k5*q_l2*Ak3*fl16*fl27 - 4.E+0*P_k3*pow(k4_l1,2)*k4_l2
             *k5_q*k5_l2*Ak3*fl16*fl27 - 1.2E+1*P_k4*k3_k4*k4_l1*k5_k5*q_l1
             *l2_l2*Ak3*fl16*fl27 + 1.2E+1*P_k4*k3_k4*k4_l1*k5_k5*q_l2*
             l1_l2*Ak3*fl16*fl27 + 1.2E+1*P_k4*k3_k4*k4_l1*k5_q*k5_l1*l2_l2
             *Ak3*fl16*fl27 + 4.E+0*P_k4*k3_k4*k4_l1*k5_q*k5_l2*l1_l2*Ak3*
             fl16*fl27 - 1.2E+1*P_k4*k3_k4*k4_l1*k5_l1*k5_l2*q_l2*Ak3*fl16*
             fl27 - 4.E+0*P_k4*k3_k4*k4_l1*pow(k5_l2,2)*q_l1*Ak3*fl16*fl27
              + 4.E+0*P_k4*k3_k5*k4_k5*k4_l1*q_l1*l2_l2*Ak3*fl16*fl27 -
             4.E+0*P_k4*k3_k5*k4_k5*k4_l1*q_l2*l1_l2*Ak3*fl16*fl27 - 2.E+0*
             P_k4*k3_k5*k4_q*k4_l1*k5_l1*l2_l2*Ak3*fl16*fl27 + 2.E+0*P_k4*
             k3_k5*k4_q*k4_l1*k5_l2*l1_l2*Ak3*fl16*fl27 - 6.E+0*P_k4*k3_k5*
             k4_l1*k4_l2*k5_q*l1_l2*Ak3*fl16*fl27 + 1.E+1*P_k4*k3_k5*k4_l1*
             k4_l2*k5_l1*q_l2*Ak3*fl16*fl27 + 4.E+0*P_k4*k3_k5*k4_l1*k4_l2*
             k5_l2*q_l1*Ak3*fl16*fl27 - 1.E+1*P_k4*k3_k5*pow(k4_l1,2)*k5_q*
             l2_l2*Ak3*fl16*fl27 + 2.E+0*P_k4*k3_k5*pow(k4_l1,2)*k5_l2*q_l2
             *Ak3*fl16*fl27 - 2.E+0*P_k4*k3_q*k4_k5*k4_l1*k5_l1*l2_l2*Ak3*
             fl16*fl27 - 6.E+0*P_k4*k3_q*k4_k5*k4_l1*k5_l2*l1_l2*Ak3*fl16*
             fl27 - 2.E+0*P_k4*k3_q*k4_l1*k4_l2*k5_k5*l1_l2*Ak3*fl16*fl27
              + 2.E+0*P_k4*k3_q*k4_l1*k4_l2*k5_l1*k5_l2*Ak3*fl16*fl27 +
             1.E+1*P_k4*k3_q*pow(k4_l1,2)*k5_k5*l2_l2*Ak3*fl16*fl27 - 2.E+0
             *P_k4*k3_q*pow(k4_l1,2)*pow(k5_l2,2)*Ak3*fl16*fl27 - 1.E+1*
             P_k4*k3_l1*k4_k5*k4_l1*k5_q*l2_l2*Ak3*fl16*fl27 + 1.E+1*P_k4*
             k3_l1*k4_k5*k4_l1*k5_l2*q_l2*Ak3*fl16*fl27 + 2.E+0*P_k4*k3_l1*
             k4_q*k4_l1*k5_k5*l2_l2*Ak3*fl16*fl27 - 2.E+0*P_k4*k3_l1*k4_q*
             k4_l1*pow(k5_l2,2)*Ak3*fl16*fl27 - 1.E+1*P_k4*k3_l1*k4_l1*
             k4_l2*k5_k5*q_l2*Ak3*fl16*fl27 + 2.E+0*P_k4*k3_l1*k4_l1*k4_l2*
             k5_q*k5_l2*Ak3*fl16*fl27 + 1.E+1*P_k4*k3_l2*k4_k5*k4_l1*k5_q*
             l1_l2*Ak3*fl16*fl27 - 6.E+0*P_k4*k3_l2*k4_k5*k4_l1*k5_l1*q_l2*
             Ak3*fl16*fl27 - 4.E+0*P_k4*k3_l2*k4_k5*k4_l1*k5_l2*q_l1*Ak3*
             fl16*fl27 - 2.E+0*P_k4*k3_l2*k4_q*k4_l1*k5_k5*l1_l2*Ak3*fl16*
             fl27 + 2.E+0*P_k4*k3_l2*k4_q*k4_l1*k5_l1*k5_l2*Ak3*fl16*fl27
              + 4.E+0*P_k4*k3_l2*k4_l1*k4_l2*k5_k5*q_l1*Ak3*fl16*fl27 );

          Chi(9,10) +=  + H * (  - 4.E+0*P_k4*k3_l2*k4_l1*k4_l2*k5_q*
             k5_l1*Ak3*fl16*fl27 - 1.E+1*P_k4*k3_l2*pow(k4_l1,2)*k5_k5*q_l2
             *Ak3*fl16*fl27 + 2.E+0*P_k4*k3_l2*pow(k4_l1,2)*k5_q*k5_l2*Ak3*
             fl16*fl27 + 4.E+0*P_k5*k3_k4*k4_k5*k4_l1*q_l1*l2_l2*Ak3*fl16*
             fl27 - 4.E+0*P_k5*k3_k4*k4_k5*k4_l1*q_l2*l1_l2*Ak3*fl16*fl27
              - 1.E+1*P_k5*k3_k4*k4_q*k4_l1*k5_l1*l2_l2*Ak3*fl16*fl27 -
             6.E+0*P_k5*k3_k4*k4_q*k4_l1*k5_l2*l1_l2*Ak3*fl16*fl27 + 2.E+0*
             P_k5*k3_k4*k4_l1*k4_l2*k5_q*l1_l2*Ak3*fl16*fl27 + 2.E+0*P_k5*
             k3_k4*k4_l1*k4_l2*k5_l1*q_l2*Ak3*fl16*fl27 + 4.E+0*P_k5*k3_k4*
             k4_l1*k4_l2*k5_l2*q_l1*Ak3*fl16*fl27 - 2.E+0*P_k5*k3_k4*pow(
             k4_l1,2)*k5_q*l2_l2*Ak3*fl16*fl27 + 1.E+1*P_k5*k3_k4*pow(
             k4_l1,2)*k5_l2*q_l2*Ak3*fl16*fl27 + 4.E+0*P_k5*k3_k5*k4_k4*
             k4_l1*q_l1*l2_l2*Ak3*fl16*fl27 - 4.E+0*P_k5*k3_k5*k4_k4*k4_l1*
             q_l2*l1_l2*Ak3*fl16*fl27 + 4.E+0*P_k5*k3_k5*k4_q*k4_l1*k4_l2*
             l1_l2*Ak3*fl16*fl27 + 1.2E+1*P_k5*k3_k5*k4_q*pow(k4_l1,2)*
             l2_l2*Ak3*fl16*fl27 - 4.E+0*P_k5*k3_k5*k4_l1*pow(k4_l2,2)*q_l1
             *Ak3*fl16*fl27 - 1.2E+1*P_k5*k3_k5*pow(k4_l1,2)*k4_l2*q_l2*Ak3
             *fl16*fl27 + 2.E+0*P_k5*k3_q*k4_k4*k4_l1*k5_l1*l2_l2*Ak3*fl16*
             fl27 + 6.E+0*P_k5*k3_q*k4_k4*k4_l1*k5_l2*l1_l2*Ak3*fl16*fl27
              + 2.E+0*P_k5*k3_q*k4_k5*k4_l1*k4_l2*l1_l2*Ak3*fl16*fl27 -
             1.E+1*P_k5*k3_q*k4_k5*pow(k4_l1,2)*l2_l2*Ak3*fl16*fl27 - 2.E+0
             *P_k5*k3_q*k4_l1*pow(k4_l2,2)*k5_l1*Ak3*fl16*fl27 + 2.E+0*P_k5
             *k3_q*pow(k4_l1,2)*k4_l2*k5_l2*Ak3*fl16*fl27 + 2.E+0*P_k5*
             k3_l1*k4_k4*k4_l1*k5_q*l2_l2*Ak3*fl16*fl27 - 2.E+0*P_k5*k3_l1*
             k4_k4*k4_l1*k5_l2*q_l2*Ak3*fl16*fl27 + 6.E+0*P_k5*k3_l1*k4_k5*
             k4_q*k4_l1*l2_l2*Ak3*fl16*fl27 + 2.E+0*P_k5*k3_l1*k4_k5*k4_l1*
             k4_l2*q_l2*Ak3*fl16*fl27 + 2.E+0*P_k5*k3_l1*k4_q*k4_l1*k4_l2*
             k5_l2*Ak3*fl16*fl27 - 2.E+0*P_k5*k3_l1*k4_l1*pow(k4_l2,2)*k5_q
             *Ak3*fl16*fl27 - 2.E+0*P_k5*k3_l2*k4_k4*k4_l1*k5_q*l1_l2*Ak3*
             fl16*fl27 + 6.E+0*P_k5*k3_l2*k4_k4*k4_l1*k5_l1*q_l2*Ak3*fl16*
             fl27 - 4.E+0*P_k5*k3_l2*k4_k4*k4_l1*k5_l2*q_l1*Ak3*fl16*fl27
              - 6.E+0*P_k5*k3_l2*k4_k5*k4_q*k4_l1*l1_l2*Ak3*fl16*fl27 +
             4.E+0*P_k5*k3_l2*k4_k5*k4_l1*k4_l2*q_l1*Ak3*fl16*fl27 + 1.E+1*
             P_k5*k3_l2*k4_k5*pow(k4_l1,2)*q_l2*Ak3*fl16*fl27 + 2.E+0*P_k5*
             k3_l2*k4_q*k4_l1*k4_l2*k5_l1*Ak3*fl16*fl27 - 4.E+0*P_k5*k3_l2*
             k4_q*pow(k4_l1,2)*k5_l2*Ak3*fl16*fl27 + 2.E+0*P_k5*k3_l2*pow(
             k4_l1,2)*k4_l2*k5_q*Ak3*fl16*fl27 - 2.E+0*P_q*k3_k4*k4_k5*
             k4_l1*k5_l1*l2_l2*Ak3*fl16*fl27 + 2.E+0*P_q*k3_k4*k4_k5*k4_l1*
             k5_l2*l1_l2*Ak3*fl16*fl27 - 2.E+0*P_q*k3_k4*k4_l1*k4_l2*k5_k5*
             l1_l2*Ak3*fl16*fl27 + 2.E+0*P_q*k3_k4*k4_l1*k4_l2*k5_l1*k5_l2*
             Ak3*fl16*fl27 + 2.E+0*P_q*k3_k4*pow(k4_l1,2)*k5_k5*l2_l2*Ak3*
             fl16*fl27 - 2.E+0*P_q*k3_k4*pow(k4_l1,2)*pow(k5_l2,2)*Ak3*fl16
             *fl27 + 2.E+0*P_q*k3_k5*k4_k4*k4_l1*k5_l1*l2_l2*Ak3*fl16*fl27
              - 2.E+0*P_q*k3_k5*k4_k4*k4_l1*k5_l2*l1_l2*Ak3*fl16*fl27 +
             2.E+0*P_q*k3_k5*k4_k5*k4_l1*k4_l2*l1_l2*Ak3*fl16*fl27 - 2.E+0*
             P_q*k3_k5*k4_k5*pow(k4_l1,2)*l2_l2*Ak3*fl16*fl27 - 2.E+0*P_q*
             k3_k5*k4_l1*pow(k4_l2,2)*k5_l1*Ak3*fl16*fl27 + 2.E+0*P_q*k3_k5
             *pow(k4_l1,2)*k4_l2*k5_l2*Ak3*fl16*fl27 - 2.E+0*P_q*k3_l1*
             k4_k4*k4_l1*k5_k5*l2_l2*Ak3*fl16*fl27 + 2.E+0*P_q*k3_l1*k4_k4*
             k4_l1*pow(k5_l2,2)*Ak3*fl16*fl27 - 4.E+0*P_q*k3_l1*k4_k5*k4_l1
             *k4_l2*k5_l2*Ak3*fl16*fl27 );

          Chi(9,10) +=  + H * ( 2.E+0*P_q*k3_l1*pow(k4_k5,2)*k4_l1*l2_l2*
             Ak3*fl16*fl27 + 2.E+0*P_q*k3_l1*k4_l1*pow(k4_l2,2)*k5_k5*Ak3*
             fl16*fl27 + 2.E+0*P_q*k3_l2*k4_k4*k4_l1*k5_k5*l1_l2*Ak3*fl16*
             fl27 - 2.E+0*P_q*k3_l2*k4_k4*k4_l1*k5_l1*k5_l2*Ak3*fl16*fl27
              + 2.E+0*P_q*k3_l2*k4_k5*k4_l1*k4_l2*k5_l1*Ak3*fl16*fl27 +
             2.E+0*P_q*k3_l2*k4_k5*pow(k4_l1,2)*k5_l2*Ak3*fl16*fl27 - 2.E+0
             *P_q*k3_l2*pow(k4_k5,2)*k4_l1*l1_l2*Ak3*fl16*fl27 - 2.E+0*P_q*
             k3_l2*pow(k4_l1,2)*k4_l2*k5_k5*Ak3*fl16*fl27 - 2.E+0*P_l1*
             k3_k4*k4_k5*k4_l1*k5_q*l2_l2*Ak3*fl16*fl27 + 2.E+0*P_l1*k3_k4*
             k4_k5*k4_l1*k5_l2*q_l2*Ak3*fl16*fl27 + 1.E+1*P_l1*k3_k4*k4_q*
             k4_l1*k5_k5*l2_l2*Ak3*fl16*fl27 + 6.E+0*P_l1*k3_k4*k4_q*k4_l1*
             pow(k5_l2,2)*Ak3*fl16*fl27 - 2.E+0*P_l1*k3_k4*k4_l1*k4_l2*
             k5_k5*q_l2*Ak3*fl16*fl27 - 6.E+0*P_l1*k3_k4*k4_l1*k4_l2*k5_q*
             k5_l2*Ak3*fl16*fl27 - 6.E+0*P_l1*k3_k5*k4_k4*k4_l1*k5_q*l2_l2*
             Ak3*fl16*fl27 + 6.E+0*P_l1*k3_k5*k4_k4*k4_l1*k5_l2*q_l2*Ak3*
             fl16*fl27 - 2.E+0*P_l1*k3_k5*k4_k5*k4_q*k4_l1*l2_l2*Ak3*fl16*
             fl27 - 6.E+0*P_l1*k3_k5*k4_k5*k4_l1*k4_l2*q_l2*Ak3*fl16*fl27
              - 6.E+0*P_l1*k3_k5*k4_q*k4_l1*k4_l2*k5_l2*Ak3*fl16*fl27 +
             6.E+0*P_l1*k3_k5*k4_l1*pow(k4_l2,2)*k5_q*Ak3*fl16*fl27 - 2.E+0
             *P_l1*k3_q*k4_k4*k4_l1*k5_k5*l2_l2*Ak3*fl16*fl27 - 6.E+0*P_l1*
             k3_q*k4_k4*k4_l1*pow(k5_l2,2)*Ak3*fl16*fl27 + 4.E+0*P_l1*k3_q*
             k4_k5*k4_l1*k4_l2*k5_l2*Ak3*fl16*fl27 + 2.E+0*P_l1*k3_q*pow(
             k4_k5,2)*k4_l1*l2_l2*Ak3*fl16*fl27 + 2.E+0*P_l1*k3_q*k4_l1*
             pow(k4_l2,2)*k5_k5*Ak3*fl16*fl27 - 6.E+0*P_l1*k3_l2*k4_k4*
             k4_l1*k5_k5*q_l2*Ak3*fl16*fl27 + 6.E+0*P_l1*k3_l2*k4_k4*k4_l1*
             k5_q*k5_l2*Ak3*fl16*fl27 + 2.E+0*P_l1*k3_l2*k4_k5*k4_q*k4_l1*
             k5_l2*Ak3*fl16*fl27 - 6.E+0*P_l1*k3_l2*k4_k5*k4_l1*k4_l2*k5_q*
             Ak3*fl16*fl27 + 6.E+0*P_l1*k3_l2*pow(k4_k5,2)*k4_l1*q_l2*Ak3*
             fl16*fl27 - 2.E+0*P_l1*k3_l2*k4_q*k4_l1*k4_l2*k5_k5*Ak3*fl16*
             fl27 + 2.E+0*P_l2*k3_k4*k4_k5*k4_l1*k5_q*l1_l2*Ak3*fl16*fl27
              + 2.E+0*P_l2*k3_k4*k4_k5*k4_l1*k5_l1*q_l2*Ak3*fl16*fl27 -
             4.E+0*P_l2*k3_k4*k4_k5*k4_l1*k5_l2*q_l1*Ak3*fl16*fl27 - 1.E+1*
             P_l2*k3_k4*k4_q*k4_l1*k5_k5*l1_l2*Ak3*fl16*fl27 + 1.E+1*P_l2*
             k3_k4*k4_q*k4_l1*k5_l1*k5_l2*Ak3*fl16*fl27 + 4.E+0*P_l2*k3_k4*
             k4_l1*k4_l2*k5_k5*q_l1*Ak3*fl16*fl27 - 4.E+0*P_l2*k3_k4*k4_l1*
             k4_l2*k5_q*k5_l1*Ak3*fl16*fl27 - 2.E+0*P_l2*k3_k4*pow(k4_l1,2)
             *k5_k5*q_l2*Ak3*fl16*fl27 - 6.E+0*P_l2*k3_k4*pow(k4_l1,2)*k5_q
             *k5_l2*Ak3*fl16*fl27 + 6.E+0*P_l2*k3_k5*k4_k4*k4_l1*k5_q*l1_l2
             *Ak3*fl16*fl27 - 2.E+0*P_l2*k3_k5*k4_k4*k4_l1*k5_l1*q_l2*Ak3*
             fl16*fl27 - 4.E+0*P_l2*k3_k5*k4_k4*k4_l1*k5_l2*q_l1*Ak3*fl16*
             fl27 + 2.E+0*P_l2*k3_k5*k4_k5*k4_q*k4_l1*l1_l2*Ak3*fl16*fl27
              + 4.E+0*P_l2*k3_k5*k4_k5*k4_l1*k4_l2*q_l1*Ak3*fl16*fl27 +
             2.E+0*P_l2*k3_k5*k4_k5*pow(k4_l1,2)*q_l2*Ak3*fl16*fl27 - 6.E+0
             *P_l2*k3_k5*k4_q*k4_l1*k4_l2*k5_l1*Ak3*fl16*fl27 - 4.E+0*P_l2*
             k3_k5*k4_q*pow(k4_l1,2)*k5_l2*Ak3*fl16*fl27 + 1.E+1*P_l2*k3_k5
             *pow(k4_l1,2)*k4_l2*k5_q*Ak3*fl16*fl27 + 2.E+0*P_l2*k3_q*k4_k4
             *k4_l1*k5_k5*l1_l2*Ak3*fl16*fl27 - 2.E+0*P_l2*k3_q*k4_k4*k4_l1
             *k5_l1*k5_l2*Ak3*fl16*fl27 + 2.E+0*P_l2*k3_q*k4_k5*k4_l1*k4_l2
             *k5_l1*Ak3*fl16*fl27 + 1.E+1*P_l2*k3_q*k4_k5*pow(k4_l1,2)*
             k5_l2*Ak3*fl16*fl27 - 2.E+0*P_l2*k3_q*pow(k4_k5,2)*k4_l1*l1_l2
             *Ak3*fl16*fl27 );

          Chi(9,10) +=  + H * (  - 1.E+1*P_l2*k3_q*pow(k4_l1,2)*k4_l2*
             k5_k5*Ak3*fl16*fl27 + 2.E+0*P_l2*k3_l1*k4_k4*k4_l1*k5_k5*q_l2*
             Ak3*fl16*fl27 - 2.E+0*P_l2*k3_l1*k4_k4*k4_l1*k5_q*k5_l2*Ak3*
             fl16*fl27 - 6.E+0*P_l2*k3_l1*k4_k5*k4_q*k4_l1*k5_l2*Ak3*fl16*
             fl27 + 2.E+0*P_l2*k3_l1*k4_k5*k4_l1*k4_l2*k5_q*Ak3*fl16*fl27
              - 2.E+0*P_l2*k3_l1*pow(k4_k5,2)*k4_l1*q_l2*Ak3*fl16*fl27 +
             6.E+0*P_l2*k3_l1*k4_q*k4_l1*k4_l2*k5_k5*Ak3*fl16*fl27 + 4.E+0*
             P_l2*k3_l2*k4_k4*k4_l1*k5_k5*q_l1*Ak3*fl16*fl27 - 4.E+0*P_l2*
             k3_l2*k4_k4*k4_l1*k5_q*k5_l1*Ak3*fl16*fl27 + 4.E+0*P_l2*k3_l2*
             k4_k5*k4_q*k4_l1*k5_l1*Ak3*fl16*fl27 - 1.2E+1*P_l2*k3_l2*k4_k5
             *pow(k4_l1,2)*k5_q*Ak3*fl16*fl27 - 4.E+0*P_l2*k3_l2*pow(
             k4_k5,2)*k4_l1*q_l1*Ak3*fl16*fl27 + 1.2E+1*P_l2*k3_l2*k4_q*
             pow(k4_l1,2)*k5_k5*Ak3*fl16*fl27 );

          Chi(9,10) +=  + G * (  - 4.E+0*P_q*k4_k4*k4_l1*k5_k5*q_l1*l2_l2
             *Bk3*fl16*fl27 + 4.E+0*P_q*k4_k4*k4_l1*k5_k5*q_l2*l1_l2*Bk3*
             fl16*fl27 + 4.E+0*P_q*k4_k4*k4_l1*k5_q*k5_l1*l2_l2*Bk3*fl16*
             fl27 - 4.E+0*P_q*k4_k4*k4_l1*k5_l1*k5_l2*q_l2*Bk3*fl16*fl27 -
             4.E+0*P_q*k4_k5*k4_q*k4_l1*k5_l1*l2_l2*Bk3*fl16*fl27 + 4.E+0*
             P_q*k4_k5*k4_l1*k4_l2*k5_q*l1_l2*Bk3*fl16*fl27 + 4.E+0*P_q*
             k4_k5*k4_l1*k4_l2*k5_l1*q_l2*Bk3*fl16*fl27 - 4.E+0*P_q*k4_k5*
             k4_l1*k4_l2*k5_l2*q_l1*Bk3*fl16*fl27 + 4.E+0*P_q*pow(k4_k5,2)*
             k4_l1*q_l1*l2_l2*Bk3*fl16*fl27 - 4.E+0*P_q*pow(k4_k5,2)*k4_l1*
             q_l2*l1_l2*Bk3*fl16*fl27 - 4.E+0*P_q*k4_q*k4_l1*k4_l2*k5_k5*
             l1_l2*Bk3*fl16*fl27 + 4.E+0*P_q*k4_q*k4_l1*k4_l2*k5_l1*k5_l2*
             Bk3*fl16*fl27 - 4.E+0*P_q*k4_q*pow(k4_l1,2)*pow(k5_l2,2)*Bk3*
             fl16*fl27 + 4.E+0*P_q*k4_l1*pow(k4_l2,2)*k5_k5*q_l1*Bk3*fl16*
             fl27 - 4.E+0*P_q*k4_l1*pow(k4_l2,2)*k5_q*k5_l1*Bk3*fl16*fl27
              + 4.E+0*P_q*pow(k4_l1,2)*k4_l2*k5_q*k5_l2*Bk3*fl16*fl27 );

          Chi(9,10) +=  + F * (  - 4.E+0*P_k4*k4_k5*k4_l1*k5_l1*l2_l2*Bk3
             *fl16*fl27 - 4.E+0*P_k4*k4_l1*k4_l2*k5_k5*l1_l2*Bk3*fl16*fl27
              + 4.E+0*P_k4*k4_l1*k4_l2*k5_l1*k5_l2*Bk3*fl16*fl27 - 4.E+0*
             P_k4*pow(k4_l1,2)*pow(k5_l2,2)*Bk3*fl16*fl27 + 4.E+0*P_k5*
             k4_k4*k4_l1*k5_l1*l2_l2*Bk3*fl16*fl27 + 4.E+0*P_k5*k4_k5*k4_l1
             *k4_l2*l1_l2*Bk3*fl16*fl27 - 4.E+0*P_k5*k4_l1*pow(k4_l2,2)*
             k5_l1*Bk3*fl16*fl27 + 4.E+0*P_k5*pow(k4_l1,2)*k4_l2*k5_l2*Bk3*
             fl16*fl27 - 4.E+0*P_l1*k4_k4*k4_l1*k5_k5*l2_l2*Bk3*fl16*fl27
              - 4.E+0*P_l1*k4_k5*k4_l1*k4_l2*k5_l2*Bk3*fl16*fl27 + 4.E+0*
             P_l1*pow(k4_k5,2)*k4_l1*l2_l2*Bk3*fl16*fl27 + 4.E+0*P_l1*k4_l1
             *pow(k4_l2,2)*k5_k5*Bk3*fl16*fl27 + 4.E+0*P_l2*k4_k4*k4_l1*
             k5_k5*l1_l2*Bk3*fl16*fl27 - 4.E+0*P_l2*k4_k4*k4_l1*k5_l1*k5_l2
             *Bk3*fl16*fl27 + 4.E+0*P_l2*k4_k5*k4_l1*k4_l2*k5_l1*Bk3*fl16*
             fl27 - 4.E+0*P_l2*pow(k4_k5,2)*k4_l1*l1_l2*Bk3*fl16*fl27 );

          Chi(9,10) +=  + E * (  - 4.E+0*k3_k4*k4_k5*k4_l1*k5_l1*l2_l2*
             Ak3*fl16*fl27 - 4.E+0*k3_k4*k4_l1*k4_l2*k5_k5*l1_l2*Ak3*fl16*
             fl27 + 4.E+0*k3_k4*k4_l1*k4_l2*k5_l1*k5_l2*Ak3*fl16*fl27 -
             4.E+0*k3_k4*pow(k4_l1,2)*pow(k5_l2,2)*Ak3*fl16*fl27 + 4.E+0*
             k3_k5*k4_k4*k4_l1*k5_l1*l2_l2*Ak3*fl16*fl27 + 4.E+0*k3_k5*
             k4_k5*k4_l1*k4_l2*l1_l2*Ak3*fl16*fl27 - 4.E+0*k3_k5*k4_l1*pow(
             k4_l2,2)*k5_l1*Ak3*fl16*fl27 + 4.E+0*k3_k5*pow(k4_l1,2)*k4_l2*
             k5_l2*Ak3*fl16*fl27 - 4.E+0*k3_l1*k4_k4*k4_l1*k5_k5*l2_l2*Ak3*
             fl16*fl27 - 4.E+0*k3_l1*k4_k5*k4_l1*k4_l2*k5_l2*Ak3*fl16*fl27
              + 4.E+0*k3_l1*pow(k4_k5,2)*k4_l1*l2_l2*Ak3*fl16*fl27 + 4.E+0*
             k3_l1*k4_l1*pow(k4_l2,2)*k5_k5*Ak3*fl16*fl27 + 4.E+0*k3_l2*
             k4_k4*k4_l1*k5_k5*l1_l2*Ak3*fl16*fl27 - 4.E+0*k3_l2*k4_k4*
             k4_l1*k5_l1*k5_l2*Ak3*fl16*fl27 + 4.E+0*k3_l2*k4_k5*k4_l1*
             k4_l2*k5_l1*Ak3*fl16*fl27 - 4.E+0*k3_l2*pow(k4_k5,2)*k4_l1*
             l1_l2*Ak3*fl16*fl27 );

       Chi(10,0) =
           + H * ( 8.E+0*x2*P_k3*k4_k4*k5_q*l1_l1*Ak3*fl17 - 8.E+0*x2*P_k3*
             k4_k4*k5_l1*q_l1*Ak3*fl17 - 8.E+0*x2*P_k3*k4_k5*k4_q*l1_l1*Ak3
             *fl17 + 8.E+0*x2*P_k3*k4_k5*k4_l1*q_l1*Ak3*fl17 + 8.E+0*x2*
             P_k3*k4_q*k4_l1*k5_l1*Ak3*fl17 - 8.E+0*x2*P_k3*pow(k4_l1,2)*
             k5_q*Ak3*fl17 - 1.6E+1*x2*P_k4*k3_k4*k5_q*l1_l1*Ak3*fl17 +
             1.6E+1*x2*P_k4*k3_k4*k5_l1*q_l1*Ak3*fl17 - 8.E+0*x2*P_k4*k3_k5
             *k4_l1*q_l1*Ak3*fl17 + 8.E+0*x2*P_k4*k3_q*k4_k5*l1_l1*Ak3*fl17
              - 8.E+0*x2*P_k4*k3_q*k4_l1*k5_l1*Ak3*fl17 - 1.6E+1*x2*P_k4*
             k3_l1*k4_k5*q_l1*Ak3*fl17 + 8.E+0*x2*P_k4*k3_l1*k4_l1*k5_q*Ak3
             *fl17 + 1.6E+1*x2*P_k5*k3_k4*k4_q*l1_l1*Ak3*fl17 - 8.E+0*x2*
             P_k5*k3_k4*k4_l1*q_l1*Ak3*fl17 - 8.E+0*x2*P_k5*k3_q*k4_k4*
             l1_l1*Ak3*fl17 + 8.E+0*x2*P_k5*k3_q*pow(k4_l1,2)*Ak3*fl17 +
             1.6E+1*x2*P_k5*k3_l1*k4_k4*q_l1*Ak3*fl17 - 8.E+0*x2*P_k5*k3_l1
             *k4_q*k4_l1*Ak3*fl17 - 1.6E+1*x2*P_l1*k3_k4*k4_q*k5_l1*Ak3*
             fl17 + 8.E+0*x2*P_l1*k3_k4*k4_l1*k5_q*Ak3*fl17 + 8.E+0*x2*P_l1
             *k3_k5*k4_q*k4_l1*Ak3*fl17 + 8.E+0*x2*P_l1*k3_q*k4_k4*k5_l1*
             Ak3*fl17 - 8.E+0*x2*P_l1*k3_q*k4_k5*k4_l1*Ak3*fl17 - 1.6E+1*x2
             *P_l1*k3_l1*k4_k4*k5_q*Ak3*fl17 + 1.6E+1*x2*P_l1*k3_l1*k4_k5*
             k4_q*Ak3*fl17 );

          Chi(10,0) +=  + G * ( 4.E+0*x2*P_q*k4_k4*k5_q*l1_l1*Bk3*fl17 -
             4.E+0*x2*P_q*k4_k4*k5_l1*q_l1*Bk3*fl17 - 4.E+0*x2*P_q*k4_k5*
             k4_q*l1_l1*Bk3*fl17 + 4.E+0*x2*P_q*k4_k5*k4_l1*q_l1*Bk3*fl17
              - 4.E+0*x2*P_q*k4_q*k4_l1*k5_l1*Bk3*fl17 + 4.E+0*x2*P_q*pow(
             k4_l1,2)*k5_q*Bk3*fl17 );

          Chi(10,0) +=  + F * (  - 4.E+0*x2*P_k4*k4_k5*l1_l1*Bk3*fl17 -
             4.E+0*x2*P_k4*k4_l1*k5_l1*Bk3*fl17 + 4.E+0*x2*P_k5*k4_k4*l1_l1
             *Bk3*fl17 + 4.E+0*x2*P_k5*pow(k4_l1,2)*Bk3*fl17 - 4.E+0*x2*
             P_l1*k4_k4*k5_l1*Bk3*fl17 + 4.E+0*x2*P_l1*k4_k5*k4_l1*Bk3*fl17
              );

          Chi(10,0) +=  + E * ( 4.E+0*x2*k3_k4*k4_k5*l1_l1*Ak3*fl17 +
             4.E+0*x2*k3_k4*k4_l1*k5_l1*Ak3*fl17 - 4.E+0*x2*k3_k5*k4_k4*
             l1_l1*Ak3*fl17 - 4.E+0*x2*k3_k5*pow(k4_l1,2)*Ak3*fl17 + 4.E+0*
             x2*k3_l1*k4_k4*k5_l1*Ak3*fl17 - 4.E+0*x2*k3_l1*k4_k5*k4_l1*Ak3
             *fl17 );

       Chi(10,1) =
           + H * ( 1.6E+1*y2*P_k3*k4_q*k4_l1*k5_l1*l2_l2*Ak3*fl17 - 1.6E+1*
             y2*P_k3*k4_q*k4_l1*k5_l2*l1_l2*Ak3*fl17 + 1.6E+1*y2*P_k3*k4_l1
             *k4_l2*k5_q*l1_l2*Ak3*fl17 - 1.6E+1*y2*P_k3*k4_l1*k4_l2*k5_l1*
             q_l2*Ak3*fl17 - 1.6E+1*y2*P_k3*pow(k4_l1,2)*k5_q*l2_l2*Ak3*
             fl17 + 1.6E+1*y2*P_k3*pow(k4_l1,2)*k5_l2*q_l2*Ak3*fl17 -
             1.6E+1*y2*P_k4*k3_k4*k5_q*l1_l1*l2_l2*Ak3*fl17 + 1.6E+1*y2*
             P_k4*k3_k4*k5_q*pow(l1_l2,2)*Ak3*fl17 + 1.6E+1*y2*P_k4*k3_k4*
             k5_l1*q_l1*l2_l2*Ak3*fl17 - 1.6E+1*y2*P_k4*k3_k4*k5_l1*q_l2*
             l1_l2*Ak3*fl17 - 1.6E+1*y2*P_k4*k3_k4*k5_l2*q_l1*l1_l2*Ak3*
             fl17 + 1.6E+1*y2*P_k4*k3_k4*k5_l2*q_l2*l1_l1*Ak3*fl17 + 4.E+0*
             y2*P_k4*k3_k5*k4_q*l1_l1*l2_l2*Ak3*fl17 - 4.E+0*y2*P_k4*k3_k5*
             k4_q*pow(l1_l2,2)*Ak3*fl17 - 4.E+0*y2*P_k4*k3_k5*k4_l1*q_l1*
             l2_l2*Ak3*fl17 + 4.E+0*y2*P_k4*k3_k5*k4_l1*q_l2*l1_l2*Ak3*fl17
              + 4.E+0*y2*P_k4*k3_k5*k4_l2*q_l1*l1_l2*Ak3*fl17 - 2.E+1*y2*
             P_k4*k3_k5*k4_l2*q_l2*l1_l1*Ak3*fl17 - 4.E+0*y2*P_k4*k3_q*
             k4_k5*l1_l1*l2_l2*Ak3*fl17 + 4.E+0*y2*P_k4*k3_q*k4_k5*pow(
             l1_l2,2)*Ak3*fl17 - 1.2E+1*y2*P_k4*k3_q*k4_l1*k5_l1*l2_l2*Ak3*
             fl17 + 1.2E+1*y2*P_k4*k3_q*k4_l1*k5_l2*l1_l2*Ak3*fl17 - 4.E+0*
             y2*P_k4*k3_q*k4_l2*k5_l1*l1_l2*Ak3*fl17 + 4.E+0*y2*P_k4*k3_q*
             k4_l2*k5_l2*l1_l1*Ak3*fl17 - 1.2E+1*y2*P_k4*k3_l1*k4_k5*q_l1*
             l2_l2*Ak3*fl17 - 4.E+0*y2*P_k4*k3_l1*k4_k5*q_l2*l1_l2*Ak3*fl17
              - 4.E+0*y2*P_k4*k3_l1*k4_q*k5_l1*l2_l2*Ak3*fl17 + 4.E+0*y2*
             P_k4*k3_l1*k4_q*k5_l2*l1_l2*Ak3*fl17 + 1.6E+1*y2*P_k4*k3_l1*
             k4_l1*k5_q*l2_l2*Ak3*fl17 - 1.6E+1*y2*P_k4*k3_l1*k4_l1*k5_l2*
             q_l2*Ak3*fl17 - 1.6E+1*y2*P_k4*k3_l1*k4_l2*k5_q*l1_l2*Ak3*fl17
              + 2.E+1*y2*P_k4*k3_l1*k4_l2*k5_l1*q_l2*Ak3*fl17 + 1.2E+1*y2*
             P_k4*k3_l1*k4_l2*k5_l2*q_l1*Ak3*fl17 - 4.E+0*y2*P_k4*k3_l2*
             k4_k5*q_l1*l1_l2*Ak3*fl17 + 2.E+1*y2*P_k4*k3_l2*k4_k5*q_l2*
             l1_l1*Ak3*fl17 + 4.E+0*y2*P_k4*k3_l2*k4_q*k5_l1*l1_l2*Ak3*fl17
              - 4.E+0*y2*P_k4*k3_l2*k4_q*k5_l2*l1_l1*Ak3*fl17 - 1.6E+1*y2*
             P_k4*k3_l2*k4_l1*k5_q*l1_l2*Ak3*fl17 + 1.2E+1*y2*P_k4*k3_l2*
             k4_l1*k5_l1*q_l2*Ak3*fl17 + 4.E+0*y2*P_k4*k3_l2*k4_l1*k5_l2*
             q_l1*Ak3*fl17 + 1.2E+1*y2*P_k5*k3_k4*k4_q*l1_l1*l2_l2*Ak3*fl17
              - 1.2E+1*y2*P_k5*k3_k4*k4_q*pow(l1_l2,2)*Ak3*fl17 - 1.2E+1*y2
             *P_k5*k3_k4*k4_l1*q_l1*l2_l2*Ak3*fl17 + 1.2E+1*y2*P_k5*k3_k4*
             k4_l1*q_l2*l1_l2*Ak3*fl17 + 1.2E+1*y2*P_k5*k3_k4*k4_l2*q_l1*
             l1_l2*Ak3*fl17 + 4.E+0*y2*P_k5*k3_k4*k4_l2*q_l2*l1_l1*Ak3*fl17
              + 4.E+0*y2*P_k5*k3_q*k4_k4*l1_l1*l2_l2*Ak3*fl17 - 4.E+0*y2*
             P_k5*k3_q*k4_k4*pow(l1_l2,2)*Ak3*fl17 - 8.E+0*y2*P_k5*k3_q*
             k4_l1*k4_l2*l1_l2*Ak3*fl17 + 1.2E+1*y2*P_k5*k3_q*pow(k4_l1,2)*
             l2_l2*Ak3*fl17 - 4.E+0*y2*P_k5*k3_q*pow(k4_l2,2)*l1_l1*Ak3*
             fl17 + 1.2E+1*y2*P_k5*k3_l1*k4_k4*q_l1*l2_l2*Ak3*fl17 + 4.E+0*
             y2*P_k5*k3_l1*k4_k4*q_l2*l1_l2*Ak3*fl17 - 1.2E+1*y2*P_k5*k3_l1
             *k4_q*k4_l1*l2_l2*Ak3*fl17 + 1.2E+1*y2*P_k5*k3_l1*k4_q*k4_l2*
             l1_l2*Ak3*fl17 - 4.E+0*y2*P_k5*k3_l1*k4_l1*k4_l2*q_l2*Ak3*fl17
              - 1.2E+1*y2*P_k5*k3_l1*pow(k4_l2,2)*q_l1*Ak3*fl17 + 4.E+0*y2*
             P_k5*k3_l2*k4_k4*q_l1*l1_l2*Ak3*fl17 - 2.E+1*y2*P_k5*k3_l2*
             k4_k4*q_l2*l1_l1*Ak3*fl17 + 1.2E+1*y2*P_k5*k3_l2*k4_q*k4_l1*
             l1_l2*Ak3*fl17 + 4.E+0*y2*P_k5*k3_l2*k4_q*k4_l2*l1_l1*Ak3*fl17
              - 4.E+0*y2*P_k5*k3_l2*k4_l1*k4_l2*q_l1*Ak3*fl17 - 1.2E+1*y2*
             P_k5*k3_l2*pow(k4_l1,2)*q_l2*Ak3*fl17 );

          Chi(10,1) +=  + H * ( 4.E+0*y2*P_q*k3_k4*k4_k5*l1_l1*l2_l2*Ak3*
             fl17 - 4.E+0*y2*P_q*k3_k4*k4_k5*pow(l1_l2,2)*Ak3*fl17 - 4.E+0*
             y2*P_q*k3_k4*k4_l1*k5_l1*l2_l2*Ak3*fl17 + 4.E+0*y2*P_q*k3_k4*
             k4_l1*k5_l2*l1_l2*Ak3*fl17 + 4.E+0*y2*P_q*k3_k4*k4_l2*k5_l1*
             l1_l2*Ak3*fl17 - 4.E+0*y2*P_q*k3_k4*k4_l2*k5_l2*l1_l1*Ak3*fl17
              - 4.E+0*y2*P_q*k3_k5*k4_k4*l1_l1*l2_l2*Ak3*fl17 + 4.E+0*y2*
             P_q*k3_k5*k4_k4*pow(l1_l2,2)*Ak3*fl17 - 8.E+0*y2*P_q*k3_k5*
             k4_l1*k4_l2*l1_l2*Ak3*fl17 + 4.E+0*y2*P_q*k3_k5*pow(k4_l1,2)*
             l2_l2*Ak3*fl17 + 4.E+0*y2*P_q*k3_k5*pow(k4_l2,2)*l1_l1*Ak3*
             fl17 + 4.E+0*y2*P_q*k3_l1*k4_k4*k5_l1*l2_l2*Ak3*fl17 - 4.E+0*
             y2*P_q*k3_l1*k4_k4*k5_l2*l1_l2*Ak3*fl17 - 4.E+0*y2*P_q*k3_l1*
             k4_k5*k4_l1*l2_l2*Ak3*fl17 + 4.E+0*y2*P_q*k3_l1*k4_k5*k4_l2*
             l1_l2*Ak3*fl17 + 4.E+0*y2*P_q*k3_l1*k4_l1*k4_l2*k5_l2*Ak3*fl17
              - 4.E+0*y2*P_q*k3_l1*pow(k4_l2,2)*k5_l1*Ak3*fl17 - 4.E+0*y2*
             P_q*k3_l2*k4_k4*k5_l1*l1_l2*Ak3*fl17 + 4.E+0*y2*P_q*k3_l2*
             k4_k4*k5_l2*l1_l1*Ak3*fl17 + 4.E+0*y2*P_q*k3_l2*k4_k5*k4_l1*
             l1_l2*Ak3*fl17 - 4.E+0*y2*P_q*k3_l2*k4_k5*k4_l2*l1_l1*Ak3*fl17
              + 4.E+0*y2*P_q*k3_l2*k4_l1*k4_l2*k5_l1*Ak3*fl17 - 4.E+0*y2*
             P_q*k3_l2*pow(k4_l1,2)*k5_l2*Ak3*fl17 - 4.E+0*y2*P_l1*k3_k4*
             k4_k5*q_l1*l2_l2*Ak3*fl17 + 2.E+1*y2*P_l1*k3_k4*k4_k5*q_l2*
             l1_l2*Ak3*fl17 - 1.2E+1*y2*P_l1*k3_k4*k4_q*k5_l1*l2_l2*Ak3*
             fl17 + 1.2E+1*y2*P_l1*k3_k4*k4_q*k5_l2*l1_l2*Ak3*fl17 + 1.6E+1
             *y2*P_l1*k3_k4*k4_l1*k5_q*l2_l2*Ak3*fl17 - 1.6E+1*y2*P_l1*
             k3_k4*k4_l1*k5_l2*q_l2*Ak3*fl17 - 1.6E+1*y2*P_l1*k3_k4*k4_l2*
             k5_q*l1_l2*Ak3*fl17 - 4.E+0*y2*P_l1*k3_k4*k4_l2*k5_l1*q_l2*Ak3
             *fl17 + 4.E+0*y2*P_l1*k3_k4*k4_l2*k5_l2*q_l1*Ak3*fl17 + 4.E+0*
             y2*P_l1*k3_k5*k4_k4*q_l1*l2_l2*Ak3*fl17 - 2.E+1*y2*P_l1*k3_k5*
             k4_k4*q_l2*l1_l2*Ak3*fl17 - 4.E+0*y2*P_l1*k3_k5*k4_q*k4_l1*
             l2_l2*Ak3*fl17 + 4.E+0*y2*P_l1*k3_k5*k4_q*k4_l2*l1_l2*Ak3*fl17
              + 2.E+1*y2*P_l1*k3_k5*k4_l1*k4_l2*q_l2*Ak3*fl17 - 4.E+0*y2*
             P_l1*k3_k5*pow(k4_l2,2)*q_l1*Ak3*fl17 - 4.E+0*y2*P_l1*k3_q*
             k4_k4*k5_l1*l2_l2*Ak3*fl17 + 4.E+0*y2*P_l1*k3_q*k4_k4*k5_l2*
             l1_l2*Ak3*fl17 + 4.E+0*y2*P_l1*k3_q*k4_k5*k4_l1*l2_l2*Ak3*fl17
              - 4.E+0*y2*P_l1*k3_q*k4_k5*k4_l2*l1_l2*Ak3*fl17 - 4.E+0*y2*
             P_l1*k3_q*k4_l1*k4_l2*k5_l2*Ak3*fl17 + 4.E+0*y2*P_l1*k3_q*pow(
             k4_l2,2)*k5_l1*Ak3*fl17 - 1.6E+1*y2*P_l1*k3_l1*k4_k4*k5_q*
             l2_l2*Ak3*fl17 + 1.6E+1*y2*P_l1*k3_l1*k4_k4*k5_l2*q_l2*Ak3*
             fl17 + 1.6E+1*y2*P_l1*k3_l1*k4_k5*k4_q*l2_l2*Ak3*fl17 - 1.6E+1
             *y2*P_l1*k3_l1*k4_k5*k4_l2*q_l2*Ak3*fl17 - 1.6E+1*y2*P_l1*
             k3_l1*k4_q*k4_l2*k5_l2*Ak3*fl17 + 1.6E+1*y2*P_l1*k3_l1*pow(
             k4_l2,2)*k5_q*Ak3*fl17 + 2.E+1*y2*P_l1*k3_l2*k4_k4*k5_l1*q_l2*
             Ak3*fl17 - 4.E+0*y2*P_l1*k3_l2*k4_k4*k5_l2*q_l1*Ak3*fl17 -
             2.E+1*y2*P_l1*k3_l2*k4_k5*k4_l1*q_l2*Ak3*fl17 + 4.E+0*y2*P_l1*
             k3_l2*k4_k5*k4_l2*q_l1*Ak3*fl17 + 4.E+0*y2*P_l1*k3_l2*k4_q*
             k4_l1*k5_l2*Ak3*fl17 - 4.E+0*y2*P_l1*k3_l2*k4_q*k4_l2*k5_l1*
             Ak3*fl17 - 1.2E+1*y2*P_l2*k3_k4*k4_k5*q_l1*l1_l2*Ak3*fl17 -
             4.E+0*y2*P_l2*k3_k4*k4_k5*q_l2*l1_l1*Ak3*fl17 + 1.2E+1*y2*P_l2
             *k3_k4*k4_q*k5_l1*l1_l2*Ak3*fl17 - 1.2E+1*y2*P_l2*k3_k4*k4_q*
             k5_l2*l1_l1*Ak3*fl17 - 1.6E+1*y2*P_l2*k3_k4*k4_l1*k5_q*l1_l2*
             Ak3*fl17 + 4.E+0*y2*P_l2*k3_k4*k4_l1*k5_l1*q_l2*Ak3*fl17 +
             1.2E+1*y2*P_l2*k3_k4*k4_l1*k5_l2*q_l1*Ak3*fl17 );

          Chi(10,1) +=  + H * ( 1.2E+1*y2*P_l2*k3_k5*k4_k4*q_l1*l1_l2*Ak3
             *fl17 + 4.E+0*y2*P_l2*k3_k5*k4_k4*q_l2*l1_l1*Ak3*fl17 + 4.E+0*
             y2*P_l2*k3_k5*k4_q*k4_l1*l1_l2*Ak3*fl17 + 1.2E+1*y2*P_l2*k3_k5
             *k4_q*k4_l2*l1_l1*Ak3*fl17 - 1.2E+1*y2*P_l2*k3_k5*k4_l1*k4_l2*
             q_l1*Ak3*fl17 - 4.E+0*y2*P_l2*k3_k5*pow(k4_l1,2)*q_l2*Ak3*fl17
              + 4.E+0*y2*P_l2*k3_q*k4_k4*k5_l1*l1_l2*Ak3*fl17 - 4.E+0*y2*
             P_l2*k3_q*k4_k4*k5_l2*l1_l1*Ak3*fl17 - 4.E+0*y2*P_l2*k3_q*
             k4_k5*k4_l1*l1_l2*Ak3*fl17 + 4.E+0*y2*P_l2*k3_q*k4_k5*k4_l2*
             l1_l1*Ak3*fl17 + 1.2E+1*y2*P_l2*k3_q*k4_l1*k4_l2*k5_l1*Ak3*
             fl17 - 1.2E+1*y2*P_l2*k3_q*pow(k4_l1,2)*k5_l2*Ak3*fl17 - 4.E+0
             *y2*P_l2*k3_l1*k4_k4*k5_l1*q_l2*Ak3*fl17 - 1.2E+1*y2*P_l2*
             k3_l1*k4_k4*k5_l2*q_l1*Ak3*fl17 + 4.E+0*y2*P_l2*k3_l1*k4_k5*
             k4_l1*q_l2*Ak3*fl17 + 1.2E+1*y2*P_l2*k3_l1*k4_k5*k4_l2*q_l1*
             Ak3*fl17 + 1.2E+1*y2*P_l2*k3_l1*k4_q*k4_l1*k5_l2*Ak3*fl17 -
             1.2E+1*y2*P_l2*k3_l1*k4_q*k4_l2*k5_l1*Ak3*fl17 + 1.6E+1*y2*
             P_l2*k3_l2*k4_k4*k5_q*l1_l1*Ak3*fl17 - 1.6E+1*y2*P_l2*k3_l2*
             k4_k4*k5_l1*q_l1*Ak3*fl17 - 1.6E+1*y2*P_l2*k3_l2*k4_k5*k4_q*
             l1_l1*Ak3*fl17 + 1.6E+1*y2*P_l2*k3_l2*k4_k5*k4_l1*q_l1*Ak3*
             fl17 - 1.6E+1*y2*P_l2*k3_l2*k4_q*k4_l1*k5_l1*Ak3*fl17 + 1.6E+1
             *y2*P_l2*k3_l2*pow(k4_l1,2)*k5_q*Ak3*fl17 );

          Chi(10,1) +=  + G * ( 8.E+0*y2*P_q*k4_k4*k5_q*l1_l1*l2_l2*Bk3*
             fl17 - 8.E+0*y2*P_q*k4_k4*k5_q*pow(l1_l2,2)*Bk3*fl17 - 8.E+0*
             y2*P_q*k4_k4*k5_l1*q_l1*l2_l2*Bk3*fl17 + 8.E+0*y2*P_q*k4_k4*
             k5_l1*q_l2*l1_l2*Bk3*fl17 + 8.E+0*y2*P_q*k4_k4*k5_l2*q_l1*
             l1_l2*Bk3*fl17 - 8.E+0*y2*P_q*k4_k4*k5_l2*q_l2*l1_l1*Bk3*fl17
              - 8.E+0*y2*P_q*k4_k5*k4_q*l1_l1*l2_l2*Bk3*fl17 + 8.E+0*y2*P_q
             *k4_k5*k4_q*pow(l1_l2,2)*Bk3*fl17 + 8.E+0*y2*P_q*k4_k5*k4_l1*
             q_l1*l2_l2*Bk3*fl17 - 8.E+0*y2*P_q*k4_k5*k4_l1*q_l2*l1_l2*Bk3*
             fl17 - 8.E+0*y2*P_q*k4_k5*k4_l2*q_l1*l1_l2*Bk3*fl17 + 8.E+0*y2
             *P_q*k4_k5*k4_l2*q_l2*l1_l1*Bk3*fl17 - 8.E+0*y2*P_q*k4_q*k4_l2
             *k5_l1*l1_l2*Bk3*fl17 + 8.E+0*y2*P_q*k4_q*k4_l2*k5_l2*l1_l1*
             Bk3*fl17 + 8.E+0*y2*P_q*k4_l1*k4_l2*k5_q*l1_l2*Bk3*fl17 -
             8.E+0*y2*P_q*k4_l1*k4_l2*k5_l2*q_l1*Bk3*fl17 - 8.E+0*y2*P_q*
             pow(k4_l2,2)*k5_q*l1_l1*Bk3*fl17 + 8.E+0*y2*P_q*pow(k4_l2,2)*
             k5_l1*q_l1*Bk3*fl17 );

          Chi(10,1) +=  + F * (  - 8.E+0*y2*P_k4*k4_k5*l1_l1*l2_l2*Bk3*
             fl17 + 8.E+0*y2*P_k4*k4_k5*pow(l1_l2,2)*Bk3*fl17 - 8.E+0*y2*
             P_k4*k4_l2*k5_l1*l1_l2*Bk3*fl17 + 8.E+0*y2*P_k4*k4_l2*k5_l2*
             l1_l1*Bk3*fl17 + 8.E+0*y2*P_k5*k4_k4*l1_l1*l2_l2*Bk3*fl17 -
             8.E+0*y2*P_k5*k4_k4*pow(l1_l2,2)*Bk3*fl17 + 8.E+0*y2*P_k5*
             k4_l1*k4_l2*l1_l2*Bk3*fl17 - 8.E+0*y2*P_k5*pow(k4_l2,2)*l1_l1*
             Bk3*fl17 - 8.E+0*y2*P_l1*k4_k4*k5_l1*l2_l2*Bk3*fl17 + 8.E+0*y2
             *P_l1*k4_k4*k5_l2*l1_l2*Bk3*fl17 + 8.E+0*y2*P_l1*k4_k5*k4_l1*
             l2_l2*Bk3*fl17 - 8.E+0*y2*P_l1*k4_k5*k4_l2*l1_l2*Bk3*fl17 -
             8.E+0*y2*P_l1*k4_l1*k4_l2*k5_l2*Bk3*fl17 + 8.E+0*y2*P_l1*pow(
             k4_l2,2)*k5_l1*Bk3*fl17 + 8.E+0*y2*P_l2*k4_k4*k5_l1*l1_l2*Bk3*
             fl17 - 8.E+0*y2*P_l2*k4_k4*k5_l2*l1_l1*Bk3*fl17 - 8.E+0*y2*
             P_l2*k4_k5*k4_l1*l1_l2*Bk3*fl17 + 8.E+0*y2*P_l2*k4_k5*k4_l2*
             l1_l1*Bk3*fl17 );

          Chi(10,1) +=  + E * ( 8.E+0*y2*k3_k4*k4_k5*l1_l1*l2_l2*Ak3*fl17
              - 8.E+0*y2*k3_k4*k4_k5*pow(l1_l2,2)*Ak3*fl17 + 8.E+0*y2*k3_k4
             *k4_l2*k5_l1*l1_l2*Ak3*fl17 - 8.E+0*y2*k3_k4*k4_l2*k5_l2*l1_l1
             *Ak3*fl17 - 8.E+0*y2*k3_k5*k4_k4*l1_l1*l2_l2*Ak3*fl17 + 8.E+0*
             y2*k3_k5*k4_k4*pow(l1_l2,2)*Ak3*fl17 - 8.E+0*y2*k3_k5*k4_l1*
             k4_l2*l1_l2*Ak3*fl17 + 8.E+0*y2*k3_k5*pow(k4_l2,2)*l1_l1*Ak3*
             fl17 + 8.E+0*y2*k3_l1*k4_k4*k5_l1*l2_l2*Ak3*fl17 - 8.E+0*y2*
             k3_l1*k4_k4*k5_l2*l1_l2*Ak3*fl17 - 8.E+0*y2*k3_l1*k4_k5*k4_l1*
             l2_l2*Ak3*fl17 + 8.E+0*y2*k3_l1*k4_k5*k4_l2*l1_l2*Ak3*fl17 +
             8.E+0*y2*k3_l1*k4_l1*k4_l2*k5_l2*Ak3*fl17 - 8.E+0*y2*k3_l1*
             pow(k4_l2,2)*k5_l1*Ak3*fl17 - 8.E+0*y2*k3_l2*k4_k4*k5_l1*l1_l2
             *Ak3*fl17 + 8.E+0*y2*k3_l2*k4_k4*k5_l2*l1_l1*Ak3*fl17 + 8.E+0*
             y2*k3_l2*k4_k5*k4_l1*l1_l2*Ak3*fl17 - 8.E+0*y2*k3_l2*k4_k5*
             k4_l2*l1_l1*Ak3*fl17 );

       Chi(10,2) =
           + H * (  - 1.6E+1*z2*P_k4*k4_k5*q_l1*l1_l2*Bk3*fl17 + 1.6E+1*z2*
             P_k4*k4_k5*q_l2*l1_l1*Bk3*fl17 - 1.6E+1*z2*P_k4*k4_l2*k5_q*
             l1_l1*Bk3*fl17 + 1.6E+1*z2*P_k4*k4_l2*k5_l1*q_l1*Bk3*fl17 +
             1.6E+1*z2*P_k5*k4_k4*q_l1*l1_l2*Bk3*fl17 - 1.6E+1*z2*P_k5*
             k4_k4*q_l2*l1_l1*Bk3*fl17 + 1.6E+1*z2*P_k5*k4_q*k4_l2*l1_l1*
             Bk3*fl17 - 1.6E+1*z2*P_k5*k4_l1*k4_l2*q_l1*Bk3*fl17 - 1.6E+1*
             z2*P_l1*k4_k4*k5_q*l1_l2*Bk3*fl17 + 1.6E+1*z2*P_l1*k4_k4*k5_l1
             *q_l2*Bk3*fl17 + 1.6E+1*z2*P_l1*k4_k5*k4_q*l1_l2*Bk3*fl17 -
             1.6E+1*z2*P_l1*k4_k5*k4_l1*q_l2*Bk3*fl17 - 1.6E+1*z2*P_l1*k4_q
             *k4_l2*k5_l1*Bk3*fl17 + 1.6E+1*z2*P_l1*k4_l1*k4_l2*k5_q*Bk3*
             fl17 + 1.6E+1*z2*P_l2*k4_k4*k5_q*l1_l1*Bk3*fl17 - 1.6E+1*z2*
             P_l2*k4_k4*k5_l1*q_l1*Bk3*fl17 - 1.6E+1*z2*P_l2*k4_k5*k4_q*
             l1_l1*Bk3*fl17 + 1.6E+1*z2*P_l2*k4_k5*k4_l1*q_l1*Bk3*fl17 );

          Chi(10,2) +=  + G * ( 8.E+0*z2*P_q*k3_k4*k4_k5*q_l1*l1_l2*Ak3*
             fl17 - 8.E+0*z2*P_q*k3_k4*k4_k5*q_l2*l1_l1*Ak3*fl17 + 8.E+0*z2
             *P_q*k3_k4*k4_l2*k5_q*l1_l1*Ak3*fl17 - 8.E+0*z2*P_q*k3_k4*
             k4_l2*k5_l1*q_l1*Ak3*fl17 - 8.E+0*z2*P_q*k3_k5*k4_k4*q_l1*
             l1_l2*Ak3*fl17 + 8.E+0*z2*P_q*k3_k5*k4_k4*q_l2*l1_l1*Ak3*fl17
              - 8.E+0*z2*P_q*k3_k5*k4_q*k4_l2*l1_l1*Ak3*fl17 + 8.E+0*z2*P_q
             *k3_k5*k4_l1*k4_l2*q_l1*Ak3*fl17 + 8.E+0*z2*P_q*k3_l1*k4_k4*
             k5_q*l1_l2*Ak3*fl17 - 8.E+0*z2*P_q*k3_l1*k4_k4*k5_l1*q_l2*Ak3*
             fl17 - 8.E+0*z2*P_q*k3_l1*k4_k5*k4_q*l1_l2*Ak3*fl17 + 8.E+0*z2
             *P_q*k3_l1*k4_k5*k4_l1*q_l2*Ak3*fl17 + 8.E+0*z2*P_q*k3_l1*k4_q
             *k4_l2*k5_l1*Ak3*fl17 - 8.E+0*z2*P_q*k3_l1*k4_l1*k4_l2*k5_q*
             Ak3*fl17 - 8.E+0*z2*P_q*k3_l2*k4_k4*k5_q*l1_l1*Ak3*fl17 +
             8.E+0*z2*P_q*k3_l2*k4_k4*k5_l1*q_l1*Ak3*fl17 + 8.E+0*z2*P_q*
             k3_l2*k4_k5*k4_q*l1_l1*Ak3*fl17 - 8.E+0*z2*P_q*k3_l2*k4_k5*
             k4_l1*q_l1*Ak3*fl17 );

          Chi(10,2) +=  + F * (  - 8.E+0*z2*P_k4*k3_k5*k4_l2*l1_l1*Ak3*
             fl17 - 8.E+0*z2*P_k4*k3_l1*k4_k5*l1_l2*Ak3*fl17 + 8.E+0*z2*
             P_k4*k3_l1*k4_l2*k5_l1*Ak3*fl17 + 8.E+0*z2*P_k4*k3_l2*k4_k5*
             l1_l1*Ak3*fl17 + 8.E+0*z2*P_k5*k3_k4*k4_l2*l1_l1*Ak3*fl17 +
             8.E+0*z2*P_k5*k3_l1*k4_k4*l1_l2*Ak3*fl17 - 8.E+0*z2*P_k5*k3_l1
             *k4_l1*k4_l2*Ak3*fl17 - 8.E+0*z2*P_k5*k3_l2*k4_k4*l1_l1*Ak3*
             fl17 + 8.E+0*z2*P_l1*k3_k4*k4_k5*l1_l2*Ak3*fl17 - 8.E+0*z2*
             P_l1*k3_k4*k4_l2*k5_l1*Ak3*fl17 - 8.E+0*z2*P_l1*k3_k5*k4_k4*
             l1_l2*Ak3*fl17 + 8.E+0*z2*P_l1*k3_k5*k4_l1*k4_l2*Ak3*fl17 +
             8.E+0*z2*P_l1*k3_l2*k4_k4*k5_l1*Ak3*fl17 - 8.E+0*z2*P_l1*k3_l2
             *k4_k5*k4_l1*Ak3*fl17 - 8.E+0*z2*P_l2*k3_k4*k4_k5*l1_l1*Ak3*
             fl17 + 8.E+0*z2*P_l2*k3_k5*k4_k4*l1_l1*Ak3*fl17 - 8.E+0*z2*
             P_l2*k3_l1*k4_k4*k5_l1*Ak3*fl17 + 8.E+0*z2*P_l2*k3_l1*k4_k5*
             k4_l1*Ak3*fl17 );

       Chi(10,3) =
           + H * ( 8.E+0*P_k3*k4_k4*k5_k5*k5_q*l1_l1*Ak3*fl17*fl20 - 8.E+0*
             P_k3*k4_k4*k5_k5*k5_l1*q_l1*Ak3*fl17*fl20 - 8.E+0*P_k3*k4_k5*
             k4_q*k5_k5*l1_l1*Ak3*fl17*fl20 + 8.E+0*P_k3*k4_k5*k4_l1*k5_k5*
             q_l1*Ak3*fl17*fl20 + 8.E+0*P_k3*k4_q*k4_l1*k5_k5*k5_l1*Ak3*
             fl17*fl20 - 8.E+0*P_k3*pow(k4_l1,2)*k5_k5*k5_q*Ak3*fl17*fl20
              - 1.6E+1*P_k4*k3_k4*k5_k5*k5_q*l1_l1*Ak3*fl17*fl20 + 1.6E+1*
             P_k4*k3_k4*k5_k5*k5_l1*q_l1*Ak3*fl17*fl20 - 8.E+0*P_k4*k3_k5*
             k4_l1*k5_k5*q_l1*Ak3*fl17*fl20 + 8.E+0*P_k4*k3_q*k4_k5*k5_k5*
             l1_l1*Ak3*fl17*fl20 - 8.E+0*P_k4*k3_q*k4_l1*k5_k5*k5_l1*Ak3*
             fl17*fl20 - 1.6E+1*P_k4*k3_l1*k4_k5*k5_k5*q_l1*Ak3*fl17*fl20
              + 8.E+0*P_k4*k3_l1*k4_l1*k5_k5*k5_q*Ak3*fl17*fl20 + 1.6E+1*
             P_k5*k3_k4*k4_q*k5_k5*l1_l1*Ak3*fl17*fl20 - 8.E+0*P_k5*k3_k4*
             k4_l1*k5_k5*q_l1*Ak3*fl17*fl20 - 8.E+0*P_k5*k3_q*k4_k4*k5_k5*
             l1_l1*Ak3*fl17*fl20 + 8.E+0*P_k5*k3_q*pow(k4_l1,2)*k5_k5*Ak3*
             fl17*fl20 + 1.6E+1*P_k5*k3_l1*k4_k4*k5_k5*q_l1*Ak3*fl17*fl20
              - 8.E+0*P_k5*k3_l1*k4_q*k4_l1*k5_k5*Ak3*fl17*fl20 - 1.6E+1*
             P_l1*k3_k4*k4_q*k5_k5*k5_l1*Ak3*fl17*fl20 + 8.E+0*P_l1*k3_k4*
             k4_l1*k5_k5*k5_q*Ak3*fl17*fl20 + 8.E+0*P_l1*k3_k5*k4_q*k4_l1*
             k5_k5*Ak3*fl17*fl20 + 8.E+0*P_l1*k3_q*k4_k4*k5_k5*k5_l1*Ak3*
             fl17*fl20 - 8.E+0*P_l1*k3_q*k4_k5*k4_l1*k5_k5*Ak3*fl17*fl20 -
             1.6E+1*P_l1*k3_l1*k4_k4*k5_k5*k5_q*Ak3*fl17*fl20 + 1.6E+1*P_l1
             *k3_l1*k4_k5*k4_q*k5_k5*Ak3*fl17*fl20 );

          Chi(10,3) +=  + G * ( 4.E+0*P_q*k4_k4*k5_k5*k5_q*l1_l1*Bk3*fl17
             *fl20 - 4.E+0*P_q*k4_k4*k5_k5*k5_l1*q_l1*Bk3*fl17*fl20 - 4.E+0
             *P_q*k4_k5*k4_q*k5_k5*l1_l1*Bk3*fl17*fl20 + 4.E+0*P_q*k4_k5*
             k4_l1*k5_k5*q_l1*Bk3*fl17*fl20 - 4.E+0*P_q*k4_q*k4_l1*k5_k5*
             k5_l1*Bk3*fl17*fl20 + 4.E+0*P_q*pow(k4_l1,2)*k5_k5*k5_q*Bk3*
             fl17*fl20 );

          Chi(10,3) +=  + F * (  - 4.E+0*P_k4*k4_k5*k5_k5*l1_l1*Bk3*fl17*
             fl20 - 4.E+0*P_k4*k4_l1*k5_k5*k5_l1*Bk3*fl17*fl20 + 4.E+0*P_k5
             *k4_k4*k5_k5*l1_l1*Bk3*fl17*fl20 + 4.E+0*P_k5*pow(k4_l1,2)*
             k5_k5*Bk3*fl17*fl20 - 4.E+0*P_l1*k4_k4*k5_k5*k5_l1*Bk3*fl17*
             fl20 + 4.E+0*P_l1*k4_k5*k4_l1*k5_k5*Bk3*fl17*fl20 );

          Chi(10,3) +=  + E * ( 4.E+0*k3_k4*k4_k5*k5_k5*l1_l1*Ak3*fl17*
             fl20 + 4.E+0*k3_k4*k4_l1*k5_k5*k5_l1*Ak3*fl17*fl20 - 4.E+0*
             k3_k5*k4_k4*k5_k5*l1_l1*Ak3*fl17*fl20 - 4.E+0*k3_k5*pow(
             k4_l1,2)*k5_k5*Ak3*fl17*fl20 + 4.E+0*k3_l1*k4_k4*k5_k5*k5_l1*
             Ak3*fl17*fl20 - 4.E+0*k3_l1*k4_k5*k4_l1*k5_k5*Ak3*fl17*fl20 );

       Chi(10,4) =
           + H * (  - 8.E+0*P_k4*k4_k5*k5_k5*k5_l2*q_l1*l1_l2*Bk3*fl17*fl21
              + 8.E+0*P_k4*k4_l1*k5_k5*k5_q*k5_l2*l1_l2*Bk3*fl17*fl21 -
             8.E+0*P_k4*k4_l1*k5_k5*k5_l1*k5_l2*q_l2*Bk3*fl17*fl21 - 8.E+0*
             P_k4*k4_l1*k5_k5*pow(k5_l2,2)*q_l1*Bk3*fl17*fl21 - 8.E+0*P_k4*
             k4_l2*k5_k5*k5_q*k5_l2*l1_l1*Bk3*fl17*fl21 + 8.E+0*P_k4*k4_l2*
             k5_k5*k5_l1*k5_l2*q_l1*Bk3*fl17*fl21 + 8.E+0*P_k5*k4_k4*k5_k5*
             k5_l2*q_l1*l1_l2*Bk3*fl17*fl21 - 8.E+0*P_k5*k4_q*k4_l1*k5_k5*
             k5_l2*l1_l2*Bk3*fl17*fl21 + 8.E+0*P_k5*k4_q*k4_l2*k5_k5*k5_l2*
             l1_l1*Bk3*fl17*fl21 + 8.E+0*P_k5*pow(k4_l1,2)*k5_k5*k5_l2*q_l2
             *Bk3*fl17*fl21 - 8.E+0*P_l1*k4_k4*k5_k5*k5_q*k5_l2*l1_l2*Bk3*
             fl17*fl21 + 8.E+0*P_l1*k4_k5*k4_q*k5_k5*k5_l2*l1_l2*Bk3*fl17*
             fl21 + 8.E+0*P_l1*k4_q*k4_l1*k5_k5*pow(k5_l2,2)*Bk3*fl17*fl21
              - 8.E+0*P_l1*k4_q*k4_l2*k5_k5*k5_l1*k5_l2*Bk3*fl17*fl21 +
             8.E+0*P_l2*k4_q*k4_l1*k5_k5*k5_l1*k5_l2*Bk3*fl17*fl21 - 8.E+0*
             P_l2*pow(k4_l1,2)*k5_k5*k5_q*k5_l2*Bk3*fl17*fl21 );

          Chi(10,4) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k5*k5_k5*k5_l2*q_l1*
             l1_l2*Ak3*fl17*fl21 - 8.E+0*P_q*k3_k4*k4_q*k5_k5*k5_l1*k5_l2*
             l1_l2*Ak3*fl17*fl21 + 8.E+0*P_q*k3_k4*k4_q*k5_k5*pow(k5_l2,2)*
             l1_l1*Ak3*fl17*fl21 + 4.E+0*P_q*k3_k4*k4_l1*k5_k5*k5_q*k5_l2*
             l1_l2*Ak3*fl17*fl21 + 4.E+0*P_q*k3_k4*k4_l1*k5_k5*k5_l1*k5_l2*
             q_l2*Ak3*fl17*fl21 - 4.E+0*P_q*k3_k4*k4_l1*k5_k5*pow(k5_l2,2)*
             q_l1*Ak3*fl17*fl21 - 4.E+0*P_q*k3_k4*k4_l2*k5_k5*k5_q*k5_l2*
             l1_l1*Ak3*fl17*fl21 + 4.E+0*P_q*k3_k4*k4_l2*k5_k5*k5_l1*k5_l2*
             q_l1*Ak3*fl17*fl21 - 4.E+0*P_q*k3_k5*k4_k4*k5_k5*k5_l2*q_l1*
             l1_l2*Ak3*fl17*fl21 + 4.E+0*P_q*k3_k5*k4_q*k4_l1*k5_k5*k5_l2*
             l1_l2*Ak3*fl17*fl21 - 4.E+0*P_q*k3_k5*k4_q*k4_l2*k5_k5*k5_l2*
             l1_l1*Ak3*fl17*fl21 - 4.E+0*P_q*k3_k5*pow(k4_l1,2)*k5_k5*k5_l2
             *q_l2*Ak3*fl17*fl21 + 4.E+0*P_q*k3_q*k4_k4*k5_k5*k5_l1*k5_l2*
             l1_l2*Ak3*fl17*fl21 - 4.E+0*P_q*k3_q*k4_k4*k5_k5*pow(k5_l2,2)*
             l1_l1*Ak3*fl17*fl21 - 4.E+0*P_q*k3_q*k4_k5*k4_l1*k5_k5*k5_l2*
             l1_l2*Ak3*fl17*fl21 + 4.E+0*P_q*k3_q*k4_k5*k4_l2*k5_k5*k5_l2*
             l1_l1*Ak3*fl17*fl21 - 4.E+0*P_q*k3_q*k4_l1*k4_l2*k5_k5*k5_l1*
             k5_l2*Ak3*fl17*fl21 + 4.E+0*P_q*k3_q*pow(k4_l1,2)*k5_k5*pow(
             k5_l2,2)*Ak3*fl17*fl21 - 4.E+0*P_q*k3_l1*k4_k4*k5_k5*k5_q*
             k5_l2*l1_l2*Ak3*fl17*fl21 + 8.E+0*P_q*k3_l1*k4_k4*k5_k5*pow(
             k5_l2,2)*q_l1*Ak3*fl17*fl21 + 4.E+0*P_q*k3_l1*k4_k5*k4_q*k5_k5
             *k5_l2*l1_l2*Ak3*fl17*fl21 - 8.E+0*P_q*k3_l1*k4_k5*k4_l2*k5_k5
             *k5_l2*q_l1*Ak3*fl17*fl21 - 4.E+0*P_q*k3_l1*k4_q*k4_l1*k5_k5*
             pow(k5_l2,2)*Ak3*fl17*fl21 + 4.E+0*P_q*k3_l1*k4_q*k4_l2*k5_k5*
             k5_l1*k5_l2*Ak3*fl17*fl21 + 4.E+0*P_q*k3_l2*k4_q*k4_l1*k5_k5*
             k5_l1*k5_l2*Ak3*fl17*fl21 - 4.E+0*P_q*k3_l2*pow(k4_l1,2)*k5_k5
             *k5_q*k5_l2*Ak3*fl17*fl21 );

          Chi(10,4) +=  + F * ( 4.E+0*P_k3*k4_k4*k5_k5*k5_l1*k5_l2*l1_l2*
             Ak3*fl17*fl21 - 4.E+0*P_k3*k4_k4*k5_k5*pow(k5_l2,2)*l1_l1*Ak3*
             fl17*fl21 - 4.E+0*P_k3*k4_k5*k4_l1*k5_k5*k5_l2*l1_l2*Ak3*fl17*
             fl21 + 4.E+0*P_k3*k4_k5*k4_l2*k5_k5*k5_l2*l1_l1*Ak3*fl17*fl21
              - 4.E+0*P_k3*k4_l1*k4_l2*k5_k5*k5_l1*k5_l2*Ak3*fl17*fl21 +
             4.E+0*P_k3*pow(k4_l1,2)*k5_k5*pow(k5_l2,2)*Ak3*fl17*fl21 -
             8.E+0*P_k4*k3_k4*k5_k5*k5_l1*k5_l2*l1_l2*Ak3*fl17*fl21 + 8.E+0
             *P_k4*k3_k4*k5_k5*pow(k5_l2,2)*l1_l1*Ak3*fl17*fl21 + 4.E+0*
             P_k4*k3_k5*k4_l1*k5_k5*k5_l2*l1_l2*Ak3*fl17*fl21 - 4.E+0*P_k4*
             k3_k5*k4_l2*k5_k5*k5_l2*l1_l1*Ak3*fl17*fl21 + 4.E+0*P_k4*k3_l1
             *k4_k5*k5_k5*k5_l2*l1_l2*Ak3*fl17*fl21 - 4.E+0*P_k4*k3_l1*
             k4_l1*k5_k5*pow(k5_l2,2)*Ak3*fl17*fl21 + 4.E+0*P_k4*k3_l1*
             k4_l2*k5_k5*k5_l1*k5_l2*Ak3*fl17*fl21 + 4.E+0*P_k4*k3_l2*k4_l1
             *k5_k5*k5_l1*k5_l2*Ak3*fl17*fl21 + 4.E+0*P_k5*k3_k4*k4_l1*
             k5_k5*k5_l2*l1_l2*Ak3*fl17*fl21 - 4.E+0*P_k5*k3_k4*k4_l2*k5_k5
             *k5_l2*l1_l1*Ak3*fl17*fl21 - 4.E+0*P_k5*k3_l1*k4_k4*k5_k5*
             k5_l2*l1_l2*Ak3*fl17*fl21 - 4.E+0*P_k5*k3_l2*pow(k4_l1,2)*
             k5_k5*k5_l2*Ak3*fl17*fl21 + 4.E+0*P_l1*k3_k4*k4_k5*k5_k5*k5_l2
             *l1_l2*Ak3*fl17*fl21 - 4.E+0*P_l1*k3_k4*k4_l1*k5_k5*pow(
             k5_l2,2)*Ak3*fl17*fl21 + 4.E+0*P_l1*k3_k4*k4_l2*k5_k5*k5_l1*
             k5_l2*Ak3*fl17*fl21 - 4.E+0*P_l1*k3_k5*k4_k4*k5_k5*k5_l2*l1_l2
             *Ak3*fl17*fl21 + 8.E+0*P_l1*k3_l1*k4_k4*k5_k5*pow(k5_l2,2)*Ak3
             *fl17*fl21 - 8.E+0*P_l1*k3_l1*k4_k5*k4_l2*k5_k5*k5_l2*Ak3*fl17
             *fl21 + 4.E+0*P_l2*k3_k4*k4_l1*k5_k5*k5_l1*k5_l2*Ak3*fl17*fl21
              - 4.E+0*P_l2*k3_k5*pow(k4_l1,2)*k5_k5*k5_l2*Ak3*fl17*fl21 );

          Chi(10,4) +=  + E * ( 4.E+0*k4_k4*k5_k5*k5_l1*k5_l2*l1_l2*Bk3*
             fl17*fl21 - 4.E+0*k4_k4*k5_k5*pow(k5_l2,2)*l1_l1*Bk3*fl17*fl21
              - 4.E+0*k4_k5*k4_l1*k5_k5*k5_l2*l1_l2*Bk3*fl17*fl21 + 4.E+0*
             k4_k5*k4_l2*k5_k5*k5_l2*l1_l1*Bk3*fl17*fl21 + 4.E+0*k4_l1*
             k4_l2*k5_k5*k5_l1*k5_l2*Bk3*fl17*fl21 - 4.E+0*pow(k4_l1,2)*
             k5_k5*pow(k5_l2,2)*Bk3*fl17*fl21 );

       Chi(10,5) =
           + H * (  - 8.E+0*P_k4*k4_k5*k5_q*l1_l1*Bk3*fl17*fl22 - 8.E+0*
             P_k4*k4_l1*k5_k5*q_l1*Bk3*fl17*fl22 + 8.E+0*P_k5*k4_k4*k5_l1*
             q_l1*Bk3*fl17*fl22 + 8.E+0*P_k5*k4_k5*k4_q*l1_l1*Bk3*fl17*fl22
              - 8.E+0*P_l1*k4_k4*k5_q*k5_l1*Bk3*fl17*fl22 + 8.E+0*P_l1*k4_q
             *k4_l1*k5_k5*Bk3*fl17*fl22 );

          Chi(10,5) +=  + G * (  - 4.E+0*P_q*k3_k4*k4_k5*k5_q*l1_l1*Ak3*
             fl17*fl22 + 8.E+0*P_q*k3_k4*k4_k5*k5_l1*q_l1*Ak3*fl17*fl22 +
             8.E+0*P_q*k3_k4*k4_q*k5_k5*l1_l1*Ak3*fl17*fl22 - 8.E+0*P_q*
             k3_k4*k4_q*pow(k5_l1,2)*Ak3*fl17*fl22 - 4.E+0*P_q*k3_k4*k4_l1*
             k5_k5*q_l1*Ak3*fl17*fl22 + 8.E+0*P_q*k3_k4*k4_l1*k5_q*k5_l1*
             Ak3*fl17*fl22 - 4.E+0*P_q*k3_k5*k4_k4*k5_l1*q_l1*Ak3*fl17*fl22
              - 4.E+0*P_q*k3_k5*k4_k5*k4_q*l1_l1*Ak3*fl17*fl22 + 8.E+0*P_q*
             k3_k5*k4_q*k4_l1*k5_l1*Ak3*fl17*fl22 - 8.E+0*P_q*k3_k5*pow(
             k4_l1,2)*k5_q*Ak3*fl17*fl22 - 4.E+0*P_q*k3_q*k4_k4*k5_k5*l1_l1
             *Ak3*fl17*fl22 + 4.E+0*P_q*k3_q*k4_k4*pow(k5_l1,2)*Ak3*fl17*
             fl22 - 8.E+0*P_q*k3_q*k4_k5*k4_l1*k5_l1*Ak3*fl17*fl22 + 4.E+0*
             P_q*k3_q*pow(k4_k5,2)*l1_l1*Ak3*fl17*fl22 + 4.E+0*P_q*k3_q*
             pow(k4_l1,2)*k5_k5*Ak3*fl17*fl22 + 8.E+0*P_q*k3_l1*k4_k4*k5_k5
             *q_l1*Ak3*fl17*fl22 - 4.E+0*P_q*k3_l1*k4_k4*k5_q*k5_l1*Ak3*
             fl17*fl22 + 8.E+0*P_q*k3_l1*k4_k5*k4_q*k5_l1*Ak3*fl17*fl22 -
             8.E+0*P_q*k3_l1*pow(k4_k5,2)*q_l1*Ak3*fl17*fl22 - 4.E+0*P_q*
             k3_l1*k4_q*k4_l1*k5_k5*Ak3*fl17*fl22 );

          Chi(10,5) +=  + F * (  - 4.E+0*P_k3*k4_k4*k5_k5*l1_l1*Ak3*fl17*
             fl22 + 4.E+0*P_k3*k4_k4*pow(k5_l1,2)*Ak3*fl17*fl22 - 8.E+0*
             P_k3*k4_k5*k4_l1*k5_l1*Ak3*fl17*fl22 + 4.E+0*P_k3*pow(k4_k5,2)
             *l1_l1*Ak3*fl17*fl22 + 4.E+0*P_k3*pow(k4_l1,2)*k5_k5*Ak3*fl17*
             fl22 + 8.E+0*P_k4*k3_k4*k5_k5*l1_l1*Ak3*fl17*fl22 - 8.E+0*P_k4
             *k3_k4*pow(k5_l1,2)*Ak3*fl17*fl22 - 4.E+0*P_k4*k3_k5*k4_k5*
             l1_l1*Ak3*fl17*fl22 + 8.E+0*P_k4*k3_k5*k4_l1*k5_l1*Ak3*fl17*
             fl22 + 8.E+0*P_k4*k3_l1*k4_k5*k5_l1*Ak3*fl17*fl22 - 4.E+0*P_k4
             *k3_l1*k4_l1*k5_k5*Ak3*fl17*fl22 - 4.E+0*P_k5*k3_k4*k4_k5*
             l1_l1*Ak3*fl17*fl22 + 8.E+0*P_k5*k3_k4*k4_l1*k5_l1*Ak3*fl17*
             fl22 - 8.E+0*P_k5*k3_k5*pow(k4_l1,2)*Ak3*fl17*fl22 - 4.E+0*
             P_k5*k3_l1*k4_k4*k5_l1*Ak3*fl17*fl22 + 8.E+0*P_l1*k3_k4*k4_k5*
             k5_l1*Ak3*fl17*fl22 - 4.E+0*P_l1*k3_k4*k4_l1*k5_k5*Ak3*fl17*
             fl22 - 4.E+0*P_l1*k3_k5*k4_k4*k5_l1*Ak3*fl17*fl22 + 8.E+0*P_l1
             *k3_l1*k4_k4*k5_k5*Ak3*fl17*fl22 - 8.E+0*P_l1*k3_l1*pow(
             k4_k5,2)*Ak3*fl17*fl22 );

          Chi(10,5) +=  + E * (  - 4.E+0*k4_k4*k5_k5*l1_l1*Bk3*fl17*fl22
              + 4.E+0*k4_k4*pow(k5_l1,2)*Bk3*fl17*fl22 + 4.E+0*pow(k4_k5,2)
             *l1_l1*Bk3*fl17*fl22 - 4.E+0*pow(k4_l1,2)*k5_k5*Bk3*fl17*fl22
              );

       Chi(10,6) =
           + H * (  - 4.666666666E+0*P_k3*k4_k4*k5_k5*q_l1*l1_l2*Ak3*fl17*
             fl23 - 3.333333333E+0*P_k3*k4_k4*k5_k5*q_l2*l1_l1*Ak3*fl17*
             fl23 - 3.333333333E+0*P_k3*k4_k4*k5_q*k5_l1*l1_l2*Ak3*fl17*
             fl23 + 3.333333333E+0*P_k3*k4_k4*k5_q*k5_l2*l1_l1*Ak3*fl17*
             fl23 + 4.666666666E+0*P_k3*k4_k4*k5_l1*k5_l2*q_l1*Ak3*fl17*
             fl23 + 3.333333333E+0*P_k3*k4_k4*pow(k5_l1,2)*q_l2*Ak3*fl17*
             fl23 - 4.666666666E+0*P_k3*k4_k5*k4_q*k5_l1*l1_l2*Ak3*fl17*
             fl23 + 4.666666666E+0*P_k3*k4_k5*k4_q*k5_l2*l1_l1*Ak3*fl17*
             fl23 + 3.333333333E+0*P_k3*k4_k5*k4_l1*k5_q*l1_l2*Ak3*fl17*
             fl23 + 1.333333333E+0*P_k3*k4_k5*k4_l1*k5_l1*q_l2*Ak3*fl17*
             fl23 - 1.266666666E+1*P_k3*k4_k5*k4_l1*k5_l2*q_l1*Ak3*fl17*
             fl23 - 3.333333333E+0*P_k3*k4_k5*k4_l2*k5_q*l1_l1*Ak3*fl17*
             fl23 - 4.666666666E+0*P_k3*k4_k5*k4_l2*k5_l1*q_l1*Ak3*fl17*
             fl23 + 4.666666666E+0*P_k3*pow(k4_k5,2)*q_l1*l1_l2*Ak3*fl17*
             fl23 + 3.333333333E+0*P_k3*pow(k4_k5,2)*q_l2*l1_l1*Ak3*fl17*
             fl23 + 4.666666666E+0*P_k3*k4_q*k4_l1*k5_k5*l1_l2*Ak3*fl17*
             fl23 - 4.666666666E+0*P_k3*k4_q*k4_l1*k5_l1*k5_l2*Ak3*fl17*
             fl23 - 4.666666666E+0*P_k3*k4_q*k4_l2*k5_k5*l1_l1*Ak3*fl17*
             fl23 + 4.666666666E+0*P_k3*k4_q*k4_l2*pow(k5_l1,2)*Ak3*fl17*
             fl23 + 1.266666666E+1*P_k3*k4_l1*k4_l2*k5_k5*q_l1*Ak3*fl17*
             fl23 - 4.666666666E+0*P_k3*k4_l1*k4_l2*k5_q*k5_l1*Ak3*fl17*
             fl23 - 4.666666666E+0*P_k3*pow(k4_l1,2)*k5_k5*q_l2*Ak3*fl17*
             fl23 + 4.666666666E+0*P_k3*pow(k4_l1,2)*k5_q*k5_l2*Ak3*fl17*
             fl23 + 1.266666666E+1*P_k4*k3_k4*k5_k5*q_l1*l1_l2*Ak3*fl17*
             fl23 + 3.333333333E+0*P_k4*k3_k4*k5_k5*q_l2*l1_l1*Ak3*fl17*
             fl23 + 3.333333333E+0*P_k4*k3_k4*k5_q*k5_l1*l1_l2*Ak3*fl17*
             fl23 - 3.333333333E+0*P_k4*k3_k4*k5_q*k5_l2*l1_l1*Ak3*fl17*
             fl23 - 1.266666666E+1*P_k4*k3_k4*k5_l1*k5_l2*q_l1*Ak3*fl17*
             fl23 - 3.333333333E+0*P_k4*k3_k4*pow(k5_l1,2)*q_l2*Ak3*fl17*
             fl23 - 4.666666666E+0*P_k4*k3_k5*k4_k5*q_l1*l1_l2*Ak3*fl17*
             fl23 - 3.333333333E+0*P_k4*k3_k5*k4_k5*q_l2*l1_l1*Ak3*fl17*
             fl23 - 1.333333333E+0*P_k4*k3_k5*k4_q*k5_l1*l1_l2*Ak3*fl17*
             fl23 + 1.333333333E+0*P_k4*k3_k5*k4_q*k5_l2*l1_l1*Ak3*fl17*
             fl23 - 2.E+0*P_k4*k3_k5*k4_l1*k5_q*l1_l2*Ak3*fl17*fl23 +
             3.333333333E+0*P_k4*k3_k5*k4_l1*k5_l1*q_l2*Ak3*fl17*fl23 +
             6.666666666E+0*P_k4*k3_k5*k4_l1*k5_l2*q_l1*Ak3*fl17*fl23 +
             2.E+0*P_k4*k3_k5*k4_l2*k5_q*l1_l1*Ak3*fl17*fl23 + 6.E+0*P_k4*
             k3_k5*k4_l2*k5_l1*q_l1*Ak3*fl17*fl23 + 6.E+0*P_k4*k3_q*k4_k5*
             k5_l1*l1_l2*Ak3*fl17*fl23 - 6.E+0*P_k4*k3_q*k4_k5*k5_l2*l1_l1*
             Ak3*fl17*fl23 - 6.E+0*P_k4*k3_q*k4_l1*k5_k5*l1_l2*Ak3*fl17*
             fl23 + 6.E+0*P_k4*k3_q*k4_l1*k5_l1*k5_l2*Ak3*fl17*fl23 + 6.E+0
             *P_k4*k3_q*k4_l2*k5_k5*l1_l1*Ak3*fl17*fl23 - 6.E+0*P_k4*k3_q*
             k4_l2*pow(k5_l1,2)*Ak3*fl17*fl23 - 9.333333333E+0*P_k4*k3_l1*
             k4_k5*k5_q*l1_l2*Ak3*fl17*fl23 + 3.333333333E+0*P_k4*k3_l1*
             k4_k5*k5_l1*q_l2*Ak3*fl17*fl23 + 1.4E+1*P_k4*k3_l1*k4_k5*k5_l2
             *q_l1*Ak3*fl17*fl23 + 1.333333333E+0*P_k4*k3_l1*k4_q*k5_k5*
             l1_l2*Ak3*fl17*fl23 - 1.333333333E+0*P_k4*k3_l1*k4_q*k5_l1*
             k5_l2*Ak3*fl17*fl23 + 4.666666666E+0*P_k4*k3_l1*k4_l1*k5_k5*
             q_l2*Ak3*fl17*fl23 - 4.666666666E+0*P_k4*k3_l1*k4_l1*k5_q*
             k5_l2*Ak3*fl17*fl23 - 1.4E+1*P_k4*k3_l1*k4_l2*k5_k5*q_l1*Ak3*
             fl17*fl23 );

          Chi(10,6) +=  + H * ( 6.E+0*P_k4*k3_l1*k4_l2*k5_q*k5_l1*Ak3*
             fl17*fl23 + 1.333333333E+0*P_k4*k3_l2*k4_k5*k5_q*l1_l1*Ak3*
             fl17*fl23 - 1.333333333E+0*P_k4*k3_l2*k4_k5*k5_l1*q_l1*Ak3*
             fl17*fl23 - 1.333333333E+0*P_k4*k3_l2*k4_q*k5_k5*l1_l1*Ak3*
             fl17*fl23 + 1.333333333E+0*P_k4*k3_l2*k4_q*pow(k5_l1,2)*Ak3*
             fl17*fl23 - 6.666666666E+0*P_k4*k3_l2*k4_l1*k5_k5*q_l1*Ak3*
             fl17*fl23 - 1.333333333E+0*P_k4*k3_l2*k4_l1*k5_q*k5_l1*Ak3*
             fl17*fl23 - 4.666666666E+0*P_k5*k3_k4*k4_k5*q_l1*l1_l2*Ak3*
             fl17*fl23 - 3.333333333E+0*P_k5*k3_k4*k4_k5*q_l2*l1_l1*Ak3*
             fl17*fl23 - 2.E+0*P_k5*k3_k4*k4_q*k5_l1*l1_l2*Ak3*fl17*fl23 +
             2.E+0*P_k5*k3_k4*k4_q*k5_l2*l1_l1*Ak3*fl17*fl23 -
             1.333333333E+0*P_k5*k3_k4*k4_l1*k5_q*l1_l2*Ak3*fl17*fl23 +
             3.333333333E+0*P_k5*k3_k4*k4_l1*k5_l1*q_l2*Ak3*fl17*fl23 +
             6.E+0*P_k5*k3_k4*k4_l1*k5_l2*q_l1*Ak3*fl17*fl23 +
             1.333333333E+0*P_k5*k3_k4*k4_l2*k5_q*l1_l1*Ak3*fl17*fl23 +
             6.666666666E+0*P_k5*k3_k4*k4_l2*k5_l1*q_l1*Ak3*fl17*fl23 -
             3.333333333E+0*P_k5*k3_k5*k4_k4*q_l1*l1_l2*Ak3*fl17*fl23 +
             3.333333333E+0*P_k5*k3_k5*k4_k4*q_l2*l1_l1*Ak3*fl17*fl23 +
             3.333333333E+0*P_k5*k3_k5*k4_q*k4_l1*l1_l2*Ak3*fl17*fl23 -
             3.333333333E+0*P_k5*k3_k5*k4_q*k4_l2*l1_l1*Ak3*fl17*fl23 -
             1.266666666E+1*P_k5*k3_k5*k4_l1*k4_l2*q_l1*Ak3*fl17*fl23 -
             3.333333333E+0*P_k5*k3_k5*pow(k4_l1,2)*q_l2*Ak3*fl17*fl23 +
             2.E+0*P_k5*k3_q*k4_k4*k5_l1*l1_l2*Ak3*fl17*fl23 - 2.E+0*P_k5*
             k3_q*k4_k4*k5_l2*l1_l1*Ak3*fl17*fl23 - 2.E+0*P_k5*k3_q*k4_k5*
             k4_l1*l1_l2*Ak3*fl17*fl23 + 2.E+0*P_k5*k3_q*k4_k5*k4_l2*l1_l1*
             Ak3*fl17*fl23 + 6.E+0*P_k5*k3_q*k4_l1*k4_l2*k5_l1*Ak3*fl17*
             fl23 - 6.E+0*P_k5*k3_q*pow(k4_l1,2)*k5_l2*Ak3*fl17*fl23 +
             1.333333333E+0*P_k5*k3_l1*k4_k4*k5_q*l1_l2*Ak3*fl17*fl23 -
             3.333333333E+0*P_k5*k3_l1*k4_k4*k5_l1*q_l2*Ak3*fl17*fl23 +
             2.E+0*P_k5*k3_l1*k4_k4*k5_l2*q_l1*Ak3*fl17*fl23 +
             6.666666666E+0*P_k5*k3_l1*k4_k5*k4_q*l1_l2*Ak3*fl17*fl23 -
             4.666666666E+0*P_k5*k3_l1*k4_k5*k4_l1*q_l2*Ak3*fl17*fl23 -
             2.E+0*P_k5*k3_l1*k4_k5*k4_l2*q_l1*Ak3*fl17*fl23 + 6.E+0*P_k5*
             k3_l1*k4_q*k4_l1*k5_l2*Ak3*fl17*fl23 - 4.666666666E+0*P_k5*
             k3_l1*k4_q*k4_l2*k5_l1*Ak3*fl17*fl23 - 1.333333333E+0*P_k5*
             k3_l1*k4_l1*k4_l2*k5_q*Ak3*fl17*fl23 - 1.333333333E+0*P_k5*
             k3_l2*k4_k4*k5_q*l1_l1*Ak3*fl17*fl23 + 1.333333333E+0*P_k5*
             k3_l2*k4_k4*k5_l1*q_l1*Ak3*fl17*fl23 + 1.333333333E+0*P_k5*
             k3_l2*k4_k5*k4_q*l1_l1*Ak3*fl17*fl23 + 6.666666666E+0*P_k5*
             k3_l2*k4_k5*k4_l1*q_l1*Ak3*fl17*fl23 - 1.333333333E+0*P_k5*
             k3_l2*k4_q*k4_l1*k5_l1*Ak3*fl17*fl23 + 1.333333333E+0*P_k5*
             k3_l2*pow(k4_l1,2)*k5_q*Ak3*fl17*fl23 - 1.333333333E+0*P_q*
             k3_k4*k4_k5*k5_l1*l1_l2*Ak3*fl17*fl23 + 1.333333333E+0*P_q*
             k3_k4*k4_k5*k5_l2*l1_l1*Ak3*fl17*fl23 + 1.333333333E+0*P_q*
             k3_k4*k4_l1*k5_k5*l1_l2*Ak3*fl17*fl23 - 1.333333333E+0*P_q*
             k3_k4*k4_l1*k5_l1*k5_l2*Ak3*fl17*fl23 - 1.333333333E+0*P_q*
             k3_k4*k4_l2*k5_k5*l1_l1*Ak3*fl17*fl23 + 1.333333333E+0*P_q*
             k3_k4*k4_l2*pow(k5_l1,2)*Ak3*fl17*fl23 + 1.333333333E+0*P_q*
             k3_k5*k4_k4*k5_l1*l1_l2*Ak3*fl17*fl23 - 1.333333333E+0*P_q*
             k3_k5*k4_k4*k5_l2*l1_l1*Ak3*fl17*fl23 - 1.333333333E+0*P_q*
             k3_k5*k4_k5*k4_l1*l1_l2*Ak3*fl17*fl23 );

          Chi(10,6) +=  + H * ( 1.333333333E+0*P_q*k3_k5*k4_k5*k4_l2*
             l1_l1*Ak3*fl17*fl23 - 1.333333333E+0*P_q*k3_k5*k4_l1*k4_l2*
             k5_l1*Ak3*fl17*fl23 + 1.333333333E+0*P_q*k3_k5*pow(k4_l1,2)*
             k5_l2*Ak3*fl17*fl23 - 1.333333333E+0*P_q*k3_l1*k4_k4*k5_k5*
             l1_l2*Ak3*fl17*fl23 + 1.333333333E+0*P_q*k3_l1*k4_k4*k5_l1*
             k5_l2*Ak3*fl17*fl23 - 1.333333333E+0*P_q*k3_l1*k4_k5*k4_l1*
             k5_l2*Ak3*fl17*fl23 - 1.333333333E+0*P_q*k3_l1*k4_k5*k4_l2*
             k5_l1*Ak3*fl17*fl23 + 1.333333333E+0*P_q*k3_l1*pow(k4_k5,2)*
             l1_l2*Ak3*fl17*fl23 + 1.333333333E+0*P_q*k3_l1*k4_l1*k4_l2*
             k5_k5*Ak3*fl17*fl23 + 1.333333333E+0*P_q*k3_l2*k4_k4*k5_k5*
             l1_l1*Ak3*fl17*fl23 - 1.333333333E+0*P_q*k3_l2*k4_k4*pow(
             k5_l1,2)*Ak3*fl17*fl23 + 2.666666666E+0*P_q*k3_l2*k4_k5*k4_l1*
             k5_l1*Ak3*fl17*fl23 - 1.333333333E+0*P_q*k3_l2*pow(k4_k5,2)*
             l1_l1*Ak3*fl17*fl23 - 1.333333333E+0*P_q*k3_l2*pow(k4_l1,2)*
             k5_k5*Ak3*fl17*fl23 + 6.E+0*P_l1*k3_k4*k4_k5*k5_q*l1_l2*Ak3*
             fl17*fl23 + 3.333333333E+0*P_l1*k3_k4*k4_k5*k5_l1*q_l2*Ak3*
             fl17*fl23 - 1.333333333E+0*P_l1*k3_k4*k4_k5*k5_l2*q_l1*Ak3*
             fl17*fl23 - 1.4E+1*P_l1*k3_k4*k4_q*k5_k5*l1_l2*Ak3*fl17*fl23
              + 1.4E+1*P_l1*k3_k4*k4_q*k5_l1*k5_l2*Ak3*fl17*fl23 +
             4.666666666E+0*P_l1*k3_k4*k4_l1*k5_k5*q_l2*Ak3*fl17*fl23 -
             4.666666666E+0*P_l1*k3_k4*k4_l1*k5_q*k5_l2*Ak3*fl17*fl23 +
             1.333333333E+0*P_l1*k3_k4*k4_l2*k5_k5*q_l1*Ak3*fl17*fl23 -
             9.333333333E+0*P_l1*k3_k4*k4_l2*k5_q*k5_l1*Ak3*fl17*fl23 +
             2.E+0*P_l1*k3_k5*k4_k4*k5_q*l1_l2*Ak3*fl17*fl23 -
             3.333333333E+0*P_l1*k3_k5*k4_k4*k5_l1*q_l2*Ak3*fl17*fl23 +
             1.333333333E+0*P_l1*k3_k5*k4_k4*k5_l2*q_l1*Ak3*fl17*fl23 +
             6.E+0*P_l1*k3_k5*k4_k5*k4_q*l1_l2*Ak3*fl17*fl23 -
             4.666666666E+0*P_l1*k3_k5*k4_k5*k4_l1*q_l2*Ak3*fl17*fl23 -
             1.333333333E+0*P_l1*k3_k5*k4_k5*k4_l2*q_l1*Ak3*fl17*fl23 -
             9.333333333E+0*P_l1*k3_k5*k4_q*k4_l1*k5_l2*Ak3*fl17*fl23 -
             4.666666666E+0*P_l1*k3_k5*k4_q*k4_l2*k5_l1*Ak3*fl17*fl23 +
             1.4E+1*P_l1*k3_k5*k4_l1*k4_l2*k5_q*Ak3*fl17*fl23 + 6.E+0*P_l1*
             k3_q*k4_k4*k5_k5*l1_l2*Ak3*fl17*fl23 - 6.E+0*P_l1*k3_q*k4_k4*
             k5_l1*k5_l2*Ak3*fl17*fl23 + 1.4E+1*P_l1*k3_q*k4_k5*k4_l1*k5_l2
             *Ak3*fl17*fl23 + 6.E+0*P_l1*k3_q*k4_k5*k4_l2*k5_l1*Ak3*fl17*
             fl23 - 6.E+0*P_l1*k3_q*pow(k4_k5,2)*l1_l2*Ak3*fl17*fl23 -
             1.4E+1*P_l1*k3_q*k4_l1*k4_l2*k5_k5*Ak3*fl17*fl23 +
             3.333333333E+0*P_l1*k3_l1*k4_k4*k5_k5*q_l2*Ak3*fl17*fl23 -
             3.333333333E+0*P_l1*k3_l1*k4_k4*k5_q*k5_l2*Ak3*fl17*fl23 -
             1.266666666E+1*P_l1*k3_l1*k4_k5*k4_q*k5_l2*Ak3*fl17*fl23 +
             3.333333333E+0*P_l1*k3_l1*k4_k5*k4_l2*k5_q*Ak3*fl17*fl23 -
             3.333333333E+0*P_l1*k3_l1*pow(k4_k5,2)*q_l2*Ak3*fl17*fl23 +
             1.266666666E+1*P_l1*k3_l1*k4_q*k4_l2*k5_k5*Ak3*fl17*fl23 -
             1.333333333E+0*P_l1*k3_l2*k4_k4*k5_k5*q_l1*Ak3*fl17*fl23 +
             1.333333333E+0*P_l1*k3_l2*k4_k4*k5_q*k5_l1*Ak3*fl17*fl23 -
             1.333333333E+0*P_l1*k3_l2*k4_k5*k4_q*k5_l1*Ak3*fl17*fl23 -
             9.333333333E+0*P_l1*k3_l2*k4_k5*k4_l1*k5_q*Ak3*fl17*fl23 +
             1.333333333E+0*P_l1*k3_l2*pow(k4_k5,2)*q_l1*Ak3*fl17*fl23 +
             9.333333333E+0*P_l1*k3_l2*k4_q*k4_l1*k5_k5*Ak3*fl17*fl23 +
             2.E+0*P_l2*k3_k4*k4_k5*k5_q*l1_l1*Ak3*fl17*fl23 - 2.E+0*P_l2*
             k3_k4*k4_k5*k5_l1*q_l1*Ak3*fl17*fl23 );

          Chi(10,6) +=  + H * (  - 2.E+0*P_l2*k3_k4*k4_q*k5_k5*l1_l1*Ak3*
             fl17*fl23 + 2.E+0*P_l2*k3_k4*k4_q*pow(k5_l1,2)*Ak3*fl17*fl23
              - 6.E+0*P_l2*k3_k4*k4_l1*k5_k5*q_l1*Ak3*fl17*fl23 - 2.E+0*
             P_l2*k3_k4*k4_l1*k5_q*k5_l1*Ak3*fl17*fl23 - 2.E+0*P_l2*k3_k5*
             k4_k4*k5_q*l1_l1*Ak3*fl17*fl23 + 2.E+0*P_l2*k3_k5*k4_k4*k5_l1*
             q_l1*Ak3*fl17*fl23 + 2.E+0*P_l2*k3_k5*k4_k5*k4_q*l1_l1*Ak3*
             fl17*fl23 + 6.E+0*P_l2*k3_k5*k4_k5*k4_l1*q_l1*Ak3*fl17*fl23 -
             2.E+0*P_l2*k3_k5*k4_q*k4_l1*k5_l1*Ak3*fl17*fl23 + 2.E+0*P_l2*
             k3_k5*pow(k4_l1,2)*k5_q*Ak3*fl17*fl23 + 2.E+0*P_l2*k3_q*k4_k4*
             k5_k5*l1_l1*Ak3*fl17*fl23 - 2.E+0*P_l2*k3_q*k4_k4*pow(k5_l1,2)
             *Ak3*fl17*fl23 - 4.E+0*P_l2*k3_q*k4_k5*k4_l1*k5_l1*Ak3*fl17*
             fl23 - 2.E+0*P_l2*k3_q*pow(k4_k5,2)*l1_l1*Ak3*fl17*fl23 +
             6.E+0*P_l2*k3_q*pow(k4_l1,2)*k5_k5*Ak3*fl17*fl23 - 2.E+0*P_l2*
             k3_l1*k4_k4*k5_k5*q_l1*Ak3*fl17*fl23 + 2.E+0*P_l2*k3_l1*k4_k4*
             k5_q*k5_l1*Ak3*fl17*fl23 - 2.E+0*P_l2*k3_l1*k4_k5*k4_q*k5_l1*
             Ak3*fl17*fl23 + 6.E+0*P_l2*k3_l1*k4_k5*k4_l1*k5_q*Ak3*fl17*
             fl23 + 2.E+0*P_l2*k3_l1*pow(k4_k5,2)*q_l1*Ak3*fl17*fl23 -
             6.E+0*P_l2*k3_l1*k4_q*k4_l1*k5_k5*Ak3*fl17*fl23 );

          Chi(10,6) +=  + G * (  - 4.E+0*P_q*k4_k4*k5_k5*q_l1*l1_l2*Bk3*
             fl17*fl23 + 4.E+0*P_q*k4_k4*k5_l1*k5_l2*q_l1*Bk3*fl17*fl23 -
             4.E+0*P_q*k4_k5*k4_q*k5_l1*l1_l2*Bk3*fl17*fl23 + 4.E+0*P_q*
             k4_k5*k4_q*k5_l2*l1_l1*Bk3*fl17*fl23 + 4.E+0*P_q*k4_k5*k4_l1*
             k5_l1*q_l2*Bk3*fl17*fl23 - 4.E+0*P_q*k4_k5*k4_l2*k5_l1*q_l1*
             Bk3*fl17*fl23 + 4.E+0*P_q*pow(k4_k5,2)*q_l1*l1_l2*Bk3*fl17*
             fl23 + 4.E+0*P_q*k4_q*k4_l1*k5_k5*l1_l2*Bk3*fl17*fl23 - 4.E+0*
             P_q*k4_q*k4_l1*k5_l1*k5_l2*Bk3*fl17*fl23 - 4.E+0*P_q*k4_q*
             k4_l2*k5_k5*l1_l1*Bk3*fl17*fl23 + 4.E+0*P_q*k4_q*k4_l2*pow(
             k5_l1,2)*Bk3*fl17*fl23 - 4.E+0*P_q*k4_l1*k4_l2*k5_q*k5_l1*Bk3*
             fl17*fl23 - 4.E+0*P_q*pow(k4_l1,2)*k5_k5*q_l2*Bk3*fl17*fl23 +
             4.E+0*P_q*pow(k4_l1,2)*k5_q*k5_l2*Bk3*fl17*fl23 );

          Chi(10,6) +=  + F * (  - 4.E+0*P_k4*k4_k5*k5_l1*l1_l2*Bk3*fl17*
             fl23 + 4.E+0*P_k4*k4_k5*k5_l2*l1_l1*Bk3*fl17*fl23 + 4.E+0*P_k4
             *k4_l1*k5_k5*l1_l2*Bk3*fl17*fl23 - 4.E+0*P_k4*k4_l1*k5_l1*
             k5_l2*Bk3*fl17*fl23 - 4.E+0*P_k4*k4_l2*k5_k5*l1_l1*Bk3*fl17*
             fl23 + 4.E+0*P_k4*k4_l2*pow(k5_l1,2)*Bk3*fl17*fl23 - 4.E+0*
             P_k5*k4_l1*k4_l2*k5_l1*Bk3*fl17*fl23 + 4.E+0*P_k5*pow(k4_l1,2)
             *k5_l2*Bk3*fl17*fl23 - 4.E+0*P_l1*k4_k4*k5_k5*l1_l2*Bk3*fl17*
             fl23 + 4.E+0*P_l1*k4_k4*k5_l1*k5_l2*Bk3*fl17*fl23 - 4.E+0*P_l1
             *k4_k5*k4_l2*k5_l1*Bk3*fl17*fl23 + 4.E+0*P_l1*pow(k4_k5,2)*
             l1_l2*Bk3*fl17*fl23 + 4.E+0*P_l2*k4_k5*k4_l1*k5_l1*Bk3*fl17*
             fl23 - 4.E+0*P_l2*pow(k4_l1,2)*k5_k5*Bk3*fl17*fl23 );

          Chi(10,6) +=  + E * (  - 4.E+0*k3_k4*k4_k5*k5_l1*l1_l2*Ak3*fl17
             *fl23 + 4.E+0*k3_k4*k4_k5*k5_l2*l1_l1*Ak3*fl17*fl23 + 4.E+0*
             k3_k4*k4_l1*k5_k5*l1_l2*Ak3*fl17*fl23 - 4.E+0*k3_k4*k4_l1*
             k5_l1*k5_l2*Ak3*fl17*fl23 - 4.E+0*k3_k4*k4_l2*k5_k5*l1_l1*Ak3*
             fl17*fl23 + 4.E+0*k3_k4*k4_l2*pow(k5_l1,2)*Ak3*fl17*fl23 -
             4.E+0*k3_k5*k4_l1*k4_l2*k5_l1*Ak3*fl17*fl23 + 4.E+0*k3_k5*pow(
             k4_l1,2)*k5_l2*Ak3*fl17*fl23 - 4.E+0*k3_l1*k4_k4*k5_k5*l1_l2*
             Ak3*fl17*fl23 + 4.E+0*k3_l1*k4_k4*k5_l1*k5_l2*Ak3*fl17*fl23 -
             4.E+0*k3_l1*k4_k5*k4_l2*k5_l1*Ak3*fl17*fl23 + 4.E+0*k3_l1*pow(
             k4_k5,2)*l1_l2*Ak3*fl17*fl23 + 4.E+0*k3_l2*k4_k5*k4_l1*k5_l1*
             Ak3*fl17*fl23 - 4.E+0*k3_l2*pow(k4_l1,2)*k5_k5*Ak3*fl17*fl23 )
             ;

       Chi(10,7) =
           + H * ( 8.E+0*P_k4*k4_k5*k5_k5*q_l1*l1_l2*Bk3*fl17*fl24 - 8.E+0*
             P_k4*k4_k5*k5_k5*q_l2*l1_l1*Bk3*fl17*fl24 + 8.E+0*P_k4*k4_l2*
             k5_k5*k5_q*l1_l1*Bk3*fl17*fl24 - 8.E+0*P_k4*k4_l2*k5_k5*k5_l1*
             q_l1*Bk3*fl17*fl24 - 8.E+0*P_k5*k4_k4*k5_k5*q_l1*l1_l2*Bk3*
             fl17*fl24 + 8.E+0*P_k5*k4_k4*k5_k5*q_l2*l1_l1*Bk3*fl17*fl24 -
             8.E+0*P_k5*k4_q*k4_l2*k5_k5*l1_l1*Bk3*fl17*fl24 + 8.E+0*P_k5*
             k4_l1*k4_l2*k5_k5*q_l1*Bk3*fl17*fl24 + 8.E+0*P_l1*k4_k4*k5_k5*
             k5_q*l1_l2*Bk3*fl17*fl24 - 8.E+0*P_l1*k4_k4*k5_k5*k5_l1*q_l2*
             Bk3*fl17*fl24 - 8.E+0*P_l1*k4_k5*k4_q*k5_k5*l1_l2*Bk3*fl17*
             fl24 + 8.E+0*P_l1*k4_k5*k4_l1*k5_k5*q_l2*Bk3*fl17*fl24 + 8.E+0
             *P_l1*k4_q*k4_l2*k5_k5*k5_l1*Bk3*fl17*fl24 - 8.E+0*P_l1*k4_l1*
             k4_l2*k5_k5*k5_q*Bk3*fl17*fl24 - 8.E+0*P_l2*k4_k4*k5_k5*k5_q*
             l1_l1*Bk3*fl17*fl24 + 8.E+0*P_l2*k4_k4*k5_k5*k5_l1*q_l1*Bk3*
             fl17*fl24 + 8.E+0*P_l2*k4_k5*k4_q*k5_k5*l1_l1*Bk3*fl17*fl24 -
             8.E+0*P_l2*k4_k5*k4_l1*k5_k5*q_l1*Bk3*fl17*fl24 );

          Chi(10,7) +=  + G * (  - 4.E+0*P_q*k3_k4*k4_k5*k5_k5*q_l1*l1_l2
             *Ak3*fl17*fl24 + 4.E+0*P_q*k3_k4*k4_k5*k5_k5*q_l2*l1_l1*Ak3*
             fl17*fl24 - 4.E+0*P_q*k3_k4*k4_l2*k5_k5*k5_q*l1_l1*Ak3*fl17*
             fl24 + 4.E+0*P_q*k3_k4*k4_l2*k5_k5*k5_l1*q_l1*Ak3*fl17*fl24 +
             4.E+0*P_q*k3_k5*k4_k4*k5_k5*q_l1*l1_l2*Ak3*fl17*fl24 - 4.E+0*
             P_q*k3_k5*k4_k4*k5_k5*q_l2*l1_l1*Ak3*fl17*fl24 + 4.E+0*P_q*
             k3_k5*k4_q*k4_l2*k5_k5*l1_l1*Ak3*fl17*fl24 - 4.E+0*P_q*k3_k5*
             k4_l1*k4_l2*k5_k5*q_l1*Ak3*fl17*fl24 - 4.E+0*P_q*k3_l1*k4_k4*
             k5_k5*k5_q*l1_l2*Ak3*fl17*fl24 + 4.E+0*P_q*k3_l1*k4_k4*k5_k5*
             k5_l1*q_l2*Ak3*fl17*fl24 + 4.E+0*P_q*k3_l1*k4_k5*k4_q*k5_k5*
             l1_l2*Ak3*fl17*fl24 - 4.E+0*P_q*k3_l1*k4_k5*k4_l1*k5_k5*q_l2*
             Ak3*fl17*fl24 - 4.E+0*P_q*k3_l1*k4_q*k4_l2*k5_k5*k5_l1*Ak3*
             fl17*fl24 + 4.E+0*P_q*k3_l1*k4_l1*k4_l2*k5_k5*k5_q*Ak3*fl17*
             fl24 + 4.E+0*P_q*k3_l2*k4_k4*k5_k5*k5_q*l1_l1*Ak3*fl17*fl24 -
             4.E+0*P_q*k3_l2*k4_k4*k5_k5*k5_l1*q_l1*Ak3*fl17*fl24 - 4.E+0*
             P_q*k3_l2*k4_k5*k4_q*k5_k5*l1_l1*Ak3*fl17*fl24 + 4.E+0*P_q*
             k3_l2*k4_k5*k4_l1*k5_k5*q_l1*Ak3*fl17*fl24 );

          Chi(10,7) +=  + F * ( 4.E+0*P_k4*k3_k5*k4_l2*k5_k5*l1_l1*Ak3*
             fl17*fl24 + 4.E+0*P_k4*k3_l1*k4_k5*k5_k5*l1_l2*Ak3*fl17*fl24
              - 4.E+0*P_k4*k3_l1*k4_l2*k5_k5*k5_l1*Ak3*fl17*fl24 - 4.E+0*
             P_k4*k3_l2*k4_k5*k5_k5*l1_l1*Ak3*fl17*fl24 - 4.E+0*P_k5*k3_k4*
             k4_l2*k5_k5*l1_l1*Ak3*fl17*fl24 - 4.E+0*P_k5*k3_l1*k4_k4*k5_k5
             *l1_l2*Ak3*fl17*fl24 + 4.E+0*P_k5*k3_l1*k4_l1*k4_l2*k5_k5*Ak3*
             fl17*fl24 + 4.E+0*P_k5*k3_l2*k4_k4*k5_k5*l1_l1*Ak3*fl17*fl24
              - 4.E+0*P_l1*k3_k4*k4_k5*k5_k5*l1_l2*Ak3*fl17*fl24 + 4.E+0*
             P_l1*k3_k4*k4_l2*k5_k5*k5_l1*Ak3*fl17*fl24 + 4.E+0*P_l1*k3_k5*
             k4_k4*k5_k5*l1_l2*Ak3*fl17*fl24 - 4.E+0*P_l1*k3_k5*k4_l1*k4_l2
             *k5_k5*Ak3*fl17*fl24 - 4.E+0*P_l1*k3_l2*k4_k4*k5_k5*k5_l1*Ak3*
             fl17*fl24 + 4.E+0*P_l1*k3_l2*k4_k5*k4_l1*k5_k5*Ak3*fl17*fl24
              + 4.E+0*P_l2*k3_k4*k4_k5*k5_k5*l1_l1*Ak3*fl17*fl24 - 4.E+0*
             P_l2*k3_k5*k4_k4*k5_k5*l1_l1*Ak3*fl17*fl24 + 4.E+0*P_l2*k3_l1*
             k4_k4*k5_k5*k5_l1*Ak3*fl17*fl24 - 4.E+0*P_l2*k3_l1*k4_k5*k4_l1
             *k5_k5*Ak3*fl17*fl24 );

       Chi(10,8) =
           + H * ( 8.E+0*P_k3*k4_q*k4_l1*k5_k5*k5_l1*l2_l2*Ak3*fl17*fl25 -
             8.E+0*P_k3*k4_q*k4_l1*k5_k5*k5_l2*l1_l2*Ak3*fl17*fl25 + 8.E+0*
             P_k3*k4_l1*k4_l2*k5_k5*k5_q*l1_l2*Ak3*fl17*fl25 - 8.E+0*P_k3*
             k4_l1*k4_l2*k5_k5*k5_l1*q_l2*Ak3*fl17*fl25 - 8.E+0*P_k3*pow(
             k4_l1,2)*k5_k5*k5_q*l2_l2*Ak3*fl17*fl25 + 8.E+0*P_k3*pow(
             k4_l1,2)*k5_k5*k5_l2*q_l2*Ak3*fl17*fl25 - 8.E+0*P_k4*k3_k4*
             k5_k5*k5_q*l1_l1*l2_l2*Ak3*fl17*fl25 + 8.E+0*P_k4*k3_k4*k5_k5*
             k5_q*pow(l1_l2,2)*Ak3*fl17*fl25 + 8.E+0*P_k4*k3_k4*k5_k5*k5_l1
             *q_l1*l2_l2*Ak3*fl17*fl25 - 8.E+0*P_k4*k3_k4*k5_k5*k5_l1*q_l2*
             l1_l2*Ak3*fl17*fl25 - 8.E+0*P_k4*k3_k4*k5_k5*k5_l2*q_l1*l1_l2*
             Ak3*fl17*fl25 + 8.E+0*P_k4*k3_k4*k5_k5*k5_l2*q_l2*l1_l1*Ak3*
             fl17*fl25 + 2.E+0*P_k4*k3_k5*k4_q*k5_k5*l1_l1*l2_l2*Ak3*fl17*
             fl25 - 2.E+0*P_k4*k3_k5*k4_q*k5_k5*pow(l1_l2,2)*Ak3*fl17*fl25
              - 2.E+0*P_k4*k3_k5*k4_l1*k5_k5*q_l1*l2_l2*Ak3*fl17*fl25 +
             2.E+0*P_k4*k3_k5*k4_l1*k5_k5*q_l2*l1_l2*Ak3*fl17*fl25 + 2.E+0*
             P_k4*k3_k5*k4_l2*k5_k5*q_l1*l1_l2*Ak3*fl17*fl25 - 1.E+1*P_k4*
             k3_k5*k4_l2*k5_k5*q_l2*l1_l1*Ak3*fl17*fl25 - 2.E+0*P_k4*k3_q*
             k4_k5*k5_k5*l1_l1*l2_l2*Ak3*fl17*fl25 + 2.E+0*P_k4*k3_q*k4_k5*
             k5_k5*pow(l1_l2,2)*Ak3*fl17*fl25 - 6.E+0*P_k4*k3_q*k4_l1*k5_k5
             *k5_l1*l2_l2*Ak3*fl17*fl25 + 6.E+0*P_k4*k3_q*k4_l1*k5_k5*k5_l2
             *l1_l2*Ak3*fl17*fl25 - 2.E+0*P_k4*k3_q*k4_l2*k5_k5*k5_l1*l1_l2
             *Ak3*fl17*fl25 + 2.E+0*P_k4*k3_q*k4_l2*k5_k5*k5_l2*l1_l1*Ak3*
             fl17*fl25 - 6.E+0*P_k4*k3_l1*k4_k5*k5_k5*q_l1*l2_l2*Ak3*fl17*
             fl25 - 2.E+0*P_k4*k3_l1*k4_k5*k5_k5*q_l2*l1_l2*Ak3*fl17*fl25
              - 2.E+0*P_k4*k3_l1*k4_q*k5_k5*k5_l1*l2_l2*Ak3*fl17*fl25 +
             2.E+0*P_k4*k3_l1*k4_q*k5_k5*k5_l2*l1_l2*Ak3*fl17*fl25 + 8.E+0*
             P_k4*k3_l1*k4_l1*k5_k5*k5_q*l2_l2*Ak3*fl17*fl25 - 8.E+0*P_k4*
             k3_l1*k4_l1*k5_k5*k5_l2*q_l2*Ak3*fl17*fl25 - 8.E+0*P_k4*k3_l1*
             k4_l2*k5_k5*k5_q*l1_l2*Ak3*fl17*fl25 + 1.E+1*P_k4*k3_l1*k4_l2*
             k5_k5*k5_l1*q_l2*Ak3*fl17*fl25 + 6.E+0*P_k4*k3_l1*k4_l2*k5_k5*
             k5_l2*q_l1*Ak3*fl17*fl25 - 2.E+0*P_k4*k3_l2*k4_k5*k5_k5*q_l1*
             l1_l2*Ak3*fl17*fl25 + 1.E+1*P_k4*k3_l2*k4_k5*k5_k5*q_l2*l1_l1*
             Ak3*fl17*fl25 + 2.E+0*P_k4*k3_l2*k4_q*k5_k5*k5_l1*l1_l2*Ak3*
             fl17*fl25 - 2.E+0*P_k4*k3_l2*k4_q*k5_k5*k5_l2*l1_l1*Ak3*fl17*
             fl25 - 8.E+0*P_k4*k3_l2*k4_l1*k5_k5*k5_q*l1_l2*Ak3*fl17*fl25
              + 6.E+0*P_k4*k3_l2*k4_l1*k5_k5*k5_l1*q_l2*Ak3*fl17*fl25 +
             2.E+0*P_k4*k3_l2*k4_l1*k5_k5*k5_l2*q_l1*Ak3*fl17*fl25 + 6.E+0*
             P_k5*k3_k4*k4_q*k5_k5*l1_l1*l2_l2*Ak3*fl17*fl25 - 6.E+0*P_k5*
             k3_k4*k4_q*k5_k5*pow(l1_l2,2)*Ak3*fl17*fl25 - 6.E+0*P_k5*k3_k4
             *k4_l1*k5_k5*q_l1*l2_l2*Ak3*fl17*fl25 + 6.E+0*P_k5*k3_k4*k4_l1
             *k5_k5*q_l2*l1_l2*Ak3*fl17*fl25 + 6.E+0*P_k5*k3_k4*k4_l2*k5_k5
             *q_l1*l1_l2*Ak3*fl17*fl25 + 2.E+0*P_k5*k3_k4*k4_l2*k5_k5*q_l2*
             l1_l1*Ak3*fl17*fl25 + 2.E+0*P_k5*k3_q*k4_k4*k5_k5*l1_l1*l2_l2*
             Ak3*fl17*fl25 - 2.E+0*P_k5*k3_q*k4_k4*k5_k5*pow(l1_l2,2)*Ak3*
             fl17*fl25 - 4.E+0*P_k5*k3_q*k4_l1*k4_l2*k5_k5*l1_l2*Ak3*fl17*
             fl25 + 6.E+0*P_k5*k3_q*pow(k4_l1,2)*k5_k5*l2_l2*Ak3*fl17*fl25
              - 2.E+0*P_k5*k3_q*pow(k4_l2,2)*k5_k5*l1_l1*Ak3*fl17*fl25 +
             6.E+0*P_k5*k3_l1*k4_k4*k5_k5*q_l1*l2_l2*Ak3*fl17*fl25 + 2.E+0*
             P_k5*k3_l1*k4_k4*k5_k5*q_l2*l1_l2*Ak3*fl17*fl25 - 6.E+0*P_k5*
             k3_l1*k4_q*k4_l1*k5_k5*l2_l2*Ak3*fl17*fl25 + 6.E+0*P_k5*k3_l1*
             k4_q*k4_l2*k5_k5*l1_l2*Ak3*fl17*fl25 );

          Chi(10,8) +=  + H * (  - 2.E+0*P_k5*k3_l1*k4_l1*k4_l2*k5_k5*
             q_l2*Ak3*fl17*fl25 - 6.E+0*P_k5*k3_l1*pow(k4_l2,2)*k5_k5*q_l1*
             Ak3*fl17*fl25 + 2.E+0*P_k5*k3_l2*k4_k4*k5_k5*q_l1*l1_l2*Ak3*
             fl17*fl25 - 1.E+1*P_k5*k3_l2*k4_k4*k5_k5*q_l2*l1_l1*Ak3*fl17*
             fl25 + 6.E+0*P_k5*k3_l2*k4_q*k4_l1*k5_k5*l1_l2*Ak3*fl17*fl25
              + 2.E+0*P_k5*k3_l2*k4_q*k4_l2*k5_k5*l1_l1*Ak3*fl17*fl25 -
             2.E+0*P_k5*k3_l2*k4_l1*k4_l2*k5_k5*q_l1*Ak3*fl17*fl25 - 6.E+0*
             P_k5*k3_l2*pow(k4_l1,2)*k5_k5*q_l2*Ak3*fl17*fl25 + 2.E+0*P_q*
             k3_k4*k4_k5*k5_k5*l1_l1*l2_l2*Ak3*fl17*fl25 - 2.E+0*P_q*k3_k4*
             k4_k5*k5_k5*pow(l1_l2,2)*Ak3*fl17*fl25 - 2.E+0*P_q*k3_k4*k4_l1
             *k5_k5*k5_l1*l2_l2*Ak3*fl17*fl25 + 2.E+0*P_q*k3_k4*k4_l1*k5_k5
             *k5_l2*l1_l2*Ak3*fl17*fl25 + 2.E+0*P_q*k3_k4*k4_l2*k5_k5*k5_l1
             *l1_l2*Ak3*fl17*fl25 - 2.E+0*P_q*k3_k4*k4_l2*k5_k5*k5_l2*l1_l1
             *Ak3*fl17*fl25 - 2.E+0*P_q*k3_k5*k4_k4*k5_k5*l1_l1*l2_l2*Ak3*
             fl17*fl25 + 2.E+0*P_q*k3_k5*k4_k4*k5_k5*pow(l1_l2,2)*Ak3*fl17*
             fl25 - 4.E+0*P_q*k3_k5*k4_l1*k4_l2*k5_k5*l1_l2*Ak3*fl17*fl25
              + 2.E+0*P_q*k3_k5*pow(k4_l1,2)*k5_k5*l2_l2*Ak3*fl17*fl25 +
             2.E+0*P_q*k3_k5*pow(k4_l2,2)*k5_k5*l1_l1*Ak3*fl17*fl25 + 2.E+0
             *P_q*k3_l1*k4_k4*k5_k5*k5_l1*l2_l2*Ak3*fl17*fl25 - 2.E+0*P_q*
             k3_l1*k4_k4*k5_k5*k5_l2*l1_l2*Ak3*fl17*fl25 - 2.E+0*P_q*k3_l1*
             k4_k5*k4_l1*k5_k5*l2_l2*Ak3*fl17*fl25 + 2.E+0*P_q*k3_l1*k4_k5*
             k4_l2*k5_k5*l1_l2*Ak3*fl17*fl25 + 2.E+0*P_q*k3_l1*k4_l1*k4_l2*
             k5_k5*k5_l2*Ak3*fl17*fl25 - 2.E+0*P_q*k3_l1*pow(k4_l2,2)*k5_k5
             *k5_l1*Ak3*fl17*fl25 - 2.E+0*P_q*k3_l2*k4_k4*k5_k5*k5_l1*l1_l2
             *Ak3*fl17*fl25 + 2.E+0*P_q*k3_l2*k4_k4*k5_k5*k5_l2*l1_l1*Ak3*
             fl17*fl25 + 2.E+0*P_q*k3_l2*k4_k5*k4_l1*k5_k5*l1_l2*Ak3*fl17*
             fl25 - 2.E+0*P_q*k3_l2*k4_k5*k4_l2*k5_k5*l1_l1*Ak3*fl17*fl25
              + 2.E+0*P_q*k3_l2*k4_l1*k4_l2*k5_k5*k5_l1*Ak3*fl17*fl25 -
             2.E+0*P_q*k3_l2*pow(k4_l1,2)*k5_k5*k5_l2*Ak3*fl17*fl25 - 2.E+0
             *P_l1*k3_k4*k4_k5*k5_k5*q_l1*l2_l2*Ak3*fl17*fl25 + 1.E+1*P_l1*
             k3_k4*k4_k5*k5_k5*q_l2*l1_l2*Ak3*fl17*fl25 - 6.E+0*P_l1*k3_k4*
             k4_q*k5_k5*k5_l1*l2_l2*Ak3*fl17*fl25 + 6.E+0*P_l1*k3_k4*k4_q*
             k5_k5*k5_l2*l1_l2*Ak3*fl17*fl25 + 8.E+0*P_l1*k3_k4*k4_l1*k5_k5
             *k5_q*l2_l2*Ak3*fl17*fl25 - 8.E+0*P_l1*k3_k4*k4_l1*k5_k5*k5_l2
             *q_l2*Ak3*fl17*fl25 - 8.E+0*P_l1*k3_k4*k4_l2*k5_k5*k5_q*l1_l2*
             Ak3*fl17*fl25 - 2.E+0*P_l1*k3_k4*k4_l2*k5_k5*k5_l1*q_l2*Ak3*
             fl17*fl25 + 2.E+0*P_l1*k3_k4*k4_l2*k5_k5*k5_l2*q_l1*Ak3*fl17*
             fl25 + 2.E+0*P_l1*k3_k5*k4_k4*k5_k5*q_l1*l2_l2*Ak3*fl17*fl25
              - 1.E+1*P_l1*k3_k5*k4_k4*k5_k5*q_l2*l1_l2*Ak3*fl17*fl25 -
             2.E+0*P_l1*k3_k5*k4_q*k4_l1*k5_k5*l2_l2*Ak3*fl17*fl25 + 2.E+0*
             P_l1*k3_k5*k4_q*k4_l2*k5_k5*l1_l2*Ak3*fl17*fl25 + 1.E+1*P_l1*
             k3_k5*k4_l1*k4_l2*k5_k5*q_l2*Ak3*fl17*fl25 - 2.E+0*P_l1*k3_k5*
             pow(k4_l2,2)*k5_k5*q_l1*Ak3*fl17*fl25 - 2.E+0*P_l1*k3_q*k4_k4*
             k5_k5*k5_l1*l2_l2*Ak3*fl17*fl25 + 2.E+0*P_l1*k3_q*k4_k4*k5_k5*
             k5_l2*l1_l2*Ak3*fl17*fl25 + 2.E+0*P_l1*k3_q*k4_k5*k4_l1*k5_k5*
             l2_l2*Ak3*fl17*fl25 - 2.E+0*P_l1*k3_q*k4_k5*k4_l2*k5_k5*l1_l2*
             Ak3*fl17*fl25 - 2.E+0*P_l1*k3_q*k4_l1*k4_l2*k5_k5*k5_l2*Ak3*
             fl17*fl25 + 2.E+0*P_l1*k3_q*pow(k4_l2,2)*k5_k5*k5_l1*Ak3*fl17*
             fl25 - 8.E+0*P_l1*k3_l1*k4_k4*k5_k5*k5_q*l2_l2*Ak3*fl17*fl25
              + 8.E+0*P_l1*k3_l1*k4_k4*k5_k5*k5_l2*q_l2*Ak3*fl17*fl25 +
             8.E+0*P_l1*k3_l1*k4_k5*k4_q*k5_k5*l2_l2*Ak3*fl17*fl25 );

          Chi(10,8) +=  + H * (  - 8.E+0*P_l1*k3_l1*k4_k5*k4_l2*k5_k5*
             q_l2*Ak3*fl17*fl25 - 8.E+0*P_l1*k3_l1*k4_q*k4_l2*k5_k5*k5_l2*
             Ak3*fl17*fl25 + 8.E+0*P_l1*k3_l1*pow(k4_l2,2)*k5_k5*k5_q*Ak3*
             fl17*fl25 + 1.E+1*P_l1*k3_l2*k4_k4*k5_k5*k5_l1*q_l2*Ak3*fl17*
             fl25 - 2.E+0*P_l1*k3_l2*k4_k4*k5_k5*k5_l2*q_l1*Ak3*fl17*fl25
              - 1.E+1*P_l1*k3_l2*k4_k5*k4_l1*k5_k5*q_l2*Ak3*fl17*fl25 +
             2.E+0*P_l1*k3_l2*k4_k5*k4_l2*k5_k5*q_l1*Ak3*fl17*fl25 + 2.E+0*
             P_l1*k3_l2*k4_q*k4_l1*k5_k5*k5_l2*Ak3*fl17*fl25 - 2.E+0*P_l1*
             k3_l2*k4_q*k4_l2*k5_k5*k5_l1*Ak3*fl17*fl25 - 6.E+0*P_l2*k3_k4*
             k4_k5*k5_k5*q_l1*l1_l2*Ak3*fl17*fl25 - 2.E+0*P_l2*k3_k4*k4_k5*
             k5_k5*q_l2*l1_l1*Ak3*fl17*fl25 + 6.E+0*P_l2*k3_k4*k4_q*k5_k5*
             k5_l1*l1_l2*Ak3*fl17*fl25 - 6.E+0*P_l2*k3_k4*k4_q*k5_k5*k5_l2*
             l1_l1*Ak3*fl17*fl25 - 8.E+0*P_l2*k3_k4*k4_l1*k5_k5*k5_q*l1_l2*
             Ak3*fl17*fl25 + 2.E+0*P_l2*k3_k4*k4_l1*k5_k5*k5_l1*q_l2*Ak3*
             fl17*fl25 + 6.E+0*P_l2*k3_k4*k4_l1*k5_k5*k5_l2*q_l1*Ak3*fl17*
             fl25 + 6.E+0*P_l2*k3_k5*k4_k4*k5_k5*q_l1*l1_l2*Ak3*fl17*fl25
              + 2.E+0*P_l2*k3_k5*k4_k4*k5_k5*q_l2*l1_l1*Ak3*fl17*fl25 +
             2.E+0*P_l2*k3_k5*k4_q*k4_l1*k5_k5*l1_l2*Ak3*fl17*fl25 + 6.E+0*
             P_l2*k3_k5*k4_q*k4_l2*k5_k5*l1_l1*Ak3*fl17*fl25 - 6.E+0*P_l2*
             k3_k5*k4_l1*k4_l2*k5_k5*q_l1*Ak3*fl17*fl25 - 2.E+0*P_l2*k3_k5*
             pow(k4_l1,2)*k5_k5*q_l2*Ak3*fl17*fl25 + 2.E+0*P_l2*k3_q*k4_k4*
             k5_k5*k5_l1*l1_l2*Ak3*fl17*fl25 - 2.E+0*P_l2*k3_q*k4_k4*k5_k5*
             k5_l2*l1_l1*Ak3*fl17*fl25 - 2.E+0*P_l2*k3_q*k4_k5*k4_l1*k5_k5*
             l1_l2*Ak3*fl17*fl25 + 2.E+0*P_l2*k3_q*k4_k5*k4_l2*k5_k5*l1_l1*
             Ak3*fl17*fl25 + 6.E+0*P_l2*k3_q*k4_l1*k4_l2*k5_k5*k5_l1*Ak3*
             fl17*fl25 - 6.E+0*P_l2*k3_q*pow(k4_l1,2)*k5_k5*k5_l2*Ak3*fl17*
             fl25 - 2.E+0*P_l2*k3_l1*k4_k4*k5_k5*k5_l1*q_l2*Ak3*fl17*fl25
              - 6.E+0*P_l2*k3_l1*k4_k4*k5_k5*k5_l2*q_l1*Ak3*fl17*fl25 +
             2.E+0*P_l2*k3_l1*k4_k5*k4_l1*k5_k5*q_l2*Ak3*fl17*fl25 + 6.E+0*
             P_l2*k3_l1*k4_k5*k4_l2*k5_k5*q_l1*Ak3*fl17*fl25 + 6.E+0*P_l2*
             k3_l1*k4_q*k4_l1*k5_k5*k5_l2*Ak3*fl17*fl25 - 6.E+0*P_l2*k3_l1*
             k4_q*k4_l2*k5_k5*k5_l1*Ak3*fl17*fl25 + 8.E+0*P_l2*k3_l2*k4_k4*
             k5_k5*k5_q*l1_l1*Ak3*fl17*fl25 - 8.E+0*P_l2*k3_l2*k4_k4*k5_k5*
             k5_l1*q_l1*Ak3*fl17*fl25 - 8.E+0*P_l2*k3_l2*k4_k5*k4_q*k5_k5*
             l1_l1*Ak3*fl17*fl25 + 8.E+0*P_l2*k3_l2*k4_k5*k4_l1*k5_k5*q_l1*
             Ak3*fl17*fl25 - 8.E+0*P_l2*k3_l2*k4_q*k4_l1*k5_k5*k5_l1*Ak3*
             fl17*fl25 + 8.E+0*P_l2*k3_l2*pow(k4_l1,2)*k5_k5*k5_q*Ak3*fl17*
             fl25 );

          Chi(10,8) +=  + G * ( 4.E+0*P_q*k4_k4*k5_k5*k5_q*l1_l1*l2_l2*
             Bk3*fl17*fl25 - 4.E+0*P_q*k4_k4*k5_k5*k5_q*pow(l1_l2,2)*Bk3*
             fl17*fl25 - 4.E+0*P_q*k4_k4*k5_k5*k5_l1*q_l1*l2_l2*Bk3*fl17*
             fl25 + 4.E+0*P_q*k4_k4*k5_k5*k5_l1*q_l2*l1_l2*Bk3*fl17*fl25 +
             4.E+0*P_q*k4_k4*k5_k5*k5_l2*q_l1*l1_l2*Bk3*fl17*fl25 - 4.E+0*
             P_q*k4_k4*k5_k5*k5_l2*q_l2*l1_l1*Bk3*fl17*fl25 - 4.E+0*P_q*
             k4_k5*k4_q*k5_k5*l1_l1*l2_l2*Bk3*fl17*fl25 + 4.E+0*P_q*k4_k5*
             k4_q*k5_k5*pow(l1_l2,2)*Bk3*fl17*fl25 + 4.E+0*P_q*k4_k5*k4_l1*
             k5_k5*q_l1*l2_l2*Bk3*fl17*fl25 - 4.E+0*P_q*k4_k5*k4_l1*k5_k5*
             q_l2*l1_l2*Bk3*fl17*fl25 - 4.E+0*P_q*k4_k5*k4_l2*k5_k5*q_l1*
             l1_l2*Bk3*fl17*fl25 + 4.E+0*P_q*k4_k5*k4_l2*k5_k5*q_l2*l1_l1*
             Bk3*fl17*fl25 - 4.E+0*P_q*k4_q*k4_l2*k5_k5*k5_l1*l1_l2*Bk3*
             fl17*fl25 + 4.E+0*P_q*k4_q*k4_l2*k5_k5*k5_l2*l1_l1*Bk3*fl17*
             fl25 + 4.E+0*P_q*k4_l1*k4_l2*k5_k5*k5_q*l1_l2*Bk3*fl17*fl25 -
             4.E+0*P_q*k4_l1*k4_l2*k5_k5*k5_l2*q_l1*Bk3*fl17*fl25 - 4.E+0*
             P_q*pow(k4_l2,2)*k5_k5*k5_q*l1_l1*Bk3*fl17*fl25 + 4.E+0*P_q*
             pow(k4_l2,2)*k5_k5*k5_l1*q_l1*Bk3*fl17*fl25 );

          Chi(10,8) +=  + F * (  - 4.E+0*P_k4*k4_k5*k5_k5*l1_l1*l2_l2*Bk3
             *fl17*fl25 + 4.E+0*P_k4*k4_k5*k5_k5*pow(l1_l2,2)*Bk3*fl17*fl25
              - 4.E+0*P_k4*k4_l2*k5_k5*k5_l1*l1_l2*Bk3*fl17*fl25 + 4.E+0*
             P_k4*k4_l2*k5_k5*k5_l2*l1_l1*Bk3*fl17*fl25 + 4.E+0*P_k5*k4_k4*
             k5_k5*l1_l1*l2_l2*Bk3*fl17*fl25 - 4.E+0*P_k5*k4_k4*k5_k5*pow(
             l1_l2,2)*Bk3*fl17*fl25 + 4.E+0*P_k5*k4_l1*k4_l2*k5_k5*l1_l2*
             Bk3*fl17*fl25 - 4.E+0*P_k5*pow(k4_l2,2)*k5_k5*l1_l1*Bk3*fl17*
             fl25 - 4.E+0*P_l1*k4_k4*k5_k5*k5_l1*l2_l2*Bk3*fl17*fl25 +
             4.E+0*P_l1*k4_k4*k5_k5*k5_l2*l1_l2*Bk3*fl17*fl25 + 4.E+0*P_l1*
             k4_k5*k4_l1*k5_k5*l2_l2*Bk3*fl17*fl25 - 4.E+0*P_l1*k4_k5*k4_l2
             *k5_k5*l1_l2*Bk3*fl17*fl25 - 4.E+0*P_l1*k4_l1*k4_l2*k5_k5*
             k5_l2*Bk3*fl17*fl25 + 4.E+0*P_l1*pow(k4_l2,2)*k5_k5*k5_l1*Bk3*
             fl17*fl25 + 4.E+0*P_l2*k4_k4*k5_k5*k5_l1*l1_l2*Bk3*fl17*fl25
              - 4.E+0*P_l2*k4_k4*k5_k5*k5_l2*l1_l1*Bk3*fl17*fl25 - 4.E+0*
             P_l2*k4_k5*k4_l1*k5_k5*l1_l2*Bk3*fl17*fl25 + 4.E+0*P_l2*k4_k5*
             k4_l2*k5_k5*l1_l1*Bk3*fl17*fl25 );

          Chi(10,8) +=  + E * ( 4.E+0*k3_k4*k4_k5*k5_k5*l1_l1*l2_l2*Ak3*
             fl17*fl25 - 4.E+0*k3_k4*k4_k5*k5_k5*pow(l1_l2,2)*Ak3*fl17*fl25
              + 4.E+0*k3_k4*k4_l2*k5_k5*k5_l1*l1_l2*Ak3*fl17*fl25 - 4.E+0*
             k3_k4*k4_l2*k5_k5*k5_l2*l1_l1*Ak3*fl17*fl25 - 4.E+0*k3_k5*
             k4_k4*k5_k5*l1_l1*l2_l2*Ak3*fl17*fl25 + 4.E+0*k3_k5*k4_k4*
             k5_k5*pow(l1_l2,2)*Ak3*fl17*fl25 - 4.E+0*k3_k5*k4_l1*k4_l2*
             k5_k5*l1_l2*Ak3*fl17*fl25 + 4.E+0*k3_k5*pow(k4_l2,2)*k5_k5*
             l1_l1*Ak3*fl17*fl25 + 4.E+0*k3_l1*k4_k4*k5_k5*k5_l1*l2_l2*Ak3*
             fl17*fl25 - 4.E+0*k3_l1*k4_k4*k5_k5*k5_l2*l1_l2*Ak3*fl17*fl25
              - 4.E+0*k3_l1*k4_k5*k4_l1*k5_k5*l2_l2*Ak3*fl17*fl25 + 4.E+0*
             k3_l1*k4_k5*k4_l2*k5_k5*l1_l2*Ak3*fl17*fl25 + 4.E+0*k3_l1*
             k4_l1*k4_l2*k5_k5*k5_l2*Ak3*fl17*fl25 - 4.E+0*k3_l1*pow(
             k4_l2,2)*k5_k5*k5_l1*Ak3*fl17*fl25 - 4.E+0*k3_l2*k4_k4*k5_k5*
             k5_l1*l1_l2*Ak3*fl17*fl25 + 4.E+0*k3_l2*k4_k4*k5_k5*k5_l2*
             l1_l1*Ak3*fl17*fl25 + 4.E+0*k3_l2*k4_k5*k4_l1*k5_k5*l1_l2*Ak3*
             fl17*fl25 - 4.E+0*k3_l2*k4_k5*k4_l2*k5_k5*l1_l1*Ak3*fl17*fl25
              );

       Chi(10,9) =
           + H * ( 4.E+0*P_k3*k4_k4*k5_k5*k5_l2*q_l1*l1_l2*Ak3*fl17*fl26 -
             4.E+0*P_k3*k4_k4*k5_k5*k5_l2*q_l2*l1_l1*Ak3*fl17*fl26 - 4.E+0*
             P_k3*k4_k4*k5_q*k5_l1*k5_l2*l1_l2*Ak3*fl17*fl26 + 1.2E+1*P_k3*
             k4_k4*k5_q*pow(k5_l2,2)*l1_l1*Ak3*fl17*fl26 - 1.2E+1*P_k3*
             k4_k4*k5_l1*pow(k5_l2,2)*q_l1*Ak3*fl17*fl26 + 4.E+0*P_k3*k4_k4
             *pow(k5_l1,2)*k5_l2*q_l2*Ak3*fl17*fl26 + 4.E+0*P_k3*k4_k5*k4_q
             *k5_l1*k5_l2*l1_l2*Ak3*fl17*fl26 - 1.2E+1*P_k3*k4_k5*k4_q*pow(
             k5_l2,2)*l1_l1*Ak3*fl17*fl26 - 4.E+0*P_k3*k4_k5*k4_l1*k5_q*
             k5_l2*l1_l2*Ak3*fl17*fl26 + 1.2E+1*P_k3*k4_k5*k4_l1*pow(
             k5_l2,2)*q_l1*Ak3*fl17*fl26 - 4.E+0*P_k3*k4_k5*k4_l2*k5_q*
             k5_l2*l1_l1*Ak3*fl17*fl26 + 4.E+0*P_k3*k4_k5*k4_l2*k5_l1*k5_l2
             *q_l1*Ak3*fl17*fl26 - 4.E+0*P_k3*pow(k4_k5,2)*k5_l2*q_l1*l1_l2
             *Ak3*fl17*fl26 + 4.E+0*P_k3*pow(k4_k5,2)*k5_l2*q_l2*l1_l1*Ak3*
             fl17*fl26 + 4.E+0*P_k3*k4_q*k4_l1*k5_k5*k5_l2*l1_l2*Ak3*fl17*
             fl26 + 4.E+0*P_k3*k4_q*k4_l1*k5_l1*pow(k5_l2,2)*Ak3*fl17*fl26
              + 4.E+0*P_k3*k4_q*k4_l2*k5_k5*k5_l2*l1_l1*Ak3*fl17*fl26 -
             4.E+0*P_k3*k4_q*k4_l2*pow(k5_l1,2)*k5_l2*Ak3*fl17*fl26 - 4.E+0
             *P_k3*k4_l1*k4_l2*k5_k5*k5_l2*q_l1*Ak3*fl17*fl26 + 4.E+0*P_k3*
             k4_l1*k4_l2*k5_q*k5_l1*k5_l2*Ak3*fl17*fl26 - 4.E+0*P_k3*pow(
             k4_l1,2)*k5_k5*k5_l2*q_l2*Ak3*fl17*fl26 - 4.E+0*P_k3*pow(
             k4_l1,2)*k5_q*pow(k5_l2,2)*Ak3*fl17*fl26 + 4.E+0*P_k4*k3_k4*
             k5_k5*k5_l2*q_l1*l1_l2*Ak3*fl17*fl26 - 4.E+0*P_k4*k3_k4*k5_k5*
             k5_l2*q_l2*l1_l1*Ak3*fl17*fl26 - 4.E+0*P_k4*k3_k4*k5_q*k5_l1*
             k5_l2*l1_l2*Ak3*fl17*fl26 - 1.2E+1*P_k4*k3_k4*k5_q*pow(
             k5_l2,2)*l1_l1*Ak3*fl17*fl26 + 1.2E+1*P_k4*k3_k4*k5_l1*pow(
             k5_l2,2)*q_l1*Ak3*fl17*fl26 + 4.E+0*P_k4*k3_k4*pow(k5_l1,2)*
             k5_l2*q_l2*Ak3*fl17*fl26 + 4.E+0*P_k4*k3_k5*k4_k5*k5_l2*q_l1*
             l1_l2*Ak3*fl17*fl26 - 4.E+0*P_k4*k3_k5*k4_k5*k5_l2*q_l2*l1_l1*
             Ak3*fl17*fl26 - 2.E+0*P_k4*k3_k5*k4_q*k5_l1*k5_l2*l1_l2*Ak3*
             fl17*fl26 + 2.E+0*P_k4*k3_k5*k4_q*pow(k5_l2,2)*l1_l1*Ak3*fl17*
             fl26 + 6.E+0*P_k4*k3_k5*k4_l1*k5_q*k5_l2*l1_l2*Ak3*fl17*fl26
              - 4.E+0*P_k4*k3_k5*k4_l1*k5_l1*k5_l2*q_l2*Ak3*fl17*fl26 -
             1.E+1*P_k4*k3_k5*k4_l1*pow(k5_l2,2)*q_l1*Ak3*fl17*fl26 + 1.E+1
             *P_k4*k3_k5*k4_l2*k5_q*k5_l2*l1_l1*Ak3*fl17*fl26 - 2.E+0*P_k4*
             k3_k5*k4_l2*k5_l1*k5_l2*q_l1*Ak3*fl17*fl26 - 2.E+0*P_k4*k3_q*
             k4_k5*k5_l1*k5_l2*l1_l2*Ak3*fl17*fl26 + 1.E+1*P_k4*k3_q*k4_k5*
             pow(k5_l2,2)*l1_l1*Ak3*fl17*fl26 - 6.E+0*P_k4*k3_q*k4_l1*k5_k5
             *k5_l2*l1_l2*Ak3*fl17*fl26 - 2.E+0*P_k4*k3_q*k4_l1*k5_l1*pow(
             k5_l2,2)*Ak3*fl17*fl26 - 2.E+0*P_k4*k3_q*k4_l2*k5_k5*k5_l2*
             l1_l1*Ak3*fl17*fl26 + 2.E+0*P_k4*k3_q*k4_l2*pow(k5_l1,2)*k5_l2
             *Ak3*fl17*fl26 + 6.E+0*P_k4*k3_l1*k4_k5*k5_q*k5_l2*l1_l2*Ak3*
             fl17*fl26 - 4.E+0*P_k4*k3_l1*k4_k5*k5_l1*k5_l2*q_l2*Ak3*fl17*
             fl26 - 1.E+1*P_k4*k3_l1*k4_k5*pow(k5_l2,2)*q_l1*Ak3*fl17*fl26
              + 2.E+0*P_k4*k3_l1*k4_q*k5_k5*k5_l2*l1_l2*Ak3*fl17*fl26 -
             2.E+0*P_k4*k3_l1*k4_q*k5_l1*pow(k5_l2,2)*Ak3*fl17*fl26 + 4.E+0
             *P_k4*k3_l1*k4_l1*k5_k5*k5_l2*q_l2*Ak3*fl17*fl26 + 4.E+0*P_k4*
             k3_l1*k4_l1*k5_q*pow(k5_l2,2)*Ak3*fl17*fl26 - 6.E+0*P_k4*k3_l1
             *k4_l2*k5_k5*k5_l2*q_l1*Ak3*fl17*fl26 - 2.E+0*P_k4*k3_l1*k4_l2
             *k5_q*k5_l1*k5_l2*Ak3*fl17*fl26 - 6.E+0*P_k4*k3_l2*k4_k5*k5_q*
             k5_l2*l1_l1*Ak3*fl17*fl26 - 2.E+0*P_k4*k3_l2*k4_k5*k5_l1*k5_l2
             *q_l1*Ak3*fl17*fl26 );

          Chi(10,9) +=  + H * (  - 2.E+0*P_k4*k3_l2*k4_q*k5_k5*k5_l2*
             l1_l1*Ak3*fl17*fl26 + 2.E+0*P_k4*k3_l2*k4_q*pow(k5_l1,2)*k5_l2
             *Ak3*fl17*fl26 + 2.E+0*P_k4*k3_l2*k4_l1*k5_k5*k5_l2*q_l1*Ak3*
             fl17*fl26 - 2.E+0*P_k4*k3_l2*k4_l1*k5_q*k5_l1*k5_l2*Ak3*fl17*
             fl26 + 4.E+0*P_k5*k3_k4*k4_k5*k5_l2*q_l1*l1_l2*Ak3*fl17*fl26
              - 4.E+0*P_k5*k3_k4*k4_k5*k5_l2*q_l2*l1_l1*Ak3*fl17*fl26 +
             6.E+0*P_k5*k3_k4*k4_q*k5_l1*k5_l2*l1_l2*Ak3*fl17*fl26 + 1.E+1*
             P_k5*k3_k4*k4_q*pow(k5_l2,2)*l1_l1*Ak3*fl17*fl26 - 2.E+0*P_k5*
             k3_k4*k4_l1*k5_q*k5_l2*l1_l2*Ak3*fl17*fl26 - 4.E+0*P_k5*k3_k4*
             k4_l1*k5_l1*k5_l2*q_l2*Ak3*fl17*fl26 - 2.E+0*P_k5*k3_k4*k4_l1*
             pow(k5_l2,2)*q_l1*Ak3*fl17*fl26 + 2.E+0*P_k5*k3_k4*k4_l2*k5_q*
             k5_l2*l1_l1*Ak3*fl17*fl26 - 1.E+1*P_k5*k3_k4*k4_l2*k5_l1*k5_l2
             *q_l1*Ak3*fl17*fl26 - 1.2E+1*P_k5*k3_k5*k4_k4*k5_l2*q_l1*l1_l2
             *Ak3*fl17*fl26 + 1.2E+1*P_k5*k3_k5*k4_k4*k5_l2*q_l2*l1_l1*Ak3*
             fl17*fl26 - 4.E+0*P_k5*k3_k5*k4_q*k4_l1*k5_l2*l1_l2*Ak3*fl17*
             fl26 - 1.2E+1*P_k5*k3_k5*k4_q*k4_l2*k5_l2*l1_l1*Ak3*fl17*fl26
              + 1.2E+1*P_k5*k3_k5*k4_l1*k4_l2*k5_l2*q_l1*Ak3*fl17*fl26 +
             4.E+0*P_k5*k3_k5*pow(k4_l1,2)*k5_l2*q_l2*Ak3*fl17*fl26 + 2.E+0
             *P_k5*k3_q*k4_k4*k5_l1*k5_l2*l1_l2*Ak3*fl17*fl26 - 1.E+1*P_k5*
             k3_q*k4_k4*pow(k5_l2,2)*l1_l1*Ak3*fl17*fl26 + 6.E+0*P_k5*k3_q*
             k4_k5*k4_l1*k5_l2*l1_l2*Ak3*fl17*fl26 + 2.E+0*P_k5*k3_q*k4_k5*
             k4_l2*k5_l2*l1_l1*Ak3*fl17*fl26 - 2.E+0*P_k5*k3_q*k4_l1*k4_l2*
             k5_l1*k5_l2*Ak3*fl17*fl26 + 2.E+0*P_k5*k3_q*pow(k4_l1,2)*pow(
             k5_l2,2)*Ak3*fl17*fl26 + 2.E+0*P_k5*k3_l1*k4_k4*k5_q*k5_l2*
             l1_l2*Ak3*fl17*fl26 - 4.E+0*P_k5*k3_l1*k4_k4*k5_l1*k5_l2*q_l2*
             Ak3*fl17*fl26 + 1.E+1*P_k5*k3_l1*k4_k4*pow(k5_l2,2)*q_l1*Ak3*
             fl17*fl26 - 1.E+1*P_k5*k3_l1*k4_k5*k4_q*k5_l2*l1_l2*Ak3*fl17*
             fl26 + 4.E+0*P_k5*k3_l1*k4_k5*k4_l1*k5_l2*q_l2*Ak3*fl17*fl26
              + 6.E+0*P_k5*k3_l1*k4_k5*k4_l2*k5_l2*q_l1*Ak3*fl17*fl26 -
             2.E+0*P_k5*k3_l1*k4_q*k4_l1*pow(k5_l2,2)*Ak3*fl17*fl26 + 4.E+0
             *P_k5*k3_l1*k4_q*k4_l2*k5_l1*k5_l2*Ak3*fl17*fl26 - 2.E+0*P_k5*
             k3_l1*k4_l1*k4_l2*k5_q*k5_l2*Ak3*fl17*fl26 - 2.E+0*P_k5*k3_l2*
             k4_k4*k5_q*k5_l2*l1_l1*Ak3*fl17*fl26 + 1.E+1*P_k5*k3_l2*k4_k4*
             k5_l1*k5_l2*q_l1*Ak3*fl17*fl26 + 1.E+1*P_k5*k3_l2*k4_k5*k4_q*
             k5_l2*l1_l1*Ak3*fl17*fl26 - 1.E+1*P_k5*k3_l2*k4_k5*k4_l1*k5_l2
             *q_l1*Ak3*fl17*fl26 - 2.E+0*P_k5*k3_l2*k4_q*k4_l1*k5_l1*k5_l2*
             Ak3*fl17*fl26 + 2.E+0*P_k5*k3_l2*pow(k4_l1,2)*k5_q*k5_l2*Ak3*
             fl17*fl26 - 2.E+0*P_q*k3_k4*k4_k5*k5_l1*k5_l2*l1_l2*Ak3*fl17*
             fl26 + 2.E+0*P_q*k3_k4*k4_k5*pow(k5_l2,2)*l1_l1*Ak3*fl17*fl26
              + 2.E+0*P_q*k3_k4*k4_l1*k5_k5*k5_l2*l1_l2*Ak3*fl17*fl26 -
             2.E+0*P_q*k3_k4*k4_l1*k5_l1*pow(k5_l2,2)*Ak3*fl17*fl26 - 2.E+0
             *P_q*k3_k4*k4_l2*k5_k5*k5_l2*l1_l1*Ak3*fl17*fl26 + 2.E+0*P_q*
             k3_k4*k4_l2*pow(k5_l1,2)*k5_l2*Ak3*fl17*fl26 + 2.E+0*P_q*k3_k5
             *k4_k4*k5_l1*k5_l2*l1_l2*Ak3*fl17*fl26 - 2.E+0*P_q*k3_k5*k4_k4
             *pow(k5_l2,2)*l1_l1*Ak3*fl17*fl26 - 2.E+0*P_q*k3_k5*k4_k5*
             k4_l1*k5_l2*l1_l2*Ak3*fl17*fl26 + 2.E+0*P_q*k3_k5*k4_k5*k4_l2*
             k5_l2*l1_l1*Ak3*fl17*fl26 - 2.E+0*P_q*k3_k5*k4_l1*k4_l2*k5_l1*
             k5_l2*Ak3*fl17*fl26 + 2.E+0*P_q*k3_k5*pow(k4_l1,2)*pow(
             k5_l2,2)*Ak3*fl17*fl26 - 2.E+0*P_q*k3_l1*k4_k4*k5_k5*k5_l2*
             l1_l2*Ak3*fl17*fl26 + 2.E+0*P_q*k3_l1*k4_k4*k5_l1*pow(k5_l2,2)
             *Ak3*fl17*fl26 );

          Chi(10,9) +=  + H * (  - 2.E+0*P_q*k3_l1*k4_k5*k4_l1*pow(
             k5_l2,2)*Ak3*fl17*fl26 - 2.E+0*P_q*k3_l1*k4_k5*k4_l2*k5_l1*
             k5_l2*Ak3*fl17*fl26 + 2.E+0*P_q*k3_l1*pow(k4_k5,2)*k5_l2*l1_l2
             *Ak3*fl17*fl26 + 2.E+0*P_q*k3_l1*k4_l1*k4_l2*k5_k5*k5_l2*Ak3*
             fl17*fl26 + 2.E+0*P_q*k3_l2*k4_k4*k5_k5*k5_l2*l1_l1*Ak3*fl17*
             fl26 - 2.E+0*P_q*k3_l2*k4_k4*pow(k5_l1,2)*k5_l2*Ak3*fl17*fl26
              + 4.E+0*P_q*k3_l2*k4_k5*k4_l1*k5_l1*k5_l2*Ak3*fl17*fl26 -
             2.E+0*P_q*k3_l2*pow(k4_k5,2)*k5_l2*l1_l1*Ak3*fl17*fl26 - 2.E+0
             *P_q*k3_l2*pow(k4_l1,2)*k5_k5*k5_l2*Ak3*fl17*fl26 - 2.E+0*P_l1
             *k3_k4*k4_k5*k5_q*k5_l2*l1_l2*Ak3*fl17*fl26 - 4.E+0*P_l1*k3_k4
             *k4_k5*k5_l1*k5_l2*q_l2*Ak3*fl17*fl26 - 2.E+0*P_l1*k3_k4*k4_k5
             *pow(k5_l2,2)*q_l1*Ak3*fl17*fl26 - 6.E+0*P_l1*k3_k4*k4_q*k5_k5
             *k5_l2*l1_l2*Ak3*fl17*fl26 - 1.E+1*P_l1*k3_k4*k4_q*k5_l1*pow(
             k5_l2,2)*Ak3*fl17*fl26 + 4.E+0*P_l1*k3_k4*k4_l1*k5_k5*k5_l2*
             q_l2*Ak3*fl17*fl26 + 4.E+0*P_l1*k3_k4*k4_l1*k5_q*pow(k5_l2,2)*
             Ak3*fl17*fl26 + 2.E+0*P_l1*k3_k4*k4_l2*k5_k5*k5_l2*q_l1*Ak3*
             fl17*fl26 + 6.E+0*P_l1*k3_k4*k4_l2*k5_q*k5_l1*k5_l2*Ak3*fl17*
             fl26 + 1.E+1*P_l1*k3_k5*k4_k4*k5_q*k5_l2*l1_l2*Ak3*fl17*fl26
              - 4.E+0*P_l1*k3_k5*k4_k4*k5_l1*k5_l2*q_l2*Ak3*fl17*fl26 +
             2.E+0*P_l1*k3_k5*k4_k4*pow(k5_l2,2)*q_l1*Ak3*fl17*fl26 - 2.E+0
             *P_l1*k3_k5*k4_k5*k4_q*k5_l2*l1_l2*Ak3*fl17*fl26 + 4.E+0*P_l1*
             k3_k5*k4_k5*k4_l1*k5_l2*q_l2*Ak3*fl17*fl26 - 2.E+0*P_l1*k3_k5*
             k4_k5*k4_l2*k5_l2*q_l1*Ak3*fl17*fl26 + 6.E+0*P_l1*k3_k5*k4_q*
             k4_l1*pow(k5_l2,2)*Ak3*fl17*fl26 + 4.E+0*P_l1*k3_k5*k4_q*k4_l2
             *k5_l1*k5_l2*Ak3*fl17*fl26 - 1.E+1*P_l1*k3_k5*k4_l1*k4_l2*k5_q
             *k5_l2*Ak3*fl17*fl26 - 2.E+0*P_l1*k3_q*k4_k4*k5_k5*k5_l2*l1_l2
             *Ak3*fl17*fl26 + 1.E+1*P_l1*k3_q*k4_k4*k5_l1*pow(k5_l2,2)*Ak3*
             fl17*fl26 - 1.E+1*P_l1*k3_q*k4_k5*k4_l1*pow(k5_l2,2)*Ak3*fl17*
             fl26 - 2.E+0*P_l1*k3_q*k4_k5*k4_l2*k5_l1*k5_l2*Ak3*fl17*fl26
              + 2.E+0*P_l1*k3_q*pow(k4_k5,2)*k5_l2*l1_l2*Ak3*fl17*fl26 +
             2.E+0*P_l1*k3_q*k4_l1*k4_l2*k5_k5*k5_l2*Ak3*fl17*fl26 - 4.E+0*
             P_l1*k3_l1*k4_k4*k5_k5*k5_l2*q_l2*Ak3*fl17*fl26 - 1.2E+1*P_l1*
             k3_l1*k4_k4*k5_q*pow(k5_l2,2)*Ak3*fl17*fl26 + 1.2E+1*P_l1*
             k3_l1*k4_k5*k4_q*pow(k5_l2,2)*Ak3*fl17*fl26 - 4.E+0*P_l1*k3_l1
             *k4_k5*k4_l2*k5_q*k5_l2*Ak3*fl17*fl26 + 4.E+0*P_l1*k3_l1*pow(
             k4_k5,2)*k5_l2*q_l2*Ak3*fl17*fl26 + 4.E+0*P_l1*k3_l1*k4_q*
             k4_l2*k5_k5*k5_l2*Ak3*fl17*fl26 - 2.E+0*P_l1*k3_l2*k4_k4*k5_k5
             *k5_l2*q_l1*Ak3*fl17*fl26 - 6.E+0*P_l1*k3_l2*k4_k4*k5_q*k5_l1*
             k5_l2*Ak3*fl17*fl26 - 2.E+0*P_l1*k3_l2*k4_k5*k4_q*k5_l1*k5_l2*
             Ak3*fl17*fl26 + 6.E+0*P_l1*k3_l2*k4_k5*k4_l1*k5_q*k5_l2*Ak3*
             fl17*fl26 + 2.E+0*P_l1*k3_l2*pow(k4_k5,2)*k5_l2*q_l1*Ak3*fl17*
             fl26 + 2.E+0*P_l1*k3_l2*k4_q*k4_l1*k5_k5*k5_l2*Ak3*fl17*fl26
              + 2.E+0*P_l2*k3_k4*k4_k5*k5_q*k5_l2*l1_l1*Ak3*fl17*fl26 +
             6.E+0*P_l2*k3_k4*k4_k5*k5_l1*k5_l2*q_l1*Ak3*fl17*fl26 + 6.E+0*
             P_l2*k3_k4*k4_q*k5_k5*k5_l2*l1_l1*Ak3*fl17*fl26 - 6.E+0*P_l2*
             k3_k4*k4_q*pow(k5_l1,2)*k5_l2*Ak3*fl17*fl26 - 6.E+0*P_l2*k3_k4
             *k4_l1*k5_k5*k5_l2*q_l1*Ak3*fl17*fl26 + 6.E+0*P_l2*k3_k4*k4_l1
             *k5_q*k5_l1*k5_l2*Ak3*fl17*fl26 - 1.E+1*P_l2*k3_k5*k4_k4*k5_q*
             k5_l2*l1_l1*Ak3*fl17*fl26 + 2.E+0*P_l2*k3_k5*k4_k4*k5_l1*k5_l2
             *q_l1*Ak3*fl17*fl26 + 2.E+0*P_l2*k3_k5*k4_k5*k4_q*k5_l2*l1_l1*
             Ak3*fl17*fl26 );

          Chi(10,9) +=  + H * (  - 2.E+0*P_l2*k3_k5*k4_k5*k4_l1*k5_l2*
             q_l1*Ak3*fl17*fl26 + 6.E+0*P_l2*k3_k5*k4_q*k4_l1*k5_l1*k5_l2*
             Ak3*fl17*fl26 - 6.E+0*P_l2*k3_k5*pow(k4_l1,2)*k5_q*k5_l2*Ak3*
             fl17*fl26 + 2.E+0*P_l2*k3_q*k4_k4*k5_k5*k5_l2*l1_l1*Ak3*fl17*
             fl26 - 2.E+0*P_l2*k3_q*k4_k4*pow(k5_l1,2)*k5_l2*Ak3*fl17*fl26
              - 4.E+0*P_l2*k3_q*k4_k5*k4_l1*k5_l1*k5_l2*Ak3*fl17*fl26 -
             2.E+0*P_l2*k3_q*pow(k4_k5,2)*k5_l2*l1_l1*Ak3*fl17*fl26 + 6.E+0
             *P_l2*k3_q*pow(k4_l1,2)*k5_k5*k5_l2*Ak3*fl17*fl26 + 6.E+0*P_l2
             *k3_l1*k4_k4*k5_k5*k5_l2*q_l1*Ak3*fl17*fl26 + 2.E+0*P_l2*k3_l1
             *k4_k4*k5_q*k5_l1*k5_l2*Ak3*fl17*fl26 + 6.E+0*P_l2*k3_l1*k4_k5
             *k4_q*k5_l1*k5_l2*Ak3*fl17*fl26 - 2.E+0*P_l2*k3_l1*k4_k5*k4_l1
             *k5_q*k5_l2*Ak3*fl17*fl26 - 6.E+0*P_l2*k3_l1*pow(k4_k5,2)*
             k5_l2*q_l1*Ak3*fl17*fl26 - 6.E+0*P_l2*k3_l1*k4_q*k4_l1*k5_k5*
             k5_l2*Ak3*fl17*fl26 );

          Chi(10,9) +=  + G * (  - 4.E+0*P_q*k4_k4*k5_k5*k5_l2*q_l1*l1_l2
             *Bk3*fl17*fl26 + 4.E+0*P_q*k4_k4*k5_k5*k5_l2*q_l2*l1_l1*Bk3*
             fl17*fl26 + 4.E+0*P_q*k4_k4*k5_q*k5_l1*k5_l2*l1_l2*Bk3*fl17*
             fl26 - 4.E+0*P_q*k4_k4*pow(k5_l1,2)*k5_l2*q_l2*Bk3*fl17*fl26
              - 4.E+0*P_q*k4_k5*k4_q*k5_l1*k5_l2*l1_l2*Bk3*fl17*fl26 +
             4.E+0*P_q*k4_k5*k4_l1*k5_l1*k5_l2*q_l2*Bk3*fl17*fl26 + 4.E+0*
             P_q*k4_k5*k4_l2*k5_q*k5_l2*l1_l1*Bk3*fl17*fl26 - 4.E+0*P_q*
             k4_k5*k4_l2*k5_l1*k5_l2*q_l1*Bk3*fl17*fl26 + 4.E+0*P_q*pow(
             k4_k5,2)*k5_l2*q_l1*l1_l2*Bk3*fl17*fl26 - 4.E+0*P_q*pow(
             k4_k5,2)*k5_l2*q_l2*l1_l1*Bk3*fl17*fl26 - 4.E+0*P_q*k4_q*k4_l1
             *k5_l1*pow(k5_l2,2)*Bk3*fl17*fl26 - 4.E+0*P_q*k4_q*k4_l2*k5_k5
             *k5_l2*l1_l1*Bk3*fl17*fl26 + 4.E+0*P_q*k4_q*k4_l2*pow(k5_l1,2)
             *k5_l2*Bk3*fl17*fl26 + 4.E+0*P_q*k4_l1*k4_l2*k5_k5*k5_l2*q_l1*
             Bk3*fl17*fl26 - 4.E+0*P_q*k4_l1*k4_l2*k5_q*k5_l1*k5_l2*Bk3*
             fl17*fl26 + 4.E+0*P_q*pow(k4_l1,2)*k5_q*pow(k5_l2,2)*Bk3*fl17*
             fl26 );

          Chi(10,9) +=  + F * (  - 4.E+0*P_k4*k4_k5*k5_l1*k5_l2*l1_l2*Bk3
             *fl17*fl26 - 4.E+0*P_k4*k4_l1*k5_l1*pow(k5_l2,2)*Bk3*fl17*fl26
              - 4.E+0*P_k4*k4_l2*k5_k5*k5_l2*l1_l1*Bk3*fl17*fl26 + 4.E+0*
             P_k4*k4_l2*pow(k5_l1,2)*k5_l2*Bk3*fl17*fl26 + 4.E+0*P_k5*k4_k4
             *k5_l1*k5_l2*l1_l2*Bk3*fl17*fl26 + 4.E+0*P_k5*k4_k5*k4_l2*
             k5_l2*l1_l1*Bk3*fl17*fl26 - 4.E+0*P_k5*k4_l1*k4_l2*k5_l1*k5_l2
             *Bk3*fl17*fl26 + 4.E+0*P_k5*pow(k4_l1,2)*pow(k5_l2,2)*Bk3*fl17
             *fl26 - 4.E+0*P_l1*k4_k4*k5_k5*k5_l2*l1_l2*Bk3*fl17*fl26 -
             4.E+0*P_l1*k4_k5*k4_l2*k5_l1*k5_l2*Bk3*fl17*fl26 + 4.E+0*P_l1*
             pow(k4_k5,2)*k5_l2*l1_l2*Bk3*fl17*fl26 + 4.E+0*P_l1*k4_l1*
             k4_l2*k5_k5*k5_l2*Bk3*fl17*fl26 + 4.E+0*P_l2*k4_k4*k5_k5*k5_l2
             *l1_l1*Bk3*fl17*fl26 - 4.E+0*P_l2*k4_k4*pow(k5_l1,2)*k5_l2*Bk3
             *fl17*fl26 + 4.E+0*P_l2*k4_k5*k4_l1*k5_l1*k5_l2*Bk3*fl17*fl26
              - 4.E+0*P_l2*pow(k4_k5,2)*k5_l2*l1_l1*Bk3*fl17*fl26 );

          Chi(10,9) +=  + E * ( 4.E+0*k3_k4*k4_k5*k5_l1*k5_l2*l1_l2*Ak3*
             fl17*fl26 + 4.E+0*k3_k4*k4_l1*k5_l1*pow(k5_l2,2)*Ak3*fl17*fl26
              + 4.E+0*k3_k4*k4_l2*k5_k5*k5_l2*l1_l1*Ak3*fl17*fl26 - 4.E+0*
             k3_k4*k4_l2*pow(k5_l1,2)*k5_l2*Ak3*fl17*fl26 - 4.E+0*k3_k5*
             k4_k4*k5_l1*k5_l2*l1_l2*Ak3*fl17*fl26 - 4.E+0*k3_k5*k4_k5*
             k4_l2*k5_l2*l1_l1*Ak3*fl17*fl26 + 4.E+0*k3_k5*k4_l1*k4_l2*
             k5_l1*k5_l2*Ak3*fl17*fl26 - 4.E+0*k3_k5*pow(k4_l1,2)*pow(
             k5_l2,2)*Ak3*fl17*fl26 + 4.E+0*k3_l1*k4_k4*k5_k5*k5_l2*l1_l2*
             Ak3*fl17*fl26 + 4.E+0*k3_l1*k4_k5*k4_l2*k5_l1*k5_l2*Ak3*fl17*
             fl26 - 4.E+0*k3_l1*pow(k4_k5,2)*k5_l2*l1_l2*Ak3*fl17*fl26 -
             4.E+0*k3_l1*k4_l1*k4_l2*k5_k5*k5_l2*Ak3*fl17*fl26 - 4.E+0*
             k3_l2*k4_k4*k5_k5*k5_l2*l1_l1*Ak3*fl17*fl26 + 4.E+0*k3_l2*
             k4_k4*pow(k5_l1,2)*k5_l2*Ak3*fl17*fl26 - 4.E+0*k3_l2*k4_k5*
             k4_l1*k5_l1*k5_l2*Ak3*fl17*fl26 + 4.E+0*k3_l2*pow(k4_k5,2)*
             k5_l2*l1_l1*Ak3*fl17*fl26 );

       Chi(10,10) =
           + H * (  - 8.E+0*P_k4*k4_k5*k5_q*l1_l1*l2_l2*Bk3*fl17*fl27 +
             8.E+0*P_k4*k4_k5*k5_q*pow(l1_l2,2)*Bk3*fl17*fl27 - 8.E+0*P_k4*
             k4_k5*k5_l1*q_l2*l1_l2*Bk3*fl17*fl27 - 8.E+0*P_k4*k4_k5*k5_l2*
             q_l1*l1_l2*Bk3*fl17*fl27 + 8.E+0*P_k4*k4_k5*k5_l2*q_l2*l1_l1*
             Bk3*fl17*fl27 + 8.E+0*P_k4*k4_l1*k5_q*k5_l2*l1_l2*Bk3*fl17*
             fl27 - 8.E+0*P_k4*k4_l1*k5_l1*k5_l2*q_l2*Bk3*fl17*fl27 - 8.E+0
             *P_k4*k4_l1*pow(k5_l2,2)*q_l1*Bk3*fl17*fl27 - 8.E+0*P_k4*k4_l2
             *k5_k5*q_l2*l1_l1*Bk3*fl17*fl27 - 8.E+0*P_k4*k4_l2*k5_q*k5_l1*
             l1_l2*Bk3*fl17*fl27 + 8.E+0*P_k4*k4_l2*k5_l1*k5_l2*q_l1*Bk3*
             fl17*fl27 + 8.E+0*P_k4*k4_l2*pow(k5_l1,2)*q_l2*Bk3*fl17*fl27
              + 8.E+0*P_k5*k4_k4*k5_l1*q_l1*l2_l2*Bk3*fl17*fl27 + 8.E+0*
             P_k5*k4_k5*k4_q*l1_l1*l2_l2*Bk3*fl17*fl27 - 8.E+0*P_k5*k4_k5*
             k4_q*pow(l1_l2,2)*Bk3*fl17*fl27 - 8.E+0*P_k5*k4_k5*k4_l1*q_l1*
             l2_l2*Bk3*fl17*fl27 + 8.E+0*P_k5*k4_k5*k4_l1*q_l2*l1_l2*Bk3*
             fl17*fl27 + 8.E+0*P_k5*k4_k5*k4_l2*q_l1*l1_l2*Bk3*fl17*fl27 -
             8.E+0*P_k5*k4_q*k4_l1*k5_l2*l1_l2*Bk3*fl17*fl27 + 8.E+0*P_k5*
             k4_q*k4_l2*k5_l1*l1_l2*Bk3*fl17*fl27 - 8.E+0*P_k5*k4_l1*k4_l2*
             k5_l1*q_l2*Bk3*fl17*fl27 + 8.E+0*P_k5*k4_l1*k4_l2*k5_l2*q_l1*
             Bk3*fl17*fl27 + 8.E+0*P_k5*pow(k4_l1,2)*k5_l2*q_l2*Bk3*fl17*
             fl27 - 8.E+0*P_k5*pow(k4_l2,2)*k5_l1*q_l1*Bk3*fl17*fl27 -
             8.E+0*P_l1*k4_k4*k5_k5*q_l2*l1_l2*Bk3*fl17*fl27 - 8.E+0*P_l1*
             k4_k4*k5_q*k5_l1*l2_l2*Bk3*fl17*fl27 + 8.E+0*P_l1*k4_k4*k5_l1*
             k5_l2*q_l2*Bk3*fl17*fl27 + 8.E+0*P_l1*k4_k5*k4_q*k5_l2*l1_l2*
             Bk3*fl17*fl27 + 8.E+0*P_l1*k4_k5*k4_l1*k5_q*l2_l2*Bk3*fl17*
             fl27 - 8.E+0*P_l1*k4_k5*k4_l1*k5_l2*q_l2*Bk3*fl17*fl27 - 8.E+0
             *P_l1*k4_k5*k4_l2*k5_q*l1_l2*Bk3*fl17*fl27 - 8.E+0*P_l1*k4_k5*
             k4_l2*k5_l1*q_l2*Bk3*fl17*fl27 + 8.E+0*P_l1*pow(k4_k5,2)*q_l2*
             l1_l2*Bk3*fl17*fl27 + 8.E+0*P_l1*k4_q*k4_l1*pow(k5_l2,2)*Bk3*
             fl17*fl27 - 8.E+0*P_l1*k4_q*k4_l2*k5_l1*k5_l2*Bk3*fl17*fl27 +
             8.E+0*P_l1*k4_l1*k4_l2*k5_k5*q_l2*Bk3*fl17*fl27 - 8.E+0*P_l1*
             k4_l1*k4_l2*k5_q*k5_l2*Bk3*fl17*fl27 + 8.E+0*P_l1*pow(k4_l2,2)
             *k5_q*k5_l1*Bk3*fl17*fl27 + 8.E+0*P_l2*k4_k4*k5_k5*q_l1*l1_l2*
             Bk3*fl17*fl27 - 8.E+0*P_l2*k4_k4*k5_l1*k5_l2*q_l1*Bk3*fl17*
             fl27 + 8.E+0*P_l2*k4_k5*k4_q*k5_l1*l1_l2*Bk3*fl17*fl27 - 8.E+0
             *P_l2*k4_k5*k4_q*k5_l2*l1_l1*Bk3*fl17*fl27 - 8.E+0*P_l2*k4_k5*
             k4_l1*k5_q*l1_l2*Bk3*fl17*fl27 + 8.E+0*P_l2*k4_k5*k4_l1*k5_l2*
             q_l1*Bk3*fl17*fl27 + 8.E+0*P_l2*k4_k5*k4_l2*k5_l1*q_l1*Bk3*
             fl17*fl27 - 8.E+0*P_l2*pow(k4_k5,2)*q_l1*l1_l2*Bk3*fl17*fl27
              + 8.E+0*P_l2*k4_q*k4_l1*k5_l1*k5_l2*Bk3*fl17*fl27 + 8.E+0*
             P_l2*k4_q*k4_l2*k5_k5*l1_l1*Bk3*fl17*fl27 - 8.E+0*P_l2*k4_q*
             k4_l2*pow(k5_l1,2)*Bk3*fl17*fl27 - 8.E+0*P_l2*k4_l1*k4_l2*
             k5_k5*q_l1*Bk3*fl17*fl27 + 8.E+0*P_l2*k4_l1*k4_l2*k5_q*k5_l1*
             Bk3*fl17*fl27 - 8.E+0*P_l2*pow(k4_l1,2)*k5_q*k5_l2*Bk3*fl17*
             fl27 );

          Chi(10,10) +=  + G * ( 4.E+0*P_q*k3_k4*k4_k5*k5_l1*q_l1*l2_l2*
             Ak3*fl17*fl27 + 4.E+0*P_q*k3_k4*k4_q*k5_k5*l1_l1*l2_l2*Ak3*
             fl17*fl27 - 4.E+0*P_q*k3_k4*k4_q*k5_k5*pow(l1_l2,2)*Ak3*fl17*
             fl27 - 4.E+0*P_q*k3_k4*k4_q*pow(k5_l1,2)*l2_l2*Ak3*fl17*fl27
              + 4.E+0*P_q*k3_k4*k4_q*pow(k5_l2,2)*l1_l1*Ak3*fl17*fl27 -
             4.E+0*P_q*k3_k4*k4_l1*k5_k5*q_l1*l2_l2*Ak3*fl17*fl27 + 4.E+0*
             P_q*k3_k4*k4_l1*k5_k5*q_l2*l1_l2*Ak3*fl17*fl27 + 4.E+0*P_q*
             k3_k4*k4_l1*k5_q*k5_l1*l2_l2*Ak3*fl17*fl27 + 4.E+0*P_q*k3_k4*
             k4_l2*k5_k5*q_l1*l1_l2*Ak3*fl17*fl27 - 4.E+0*P_q*k3_k4*k4_l2*
             k5_q*k5_l2*l1_l1*Ak3*fl17*fl27 - 4.E+0*P_q*k3_k5*k4_k4*k5_q*
             l1_l1*l2_l2*Ak3*fl17*fl27 + 4.E+0*P_q*k3_k5*k4_k4*k5_q*pow(
             l1_l2,2)*Ak3*fl17*fl27 - 4.E+0*P_q*k3_k5*k4_k4*k5_l1*q_l2*
             l1_l2*Ak3*fl17*fl27 - 4.E+0*P_q*k3_k5*k4_k4*k5_l2*q_l1*l1_l2*
             Ak3*fl17*fl27 + 4.E+0*P_q*k3_k5*k4_k4*k5_l2*q_l2*l1_l1*Ak3*
             fl17*fl27 - 4.E+0*P_q*k3_k5*k4_k5*k4_l2*q_l2*l1_l1*Ak3*fl17*
             fl27 + 4.E+0*P_q*k3_k5*k4_q*k4_l1*k5_l1*l2_l2*Ak3*fl17*fl27 -
             4.E+0*P_q*k3_k5*k4_q*k4_l2*k5_l2*l1_l1*Ak3*fl17*fl27 - 4.E+0*
             P_q*k3_k5*pow(k4_l1,2)*k5_q*l2_l2*Ak3*fl17*fl27 + 4.E+0*P_q*
             k3_k5*pow(k4_l2,2)*k5_q*l1_l1*Ak3*fl17*fl27 + 4.E+0*P_q*k3_q*
             k4_k4*k5_l1*k5_l2*l1_l2*Ak3*fl17*fl27 - 4.E+0*P_q*k3_q*k4_k4*
             pow(k5_l2,2)*l1_l1*Ak3*fl17*fl27 - 4.E+0*P_q*k3_q*k4_k5*k4_l1*
             k5_l1*l2_l2*Ak3*fl17*fl27 + 4.E+0*P_q*k3_q*k4_k5*k4_l2*k5_l2*
             l1_l1*Ak3*fl17*fl27 - 4.E+0*P_q*k3_q*k4_l1*k4_l2*k5_k5*l1_l2*
             Ak3*fl17*fl27 + 4.E+0*P_q*k3_q*pow(k4_l1,2)*k5_k5*l2_l2*Ak3*
             fl17*fl27 + 4.E+0*P_q*k3_l1*k4_k4*k5_k5*q_l1*l2_l2*Ak3*fl17*
             fl27 - 4.E+0*P_q*k3_l1*k4_k4*k5_q*k5_l2*l1_l2*Ak3*fl17*fl27 +
             4.E+0*P_q*k3_l1*k4_k4*pow(k5_l2,2)*q_l1*Ak3*fl17*fl27 + 4.E+0*
             P_q*k3_l1*k4_k5*k4_q*k5_l1*l2_l2*Ak3*fl17*fl27 - 4.E+0*P_q*
             k3_l1*pow(k4_k5,2)*q_l1*l2_l2*Ak3*fl17*fl27 - 4.E+0*P_q*k3_l1*
             k4_q*k4_l1*k5_k5*l2_l2*Ak3*fl17*fl27 + 4.E+0*P_q*k3_l1*k4_q*
             k4_l2*k5_k5*l1_l2*Ak3*fl17*fl27 - 4.E+0*P_q*k3_l1*pow(k4_l2,2)
             *k5_k5*q_l1*Ak3*fl17*fl27 - 4.E+0*P_q*k3_l2*k4_k4*k5_k5*q_l2*
             l1_l1*Ak3*fl17*fl27 - 4.E+0*P_q*k3_l2*k4_k4*k5_q*k5_l1*l1_l2*
             Ak3*fl17*fl27 + 4.E+0*P_q*k3_l2*k4_k4*k5_q*k5_l2*l1_l1*Ak3*
             fl17*fl27 + 4.E+0*P_q*k3_l2*k4_k4*pow(k5_l1,2)*q_l2*Ak3*fl17*
             fl27 - 4.E+0*P_q*k3_l2*k4_k5*k4_l2*k5_q*l1_l1*Ak3*fl17*fl27 +
             4.E+0*P_q*k3_l2*pow(k4_k5,2)*q_l2*l1_l1*Ak3*fl17*fl27 + 4.E+0*
             P_q*k3_l2*k4_q*k4_l1*k5_k5*l1_l2*Ak3*fl17*fl27 - 4.E+0*P_q*
             k3_l2*pow(k4_l1,2)*k5_k5*q_l2*Ak3*fl17*fl27 );

          Chi(10,10) +=  + F * ( 4.E+0*P_k3*k4_k4*k5_l1*k5_l2*l1_l2*Ak3*
             fl17*fl27 - 4.E+0*P_k3*k4_k4*pow(k5_l2,2)*l1_l1*Ak3*fl17*fl27
              - 4.E+0*P_k3*k4_k5*k4_l1*k5_l1*l2_l2*Ak3*fl17*fl27 + 4.E+0*
             P_k3*k4_k5*k4_l2*k5_l2*l1_l1*Ak3*fl17*fl27 - 4.E+0*P_k3*k4_l1*
             k4_l2*k5_k5*l1_l2*Ak3*fl17*fl27 + 4.E+0*P_k3*pow(k4_l1,2)*
             k5_k5*l2_l2*Ak3*fl17*fl27 + 4.E+0*P_k4*k3_k4*k5_k5*l1_l1*l2_l2
             *Ak3*fl17*fl27 - 4.E+0*P_k4*k3_k4*k5_k5*pow(l1_l2,2)*Ak3*fl17*
             fl27 - 4.E+0*P_k4*k3_k4*pow(k5_l1,2)*l2_l2*Ak3*fl17*fl27 +
             4.E+0*P_k4*k3_k4*pow(k5_l2,2)*l1_l1*Ak3*fl17*fl27 + 4.E+0*P_k4
             *k3_k5*k4_l1*k5_l1*l2_l2*Ak3*fl17*fl27 - 4.E+0*P_k4*k3_k5*
             k4_l2*k5_l2*l1_l1*Ak3*fl17*fl27 + 4.E+0*P_k4*k3_l1*k4_k5*k5_l1
             *l2_l2*Ak3*fl17*fl27 - 4.E+0*P_k4*k3_l1*k4_l1*k5_k5*l2_l2*Ak3*
             fl17*fl27 + 4.E+0*P_k4*k3_l1*k4_l2*k5_k5*l1_l2*Ak3*fl17*fl27
              + 4.E+0*P_k4*k3_l2*k4_l1*k5_k5*l1_l2*Ak3*fl17*fl27 + 4.E+0*
             P_k5*k3_k4*k4_l1*k5_l1*l2_l2*Ak3*fl17*fl27 - 4.E+0*P_k5*k3_k4*
             k4_l2*k5_l2*l1_l1*Ak3*fl17*fl27 - 4.E+0*P_k5*k3_k5*k4_k4*l1_l1
             *l2_l2*Ak3*fl17*fl27 + 4.E+0*P_k5*k3_k5*k4_k4*pow(l1_l2,2)*Ak3
             *fl17*fl27 - 4.E+0*P_k5*k3_k5*pow(k4_l1,2)*l2_l2*Ak3*fl17*fl27
              + 4.E+0*P_k5*k3_k5*pow(k4_l2,2)*l1_l1*Ak3*fl17*fl27 - 4.E+0*
             P_k5*k3_l1*k4_k4*k5_l2*l1_l2*Ak3*fl17*fl27 - 4.E+0*P_k5*k3_l2*
             k4_k4*k5_l1*l1_l2*Ak3*fl17*fl27 + 4.E+0*P_k5*k3_l2*k4_k4*k5_l2
             *l1_l1*Ak3*fl17*fl27 - 4.E+0*P_k5*k3_l2*k4_k5*k4_l2*l1_l1*Ak3*
             fl17*fl27 + 4.E+0*P_l1*k3_k4*k4_k5*k5_l1*l2_l2*Ak3*fl17*fl27
              - 4.E+0*P_l1*k3_k4*k4_l1*k5_k5*l2_l2*Ak3*fl17*fl27 + 4.E+0*
             P_l1*k3_k4*k4_l2*k5_k5*l1_l2*Ak3*fl17*fl27 - 4.E+0*P_l1*k3_k5*
             k4_k4*k5_l2*l1_l2*Ak3*fl17*fl27 + 4.E+0*P_l1*k3_l1*k4_k4*k5_k5
             *l2_l2*Ak3*fl17*fl27 + 4.E+0*P_l1*k3_l1*k4_k4*pow(k5_l2,2)*Ak3
             *fl17*fl27 - 4.E+0*P_l1*k3_l1*pow(k4_k5,2)*l2_l2*Ak3*fl17*fl27
              - 4.E+0*P_l1*k3_l1*pow(k4_l2,2)*k5_k5*Ak3*fl17*fl27 + 4.E+0*
             P_l2*k3_k4*k4_l1*k5_k5*l1_l2*Ak3*fl17*fl27 - 4.E+0*P_l2*k3_k5*
             k4_k4*k5_l1*l1_l2*Ak3*fl17*fl27 + 4.E+0*P_l2*k3_k5*k4_k4*k5_l2
             *l1_l1*Ak3*fl17*fl27 - 4.E+0*P_l2*k3_k5*k4_k5*k4_l2*l1_l1*Ak3*
             fl17*fl27 - 4.E+0*P_l2*k3_l2*k4_k4*k5_k5*l1_l1*Ak3*fl17*fl27
              + 4.E+0*P_l2*k3_l2*k4_k4*pow(k5_l1,2)*Ak3*fl17*fl27 + 4.E+0*
             P_l2*k3_l2*pow(k4_k5,2)*l1_l1*Ak3*fl17*fl27 - 4.E+0*P_l2*k3_l2
             *pow(k4_l1,2)*k5_k5*Ak3*fl17*fl27 );

          Chi(10,10) +=  + E * (  - 4.E+0*k4_k4*k5_k5*l1_l1*l2_l2*Bk3*
             fl17*fl27 + 4.E+0*k4_k4*k5_k5*pow(l1_l2,2)*Bk3*fl17*fl27 -
             4.E+0*k4_k4*k5_l1*k5_l2*l1_l2*Bk3*fl17*fl27 + 4.E+0*k4_k4*pow(
             k5_l1,2)*l2_l2*Bk3*fl17*fl27 - 4.E+0*k4_k5*k4_l1*k5_l1*l2_l2*
             Bk3*fl17*fl27 + 8.E+0*k4_k5*k4_l2*k5_l1*l1_l2*Bk3*fl17*fl27 -
             4.E+0*k4_k5*k4_l2*k5_l2*l1_l1*Bk3*fl17*fl27 + 4.E+0*pow(
             k4_k5,2)*l1_l1*l2_l2*Bk3*fl17*fl27 - 4.E+0*pow(k4_k5,2)*pow(
             l1_l2,2)*Bk3*fl17*fl27 - 4.E+0*k4_l1*k4_l2*k5_k5*l1_l2*Bk3*
             fl17*fl27 + 8.E+0*k4_l1*k4_l2*k5_l1*k5_l2*Bk3*fl17*fl27 -
             4.E+0*pow(k4_l1,2)*pow(k5_l2,2)*Bk3*fl17*fl27 + 4.E+0*pow(
             k4_l2,2)*k5_k5*l1_l1*Bk3*fl17*fl27 - 4.E+0*pow(k4_l2,2)*pow(
             k5_l1,2)*Bk3*fl17*fl27 );


        //#########################################################################################


}


//#######################################################################################################################









